import unittest
from unittest.mock import patch

from .bases import BaseApproverTest
from ...handlers.file_approvers import PBSApprover
from ...handlers.file_approvers.exceptions import PBSError


class TestPBSApprover(BaseApproverTest, unittest.TestCase):
    _approver_class = PBSApprover
    _exception = PBSError

    def setUp(self):
        super().setUp()
        self.approver.queuing_system = "local"
        self.approver.command = "command"

    @patch("abisuite.handlers.file_approvers.pbs_approver.which")
    def test_approver_works(self, whichmock):
        whichmock.return_value = "not None"
        super().test_approver_works()

    @patch("abisuite.handlers.file_approvers.pbs_approver.which")
    def test_command_approval(self, whichmock):
        whichmock.return_value = "not None"
        # check that a command exists in env but is not directly a file works
        self.approver.command = "some_command_without_a_direct_file"
        # check that if force local is True, nothing happens
        self.approver.queuing_system = "local"
        self.approver.validate()
        self.assertTrue(self.approver.is_valid, msg=self.approver.errors)
        # check that nothing happens if it's not the case either
        self.approver.queuing_system = "torque"
        self.approver.validate()
        self.assertTrue(self.approver.is_valid)
        # check that an error is raised if which returns None
        whichmock.return_value = None
        self.approver.validate()
        self.assertFalse(self.approver.is_valid)
