import os

from .bases import BaseFilesWriter
from ...file_structures import AbinitFilesStructure


class AbinitFilesWriter(BaseFilesWriter):
    """Class that can write an Abinit files file."""
    _loggername = "AbinitFilesWriter"
    _structure_class = AbinitFilesStructure

    @property
    def lines(self):
        lines = super().lines
        # third is input data prefix
        lines.append(os.path.join(self.input_data_dir,
                                  self.input_data_prefix) + "\n")
        # fourth is output data prefix
        lines.append(os.path.join(self.output_data_dir,
                                  self.output_data_prefix) + "\n")
        # fifth is tmp data prefix
        lines.append(os.path.join(self.tmp_data_dir,
                                  self.tmp_data_prefix) + "\n")
        # finally it is the pseudos
        from ...file_handlers import PseudoFile
        for pseudo in self.pseudos:
            if isinstance(pseudo, PseudoFile):
                pseudo = pseudo.path
            lines += pseudo + "\n"
        return lines
