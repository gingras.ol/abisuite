import unittest

from .bases import BaseParserTest, BaseInputParserTest
from ..variables_for_tests import (
        qe_dosx_vars, qe_dynmatx_vars, qe_epsilonx_vars, qe_epwx_vars,
        qe_fsx_vars,
        qe_ld1x_vars,
        qe_matdynx_vars,
        qe_phx_dyn0_example, qe_phxlog_example, qe_phx_vars,
        qe_ppx_vars, qe_projwfcx_vars, qe_pwxlog_example,
        qe_pwx_vars, qe_pw2wannier90x_vars, qe_q2rx_vars,
        )
from ...handlers.file_parsers import (
        QEDOSInputParser, QEDynmatInputParser, QEEpsilonInputParser,
        QEEPWInputParser,
        QEFSInputParser, QELD1InputParser, QEMatdynInputParser,
        QEPHDyn0Parser, QEPHInputParser, QEPHLogParser,
        QEPPInputParser, QEProjwfcInputParser,
        QEPWInputParser, QEPWLogParser, QEPW2Wannier90InputParser,
        QEQ2RInputParser,
        )
from ...handlers.file_writers import (
        QEDOSInputWriter, QEDynmatInputWriter, QEEpsilonInputWriter,
        QEEPWInputWriter,
        QEFSInputWriter, QELD1InputWriter, QEMatdynInputWriter,
        QEPHInputWriter, QEPPInputWriter, QEProjwfcInputWriter,
        QEPWInputWriter, QEPW2Wannier90InputWriter, QEQ2RInputWriter,
        )


class TestQEPHDyn0Parser(BaseParserTest, unittest.TestCase):
    _example_file = qe_phx_dyn0_example
    _parser_class = QEPHDyn0Parser

    def test_parsing(self):
        super().test_parsing()
        # confirm data extracted
        qpts = self.parser.qpts
        self.assertEqual(len(qpts), self.parser.nqpt)
        self.assertListEqual(self.parser.qgrid, [2, 2, 2])
        self.assertListEqual(qpts,
                             [[0.0, 0.0, 0.0],
                              [0.5, -0.5, 0.5],
                              [0.0, -1.0, 0.0]])


class TestQEPHLogParser(BaseParserTest, unittest.TestCase):
    _example_file = qe_phxlog_example
    _parser_class = QEPHLogParser

    def test_parsing(self):
        super().test_parsing()
        # data directly taken from file
        freqs = self.parser.phonon_frequencies
        # 3 modes are computed
        self.assertEqual(len(self.parser.phonon_frequencies), 3)
        # silicon has 6 modes
        for freq in freqs:
            self.assertEqual(len(freq["frequencies_THz"]), 6)
            self.assertEqual(len(freq["frequencies_cm-1"]), 6)
            self.assertEqual(len(freq["q_point"]), 3)


class TestQEPWLogParser(BaseParserTest, unittest.TestCase):
    _example_file = qe_pwxlog_example
    _parser_class = QEPWLogParser


class TestQEDOSInputParser(BaseInputParserTest, unittest.TestCase):
    _input_vars = qe_dosx_vars.copy()
    _parser_class = QEDOSInputParser
    _writer_class = QEDOSInputWriter


class TestQEDynmatInputParser(BaseInputParserTest, unittest.TestCase):
    _input_vars = qe_dynmatx_vars.copy()
    _parser_class = QEDynmatInputParser
    _writer_class = QEDynmatInputWriter


class TestQEEpsilonInputParser(BaseInputParserTest, unittest.TestCase):
    _input_vars = qe_epsilonx_vars.copy()
    _parser_class = QEEpsilonInputParser
    _writer_class = QEEpsilonInputWriter


class TestQEEPWInputParser(BaseInputParserTest, unittest.TestCase):
    _input_vars = qe_epwx_vars.copy()
    _parser_class = QEEPWInputParser
    _writer_class = QEEPWInputWriter


class TestQEFSInputParser(BaseInputParserTest, unittest.TestCase):
    _input_vars = qe_fsx_vars.copy()
    _parser_class = QEFSInputParser
    _writer_class = QEFSInputWriter


class TestQELD1InputParser(BaseInputParserTest, unittest.TestCase):
    _input_vars = qe_ld1x_vars.copy()
    _parser_class = QELD1InputParser
    _writer_class = QELD1InputWriter


class TestQEMatdynInputParser(BaseInputParserTest, unittest.TestCase):
    _input_vars = qe_matdynx_vars.copy()
    _parser_class = QEMatdynInputParser
    _writer_class = QEMatdynInputWriter


class TestQEPHInputParser(BaseInputParserTest, unittest.TestCase):
    _input_vars = qe_phx_vars.copy()
    _parser_class = QEPHInputParser
    _writer_class = QEPHInputWriter


class TestQEPPInputParser(BaseInputParserTest, unittest.TestCase):
    _input_vars = qe_ppx_vars.copy()
    _parser_class = QEPPInputParser
    _writer_class = QEPPInputWriter


class TestQEProjwfcInputParser(BaseInputParserTest, unittest.TestCase):
    _input_vars = qe_projwfcx_vars.copy()
    _parser_class = QEProjwfcInputParser
    _writer_class = QEProjwfcInputWriter


class TestQEPWInputParser(BaseInputParserTest, unittest.TestCase):
    _input_vars = qe_pwx_vars.copy()
    _parser_class = QEPWInputParser
    _writer_class = QEPWInputWriter


class TestQEPW2Wannier90InputParser(BaseInputParserTest, unittest.TestCase):
    _input_vars = qe_pw2wannier90x_vars.copy()
    _parser_class = QEPW2Wannier90InputParser
    _writer_class = QEPW2Wannier90InputWriter


class TestQEQ2RInputParser(BaseInputParserTest, unittest.TestCase):
    _input_vars = qe_q2rx_vars.copy()
    _parser_class = QEQ2RInputParser
    _writer_class = QEQ2RInputWriter
