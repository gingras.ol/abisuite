import os

from .bases import BaseBandFreqParser
from ...file_structures import QEMatdynFreqStructure
from ....routines import decompose_line


class QEMatdynFreqParser(BaseBandFreqParser):
    """A Quantum Espresso .freq file parser that extracts data from
    a .freq file produced by the 'matdyn.x' script.
    """
    _expected_ending = ".freq"
    _loggername = "QEMatdynFreqParser"
    _structure_class = QEMatdynFreqStructure

    @classmethod
    def _filepath_from_meta(cls, meta, *args, **kwargs):
        return os.path.join(meta.output_data_dir,
                            meta.jobname + cls._expected_ending)

    def _extract_data_many_atoms(self, lines):
        eigs = []
        kpts = []
        # energies are grouped by 6 for Matdyn freq files.
        # compute number of lines for the eigenenergies
        nsublines = self.nbands // 6  # min number of lines
        if self.nbands % 6:
            # extra data
            nsublines += 1
        skip = 1
        for j, line in enumerate(lines):
            if j < skip:
                continue
            # data alternates between coordinates and energies
            s, i, f = decompose_line(line)
            # this breaks if there is only 1 atom thus 3 branches
            if len(f) == 3:
                # coordinate line
                kpts.append(f)
                self._logger.debug(f"Found kpt: {f}")
                continue
            # else we have an energy line
            eigs_this_kpt = []
            for jj, subline in enumerate(lines[j:]):
                if jj + 1 > nsublines:
                    skip = j + jj
                    break
                s, i, f = decompose_line(subline)
                eigs_this_kpt += f
            else:
                # arrived at the end of the file
                skip = len(lines)  # finish the main loop
            self._logger.debug(f"Found eienvalues: {eigs_this_kpt}")
            eigs.append(eigs_this_kpt)
        return kpts, eigs
