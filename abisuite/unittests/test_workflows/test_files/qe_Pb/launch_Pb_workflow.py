from abisuite import QEWorkflow


GAMMA = {r"$\Gamma$": [0.0, 0.0, 0.0]}   # Gamma
X = {"X": [0.0, 0.5, 0.5]}   # X
K = {"K": [3/8, 3/8, 3/4]}
L = {"L": [1/2, 1/2, 1/2]}
W = {"W": [1/4, 1/2, 3/4]}

NQPTS = 100  # number of points between each symmetry points
NKPTS = 100
KPATH = [GAMMA, X, W, L, K, GAMMA, W]
QPATH = KPATH

workflow = QEWorkflow(
        band_structure=True,
        fermi_surface=True,
        gs=True,
        gs_ecut_convergence=True,
        lattice_expansion=False,
        gs_kgrid_convergence=False,
        gs_smearing_convergence=True,
        phonon_ecut_convergence=True,
        phonon_smearing_convergence=True,
        relaxation=True,
        epw_interpolation=True,
        ibte=False,
        ibte_fsthick_convergence=False,
        converged_ibte_fsthick=0.1,
        ibte_fine_grid_convergence=True,
        epw_interpolation_with_thermal_expansion=False,
        ibte_with_thermal_expansion=False,
        phonon_dispersion=True,
        phonon_dispersion_qgrid_convergence=False,
        ziman=True,
        )

workflow.set_gs_ecut_convergence(
        root_workdir="gs_ecut_convergence",
        scf_input_variables={
                "calculation": "scf",
                "ibrav": 2,
                "ntyp": 1,
                "nat": 1,
                "celldm(1)": 9.2225583816,
                "pseudo_dir": "../pseudos",
                "occupations": "smearing",
                "degauss": 0.1,
                "conv_thr": 1.0e-8,
                "atomic_species": [
                    {"atom": "Pb",
                     "atomic_mass": 207.2,
                     "pseudo": "pb_s.UPF",
                     }],
                "k_points": {"parameter": "automatic",
                             "k_points": [8, 8, 8, 1, 1, 1],
                             },
                "atomic_positions": {"parameter": "crystal",
                                     "positions": {"Pb": [0.0, 0.0, 0.0]}},
                },
        scf_calculation_parameters={
            "mpi_command": "mpirun -np 4",
            "command_arguments": "-npool 4",
            },
        scf_ecuts=[10, 20, 30, 50, 100],
        scf_convergence_criterion=100,
        plot_calculation_parameters={"show": False},
        )
# workflow.set_gs_kgrid_convergence(
#         root_workdir="gs_kgrid_convergence",
#         scf_input_variables={
#                 "calculation": "scf",
#                 "ibrav": 2,
#                 "ntyp": 1,
#                 "nat": 1,
#                 "celldm(1)": 9.2225583816,
#                 "pseudo_dir": "../pseudos",
#                 "occupations": "smearing",
#                 "degauss": 0.1,
#                 "conv_thr": 1.0e-8,
#                 "atomic_species": [
#                     {"atom": "Pb",
#                      "atomic_mass": 207.2,
#                      "pseudo": "pb_s.UPF",
#                      }],
#                 "k_points": {"parameter": "automatic",
#                              "k_points": [8, 8, 8, 1, 1, 1],
#                              },
#                 "atomic_positions": {"parameter": "crystal",
#                                      "positions": {"Pb": [0.0, 0.0, 0.0]}},
#                 },
#         scf_calculation_parameters={
#             "mpi_command": "mpirun -np 4",
#             "command_arguments": "-npool 4",
#             },
#         scf_kgrids=[[2, 2, 2, 1, 1, 1],
#                     [4, 4, 4, 1, 1, 1],
#                     [8, 8, 8, 1, 1, 1]],
#         scf_convergence_criterion=10,
#         use_gs_converged_ecut=True,
#         plot_calculation_parameters={"show": False},
#         )

workflow.set_gs_smearing_convergence(
        root_workdir="gs_smearing_convergence",
        scf_input_variables={
                "calculation": "scf",
                # "ibrav": 2,
                # "ntyp": 1,
                # "nat": 1,
                # "celldm(1)": 9.2225583816,
                "pseudo_dir": "../pseudos",
                "occupations": "smearing",
                "conv_thr": 1.0e-8,
                # "atomic_species": [
                #     {"atom": "Pb",
                #      "atomic_mass": 207.2,
                #      "pseudo": "pb_s.UPF",
                #      }],
                # "atomic_positions": {"parameter": "crystal",
                #                      "positions": {"Pb": [0.0, 0.0, 0.0]}},
                },
        scf_calculation_parameters={
            "mpi_command": "mpirun -np 4",
            "command_arguments": "-npool 4",
            },
        scf_kgrids=[[x, x, x, 1, 1, 1] for x in [2, 4, 8]],
        scf_smearings=[0.1, 0.01],
        scf_convergence_criterion=10,
        use_gs_converged_ecut=True,
        use_geometry_from="gs_ecut_convergence",
        plot_calculation_parameters={"show": False},
        )

workflow.set_relaxation(
        root_workdir="relax_run",
        maximum_relaxations=20,
        relax_atoms=False,  # only one atom, don't need to relax
        relax_cell=True,
        scf_input_variables={
                "calculation": "scf",
                "ibrav": 2,
                "ntyp": 1,
                "nat": 1,
                "celldm(1)": 9.2225583816,
                "pseudo_dir": "../pseudos",
                "occupations": "smearing",
                "press_conv_thr": 0.0001,
                "forc_conv_thr": 1e-6,
                "etot_conv_thr": 1e-6,
                "ecutwfc": 100,
                "conv_thr": 1.0e-8,
                "atomic_species": [
                    {"atom": "Pb",
                     "atomic_mass": 207.2,
                     "pseudo": "pb_s.UPF",
                     }],
                "atomic_positions": {"parameter": "crystal",
                                     "positions": {"Pb": [0.0, 0.0, 0.0]}},
                },
        scf_calculation_parameters={
            "mpi_command": "mpirun -np 4",
            "command_arguments": "-npool 4",
            },
        # use_gs_converged_ecut=True,
        use_gs_converged_smearing=True,
        )
workflow.set_phonon_ecut_convergence(
        root_workdir="phonon_ecut_convergence",
        scf_input_variables={
                "calculation": "scf",
                "pseudo_dir": "../pseudos",
                "conv_thr": 1.0e-8,
                },
        use_relaxed_geometry=True,
        use_gs_converged_smearing=True,
        scf_ecuts=[10, 20, 30],
        scf_calculation_parameters={
            "mpi_command": "mpirun -np 4",
            "command_arguments": "-npool 4",
            },
        phonons_qpt=[0.0, 0.0, 0.0],  # gamma
        phonons_calculation_parameters={
            "mpi_command": "mpirun -np 4",
            "command_arguments": "-npool 4",
            },
        phonons_convergence_criterion=10,
        plot_calculation_parameters={"show": False},
        )

workflow.set_phonon_smearing_convergence(
        root_workdir="phonon_smearing_convergence",
        scf_input_variables={
                "calculation": "scf",
                "pseudo_dir": "../pseudos",
                "occupations": "smearing",
                "conv_thr": 1.0e-8,
                },
        use_relaxed_geometry=True,
        use_phonon_converged_ecut=True,
        scf_kgrids=[[x, x, x, 1, 1, 1] for x in [2, 4, 8]],
        scf_smearings=[0.1, 0.01],
        scf_calculation_parameters={
            "mpi_command": "mpirun -np 4",
            "command_arguments": "-npool 4",
            },
        phonons_qpt=[0.0, 0.0, 0.0],  # gamma
        phonons_calculation_parameters={
            "mpi_command": "mpirun -np 4",
            "command_arguments": "-npool 4",
            },
        phonons_convergence_criterion=10,
        plot_calculation_parameters={"show": False},
        )


workflow.set_gs(
        root_workdir="gs_run",
        scf_input_variables={
                "calculation": "scf",
                "pseudo_dir": "../pseudos",
                "occupations": "smearing",
                "degauss": 0.1,
                "conv_thr": 1.0e-8,
                "k_points": {"parameter": "automatic",
                             "k_points": [8, 8, 8, 1, 1, 1],
                             },
                },
        use_relaxed_geometry=True,
        use_gs_converged_ecut=True,
        scf_calculation_parameters={
            "mpi_command": "mpirun -np 4",
            "command_arguments": "-npool 4",
            },
        )
workflow.set_band_structure(
        root_workdir="band_structure",
        use_gs=True,
        band_structure_kpoint_path=KPATH,
        band_structure_kpoint_path_density=NKPTS,
        band_structure_calculation_parameters={
            "mpi_command": "mpirun -np 4", "command_arguments": "-npool 4"},
        plot_calculation_parameters={"show": False},
        )
workflow.set_fermi_surface(
        root_workdir="fermi_surface",
        use_gs=True,
        nscf_kgrid=[8, 8, 8],
        nscf_calculation_parameters={
            "mpi_command": "mpirun -np 4", "command_arguments": "-npool 4"},
        nscf_input_variables={},
        plot_calculation_parameters={},
        )

workflow.set_lattice_expansion(
        deltas_volumes=[0, -2, 2, -4, 4, -6, 6],
        asr="crystal",
        temperatures=[50, 150, 300, 600],
        scf_input_variables={
            "occupations": "smearing",
            "conv_thr": 1.0e-8,
            "ibrav": 2,
            "ntyp": 1,
            "nat": 1,
            "celldm(1)": 9.2225583816,
            "pseudo_dir": "../pseudos",
            "conv_thr": 1.0e-8,
            "atomic_species": [
                {"atom": "Pb",
                 "atomic_mass": 207.2,
                 "pseudo": "pb_s.UPF",
                 }],
            "atomic_positions": {"parameter": "crystal",
                                 "positions": {"Pb": [0.0, 0.0, 0.0]}},
            "occupations": "smearing",
            "ecutwfc": 40,
            "degauss": 0.1,
            "k_points": {"parameter": "automatic",
                         "k_points": [6, 6, 6, 1, 1, 1],
                         },
            },
        scf_calculation_parameters={
            "mpi_command": "mpirun -np 4", "command_arguments": "-npool 4",
            },
        use_relaxed_geometry=False,
        use_phonon_converged_smearing=False,
        use_phonon_converged_ecut=False,
        bulk_modulus_initial_guess=37,
        root_workdir="lattice_expansion",
        phonons_input_variables={
            "ldisp": True,
            "tr2_ph": 1e-12,
            },
        phonons_qpoint_grid=[2, 2, 2],
        phonons_calculation_parameters={
            "mpi_command": "mpirun -np 4",
            "command_arguments": "-npool 4",
            },
        q2r_input_variables={},
        q2r_calculation_parameters={},
        matdyn_calculation_parameters={},
        plot_calculation_parameters={"show": False},
        )

workflow.set_phonon_dispersion(
        root_workdir="phonon_dispersion",
        use_gs=True,
        phonons_input_variables={"tr2_ph": 1e-12},
        phonons_qpoint_grid=[2, 2, 2],
        phonons_calculation_parameters={
            "mpi_command": "mpirun -np 4", "command_arguments": "-npool 4",
            },
        q2r_input_variables={},
        q2r_calculation_parameters={},
        matdyn_input_variables={"asr": "crystal", "q_in_cryst_coord": True},
        qpoint_path=QPATH,
        qpoint_path_density=NQPTS,
        matdyn_calculation_parameters={},
        plot_calculation_parameters={"show": False},
        )
# workflow.set_phonon_dispersion_qgrid_convergence(
#         root_workdir="phonon_dispersion_qgrid_convergence",
#         use_gs=True,
#         phonons_input_variables={"tr2_ph": 1e-12},
#         phonons_qpoint_grids=[[2, 2, 2], [4, 4, 4]],
#         phonons_calculation_parameters={
#             "mpi_command": "mpirun -np 4", "command_arguments": "-npool 4",
#             },
#         q2r_input_variables={},
#         q2r_calculation_parameters={},
#         matdyn_input_variables={"asr": "crystal"},
#         matdyn_qpoint_path=QPATH,
#         matdyn_qpoint_path_density=NQPTS,
#         matdyn_calculation_parameters={},
#         )
workflow.set_epw_interpolation(
        root_workdir="epw_interpolation",
        use_gs=True,
        nscf_kgrid=[6, 6, 6],
        nscf_input_variables={
            "tprnfor": True,
            "tstress": True,
            "conv_thr": 1e-10,
            "diagonalization": "cg",
            "diago_full_acc": True,
            "noncolin": False,
            "lspinorb": False,
            "nbnd": 15},
        nscf_calculation_parameters={
            "mpi_command": "mpirun -np 4", "command_arguments": "-npool 4"},
        use_band_structure=True,
        use_phonon_dispersion=True,
        epw_input_variables={
            "amass(1)": 207.2,
            "elph": True,
            "band_plot": True,
            "etf_mem": 0,
            "kmaps": False,
            "epbwrite": True,
            "epbread": False,
            "epwwrite": True,
            "epwread": False,
            "nbndsub": 4,   # number of wannier functions to use
            # "nbndskip": 10,  # nbands under the disentanglement window
            "bands_skipped": "exclude_bands = 1-5",
            "wannierize": True,
            "num_iter": 1000,
            "wdata(1)": "dis_num_iter = 1000",
            "wdata(2)": "guiding_centres = True",
            "wdata(3)": "num_print_cycles = 100",
            "wdata(4)": "conv_window = 10",
            "dis_win_max": 21,
            "dis_win_min": -3,
            "dis_froz_min": -3,
            "dis_froz_max": 13.5,
            "proj(1)": "Pb:sp3",
            "iverbosity": 0,
            "elecselfen": False,
            "phonselfen": False,
            "fsthick": 6,  # fermi surface thickness
            "degaussw": 0.0,
            "ngaussw": -99,  # force FD distribution from the start
            "a2f": False,
            "vme": True,
            },
        epw_calculation_parameters={
               "mpi_command": "mpirun -np 4", "command_arguments": "-npool 4"
                },
        plot_calculation_parameters={"show": False},
        )
workflow.set_ibte_fsthick_convergence(
        root_workdir="ibte_fsthick_convergence",
        ibte_input_variables={
            "assume_metal": True,
            "degaussw": 0.0,
            "restart_step": 100,
            "etf_mem": 0,
            "ngaussw": -99,
            "vme": True,
            "temps": [50, 100, 300, 600],
            "nstemp": 4,
            "nkf1": 10, "nkf2": 10, "nkf3": 10,
            "nqf1": 10, "nqf2": 10, "nqf3": 10,
            },
        ibte_calculation_parameters={
            "mpi_command": "mpirun -np 4", "command_arguments": "-npool 4",
            },
        ibte_fsthicks=[0.1, 0.2, 0.3],
        plot_calculation_parameters={"show": False},
        )
workflow.set_ibte_fine_grid_convergence(
        root_workdir="ibte_fine_grid_convergence",
        ibte_input_variables={
            "assume_metal": True,
            "degaussw": 0.0,
            "restart_step": 100,
            "etf_mem": 0,
            "ngaussw": -99,
            "vme": True,
            "temps": [50, 100, 300, 600],
            "nstemp": 4,
            "nkf1": 10, "nkf2": 10, "nkf3": 10,
            "nqf1": 10, "nqf2": 10, "nqf3": 10,
            },
        ibte_fine_grids=[
            {"nqf1": x, "nqf2": x, "nqf3": x, "nkf1": x, "nkf2": x, "nkf3": x}
            for x in [10, 20]],
        ibte_calculation_parameters={
            "mpi_command": "mpirun -np 4", "command_arguments": "-npool 4",
            },
        plot_calculation_parameters={"show": True},
        use_ibte_converged_fsthick=True,
        )
# workflow.set_ibte(
#         root_workdir="ibte",
#         ibte_input_variables={
#             "assume_metal": True,
#             "restart_step": 100,
#             "degaussw": 0.0,
#             "etf_mem": 0,
#             "fsthick": 0.1,
#             "ngaussw": -99,
#             "vme": True,
#             "temps": [50, 100, 300, 600],
#             "nstemp": 4,
#             "nkf1": 10, "nkf2": 10, "nkf3": 10,
#             "nqf1": 10, "nqf2": 10, "nqf3": 10},
#         ibte_calculation_parameters={
#             "mpi_command": "mpirun -np 4",
#             "command_arguments": "-npool 4"},
#         plot_calculation_parameters={"elements": [[0, 0]], },
#         )
#
workflow.set_epw_interpolation_with_thermal_expansion(
        root_workdir="epw_with_lattice_expansion",
        asr="crystal",
        scf_input_variables={
            "calculation": "scf",
            "pseudo_dir": "../pseudos",
            "conv_thr": 1.0e-8,
            "occupations": "smearing",
            "ecutwfc": 40,
            "degauss": 0.1,
            "k_points": {"parameter": "automatic",
                         "k_points": [6, 6, 6, 1, 1, 1],
                         },
            },
        scf_calculation_parameters={
            "mpi_command": "mpirun -np 4", "command_arguments": "-npool 4",
            },
        use_phonon_converged_smearing=False,
        use_phonon_converged_ecut=False,
        use_relaxed_geometry=True,
        nscf_kgrid=[6, 6, 6],
        nscf_input_variables={
            "tprnfor": True,
            "tstress": True,
            "conv_thr": 1e-10,
            "diagonalization": "cg",
            "diago_full_acc": True,
            "noncolin": False,
            "lspinorb": False,
            "nbnd": 15,
            "occupations": "smearing",
            "ecutwfc": 40,
            "degauss": 0.1,
            },
        nscf_calculation_parameters={
            "mpi_command": "mpirun -np 4", "command_arguments": "-npool 4",
            },
        band_structure_input_variables={
            "calculation": "scf",
            "pseudo_dir": "../pseudos",
            "conv_thr": 1.0e-8,
            "occupations": "smearing",
            "ecutwfc": 40,
            "degauss": 0.1,
            },
        band_structure_kpoint_path=KPATH,
        band_structure_kpoint_path_density=NKPTS,
        band_structure_calculation_parameters={
            "mpi_command": "mpirun -np 4", "command_arguments": "-npool 4",
            },
        phonons_input_variables={
            "ldisp": True,
            "tr2_ph": 1e-12,
            },
        phonons_calculation_parameters={
            "mpi_command": "mpirun -np 4", "command_arguments": "-npool 4",
            },
        phonons_qpoint_grid=[2, 2, 2],
        qpoint_path=QPATH,
        qpoint_path_density=NQPTS,
        q2r_input_variables={},
        q2r_calculation_parameters={},
        matdyn_calculation_parameters={},
        epw_input_variables={
            "amass(1)": 207.2,
            "elph": True,
            "band_plot": True,
            "etf_mem": 0,
            "kmaps": False,
            "epbwrite": True,
            "epbread": False,
            "epwwrite": True,
            "epwread": False,
            "nbndsub": 4,   # number of wannier functions to use
            # "nbndskip": 10,  # nbands under the disentanglement window
            "bands_skipped": "exclude_bands = 1-5",
            "wannierize": True,
            "num_iter": 1000,
            "wdata(1)": "dis_num_iter = 1000",
            "wdata(2)": "guiding_centres = True",
            "wdata(3)": "num_print_cycles = 100",
            "wdata(4)": "conv_window = 10",
            "dis_win_max": 21,
            "dis_win_min": -3,
            "dis_froz_min": -3,
            "dis_froz_max": 13.5,
            "proj(1)": "Pb:sp3",
            "iverbosity": 0,
            "elecselfen": False,
            "phonselfen": False,
            "fsthick": 6,  # fermi surface thickness
            "degaussw": 0.0,
            "ngaussw": -99,  # force FD distribution from the start
            "a2f": False,
            "vme": True,
            },
        epw_calculation_parameters={
               "mpi_command": "mpirun -np 4", "command_arguments": "-npool 4"
                },
        plot_calculation_parameters={"show": False},
        )
fine = 10
workflow.set_ziman(
        root_workdir="ziman",
        ziman_input_variables={
            "a2f": True, "phonselfen": True, "degaussw": 0.005, "ngaussw": -99,
            "fsthick": 0.2, "rand_nk": fine ** 3, "rand_nq": fine ** 3,
            "rand_q": True, "rand_k": True, "vme": True,
            },
        ziman_calculation_parameters={
            "mpi_command": "mpirun -np 4", "command_arguments": "-npool 4",
            },
        plot_calculation_parameters={},
        )
# workflow.set_ibte_with_thermal_expansion(
#         ibte_workdir="ibte_with_thermal_expansion",
#         ibte_input_variables={
#             "assume_metal": True,
#             "restart_step": 100,
#             "degaussw": 0.0,
#             "etf_mem": 0,
#             "fsthick": 0.1,
#             "ngaussw": -99,
#             "vme": True,
#             "nkf1": 10, "nkf2": 10, "nkf3": 10,
#             "nqf1": 10, "nqf2": 10, "nqf3": 10},
#         ibte_calculation_parameters={
#             "mpi_command": "mpirun -np 4",
#             "command_arguments": "-npool 4"},
#         plot_parameters={"elements": [[0, 0]], },
#         )

# workflow.write()
workflow.run()
