#!/bin/bash

MPIRUN="mpirun -np 4"
EXECUTABLE="/home/fgoudreault/Workspace/abinit/abinit/build/develop/2021-04-16/src/98_main/abinit"
INPUT=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_workflows/test_files/abinit_Al/gs_smearing_convergence/tsmear0.1/ngkpt4_4_4/run/ngkpt4_4_4.files
LOG=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_workflows/test_files/abinit_Al/gs_smearing_convergence/tsmear0.1/ngkpt4_4_4/ngkpt4_4_4.log
STDERR=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_workflows/test_files/abinit_Al/gs_smearing_convergence/tsmear0.1/ngkpt4_4_4/ngkpt4_4_4.stderr


$MPIRUN $EXECUTABLE < $INPUT > $LOG 2> $STDERR
