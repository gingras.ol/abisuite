import os

from .bases import BaseWritableDirectoryHandler, DirectoryContent
from .generic_directory import GenericDirectory
from .output_data_directory import OutputDataDirectory
from ..file_handlers import PBSFile


class RunDirectory(BaseWritableDirectoryHandler):
    """Class that represents the 'run' directory of a calculation.
    It contains the PBS file which is served as the batch file to
    launch calculations.

    Also contains the OutputDataDirectory.
    """
    _loggername = "RunDirectory"
    _output_data_dir_name = "output_data"

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._pbs_file = PBSFile(loglevel=self._loglevel)
        self.output_data_directory = OutputDataDirectory(
                loglevel=self._loglevel)
        self.check_pbs_file = True

    @property
    def content(self):
        # get content + add pbs file
        content = super().content
        extra = DirectoryContent(loglevel=self._loglevel)
        extra.append(self.pbs_file)
        extra.append(self.output_data_directory)
        return content + extra

    @content.deleter
    def content(self):
        BaseWritableDirectoryHandler.content.fdel(self)

    @property
    def pbs_file(self):
        # if self._pbs_file.exists:
        #     self._pbs_file.read()
        return self._pbs_file

    @pbs_file.setter
    def pbs_file(self, pbs_file):
        if not isinstance(pbs_file, PBSFile):
            raise TypeError(
                    f"Expected a 'PBSFile' object but got: {pbs_file}.")
        self._pbs_file = pbs_file

    def clear_content(self):
        super().clear_content()
        self.output_data_directory.clear_content()

    def copy_handler(self, *args, **kwargs):
        new = super().copy_handler(*args, **kwargs)
        new.pbs_file = self.pbs_file.copy_handler()
        new.pbs_file.path = self.pbs_file.path
        new.output_data_directory = self.output_data_directory.copy_handler()
        return new

    def write_content(self, **kwargs):
        """Write the content of the directory.
        """
        # first check if the pbs file is actually inside the run dir
        if not self.pbs_file.path.startswith(
                self.path) and self.check_pbs_file:
            raise ValueError(f"PBS file is not in Run directory: "
                             f"{self.pbs_file.path}.")
        super().write_content(**kwargs)

    def _add_file_to_content(self, content, path):
        # if a pbs file, add as a pbs file object. else rely on base class
        if path.endswith(PBSFile._parser_class._expected_ending):
            self.pbs_file.path = path
            self.pbs_file.read()
        else:
            super()._add_file_to_content(content, path)

    def _add_subdir_to_content(self, content, path):
        if path.endswith(self._output_data_dir_name):
            self.output_data_directory.path = path
            self.output_data_directory.read()
            return
        subdir = GenericDirectory(loglevel=self._loglevel)
        subdir.path = path
        subdir.read()
        content.append(subdir)

    def _set_subpath(self):
        super()._set_subpath()
        outpath = os.path.join(self.path, self._output_data_dir_name)
        self.output_data_directory.path = outpath
        self._reset_special_subfile_path(self.pbs_file)
