from .bases import BaseQECleaner


class QEPWCleaner(BaseQECleaner):
    """Cleaner class for a 'qe_pw' calculation.
    """
    _calctype = "qe_pw"
    _loggername = "QEPWCleaner"
