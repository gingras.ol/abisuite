import os
import pickle

import matplotlib.pyplot as plt
import numpy as np

from .plot import Plot
from ..bases import BaseUtility
from ..linux_tools import mkdir
from ..routines import is_list_like, suppress_warnings


class MultiPlot(BaseUtility):
    """Class to make a single plot with subplots.
    """
    _loggername = "MultiPlot"

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self._fig = None
        self.title = ""
        self.plot_rows = [[]]

    def __setattr__(self, attr, value):
        # if attr not in dir(self), set this attribute to all plots
        if attr in dir(self) or attr in (
                        "plot_rows", "title", "_fig", "_logger", "_loglevel"):
            super().__setattr__(attr, value)
            return
        for plot in self.plots:
            setattr(plot, attr, value)

    def __iter__(self):
        for plot in self.plots:
            yield plot

    @property
    def plots(self):
        allplots = []
        for row in self.plot_rows:
            allplots += row
        return allplots

    def add_plot(self, plot, row):
        """Add a :class:`Plot instance <abisuite.plotters.plot.Plot>` on a row.

        Parameters
        ----------
        plot : :class:`Plot <abisuite.plotters.plot.Plot>` instance
            The plot to add to the MultiPlot object.
        row : int
            The row index to add the plot.

        Raises
        ------
        TypeError: If the given argument is not a `Plot` object.
        """
        if not isinstance(plot, Plot):
            raise TypeError("Argument must be a Plot instance.")
        while row + 1 > len(self.plot_rows):
            self.plot_rows.append([])
        self.plot_rows[row].append(plot)
        plot.legend = False
        plot.colorbar = False

    def align_yaxes(self, row):
        """Align all yaxes on a given row.

        Applies the same 'ylims' property to all plots in the row. This common
        ylims is computed from the maximal ylims possible for all plots.

        Parameters
        ----------
        row: int
            The row index.

        Raises
        ------
        KeyError: If the index given does not correspond to any row.
        """
        try:
            row = self.plot_rows[row]
        except KeyError:
            raise KeyError(f"'row'='{row}' not a valid row index.")
        mins = []
        maxs = []
        for plot in row:
            if plot.ylims is None:
                # get the min/max from curves
                mins = [c.min for c in plot.curves]
                maxs = [c.max for c in plot.curves]
                if mins:
                    mins.append(0.95 * min([c.min for c in plot.curves]))
                if maxs:
                    maxs.append(1.05 * max([c.max for c in plot.curves]))
            else:
                mins.append(plot.ylims[0])
                maxs.append(plot.ylims[1])
        if not mins or not maxs:
            self._logger.warning("No mins or maxs to determine y alignment.")
            return
        for plot in row:
            plot.ylims = [min(mins), max(maxs)]

    @staticmethod
    def load_plot(path):
        """Load a plot object from a previously pickled file.

        Parameters
        ----------
        path: str
            The path to the file where the object is contained.
        """
        return Plot.load_plot(path)

    @suppress_warnings
    def plot(
            self, show=True, show_colorbar_on=None, show_legend_on=None,
            legend_outside=False):
        """Plot the multiplot.

        Parameters
        ----------
        show: bool, optional
              If True, the plos is shown.
        show_legend_on: list, optional
            The list of subplots to show the legend.
            It's a list of tuples with the coordinates being the
            ('row number', 'column number')
        show_colorbar_on: list, optional
            The list of subplots to show colorbar on. Same structure as for
            the 'show_legend_on' argument.
        legend_outside: bool, optional
            If set to True, the legend is drawn outside the plots.
        """
        self._fig = plt.figure()
        show_legend_on = self._sanitize_show_on(show_legend_on)
        show_colorbar_on = self._sanitize_show_on(show_colorbar_on)

        n_rows = len(self.plot_rows)
        n_columns = len(self.plot_rows[0])
        plot_index = list(range(1, n_rows * n_columns + 1))
        plot_index_count = 0
        for i, row in enumerate(self.plot_rows):
            for j, plot in enumerate(row):
                if plot._need_3d_projection:
                    # unused import but needed for 3D plots.
                    from mpl_toolkits.mplot3d import Axes3D  # noqa: F401
                    ax = self._fig.add_subplot(
                            n_rows, n_columns,
                            plot_index[plot_index_count],
                            projection="3d")
                else:
                    ax = self._fig.add_subplot(
                            n_rows, n_columns,
                            plot_index[plot_index_count])
                plot_index_count += 1
                plot.legend = False
                if (i, j) in show_legend_on:
                    plot.legend = True
                if (i, j) in show_colorbar_on:
                    plot.colorbar = True
                plot.plot_on_axis(self._fig, ax, show=False)
        self._fig.suptitle(self.title)
        if show:
            plt.show()

    def reset(self):
        """Resets (delete) the matplotlib attributes.
        """
        if self._fig is not None:
            plt.close(self._fig)
        del self._fig
        self._fig = None
        # resets all subplots as well
        for plot in self:
            plot.reset()

    @suppress_warnings
    def save(self, path, overwrite=False):
        """Saves the MultiPlot as a png or pdf image using the matplotlib
        savefig method.

        Parameters
        ----------
        path: str
            The path where the file will be saved. If the path ends with
            '.plot' or '.pickle' then the 'save_plot' method is used instead
            to pickle the object.
        overwrite: bool, optional
            If True and the png/pdf file already exists, it will be
            overwritten. Otherwise a FileExistsError is thrown.

        Raises
        ------
        FileExistsError:
            If a file already exists at the 'path' and overwrite is False.
        RuntimeError:
            If the 'plot' method was not called prior to the save method.
        """
        if path.endswith(".plot") or path.endswith(".pickle"):
            self.save_plot(path)
            return
        if self._fig is None:
            raise RuntimeError("Run the plot method to create "
                               "the figure before saving it.")
        dirname = os.path.dirname(path)
        if not os.path.exists(dirname):
            mkdir(dirname)
        if os.path.exists(path):
            if overwrite:
                os.remove(path)
            else:
                FileExistsError(path)
        # be careful with legend
        if any([p.legend_outside for p in self.plots]):
            # legend outside, add the legends to the savefig to make sure
            # they are saved
            legends = [p._ax.get_legend() for p in self.plots]
            # discards None values (in case not all plots have legends)
            legends = [x for x in legends if x is not None]
            self._fig.tight_layout()
            self._fig.savefig(path,
                              bbox_extra_artists=legends,
                              bbox_inches="tight")
            return
        self._fig.tight_layout()
        self._fig.savefig(path)

    def save_pickle(self, *args, **kwargs):
        """Alias method for the 'save_plot' method.
        """
        self.save_plot(*args, **kwargs)

    def save_plot(self, path, overwrite=False):
        """Saves the MultiPlot object into a file for easy reloading
        afterwards.

        This is done using the 'pickle' module. To load the saved MultiPlot
        one needs to call the staticmethod 'load_plot'.

        Parameters
        ----------
        path: str
            The path where the pickle object will be saved.
        overwrite: bool, optional
            If True and the path already exists, the existing file will be
            overwritten.
        """
        if os.path.exists(path):
            if not overwrite:
                raise FileExistsError(path)
            else:
                os.remove(path)
        # call reset in order to not have to pickle matplotlib figures
        self.reset()
        with open(path, "wb") as f:
            pickle.dump(self, f)

    def save_individual_plots(self, paths):
        if not is_list_like(paths):
            raise TypeError("Need to give list of paths in order.")
        if len(paths) != len(self.plots):
            raise ValueError("# of paths don't match # of plots.")
        for plot, path in zip(self.plots, paths):
            plot.save(path)

    def _check_rows_equal(self):
        length = len(self.plot_rows[0])
        for row in self.plot_rows[1:]:
            if len(row) != length:
                raise ValueError("Not all rows are equals!")

    def _sanitize_show_on(self, show_on):
        if show_on is None:
            return []
        arr = np.array(show_on)
        if len(arr.shape) == 1 and arr.shape[0] == 2:
            # only one coordinate
            show_on = [tuple(show_on)]
        elif len(arr.shape) != 2 or (len(arr.shape) == 2 and
                                     arr.shape[-1] != 2):
            raise ValueError(f"show_legend_on: {show_on} invalid shape")
        # else return the list of tuples
        # but make sure indices are good
        new = []
        n_rows = len(self.plot_rows)
        # we assume here that all rows are equal
        n_columns = len(self.plot_rows[0])
        for coord in show_on:
            x = coord[0]
            y = coord[1]
            while x < 0:
                x += n_rows
            while y < 0:
                y += n_columns
            if x > n_rows:
                raise ValueError("Cannot ask to show legena/colorbar"
                                 " further than n_rows!")
            if y > n_columns:
                raise ValueError("Cannot ask to show legend/colorbar"
                                 " further than n_columns!")
            new.append((x, y))
        return new
