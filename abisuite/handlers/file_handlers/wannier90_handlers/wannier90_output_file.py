from .bases import BaseWannier90FileHandler
from ..bases import BaseLogFileHandler
from ...file_parsers import Wannier90OutputParser
from ...file_structures import Wannier90OutputStructure


class Wannier90OutputFile(BaseLogFileHandler, BaseWannier90FileHandler):
    """File handler for a wannier90.x output file.
    """
    _loggername = "Wannier90OutputFile"
    _parser_class = Wannier90OutputParser
    _structure_class = Wannier90OutputStructure
