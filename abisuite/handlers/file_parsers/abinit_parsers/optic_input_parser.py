from ..bases import BaseInputParser
from ...file_structures import AbinitOpticInputStructure


class AbinitOpticInputParser(BaseInputParser):
    """Parser for an optic input file.
    """
    _loggername = "AbinitOpticInputParser"
    _structure_class = AbinitOpticInputStructure

    def _extract_data(self):
        super()._extract_data(
                ignore_lines_starting_with=["&", "/"],
                strip_newlines=True,
                strip_spaces=True,
                strip_comas=True,
                ignore_empty_lines=True,
                ignore_fortran_comments=False,
                ignore_python_comments=False,
                )

    def _extract_data_from_lines(self, lines):
        # now lines only contain variables
        # e.g.: name = value or name = 'value'
        data = {}
        for line in lines:
            split = [x.strip() for x in line.split("=")]
            if len(split) != 2:
                raise LookupError(f"# elements != 2 when splitting {line}"
                                  f" by '=' in {self.filename}")
            if "'" in split[1] or '"' in split[1]:
                # string variable
                data[split[0]] = split[1].strip("'").strip('"')
            elif "." in split[1]:
                # float variable
                data[split[0]] = float(split[1])
            elif "," in split[1]:
                # array variable
                arr = [x.strip() for x in split[1].split(",")]
                newarr = []
                for element in arr:
                    if "." in element:
                        newarr.append(float(element))
                    else:
                        newarr.append(int(element))
                data[split[0]] = newarr
            else:
                # int
                data[split[0]] = int(split[1])
        return {"input_variables": data}
