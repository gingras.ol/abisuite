from ..bases import BaseUtility
from ..plotters import Plot
from ..routines import sort_data
import numpy as np
from scipy import optimize
from scipy.interpolate import CubicSpline


def decay_exp(x, a, b, c):
    return a * np.exp(-b * x) + c


class Fit(BaseUtility):
    """Class that fits data to a given function. The fits is made to
    the function set in the fitfunction attribute. This function must
    accept parameters as a list in its first argument and the xdata
    in its second argument.
    """
    _loggername = "Fit"

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._xdata = None
        self._ydata = None
        self._fitfunc = None
        self.fit_parameters = None

    @property
    def xdata(self):
        if self._xdata is not None:
            return self._xdata
        raise ValueError("Set xdata attribute.")

    @xdata.setter
    def xdata(self, xdata):
        self._xdata = np.array(xdata)

    @property
    def ydata(self):
        if self._ydata is not None:
            return self._ydata
        raise ValueError("Set ydata attribute.")

    @ydata.setter
    def ydata(self, ydata):
        self._ydata = np.array(ydata)

    @property
    def fitfunction(self):
        if self._fitfunc is not None:
            return self._fitfunc
        raise ValueError("Set the fitfunction attribute.")

    @fitfunction.setter
    def fitfunction(self, fitfunc):
        if not callable(fitfunc):
            raise TypeError("Fitting function must be a function!")
        self._fitfunc = fitfunc

    def fit(self, initial_guess=None):
        """Do the fit using a least square optimization.

        Parameters
        ----------
        initial_guess : list
                        The list of initial guesses for the fitted
                        parameters.

        Returns
        -------
        The fitted parameters or polynomial coefficients.
        """
        # Sort xdata and ydata according to sorted xdata
        # taken from:
        # https://stackoverflow.com/a/6618543/6362595
        self.xdata, self.ydata = sort_data(self.xdata, self.ydata)
        parameters, success = optimize.curve_fit(self.fitfunction, self.xdata,
                                                 self.ydata, p0=initial_guess)
        self.fit_parameters = parameters
        return self.fit_parameters

    def plot_fit(self, xmin, xmax, n_pts, xlabel="x", ylabel="y", xunits=None,
                 yunits=None, show=True, semilogx=False):
        """Plot fit alongside with data.

        Parameters
        ----------
        xmin : float
               minimal value for the x axis.
        xmax : float
               maximal value for the x axis.
        n_pts : int
                Number of points on the x axis.
        xlabel : str, optional
                 The x axis label.
        ylabel : str, optional
                 The y axis label.
        xunits : str, optional
                 The x axis units.
        yunits : str, optional
                 The y axis units.
        semilogx : bool, optional
                   If True, the xaxis will be semilog.
        show : bool, optional
               If True, the plot is shown.

        Returns
        -------
        Plot instance of the shown figure.
        """
        if xmin == xmax:
            raise ValueError("xmin should be different than xmax!")
        if xmin > xmax:
            # invert selection
            newxmax = xmin
            xmin = xmax
            xmax = newxmax
        xpts = np.linspace(xmin, xmax, n_pts)
        ypts = self.fitfunction(xpts, *self.fit_parameters)
        plot = Plot(loglevel=self._logger.level)
        plot.show_grid = True
        # actual data
        plot.add_curve(self.xdata, self.ydata, marker="o", color="b",
                       label="Data", semilogx=semilogx)
        # fit
        plot.add_curve(xpts, ypts, tag="fit", linestyle=":", label="Fit",
                       color="r", semilogx=semilogx)
        # labels
        plot.xlabel = xlabel
        plot.ylabel = ylabel
        if xunits is not None:
            plot.xlabel += f" [{xunits}]"
        if yunits is not None:
            plot.ylabel += f" [{yunits}]"
        if show:
            plot.plot()
        return plot


class PolynomialFit(Fit):
    """Fits data as a polynomial.
    """
    _loggername = "PolynomialFit"

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        # need to revert the arguments. TODO: fix this
        self.fitfunction = lambda x, *parameters: np.polyval(list(parameters),
                                                             x)

    def fit(self, degree):
        """Fit a polynomial of a certain degree.

        Parameters
        ----------
        degree : int, the polynomial degree.
        """
        self.xdata, self.ydata = sort_data(self.xdata, self.ydata)
        self.fit_parameters = np.polyfit(self.xdata, self.ydata, degree)
        return self.fit_parameters


class ConvergenceFit(Fit):
    """Fits two data as a convergence parameter. The fit function is an
    decaying exponential:

    y(x) = Aexp(-Bx) + C

    It can plot beautifully the convergency.
    """
    _loggername = "ConvergenceFit"

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fitfunction = decay_exp
        self.ydiff = None  # difference between infinite value and data

    def fit(self, *args, take_last_val_as_offset=False, **kwargs):
        """Fit the exponential decay.

        Parameters
        ----------
        take_last_val_as_offset : bool, optional
                                  If True, the last value will be taken as
                                  the exponential offset. This will simplify
                                  fitting as we can now use a polynomial Fit
                                  instead of a nonlinear fit.
                                  If False, a nonlinear fit is done using the
                                  value of the same polynomial fit as initial
                                  guess.
        """
        # make polynomial fit taking the last value as an offset
        self.xdata, self.ydata = sort_data(self.xdata, self.ydata)
        # fit log(y - y[-1]) vs x
        logy = np.log(self.ydata - self.ydata[-1])
        polyfit = PolynomialFit(loglevel=self._logger.level)
        polyfit.xdata = self.xdata
        polyfit.ydata = logy
        polyfit_params = polyfit.fit(1)  # linear fit (1)
        # polyfit params are the coefficient starting with highest power
        fit_parameters = [np.exp(polyfit_params[-1]),
                          -polyfit_params[0],
                          self.ydata[-1]]
        if take_last_val_as_offset:
            # end here and return fit parameters
            self.fit_parameters = fit_parameters
        else:
            # compute fitted parameters with starting guess using the
            # polynomial fit
            super().fit(*args, initial_guess=self.fit_parameters, **kwargs)
        self.ydiff = np.abs(self.ydata - self.fit_parameters[-1])
        self.xdiff = self.xdata
        if take_last_val_as_offset:
            # remove last val of diff since it is 0 => giving -inf in log scale
            self.ydiff = self.ydiff[:-1]
            self.xdiff = self.xdiff[:-1]
        return self.fit_parameters

    def plot_fit(self, *args, **kwargs):
        """Plot the convergence curve using a Plot object which is returned.
        """
        show = kwargs.pop("show", True)
        ylabel = kwargs.get("ylabel", "y")
        yunits = kwargs.get("yunits", "")
        plot = super().plot_fit(*args, show=False, **kwargs)
        # add other stuff
        # change the fit label
        plot.curves["fit"].label = "Decaying exp fit"
        # infinite value
        plot.add_hline(self.fit_parameters[-1], linestyle="--", color="b",
                       label=(f"{ylabel}$(\\infty)="
                              f"{self.fit_parameters[-1]}$"  # noqa
                              f" [{yunits}]"))
        # difference with infinite value
        ydifflabel = f"$|${ylabel}-{ylabel}$(\\infty)|$ [{yunits}]"
        plot.add_curve(self.xdiff, self.ydiff, twinx=True, semilogy=True,
                       color="r", marker="o",
                       label=ydifflabel)
        plot.ylabel_twinx = ydifflabel
        if show:
            # if legend outside, it hides the twinx label
            plot.plot(legend_outside=False)
        return plot


class CubicSplineInterpolation(Fit):
    # Use Fit as base class for the xdata and other properties
    # TODO: Maybe it will be useful to rebase here
    """Does a cubic spline interpolation of data. Also implements
    some useful properties to play with the interpolation data.
    """
    _loggername = "CubicSplineInterpolation"

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._cs = None
        self._npts = None
        self.__xfit = None

    @property
    def npts(self):
        if self._npts is not None:
            return self._npts
        raise ValueError("Set npts attribute before using interpolation.")

    @npts.setter
    def npts(self, npts):
        if not isinstance(npts, int):
            raise TypeError(f"npts should be integer but received {npts}")
        self._npts = npts

    @property
    def _xfit(self):
        return self.__xfit

    @_xfit.setter
    def _xfit(self, xfit):
        self.__xfit = xfit

    def fit(self):
        self.xdata, self.ydata = sort_data(self.xdata, self.ydata)
        self._xfit = np.linspace(self.xdata[0], self.xdata[-1], self.npts)
        self._cs = CubicSpline(self.xdata, self.ydata)(self._xfit)

    def interpolate(self):
        self.fit()

    def plot_interpolation(self, xlabel="", xunits="", ylabel="", yunits="",
                           show=True):
        """Plot the interpolation data.

        Parmeters
        ---------
        xlabel : str, optional
                 The xlabel.
        ylabel : str, optional
                 The ylabel.
        xunits : str, optional
                 The xlabel units.
        yunits : str, optional
                 The ylabel units.
        show : bool, optional
               If True, the graph is shown.
        """
        plot = Plot(loglevel=self._logger.level)
        # data
        plot.add_curve(self.xdata, self.ydata, linestyle=None, marker="o",
                       tag="data", label="data")
        # interpolation
        plot.add_curve(self._xfit, self._cs, tag="interpolation")
        plot.xlabel = xlabel
        if xunits:
            plot.xlabel += f" [{xunits}]"
        plot.ylabel = ylabel
        if yunits:
            plot.ylabel += f" [{yunits}]"
        if show:
            plot.plot()
        return plot

    @property
    def interpolation(self):
        if self._cs is None:
            self._logger.error("Call interpolate() before using interpolation")
        return self._cs

    @property
    def min(self):
        return np.min(self.interpolation)

    @property
    def max(self):
        return np.max(self.interpolation)

    @property
    def wheremin(self):
        return self._xfit[np.where(self.interpolation == self.min)[0]]

    @property
    def wheremax(self):
        return self._xfit[np.where(self.interpolation == self.max)[0]]
