import tempfile
import unittest

from .bases import BaseParserTest
from ...handlers.file_parsers import GenericParser


class TestGenericParser(BaseParserTest, unittest.TestCase):
    _parser_class = GenericParser

    def setUp(self):
        self.tempfile = tempfile.NamedTemporaryFile(delete=False)
        self._example_file = self.tempfile.name
        super().setUp()

    def tearDown(self):
        del self.tempfile
