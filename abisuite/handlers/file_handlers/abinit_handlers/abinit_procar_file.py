from ..bases import BaseFileHandler
from ...file_parsers import AbinitProcarParser
from ...file_structures import AbinitProcarStructure


class AbinitProcarFile(BaseFileHandler):
    """File handler class for a PROCAR file produced by abinit.
    """
    _loggername = "AbinitProcarFile"
    _parser_class = AbinitProcarParser
    _structure_class = AbinitProcarStructure
