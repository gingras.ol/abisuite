from .bases import BaseQEInputParser
from ...file_structures import QEPW2Wannier90InputStructure


class QEPW2Wannier90InputParser(BaseQEInputParser):
    """Class that can parse a Quantum Espresso pw2wannier90.x input File.
    """
    _loggername = "QEPW2Wannier90InputParser"
    _structure_class = QEPW2Wannier90InputStructure
