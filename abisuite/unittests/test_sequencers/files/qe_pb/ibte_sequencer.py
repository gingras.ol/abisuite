from abisuite.sequencers import QEIBTESequencer


fine = 10

sequencer = QEIBTESequencer()
sequencer.ibte_workdir = "ibte_run"
sequencer.epw_interpolation_calculation = "epw_run"
sequencer.ibte_input_variables = {
        "assume_metal": True,
        "degaussw": 0.0,  # automatic smearing
        "iverbosity": 1,
        "etf_mem": 0,
        "fsthick": 0.2,
        "ngaussw": -99,  # FD
        "vme": True,
        "temps": [100],
        "restart_step": 100,
        "nstemp": 1,
        "nkf1": fine, "nkf2": fine, "nkf3": fine,
        "nqf1": fine, "nqf2": fine, "nqf3": fine,
        }
sequencer.ibte_mpi_command = "mpirun -np 4"
sequencer.ibte_command_arguments = "-npool 4"

sequencer.run()
