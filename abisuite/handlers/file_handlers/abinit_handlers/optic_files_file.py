from .bases import BaseFilesFile
from ...file_parsers import AbinitOpticFilesParser
from ...file_structures import AbinitOpticFilesStructure
from ...file_writers import AbinitOpticFilesWriter


class AbinitOpticFilesFile(BaseFilesFile):
    """File handler for an optic files file.
    """
    _loggername = "AbinitOpticFilesFile"
    _parser_class = AbinitOpticFilesParser
    _structure_class = AbinitOpticFilesStructure
    _writer_class = AbinitOpticFilesWriter
