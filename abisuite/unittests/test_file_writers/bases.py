import os
import tempfile

from ...exceptions import DevError
from ...routines import is_list_like

# TODO: make a global test with corresponding parser to make sure everything
# has been well written


class BaseWriterTest:
    """Base class for any writer test case.
    """
    maxDiff = None  # to see long diffs
    _parser_class = None
    _writer_class = None

    def setUp(self, write=True):
        if self._parser_class is None:
            raise DevError(
                    f"Need to set '_parser_class' for '{self.__class__}'.")
        if self._writer_class is None:
            raise DevError(
                    f"Need to set '_writer_class' for '{self.__class__}'.")
        self.tempdir = tempfile.TemporaryDirectory()
        self.writer = self._writer_class(loglevel=1)
        if is_list_like(self._parser_class._expected_ending):
            # take first one only
            ext = self._parser_class._expected_ending[0]
        else:
            ext = self._parser_class._expected_ending
        self.writer.path = os.path.join(
                        self.tempdir.name,
                        "test" + ext)
        if write:
            self.writer.write()

    def tearDown(self):
        self.tempdir.cleanup()
        del self.tempdir

    def test_writing(self):
        # check that file exists
        self.assertTrue(os.path.isfile(self.writer.path))
        # check that it has written everything it had to
        parser = self._parser_class.from_file(self.writer.path)
        parser.read()
        self.assertEqual(parser.structure, self.writer.structure,
                         msg=f"{parser.structure} != {self.writer.structure}")

    def test_error_prevent_overwrite(self):
        # check that trying to overwrite it raises an error
        self.writer.write(overwrite=True)  # just do it in case it was not
        # done already
        with self.assertRaises(FileExistsError):
            self.writer.write()

    def test_path(self):
        # # test that suffix is added if not set
        # self.writer.path = os.path.join(self.tempdir.name, "test")
        # suffix = self._writer_class._file_suffix
        # self.assertTrue(self.writer.path.endswith(suffix))
        # reset path
        self.writer._path = None
        # test that not setting path raises an error
        with self.assertRaises(ValueError):
            self.writer.path

    def test_workdir(self):
        # test that even if workdir not set, it is automatically set
        # from path
        self.assertEqual(self.writer.workdir, self.tempdir.name)


class BaseInputWriterTest(BaseWriterTest):
    """Base input file writer test case for a regular input file with
    input variables.
    """
    _input_vars = None

    def setUp(self):
        super().setUp(write=False)
        if self._input_vars is None:
            raise DevError(
                    f"Need to set '_input_vars' in '{self.__class__}'.")
        self.writer.input_variables = self._input_vars
        self.writer.write()
