#!/bin/bash

MPIRUN="mpirun -np 4"
EXECUTABLE="/home/fgoudreault/Workspace/q-e/bin/epw.x -npool 4"
INPUT=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_workflows/test_files/qe_Pb/ibte_with_thermal_expansion/ibte_runs/50K/50K.in
LOG=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_workflows/test_files/qe_Pb/ibte_with_thermal_expansion/ibte_runs/50K/50K.log
STDERR=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_workflows/test_files/qe_Pb/ibte_with_thermal_expansion/ibte_runs/50K/50K.stderr


$MPIRUN $EXECUTABLE -input $INPUT > $LOG 2> $STDERR
