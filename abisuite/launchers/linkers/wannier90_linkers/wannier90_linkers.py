import numpy as np
import os

from .bases import BaseWannier90Linker
from ....handlers import CalculationDirectory, MetaDataFile, QEPWLogFile


class Wannier90fromQEPWLinker(BaseWannier90Linker):
    """Linker class to link a qe_pw calculation from the pw.x script of
    Quantum Espresso to a wannier90 calculation.
    """
    _link_calctype = "qe_pw"
    _loggername = "Wannier90fromQEPWLinker"

    def _link_files_from_calculation(self, meta):
        # actually, we don't need to link any files, just parse
        # some files from
        # previous calculation and update input file.
        if "-pp" not in self.launcher.command_arguments:
            raise TypeError(
                    "Can't link a 'qe_pw' calc if '-pp' flag is "
                    "not used with the wannier90.x script.")
        # check status of parent calculation
        with CalculationDirectory.from_calculation(
                meta.calc_workdir, loglevel=self._loglevel) as pwcalc:
            with pwcalc.input_file as input_file:
                if input_file.input_variables["calculation"] != "nscf":
                    raise TypeError(
                            "Need to launch 'wannier90.x' from a nscf "
                            "calculation only.")
        # everything looks fine from here, now do the actual linking.
        # first need to get the unit cell parameters.
        ibrav = pwcalc.input_file.input_variables["ibrav"]
        positions = pwcalc.input_file.input_variables["atomic_positions"]
        positions = positions["positions"]
        kpoints = pwcalc.input_file.input_variables["k_points"]["k_points"]
        with QEPWLogFile.from_meta_data_file(meta) as log:
            axes = np.array(log.crystal_axes)
            celldm1 = log.lattice_parameters["celldm(1)"]
            # celldm3 = log.lattice_parameters["celldm(3)"]
            nbnd = log.input_variables["nbnd"]
            # fermi_energy = log.fermi_energy
        if ibrav in (2, 7):
            unit_cell = celldm1 * axes
        else:
            raise NotImplementedError(f"ibrav={ibrav.value}")
        unit_cell = unit_cell.tolist()
        self.launcher.input_variables.update(
                {"unit_cell": {
                    "parameter": "unit_cell_cart",
                    "units": "bohr",
                    "vectors": unit_cell,
                    },
                 "atomic_positions": {
                    "parameter": "atoms_frac",
                    "positions": positions,
                    },
                 "kpoints": kpoints,
                 "num_bands": nbnd,
                 # "fermi_energy": fermi_energy,
                 })


class Wannier90fromQEPW2Wannier90Linker(BaseWannier90Linker):
    """Linker class to link a qe_pw2wannier90 calculation to a wannier90
    calculation.
    """
    _link_calctype = "qe_pw2wannier90"
    _loggername = "Wannier90fromQEPW2Wannier90Linker"

    def _link_files_from_calculation(self, meta):
        # link all the files produced by this script in the rundir except
        # from the batch script ofc
        if "-pp" in self.launcher.command_arguments:
            raise ValueError(
                    "When linking a 'pw2wannier90' calculation don't"
                    " put the '-pp' flag.")
        rundir = meta.rundir
        # use copy=True cause wannier90 overwrites the files
        self.launcher.add_input_file(os.path.join(rundir, "*.amn"), copy=True)
        self.launcher.add_input_file(os.path.join(rundir, "*.eig"), copy=True)
        self.launcher.add_input_file(os.path.join(rundir, "*.mmn"), copy=True)
        self.launcher.add_input_file(os.path.join(rundir, "*.nnkp"), copy=True)
        # self.launcher.add_input_file(os.path.join(rundir, "UNK*"))
        # link the pw2wannier90's wannier90 -pp parent
        for parent in meta.parents:
            with MetaDataFile.from_calculation(
                        parent, loglevel=self._loglevel) as meta:
                if meta.calctype == "wannier90":
                    self.launcher.link_calculation(parent)
                    break
        else:
            self._logger.error(
                    "Could not find 'wannier90' pp run from this "
                    "'qe_pw2wannier90' run. Need to link manually."
                    f" ({meta.calc_workdir})")


class Wannier90fromWannier90Linker(BaseWannier90Linker):
    """Linker class to link a wannier90 calculation to a new wannier90
    calculation.
    """
    _link_calctype = "wannier90"
    _loggername = "Wannier90fromWannier90Linker"

    def _link_files_from_calculation(self, meta):
        # repeat the same input variables
        if "-pp" in self.launcher.command_arguments:
            raise ValueError("When linking a 'wannier90' calculation don't"
                             " put the '-pp' flag.")
        with CalculationDirectory.from_calculation(
                meta.calc_workdir, loglevel=self._loglevel) as calc:
            with calc.input_file as input_file:
                self.launcher.input_variables = input_file.input_variables
