from .bases import BaseAbisuiteSubScript
from .. import __SYSTEM_QUEUE__
from ..colors import Colors


class AbisuiteQueueScript(BaseAbisuiteSubScript):
    """Executes the 'queue' subcommand of the abisuite script."""
    _loggername = "AbisuiteQueueScript"

    def cancel_calculations(self):
        """Cancels calculations that are running.
        """
        print(Colors.bold_text("CANCELLING running calculations..."))
        for calculation in self.calculations:
            if self.skip_this_calculation(
                    calculation, continue_on_running=False,
                    continue_on_unstarted=False,
                    continue_on_error=False):
                # don't continue on error since errored calculations
                # sometimes are styll running
                continue
            with __SYSTEM_QUEUE__ as queue:
                print(Colors.bold_text("CANCELLING:") +
                      f"'{calculation.path}'.")
                if calculation not in queue:
                    print(Colors.bold_text("Not running."))
                else:
                    queue.cancel_job(calculation)
            self.successfull("Job cancellation.")

    def print_queue(self, tablefmt="simple"):
        """Prints the system queue.

        Parameters
        ----------
        tablefmt: str, optional
            Format for the tabulate module to print out the table.
        """
        with __SYSTEM_QUEUE__ as queue:
            queue.print(tablefmt=tablefmt)
