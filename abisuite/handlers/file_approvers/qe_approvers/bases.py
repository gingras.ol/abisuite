from ..bases import BaseInputParalApprover


class BaseQEInputParalApprover(BaseInputParalApprover):
    """Base class for QE input paral approvers.
    """

    def validate(self, *args, **kwargs):
        super().validate(*args, **kwargs)
        self._check_enough_mpi_proc_for_npool()

    def _check_enough_mpi_proc_for_npool(self):
        # QE can be called using the -npool X argument where
        # X is the number of divisions for the kpts parallelization
        # make sure X <= nb of mpi proc.
        if "-npool" not in self.command_arguments:
            # nothing to check
            return
        # split cmd to get X
        npool = self.command_arguments["-npool"]
        try:
            npool = int(npool)
        except ValueError:
            self.errors.append(f"Was expecting int for -npool arg but got:"
                               f" '{npool}'")
            return
        if self.total_ncpus < npool:
            self.errors.append(f"npool ({npool}) > nb. of proc. "
                               f" ({self.total_ncpus})")
        # cross check with mpi command
        if not self.mpi_command:
            self.errors.append(
                    "Specified parallelism scheme with 'npool' > 1 but did "
                    "not specify any mpi command!")
            return
        if self.mpi_command.strip() == "srun":
            # exception for srun as, by default, it takes all processors avail
            # (default on beluga only?!?)
            return
        if "-np" in self.mpi_command_arguments:
            total_proc = self.mpi_command_arguments["-np"]
        else:
            return
        if total_proc < npool:
            self.errors.append(f"npool ({npool}) > nb of avail proc "
                               f" ({total_proc}).")
