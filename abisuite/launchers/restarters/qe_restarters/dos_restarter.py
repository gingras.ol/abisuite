from .bases import BaseQERestarter


class QEDOSRestarter(BaseQERestarter):
    """Restarter class for a qe_dos calculation for the dos.x script
    of Quantum Espresso.
    """
    _calctype = "qe_dos"
    _loggername = "QEDOSRestarter"
