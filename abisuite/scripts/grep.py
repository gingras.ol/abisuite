import os

from .bases import BaseAbisuiteSubScript
from ..routines import is_scalar_or_str, is_list_like, is_2d_arr
from ..utils import TerminalTable


class AbisuiteGrepScript(BaseAbisuiteSubScript):
    """Executes the 'grep' subcommand for the abisuite script.
    """
    _loggername = "AbisuiteGrepScript"

    def grep(self, input_variable=None, log_file_attribute=None):
        """Greps data from calculations and prints them in a nicely
        formatted table.

        Parameters
        ----------
        input_variable: list-like, optional
            If not None, gets the given list of input variable in the input
            file to grep.
        log_file_attribute: str, optional
            If not None, gets the given 'log file attribute'. Can be anything
            defined in the corresponding structure file.
        """
        pwd = os.getcwd()
        column_names = ["Calculation(s)"]
        column_alignments = ["left"]
        if input_variable is not None:
            for input_var in input_variable:
                column_names.append(input_var + " (input file)")
                column_alignments.append("right")
        if log_file_attribute is not None:
            column_names.append(log_file_attribute + " (log)")
            column_alignments.append("right")
        table = TerminalTable(
                column_names, column_alignments=column_alignments,
                bold_column_headers=True,
                loglevel=self._loglevel)
        for calculation in self.calculations:
            with calculation as calc:
                row = [os.path.relpath(calc.path, pwd)]
                if input_variable is not None:
                    # print the input variable
                    with calc.input_file as inp:
                        for input_var in input_variable:
                            if input_var not in inp.input_variables:
                                # print that it is not defined
                                row.append("NOT DEFINED")
                            else:
                                var = inp.input_variables[input_var].value
                                row.append(self._format_input_var(var))
                if log_file_attribute is not None:
                    try:
                        with calc.log_file as log:
                            row.append(getattr(log, log_file_attribute))
                    except Exception:
                        row.append("UNAVAILABLE")
            table.add_row(row)
        table.print()

    def _format_input_var(self, value):
        # formats the input variable value such that it fits nicely into the
        # table
        if is_2d_arr(value):
            return "\n".join([str(x) for x in value])
        elif is_scalar_or_str(value) or is_list_like(value):
            return str(value)
        else:
            raise NotImplementedError(value)
