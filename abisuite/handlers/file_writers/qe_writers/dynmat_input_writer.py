from .bases import BaseQEInputWriter
from ...file_structures import QEDynmatInputStructure


class QEDynmatInputWriter(BaseQEInputWriter):
    """Class that can write a Quantum Espresso dynmat.x input file.
    """
    _loggername = "QEDynmatInputWriter"
    _structure_class = QEDynmatInputStructure

    @property
    def lines(self):
        lines = []
        self._append_var_block(lines, "input")
        return lines
