from .bases import BaseQECalculationStatusChecker


class QEDOSCalculationStatusChecker(BaseQECalculationStatusChecker):
    """Status checker for a dos.x calculation from Quantum Espresso.
    """
    _loggername = "QEDOSCalculationStatusChecker"
