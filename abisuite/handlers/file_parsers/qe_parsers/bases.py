import abc
import numpy as np

from ..bases import (
        BaseFormattedCalculationFileParser, BaseInputParser, BaseLogParser,
        )
from ....routines import decompose_line


class BaseQEParser(BaseFormattedCalculationFileParser):
    """Just a base class for Quantum Espresso data file parsers.
    """
    pass


class BaseQELogParser(BaseQEParser, BaseLogParser):
    """Base log parser for Quantum Espresso scripts.
    """
    pass


class BaseQEDOSParser(BaseQEParser, abc.ABC):
    """Base class for QE DOS parsers.
    """

    def _extract_data(self):
        # override this method as the data is simply organised
        data = np.loadtxt(self.path)
        data = self._extract_data_from_array(data)
        for k, v in data.items():
            setattr(self, k, v)

    @abc.abstractmethod
    def _extract_data_from_array(self):
        pass

    def _extract_data_from_lines(self):
        # unnecessary
        pass


class BaseQEInputParser(BaseInputParser):
    """Base class for Quantum Espresso input parsers.
    """

    def _extract_data(self):
        super()._extract_data(
                strip_newlines=True, strip_spaces=True,
                ignore_lines_starting_with=["&", "/"],
                ignore_empty_lines=True,
                )

    def _extract_data_from_lines(self, lines):
        data = {}
        next_end = 0
        for i, line in enumerate(lines):
            if i < next_end:
                continue
            if "=" in line:
                varname, value = self._extract_variable_from_line(line)
                data[varname] = value
                continue
            spec_var_data, rel_end = self._check_for_special_input_var(i,
                                                                       lines)
            data.update(spec_var_data)
            next_end = i + rel_end
        return {"input_variables": data}

    def _check_for_special_input_var(self, *args, **kwargs):
        # not necesserily needed but override if needed
        return {}, 0

    def _extract_variable_from_line(self, line):
        # a variable is defined here
        # e.g.: calculation = 'scf',
        strip_comma = line.strip(",")
        split = strip_comma.split("=")
        if "'" in line or '"' in line:
            # value is a string. we need to take into account the fact
            # that there could be a '=' inside the string as well.
            # e.g.: EPW: wdata(1) = 'bands_plot = True'
            if len(split) > 2:
                split = [split[0]] + ["=".join(split[1:])]
        split = [s.strip(" ") for s in split]
        varname = split[0]
        varvalue = split[1]
        if "'" in varvalue or '"' in varvalue:
            # value is a string
            return varname, varvalue.strip("\'\"")
        # check if it is an integer or float
        try:
            return varname, int(varvalue)
        except ValueError:
            # not an integer
            pass
        try:
            return varname, float(varvalue)
        except ValueError:  # pragma: no cover
            # not a float
            pass
        # try bool
        if varvalue.lower().strip(".") == "false":
            return varname, False
        if varvalue.lower().strip(".") == "true":
            return varname, True
        # maybe an array?
        split = varvalue.split(" ")
        if len(split) > 1:
            # value is an array. Try an array of ints
            ints = [int(x) for x in split]
            floats = [float(x) for x in split]
            for i, f in zip(ints, floats):
                if i != f:
                    # one of the elemtn was rounded by the int casting
                    # those are not ints
                    return varname, floats
            return varname, ints
        # if we are here: what type is this?????
        raise LookupError(f"Could not determine var type of {line}...")  # pragma: no cover  # noqa


class BaseBandFreqParser(BaseQEParser, abc.ABC):
    """Base class for standard dispersion band parsers.
    """
    # _expected_ending = ".freq"

    def _extract_data_from_lines(self, lines):
        # data in first line
        # &plot nbnd=   6, nks=  41 /
        # but first need to split by ','
        split0 = lines[0].split(",")
        s, i, f = decompose_line(split0[0].split("=")[-1])
        nbands = i[0]
        self.nbands = nbands
        s, i, f = decompose_line(split0[1].split("=")[-1])
        self.npts = i[0]
        # check if there is only 1 atom. if that's the case, lines only have
        # 3 data each (alternating qpts and freqs) and this is really confusing
        firsteigline = lines[2]
        s, i, f = decompose_line(firsteigline)
        if len(f) == 3:
            kpts, eigs = self._extract_data_1_atom(lines)
        else:
            kpts, eigs = self._extract_data_many_atoms(lines)
        # make some checks
        assert len(eigs) == len(kpts)
        assert len(eigs) == self.npts
        assert len(eigs[0]) == self.nbands
        return {"coordinates": kpts, "eigenvalues": eigs}

    def _extract_data_1_atom(self, lines):
        kpts, eigs = [], []
        for j, line in enumerate(lines[1:]):
            s, i, f = decompose_line(line)
            if j % 2:
                # odd line number => eigs
                eigs.append(f)
            else:
                kpts.append(f)
        return kpts, eigs

    @abc.abstractmethod
    def _extract_data_many_atoms(self, *args, **kwargs):
        pass
