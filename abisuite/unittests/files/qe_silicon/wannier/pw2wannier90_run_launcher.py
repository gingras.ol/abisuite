from abisuite import QEPW2Wannier90Launcher as Launcher

# prewannier

variables = {
        "spin_component": "none",
        "write_mmn": True,
        "write_amn": True,
        "write_unk": False,
        "reduce_unk": False,
        }


launcher = Launcher("pw2wannier90_run")
launcher.workdir = "pw2wannier90_run"
launcher.input_variables = variables
launcher.mpi_command = "mpirun -np 4"
launcher.link_calculation("../pw_run_nscf_positive_ks")
launcher.link_calculation("pp_wannier_run")
launcher.write()
launcher.run()
