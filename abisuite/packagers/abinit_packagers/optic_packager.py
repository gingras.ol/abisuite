from .bases import BaseAbinitPackager


class AbinitOpticPackager(BaseAbinitPackager):
    """Packager class for an optic run using the abinit software.
    """
    _calctype = "abinit_optic"
    _loggername = "AbinitOpticPackager"

    def keep_file(self, handler, *args, **kwargs):
        if "_OPTIC" in handler.basename:
            return True
        if "linopt" in handler.basename:
            return True
        return super().keep_file(handler)
