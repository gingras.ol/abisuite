import os
import tempfile

from ..routines_for_tests import TemporaryCalculation
from ...exceptions import DevError


class BasePackagerTest:
    """Base class for Packager objects.
    """
    _example = None
    _packager_cls = None

    def setUp(self):
        if self._example is None:
            raise DevError("Need to set '_example'.")
        self.calc = TemporaryCalculation(
                self._example, copy_on_creation=False,
                connect_to_temporary_database=True,
                new_parents=[])
        if self._packager_cls is None:
            raise DevError("Need to set '_packager_cls'.")
        self.packager = self._packager_cls(loglevel=1)
        self.dest = tempfile.TemporaryDirectory()

    def tearDown(self):
        self.calc.cleanup()
        self.dest.cleanup()

    def test_package(self, calc_example=None):
        """Test the packaging feature of the Packaging object.

        Parameters
        ----------
        calc_example: str, optional
            If None, the example calculation used is the one given as
            the class attribute `_example`. Otherwise we use the one
            given.
        """
        if calc_example is not None:
            calc = TemporaryCalculation(
                    calc_example, copy_on_creation=False,
                    connect_to_temporary_database=True,
                    new_parents=[])
        else:
            calc = self.calc
        calc.copy_calculation()
        self.packager.calculation_directory = calc.path
        prev_walk = self.packager.calculation_directory.walk(
                paths_only=False)
        prev_walk_paths = [x.path for x in prev_walk]
        self.packager.calculation_directory.connect_to_database(
                calc.temporary_database)
        self.assertFalse(self.packager.calculation_directory.is_packaged)
        self.packager.package(
                src_root=os.path.dirname(calc.path),
                dest_root=self.dest.name,
                replace_with_symlink=True)
        basename = os.path.basename(calc.path)
        # check that all files needed to copy were copied
        # and that all files copied needed to be copy (1:1 correspondance)
        for handler in prev_walk:
            if self.packager.keep_file(handler):
                # make sure this file appears in the dest
                relpath = os.path.relpath(handler.path, calc.path)
                newpath = os.path.join(self.dest.name, basename, relpath)
                self.assertTrue(os.path.isfile(newpath))
        for root, _, files in os.walk(self.dest.name):
            for filename in files:
                relpath = os.path.relpath(
                        os.path.join(root, filename),
                        os.path.join(self.dest.name, basename))
                oldpath = os.path.join(calc.path, relpath)
                self.assertIn(oldpath, prev_walk_paths)
        # check prev directory has been symlinked
        self.assertTrue(
                os.path.islink(calc.path),
                msg=(f"{os.path.dirname(calc.path)}: "
                     f"{os.listdir(os.path.dirname(calc.path))}"))
        self.assertEqual(
                os.readlink(calc.path),
                os.path.join(self.dest.name, basename))
        # check that the db entry is now
        self.assertTrue(
                calc.temporary_database.get_packaged(
                    os.path.join(self.dest.name, os.path.basename(calc.path))))
        if calc_example is not None:
            calc.cleanup()
