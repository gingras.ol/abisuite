from ..colors import Colors


class InvalidInputVariableError(Exception):
    """Custom error class for invalid input variables.
    """
    def __init__(self, input_var):
        """InputVariableError's init method.

        Parameters
        ----------
        input_var: str
            The name of the input variable.
        """
        super().__init__(
                Colors.color_text("ERROR", "bold", "red") +
                f": Invalid input variable: '{input_var}'. Not present in the "
                "input variables DB.")
