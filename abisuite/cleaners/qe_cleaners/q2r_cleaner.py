from .bases import BaseQECleaner


class QEQ2RCleaner(BaseQECleaner):
    """Cleaner class for a 'qe_q2r' calculation. For now this class cleans
    nothing!
    """
    _calctype = "qe_q2r"
    _loggername = "QEQ2RCleaner"

    def get_files_to_delete(self):
        return []
