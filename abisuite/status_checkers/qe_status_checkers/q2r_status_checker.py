from .bases import BaseQECalculationStatusChecker


class QEQ2RCalculationStatusChecker(BaseQECalculationStatusChecker):
    """Status checker for a q2r.x calculation from Quantum Espresso.
    """
    _loggername = "QEQ2RCalculationStatusChecker"
