import os
import tempfile

from .bases import BaseWriterTest
from ..bases import TestCase
from ...handlers.file_parsers import SymLinkParser
from ...handlers.file_writers import SymLinkWriter
from ...linux_tools import touch


class TestSymLinkWriter(BaseWriterTest, TestCase):
    """Test case for the symlink file writer.
    """
    _parser_class = SymLinkParser
    _writer_class = SymLinkWriter

    def setUp(self):
        super().setUp(write=False)
        # need to create file to link (source)
        self.source = os.path.join(self.tempdir.name, "test_symlink")
        touch(self.source)
        self.writer.source = self.source
        self.writer.write()

    def test_writing(self):
        super().test_writing()
        self.assertTrue(os.path.islink(self.writer.path))
        self.assertTrue(
            os.path.samefile(
                os.readlink(self.writer.path), self.source))

    def test_dirlink(self):
        # check if source is a directory instead of a file
        self.source = tempfile.TemporaryDirectory(dir=self.tempdir.name)
        self.writer.source = self.source.name
        # check that a direct write creates a symlink dir
        self.writer.write(overwrite=True)
        self.assertTrue(os.path.islink(self.writer.path))
        self.assertTrue(
            os.path.samefile(
                os.readlink(self.writer.path), self.source.name))
