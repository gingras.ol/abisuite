from ..bases import BaseStructure


class Wannier90OutputStructure(BaseStructure):
    """Structure class for a Wannier90 output file.
    """
    all_attributes = ("walltime", )
    optional_attributes = ("walltime", )
