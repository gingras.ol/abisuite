import os

import numpy as np

from .ddk_sequencer import DDKSequencer
from ..bases import SequenceCalculation
from ..individual_phonon_sequencers import (
        BaseIndividualPhononSequencer, BasePhononConvergenceSequencer,
        )
from ...constants import HARTREE_TO_CM
from ...handlers import AbinitLogFile
from ...routines import is_2d_arr, is_list_like


class AbinitIndividualPhononSequencer(
        BaseIndividualPhononSequencer, DDKSequencer):
    """Individual phonon sequencer for abinit. The full sequence will depend
    if the electric field response is needed or not or if the dynamical matrix
    is needed or not at the end.
    """
    _all_sequencer_prefixes = ("scf_", "ddk_", "phonons_")
    _loggername = "AbinitIndividualPhononSequencer"

    def __init__(self, *args, **kwargs):
        BaseIndividualPhononSequencer.__init__(self, "abinit", *args, **kwargs)
        DDKSequencer.__init__(self, *args, **kwargs)
        self._phonons_qpoint_grid = None
        # use a None init parameter to force user to tell to compute electric
        # field response or not.
        self._compute_electric_field_response = None
        # dict containing the irr at pols for each qpts that is requested
        # to split by irreps
        self._irreps = {}

    @property
    def compute_electric_field_response(self):
        if self._compute_electric_field_response is not None:
            return self._compute_electric_field_response
        raise ValueError("Need to set 'compute_electric_field_response'.")

    @compute_electric_field_response.setter
    def compute_electric_field_response(self, compute):
        self._compute_electric_field_response = compute

    @property
    def phonons_input_variables(self):
        return BaseIndividualPhononSequencer.phonons_input_variables.fget(self)

    @phonons_input_variables.setter
    def phonons_input_variables(self, invars):
        if not isinstance(invars, dict):
            raise TypeError("'phonons_input_variables' must be a dict.")
        if "rfatpol" not in invars:
            raise ValueError(
                    "Need to set 'rfatpol' in phonons input variables.")
        BaseIndividualPhononSequencer.phonons_input_variables.fset(
                self, invars)

    @property
    def phonons_qpoint_grid(self):
        if self._phonons_qpoint_grid is not None:
            return self._phonons_qpoint_grid
        raise ValueError("Need to set 'phonons_qpoint_grid'.")

    @phonons_qpoint_grid.setter
    def phonons_qpoint_grid(self, qpts):
        if not is_2d_arr(qpts):
            if len(qpts) == 3:
                # only one qpt
                qpts = [qpts]
            else:
                raise ValueError(
                        f"Expected a list of qpts coordinates but got: "
                        f"'{qpts}'."
                        )
        if self._nphonons is None:
            self.nphonons = len(qpts)
        else:
            if len(qpts) != self.nphonons:
                raise ValueError(
                        f"Expected '{self.nphonons}' qpts but got "
                        f"'{len(qpts)}' instead.")
        self._phonons_qpoint_grid = qpts

    # FG: 2021/07/16
    # TODO: rebase this with the QEPhononDispersion sequence_completed
    @property
    def sequence_completed(self):
        if not self.sequence:
            try:
                self.init_sequence()
            except (TypeError, ValueError, KeyError) as err:
                print(err)
                return False
        if not self.phonons_qpoints_split_by_irreps:
            return BaseIndividualPhononSequencer.sequence_completed.fget(self)
        # need to check that all calculations were added to sequence
        # if a qpt is not present in _irreps, return False as it was not
        # treated yet
        for iqpt in self.phonons_qpoints_split_by_irreps:
            if iqpt - 1 not in self._irreps:
                return False
        # all irreps should have been completed
        return BaseIndividualPhononSequencer.sequence_completed.fget(self)

    def init_scf_sequence(self, *args, **kwargs):
        """init the sequences before the phonons calculations.
        """
        BaseIndividualPhononSequencer.init_scf_sequence(self, *args, **kwargs)
        if self.compute_electric_field_response:
            self.init_ddk_sequence()

    def init_sequence(self):
        BaseIndividualPhononSequencer.init_sequence(self)
        # if Gamma and if electric field response, link ddk calc
        if self._nphonons is None:
            return
        for i in range(self.nphonons):
            qpt = self._get_phonons_specific_input_variables(i)["qpt"]
            if self.compute_electric_field_response and qpt == [0.0, 0.0, 0.0]:
                self.link_ddk_to_phonons()

    def link_ddk_to_phonons(self):
        ddk_calc = None
        # find ddk calc
        for calc in self.sequence:
            if self.ddk_workdir in calc.workdir:
                # there should be only one so break after
                ddk_calc = calc
                break
        else:
            raise LookupError()
        # link to phonon calcs
        if self.phonons_input_variables is None:
            # nothing to link
            return
        for calc in self.sequence:
            # only gamma phonon need to be linked
            if self.phonons_workdir in calc.workdir:
                if "qpt" not in calc.input_variables:
                    continue
                if list(calc.input_variables["qpt"]) == [0.0, 0.0, 0.0]:
                    calc.dependencies.append(ddk_calc)
                    break
        else:
            raise LookupError()

    def init_phonons_qpoint_split_by_irreps(self, iqpt, scf_calc):
        """Init phonons calculations split by irreducible perturbations for
        a given qpoint number and a given scf calculation.

        Parameters
        ----------
        iqpt: int
            The qpt index to do.
        scf_calc: SequenceCalculation object
            The SequenceCalculation object of the SCF calculation
        """
        if iqpt not in self._irreps:
            # we need first to get the at pol to do (cause some of them might
            # be redundant because of symmetry).
            invars = self.phonons_input_variables.copy()
            invars.update(
                    self._get_phonons_specific_input_variables(iqpt))
            # the magical value of paral_rf = -1 prints the irreps
            # into the log file
            invars.update({"paral_rf": -1})
            params = self.phonons_calculation_parameters.copy()
            params.update({"queuing_system": "local", "mpi_command": ""})
            calc = SequenceCalculation(
                    "abinit",
                    os.path.join(
                        self._get_phonon_workdir(iqpt, scf_calc),
                        "irreps_determination"),
                    invars, params, bypass_convergence_check=True)
            calc.load_geometry_from = scf_calc
            calc.dependencies.append(scf_calc)
            self.sequence.append(calc)
            # check if calc is finished. if yes, redo this method
            with calc.calculation_directory as calcdir:
                if not calcdir.exists:
                    return
                if calcdir.status["calculation_finished"] is True:
                    with AbinitLogFile.from_calculation(
                            calc.workdir, loglevel=self._loglevel) as log:
                        self._irreps[iqpt] = (
                                log.irreducible_perturbations)
                    self.init_phonons_qpoint_split_by_irreps(iqpt, scf_calc)
        else:
            # rfatpol = self.phonons_input_variables.get("rfatpol")
            for irrep in self._irreps[iqpt]:
                self.sequence.append(
                        self._get_phonon_calculation(
                            iqpt, scf_calc, iatpol=irrep["iatpol"],
                            idir=irrep["idir"]))

    def _get_phonon_calculation(
            self, iph, scf_calculation, iatpol=None, idir=None):
        # create a phonon calculation to the sequence based on the phonon index
        # base_workdir is the root directory
        # if iatpol is None, produce a subdir for this atomic polarization
        # idir is the corresponding idir for the perturbation
        if self.phonons_input_variables is not None:
            phvars = self.phonons_input_variables.copy()
            phvars.update(
                    self._get_phonons_specific_input_variables(
                        iph, iatpol=iatpol, idir=idir))
        else:
            phvars = None
        workdir = self._get_phonon_workdir(
                iph, scf_calculation, iatpol=iatpol, idir=idir)
        phon_calc = SequenceCalculation(
                        "abinit", workdir, phvars,
                        self.phonons_calculation_parameters,
                        loglevel=self._loglevel)
        phon_calc.dependencies.append(scf_calculation)  # gs calculation
        phon_calc.load_geometry_from = scf_calculation
        phon_calc.load_kpts_from = scf_calculation
        return phon_calc

    def _get_phonons_specific_input_variables(
            self, iph, iatpol=None, idir=None):
        # returns specific input variable for phonon number 'iph'.
        # if iatpol is not None, do calculation only for this iatpol
        # idir is for the direction of the irr perturbation
        qpt = self._get_phonon_qpt_coordinates(iph)
        if not is_list_like(qpt):
            qpt = list(qpt)
        spec = {"kptopt": 3, "rfdir": [1, 1, 1], "rfphon": 1, "irdwfk": 1,
                "irdddk": 0, "qpt": qpt, "nqpt": 1}
        if iatpol is not None:
            spec.update({"rfatpol": [iatpol, iatpol]})
        if idir is not None:
            # idir \in [1, 2, 3] => need to translate to [x, y, z] with
            # x, y, z \in [0, 1]
            rfdir = [0, 0, 0]
            rfdir[idir - 1] = 1
            spec.update({"rfdir": rfdir})
        if qpt == [0.0, 0.0, 0.0]:
            # read ddk if electric field response
            if self.compute_electric_field_response:
                spec["irdddk"] = 1
                # compute electric field response only for gamma
                spec["rfelfd"] = 3
            # set kptopt to 2 instead for Gamma because we can use TR syms
            spec["kptopt"] = 2
        try:
            spec.update(self.phonons_specific_input_variables[iph])
        except ValueError:
            # don't mind if phonons specific input vars are not set
            pass
        return spec

    def _get_phonon_qpt_coordinates(self, iph):
        return self.phonons_qpoint_grid[iph]

    def _get_phonon_workdir(self, *args, iatpol=None, idir=None, **kwargs):
        # if iatpol is not None, add subdir for this iatpol
        workdir = BaseIndividualPhononSequencer._get_phonon_workdir(
                self, *args, **kwargs)
        if iatpol is not None:
            basename = os.path.basename(workdir)
            workdir = os.path.join(workdir, f"{basename}_atom_pol{iatpol}")
        if idir is not None:
            workdir += f"_idir{idir}"
        return workdir


class AbinitPhononConvergenceSequencer(
        BasePhononConvergenceSequencer,
        AbinitIndividualPhononSequencer,
        DDKSequencer):
    """Base class for abinit phonon convergence sequencers.
    """
    _all_sequencer_prefixes = ("scf_", "ddk_", "phonons_", "plot_", )

    def __init__(self, *args, **kwargs):
        BasePhononConvergenceSequencer.__init__(
                self, "abinit", *args, **kwargs)
        AbinitIndividualPhononSequencer.__init__(
                self, *args, **kwargs)
        DDKSequencer.__init__(self, *args, **kwargs)
        self._nphonons = 1

    @property
    def phonons_specific_input_variables(self):
        aips = AbinitIndividualPhononSequencer
        return aips.phonons_specific_input_variables.fget(self)

    @phonons_specific_input_variables.setter
    def phonons_specific_input_variables(self, spec):
        aips = AbinitIndividualPhononSequencer
        aips.phonons_specific_input_variables.fset(self, spec)

    @property
    def phonons_qpoint_grid(self):
        return [self.phonons_qpt]

    def init_sequence(self):
        BasePhononConvergenceSequencer.init_sequence(self)
        # if we need to compute electric field response, we need to add
        # ddk calculations for gamma phonons conly
        if list(self.phonons_qpt) != [0.0, 0.0, 0.0]:
            return
        if not self.compute_electric_field_response:
            return
        self.init_ddk_sequence()

    def init_ddk_sequence(self):
        # adds ddk calculations
        n_scf_calcs = len(self._parameters_to_converge)
        scf_calcs = self.sequence[:n_scf_calcs]
        # phonon calcs should have been prepared already
        ph_calcs = self.sequence[n_scf_calcs:]
        ddk_calcs = []
        for icalc, (scf_calc, ph_calc, param) in enumerate(zip(
                scf_calcs, ph_calcs, self._parameters_to_converge)):
            # arg of 0 is because we only do 1 phonon for each scf calc
            workdir = os.path.join(
                    self.ddk_workdir,
                    self._parameter_to_converge_input_variable_name,
                    (self._parameter_to_converge_input_variable_name + "_" +
                     self._convert_param_to_str(param, icalc)))
            if self.ddk_input_variables is None:
                ddk_input_vars = {}
            else:
                ddk_input_vars = self.ddk_input_variables.copy()
            ddk_input_vars.update(
                    {"qpt": self.phonons_qpt,
                     self._parameter_to_converge_input_variable_name: param}
                    )
            seq = SequenceCalculation(
                    "abinit", workdir, ddk_input_vars,
                    self.ddk_calculation_parameters, loglevel=self._loglevel)
            seq.dependencies.append(scf_calc)
            seq.load_geometry_from = scf_calc
            seq.load_kpts_from = scf_calc
            self.sequence.append(seq)
            ddk_calcs.append(seq)
            # then add ddk calc as dependency to phonon calc
            ph_calc.dependencies.append(seq)
        self.sequence += ddk_calcs

    def _get_phonon_frequencies_from_log(self, log):
        eigs = log.dtsets[0]["phonon_frequencies"]["eigenvalues"]
        return np.array(eigs) * HARTREE_TO_CM
