from ..bases import BaseStructure


class AbinitProjectedDOSStructure(BaseStructure):
    """Structure class for a projected _DOS file produced by Abinit.
    """
    all_attributes = (
            "energies", "fermi_energy", "projected_dos",
            "projected_integrated_dos", )
