#!/bin/bash

MPIRUN="mpirun -np 4"
EXECUTABLE="/home/fgoudreault/Workspace/abinit/abinit/build/develop/2021-04-16/src/98_main/abinit"
INPUT=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_sequencers/files/abinit_optic_GaAs/nscf_run/run/nscf_run.files
LOG=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_sequencers/files/abinit_optic_GaAs/nscf_run/nscf_run.log
STDERR=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_sequencers/files/abinit_optic_GaAs/nscf_run/nscf_run.stderr


$MPIRUN $EXECUTABLE < $INPUT > $LOG 2> $STDERR
