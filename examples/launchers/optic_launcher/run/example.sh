#!/bin/bash


MPIRUN=""
EXECUTABLE=/home/fgoudreault/Workspace/abinit/build/master/vanilla/src/98_main/optic
INPUT=/home/fgoudreault/Workspace/abilaunch/examples/launchers/optic_launcher/run/example.files
LOG=/home/fgoudreault/Workspace/abilaunch/examples/launchers/optic_launcher/example.log
STDERR=/home/fgoudreault/Workspace/abilaunch/examples/launchers/optic_launcher/example.stderr

$MPIRUN $EXECUTABLE < $INPUT > $LOG 2> $STDERR
