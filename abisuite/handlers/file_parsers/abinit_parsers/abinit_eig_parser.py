import numpy as np

from .bases import BaseAbinitNCParser
from .subparsers import EIGSubParser
from ...file_structures import AbinitEIGStructure


class AbinitEIGParser(BaseAbinitNCParser):
    """Parser class for an abinit EIG file.
    """
    _expected_ending = "_EIG"
    _loggername = "AbinitEIGParser"
    _structure_class = AbinitEIGStructure

    def _extract_data_from_lines(self, lines):
        # use EIGSubParser to get eigenvalues
        subparser = EIGSubParser(lines, loglevel=self._loglevel)
        return subparser.data

    def _extract_data_from_netcdf_dataset(self, dtset):
        return {
                "eigenvalues": np.array(dtset.variables["Eigenvalues"])[0],
                "k_points": np.array(dtset.variables["Kptns"]),
                }
