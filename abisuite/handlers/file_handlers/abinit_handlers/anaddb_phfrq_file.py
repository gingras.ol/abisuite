from ..bases import BaseFileHandler
from ...file_parsers import AbinitAnaddbPhfrqParser
from ...file_structures import AbinitAnaddbPhfrqStructure


class AbinitAnaddbPhfrqFile(BaseFileHandler):
    """File handler class for an anaddb PHFRQ file.
    """
    _loggername = "AbinitAnaddbPhfrqFile"
    _parser_class = AbinitAnaddbPhfrqParser
    _structure_class = AbinitAnaddbPhfrqStructure
