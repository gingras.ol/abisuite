from ..bases import PathConvertibleFileHandler


class BaseFilesFile(PathConvertibleFileHandler):
    """Base class for Files file handlers.
    """
    pass
