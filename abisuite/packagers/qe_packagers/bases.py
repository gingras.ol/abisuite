import abc

from ..bases import BasePackager


class BaseQEPackager(BasePackager, abc.ABC):
    """Base class for any Quantum Espresso calculation packager.
    """

    @abc.abstractmethod
    def keep_file(self, handler, keep_wavefunctions=False, keep_density=False):
        if handler.basename == "filkf.txt":
            return True
        if handler.basename == "filqf.txt":
            return True
        if ".wfc" in handler.basename and keep_wavefunctions:
            return True
        if handler.basename.startswith("wfc") and (
                handler.basename.endswith(".dat") and keep_wavefunctions):
            return True
        if handler.basename == "charge-density.dat" and keep_density:
            return True
        return super().keep_file(handler)
