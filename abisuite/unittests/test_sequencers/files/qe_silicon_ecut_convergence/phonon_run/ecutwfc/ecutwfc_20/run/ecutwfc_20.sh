#!/bin/bash

MPIRUN="mpirun -np 4"
EXECUTABLE="/home/fgoudreault/Workspace/q-e/bin/ph.x -npool 4"
INPUT=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_sequencers/files/qe_silicon_ecut_convergence/phonon_run/ecutwfc/ecutwfc_20/ecutwfc_20.in
LOG=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_sequencers/files/qe_silicon_ecut_convergence/phonon_run/ecutwfc/ecutwfc_20/ecutwfc_20.log
STDERR=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_sequencers/files/qe_silicon_ecut_convergence/phonon_run/ecutwfc/ecutwfc_20/ecutwfc_20.stderr


$MPIRUN $EXECUTABLE -input $INPUT > $LOG 2> $STDERR
