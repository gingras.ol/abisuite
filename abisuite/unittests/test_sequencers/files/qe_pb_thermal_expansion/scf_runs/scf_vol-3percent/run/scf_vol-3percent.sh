#!/bin/bash

MPIRUN="mpirun -np 4"
EXECUTABLE="/home/fgoudreault/Workspace/q-e/bin/pw.x -npool 4"
INPUT=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_sequencers/files/qe_pb_thermal_expansion/scf_runs/scf_vol-3percent/scf_vol-3percent.in
LOG=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_sequencers/files/qe_pb_thermal_expansion/scf_runs/scf_vol-3percent/scf_vol-3percent.log
STDERR=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_sequencers/files/qe_pb_thermal_expansion/scf_runs/scf_vol-3percent/scf_vol-3percent.stderr


$MPIRUN $EXECUTABLE -input $INPUT > $LOG 2> $STDERR
