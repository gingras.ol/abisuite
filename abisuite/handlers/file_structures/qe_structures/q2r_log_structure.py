from .bases import BaseQELogStructure


class QEQ2RLogStructure(BaseQELogStructure):
    """Structure for a q2r.x log file from Quantum Espresso.
    """
    all_attributes = ("timing", )
