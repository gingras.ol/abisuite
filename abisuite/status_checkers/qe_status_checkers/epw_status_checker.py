from .bases import BaseQECalculationStatusChecker


class QEEPWCalculationStatusChecker(BaseQECalculationStatusChecker):
    """Status checker for a epw.x calculation from Quantum Espresso.
    """
    _calculation_completed_keyword_trigger = "Total program execution"
    _loggername = "QEEPWCalculationStatusChecker"
