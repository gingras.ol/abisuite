from .bases import BaseQELogStructure


class QEPPLogStructure(BaseQELogStructure):
    """Structure for a pp.x log file from Quantum Espresso.
    """
    all_attributes = tuple([])
