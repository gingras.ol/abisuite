import os

from ..bases import BaseFileHandler


class BaseWannier90FileHandler(BaseFileHandler):
    """Base file handler class for a wannier90 file.
    """

    @classmethod
    def _get_file_from_meta_data_file(cls, meta):
        # add a special case for EPW calculations which can produce (and
        # usually will) wannier90 files.
        with meta:
            if meta.calctype == "wannier90":
                return super()._get_file_from_meta_data_file(meta)
            elif meta.calctype == "qe_epw":
                return cls._get_file_from_meta_data_file_epw(meta)
            else:
                raise ValueError(f"Unsupported calctype: '{meta.calctype}'.")

    @classmethod
    def _get_file_from_meta_data_file_epw(cls, meta):
        # get the file produced by a EPW calculation
        # By default it is located in the rundir/jobname + expected ending
        with meta:
            return os.path.join(
                    meta.rundir, meta.jobname +
                    cls._parser_class._expected_ending)
