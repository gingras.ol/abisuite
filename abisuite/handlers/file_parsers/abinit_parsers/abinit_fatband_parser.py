import os

import numpy as np

from ..bases import BaseCalculationFileParser
from ...file_structures import AbinitFatbandStructure


class AbinitFatbandParser(BaseCalculationFileParser):
    """Parser class for an abinit _FATBANDS_* file.
    """
    _expected_ending = ""
    _loggername = "FatbandParser"
    _structure_class = AbinitFatbandStructure

    def _extract_data(self):
        data = np.loadtxt(self.path, comments=["#", "@", "&"])
        # count number of bands and rechape data
        with open(self.path, "r") as f:
            lines = f.readlines()
        nbands = 0
        for line in lines:
            if "# BAND" in line:
                nbands += 1
        data = np.reshape(
                data, (nbands, data.shape[0] // nbands, data.shape[1]))
        nkpts = len(data[0])
        characters = data[:, :, 2]
        eigs = data[:, :, 1]
        self._set_attributes_from_data(
                {"nbands": nbands, "nkpts": nkpts, "characters": characters,
                 "eigenvalues": eigs})

    @classmethod
    def _filepath_from_meta(cls, meta, *args, **kwargs):
        # FATBANDS files are located in the output data directory
        # there might be a lot of fatbands file.
        files = {}
        for path in os.listdir(meta.output_data_dir):
            if "_FATBANDS_" not in path:
                continue
            key = path.split("_FATBANDS_")[-1]
            files[key] = os.path.join(meta.output_data_dir, path)
        if not files:
            raise FileNotFoundError("No FATBANDS files found.")
        return files
