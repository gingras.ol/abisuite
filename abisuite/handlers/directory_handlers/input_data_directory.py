from .bases import BaseWritableDirectoryHandler


class InputDataDirectory(BaseWritableDirectoryHandler):
    """Class that represents an input data directory. It handles the creation
    or linking of input files.
    """
    _loggername = "InputDataDirectory"
