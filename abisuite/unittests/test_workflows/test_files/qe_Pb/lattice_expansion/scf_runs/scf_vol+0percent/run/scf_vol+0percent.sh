#!/bin/bash

MPIRUN="mpirun -np 4"
EXECUTABLE="/home/fgoudreault/Workspace/q-e/bin/pw.x -npool 4"
INPUT=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_workflows/test_files/qe_Pb/lattice_expansion/scf_runs/scf_vol+0percent/scf_vol+0percent.in
LOG=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_workflows/test_files/qe_Pb/lattice_expansion/scf_runs/scf_vol+0percent/scf_vol+0percent.log
STDERR=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_workflows/test_files/qe_Pb/lattice_expansion/scf_runs/scf_vol+0percent/scf_vol+0percent.stderr


$MPIRUN $EXECUTABLE -input $INPUT > $LOG 2> $STDERR
