from .bases import BaseStructure


class StderrStructure(BaseStructure):
    """Structure class for a 'stderr' file.
    """
    all_attributes = ("nlines", )
