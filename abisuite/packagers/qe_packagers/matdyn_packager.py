from .bases import BaseQEPackager


class QEMatdynPackager(BaseQEPackager):
    """Packager class for a matdyn.x calculation from the Quantum Espresso
    suite.
    """
    _calctype = "qe_matdyn"
    _loggername = "QEMatdynPackager"

    def keep_file(self, handler, *args, **kwargs):
        if handler.basename.endswith(".eig"):
            return True
        if handler.basename.endswith(".freq"):
            return True
        return super().keep_file(handler, *args, **kwargs)
