import abc

from .exceptions import InputFileError
from ..bases import BaseStructuredObject
from ..file_structures import MPIStructure
from ...exceptions import DevError


class BaseApprover(BaseStructuredObject, abc.ABC):
    """Base class for approvers.
    """
    _exception = None

    def __init__(self, *args, **kwargs):
        """Base approver init method.
        """
        if self._exception is None:
            raise DevError(
                    "Need to set '_exception' cls attr for {self.__class__}'.")
        super().__init__(*args, **kwargs)
        self._has_been_validated = False
        self.errors = []

    @property
    def is_valid(self):
        """Returns True if variables are valid (i.e.: if no errors).
        """
        if not self._has_been_validated:
            raise self._exception("Validate approver before checking if valid")
        n_err = len(self.errors)
        if n_err:
            self._logger.error(f"Found {n_err} errors.")
            return False
        self._logger.debug("No errors found: file is valid.")
        return True

    @abc.abstractmethod
    def validate(self):
        """Abstract method to validate variables.
        """
        self._logger.debug("Validating.")
        self._has_been_validated = True

    def raise_errors(self):
        """Raise a custom exception based on the approver if there are errors.
        """
        if self.is_valid:
            self._logger.debug("No error to raise.")
            return
        raise self._exception(
                f"{self._loggername} has found errors:" + "\n\n"
                + "\n".join(self.errors))


class BaseCrossApprover(BaseApprover):
    """Base class for input file approvers with cross checks with other
    instances.
    """
    _exception = InputFileError  # since the errors come from bad input vars
    _input_approver_class = None

    @classmethod
    def from_approver(cls, approver, *args, **kwargs):
        if cls._input_approver_class is None:
            raise DevError(f"{cls} has undefined input approver class")
        if not isinstance(approver, cls._input_approver_class):
            raise TypeError(f"Expected {cls._input_approver_class} approver "
                            f"instance but got: {approver}")
        instance = cls(*args, **kwargs)
        instance.structure = approver.structure.copy()
        return instance


class BaseInputParalApprover(BaseCrossApprover, abc.ABC):
    """Base class for input file approver and comparison with mpi settings.
    """
    # put import statement here to prevent import loops
    from .mpi_approver import MPIApprover  # noqa
    _mpi_approver_class = MPIApprover
    _mpi_structure_class = MPIStructure

    def __init__(self, *args, **kwargs):
        """Input paral approver. It takes all variables (mpi settings +
        input variables) as a single dictionary. If you have parameters
        separated, consider using MPIApprover and InputApprover separately
        and then call this class' classmethod cls.from_approvers.

        Parameters
        ----------
        variables : dict
                    The dictionary that contains both the mpi settings and the
                    abinit input variables.
        """
        super().__init__(*args, **kwargs)
        self.mpi_structure = MPIStructure()

    def _extract_nodes(self, nodes):
        if nodes is None:
            raise ValueError(f"Was expecting a str or int but got {nodes}")
        # return the number of nodes
        if isinstance(nodes, str):
            if ":" in nodes:
                # nodes = 3:m48G  for example
                return int(nodes.split(":")[0])
            else:
                return int(nodes)
        return nodes

    @classmethod
    def from_approvers(cls, input_approver, mpi_approver, *args, **kwargs):
        """Init method from an input file approver and a mpi approver.

        Parameters
        ----------
        input_approver : InputApprover instance.
        mpi_approver : MPIApprover instance.
        kwargs : other kwargs are passed to the constructor.
        """
        # call ancestor's class method
        instance = cls.from_approver(input_approver, *args,
                                     **kwargs)
        if not isinstance(mpi_approver, cls._mpi_approver_class):
            raise TypeError("Second argument should be a MPIApprover"
                            " instance.")
        instance.mpi_structure = mpi_approver.structure.copy()
        return instance


class BaseInputApprover(BaseApprover):
    """Base class for input file approvers.
    """
    _exception = InputFileError
    _variables_db = None
    # _pseudos_cross_approver = None

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._pseudo_files = None

    @property
    def pseudo_files(self):
        if self._pseudo_files is not None:
            return self._pseudo_files
        # import here to prevent import loops
        from .. import PseudoFile
        pseudo_files = []
        if self.pseudos is None:
            raise ValueError(
                    "Tried to access pseudo_files but no pseudos defined.")
        for ps in self.pseudos:
            if not isinstance(ps, PseudoFile):
                ps = PseudoFile.from_file(ps, loglevel=self._loglevel)
                ps.read()
                pseudo_files.append(ps)
            else:
                if not ps.has_been_read:
                    ps.read()
                pseudo_files.append(ps)
        self._pseudo_files = pseudo_files
        return self.pseudo_files

    def validate(self):
        super().validate()
        # validate consistency of input variables from the input variables dict
        self.input_variables.validate()
        self._check_mandatory_variables()
        if self.pseudos is not None:
            self._logger.debug("Pseudos given in input => cross check input"
                               " vars with pseudos.")
            self._validate_pseudos()
            # self._cross_check_with_pseudos()

    def _validate_pseudos(self):
        # validate only pseudos
        for pseudo in self.pseudo_files:
            # should be pseudo file handlers here
            if not pseudo.has_been_read:
                try:
                    pseudo.read()
                except FileNotFoundError:
                    self.errors.append(f"Pseudo not found: {pseudo}")

    # def _cross_check_with_pseudos(self):
    #     # cross check input variables with pseudo potentials
    #     if self._pseudos_cross_approver is None:
    #         raise DevError("Specify pseudos cross approver attribute.")
    #     lvl = self._loglevel
    #     approver = self._pseudos_cross_approver.from_approver(self,
    #                                                           loglevel=lvl)
    #     approver.validate()
    #     if not approver.is_valid:
    #         approver.raise_errors()

    def _check_mandatory_variables(self):
        # get all variables
        # Put the import here to prevent circular import loops
        allvars = self._variables_db
        for var, params in allvars.items():
            if params["mandatory"]:
                if "*" in var:
                    # assert at least one of this var is present
                    splitvar = var.split("*")
                    for name in self.input_variables:
                        if name.startswith(
                                splitvar[0]) and name.endswith(splitvar[-1]):
                            break
                    else:
                        self.errors.append(
                                f"Missing mandatory variable: {var}")
                    continue
                if var not in self.input_variables:
                    self.errors.append(f"Missing mandatory variable: {var}")
