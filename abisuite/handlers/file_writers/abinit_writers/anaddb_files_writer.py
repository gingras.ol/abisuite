from .bases import BaseFilesWriter
from ...file_structures import AbinitAnaddbFilesStructure


class AbinitAnaddbFilesWriter(BaseFilesWriter):
    """Class that can write an anaddb files file."""
    _loggername = "AbinitAnaddbFilesWriter"
    _structure_class = AbinitAnaddbFilesStructure

    @property
    def lines(self):
        lines = super().lines
        # third is ddb file path
        lines.append(self.ddb_file_path + "\n")
        # fourth is phonon band structure file path
        lines.append(self.band_structure_file_path + "\n")
        # fifth gkk file path
        lines.append(self.gkk_file_path + "\n")
        # sixth is the eph file prefix
        lines.append(self.eph_data_prefix + "\n")
        # seventh is the ddk file path
        lines.append(self.ddk_file_path + "\n")
        return lines
