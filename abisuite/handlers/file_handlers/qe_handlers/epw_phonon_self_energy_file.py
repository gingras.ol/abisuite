from ..bases import BaseFileHandler
from ...file_parsers import QEEPWPhononSelfEnergyParser
from ...file_structures import QEEPWPhononSelfEnergyStructure


class QEEPWPhononSelfEnergyFile(BaseFileHandler):
    """Class that represents a phonon self energy file produced by a
    epw.x calculation from Quantum Espresso.
    """
    _loggername = "QEEPWPhononSelfEnergyFile"
    _parser_class = QEEPWPhononSelfEnergyParser
    _structure_class = QEEPWPhononSelfEnergyStructure
