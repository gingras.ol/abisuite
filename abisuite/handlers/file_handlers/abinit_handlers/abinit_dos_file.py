from ..bases import BaseFileHandler
from ...file_parsers import AbinitDOSParser
from ...file_structures import AbinitDOSStructure


class AbinitDOSFile(BaseFileHandler):
    """File handler class for a _DOS file produced by the abinit software.
    """
    _loggername = "AbinitDOSFile"
    _parser_class = AbinitDOSParser
    _structure_class = AbinitDOSStructure
