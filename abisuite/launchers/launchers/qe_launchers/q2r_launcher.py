import os

from .bases import BaseQELauncher_for_fildyn, BasePostPHLauncher
from ....handlers import QEQ2RInputFile


class QEQ2RLauncher(BaseQELauncher_for_fildyn, BasePostPHLauncher):
    """Launcher class for a Quantum Espresso calculation using
    the q2r.x script.
    """
    _loggername = "QEQ2RLauncher"
    _input_file_handler_class = QEQ2RInputFile
    _calctype = "qe_q2r"

    def _preprocess_inputs(self, inputs):
        inputs = BaseQELauncher_for_fildyn._preprocess_inputs(self, inputs)
        flfrc = os.path.join(self.output_data_dir.path, self.jobname + ".fc")
        inputs = self._overwrite_input_var(inputs, "flfrc", flfrc)
        return inputs
