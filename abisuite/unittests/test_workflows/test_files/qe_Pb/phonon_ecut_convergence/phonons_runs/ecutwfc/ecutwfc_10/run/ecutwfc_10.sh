#!/bin/bash

MPIRUN="mpirun -np 4"
EXECUTABLE="/home/fgoudreault/Workspace/q-e/bin/ph.x -npool 4"
INPUT=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_workflows/test_files/qe_Pb/phonon_ecut_convergence/phonons_runs/ecutwfc/ecutwfc_10/ecutwfc_10.in
LOG=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_workflows/test_files/qe_Pb/phonon_ecut_convergence/phonons_runs/ecutwfc/ecutwfc_10/ecutwfc_10.log
STDERR=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_workflows/test_files/qe_Pb/phonon_ecut_convergence/phonons_runs/ecutwfc/ecutwfc_10/ecutwfc_10.stderr


$MPIRUN $EXECUTABLE -input $INPUT > $LOG 2> $STDERR
