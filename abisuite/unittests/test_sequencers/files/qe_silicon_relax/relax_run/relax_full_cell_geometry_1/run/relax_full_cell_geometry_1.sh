#!/bin/bash

MPIRUN="mpirun -np 4"
EXECUTABLE="/home/fgoudreault/Workspace/q-e/bin/pw.x -npool 4"
INPUT=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_sequencers/files/qe_silicon_relax/relax_run/relax_full_cell_geometry_1/relax_full_cell_geometry_1.in
LOG=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_sequencers/files/qe_silicon_relax/relax_run/relax_full_cell_geometry_1/relax_full_cell_geometry_1.log
STDERR=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_sequencers/files/qe_silicon_relax/relax_run/relax_full_cell_geometry_1/relax_full_cell_geometry_1.stderr


$MPIRUN $EXECUTABLE -input $INPUT > $LOG 2> $STDERR
