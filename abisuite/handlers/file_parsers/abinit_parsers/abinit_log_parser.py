import os

from .subparsers import (
        DtsetSubParser, HeaderParser,
        FooterParser,
        )
from .subparsers.abinit_log_subparsers import (
        AbinitLogIrreduciblePerturbationsSubParser,
        AbinitLogOccSubParser,
        AbinitLogWalltimeSubParser,
        )
from ..bases import BaseParserWithSubParsers
from ...file_structures import AbinitLogStructure


class AbinitLogParser(BaseParserWithSubParsers):
    """An ABINIT log file parser.
    """
    _expected_ending = ".log"
    _loggername = "AbinitLogParser"
    _structure_class = AbinitLogStructure
    _subparsers = (
            AbinitLogIrreduciblePerturbationsSubParser,
            AbinitLogOccSubParser, AbinitLogWalltimeSubParser,
            DtsetSubParser, HeaderParser, FooterParser,
            )

    # same as abinit output file
    @classmethod
    def _filepath_from_meta(cls, meta):
        # usually, output file in workdir
        return os.path.join(meta.calc_workdir,
                            meta.jobname + cls._expected_ending)
