from .subparsers.mrgddb_log_subparsers import AbinitMrgddbLogWalltimeSubParser
from ..bases import BaseLogParser
from ...file_structures import AbinitMrgddbLogStructure


class AbinitMrgddbLogParser(BaseLogParser):
    """An mrgddb log file parser that gets data from an output file.

    Parameters
    ----------
    filepath : str
               The path to the output file.
    """
    _loggername = "AbinitMrgddbLogParser"
    _structure_class = AbinitMrgddbLogStructure
    _subparsers = (AbinitMrgddbLogWalltimeSubParser, )
