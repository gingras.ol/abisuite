from .bases import BaseAbinitPackager


class AbinitPackager(BaseAbinitPackager):
    """Packager class for an abinit calculation.
    """
    _calctype = "abinit"
    _loggername = "AbinitPackager"

    def keep_file(
            self, handler, keep_density=False, keep_wavefunctions=False):
        # keep GSR file (for band structures)
        if "_GSR" in handler.basename:
            return True
        if "PROCAR" in handler.basename:
            return True
        if "_EIG" in handler.basename:
            return True
        if "_DOS" in handler.basename:
            return True
        if "_WFK" in handler.basename and keep_wavefunctions:
            return True
        if "_DEN" in handler.basename and keep_density:
            return True
        if "_1WF" in handler.basename and keep_wavefunctions:
            return True
        return super().keep_file(handler)
