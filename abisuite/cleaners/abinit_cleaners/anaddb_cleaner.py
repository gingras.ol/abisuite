from ..bases import BaseCleaner


class AbinitAnaddbCleaner(BaseCleaner):
    """Cleaner class for an anaddb calculation.
    """
    _calctype = "abinit_anaddb"
    _loggername = "AbinitAnaddbCleaner"

    def delete_this_file(self, handler):
        return super().delete_this_file(handler)
