from abilaunch.writers import AbinitFilesFileWriter


path = "example.files"

writer = AbinitFilesFileWriter()
writer.path = path
writer.input_file_path = "example.in"
writer.output_file_path = "example.out"
writer.input_data_prefix = "idat_example"
writer.output_data_prefix = "odat_example"
writer.tmp_data_prefix = "tmp_example"
writer.input_data_dir = "."
writer.output_data_dir = "."
writer.tmp_data_dir = "."
writer.pseudos = ["pseudo1", "pseudo2"]
writer.write(overwrite=True)
