from .bases import BaseAbisuiteSubScript
from ..colors import Colors


class AbisuiteRmScript(BaseAbisuiteSubScript):
    """Executes the 'rm' part of the abisuite script.
    """
    _loggername = "AbisuiteRmScript"

    def rm(self, only_errored=False, dry=False, force=False):
        """Deletes the calculations stored in the 'calculations' attribute.

        If calculation is running, it is deleted from the queue also.

        Parameters
        ----------
        dry: bool, optional
            If True, only what will be deleted will be printed on screen
            but nothing is deleted.
        force: bool, optional
            If True, whatever happens, the script will try to delete the
            calculation"
        only_errored: bool, optional
            If True, only calculations that errored are removed.
        """
        if dry:
            print(Colors.color_text(
                "Dry run! This is what will happen:", "yellow", "bold"))
        for calculation in self.calculations:
            try:
                if self.skip_this_calculation(
                        calculation,
                        continue_on_error=False,
                        continue_on_unstarted=only_errored,
                        continue_on_running=only_errored,
                        continue_on_finished=only_errored,
                        continue_on_converged=only_errored,
                        ):
                    continue
                # remove from database first
                calculation.connect_to_database()
                if dry:
                    print(Colors.bold_text("Will delete:") + calculation.path)
                else:
                    print(Colors.bold_text("DELETING:") + calculation.path)
                if calculation.is_in_database:
                    if dry:
                        print(Colors.bold_text("Will be removed from DB"))
                    else:
                        calculation.remove_from_database()
                        print(Colors.color_text(
                            "SUCCESSFULLY Removed from db ", "green", "bold"))
                # if calc is running, stop it!
                self.cancel_job(calculation, dry=dry)
                if dry:
                    continue
            except Exception as err:
                if not force:
                    self._logger.exception(err)
                    raise err
                if dry:
                    self._logger.error(
                            "An error occured while treating "
                            f"'{calculation.path}' but dry run.")
                    continue
                if force:
                    self._logger.error(
                            "An error occured while handling: "
                            f"'{calculation.path}' but force.")
            # delete directory
            calculation.delete()
            print(Colors.color_text(
                "SUCCESSFULLY deleted", "green", "bold"))
            calculation.clear_content()
