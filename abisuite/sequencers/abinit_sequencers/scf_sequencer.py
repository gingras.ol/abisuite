from ..scf_sequencer import SCFSequencer


class AbinitSCFSequencer(SCFSequencer):
    """SCF Sequencer for Abinit.
    """
    _loggername = "AbinitSCFSequencer"

    def __init__(self, *args, **kwargs):
        SCFSequencer.__init__(self, "abinit", *args, **kwargs)
