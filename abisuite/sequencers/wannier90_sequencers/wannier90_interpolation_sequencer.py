from ..bases import SequenceCalculation
from ..nscf_sequencers import PositiveKpointsNSCFSequencer
from ..qe_sequencers import QEBandStructureSequencer
from ...post_processors import BandStructure
from ...routines import full_abspath


# the following input variables are allowed to change when restarting the
# wannier90 part of the sequence. It allows to change some variables
# without having to reexecute the pw2wannier90 part.
WANNIER90_INPUT_VARS_CAN_CHANGE = (
    "dis_num_iter", "num_iter", "conv_window", "num_print_cycles",
    )


class Wannier90InterpolationSequencer(
        QEBandStructureSequencer, PositiveKpointsNSCFSequencer):
    """A sequencer that launches coherent calculations in order to get the
    wannier interpolation of a system. The sequence is:

    (For Quantum Espresso:)
        1- A ground state SCF calculation.
        2- A ground state NSCF calculation on a finer grid with kpts having
        only 'positive' coordinates (for wannier90).
        3- A band structure calculation following the SCF calculation.
        4- A pre-processing wannier90 run.
        5- A pw2wannier90 run in preparation for the wannier90 run.
        6- The wannier90 run.
        7- The bandstructure plot is generated.
    """
    _all_sequencer_prefixes = (
            "scf_", "nscf_", "wannier90_", "band_structure_", "pw2wannier90_",
            "plot_",
            )
    _loggername = "Wannier90InterpolationSequencer"

    def __init__(self, software, *args, **kwargs):
        """WannierInterpolationSequencer init method.

        Parameters
        ----------
        software : {'qe', 'abinit'}
            The software to use for the GS calculations.
        """
        if software != "qe":
            # TODO: later
            raise NotImplementedError(software)
        QEBandStructureSequencer.__init__(self, *args, **kwargs)
        PositiveKpointsNSCFSequencer.__init__(self, software, *args, **kwargs)
        # WANNIER EXTRA PARAMETERS
        self._updated_wannier90_input_variables = None

    @property
    def pw2wannier90_input_variables(self):
        return self._pw2wannier90_input_variables

    @pw2wannier90_input_variables.setter
    def pw2wannier90_input_variables(self, input_vars):
        if not isinstance(input_vars, dict):
            raise TypeError(
                    f"Expected a dict for pw2wannier90 inputvariables but got:"
                    f" {input_vars}")
        self._pw2wannier90_input_variables = input_vars

    @property
    def wannier90_input_variables(self):
        return self._wannier90_input_variables

    @wannier90_input_variables.setter
    def wannier90_input_variables(self, input_vars):
        input_vars["mp_grid"] = self.nscf_kgrid
        # add kpoint path
        kpts_path = []

        def replace_latex_chars(label, latex_symbol, replace_with):
            if label.startswith("$" + latex_symbol):
                return label.replace(
                        "$" + latex_symbol, replace_with).strip("$")
            return label

        for i, kpoint in enumerate(self.band_structure_kpoint_path[:-1]):
            label1 = list(kpoint.keys())[0]
            coords1 = kpoint[label1]
            label2 = list(self.band_structure_kpoint_path[i + 1].keys())[0]
            coords2 = self.band_structure_kpoint_path[i + 1][label2]
            for latex_symbol, replacement in zip(
                            (r"\Gamma", r"\Sigma"), ("G", "S")):
                label1 = replace_latex_chars(label1, latex_symbol, replacement)
                label2 = replace_latex_chars(label2, latex_symbol, replacement)
            kpts_path.append([{label1: coords1}, {label2: coords2}])
        input_vars["kpoint_path"] = kpts_path
        self._wannier90_input_variables = input_vars

    @property
    def wannier90_pp_workdir(self):
        if self.wannier90_workdir is None:
            raise ValueError("Need to set 'wannier90_workdir'.")
        tryout = full_abspath(self.wannier90_workdir + "_pp")
        # if os.path.exists(tryout):
        #     self._logger.error(f"Please remove {tryout}")
        #     raise IsADirectoryError(tryout)
        return tryout

    @property
    def wannier90_pp_calculation_parameters(self):
        calc_params = self.wannier90_calculation_parameters.copy()
        cmd_args = calc_params.get("command_arguments", "")
        if "-pp" not in cmd_args:
            cmd_args += " -pp"
        calc_params["command_arguments"] = cmd_args
        # remove mpirun command if preset
        calc_params.pop("mpi_command", None)
        # set to local since calculation is trivial and only creates files
        calc_params["queuing_system"] = "local"
        return calc_params

    def init_sequence(self, *args, **kwargs):
        # SCF and BS calculations done in BandStructure mother class
        QEBandStructureSequencer.init_sequence(self, *args, **kwargs)
        # NSCF calculation done in other mother class
        PositiveKpointsNSCFSequencer.init_sequence(self, *args, **kwargs)
        # NOTE: SCF sequence is in double but that should be fine cause
        # it's exactly the same calculation thus it won't be rerun 2 times.
        # nscf calc is the last one from that point
        for calc in self.sequence:
            if calc.workdir == self.nscf_workdir:
                nscfcalc = calc
                break
        else:
            raise LookupError("Could not find NSCF calculation in sequence.")
        for step in ("wannier90_pp", "pw2wannier90", "wannier90"):
            workdir_attr = step + "_workdir"
            workdir = getattr(self, workdir_attr)
            # set full path
            if step != "wannier90_pp":
                # special case
                if workdir is None:
                    raise ValueError(f"Need to set workdir for '{step}'.")
                setattr(self, workdir_attr, full_abspath(workdir))
        if self.software != "qe":
            raise NotImplementedError(self.software)
        # add WANNIER90_PP calculation
        wannier90ppcalc = SequenceCalculation(
                    "wannier90", self.wannier90_pp_workdir,
                    self.wannier90_input_variables,
                    self.wannier90_pp_calculation_parameters,
                    loglevel=self._loglevel)
        wannier90ppcalc.dependencies.append(nscfcalc)
        self.sequence.append(wannier90ppcalc)
        # add PW2WANNIER90 calculation
        pw2wannier90calc = SequenceCalculation(
                    "qe_pw2wannier90", self.pw2wannier90_workdir,
                    self.pw2wannier90_input_variables,
                    self.pw2wannier90_calculation_parameters,
                    loglevel=self._loglevel)
        pw2wannier90calc.dependencies += [nscfcalc,
                                          wannier90ppcalc]
        self.sequence.append(pw2wannier90calc)
        # add WANNIER90 calculation
        wannier90calc = SequenceCalculation(
                    "wannier90", self.wannier90_workdir,
                    self.wannier90_input_variables,
                    self.wannier90_calculation_parameters,
                    loglevel=self._loglevel)
        wannier90calc.dependencies += [wannier90ppcalc,
                                       pw2wannier90calc]
        self.sequence.append(wannier90calc)

    def plot_band_structure(self):
        # Plot the band structure with the wannier interpolation on top of it.
        # pop the plot parameters to make sure the bs plot is not shown/saved
        show = self.plot_calculation_parameters.pop("show", True)
        self.plot_show = False
        save = self.plot_calculation_parameters.pop("save", None)
        self.plot_save = None
        dft_plot = super().plot_band_structure()
        # reset parameters
        self.plot_show = show
        self.plot_save = save
        self._logger.info("Plotting Wannier Interpolation...")

        wannier_bs = BandStructure.from_calculation(
                self.wannier90_workdir, loglevel=self._loglevel)
        wannier_plot = wannier_bs.get_plot(
                yunits="eV",
                ylabel="Energy",
                color="r",
                linewidth=2,
                symmetry="none",
                all_k_labels="none")
        final_plot = dft_plot + wannier_plot
        final_plot.curves[0].label = "dft"
        final_plot.curves[-1].label = "wannier"
        # grid on by default
        final_plot.grid = True
        # apply other parameters
        for attr, value in self.plot_calculation_parameters.items():
            if attr in ("plot", "save"):
                continue
            setattr(final_plot, attr, value)
        final_plot.plot(show=show)
        if save is not None:
            final_plot.save(save)
        return final_plot

    def update_wannier90_input_variables(self, input_vars):
        if not isinstance(input_vars, dict):
            raise TypeError("need dict for updated input vars.")
        for var in input_vars:
            if var not in WANNIER90_INPUT_VARS_CAN_CHANGE:
                raise NameError(f"'{var}' is not allowed to change.")
        self._updated_wannier90_input_variables = input_vars

    def _create_new_calculation_launcher(self, *args, **kwargs):
        launcher = super()._create_new_calculation_launcher(
                        *args, **kwargs)
        if launcher.workdir == full_abspath(self.wannier90_workdir):
            if self._updated_wannier90_input_variables is not None:
                launcher.input_variables.update(
                                self._updated_wannier90_input_variables)
        return launcher
