import tempfile
import unittest

from .bases import BasePathConvertibleFileHandlerTest
from ...handlers import SymLinkFile


class TestSymLinkFile(BasePathConvertibleFileHandlerTest, unittest.TestCase):
    """Test case for a symlink file handler.
    """
    _file_handler_class = SymLinkFile

    def setUp(self):
        super().setUp(write=False)
        # create source file
        source = tempfile.NamedTemporaryFile(dir=self.tempdir.name,
                                             delete=False)
        self.handler.source = source.name
        self.handler.write()

    def _change_random_property(self, handler):
        # only one property to change is source
        source = tempfile.NamedTemporaryFile(dir=self.tempdir.name,
                                             delete=False)
        handler.source = source.name
