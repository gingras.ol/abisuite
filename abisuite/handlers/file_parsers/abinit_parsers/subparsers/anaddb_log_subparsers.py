from ...bases import BaseSubParser
from .....routines import decompose_line


class AbinitAnaddbLogTimingSubparser(BaseSubParser):
    """Subparser for the timing analysis of an anaddb log file.
    """
    _loggername = "AbinitAnaddbLogTimingSubparser"
    subject = "timing"
    trigger = "Total cpu time"

    def _extract_data_from_lines(self, lines):
        data = {}
        # data is located on first line which looks like:
        # +Total cpu time      0.224  and wall time      0.269 sec
        s, i, f = decompose_line(lines[0])
        data["walltime"] = f[-1]
        data["cputime"] = f[0]
        self.ending_relative_index = 0
        return data


class AbinitAnaddbLogNonAnalyticalGammaCorrectionsSubParser(BaseSubParser):
    """Subparser for the non-analytical gamma corrections at gamma
    (lo-to splitting).
    """
    _loggername = "AbinitAnaddbLogNonAnalyticalGammaCorrectionsSubParser"
    subject = "non_analytical_gamma_corrections"
    trigger = "Treat the second list of vectors"

    def _extract_data_from_lines(self, lines):
        data = {}
        # data looks like this
        # Treat the second list of vectors
        #
        # phonon wavelength (reduced coordinates) , norm, and energies in hartree                # noqa: E501
        #   1.00 0.00 0.00 0.00
        #   0.000000000E+00  0.000000000E+00  0.000000000E+00  1.568553720E-03  1.568553720E-03  # noqa: E501
        #   1.730353189E-03
        #  Zero Point Motion energy (sum of freqs/2)=  2.433730315E-03
        #  Proc.   0 individual time (sec): cpu=          0.7  wall=          0.7                # noqa: E501
        eigs = []
        treating_eigs = False
        for i, line in enumerate(lines):
            if "Zero Point Motion" in line:
                # return what we got
                self.ending_relative_index = i
                data["eigenvalues"] = eigs
                return data
            s, i, f = decompose_line(line)
            if f:
                if not treating_eigs:
                    treating_eigs = True
                    # this is the direction line
                    data["direction"] = f[:-1]
                    continue
                else:
                    # rest is eigenvalues
                    eigs += f
                    continue
