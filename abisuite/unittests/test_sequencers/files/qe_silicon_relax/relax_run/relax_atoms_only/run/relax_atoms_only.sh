#!/bin/bash

MPIRUN="mpirun -np 4"
EXECUTABLE="/home/fgoudreault/Workspace/q-e/bin/pw.x -npool 4"
INPUT=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_sequencers/files/qe_silicon_relax/relax_run/relax_atoms_only/relax_atoms_only.in
LOG=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_sequencers/files/qe_silicon_relax/relax_run/relax_atoms_only/relax_atoms_only.log
STDERR=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_sequencers/files/qe_silicon_relax/relax_run/relax_atoms_only/relax_atoms_only.stderr


$MPIRUN $EXECUTABLE -input $INPUT > $LOG 2> $STDERR
