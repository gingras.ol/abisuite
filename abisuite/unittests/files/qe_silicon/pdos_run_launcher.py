from abisuite import QEProjwfcLauncher as Launcher


variables = {
        "ngauss": 0,
        "degauss": 0.05,
        "Emin": -20,
        "Emax": 20,
        "DeltaE": 0.01,
        }

launcher = Launcher("pdos")
launcher.workdir = "pdos_run"
launcher.input_variables = variables
launcher.command_arguments = "-npool 4"
launcher.mpi_command = "mpirun -np 4"
launcher.link_calculation("pw_run_nscf")
launcher.write()
launcher.run()
