import abc
import os

from ..bases import BaseStructuredObject
from ...routines import full_abspath


class BaseStreamLineWriter(BaseStructuredObject, abc.ABC):
    """Base class for writers without enforcing approvers or with custom
    writing properties / methods.
    """

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self._workdir = None
        self._filename = None
        self._path = None

    @property
    def path(self):
        if self._path is not None:
            return self._path
        raise ValueError("Need to setup file path attr.")

    @path.setter
    def path(self, path):
        self._path = path
        self._logger.debug(f"Setting file path to {self.path}")

    @property
    def workdir(self):
        if self._workdir is not None:
            return self._workdir
        self._workdir = full_abspath(os.path.dirname(self.path))
        self._logger.debug(f"Assigning workdir to {self.workdir}")
        return self.workdir

    def write(self, overwrite=False):
        """Write the file.

        Parameters
        ----------
        overwrite: bool, optional
                   If False, an error is raised when attempting to write a
                   file that already exists under the same name.
        """
        if os.path.exists(self.path) or os.path.islink(self.path):
            # second condition exists cause broken link will return False
            # on os.path.exists where as the broken symlink actually exists
            if not overwrite:
                # raise the error
                raise FileExistsError(f"{self.path} already exists.")
            # else, erase file and rewrite
            os.remove(self.path)
        self._logger.debug(f"Writing file at: {self.path}")
        self._do_write()

    @abc.abstractmethod
    def _do_write(self, *args, **kwargs):  # pragma: no cover
        pass


class BaseWriter(BaseStreamLineWriter, abc.ABC):
    """Base class for writers that have a custom syntax.
    For these, the lines are computed and written manually.
    """

    @property
    @abc.abstractmethod
    def lines(self):
        # need to compute lines
        pass  # pragma: no cover

    def _do_write(self):
        # ready to write at this point
        with open(self.path, "w") as f:
            for line in self.lines:
                f.write(line)


# TODO: get rid of this class
class BaseInputWriter(BaseWriter):
    pass
