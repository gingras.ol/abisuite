import os
import unittest

from .bases import (
        BaseFileHandlerTest, BasePathConvertibleFileHandlerTest,
        BaseInputFileHandlerTest,
        )
from ..variables_for_tests import (
        abinit_vars, abinit_anaddb_vars, abinit_optic_vars, abinit_out_example,
        abinit_anaddb_log_example, abinit_anaddb_phfrq_example,
        abinit_dmft_hamiltonian_example,
        abinit_dmft_projectors_example, abinit_mrgddb_log_example,
        abinit_AlAs_pseudos,
        )
from ...handlers import (
        AbinitAnaddbFilesFile, AbinitAnaddbInputFile, AbinitAnaddbLogFile,
        AbinitAnaddbPhfrqFile, AbinitDMFTEigFile, AbinitDMFTProjectorsFile,
        AbinitFilesFile, AbinitInputFile, AbinitMrgddbInputFile,
        AbinitMrgddbLogFile, AbinitOpticFilesFile, AbinitOpticInputFile,
        AbinitOutputFile,
        )


class BaseFilesHandlerTest(BasePathConvertibleFileHandlerTest):
    """Base test case class for all files file handler classes.
    """
    def setUp(self, write=True):
        super().setUp(write=False)
        self.handler.input_file_path = os.path.join(self.tempdir.name,
                                                    "input")
        self.handler.output_file_path = os.path.join(self.tempdir.name,
                                                     "output")
        if write:
            self.handler.write()

    def _change_random_property(self, handler):
        handler.output_file_path += "_new"


class TestAbinitAnaddbFilesFile(BaseFilesHandlerTest, unittest.TestCase):
    """Test case for an anaddb files file handler.
    """
    _file_handler_class = AbinitAnaddbFilesFile

    def setUp(self):
        super().setUp(write=False)
        self.handler.ddb_file_path = "test_ddb"
        self.handler.band_structure_file_path = "test_band_structure"
        self.handler.gkk_file_path = "test_gkk"
        self.handler.eph_data_prefix = "test_eph_data_prexif"
        self.handler.ddk_file_path = "test_ddk"
        self.handler.write()


class TestAbinitAnaddbInputFile(BaseInputFileHandlerTest, unittest.TestCase):
    """Test case for the anaddb input file handler.
    """
    _file_handler_class = AbinitAnaddbInputFile
    _input_vars = abinit_anaddb_vars.copy()

    def _change_random_property(self, handler):
        handler.input_variables["ifcout"] += 1


class TestAbinitAnaddbLogFile(BaseFileHandlerTest, unittest.TestCase):
    """Test case for an anaddb log file handler.
    """
    _file_handler_class = AbinitAnaddbLogFile
    _example_file = abinit_anaddb_log_example


class TestAbinitAnaddbPhfrqFile(BaseFileHandlerTest, unittest.TestCase):
    """Test case for an anaddb PHFRQ file handler.
    """
    _file_handler_class = AbinitAnaddbPhfrqFile
    _example_file = abinit_anaddb_phfrq_example


class TestAbinitFilesFile(BaseFilesHandlerTest, unittest.TestCase):
    _file_handler_class = AbinitFilesFile

    def setUp(self):
        super().setUp(write=False)
        self.handler.output_data_prefix = "data"
        self.handler.output_data_dir = self.tempdir.name
        self.handler.tmp_data_prefix = "tmp"
        self.handler.tmp_data_dir = self.tempdir.name
        self.handler.input_data_prefix = "input"
        self.handler.input_data_dir = self.tempdir.name
        self.handler.pseudos = abinit_AlAs_pseudos
        self.handler.write()


class TestAbinitInputFile(BaseInputFileHandlerTest, unittest.TestCase):
    _file_handler_class = AbinitInputFile
    _input_vars = abinit_vars.copy()

    def _change_random_property(self, handler):
        handler.input_variables["ecut"] += 1.0


class TestAbinitMrgddbInputFile(
        BasePathConvertibleFileHandlerTest, unittest.TestCase):
    _file_handler_class = AbinitMrgddbInputFile

    def setUp(self):
        super().setUp(write=False)
        self.handler.output_file_path = "test_output"
        self.handler.title = "test title"
        self.handler.ddb_paths = ["ddb1_path", "ddb2_path", "ddb3_path"]
        self.handler.write()

    def _change_random_property(self, handler):
        handler.title += " new"


class TestAbinitMrgddbLogFile(BaseFileHandlerTest, unittest.TestCase):
    _file_handler_class = AbinitMrgddbLogFile
    _example_file = abinit_mrgddb_log_example


class TestAbinitOutputFile(BaseFileHandlerTest, unittest.TestCase):
    _file_handler_class = AbinitOutputFile
    _example_file = abinit_out_example


class TestAbinitOpticInputFile(BaseInputFileHandlerTest, unittest.TestCase):
    _file_handler_class = AbinitOpticInputFile
    _input_vars = abinit_optic_vars.copy()

    def _change_random_property(self, handler):
        handler.input_variables["broadening"] += 0.01


class TestAbinitOpticFilesFile(BaseFilesHandlerTest, unittest.TestCase):
    """Test case for the abinit optic files file handler.
    """
    _file_handler_class = AbinitOpticFilesFile

    def setUp(self):
        super().setUp(write=False)
        self.handler.output_data_prefix = "data"
        self.handler.output_data_dir = self.tempdir.name
        self.handler.write()


class TestAbinitDMFTEigFile(BaseFileHandlerTest, unittest.TestCase):
    _file_handler_class = AbinitDMFTEigFile
    _example_file = abinit_dmft_hamiltonian_example


class TestAbinitDMFTProjectorsFile(BaseFileHandlerTest, unittest.TestCase):
    _file_handler_class = AbinitDMFTProjectorsFile
    _example_file = abinit_dmft_projectors_example
