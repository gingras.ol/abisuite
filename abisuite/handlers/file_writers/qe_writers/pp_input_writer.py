from .bases import BaseQEInputWriter
from ...file_structures import QEPPInputStructure


class QEPPInputWriter(BaseQEInputWriter):
    """Class that can write a Quantum Espresso pp.x input file.
    """
    _loggername = "QEPPInputWriter"
    _structure_class = QEPPInputStructure

    @property
    def lines(self):
        lines = []
        self._append_var_block(lines, "inputpp")
        self._append_var_block(lines, "plot")
        return lines
