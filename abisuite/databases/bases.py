from collections import OrderedDict  # in case python <= 3.5
import os
import sqlite3

from .exceptions import ColumnDoesNotExistsError
from ..bases import BaseUtility
from ..routines import full_abspath


# use lower case here
__ALL_SQL_DATA_TYPES__ = ("int", "integer", "text", "float", "bit", )
__SQL_DATA_TYPES_TO_PYTHON_CAST__ = {
        "int": int,
        "integer": int,
        "text": str,
        "float": float,
        "real": float,
        "bit": bool,
        }


def __PYTHON_CAST_TO_SQL_DATA_TYPES__(python_cast):
    """The equivalent of the '__SQL_DATA_TYPES_TO_PYTHON_CAST__' but the
    other way around and with some checks.
    """
    if python_cast is int:
        return "INT"
    elif python_cast is str:
        return "TEXT"
    elif python_cast is float:
        return "FLOAT"
    elif python_cast is bool:
        return "BIT"
    if not isinstance(python_cast, str):
        raise TypeError(
                f"Unrecognized data cast: '{python_cast}'.")
    if python_cast.lower() not in __ALL_SQL_DATA_TYPES__:
        raise TypeError(
                f"Unrecognized data cast: '{python_cast}'.")
    return python_cast


class DB(BaseUtility):
    """Class that handles a 'local' database. It uses sqlite3 that
    is interfaced via the built-in python module.
    """
    _loggername = "DB"

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._primary_key = None
        self._db = None
        self._connection = None
        self._cursor = None
        self._tables_have_been_read = False
        self._tables = []
        self._columns = {}
        self._columns_have_been_read = False
        self.default_values_of_columns = {}
        self.columns_can_be_none = []

    def __enter__(self):
        if self._connection is None:
            try:
                self._connection = sqlite3.connect(self.db)
            except sqlite3.OperationalError:
                if not os.path.isfile(self.db):
                    self._logger.error(
                        f"DB file does not exists: '{self.db}'.")
                raise
        with self._connection:
            return self

    def __exit__(self, exc_type, exc_value, traceback):
        # set connection and cursor back to none
        self._connection, self._cursor = None, None

    def __repr__(self):
        return f"{self.__class__}: {self.db}"

    @property
    def columns(self):
        if self._columns_have_been_read:
            return self._columns
        columns = {}
        for table in self.tables:
            columns[table] = self.list_columns_from_table(table)
        self._columns = columns
        self._columns_have_been_read = True
        return self.columns

    @property
    def cursor(self):
        if self._cursor is not None:
            return self._cursor
        if self._connection is None:
            raise RuntimeError(
                    "Set connection first. Use the 'with' statement.")
        self._cursor = self._connection.cursor()
        return self.cursor

    @property
    def db(self):
        """The path to the database.
        """
        if self._db is not None:
            return self._db
        raise AttributeError(
                "Need to set 'db' before using it.")

    @db.setter
    def db(self, db):
        if not isinstance(db, str):
            raise TypeError(
                    f"Expected a string but got: '{db}'.")
        db = full_abspath(db)
        if not os.path.isfile(db):
            raise FileNotFoundError(
                    f"No database found here: '{db}'.")
        self._db = db

    @property
    def primary_key(self):
        if self._columns_have_been_read:
            return self._primary_key
        # call self.columns and return pk
        self.columns
        return self.primary_key

    @property
    def tables(self):
        """The list of all tables in the db.
        """
        if self._tables_have_been_read:
            return self._tables
        self._tables = self.list_tables()
        self._tables_have_been_read = True
        return self.tables

    def add_columns(self, table_name, **kwargs):
        """Add one or more columns to a database table.

        Parameters
        ----------
        table_name : str
            The name of the table.
        kwargs : key:value pairs where keys are the new columns names and their
            corresponding values is the cast of the data of the column.
        """
        if table_name not in self.tables:
            raise ValueError(
                    f"Table doesn't exist in database: '{table_name}'.")
        if not len(kwargs):
            raise ValueError("No column to add.")
        for col, value in kwargs.items():
            if col in self.columns[table_name]:
                raise ValueError(
                        f"Column already exists in table: '{col}'.")
            # if something is wrong, the error is raised in the function
            value = __PYTHON_CAST_TO_SQL_DATA_TYPES__(value)
            self._logger.info(
                    f"Adding column '{col}' to table '{table_name}'.")
            stmt = f"ALTER TABLE {table_name} ADD COLUMN {col} {value}"
            self.execute(stmt)
        self._columns_have_been_read = False

    def create_database(self, path, overwrite=False):
        """Initialize a database at the given path. Also sets the current
        'db' attribute to the given path if everything works.

        Parameters
        ----------
        path : str
            The path of the database file.
        overwrite : bool, optional
            If True and if a file already exists, it will be removed.
        """
        if not isinstance(path, str):
            raise TypeError(
                    f"Expected a string but got: '{path}'.")
        path = full_abspath(path)
        if os.path.isfile(path):
            if overwrite:
                os.remove(path)
            else:
                raise FileExistsError(path)
        if not path.endswith(".db"):
            raise NameError("Please name file with a '.db' extension.")
        # just create a connection with sqlite3 and that's it.
        with sqlite3.connect(path):
            pass
        self.db = path

    def create_table(self, table_name, primary_key=None, **kwargs):
        """Create a table.

        Parameters
        ----------
        table_name : str
            The name of the table.
        primary_key : str, optional
            States the primary key among the table columns. It must be
            in the given kwargs of course.
        kwargs: dict, optional
            The kwargs are the column of the table. The key represents the
            column name and it's corresponding value is the cast of the data
            of the column. E.g.: id=int, calctype=str, ...
        """
        if not isinstance(table_name, str):
            raise TypeError(
                    f"Expected str for table name but got: '{table_name}'.")
        if not len(kwargs):
            raise ValueError(
                    "Expected column names and casts but we got nothing!")
        # check that table don't already exist
        if table_name in self.tables:
            raise ValueError(
                    f"This table already exists: '{table_name}'.")
        if primary_key is not None:
            if not isinstance(primary_key, str):
                raise TypeError(
                        "primary_key must be a string but got: "
                        f"'{primary_key}'.")
            # check primary key is in kwargs
            if primary_key not in kwargs:
                raise ValueError(
                        "The chosen primary_key does not exists amongst the "
                        f" given column names: '{primary_key}'.")
        statement = f"CREATE TABLE {table_name}("
        for k, v in kwargs.items():
            v = __PYTHON_CAST_TO_SQL_DATA_TYPES__(v)
            if k == primary_key:
                if v.lower() not in ("int", "integer"):
                    raise TypeError(
                            "The primary key must be an integer!")
                statement += (
                        f"{k} INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,"
                        )
            else:
                statement += f"{k} {v},"
        # delete last ',' and close statement
        statement = statement[:-1]
        statement += ")"
        self._logger.info(f"Creating table '{table_name}' in db: '{self.db}'.")
        self._logger.info(f"Table creation statement: '{statement}'.")
        self.execute(statement)
        self._tables_have_been_read = False  # reset flag
        self._columns_have_been_read = False

    def delete_column(self, table_name, column):
        """Delete a single column from the database.

        Note
        ----
        Since we use SQLite, which does not support natively column deletion,
        we must copy data into a new column and filtering out the data
        we don't want anymore. More details there:

        https://stackoverflow.com/a/5987838/6362595

        Parameters
        ----------
        table_name: str
            The name of the table of which we want to delete the column.
        column: str
            The name of the column to delete.

        Raises
        ------
        ValueError: If table_name or column name does not exist in DB.
        """
        if table_name not in self.tables:
            raise ValueError(
                    f"Table does not exist in database: '{table_name}'.")
        if column not in self.columns[table_name]:
            raise ValueError(
                     f"Column to delete not in table's columns: '{column}'.")
        self._logger.info(
                f"Deleting column '{column}' from table '{table_name}'.")
        # complicated procedure since SQLite does not support dropping
        # a column in its ALTER statement (see docstring for SO post).
        # start by making a backup of the table we want without copying the
        # column we want to delete
        cols_now = self.columns[table_name]
        backup_table_name = table_name + "_backup"
        # create backup table and remove column we don't want
        kwargs = cols_now.copy()
        if table_name in self.primary_key:
            pk = self.primary_key[table_name]
            if pk != column:
                kwargs["primary_key"] = pk  # keep primary key
        kwargs.pop(column)
        self.create_table(backup_table_name, **kwargs)
        # copy data from old table to backup table
        # do it using a SQL statement (easier)
        cols_to_insert = ", ".join([str(x)
                                    for x in kwargs if x != "primary_key"])
        self.execute(
                f"INSERT INTO {backup_table_name} SELECT {cols_to_insert} "
                f"FROM {table_name};")

        # delete actual table
        self.delete_table(table_name)
        # recreate old table
        self.create_table(table_name, **kwargs)
        # repopulate new old table with backed up data
        self.execute(
                f"INSERT INTO {table_name} SELECT {cols_to_insert} "
                f"FROM {backup_table_name};")
        # delete backup table
        self.delete_table(backup_table_name)
        self._columns_have_been_read = False

    def delete_database(self):
        """Delete the whole database.
        """
        # just need to remove the file if it exists.
        if not os.path.isfile(self.db):
            return
        os.remove(self.db)
        self._db = None  # reset db path
        self._tables_have_been_read = False
        self._columns_have_been_read = False

    def delete_row(self, table_name, condition):
        """Delete a row in the database.

        Parameters
        ----------
        table_name : str
            The name of the table to delete data.
        condition : str
            The condition for the SQL statement that will delete the row(s).
        """
        if table_name not in self.tables:
            raise ValueError(
                    f"Table is not in database: '{table_name}'.")
        self.execute(f"DELETE FROM {table_name} WHERE {condition};")

    def delete_table(self, table_name):
        """Delete a table if it exists.
        """
        if table_name not in self.tables:
            # table does not exist
            return
        self.execute(f"DROP TABLE {table_name}")
        self._tables_have_been_read = False
        self._columns_have_been_read = False

    def execute(self, statement, max_retries=5):
        """Execute a SQL statement and returns the data associated with the
        'fetchall' command.

        Parameters
        ----------
        statement: str
            The SQL statement to execute.
        max_retries: int, optional
            In case of a sqlite3 exception, the statement will be retried
            this number of times maximum.

        Notes
        -----
        WARNING: The statement must be a valid SQL statement. No checks
        are done in this function.
        """
        self._logger.debug(f"Executing SQL: {statement}")
        i_retry = 0
        exceptions = []
        while i_retry < max_retries:
            with self:
                try:
                    self.cursor.execute(statement)
                except sqlite3.OperationalError as exc:
                    exceptions.append(exc)
                    if i_retry == max_retries:
                        # log all exceptions
                        self._logger.error(
                                "The maximum number of retries have been "
                                f"reached ({max_retries}) while trying to "
                                f"interact with database ({self.db}).")
                        self._logger.error(f"SQL statement was '{statement}'.")
                        self._logger.error(
                                "Printing all exceptions that occured:")
                        for exception in exceptions:
                            self._logger.exception(exception)
                        # raise last known exception
                        raise exc
                    i_retry += 1
                self._connection.commit()  # commit changes (if any)
                return self.cursor.fetchall()  # return everything if relevent

    def insert(self, table_name, **kwargs):
        """Insert 1 entry data into a table.

        Parameters
        ----------
        table_name : str
            The table to add data into it.
        kwargs : dict
            The kwargs' keys and values are the data to insert into the table.
            They must match the declaration of the table. If not, an error is
            raised.
        """
        if table_name not in self.tables:
            raise ValueError(
                    f"Table '{table_name}' does not exists in '{self.db}'.")
        # check data is good
        kwargs = self._check_data(table_name, kwargs)
        if table_name in self.primary_key:
            # there is a primary key with autoincrement. In this case we must
            # explicitely state which columns are inserted
            keys = list(self.columns[table_name].keys())
            keys.remove(self.primary_key[table_name])
            statement = (f"INSERT INTO {table_name}("
                         f"{','.join(keys)}) VALUES(")
        else:
            statement = f"INSERT INTO {table_name} VALUES("
        # check all data is present and create statement
        columns = self.columns[table_name]
        for k in columns:
            if table_name in self.primary_key:
                if k == self.primary_key[table_name]:
                    # automatic handling by the Database for this key.
                    continue
            if k not in kwargs:
                if k in self.default_values_of_columns:
                    value = self.default_values_of_columns[k]
                else:
                    raise ValueError(
                            f"Missing data for column: '{k}'.")
            else:
                value = kwargs[k]
            if isinstance(value, str):
                statement += f"'{value}',"
            elif value is None:
                # None values translate to Null in SQL language
                statement += "Null,"
            else:
                statement += f"{value},"
        # replace last comma with a closing parenthesis
        statement = statement[:-1] + ");"
        # execute statement
        self.execute(statement)

    def list_columns_from_table(self, table_name):
        """List the columns and their data types from a specific table in the
        database.

        Parameters
        ----------
        table_name : str
            The name of the table.
        """
        if table_name not in self.tables:
            raise ValueError(
                    f"Table '{table_name}' does not exists in db: '{self.db}'."
                    )
        self._logger.debug(
                f"Creating list of columns for table '{table_name}'.")
        # Taken from:
        # https://stackoverflow.com/a/948204/6362595
        data = self.execute(f"PRAGMA table_info('{table_name}')")
        # data example:
        #  [(0, 'id', 'INT', None, 0),
        #   (1, 'name', 'TEXT', None, 0)]
        # first index = ?
        # second = name of column
        # third = type of data in column
        # fourth = default value of column
        # fifth = 0 if column is primary key or 'column number' if it is
        # see: https://www.sqlite.org/draft/pragma.html#pragma_table_info
        # for more info
        toreturn = OrderedDict()
        self._primary_key = {}
        for column in data:
            toreturn[column[1]] = __SQL_DATA_TYPES_TO_PYTHON_CAST__[
                                    column[2].lower()]
            if column[-1] != 0:
                self._primary_key[table_name] = column[1]
        return toreturn

    def list_tables(self):
        """Return the name of all tables in the db.
        """
        statement = (
                "SELECT name FROM sqlite_master WHERE type='table' and "
                "name NOT LIKE 'sqlite_%';"
                )
        self._logger.debug(
                f"Creating list of tables for db '{self.db}'.")
        data = self.execute(statement)
        # e.g. of data: [('table1', ), ('table2', ), ...]
        return [x[0] for x in data]

    def rename_table(self, oldname, newname):
        """Rename a table.

        Parameters
        ----------
        oldname : str
            The table to rename.
        newname : str
            The new name for the table.
        """
        if oldname not in self.tables:
            raise ValueError(f"Table '{oldname}' does not exist in database.")
        if newname in self.tables:
            raise ValueError(
                    f"A table with the name '{newname}' already exist in the "
                    "database.")
        stmt = f"ALTER TABLE {oldname} RENAME TO {newname};"
        self.execute(stmt)
        self._tables_have_been_read = False

    def select(self, selection, table_name, condition=None):
        """Returns the result of the 'SELECT' query.

        Parameters
        ----------
        selection : str
            What to select from the table.
        table_name : str
            The table to get the selection from.
        condition : str, optional
            A condition to be included in a WHERE SQL statement when
            executing SELECT. Note: it must be valid for the databse.
            If None, no WHERE clause is added.
        """
        if table_name not in self.tables:
            raise ValueError(
                    f"'{table_name}' not in the db: '{self.db}'.")
        if selection != "*":
            # check column exists
            columns = [x.strip() for x in selection.split(",")]
            table_cols = self.columns[table_name]
            for col in columns:
                if col not in table_cols:
                    raise ColumnDoesNotExistsError(
                            f"Column '{col}' not in list of all columns for "
                            f"table '{table_name}'.")
        stmt = f"SELECT {selection} FROM {table_name}"
        if condition is not None:
            if not isinstance(condition, str):
                raise TypeError(
                        f"Expected a string for SQL condition but got: "
                        f"'{condition}'.")
            stmt += f" WHERE {condition};"
        else:
            stmt += ";"
        return self.execute(stmt)

    def update(self, table_name, condition=None, **kwargs):
        """Update rows of a table according to a given condition.

        Parameters
        ----------
        table_name : str
            The name of the table in which we need to update rows.
        condition : str, optional
            The WHERE condition statement for the SQL UPDATE statement.
            If not None, must be a valid condition statement.
        kwargs : key=value where keys are the columns to update and
            corresponding values are the updated values.
        """
        if table_name not in self.tables:
            raise ValueError(
                    f"'{table_name}' not in database.")
        if condition is not None:
            if not isinstance(condition, str):
                raise TypeError(
                        f"Expected a str for condition but got '{condition}'."
                        )
        kwargs = self._check_data(table_name, kwargs)
        sets = " , ".join([" = ".join(
            [key, str(value) if not isinstance(value, str) else f"'{value}'"])
                           for key, value in kwargs.items()])
        stmt = f"UPDATE {table_name} SET {sets}"
        if condition is not None:
            stmt += f" WHERE {condition};"
        else:
            stmt += ";"
        self.execute(stmt)

    @classmethod
    def from_file(cls, path, *args, **kwargs):
        """Create a DB object and sets the db path to the path given.
        """
        obj = cls(*args, **kwargs)
        obj.db = path
        return obj

    def _check_data(self, table_name, data):
        # checks data is good and correct it if needed
        # NOTE: the following check could be irrelevent in the invent if all
        # columns have default values or if all columns are primary keys
        if not len(data):
            raise ValueError(
                    "Expected data to insert but got none.")
        columns = self.columns[table_name]
        for k, v in data.items():
            if k not in columns:
                # check we don't have too much data to give to the column
                raise ValueError(
                        f"Invalid data column: '{k}' for table '{table_name}'."
                        )
            # check cast of data
            cast = columns[k]
            # separate bool from other casts
            if cast is not bool and not isinstance(v, cast):
                if v is None and k not in self.columns_can_be_none:
                    raise TypeError(
                        f"Expect data type if '{k}' to be '{columns[k]}' but "
                        f"we got instead: '{v}'.")
            elif cast is bool:
                # expected datatype is boolean
                # this case is handled differently since you can't just recast
                # the value in the new datatype
                if v not in (0, 1, True, False):
                    raise TypeError(
                            f"Expected something between '0', '1', 'True', "
                            f"'False' for bool datatype but got '{v}' instead"
                            ".")
                # recast if needed
                if v is True:
                    data[k] = 1
                elif v is False:
                    data[k] = 0
            # else everything should be ok
        return data
