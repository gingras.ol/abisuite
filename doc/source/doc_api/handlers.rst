.. _handlers_api:

===============
Handlers Module
===============

.. contents::
    :local:

The handlers module contains classes that handles files and directories.
There are common base classes for file handlers and directory handlers
but then, each file type and directory type have their own sub classes.

TODO: the API for file parsers, file writers and file structures.

Bases Classes
=============

.. automodule:: abisuite.handlers.bases
    :members:
    :show-inheritance:

Directory Handlers
==================

The tree of a prototypical calculation directory in abisuite is made up
of various sub directories. The main one is the
:class:`~abisuite.handlers.directory_handlers.calculation_directory.CalculationDirectory`.
It contains an instance of various files: a :class:`~abisuite.handlers.file_handlers.meta_data_file.MetaDataFile`,
an `Input File` (the class depends of the `calctype`), a :class:`~abisuite.handlers.file_handlers.stderr_file.StderrFile`
(if the file exists), a `Log File` (the class depends on the `calctype` and it will be present
if the file exists on disk) and all the other files which is contained in the folder and sub folders.

If also contains various subdirectories: an
:class:`~abisuite.handlers.directory_handlers.input_data_directory.InputDataDirectory` and
a :class:`~abisuite.handlers.directory_handlers.run_directory.RunDirectory` which
itself contains an instance of an
:class:`~abisuite.handlers.directory_handlers.output_data_directory.OutputDataDirectory`.


.. automodule:: abisuite.handlers.directory_handlers.bases
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.directory_handlers.calculation_directory
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.directory_handlers.generic_directory
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.directory_handlers.input_data_directory
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.directory_handlers.output_data_directory
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.directory_handlers.run_directory
    :members:
    :show-inheritance:

File Handlers
=============

The file handlers manages files directly. They are not built the same but they share common
classes (see :mod:`~abisuite.handlers.file_handlers.bases`). All file handlers have a
`File Structure` which states which properties a given file possess. They all have
a corresponding `File Parser` which parses the file. Writable files possess an `File Writer`
to write them.

:mod:`Bases <abisuite.handlers.file_handlers.bases>`
----------------------------------------------------

The file handlers main base classes are stored in this module.

.. automodule:: abisuite.handlers.file_handlers.bases
    :members:
    :show-inheritance:

:mod:`Generic File Handlers <abisuite.handlers.file_handlers.generic_files>`
----------------------------------------------------------------------------

Generic file handlers. They can be used for any file but won't be able to parse
them nor write them. They should only use for direct file manipulation like
copying, moving or deleting when the user is not concerned in the actual file.

.. automodule:: abisuite.handlers.file_handlers.generic_files
    :members:
    :show-inheritance:

:mod:`Meta Data File Handler <abisuite.handlers.file_handlers.meta_data_file>`
------------------------------------------------------------------------------

.. automodule:: abisuite.handlers.file_handlers.meta_data_file
    :members:
    :show-inheritance:

:mod:`PBS File <abisuite.handlers.file_handlers.pbs_file>`
----------------------------------------------------------

.. automodule:: abisuite.handlers.file_handlers.pbs_file
    :members:
    :show-inheritance:

:mod:`Pseudo Potential File Handlers <abisuite.handlers.file_handlers.pseudo_file>`
-----------------------------------------------------------------------------------

.. automodule:: abisuite.handlers.file_handlers.pseudo_file
    :members:
    :show-inheritance:

:mod:`Stderr File Handler <abisuite.handlers.file_handlers.stderr_file>`
------------------------------------------------------------------------

.. automodule:: abisuite.handlers.file_handlers.stderr_file
    :members:
    :show-inheritance:

:mod:`Sym Link File Handler <abisuite.handlers.file_handlers.symlink_file>`
---------------------------------------------------------------------------

.. automodule:: abisuite.handlers.file_handlers.symlink_file
    :members:
    :show-inheritance:

:mod:`Abinit File Handlers <abisuite.handlers.file_handlers.abinit_handlers>`
-----------------------------------------------------------------------------

.. automodule:: abisuite.handlers.file_handlers.abinit_handlers.bases
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.file_handlers.abinit_handlers.abinit_dmft_eig_file
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.file_handlers.abinit_handlers.abinit_dmft_projectors_file
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.file_handlers.abinit_handlers.abinit_dos_file
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.file_handlers.abinit_handlers.abinit_eig_file
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.file_handlers.abinit_handlers.abinit_fatband_file
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.file_handlers.abinit_handlers.abinit_files_file
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.file_handlers.abinit_handlers.abinit_gsr_file
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.file_handlers.abinit_handlers.abinit_input_file
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.file_handlers.abinit_handlers.abinit_log_file
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.file_handlers.abinit_handlers.abinit_output_file
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.file_handlers.abinit_handlers.abinit_procar_file
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.file_handlers.abinit_handlers.abinit_projected_dos_file
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.file_handlers.abinit_handlers.anaddb_files_file
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.file_handlers.abinit_handlers.anaddb_input_file
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.file_handlers.abinit_handlers.anaddb_log_file
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.file_handlers.abinit_handlers.anaddb_phfrq_file
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.file_handlers.abinit_handlers.cut3d_input_file
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.file_handlers.abinit_handlers.mrgddb_input_file
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.file_handlers.abinit_handlers.mrgddb_log_file
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.file_handlers.abinit_handlers.optic_files_file
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.file_handlers.abinit_handlers.optic_input_file
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.file_handlers.abinit_handlers.optic_lincomp_file
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.file_handlers.abinit_handlers.optic_log_file
    :members:
    :show-inheritance:

:mod:`Quantum Espresso File Handlers <abisuite.handlers.file_handlers.qe_handlers>`
-----------------------------------------------------------------------------------

.. automodule:: abisuite.handlers.file_handlers.qe_handlers.dos_dos_file
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.file_handlers.qe_handlers.dos_input_file
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.file_handlers.qe_handlers.dos_log_file
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.file_handlers.qe_handlers.dynmat_input_file
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.file_handlers.qe_handlers.dynmat_log_file
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.file_handlers.qe_handlers.epsilon_input_file
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.file_handlers.qe_handlers.epsilon_log_file
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.file_handlers.qe_handlers.epw_a2f_file
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.file_handlers.qe_handlers.epw_band_eig_file
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.file_handlers.qe_handlers.epw_conductivity_tensor_file
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.file_handlers.qe_handlers.epw_input_file
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.file_handlers.qe_handlers.epw_log_file
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.file_handlers.qe_handlers.epw_phbandfreq_file
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.file_handlers.qe_handlers.epw_phonon_self_energy_file
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.file_handlers.qe_handlers.epw_resistivity_file
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.file_handlers.qe_handlers.epw_spec_fun_phon_file
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.file_handlers.qe_handlers.fs_input_file
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.file_handlers.qe_handlers.fs_log_file
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.file_handlers.qe_handlers.ld1_input_file
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.file_handlers.qe_handlers.ld1_log_file
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.file_handlers.qe_handlers.matdyn_dos_file
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.file_handlers.qe_handlers.matdyn_eig_file
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.file_handlers.qe_handlers.matdyn_freq_file
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.file_handlers.qe_handlers.matdyn_input_file
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.file_handlers.qe_handlers.matdyn_log_file
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.file_handlers.qe_handlers.ph_dyn0_file
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.file_handlers.qe_handlers.ph_input_file
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.file_handlers.qe_handlers.ph_log_file
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.file_handlers.qe_handlers.pp_input_file
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.file_handlers.qe_handlers.pp_log_file
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.file_handlers.qe_handlers.projwfc_input_file
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.file_handlers.qe_handlers.projwfc_log_file
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.file_handlers.qe_handlers.projwfc_pdos_file
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.file_handlers.qe_handlers.pw2wannier90_input_file
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.file_handlers.qe_handlers.pw2wannier90_log_file
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.file_handlers.qe_handlers.pw_input_file
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.file_handlers.qe_handlers.pw_log_file
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.file_handlers.qe_handlers.q2r_input_file
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.file_handlers.qe_handlers.q2r_log_file
    :members:
    :show-inheritance:

:mod:`Wannier90 File Handlers <abisuite.handlers.file_handlers.wannier90_handlers>`
-----------------------------------------------------------------------------------

.. automodule:: abisuite.handlers.file_handlers.wannier90_handlers.bases
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.file_handlers.wannier90_handlers.wannier90_banddat_file
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.file_handlers.wannier90_handlers.wannier90_bandkpt_file
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.file_handlers.wannier90_handlers.wannier90_input_file
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.file_handlers.wannier90_handlers.wannier90_output_file
    :members:
    :show-inheritance:
