from .bases import CommonQEInputVariable
from ..bases import (MANDATORY_VARIABLE_KEYS, OPTIONAL_VARIABLE_KEYS,
                     variableDBIntegrityChecker)


# #############################################################################
# ################ Quantum Espresso dos.x script variables ####################
# #############################################################################


QEDOS_ALLOWED_BLOCKS = ("dos", )
QEDOS_ALLOWED_CLASSES = (CommonQEInputVariable, )
RAW_QEDOS_VARIABLES = {
        "bz_sum": {
            "allowed": ("smearing", "tetrahedra", "tetrahedra_lin",
                        "tetrahedra_opt", ),
            "block": "dos",
            "class": CommonQEInputVariable,
            "mandatory": False,
            "type": str,
            },
        "degauss": {
            "block": "dos",
            "class": CommonQEInputVariable,
            "mandatory": True,
            "type": float,
            },
        "DeltaE": {
            "block": "dos",
            "class": CommonQEInputVariable,
            "mandatory": True,
            "type": float,
            },
        "Emax": {
            "block": "dos",
            "class": CommonQEInputVariable,
            "mandatory": False,
            "type": float,
            },
        "Emin": {
            "block": "dos",
            "class": CommonQEInputVariable,
            "mandatory": False,
            "type": float,
            },
        "fildos": {
            "block": "dos",
            "class": CommonQEInputVariable,
            "mandatory": True,
            "type": str,
            },
        "ngauss": {
            "allowed": (0, 1, -1, -99),
            "block": "dos",
            "class": CommonQEInputVariable,
            "mandatory": False,
            "type": int,
            },
        "outdir": {
            "block": "dos",
            "class": CommonQEInputVariable,
            "mandatory": True,
            "type": str
            },
        "prefix": {
            "block": "dos",
            "class": CommonQEInputVariable,
            "mandatory": True,
            "type": str
            },
        }

ALL_QEDOS_VARIABLES = variableDBIntegrityChecker(
        RAW_QEDOS_VARIABLES,
        QEDOS_ALLOWED_BLOCKS,
        MANDATORY_VARIABLE_KEYS,
        QEDOS_ALLOWED_CLASSES,
        optional_keys=OPTIONAL_VARIABLE_KEYS)
