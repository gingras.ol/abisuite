#!/bin/bash

MPIRUN=""
EXECUTABLE="/Users/fgoudreault/Workspace/abinit/build/mpi/src/98_main/anaddb"
INPUT=/Users/fgoudreault/Workspace/abisuite/abisuite/unittests/files/abinit_AlAs/anaddb/run/anaddb.files
LOG=/Users/fgoudreault/Workspace/abisuite/abisuite/unittests/files/abinit_AlAs/anaddb/anaddb.log
STDERR=/Users/fgoudreault/Workspace/abisuite/abisuite/unittests/files/abinit_AlAs/anaddb/anaddb.stderr


$MPIRUN $EXECUTABLE < $INPUT > $LOG 2> $STDERR
