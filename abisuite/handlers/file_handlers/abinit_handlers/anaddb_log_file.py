from ..bases import BaseLogFileHandler
from ...file_parsers import AbinitAnaddbLogParser
from ...file_structures import AbinitAnaddbLogStructure


class AbinitAnaddbLogFile(BaseLogFileHandler):
    """File handler class for an anaddb log file.
    """
    _loggername = "AbinitAnaddbLogFile"
    _parser_class = AbinitAnaddbLogParser
    _structure_class = AbinitAnaddbLogStructure
