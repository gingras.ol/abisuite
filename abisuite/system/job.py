from ..bases import BaseUtility


__ALL_JOB_ATTRIBUTES__ = [
        "id", "username", "account", "jobname", "nodes", "proc", "status",
        "time_left", "workdir",
        ]


class QueuedJob(BaseUtility):
    """Object that represents a queued (or running) job that appears in the
    queueing system.

    For now it is just a data structure.
    """
    _loggername = "QueuedJob"

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        # store some properties
        for attr in __ALL_JOB_ATTRIBUTES__:
            setattr(self, attr, None)

    # alias
    @property
    def tasks(self):
        return self.proc

    @tasks.setter
    def tasks(self, tasks):
        self.proc = tasks
