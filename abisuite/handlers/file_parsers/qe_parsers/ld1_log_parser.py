from .bases import BaseQELogParser
from ...file_structures import QELD1LogStructure


class QELD1LogParser(BaseQELogParser):
    """Parser class for a Quantum Espresso ld1.x log file.
    """
    _loggername = "QELD1LogParser"
    _structure_class = QELD1LogStructure
