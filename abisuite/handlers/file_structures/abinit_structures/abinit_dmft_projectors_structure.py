from ..bases import BaseStructure


class AbinitDMFTProjectorsStructure(BaseStructure):
    """Structure class for a '.ovlp' file created by the DMFT part of Abinit.
    """
    all_attributes = ("nband", "projectors")
