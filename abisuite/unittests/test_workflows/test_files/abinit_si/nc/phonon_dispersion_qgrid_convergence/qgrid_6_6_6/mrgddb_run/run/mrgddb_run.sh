#!/bin/bash

MPIRUN=""
EXECUTABLE="/home/fgoudreault/Workspace/abinit/abinit/build/develop/2021-04-20/src/98_main/mrgddb"
INPUT=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_workflows/test_files/abinit_si/nc/phonon_dispersion_qgrid_convergence/qgrid_6_6_6/mrgddb_run/mrgddb_run.in
LOG=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_workflows/test_files/abinit_si/nc/phonon_dispersion_qgrid_convergence/qgrid_6_6_6/mrgddb_run/mrgddb_run.log
STDERR=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_workflows/test_files/abinit_si/nc/phonon_dispersion_qgrid_convergence/qgrid_6_6_6/mrgddb_run/mrgddb_run.stderr


$MPIRUN $EXECUTABLE < $INPUT > $LOG 2> $STDERR
