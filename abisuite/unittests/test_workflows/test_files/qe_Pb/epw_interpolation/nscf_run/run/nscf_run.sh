#!/bin/bash

MPIRUN=""
EXECUTABLE="/home/fgoudreault/Workspace/q-e/bin/pw.x"
INPUT=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_workflows/test_files/qe_Pb/epw_interpolation/nscf_run/nscf_run.in
LOG=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_workflows/test_files/qe_Pb/epw_interpolation/nscf_run/nscf_run.log
STDERR=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_workflows/test_files/qe_Pb/epw_interpolation/nscf_run/nscf_run.stderr


$MPIRUN $EXECUTABLE -input $INPUT > $LOG 2> $STDERR
