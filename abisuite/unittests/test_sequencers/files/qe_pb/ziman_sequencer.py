from abisuite.sequencers import QEEPWZimanSequencer


fine = 10

sequencer = QEEPWZimanSequencer()
sequencer.ziman_workdir = "ziman_run"
sequencer.epw_interpolation_calculation = "epw_run"
sequencer.ziman_input_variables = {
            "a2f": True, "phonselfen": True, "degaussw": 0.005, "ngaussw": -99,
            "fsthick": 0.2, "rand_nk": fine ** 3, "rand_nq": fine ** 3,
            "rand_q": True, "rand_k": True, "vme": True,
            }
sequencer.ziman_mpi_command = "mpirun -np 4"
sequencer.ziman_command_arguments = "-npool 4"

sequencer.run()
