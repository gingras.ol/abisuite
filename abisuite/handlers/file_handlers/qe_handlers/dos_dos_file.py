from ..bases import BaseFileHandler
from ...file_parsers import QEDOSDOSParser
from ...file_structures import QEDOSDOSStructure


class QEDOSDOSFile(BaseFileHandler):
    """Class that represents a dos function file produced by a dos.x
    calculation from Quantum Espresso.
    """
    _loggername = "QEDOSDOSFile"
    _parser_class = QEDOSDOSParser
    _structure_class = QEDOSDOSStructure
