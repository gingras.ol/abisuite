from .bases import BaseQELogParser
from .subparsers.epw_log_subparsers import (
        QEEPWLogConductivitiesSubParser, QEEPWLogSERTAConductivitiesSubParser,
        QEEPWLogTimingSubParser,
        )
from ...file_structures import QEEPWLogStructure


class QEEPWLogParser(BaseQELogParser):
    """Parser class for a Quantum Espresso epw.x log file.
    """
    _loggername = "QEEPWLogParser"
    _structure_class = QEEPWLogStructure
    _subparsers = (
            QEEPWLogConductivitiesSubParser,
            QEEPWLogSERTAConductivitiesSubParser, QEEPWLogTimingSubParser,
            )
