#!/bin/bash

MPIRUN="mpirun -np 4"
EXECUTABLE="/home/fgoudreault/Workspace/abinit/abinit/build/develop/2021-04-16/src/98_main/abinit"
INPUT=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_sequencers/files/abinit_AlAs_ecut_convergence/ddk/ecut/ecut_5.0/run/ecut_5.0.files
LOG=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_sequencers/files/abinit_AlAs_ecut_convergence/ddk/ecut/ecut_5.0/ecut_5.0.log
STDERR=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_sequencers/files/abinit_AlAs_ecut_convergence/ddk/ecut/ecut_5.0/ecut_5.0.stderr


$MPIRUN $EXECUTABLE < $INPUT > $LOG 2> $STDERR
