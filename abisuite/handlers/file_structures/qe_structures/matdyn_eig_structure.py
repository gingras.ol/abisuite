from ..bases import BaseStructure


class QEMatdynEigStructure(BaseStructure):
    """Structure for a '.eig' file created by Quantum Espresso.
    """
    all_attributes = ("eigenvectors", )
