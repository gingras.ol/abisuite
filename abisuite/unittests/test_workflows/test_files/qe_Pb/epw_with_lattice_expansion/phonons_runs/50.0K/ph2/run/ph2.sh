#!/bin/bash

MPIRUN="mpirun -np 4"
EXECUTABLE="/home/fgoudreault/Workspace/q-e/bin/ph.x -npool 4"
INPUT=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_workflows/test_files/qe_Pb/epw_with_lattice_expansion/phonons_runs/50.0K/ph2/ph2.in
LOG=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_workflows/test_files/qe_Pb/epw_with_lattice_expansion/phonons_runs/50.0K/ph2/ph2.log
STDERR=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_workflows/test_files/qe_Pb/epw_with_lattice_expansion/phonons_runs/50.0K/ph2/ph2.stderr


$MPIRUN $EXECUTABLE -input $INPUT > $LOG 2> $STDERR
