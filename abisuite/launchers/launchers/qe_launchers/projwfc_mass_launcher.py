from .projwfc_launcher import QEProjwfcLauncher
from ..bases import BaseMassLauncher


class QEProjwfcMassLauncher(BaseMassLauncher):
    """The mass launcher class for Quantum Espresso calculations
    using the projwfc.x script.
    """
    _launcher_class = QEProjwfcLauncher
    _loggername = "QEProjwfcMassLauncher"
