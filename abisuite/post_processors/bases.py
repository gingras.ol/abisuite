from ..bases import BaseUtility
from ..plotters import Plot
from ..routines import is_scalar_or_str
import abc


class BasePostProcClass(BaseUtility, abc.ABC):
    """Base class for post-processing utilities that usually plots something.
    """

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self._plot = None

    def get_plot(self, xlabel="", ylabel="", xunits=None, yunits=None,
                 title=""):
        """Creates a basic plot.

        Parameters
        ----------
        xlabel : str, optional
                 The xlabel for the plot.
        ylabel : str, optional
                 The ylabel for the plot.
        xunits : str, optional
                 If not None, the units for the xaxis.
        yunits : str, optional
                 If not None, the units for the yaxis.
        title : str, optional
                Title for the plot.
        """
        plot = Plot(loglevel=self._logger.level)
        plot.xlabel = xlabel
        plot.ylabel = ylabel
        if xunits is not None:
            plot.xlabel += f" [{xunits}]"
        if yunits is not None:
            plot.ylabel += f" [{yunits}]"
        plot.title = title
        return plot

    def show(self, *args, save_at=None, **kwargs):
        """Create the plot and show it. Also can save the plot.

        Parameters
        ----------
        save_at: str, optional
                 If not None, gives the path to where the figure will be saved.
        """
        if self._plot is None:
            self._plot = self.get_plot(*args, **kwargs)
        if save_at is not None:
            self._plot.save(save_at)
        self._plot.plot()

    def _extract_main_plot_params_from_kwargs(self, kwargs):
        return {"title": kwargs.pop("title", ""),
                "xlabel": kwargs.pop("xlabel", ""),
                "xunits": kwargs.pop("xunits", None),
                "ylabel": kwargs.pop("ylabel", ""),
                "yunits": kwargs.pop("yunits", None),
                }


class BasePostProcClass_w_Fermi(BasePostProcClass):
    """Base post proc class with fermi energy property.
    """
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._fermienergy = None

    @property
    def fermi_energy(self):
        if self._fermienergy is not None:
            return self._fermienergy
        raise ValueError("Set fermi_energy before using it.")

    @fermi_energy.setter
    def fermi_energy(self, fermi_energy):
        if not is_scalar_or_str(fermi_energy):
            raise TypeError(f"Expected scalar but got: {fermi_energy}")
        self._fermienergy = float(fermi_energy)
