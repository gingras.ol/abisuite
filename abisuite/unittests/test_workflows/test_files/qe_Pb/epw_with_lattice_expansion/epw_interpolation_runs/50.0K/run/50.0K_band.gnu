set style data dots
set nokey
set xrange [0: 6.31961]
set yrange [ -1.86865 : 18.76150]
set arrow from  1.26438,  -1.86865 to  1.26438,  18.76150 nohead
set arrow from  1.89658,  -1.86865 to  1.89658,  18.76150 nohead
set arrow from  2.79063,  -1.86865 to  2.79063,  18.76150 nohead
set arrow from  3.56490,  -1.86865 to  3.56490,  18.76150 nohead
set arrow from  4.90599,  -1.86865 to  4.90599,  18.76150 nohead
set xtics ("G"  0.00000,"X"  1.26438,"W"  1.89658,"L"  2.79063,"K"  3.56490,"G"  4.90599,"W"  6.31961)
 plot "50.0K_band.dat"
