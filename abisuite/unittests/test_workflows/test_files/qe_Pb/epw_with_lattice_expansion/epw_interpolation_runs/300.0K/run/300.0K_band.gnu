set style data dots
set nokey
set xrange [0: 6.27445]
set yrange [ -2.04685 : 18.27440]
set arrow from  1.25535,  -2.04685 to  1.25535,  18.27440 nohead
set arrow from  1.88302,  -2.04685 to  1.88302,  18.27440 nohead
set arrow from  2.77069,  -2.04685 to  2.77069,  18.27440 nohead
set arrow from  3.53943,  -2.04685 to  3.53943,  18.27440 nohead
set arrow from  4.87093,  -2.04685 to  4.87093,  18.27440 nohead
set xtics ("G"  0.00000,"X"  1.25535,"W"  1.88302,"L"  2.77069,"K"  3.53943,"G"  4.87093,"W"  6.27445)
 plot "300.0K_band.dat"
