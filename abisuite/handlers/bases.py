import os

from ..bases import BaseUtility
from ..exceptions import DevError
from ..routines import full_abspath


class BaseHandler:
    """Base class for any handler object."""
    _is_writable = False

    def __init__(self, *args, **kwargs):
        self._path = None
        self.has_been_read = False
        self._basename = None

    def __enter__(self):
        if not self._is_writable and not self.exists:
            raise FileNotFoundError(
                    f"File/directory does not exist: '{self.path}'.")
        if not self.has_been_read and self.exists:
            self.read()
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        pass

    @property
    def basename(self):
        if os.path.basename(self._path) != self._basename:
            raise LookupError()
        return self._basename

    @property
    def exists(self):
        try:
            return os.path.exists(self.path)
        except (ValueError, TypeError):
            # path no set => return False
            return False

    @property
    def islink(self):
        return os.path.islink(self.path)

    @property
    def path(self):
        if self._path is not None:
            return self._path
        raise ValueError("Need to set 'path'.")

    @path.setter
    def path(self, path):
        self._path = full_abspath(path)
        self._basename = os.path.basename(self.path)

    def read(self):
        self.has_been_read = True


class BaseStructuredObject(BaseUtility):
    """Base class for all objects that supports a 'Structure' object.
    """
    _structure_class = None
    _mpi_structure_class = None

    def __init__(self, *args, **kwargs):
        if self._structure_class is None:
            raise DevError(
                    "_structure_class attribute should be set in "
                    f"{self.__class__}.")
        self._structure = self._structure_class()
        self._mpi_structure = None
        if self._mpi_structure_class is not None:
            self._mpi_structure = self._mpi_structure_class()
        super().__init__(*args, **kwargs)

    def __getattr__(self, attr):
        struc = self.structure
        mpistruc = self.mpi_structure
        if attr in struc.all_attributes or attr in dir(struc):
            return getattr(self.structure, attr)
        if self.mpi_structure is not None:
            if attr in mpistruc.all_attributes or attr in dir(mpistruc):
                return getattr(self.mpi_structure, attr)
        raise AttributeError(f"'{self.__class__}' object has no attribute "
                             f"'{attr}'.")

    def __delattr__(self, attr):
        if attr in self.structure.all_attributes:
            delattr(self.structure, attr)
            return
        if self.mpi_structure is not None:
            if attr in self.mpi_structure.all_attributes:
                delattr(self.mpi_structure, attr)
                return
        super().__delattr__(attr)

    def __setattr__(self, attr, value):
        # check this first to prevent infinite recursions
        if attr.endswith("structure") or attr in dir(self):
            super().__setattr__(attr, value)
            return
        if attr in self.structure.all_attributes or attr in dir(
                self.structure):
            setattr(self.structure, attr, value)
            # nest here because same attribute can be defined in both structurs
            if self.mpi_structure is not None:
                if attr in self.mpi_structure.all_attributes or attr in dir(
                        self.mpi_structure):
                    setattr(self.mpi_structure, attr, value)
            return
        # in case attr is only defined in mpi structure
        if self.mpi_structure is not None:
            if attr in self.mpi_structure.all_attributes:
                setattr(self.mpi_structure, attr, value)
                return
        super().__setattr__(attr, value)

    @property
    def structure(self):
        return self._structure

    @structure.setter
    def structure(self, structure):
        if not isinstance(structure, self._structure_class):
            raise TypeError(f"Expected '{self._structure_class}' instance but "
                            f"got '{structure}'")
        self._structure = structure

    @property
    def mpi_structure(self):
        return self._mpi_structure

    @mpi_structure.setter
    def mpi_structure(self, structure):
        if not isinstance(structure, self._mpi_structure_class):
            raise TypeError(f"Expected '{self._mpi_structure_class}' "
                            f"instance but got: '{structure}'")
        self._mpi_structure = structure
