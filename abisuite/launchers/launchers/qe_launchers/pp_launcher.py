import os

from .bases import BaseQELauncher_for_outdir_prefix, BasePostPWLauncher
from ....handlers import QEPPInputFile


class QEPPLauncher(BaseQELauncher_for_outdir_prefix, BasePostPWLauncher):
    """Launcher class for a Quantum Espresso Calculation using pp.x script.
    """
    _loggername = "QEPPLauncher"
    _input_file_handler_class = QEPPInputFile
    _calctype = "qe_pp"

    def _preprocess_inputs(self, inputs):
        inputs = super()._preprocess_inputs(inputs)
        # setup the output file filplot
        fil = os.path.join(self.output_data_dir.path, self.jobname + ".wavefc")
        inputs = self._overwrite_input_var(inputs, "filplot", fil)
        # also the xsf output file
        xsf = os.path.join(self.output_data_dir.path, self.jobname + ".xsf")
        inputs = self._overwrite_input_var(inputs, "fileout", xsf)
        return inputs

    def validate(self, *args, **kwargs):
        super().validate(*args, **kwargs)
        # check for a ".save" file has been linked to the calculation
        self._check_for_input_file(".save")
