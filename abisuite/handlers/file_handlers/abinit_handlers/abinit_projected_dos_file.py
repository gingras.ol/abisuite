from ..bases import BaseFileHandler
from ...file_parsers import AbinitProjectedDOSParser
from ...file_structures import AbinitProjectedDOSStructure


class AbinitProjectedDOSFile(BaseFileHandler):
    """File handler class for a _DOS_AT file produced by the abinit software.
    """
    _loggername = "AbinitProjectedDOSFile"
    _parser_class = AbinitProjectedDOSParser
    _structure_class = AbinitProjectedDOSStructure
