from .dos_launcher import QEDOSLauncher
from ..bases import BaseMassLauncher


class QEDOSMassLauncher(BaseMassLauncher):
    """The mass launcher class for Quantum Espresso calculations
    using the dos.x script.
    """
    _launcher_class = QEDOSLauncher
    _loggername = "QEDOSMassLauncher"
