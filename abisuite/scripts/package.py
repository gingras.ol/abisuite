import os

import tqdm

from .bases import BaseAbisuiteSubScript
from ..colors import Colors
from ..handlers import (
        GenericDirectory, GenericFile,
        SlurmFilesToDeleteFile, is_calculation_directory,
        )
from ..packagers.routines import compute_packaging_path, get_packaging_roots


# Package all files ending with these extensions as well
# even though they are outside any calculation directory
__PACKAGE_FILE_ENDINGS__ = (".pickle", ".pdf", ".py", ".txt", "readme")

__BACKUP_FILE_NAME__ = "TEMP_PACKAGING_BACKUP"


class AbisuitePackageScript(BaseAbisuiteSubScript):
    """Executes the 'package' subcommand of the abisuite script.
    """
    _loggername = "AbisuitePackageScript"

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        # calculations_src are the main srcs used to build the calculation
        # tree. It can be built from various ways.
        self.calculations_src = []
        self.tot_files_cleared = 0
        self.tot_Gbs_cleared = 0
        self.tot_files_packaged = 0
        self.tot_Gbs_packaged = 0

    def create_backup_file(self):
        """Backup all the calculations to package in a temporary file.
        """
        with open(__BACKUP_FILE_NAME__, "w") as f:
            for calc in self.calculations_src:
                f.write(calc + "\n")

    def delete_backup_file(self):
        """Delete the backup file if it still exists.
        """
        if os.path.exists(__BACKUP_FILE_NAME__):
            os.remove(__BACKUP_FILE_NAME__)

    def from_file(self, filename):
        """Loads calculations from given file.

        Parameters
        ----------
        filename: str
            The file path.

        Raises
        ------
        FileNotFoundError: if file is not found.
        """
        if not os.path.isfile(filename):
            raise FileNotFoundError(filename)
        self._logger.info(
                f"Loading calculations to package from '{filename}'.")
        with open(filename, "r") as f:
            calculations = f.readlines()
        self.calculations_src = [x.strip("\n") for x in calculations]

    def load_calculations(self):
        super().load_calculations(self.calculations_src)

    def package_calculations(
            self, dry=False, force=False, overwrite=False,
            replace_with_symlink=False, only_successfull=False,
            keep_density=False, keep_wavefunctions=False,
            ):
        """Packages the calculations stored in the 'calculations' property.

        Parameters
        ----------
        dry: bool, optional
            If True, dry mode is activated and only what will be done will
            be printed on screen. Nothing else is done.
        force: bool, optional
            If True, already packaged calculations (according to database)
            will be repackaged. This option also turns 'overwrite' option
            to True. Also, all calculations, regarding of its status will
            be packaged.
        overwrite: bool, optional
            In case a calculation is not marked as 'packaged' in the database
            but files already exists at destination, these files will
            be overwritten. This option is automatically turned on if
            'force' is True.
        replace_with_symlink: bool, optional
            If True, the packaged calculation will be replaced with a symlink.
        only_successfull: bool, optional
            If True, only successfull calculations will be packaged.
        keep_density: bool, optional
            If True, the density files will be packaged even though they
            would be left out otherwise.
        keep_wavefunctions: bool, optional
            If True, the wavefunction files will be packaged even though they
            would be left out otherwise.
        """
        for calculation in self.calculations:
            if self.skip_this_calculation(
                    calculation,
                    continue_on_error=not force or only_successfull,
                    continue_on_unstarted=not force or only_successfull,
                    continue_on_running=not force or only_successfull,
                    continue_on_finished=False,
                    continue_on_converged=False,
                    ):
                continue
            if dry:
                print(Colors.bold_text("Will Package:") +
                      f" '{calculation.path}'.")
                continue
            print(Colors.color_text("Packaging:", "bold") +
                  f" '{calculation.path}'.")
            calculation.connect_to_database()
            if calculation.is_packaged and not force:
                self._logger.info(
                        Colors.color_text(
                            "Skipping Already Packaged", "green", "bold") +
                        " calculation")
                continue
            try:
                nGbs = calculation.directory_size_Gb
                nfiles = len(calculation.walk(paths_only=True))
                calculation.package(
                        overwrite=overwrite or force,
                        replace_with_symlink=replace_with_symlink,
                        keep_wavefunctions=keep_wavefunctions,
                        keep_density=keep_density,
                        )
            except Exception:
                self._logger.exception(Colors.color_text("FAILED", "red"))
            else:
                self.tot_Gbs_packaged += calculation.packager.nGbs_saved
                self.tot_files_packaged += calculation.packager.nfiles_saved
                print(Colors.color_text("SUCCESSFULLY PACKAGED", "green") +
                      f" {calculation.packager.nfiles_saved} files saved and "
                      f" {calculation.packager.nGbs_saved:.2f} Gb saved.")
                if replace_with_symlink:
                    self.success(
                            "replaced with symlink:" +
                            f" {nfiles} files for {nGbs:.2f} Gb cleared.")
                    self.tot_Gbs_cleared += nGbs
                    self.tot_files_cleared += nfiles
            # clear content of directory to free RAM
            calculation.clear_content()

    def package_other_files(self, overwrite=False, dry=False):
        """From the 'calculations_src' property, packages all relevant files
        which are outside calculations directories.

        Parameters
        ----------
        dry: bool, optional
            If True, dry mode is activated and only what will be done will
            be printed on screen. Nothing else is done.
        overwrite: bool, optional
            If True, files present at destination will be overwritten.
        """
        # also package results directory and python files independantly
        src_root, dest_root = get_packaging_roots()
        for src in self.calculations_src:
            # walk all source directory and copy every non-calculation files
            # that are relevant
            for root, dirs, files in os.walk(src):
                for dir_ in dirs:
                    if not self._package_directory(dir_):
                        continue
                    path = os.path.join(root, dir_)
                    if dry:
                        print(Colors.bold_text("Will package:") +
                              f" {path}.")
                        continue
                    handler = GenericDirectory(loglevel=self._loglevel)
                    handler.path = path
                    print(Colors.bold_text("Packaging") +
                          f" results directory: '{handler.path}'.")
                    with handler:
                        try:
                            handler.copy(
                                compute_packaging_path(
                                    handler.path, src_root, dest_root),
                                overwrite=True)
                        # except (IsADirectoryError, FileExistsError):
                        #     print(Colors.color_text(
                        #           "FAILED", "red", "bold") +
                        #           ": results dir/file already there. Use "
                        #           "--overwrite to force repackaging.")
                        except Exception:
                            self._logger.exception(
                                  Colors.color_text("FAILED", "red", "bold"))
                        else:
                            self.tot_files_packaged += len(
                                    handler.walk(paths_only=False))
                            self.tot_Gbs_packaged += handler.directory_size_Gb
                for file_ in files:
                    if not self._package_file(file_):
                        continue
                    path = os.path.join(root, file_)
                    if dry:
                        print(Colors.bold_text("Will package:") +
                              f" {path}.")
                        continue
                    handler = GenericFile(loglevel=self._loglevel)
                    handler.path = path
                    print(Colors.bold_text("Packaging") +
                          f" file '{handler.path}'.")
                    try:
                        handler.copy(compute_packaging_path(
                            handler.path, src_root, dest_root),
                            overwrite=True)
                    # except FileExistsError:
                    #     print(Colors.color_text("FAILED", "red", "bold") +
                    #           ": python script already there. Use " +
                    #           "--overwrite to force repackaging.")
                    except Exception:
                        self._logger.exception(
                                Colors.color_text("FAILED", "red"))
                    else:
                        self.tot_files_packaged += 1
                        self.tot_Gbs_packaged += handler.file_size_Gb

    def print_summary(self, replace_with_symlink=False):
        """Prints the packaging summary.

        Parameters
        ----------
        replace_with_symlink: bool, optional
            If True, prints out as well what has been cleared.
        """
        print(Colors.bold_text("Packaging summary:"))
        print(f"{self.tot_files_packaged} files for "
              f"{self.tot_Gbs_packaged:.2f} Gb packaged.")
        if replace_with_symlink:
            print(f"{self.tot_files_cleared} files for "
                  f"{self.tot_Gbs_cleared:.2f} Gb cleared.")

    def use_slurm_files_to_delete(self, path):
        """Loads calculations from a 'slurm files to delete' file.

        This method fills the 'self.calculations_src' list.

        Parameters
        ----------
        path: str
            The path to the slurm file.
        """
        print(Colors.bold_text("USING Slurm files to delete: ") + path)
        with SlurmFilesToDeleteFile.from_file(
                path, loglevel=self._loglevel) as slurm:
            files_to_delete = slurm.files_to_delete
        self.calculations_src = set()  # to avoid duplicates

        print("Parsing slurm file. This may take a while...")
        for file_to_delete in tqdm.tqdm(
                files_to_delete, total=len(files_to_delete), unit="filename",
                unit_scale=True):
            # regroup only calc dirs and
            src = self._get_calc_src(file_to_delete)
            if src is not None:
                self.calculations_src.add(src)
        self.calculations_src = list(self.calculations_src)
        # calculations = []
        # for calc in calculations_src:
        #     if is_calculation_directory(calc):
        #         calculations.append(
        #                 CalculationDirectory.from_calculation(calc))

    def _get_calc_src(self, filename):
        # from a filename, returns the corresponding calculation directory
        # or the parent directory in case it's a file we want to package
        for ending in __PACKAGE_FILE_ENDINGS__:
            if filename.endswith(ending):
                # return it's dirname since the dirname will be walked
                # through later
                return os.path.dirname(filename)
        # go up recursively until we find a calcdir
        dirname = os.path.dirname(filename)
        while not is_calculation_directory(dirname):
            dirname_of_dirname = os.path.dirname(dirname)
            if dirname_of_dirname == dirname:
                # reached root dir, stop it there
                return None
            dirname = dirname_of_dirname
        return dirname

    def _package_directory(self, dirname):
        """Tells if we keep this directory or not based on its name.

        Parameters
        ----------
        dirname: str
            The name (basename) of the directory.

        Returns
        -------
        bool: True if we package. False otherwise.
        """
        if is_calculation_directory(dirname):
            # calculations directories are supposed to be already packaged
            return False
        if dirname.lower().endswith("results"):
            return True
        return False

    def _package_file(self, filename):
        """Tells if we keep this file or not based on its name.

        Parameters
        ----------
        filename: str
            The name (basename) of the file.

        Returns
        -------
        bool: True if we package. False otherwise.
        """
        filename = filename.lower()
        for ending in __PACKAGE_FILE_ENDINGS__:
            if filename.endswith(ending):
                return True
        return False
