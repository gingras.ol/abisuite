.. _plotters_tutorial:


Plotters Tutorial
=================

.. contents::
    :local:

The :ref:`Plotters <plotters_api>` module contains classes to make plotting
much easier and more straightforward than directly using matplotlib
tools. It is not as versatile as matplotlib functions but can handle
most plot situations in a quicker way.
All utilities in ``abisuite`` makes use of this module.

The main reason why I develop these tools was to make it easy to save a
plot configuration and reload it later without having te regenerate all data
which can take long (especially on super clusters that have slow fiole systems
(yes I'm looking at you Beluga)!). Another nice usage is the implementation
of the ``__add__`` method which allows the addition of ``Plots``

Example: Make a plot with simple curves
---------------------------------------

Building the :class:`Plot <abisuite.plotters.plot.Plot>` object::

  from abisuite import Plot

  plot = Plot()
    
Use the :meth:`add_curve <abisuite.plotters.plot.Plot.add_curve>` method
to add curves::

   plot.add_curve(
           [1, 2, 3],       # x data
           [1, 4, 9],       # y data
           linestyle="--",
           linewidth=2,
           marker="s",
           markersize=5,
           markerfacecolor="c",
           color="c",
           label="label",   # label of curve if legend is activated
           semilogx=False,  # set to True for log x axis
           semilogy=False,  # set to True for log y axis
           loglog=False,    # set to True for log x and y axis
           twinx=False,     # set to True for a twinx curve (second y axis on the right)
           tag="tag",       # set to a tag to retrieve the curve using the tag in the curves list
           )

Adjust plot global parameters (see :class:`full Plot API <abisuite.plotters.plot.Plot>` for more options)::

  plot.xlims = [-1, 10]           # x axis limits
  plot.ylims = [-2, 8]            # y axis limits
  plot.ylims_twinx = [0, 10]      # twinx y axis limits (if relevant)
  plot.legend = True              # show legend
  plot.grid = True                # show grid
  plot.xlabel = "x label"         # x axis label
  plot.ylabel = "y label"         # y axis label 
  plot.ylabel_twinx = "y2 label"  # twinx y axis label (if relevant)
  # see full API for more options...

If a tag was used when creating the curve, it can be accessed later via
it's tag (or it's index)::

  curve = plot.curves["tag"]  # or curve = plot.curves[0]
  # now we can change the curve's properties
  curve.xdata = [0, 2, 9]
  curve.color = "g"

See full :class:`Curves <abisuite.plotters.curves.Curve>` API for more options.
To generate/show the plot, call the :meth:`plot <abisuite.plotters.plot.Plot.plot>` method::

  plot.plot(show=True)

To save plot (must be done after calling the :meth:`plot <abisuite.plotters.plot.Plot.plot>` function), call the
:meth:`save <abisuite.plotters.plot.Plot.save>` method::

  plot.save(
      "path/where/to/save/plot.pdf",
      overwrite=False,
      )

One can also save a *pickle* of the plot object which can be reloaded later
by using the :meth:`save_pickle <abisuite.plotters.plot.Plot.save_pickle>` method.
Thus, one can manipulate the plot later. This is really useful when a long
script was used to generate a plot. Then we don't need to rerun this script again
to modify the plot but just use :meth:`load_plot <abisuite.plotters.plot.Plot.load_plot>`
to unpickle the pickle::

  # save pickle
  plot.save_pickle(
      "path/where/to/save/plot.pickle",
      overwrite=False,
      )
   # load pickle (classmethod)
   plot = Plot.load_plot("path/where/to/save/plot.pickle")

Adding Plots
------------

Sometimes we just wish we could add plots to better compare the curves within
different plots. Well this can be done using this class! Plots can indeed be added.
The effect of this will be to just concatenate the
:attr:`curves <abisuite.plotters.plot.Plot.curves>` list of each individual
plots (or any other curve container). Some properties must be equal between plots
though::

  plot1 = Plot()
  plot1.add_curve([0, 1, 2], [0, 1, 2], color="k", label="plot1")
  
  plot2 = Plot()
  plot2.add_curve([0, 1, 2], [0, 1, 4], color="r", label="plot2")

  plot_combined = plot1 + plot2
  plot_combined.plot(show=True)

Multi Plots
-----------

Another method to compare plots is to put them side-by-side. This can be done
using a :class:`MultiPlot <abisuite.plotters.multiplot.MultiPlot>` object.
Create a :class:`MultiPlot <abisuite.plotters.multiplot.MultiPlot>` instance and, using the
:meth:`add_plot <abisuite.plotters.multiplot.MultiPlot.add_plot>`
method, add the :class:`Plot <abisuite.plotters.plot.Plot>` instances to the
:class:`MultiPlot <abisuite.plotters.multiplot.MultiPlot>` object. Then, one
can show the :class:`MultiPlot <abisuite.plotters.multiplot.MultiPlot>`
in the same way as a :class:`Plot <abisuite.plotters.plot.Plot>` instance::

  from abisuite import MultiPlot

  multi_plot = MultiPlot()
  multi_plot.add_plot(plot1, row=0)
  multi_plot.add_plot(plot2, row=0)
  multi_plot.plot(
      show=True,
      show_legend_on=[[0, 0]],  # this argument specifies which subplot has a legend
      )
  # the multiplot can also be saved/pickled as well
  multi_plot.save(
      "path/where/to/save/multiplot.pdf",
      overwrite=False,
      )
  multi_plot.save_pickle(
      "path/where/to/save/multiplot.pickle",
      overwrite=False,
      )
  # multi plot can also be unpickled
  multi_plot = MultiPlot.load_plot("path/where/to/save/multiplot.pickle")
