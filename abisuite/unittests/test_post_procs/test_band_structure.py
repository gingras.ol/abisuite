import os
import shutil

from .bases import BasePostProcTest
from ..bases import TestCase
from ..variables_for_tests import (
        QEPWBandCalc, QEPWBandCalclog,
        QEPWCalc)
from ...handlers import MetaDataFile, PBSFile
from ...post_processors import BandStructure


class TestBandStructure(BasePostProcTest, TestCase):
    """Test case for the Band Structure post processor object.
    """
    _post_proc_class = BandStructure
    _from_file = QEPWBandCalclog
    _from_calc = QEPWBandCalc

    def test_from_calculation(self):
        # need to modify meta files because meta file are LOCAL
        # do this in a temporary directory to not modify file inplace
        # need to copy PWCalc (GS calculation)
        pwcalc = shutil.copytree(QEPWCalc,
                                 os.path.join(self._link_tempdir.name,
                                              "PWCalc"),
                                 symlinks=True)
        # reset that meta data file
        with MetaDataFile.from_calculation(pwcalc) as meta:
            meta.calc_workdir = pwcalc
        # recast PBS file of that new dir
        with PBSFile.from_calculation(pwcalc, loglevel=1) as pbs:
            log = os.path.basename(pbs.log_path)
            pbs.log_path = os.path.join(self._link_tempdir.name,
                                        log)
        calc = shutil.copytree(self._from_calc, self._link_tempdir_path,
                               symlinks=True)
        # change meta file there
        with MetaDataFile.from_calculation(calc, loglevel=1) as meta:
            # rename all files from the temporary root
            meta.calc_workdir = calc
            # change path of parent to a the actual path
            meta.parents[0] = pwcalc
            # force write here to make sure all properties are changed
            # according to new calc workdir.
            meta.write(overwrite=True)
            # recast pbs file of moved directory
            with PBSFile.from_meta_data_file(meta, loglevel=1) as pbs:
                pbs.log_path = os.path.join(meta.calc_workdir,
                                            os.path.basename(pbs.log_path))
        super().test_from_calculation()

    def test_from_file(self):
        # just check that it works and doesn't raise an error
        # from a QE bandstructure calculation
        self.postproc = BandStructure.from_qelog(QEPWBandCalclog)
        self.postproc.get_plot(all_k_labels="none")
