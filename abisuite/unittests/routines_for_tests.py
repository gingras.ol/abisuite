import os
import shutil
import tempfile

from abisuite.handlers import (
        AbinitFilesFile, MetaDataFile, PBSFile, CALCTYPES_TO_INPUT_FILE_CLS,
        )
from ..databases import AbiDB
from ..exceptions import DevError
from ..routines import full_abspath, is_list_like


def copy_calculation(
        calc, whereto, new_parents=None, new_pseudos=None, **kwargs):
    """copy a calculation from one point to another
    only use this function for unittests!!!!
    need to modify meta files because meta file are LOCAL

    Parameters
    ----------
    calc: str
        The path to the calculation to copy.
    whereto: str
        The destination where the calculation will be copied.
    new_parents: list, optional
        The list of new parents. Useful if parents of calculation
        would be copied as well with this function.
    new_pseudos: list, optional
        Only used for abinit calculations. The list of new pseudos path.
    other kwargs: key=value pairs where key is an input variable name and
        value its new value.
    """
    # copy tree
    # hard copy files instead of links
    print(f"TESTCOPY: {calc} -> {whereto}")
    calc = shutil.copytree(calc, whereto, symlinks=False)
    # change meta file there
    with MetaDataFile.from_calculation(calc) as meta:
        meta.calc_workdir = calc
        meta.children = []
        if new_parents is not None:
            if not is_list_like(new_parents):
                raise DevError("'new_parents' must be a list.")
            meta.parents = new_parents
        # the following write is necessary as the meta file is read to give
        # path to the pbs file. then the pbs file is modified. we need the
        # new copied pbs file not the original one
        meta.write(overwrite=True)
        with PBSFile.from_calculation(calc) as pbs:
            # fix log file path for future
            pbs.log_path = os.path.join(
                    meta.calc_workdir,
                    os.path.basename(pbs.log_path))
            # also the input file
            pbs.input_file_path = os.path.join(
                    meta.calc_workdir,
                    os.path.basename(pbs.input_file_path))
        # if qe, we need to change outdir variable if relevant
        if meta.calctype.startswith("qe"):
            with CALCTYPES_TO_INPUT_FILE_CLS[
                    meta.calctype].from_meta_data_file(meta) as inp:
                if "outdir" in inp.input_variables:
                    inp.input_variables["outdir"] = meta.output_data_dir
                # change kwargs
                for varname, varvalue in kwargs.items():
                    inp.input_variables[varname] = varvalue
                if meta.calctype == "qe_ph":
                    # need to modify the 'fildyn' variable
                    fildyn = os.path.basename(
                            inp.input_variables["fildyn"].value)
                    new_fildyn = os.path.join(meta.output_data_dir, fildyn)
                    inp.input_variables["fildyn"] = new_fildyn
                elif meta.calctype == "qe_q2r":
                    # need to modify the 'flfrc' and 'fildyn' variables
                    for varname in ("fildyn", "flfrc"):
                        old = os.path.basename(
                                inp.input_variables[varname].value)
                        new = os.path.join(meta.output_data_dir, old)
                        inp.input_variables[varname] = new
                elif meta.calctype == "qe_matdyn":
                    # also need to modify 'flfrc', 'flvec', 'fleig' and 'flfrq'
                    for varname in ("flfrc", ):
                        old = os.path.basename(
                                inp.input_variables[varname].value)
                        new = os.path.join(meta.input_data_dir, old)
                        inp.input_variables[varname] = new
                    for varname in ("fleig", "flvec",  "flfrq", ):
                        if varname not in inp.input_variables:
                            continue
                        old = os.path.basename(
                                inp.input_variables[varname].value)
                        new = os.path.join(meta.output_data_dir, old)
                        inp.input_variables[varname] = new
                elif meta.calctype == "qe_epw":
                    new = os.path.join(meta.input_data_dir) + "/"
                    inp.input_variables["dvscf_dir"] = new
        elif meta.calctype == "abinit":
            # need to reset pseudos in files file
            if new_pseudos is None:
                raise ValueError("Need to set 'new_pseudos'.")
            with AbinitFilesFile.from_calculation(meta.calc_workdir) as files:
                files.pseudos = new_pseudos
        elif meta.calctype == "abinit_optic":
            # need to change the ddkfiles input variables
            with CALCTYPES_TO_INPUT_FILE_CLS[
                    meta.calctype].from_meta_data_file(meta) as inf:
                for varname in ("ddkfile_1", "ddkfile_2",
                                "ddkfile_3", "wfkfile"):
                    # these files are located in the input data dir
                    inf.input_variables[varname] = os.path.join(
                            meta.input_data_dir,
                            os.path.basename(
                                inf.input_variables[varname].value))
        elif meta.calctype == "abinit_mrgddb":
            # need to modify all input file entries
            with CALCTYPES_TO_INPUT_FILE_CLS[
                    meta.calctype].from_meta_data_file(meta) as inf:
                # first is main output file
                inf.output_file_path = os.path.join(
                        meta.output_data_dir,
                        os.path.basename(inf.output_file_path))
                new_ddbs = []
                for ddb in inf.ddb_paths:
                    new_ddbs.append(
                            os.path.join(meta.input_data_dir,
                                         os.path.basename(ddb)))
                inf.ddb_paths = new_ddbs


def create_pbs_test_writer(pbs_writer):
    for prop in pbs_writer.structure.all_attributes:
        # these properties are set manually
        if prop in ("nodes", "ppn", "total_ncpus", "queuing_system", "ntasks",
                    "memory", "memory_per_cpu", ):
            continue
        if prop in pbs_writer.structure.set_attributes:
            setattr(pbs_writer, prop, set())
            continue
        if prop == "modules_to_swap":
            pbs_writer.modules_to_swap = [["module1", "module2"]]
            continue
        if prop in pbs_writer.structure.list_attributes:
            setattr(pbs_writer, prop, ["module"])
            continue
        if prop in pbs_writer.structure.optional_attributes:
            setattr(pbs_writer, prop, prop)
        # if attribute has a default dont overwrite it
        try:
            getattr(pbs_writer, prop)
        except ValueError:
            setattr(pbs_writer, prop, prop)
    pbs_writer.nodes = 1  # cannot just set anything for these
    pbs_writer.ppn = 1
    pbs_writer.cpus_per_task = None
    pbs_writer.memory = "1M"
    pbs_writer.queuing_system = "local"  # default test
    pbs_writer.quality_of_service = "quality_of_service"
    pbs_writer.queue = "queue"


class TemporaryCalculation:
    """Class that transforms a real calculation to a temporary one in a
    temporary directory.
    """
    def __init__(
            self, realcalc, copy_tempdir=None, copy_on_creation=True,
            connect_to_temporary_database=False,
            new_pseudos=None, new_parents=None):
        """TemporaryCalculation's init method.

        Parameters
        ----------
        realcalc: str
            The path to the actual calculation.
        copy_on_creation: bool, optional
            If True, the real calculation is copied upon init.
        copy_tempdir: tempfile.TemporaryCalculation object
            If not None, this specifies where the calculation will be copied.
        connect_to_temporary_database: bool, optional
            If True, a temporary database is created, and the copied
            calculation is added to it.
        new_pseudos: list-like, optional
            The list of new pseudos path.
        new_parents: list-like, optional
            If the copied calculation has parents, also point t
        """
        realcalc = full_abspath(realcalc)
        if not os.path.isdir(realcalc):
            raise NotADirectoryError(realcalc)
        self.real_calculation = realcalc
        if copy_tempdir is None:
            self._tempdir = tempfile.TemporaryDirectory()
            self._clean_tempdir_on_cleanup = True
        else:
            if not isinstance(copy_tempdir, tempfile.TemporaryDirectory):
                raise TypeError(
                        "Expected 'tempfile.TemporaryDirectory' object but "
                        f"got: '{copy_tempdir}'.")
            self._tempdir = copy_tempdir
            self._clean_tempdir_on_cleanup = False
        self.new_pseudos = new_pseudos
        self.new_parents = new_parents
        self.path = os.path.join(
                self._tempdir.name, os.path.basename(realcalc))
        self.connect_to_temporary_database = connect_to_temporary_database
        self.temporary_database = None
        if copy_on_creation:
            self.copy_calculation()

    def __del__(self):
        # if not already done, cleanup
        self.cleanup()

    def cleanup(self):
        """Cleanup the temporary directory if it exists.
        """
        if not os.path.exists(self._tempdir.name):
            # nothing to do
            return
        if self._clean_tempdir_on_cleanup:
            self._tempdir.cleanup()
        else:
            # just rmtree where it was copied
            shutil.rmtree(self.path)
        if self.connect_to_temporary_database and (
                self.temporary_database is not None):
            if not os.path.exists(self.temporary_database.db):
                return
            os.remove(self.temporary_database.db)

    def copy_calculation(self, **kwargs):
        """Copies the temporary calculation to its temp location.

        Parameters
        ----------
        All kwargs are passed to the `copy_calculation` function.
        """
        copy_calculation(
                self.real_calculation, self.path,
                new_pseudos=self.new_pseudos,
                new_parents=self.new_parents, **kwargs)
        if self.connect_to_temporary_database:
            self.temporary_database = AbiDB()
            self.temporary_database.create_database(
                    os.path.join(self._tempdir.name, "test.db"))
            self.temporary_database.add_calculation(self.path)
