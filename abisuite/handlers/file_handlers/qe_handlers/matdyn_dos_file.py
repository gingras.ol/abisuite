from ..bases import BaseFileHandler
from ...file_parsers import QEMatdynDOSParser
from ...file_structures import QEMatdynDOSStructure


class QEMatdynDOSFile(BaseFileHandler):
    """Class that represents a dos file produced by the matdyn.x script
    of Quantum Espresso.
    """
    _loggername = "QEMatdynDOSFile"
    _parser_class = QEMatdynDOSParser
    _structure_class = QEMatdynDOSStructure
