import logging
import os

from .bases import (
        BaseDirectoryHandler, BaseWritableDirectoryHandler, DirectoryContent,
        )
from .generic_directory import GenericDirectory
from .input_data_directory import InputDataDirectory
from .run_directory import RunDirectory
from ..file_handlers import (
        MetaDataFile, StderrFile, SymLinkFile, CALCTYPES_TO_LOG_FILE_CLS,
        CALCTYPES_TO_INPUT_FILE_CLS
        )
from ..file_handlers.bases import BaseInputFileHandlerNoInputVariables
from ... import USER_CONFIG
from ...cleaners import __CALCTYPES_TO_CLEANER_CLS__
from ...packagers import __CALCTYPES_TO_PACKAGER_CLS__
from ...routines import full_abspath
from ...status_checkers import CALCTYPES_TO_STATUS_CHECKER_CLS


class CalculationDirectory(BaseWritableDirectoryHandler):
    """A calculation directory is a directory that contains:

        - input data sub directory
        - output data directory
        - run data directory
        - input file and meta data file
        - batch script file (in RunDirectory)
    """
    _loggername = "CalculationDirectory"
    _input_data_dir_name = "input_data"
    _run_dir_name = "run"

    def __init__(self, input_file_cls, *args, **kwargs):
        """Calculation Directory init method.

        Parameters
        ----------
        input_file_cls : class for the input file.
        """
        if not issubclass(
                input_file_cls, BaseInputFileHandlerNoInputVariables):
            raise ValueError("Need an input file class but got: "
                             f"{input_file_cls}")
        super().__init__(*args, **kwargs)
        self.run_directory = RunDirectory(loglevel=self._loglevel)
        self.input_data_directory = InputDataDirectory(loglevel=self._loglevel)
        self.meta_data_file = MetaDataFile(loglevel=self._loglevel)
        self.input_file = input_file_cls(loglevel=self._loglevel)
        self._database = None
        self._log_file = None
        self._stderr_file = None
        self._status_checker = None
        self._cleaner = None
        self._packager = None

    @property
    def calctype(self):
        with self.meta_data_file as meta:
            return meta.calctype

    @calctype.setter
    def calctype(self, calctype):
        self.meta_data_file.calctype = calctype

    @property
    def cleaner(self):
        if self._cleaner is not None:
            return self._cleaner
        try:
            cleaner_cls = __CALCTYPES_TO_CLEANER_CLS__[self.calctype]
        except KeyError:
            raise NotImplementedError(
                    f"Cleaner not implemented for {self.calctype}")
        self._cleaner = cleaner_cls(loglevel=self._loglevel)
        self._cleaner.calculation_directory = self
        return self.cleaner

    @property
    def content(self):
        content = super().content
        extra = DirectoryContent(loglevel=self._loglevel)
        extra.append(self.input_data_directory)
        extra.append(self.run_directory)
        extra.append(self.meta_data_file)
        extra.append(self.input_file)
        if self._log_file is not None:
            extra.append(self.log_file)
        if self._stderr_file is not None:
            extra.append(self.stderr_file)
        return content + extra

    @content.deleter
    def content(self):
        BaseWritableDirectoryHandler.content.fdel(self)

    @property
    def database(self):
        if self._database is None:
            raise RuntimeError("Need to connect to a database first.")
        return self._database

    @property
    def is_connected_to_database(self):
        return self._database is not None

    @property
    def is_in_database(self):
        if not self.is_connected_to_database:
            raise RuntimeError("Need to connect to database first!")
        return self.path in self._database

    @property
    def is_packaged(self):
        if not self.is_connected_to_database:
            raise RuntimeError("Need to connect to database first!")
        if not self.is_in_database:
            return False
        return self._database.get_packaged(self.path)

    @property
    def jobname(self):
        with self.meta_data_file as meta:
            return meta.jobname

    @jobname.setter
    def jobname(self, jobname):
        with self.meta_data_file as meta:
            meta.jobname = jobname
        with self.pbs_file as pbs:
            pbs.jobname = jobname

    @property
    def log_file(self):
        if self._log_file is not None:
            return self._log_file
        # need to get log file
        logcls = CALCTYPES_TO_LOG_FILE_CLS[self.calctype]
        # the class method will do the init
        self._logger.debug(f"Getting log object for calc: '{self.path}'.")
        self._log_file = logcls.from_meta_data_file(
                self.meta_data_file, loglevel=self._loglevel
                )
        return self.log_file

    # some aliases
    @property
    def output_data_directory(self):
        return self.run_directory.output_data_directory

    @property
    def packager(self):
        if self._packager is not None:
            return self._packager
        try:
            packager_cls = __CALCTYPES_TO_PACKAGER_CLS__[self.calctype]
        except KeyError:
            raise NotImplementedError(
                    f"Packager not implemented for {self.calctype}")
        self._packager = packager_cls(loglevel=self._loglevel)
        self._packager.calculation_directory = self
        return self.packager

    @property
    def parents(self):
        with self.meta_data_file as meta:
            return meta.parents

    @property
    def path(self):
        return super().path

    @path.setter
    def path(self, path):
        BaseWritableDirectoryHandler.path.fset(self, path)
        # then set the meta data file with all the paths
        self.meta_data_file.calc_workdir = self.path
        self.run_directory.path = os.path.join(self.path, self._run_dir_name)
        self.input_data_directory.path = os.path.join(
                self.path, self._input_data_dir_name
                )
        for item in (self.input_file, self.meta_data_file):
            self._reset_special_subfile_path(item)
        self.meta_data_file.input_data_dir = self.input_data_directory.path
        self.meta_data_file.rundir = self.run_directory.path
        odd = self.run_directory.output_data_directory.path  # alias
        self.meta_data_file.output_data_dir = odd
        if self.meta_data_file.exists:
            if not self.meta_data_file.has_been_read:
                self.meta_data_file.read()
            # set input file and pbs file path already
            self.input_file.path = self.meta_data_file.input_file_path
            self.pbs_file.path = self.meta_data_file.pbs_file_path

    @property
    def pbs_file(self):
        # read run directory before to make sure pbs_file exists
        # NOTE: don't do this as it is allowed to have a pbs file object
        # without having a real file existing.
        # if self.run_directory.exists:
        #     self.run_directory.read()
        return self.run_directory.pbs_file

    @pbs_file.setter
    def pbs_file(self, pbs_file):
        self.run_directory.pbs_file = pbs_file

    @property
    def status(self):
        if not self.is_connected_to_database:
            self.connect_to_database()
        from ...databases.exceptions import CalculationDoesNotExistError
        try:
            return self.status_checker.calculation_status
        except CalculationDoesNotExistError:
            self.add_to_database()
            return self.status_checker.calculation_status

    @property
    def status_checker(self):
        if self._status_checker is not None:
            return self._status_checker
        try:
            stat_cls = CALCTYPES_TO_STATUS_CHECKER_CLS[self.calctype]
        except KeyError:
            raise NotImplementedError(f"Status checker not implemented for "
                                      f"{self.calctype}")
        self._status_checker = stat_cls(loglevel=self._loglevel)
        self._status_checker.calculation_directory = self
        return self.status_checker

    @property
    def stderr_file(self):
        if self._stderr_file is not None:
            return self._stderr_file
        # the class method will do the init
        stderr_file = StderrFile(loglevel=self._loglevel)
        # .from_meta_data_file(
        #         self.meta_data_file, loglevel=self._loglevel
        #         )
        try:
            if self.pbs_file.exists:
                self.pbs_file.read()
            stderr_file.path = self.pbs_file.stderr_path
        except ValueError:
            # stderr file path not set in pbs file yet
            pass
        try:
            if self.meta_data_file.exists:
                self.meta_data_file.read()
            stderr_file.path = os.path.join(
                        self.path,
                        self.meta_data_file.jobname + ".stderr")
        except ValueError:
            # jobname not defined
            raise FileNotFoundError("Cannot instanciate stderr file.")
        self._stderr_file = stderr_file
        return self._stderr_file

    @property
    def walltime(self):
        """The walltime of the calculation as stored in the database.
        If not connected to database or not in a database, the calculation
        directory is connected and/or added to database.
        """
        if not self.is_connected_to_database:
            self.connect_to_database()
        # FIXME: for some reason, this does not work all the time
        # and thus we have to resort to try except stmt
        # if not self.is_in_database:
        #     self.add_to_database()
        from ...databases.exceptions import CalculationDoesNotExistError
        try:
            return self._database.get_walltime(self, refresh=True)
        except CalculationDoesNotExistError:
            self._logger.info(f"Adding calculation to db: '{self.path}'.")
            self.add_to_database()
            return self._database.get_walltime(self, refresh=True)

    def add_copied_file(self, path, source, *args, **kwargs):
        """Add a file to copy inside calculation diectory.

        Parameters
        ----------
        path: str
              The path of the new file to copy. Path must start with the
              input data directory path.
        source : str
                 The path to the file to copy.
        """
        super().add_copied_file(path, source, *args, **kwargs)
        # also add the path to the meta data file
        self.meta_data_file.add_copied_file(source)

    def add_symlink(self, path, source, *args, **kwargs):
        """Add a symlink into the directory.

        Parameters
        ----------
        path: str
              The path of the new file to symlink.
        source : str
                 The path to the file to link to.
        """
        # add the synlinked file to the meta data file
        super().add_symlink(path, source, *args, **kwargs)
        self.meta_data_file.add_linked_file(source)

    def add_to_database(self):
        """Add calculation to the connected database.
        """
        if self._database is None:
            raise RuntimeError(
                    "Connect to database first.")
        self._database.add_calculation(self)

    def clean(self, *args, **kwargs):
        """Uses the cleaner object adapted for the given 'calctype'.
        All arguments are passed to the corresponding 'clean' method.
        """
        self.cleaner.clean(*args, **kwargs)

    def clear_content(self):
        super().clear_content()
        self.run_directory.clear_content()
        self.input_data_directory.clear_content()

    def copy_handler(self):
        new = super().copy_handler()
        new.meta_data_file = self.meta_data_file.copy_handler()
        new.run_directory = self.run_directory.copy_handler()
        new.input_file = self.input_file.copy_handler()
        new.input_data_directory = self.input_data_directory.copy_handler()
        if self._log_file is not None:
            new._log_file = self.log_file.copy_handler()
        if self._stderr_file is not None:
            with self.stderr_file:
                new._stderr_file = self.stderr_file.copy_handler()
        return new

    def connect_to_database(self, database=None):
        """Connect this calculation handler to a database.

        Parameters
        ----------
        database : str, optional
            The path to the database. If None or 'default', the default
            database path specified in the user config file is taken.
        """
        from ...databases import AbiDB
        if isinstance(database, AbiDB):
            self._database = database
        else:
            if database == "default" or database is None:
                database = USER_CONFIG.DATABASE.path
            if database is None:
                self._logger.warning("No database to connect to.")
                return
            if not isinstance(database, str):
                raise TypeError(database)
            self._database = AbiDB.from_file(database, loglevel=self._loglevel)
        if not self.is_in_database and self.exists:
            self.add_to_database()

    def move(self, newpath, *args, **kwargs):
        # call the base class's method and change the content of the meta
        # data file if needed
        # check if calculation was in a connected database.
        oldpath = self.path
        if self.is_connected_to_database:
            db_path = self._database.db
            if db_path in self.walk(paths_only=True, allow_read_content=True):
                # if db inside moving directory, we won't be able to update it
                # TODO: maybe there is a way to do it, just change the path
                # of the db object
                raise FileExistsError(
                    f"DB: '{db_path}' inside moving directory '{self.path}'.")
            self.update_database_calculation(newpath, force=True)
        super().move(newpath, *args, **kwargs)
        # make subdirs to reread themselve cause we looped over the walk() meth
        # for subdir in (self.run_directory, self.input_data_directory):
        #     subdir.path = self.path + os.path.basename(subdir.path)
        #     subdir.read()
        self.update_new_meta_data_file(newpath, oldpath)

    def package(self, *args, **kwargs):
        """Packages results from this calculation directory.
        """
        self.packager.package(*args, **kwargs)

    def remove_from_database(self):
        """Remove the calculation from the database.
        """
        if self.is_in_database:
            self._database.delete_calculation(self.path)
        else:
            raise RuntimeError(
                    "Cannot delete calculation from database since it is "
                    "not in it.")

    def reset_status(self):
        """Resets the calculation status in order to recompute. Useful after
        relaunching for instance.

        Also reset walltime while we're here.
        """
        if self.is_in_database:
            self._database.update_calculation_status(
                    self.path,
                    new_status=self.status_checker.get_initial_status_dict())
            self._database.update_walltime(
                    self.path, new_walltime=None, force=True)
        # else, nothing else to do

    def update_database_calculation(self, new_path, **kwargs):
        """Updates the database entry for the path of this calculation.

        Parameters
        ----------
        new_path: str
            The new path of the databse entry for this calculation.

        Other Parameters
        ----------------
        other kwargs are passed to the database method:
            :func:`~abisuite.databases.abi_db.AbiDB.update_calculation`.
        """
        if not self.is_in_database:
            raise RuntimeError("Need to add calculation to database first.")
        self._logger.info("Updating database calculation path entry.")
        id_ = self._database[self.path]
        self._database.update_calculation(id_, new_path, **kwargs)

    def update_database_monitored(self, monitored):
        """Sets the monitored status to the given value.

        Parameters
        ----------
        monitored: bool
            The monitored status to set to.
        """
        self._update_database("monitored", monitored)

    def update_database_packaged(self, packaged):
        """Sets the pacakged tag to the given value in the database.

        Parameters
        ----------
        packaged: str
            The new packaged tag.
        """
        self._update_database("packaged", packaged)

    def update_database_project(self, project):
        """Sets the project tag to the given value in the database.

        Parameters
        ----------
        project: str
            The project tag.
        """
        self._update_database("project", project)

    def update_database_status(self):
        """Update the calculation status in the database.
        """
        self._update_database("status", self.status)

    def update_new_meta_data_file(self, newpath, oldpath):
        """Updates the content of a new meta data file located
        at destination after moving or packaging.

        Parameters
        ----------
        newpath: str
            The path where the new calculation is stored.
        oldpath: str
            The path where the old calculation was stored.
        """
        with MetaDataFile.from_calculation(
                newpath, loglevel=self._loglevel) as meta:
            to_remove = []
            for child in meta.children:
                # need to change the parent's location for these children
                if not self.is_calculation_directory(child):
                    # it was removed at some point
                    to_remove.append(child)
                    continue
                with MetaDataFile.from_calculation(
                        child, loglevel=self._loglevel) as childmeta:
                    if oldpath not in childmeta.parents:
                        self._logger.warning(
                                "Expected to change a dir location in "
                                " children of newly packaged/moved dir. But "
                                "calc was not found in parents of children: "
                                f"'{child}'.")
                        continue
                    childmeta.parents.remove(oldpath)
                    childmeta.parents.append(newpath)
            for child in to_remove:
                meta.children.pop(meta.children.index(child))
            # change parent calculation too
            for parent in meta.parents:
                with MetaDataFile.from_calculation(
                        parent, loglevel=self._loglevel) as parentmeta:
                    if oldpath not in parentmeta.children:
                        self._logger.warning(
                                "Expected to change a dir location in parents"
                                " of newly packaged/moved dir. "
                                "But old calc was not found in children of "
                                f"parent: '{parent}'.")
                        continue
                    parentmeta.children.remove(oldpath)
                    parentmeta.children.append(newpath)

    def _update_database(self, attribute, value):
        # actually updates the property attribute of the database.
        if not self.is_in_database:
            raise RuntimeError("Need to add calculation to database first.")
        getattr(self._database, f"update_calculation_{attribute}")(
                self.path, value)

    def write_content(self, *args, **kwargs):
        """Write content of directory.
        """
        # if log/stderr file is not None, files should not be written
        if self._log_file is not None:
            if self.log_file.exists:
                self._logger.error(
                        "Log file exists. Should not write a directory")
                raise FileExistsError(self.log_file.path)
        if self._stderr_file is not None:
            if self.stderr_file.exists:
                self._logger.error(
                    "Stderr file exists. Should not write a directory")
                raise FileExistsError(self.stderr_file.path)
        if not self.meta_data_file.path.startswith(self.path):
            raise ValueError(f"meta data file not in calculation dir: "
                             f"{self.meta_data_file.path}")
        if not self.input_file.path.startswith(self.path):
            raise ValueError(f"Input file not in calculation dir: "
                             f"{self.input_file.path}")
        # set some data in meta data
        self.meta_data_file.input_file_path = self.input_file.path
        sifp = self.pbs_file.input_file_path  # alias
        self.meta_data_file.script_input_file_path = sifp
        self.meta_data_file.pbs_file_path = self.pbs_file.path
        super().write_content(*args, **kwargs)

    @classmethod
    def from_calculation(cls, path, *args, **kwargs):
        """Create a CalculationDirectory object from an already existing
        calculation directory only from it's path.
        """
        if isinstance(path, cls):
            # if already a calc dir, return it
            return path
        loglevel = kwargs.get("loglevel", logging.INFO)
        with MetaDataFile.from_calculation(path, loglevel=loglevel) as meta:
            return cls.from_meta_data_file(meta, *args, **kwargs)

    @classmethod
    def from_meta_data_file(cls, meta, *args, **kwargs):
        """Create a Calculation Directory object from a MetaDataFile object.
        """
        ifcls = CALCTYPES_TO_INPUT_FILE_CLS[meta.calctype]
        cd = cls(ifcls, *args, **kwargs)
        # set meta data file first since setting the calc path will need to
        # know the current location of the meta data file.
        cd.meta_data_file.path = meta.path
        cd.path = meta.calc_workdir
        # cd.read()
        return cd

    @staticmethod
    def is_calculation_directory(directory, loglevel=None):
        """Returns True if directory is a calculation directory.
        Returns False otherwise.

        What this method do: try to create a calculation directory
        and tries to read it. If somethings wrong, aborts and returns False.
        """
        logger = None
        calc_loglevel = logging.INFO
        if loglevel is not None:
            logging.basicConfig()
            logger = logging.getLogger(CalculationDirectory._loggername)
            logger.setLevel(loglevel)
            calc_loglevel = loglevel
            logger.debug(f"Checking if {directory} is a calc dir.")
        directory = full_abspath(directory)
        try:
            if logger is not None:
                logger.debug(
                    f"Try creating calc dir: {directory}")
            calc = CalculationDirectory.from_calculation(
                    directory, loglevel=calc_loglevel)
            try:
                # check that all import file exists
                if logger is not None:
                    logger.debug(
                        f"Checking for input file at {calc.input_file.path}")
                if not os.path.isfile(calc.input_file.path):
                    if logger is not None:
                        logger.debug(
                            f"No input file found at: {calc.input_file.path}")
                    return False
                if logger is not None:
                    logger.debug(
                        f"Checking for meta file at "
                        f"{calc.meta_data_file.path}")
                if not os.path.isfile(calc.meta_data_file.path):
                    if logger is not None:
                        logger.debug(
                            f"No meta file found at: "
                            f"{calc.meta_data_file.path}")
                    return False
                # FG: 2021/05/12 I disabled the pbs file check since
                # we can remove or modify this file without taking out
                # any actual calculation properties. Also, I am trying to
                # make the calc dir more flexible by not necessarily relying
                # onto the pbs file and more on the meta data file.
                # if logger is not None:
                #     logger.debug(
                #         f"Checking for pbs file at "
                #         f"{calc.pbs_file.path}")
                # if not os.path.isfile(calc.pbs_file.path):
                #     if logger is not None:
                #         logger.debug(
                #             f"No pbs file found at: {calc.pbs_file.path}")
                #     return False
                # NOTE: had problem with this check as wannier90 links
                # files directly in workdir this input data dir is empty
                # even though it has linked/copied files
                # if logger is not None:
                #     logger.debug(
                #         f"Checking for input data dir at "
                #         f"{calc.input_data_directory.path}")
                # if not os.path.isdir(calc.input_data_directory.path):
                #     # it's ok to lack a run directory if there's nothing
                #     # linked to the calculation
                #     linked_files = calc.meta_data_file.linked_files
                #     copied_files = calc.meta_data_file.copied_files
                #     if linked_files or copied_files:
                #         if logger is not None:
                #             logger.debug(
                #                 f"No input data dir found at: "
                #                 f"{calc.input_data_directory.path}")
                #         return False
                if logger is not None:
                    logger.debug(
                        f"Checking for run directory at "
                        f"{calc.run_directory.path}")
                if not os.path.isdir(calc.run_directory.path):
                    if logger is not None:
                        logger.debug(
                            f"No run dir found at: {calc.run_directory.path}")
                    return False
            except ValueError:
                # some paths not set, return False
                if logger is not None:
                    logger.debug(
                        "Some paths not set.")
                return False
        except (NotADirectoryError, FileNotFoundError):
            # if any error occurs, just return False
            if logger is not None:
                logger.debug(
                    "An error occured.")
            return False
        # if everything works, return True
        if logger is not None:
            logger.debug(
                    f"Is a calc dir: {directory}")
        return True

    def _add_file_to_content(self, content, path):
        # if an input file, add as the input file
        if path.endswith(self.input_file._parser_class._expected_ending):
            self.input_file.path = path
            # self.input_file.read()
            return
        # do samething for meta data file
        elif path.endswith(
                self.meta_data_file._parser_class._expected_ending
                ):
            if self.meta_data_file._path is not None:
                # file already set, don't reread it
                # set because log file was found first
                return
            self._logger.debug(f"Found meta data file: {path}")
            self.meta_data_file.path = path
            # self.meta_data_file.read()
            return

        def create_meta():
            # this temporary method creates a meta data file if needed
            if self.meta_data_file._path is None:
                # set meta file if not already done
                self.meta_data_file = MetaDataFile.from_calculation(
                        self.path, loglevel=self._loglevel)
                self.meta_data_file.read()

        # samething for stderr file
        if path.endswith(
                StderrFile._parser_class._expected_ending):
            # if meta data file was not created yet, create it
            create_meta()
            self.stderr_file.path = path
            return
        # TODO: find a better way to dothis
        # proposition: first get meta file when reading. then gather input and
        # log files if they exist. Then get all other files.
        elif path.endswith("log") or path.endswith("wout"):
            # MOST PROBABLY A LOG FILE, therefore, meta data file must already
            # be written prior to the read. if its not the case everything will
            # break down anyway.
            create_meta()
            # call log_file to set it and continue
            self.log_file
            # just a check, if log file path is different from this one, append
            # it as a regular file
            if self.log_file.path != path:
                super()._add_file_to_content(content, path)
            return
        else:
            super()._add_file_to_content(content, path)

    def _add_subdir_to_content(self, content, path):
        if path.endswith(self._run_dir_name):
            self.run_directory.path = path
            # self.run_directory.read()
            return
        elif path.endswith(self._input_data_dir_name):
            self.input_data_directory.path = path
            # self.input_data_directory.read()
            return
        subdir = GenericDirectory(loglevel=self._loglevel)
        subdir.path = path
        # subdir.read()
        content.append(subdir)

    def _get_new_instance(self):
        return self.__class__(self.input_file.__class__,
                              loglevel=self._loglevel)

    def _post_file_move(self, oldpath, newpath):
        # after moving a file because directory is moving, make checks
        # in all children to see if we need to change links
        for child in self.meta_data_file.children:
            with MetaDataFile.from_calculation(
                    child, loglevel=self._loglevel) as child_meta:
                if oldpath in child_meta.copied_files:
                    # just change the reference and continue
                    child_meta.copied_files.remove(oldpath)
                    child_meta.copied_files.append(newpath)
                    continue
                if oldpath not in child_meta.linked_files:
                    continue
                # moved file was a target of another calculation. change this
                # other calculation to make sure all links stays the good
                # first we change the file in meta data
                child_meta.linked_files.remove(oldpath)
                child_meta.linked_files.append(newpath)
                # then parse the whole calculation to find the corresonding
                # links and change them
                with CalculationDirectory.from_calculation(
                        child, loglevel=self._loglevel) as calc:
                    for path in calc.walk():
                        if not os.path.islink(path):
                            continue
                        target = os.readlink(path)
                        if oldpath not in target:
                            continue
                        # ok we found a file pointing at the moved file.
                        # change it now
                        sym = SymLinkFile(loglevel=self._loglevel)
                        sym.path = path
                        sym.source = newpath
                        sym.write(overwrite=True)
                        # continue to loop just in case there are other files
                        # pointing at the same thing


class CalculationTree(BaseDirectoryHandler):
    """Class that represents a tree of directories which contain calculations.
    """
    _loggername = "CalculationTree"

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.calculations = []

    def __add__(self, calctree):
        # defines the addition between 2 calc trees
        # just return the concatenated list of the calculations
        self.calculations += calctree.calculations
        return self

    def __len__(self):
        return len(self.calculations)

    @property
    def status(self):
        # return the list of status of all calculations
        return [calc["calculation_directory"].status
                for calc in self.calculations]

    def build_tree(self, follow_symlinks=True):
        """Build the calculation tree. Fills the 'calculations' attribute
        of the tree.

        The calculations attribute is a list of dict containing the
        CalculationDirectory object, the path in the tree and a flag to
        tell if it is a symlink. We do this way to handle symlinked calcdir.

        Parameters
        ----------
        follow_symlinks: bool, optional
            If True, symlinks are followed and the calculation it points to
            is added to the tree. Otherwise, symlinks are ignored.
        """
        self.read()
        if CalculationDirectory.is_calculation_directory(self.path):
            if os.path.islink(self.path):
                if not follow_symlinks:
                    return
                path = os.readlink(self.path)
                is_symlink = True
            else:
                path = self.path
                is_symlink = False
            calc = CalculationDirectory.from_calculation(
                            path, loglevel=self._loglevel)
            self.calculations.append(
                    {"calculation_directory": calc,
                     "path": self.path,
                     "is_symlink": is_symlink,
                     })
            return

        def analyse_item(item):
            # returns True if we analyse item. otherwise return false
            for calculation in self.calculations:
                # make sure training slash is present by joining nothing
                if item.path.startswith(os.path.join(calculation["path"], "")):
                    return False
            return True

        for item in self:
            # check if a calculation path is already contained in the path
            # if so don't bother checking the item
            if not analyse_item(item):
                continue
            if isinstance(item, CalculationDirectory):
                self.calculations.append(
                        {"calculation_directory": item,
                         "is_symlink": False,
                         "path": item.path,
                         })
                continue
            elif isinstance(item, SymLinkFile) and follow_symlinks:
                try:
                    with item:
                        if CalculationDirectory.is_calculation_directory(
                                item.source):
                            calc = CalculationDirectory.from_calculation(
                                    item.source, loglevel=self._loglevel)
                            self.calculations.append({
                                "calculation_directory": calc,
                                "is_symlink": True,
                                "path": item.path,  # path is the symlink path
                                }
                                )
                except ValueError:
                    # broken symlink we don't use for building a tree but still
                    # log in debug
                    self._logger.debug(f"Symlink '{item.path}' broken?")
                    continue
            elif isinstance(item, CalculationTree):
                item.build_tree(follow_symlinks=follow_symlinks)
                self.calculations += item.calculations  # concatenate trees

    def sort_tree(self):
        """Sort calculation tree according to calculation name.
        """
        if not len(self.calculations):
            # trivial sorting haha
            return
            # raise RuntimeError("Calculation tree not built yet.")
        # sort calculations within each subdirs separately
        # first build calcs_per_subdir blocks
        block_names = []
        for calc in self.calculations:
            dirname = os.path.dirname(calc["path"])
            if dirname not in block_names:
                block_names.append(dirname)
        blocks = {}
        for block_name in block_names:
            blocks.setdefault(block_name, [])
            for calc in self.calculations:
                if os.path.dirname(calc["path"]) == block_name:
                    blocks[block_name].append(calc)
        # sort every block
        newblocks = {}
        for block_name, block in blocks.items():
            calcnames = [os.path.basename(x["path"]) for x in block]
            calcnames = sorted(calcnames)
            newblock = []
            for calcname in calcnames:
                for calc in block:
                    if os.path.basename(calc["path"]) == calcname:
                        newblock.append(calc)
                        break
            newblocks[block_name] = newblock
        # reset self.calculations with sorted calculations
        newcalcs = []
        for block in newblocks.values():
            newcalcs += block
        if len(newcalcs) != len(self.calculations):
            self._logger.error(
                f"Number of ordered calculations = {len(newcalcs)}\n "
                f"Number of calculations = {len(self.calculations)}")
            self._logger.error(
                "Ordered calculations: "
                f"{[x.basename for x in newcalcs]}")
            self._logger.error(
                "Calculations: "
                f"{[x.basename for x in self.calculations]}")
            raise LookupError("Something happened :(")
        assert len(newcalcs) == len(self.calculations)
        self.calculations = newcalcs

    def _add_subdir_to_content(self, content, path):
        if CalculationDirectory.is_calculation_directory(path):
            self._logger.debug(f"Found a calculation dir at: {path}")
            calc = CalculationDirectory.from_calculation(
                    path, loglevel=self._loglevel)
            # self.calculations.append(calc)
            content.append(calc)
            return
        # else add a new Sub calculation tree (just call super())
        super()._add_subdir_to_content(content, path)
