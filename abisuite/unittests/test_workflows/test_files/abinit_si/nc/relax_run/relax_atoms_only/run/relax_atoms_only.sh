#!/bin/bash

MPIRUN="mpirun -np 4"
EXECUTABLE="/home/fgoudreault/Workspace/abinit/abinit/build/develop/2021-04-16/src/98_main/abinit"
INPUT=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_workflows/test_files/abinit_si/nc/relax_run/relax_atoms_only/run/relax_atoms_only.files
LOG=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_workflows/test_files/abinit_si/nc/relax_run/relax_atoms_only/relax_atoms_only.log
STDERR=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_workflows/test_files/abinit_si/nc/relax_run/relax_atoms_only/relax_atoms_only.stderr


$MPIRUN $EXECUTABLE < $INPUT > $LOG 2> $STDERR
