import os

from .bases import BaseApprover
from .exceptions import MPIError
from ..file_structures import MPIStructure


__ALL_MPI_ARGUMENTS_ALLOWED__ = ("-np", "-npernode", "-n", )


class MPIApprover(BaseApprover):
    """Class that checks if mpi options are well set.
    """
    _loggername = "MPIApprover"
    _exception = MPIError
    _structure_class = MPIStructure

    def validate(self):
        super().validate()
        self._check_cmd()
        self._check_nproc()

    def _check_cmd(self):
        # check that we have a command if we specify command arguements
        if self.mpi_command_arguments and not self.mpi_command:
            self.errors.append(
                    "Need to set a command if you set command arguements!")
        if self.mpi_command_arguments:
            # only options are -np or -npernodes
            for argname, argvalue in self.mpi_command_arguments.items():
                if argname not in __ALL_MPI_ARGUMENTS_ALLOWED__:
                    self.errors.append(
                            f"Invalid mpi argument: '{argname} {argvalue}'.")

    def _check_nproc(self):
        try:
            cmd = self.mpi_command
        except ValueError:
            # nothing to check
            return
        if self.queuing_system == "local":
            # just check number of proc asked is not more than avail
            ppn = os.cpu_count()
            nodes = 1
        else:
            ppn = self.ppn
            nodes = self.nodes
            if not nodes:
                nodes = None
            else:
                nodes = self.extract_number_of_nodes(nodes)
        npernodes = self.extract_npernode(cmd)
        if npernodes is None:
            # No parallelization
            return
        elif npernodes == "all":
            # use all available proc
            return
        if nodes is None or ppn is None:
            # probably number of tasks is used
            available = self.ntasks
        else:
            available = nodes * ppn
        if "npernode" in self.mpi_command:
            total_procs_asked = nodes * npernodes
        else:
            total_procs_asked = npernodes
        if total_procs_asked > available:
            self.errors.append(f"# of procs. asked {total_procs_asked} is"
                               f" more than available"
                               f" ({available})!")

    def extract_number_of_nodes(self, nodesstr):
        if nodesstr is None:
            # raise ValueError(f"Expected str but got '{nodesstr}'")
            return None
        if isinstance(nodesstr, int):
            return nodesstr
        if ":" in nodesstr:
            return int(nodesstr.split(":")[0])
        return int(nodesstr)

    @staticmethod
    def extract_npernode(command):
        if command is None or not isinstance(command, str) or not len(command):
            return None
        # mpirun == 'mpirun -np 12' or -> number of processes
        # mpirun == 'mpirun -npernode 12' -> nb of procs. per node
        split = command.split()
        try:
            return int(split[-1])
        except ValueError:
            # just 'mpirun' => uses all avalaible proc
            return "all"
