from ..bases import BaseFileHandler
from ...file_parsers import QEEPWConductivityTensorParser
from ...file_structures import QEEPWConductivityTensorStructure


class QEEPWConductivityTensorFile(BaseFileHandler):
    """Class that represents a conductivity tensor file produced by a epw.x
    calculation from Quantum Espresso.
    """
    _loggername = "QEEPWConductivityTensorFile"
    _parser_class = QEEPWConductivityTensorParser
    _structure_class = QEEPWConductivityTensorStructure
