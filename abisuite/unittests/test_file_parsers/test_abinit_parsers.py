import unittest

from .bases import (
        BaseParserTest, BaseInputParserTest, BaseWritableParserTest,
        BaseInputParserTestNoInputVariables,
        )
from ..variables_for_tests import (
        abinit_dmft_hamiltonian_example, abinit_dmft_projectors_example,
        abinit_out_example, abinit_vars, abinit_AlAs_pseudos,
        abinit_anaddb_vars,
        abinit_anaddb_log_example, abinit_anaddb_phfrq_example,
        abinit_mrgddb_log_example,
        abinit_optic_lincomp_example, abinit_optic_vars
        )
from ...handlers.file_parsers import (
        AbinitAnaddbFilesParser, AbinitAnaddbInputParser,
        AbinitAnaddbLogParser, AbinitAnaddbPhfrqParser, AbinitDMFTEigParser,
        AbinitDMFTProjectorsParser, AbinitInputParser, AbinitOutputParser,
        AbinitFilesParser, AbinitMrgddbInputParser, AbinitMrgddbLogParser,
        AbinitOpticLincompParser, AbinitOpticFilesParser,
        AbinitOpticInputParser,
        )
from ...handlers.file_writers import (
        AbinitAnaddbFilesWriter, AbinitAnaddbInputWriter, AbinitInputWriter,
        AbinitFilesWriter, AbinitMrgddbInputWriter,
        AbinitOpticFilesWriter,
        AbinitOpticInputWriter)


class BaseFilesParserTest(BaseWritableParserTest):
    """Base test case for files file parsers.
    """
    _file_suffix = ".files"

    def setUp(self, write=True):
        super().setUp(write=False)
        self.writer.input_file_path = "test_files_file_parser.in"
        self.writer.output_file_path = "test_files_file_parser.out"
        if write:
            self.writer.write()


class TestAbinitAnaddbFilesParser(BaseFilesParserTest, unittest.TestCase):
    """Test Case for abinit anaddb files file parser class.
    """
    _parser_class = AbinitAnaddbFilesParser
    _writer_class = AbinitAnaddbFilesWriter

    def setUp(self):
        super().setUp(write=False)
        self.writer.ddb_file_path = "test_ddb"
        self.writer.band_structure_file_path = "test_band_structure"
        self.writer.gkk_file_path = "test_gkk"
        self.writer.eph_data_prefix = "test_eph_prefix"
        self.writer.ddk_file_path = "test_ddk"
        self.writer.write()


class TestAbinitAnaddbInputParser(BaseInputParserTest, unittest.TestCase):
    _input_vars = abinit_anaddb_vars
    _parser_class = AbinitAnaddbInputParser
    _writer_class = AbinitAnaddbInputWriter


class TestAbinitAnaddbLogParser(BaseParserTest, unittest.TestCase):
    _parser_class = AbinitAnaddbLogParser
    _example_file = abinit_anaddb_log_example


class TestAbinitAnaddbPhfrqParser(BaseParserTest, unittest.TestCase):
    """Test case for the anaddb PHFRQ file parser.
    """
    _parser_class = AbinitAnaddbPhfrqParser
    _example_file = abinit_anaddb_phfrq_example

    def test_parsing(self):
        super().test_parsing()
        # test that number of branches and qpts is good
        nqpts = self.parser.frequencies.shape[0]
        nbranches = self.parser.frequencies.shape[1]
        self.assertEqual(nqpts, self.parser.nqpts)
        self.assertEqual(nbranches, self.parser.nbranches)


class TestAbinitInputParser(BaseInputParserTest, unittest.TestCase):
    _input_vars = abinit_vars
    _parser_class = AbinitInputParser
    _writer_class = AbinitInputWriter


class TestAbinitOutputParser(BaseParserTest, unittest.TestCase):
    _example_file = abinit_out_example
    _parser_class = AbinitOutputParser

    def test_output_parser(self):
        # in this example file, it is just a GS calculation and all the input
        # variables are fixed => they do not change after the calculation
        # they should be the same as the abinit_vars dict
        for name, value in abinit_vars.items():
            if name not in self.parser.input_variables and (
                    name not in self.parser.output_variables):
                # this var is not echoed
                continue
            self.assertIn(name, self.parser.input_variables)
            self.assertEqual(
                    value, self.parser.input_variables[name], msg=name)
            self.assertIn(name, self.parser.output_variables)
            self.assertEqual(value, self.parser.output_variables[name])


class TestAbinitOpticInputParser(BaseInputParserTest, unittest.TestCase):
    _input_vars = abinit_optic_vars.copy()
    _parser_class = AbinitOpticInputParser
    _writer_class = AbinitOpticInputWriter


class TestAbinitOpticLincompParser(BaseParserTest, unittest.TestCase):
    _example_file = abinit_optic_lincomp_example
    _parser_class = AbinitOpticLincompParser


class TestAbinitOpticFilesParser(BaseFilesParserTest, unittest.TestCase):
    """Test case for the abinit optic files file parser class.
    """
    _parser_class = AbinitOpticFilesParser
    _writer_class = AbinitOpticFilesWriter

    def setUp(self):
        super().setUp(write=False)
        self.writer.output_data_prefix = "odat_test_files_file_parser"
        self.writer.output_data_dir = self.tempdir.name
        self.writer.write()


class TestAbinitFilesParser(BaseFilesParserTest, unittest.TestCase):
    """Test case for the abinit files file parser class.
    """
    _parser_class = AbinitFilesParser
    _writer_class = AbinitFilesWriter

    def setUp(self):
        super().setUp(write=False)
        self.writer.output_data_prefix = "odat_test_files_file_parser"
        self.writer.output_data_dir = self.tempdir.name
        self.writer.input_data_prefix = "idat_test_files_file_parser"
        self.writer.input_data_dir = self.tempdir.name
        self.writer.tmp_data_prefix = "tmp_test"
        self.writer.tmp_data_dir = self.tempdir.name
        self.writer.pseudos = tuple(abinit_AlAs_pseudos)
        self.writer.write()


class TestAbinitDMFTProjectorsParser(BaseParserTest, unittest.TestCase):
    _parser_class = AbinitDMFTProjectorsParser
    _example_file = abinit_dmft_projectors_example


class TestAbinitDMFTEigParser(BaseParserTest, unittest.TestCase):
    _parser_class = AbinitDMFTEigParser
    _example_file = abinit_dmft_hamiltonian_example


class TestAbinitMrgddbInputParser(
        BaseInputParserTestNoInputVariables, unittest.TestCase):
    """Test case for the mrgddb input parser class.
    """
    _parser_class = AbinitMrgddbInputParser
    _writer_class = AbinitMrgddbInputWriter

    def setUp(self):
        super().setUp(write=False)
        self.writer.output_file_path = "test_output"
        self.writer.title = "test title"
        self.writer.ddb_paths = ["ddb1", "ddb2"]
        self.writer.write()


class TestAbinitMrgddbLogParser(BaseParserTest, unittest.TestCase):
    """Test case for a the mrgddb log parser class.
    """
    _example_file = abinit_mrgddb_log_example
    _parser_class = AbinitMrgddbLogParser
