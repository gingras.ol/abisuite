from ..bases import BaseInputApprover
from ...file_structures import AbinitAnaddbInputStructure
from ....variables import ALL_ABINIT_ANADDB_VARIABLES


class AbinitAnaddbInputApprover(BaseInputApprover):
    """Approver class to validate an anaddb input file.
    """
    _loggername = "AbinitAnaddbInputApprover"
    _variables_db = ALL_ABINIT_ANADDB_VARIABLES
    _structure_class = AbinitAnaddbInputStructure
