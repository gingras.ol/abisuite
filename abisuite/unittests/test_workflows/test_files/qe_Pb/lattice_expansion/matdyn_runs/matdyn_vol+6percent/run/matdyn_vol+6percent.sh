#!/bin/bash

MPIRUN=""
EXECUTABLE="/home/fgoudreault/Workspace/q-e/bin/matdyn.x"
INPUT=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_workflows/test_files/qe_Pb/lattice_expansion/matdyn_runs/matdyn_vol+6percent/matdyn_vol+6percent.in
LOG=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_workflows/test_files/qe_Pb/lattice_expansion/matdyn_runs/matdyn_vol+6percent/matdyn_vol+6percent.log
STDERR=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_workflows/test_files/qe_Pb/lattice_expansion/matdyn_runs/matdyn_vol+6percent/matdyn_vol+6percent.stderr


$MPIRUN $EXECUTABLE -input $INPUT > $LOG 2> $STDERR
