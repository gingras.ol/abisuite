set style data dots
set nokey
set xrange [0: 5.01151]
set yrange [ -1.40611 : 20.09079]
set arrow from  1.11496,  -1.40611 to  1.11496,  20.09079 nohead
set arrow from  2.40240,  -1.40611 to  2.40240,  20.09079 nohead
set arrow from  2.85758,  -1.40611 to  2.85758,  20.09079 nohead
set arrow from  3.64597,  -1.40611 to  3.64597,  20.09079 nohead
set xtics ("L"  0.00000,"G"  1.11496,"X"  2.40240,"U"  2.85758,"K"  3.64597,"G"  5.01151)
 plot "epw_run_band.dat"
