#!/bin/bash

MPIRUN="mpirun -np 4"
EXECUTABLE="/home/fgoudreault/Workspace/q-e/bin/wannier90.x"
INPUT=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_sequencers/files/qe_silicon/wannier_run/wannier_run.win
LOG=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_sequencers/files/qe_silicon/wannier_run/wannier_run.log
STDERR=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_sequencers/files/qe_silicon/wannier_run/wannier_run.stderr


$MPIRUN $EXECUTABLE ../wannier_run.win > $LOG 2> $STDERR
