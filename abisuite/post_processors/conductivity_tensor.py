import numpy as np

from .bases import BasePostProcClass
from ..handlers import (
        CalculationDirectory, QEEPWLogFile,
        )
from ..routines import is_list_like


class ConductivityTensor(BasePostProcClass):
    """Object that can handle a Conductivity Tensor and plot it.
    """
    _loggername = "ConductivityTensor"

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._temperatures = None
        self._conductivity_tensor = None
        self._serta_conductivity_tensor = None
        self._resistivity_tensor = None
        self._serta_resistivity_tensor = None

    @property
    def temperatures(self):
        if self._temperatures is not None:
            return self._temperatures
        raise ValueError("Need to set 'temperatures'.")

    @temperatures.setter
    def temperatures(self, temps):
        self._temperatures = temps

    @property
    def conductivity_tensor(self):
        if self._conductivity_tensor is not None:
            return self._conductivity_tensor
        raise ValueError("Need to set 'conductivity_tensor'.")

    @conductivity_tensor.setter
    def conductivity_tensor(self, sigma):
        self._conductivity_tensor = sigma

    @property
    def serta_conductivity_tensor(self):
        if self._serta_conductivity_tensor is not None:
            return self._serta_conductivity_tensor
        raise ValueError("Need to set 'serta_conductivity_tensor'.")

    @serta_conductivity_tensor.setter
    def serta_conductivity_tensor(self, sigma):
        self._serta_conductivity_tensor = sigma

    @property
    def resistivity_tensor(self):
        if self._resistivity_tensor is not None:
            return self._resistivity_tensor
        # need to compute inverse of conductivity tensor
        self._resistivity_tensor = np.array(
                [np.linalg.inv(x) for x in self.conductivity_tensor.copy()])
        return self.resistivity_tensor

    @property
    def serta_resistivity_tensor(self):
        if self._serta_resistivity_tensor is not None:
            return self._serta_resistivity_tensor
        # need to compute inverse of conductivity tensor
        self._serta_resistivity_tensor = np.array(
                [np.linalg.inv(x)
                 for x in self.serta_conductivity_tensor.copy()])
        return self.resistivity_tensor

    def get_plot(
            self, resistivity=False, conversion_factor=1, elements=None,
            linestyle="-", label="", color=None, serta=False,
            semilogy=False, marker=None, markersize=5,
            markerfacecolor=None, linewidth=2, **kwargs):
        """Create the conductivity tensor plot.

        Parameters
        ----------
        resistivity : bool, optional
            If True, the inverse of the conductivity tensor are shown.
        conversion_factor : float, optional
            Sets a conversion factor to multiply the data (to change units
            for instance).
        elements : list-like, optional
            Tells which elements to plot. It should be a list of two elements
            list which contains the indices to plot (0, 1, 2). If None, all
            are plotted.
        label : str, optional
            Extra label to add to the tensor label.
        linestyle : str, optional
            The linestyle for the curves.
        color : str or list, optional
            Forces a curve color. If a list is given, it is the color for
            each tensor elements plotted. Thus it must be the same length
            as the 'elements' kwarg.
        semilogy : bool, optional
            If True y axis is semilog.
        serta : bool, optional
            If True, we plot the SERTA conductivity/resistivity instead.
        marker: str, optional
            Give the type of markers to draw. If None, not markers are shown.
        markersize: int, optional
            The markersize.
        markerfacecolor: str, optional
            If None, defaults to the curve color.
        linewidth: int, optional
            The curve linewidth.
        """
        if markerfacecolor is None:
            markerfacecolor = color
        ylabel = r"$\sigma_{ij}$"
        data = self.conductivity_tensor.copy()
        symbol = r"$\sigma$"
        if serta:
            data = self.serta_conductivity_tensor.copy()
        if resistivity:
            ylabel = r"$\rho_{ij}$"
            symbol = r"$\rho$"
            if serta:
                data = self.serta_resistivity_tensor.copy()
            else:
                data = self.resistivity_tensor.copy()
        data *= conversion_factor
        plot = super().get_plot(xlabel="Temperature", ylabel=ylabel, **kwargs)
        ydatas = []
        labels = []
        colors = ["r", "m", "orange", "b", "c", "g"]
        ylabels = [
                [symbol + r"$_{xx}$",
                 symbol + r"$_{xy}$",
                 symbol + r"$_{xz}$"],
                [symbol + r"$_{yx}$",
                 symbol + r"$_{yy}$",
                 symbol + r"$_{yz}$"],
                [symbol + "$_{zx}$",
                 symbol + "$_{zy}$",
                 symbol + r"$_{zz}$"]]
        if elements is None:
            indices = [(0, 0), (0, 1), (0, 2), (1, 1), (1, 2), (2, 2)]
        else:
            colors = colors[:len(elements)]
            if color is not None:
                if not is_list_like(color):
                    colors = [color] * len(colors)
                else:
                    colors = color
                if len(colors) != len(elements):
                    raise ValueError(
                            "List of colors must be same length as number of "
                            " tensor elements to plot.")
            indices = elements
        if label and not label.startswith(" "):
            # add beginning space if necessary
            label = " " + label
        for index in indices:
            i = index[0]
            j = index[1]
            ydatas.append(data[:, i, j])
            labels.append(ylabels[i][j] + label if label is not None else None)
        # order temperatures in case they were disordered
        order = np.argsort(self.temperatures)
        temperatures = np.array(self.temperatures)[order]
        for ydata, ylabel, color in zip(ydatas, labels, colors):
            plot.add_curve(
                    temperatures, np.array(ydata)[order], label=ylabel,
                    linewidth=linewidth,
                    color=color, linestyle=linestyle,
                    marker=marker, markerfacecolor=markerfacecolor,
                    markersize=markersize)
        return plot

    @classmethod
    def from_calculation(cls, path, *args, **kwargs):
        """Create ConductivityTensor object from a path to a calculation.

        Parameters
        ----------
        path : str
            Path to calculation.
        """
        with CalculationDirectory.from_calculation(path) as calc:
            if calc.calctype != "qe_epw":
                raise NotImplementedError(calc.calctype)
            # qe epw calc. use the from handler class method
            return cls.from_handler(calc.log_file, *args, **kwargs)

    @classmethod
    def from_file(cls, path, *args, **kwargs):
        """Create a ConductivityTensor object from a conductivity tensor file.

        Only supports an EPW calculation for now.
        """
        if path.endswith(".log"):
            # try to parse a qe epw log file for the conductivity tensor
            with QEEPWLogFile.from_file(path) as log:
                return cls.from_handler(log, *args, **kwargs)
        else:
            raise NotImplementedError(path)

    @classmethod
    def from_handler(cls, handler, *args, **kwargs):
        """Create a ConductivityTensor object from a file handler.

        Parameters
        ----------
        handler : FileHandler object
        """
        with handler:
            # just to make sure it was read
            pass
        mob = cls(*args, **kwargs)
        # only implemented for metals at the moment
        if not handler.conductivities["metals"]:
            raise NotImplementedError(
                    "Only metals are implemented for IBTE")
        mob.temperatures = [[y["temperature"] for y in x]
                            for x in handler.conductivities["metals"]][0]
        # take last iteration only for IBTE
        mob.conductivity_tensor = []
        for temp in handler.conductivities["metals"][-1]:
            mob.conductivity_tensor.append([
                [temp["xx"], temp["xy"], temp["xz"]],
                [temp["yx"], temp["yy"], temp["yz"]],
                [temp["zx"], temp["zy"], temp["zz"]]])
        mob.conductivity_tensor = np.array(mob.conductivity_tensor)
        # now set the serta conductivity tesor
        mob.serta_conductivity_tensor = []
        for temp in handler.serta_conductivities["metals"][-1]:
            mob.serta_conductivity_tensor.append([
                [temp["xx"], temp["xy"], temp["xz"]],
                [temp["yx"], temp["yy"], temp["yz"]],
                [temp["zx"], temp["zy"], temp["zz"]]])
        mob.serta_conductivity_tensor = np.array(
                mob.serta_conductivity_tensor)
        return mob
