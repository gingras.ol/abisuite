import logging
import os
import pickle

from matplotlib.collections import LineCollection
from matplotlib.colors import (ListedColormap, BoundaryNorm,
                               LinearSegmentedColormap)
import colorsys
import matplotlib
matplotlib.rcParams["toolbar"] = "toolmanager"  # noqa: E412
import matplotlib.pyplot as plt  # noqa: E402
import numpy as np  # noqa: E402

from .curves import (  # noqa: E402
        Contour, Curve, Histogram, HLine, VLine, FillBetween, Image, PNGImage,
        Scatter, Surface, Text,
        )
from .plot_tools import (  # noqa: E402
        CurveColor, ToggleLegend, ToggleGrid, TickFontSize,  # noqa: E402
        LabelFontSize,
        )
from ..bases import BaseUtility  # noqa: E402
from ..linux_tools import mkdir  # noqa: E402
from ..routines import is_list_like, suppress_warnings  # noqa: E402
from ..utils import TaggedList  # noqa: E402


# purely arbitrary but reasonable value here. Thanks Yoda for this number!
__DEFAULT_LABEL_FONTSIZE__ = 10


# the following method is to use random colors displayed right
# taken from:
# https://stackoverflow.com/a/32520273/6362595
def rand_cmap(nlabels, type='bright', first_color_black=True,
              last_color_black=False):
    """
    Creates a random colormap to be used together with matplotlib.
    Useful for segmentation tasks

    Parameters
    ----------
    nlabels : int
              Number of labels (size of colormap)
    type : str, optional
           'bright' for strong colors, 'soft' for pastel colors
    first_color_black : bool, optional
                        Option to use first color as black.
    last_color_black : bool, optional
                       Option to use last color as black.

    Returns
    -------
    colormap for matplotlib
    """
    if type not in ('bright', 'soft'):
        raise ValueError('Please choose "bright" or "soft" for type')

    # Generate color map for bright colors, based on hsv
    if type == 'bright':
        randHSVcolors = [(np.random.uniform(low=0.0, high=1),
                          np.random.uniform(low=0.2, high=1),
                          np.random.uniform(low=0.9, high=1))
                         for i in range(nlabels)]

        # Convert HSV list to RGB
        randRGBcolors = []
        for HSVcolor in randHSVcolors:
            randRGBcolors.append(colorsys.hsv_to_rgb(HSVcolor[0], HSVcolor[1],
                                                     HSVcolor[2]))

        if first_color_black:
            randRGBcolors[0] = [0, 0, 0]

        if last_color_black:
            randRGBcolors[-1] = [0, 0, 0]

        random_colormap = LinearSegmentedColormap.from_list('new_map',
                                                            randRGBcolors,
                                                            N=nlabels)

    # Generate soft pastel colors, by limiting the RGB spectrum
    if type == 'soft':
        low = 0.6
        high = 0.95
        randRGBcolors = [(np.random.uniform(low=low, high=high),
                          np.random.uniform(low=low, high=high),
                          np.random.uniform(low=low, high=high))
                         for i in range(nlabels)]

        if first_color_black:
            randRGBcolors[0] = [0, 0, 0]

        if last_color_black:
            randRGBcolors[-1] = [0, 0, 0]
        random_colormap = LinearSegmentedColormap.from_list('new_map',
                                                            randRGBcolors,
                                                            N=nlabels)
    return random_colormap


class Plot(BaseUtility):
    """Class to plot something.

    Attributes
    ----------
    curves:
        The :class:`list <abisuite.utils.tagged_list.TaggedList>` of
        :class:`Curves <abisuite.plotters.curves.Curve>`.
    """
    _loggername = "Plot"

    def __init__(self, **kwargs):
        """Init method.
        """
        # first mute the matplotlib logger because
        # it is unnecesseraily annoying
        logger = logging.getLogger("matplotlib.backend_managers")
        logger.setLevel(logging.ERROR)
        super().__init__(**kwargs)
        # init the attributes of the figure and the curves
        self._fig = None
        self._ax = None
        self._twinx = None
        self._twiny = None
        self._bar = None
        self.curves = TaggedList(loglevel=self._logger.level)
        self.curves_twinx = TaggedList(loglevel=self._logger.level)
        self.curves_twiny = TaggedList(loglevel=self._logger.level)
        self.histograms = TaggedList(loglevel=self._logger.level)
        self.images = TaggedList(loglevel=self._logger.level)
        self.collections = TaggedList(loglevel=self._logger.level)
        self.collections_twinx = TaggedList(loglevel=self._logger.level)
        self.collections_twiny = TaggedList(loglevel=self._logger.level)
        self.colorbar = True  # true => shows colorbar
        self.contours = TaggedList(loglevel=self._logger.level)
        self._xtick_labels = None
        self._xtick_rotation = None
        self._xtick_labels_twiny = None
        self._xtick_rotation_twiny = None
        self.tick_labelsize = None
        self.vlines = TaggedList(loglevel=self._logger.level)
        self.vlines_twiny = TaggedList(loglevel=self._logger.level)
        self.hlines = TaggedList(loglevel=self._logger.level)
        self.hlines_twinx = TaggedList(loglevel=self._logger.level)
        self.fill_between = TaggedList(loglevel=self._logger.level)
        self.fill_between_twinx = TaggedList(loglevel=self._logger.level)
        self.fill_between_twiny = TaggedList(loglevel=self._logger.level)
        self.scatters = TaggedList(loglevel=self._logger.level)
        self.surfaces = TaggedList(loglevel=self._logger.level)
        self.texts = TaggedList(loglevel=self._loglevel)
        # axes colors
        self.bottom_axis_color = "k"
        self.top_axis_color = "k"
        self.left_axis_color = "k"
        self.right_axis_color = "k"
        self.grid = False
        self.aspect = "auto"
        self.legend_fontsize = __DEFAULT_LABEL_FONTSIZE__
        self.legend_transparency = 0.9  # default is a bit transparent
        self._legend = True
        self._legend_outside = False
        self._xlabel = ""
        self._xlabel_twiny = ""
        self._xlims = None
        self._xlims_twiny = None
        self.xlabel_fontsize = __DEFAULT_LABEL_FONTSIZE__
        self._ylabel = ""
        self._ylabel_twinx = ""
        self.ylabel_fontsize = __DEFAULT_LABEL_FONTSIZE__
        self._ylims = None
        self._ylims_twinx = None
        self._zlabel = ""
        self._zlims = None
        self.zlabel_fontsize = __DEFAULT_LABEL_FONTSIZE__
        self._title = ""
        self._colorbarlabel = ""

    def __add__(self, plot):
        """Method that allows the addition of two plots.

        The addition is defined as another plot with the same characteritics
        but the superposition of all curves.
        """
        xx = self.xlabel != plot.xlabel
        yy = self.ylabel != plot.ylabel
        yx = self.ylabel_twinx != plot.ylabel_twinx
        xy = self.xlabel_twiny != plot.xlabel_twiny
        zz = self.zlabel != plot.zlabel
        if any((xx, yy, yx, xy, zz)):
            raise ValueError("Axis labels does not match :(")
        newplot = Plot()
        # set labels
        newplot.xlabel = self.xlabel
        newplot.ylabel = self.ylabel
        newplot.zlabel = self.zlabel
        newplot.ylabel_twinx = self.ylabel_twinx
        newplot.xlabel_twiny = self.xlabel_twiny
        for attr in ("contours", "curves_twinx", "curves", "curves_twiny",
                     "fill_between", "fill_between_twinx",
                     "fill_between_twiny", "histograms", "hlines",
                     "hlines_twinx", "images",
                     "scatters", "surfaces", "vlines", "vlines_twiny",
                     "texts"):
            try:
                setattr(newplot, attr,
                        getattr(self, attr) + getattr(plot, attr))
            except AttributeError:
                # if plot was old (e.g.: loaded from an old pickle) it might
                # not have a given more recent attribute
                # add at least what exists
                for obj in (self, plot):
                    try:
                        setattr(newplot, attr, getattr(obj, attr))
                    except AttributeError:
                        # this object is not updated
                        pass
                pass
        for tick in ("xtick_labels", "xtick_labels_twiny"):
            selftick = getattr(self, tick)
            plottick = getattr(plot, tick)
            if selftick is not None and plottick is not None:
                # both plots have custom xticks. Merge them
                alltick = list(selftick)
                for xtick in plottick:
                    if xtick not in alltick:
                        alltick.append(xtick)
                setattr(newplot, tick, alltick)
            elif selftick is None and plottick is not None:
                setattr(newplot, tick, plottick)
            elif selftick is not None and plottick is None:
                setattr(newplot, tick, selftick)
            # else do nothing
        # compute new title
        if self.title == "" and plot.title != "":
            title = plot.title
        elif self.title != "" and plot.title == "":
            title = self.title
        elif self.title == "" and plot.title == "":
            title = ""
        else:
            title = self.title + " + " + plot.title
        newplot.title = title
        # grid, show if one of the plots is set to shot it
        newplot.grid = self.grid or plot.grid
        return newplot

    @property
    def all_items(self):
        """The list of all items that will be on main axis.
        """
        return self.get_all_items_from_names(
                "contours", "curves", "fill_between", "histograms", "hlines",
                "scatters", "surfaces", "texts", "vlines", "images",
                )

    @property
    def all_items_twinx(self):
        """The list of all items that will be on twinx axis.
        """
        return self.get_all_items_from_names(
                "curves_twinx", "fill_between_twinx", "hlines_twinx",
                "collections_twinx",
                )

    @property
    def all_items_twiny(self):
        """The list of all items that will be on twiny axis.
        """
        return self.get_all_items_from_names(
                "curves_twiny", "fill_between_twinx", "vlines_twiny",
                "collections_twiny",
                )

    def get_all_items_from_names(self, *args):
        """Returns the list of items using the item names to get the
        attributes.
        """
        items = []
        for item_name in args:
            if not hasattr(self, item_name):
                # maybe old version of plot doesn't have this attr
                continue
            for item in getattr(self, item_name):
                items.append(item)
        return items

    @property
    def colorbarlabel(self):
        return self._colorbarlabel

    @colorbarlabel.setter
    def colorbarlabel(self, label):
        self._colorbarlabel = label

    @property
    def curves_color(self):
        return [c.color for c in self.curves]

    @curves_color.setter
    def curves_color(self, colors):
        self._set_curves_color(self.curves, colors)

    @property
    def curves_color_twinx(self):
        return [c.color for c in self.curves_twinx]

    @curves_color_twinx.setter
    def curves_color_twinx(self, colors):
        self._set_curves_color(self.curves_twinx, colors)

    @property
    def curves_color_twiny(self):
        return [c.color for c in self.curves_twiny]

    @curves_color_twiny.setter
    def curves_color_twiny(self, colors):
        self._set_curves_color(self.curves_twiny, colors)

    @property
    def legend(self):
        return self._legend

    @legend.setter
    def legend(self, legend):
        if legend is not False and legend is not True:
            raise TypeError(f"Expected True or False but got: {legend}")
        self._legend = legend

    @property
    def legend_outside(self):
        return self._legend_outside

    @legend_outside.setter
    def legend_outside(self, legend):
        if legend is not False and legend is not True:
            raise TypeError(f"Expected True or False but got: {legend}")
        self._legend_outside = legend

    @property
    def title(self):
        return self._title

    @title.setter
    def title(self, title):
        if not isinstance(title, str):
            raise TypeError("Title must be str.")
        self._title = title

    @property
    def xlabel(self):
        return self._xlabel

    @xlabel.setter
    def xlabel(self, label):
        if not isinstance(label, str):
            raise TypeError("Label must be str.")
        self._xlabel = label

    @property
    def xlabel_twiny(self):
        return self._xlabel_twiny

    @xlabel_twiny.setter
    def xlabel_twiny(self, xlabel):
        self._xlabel_twiny = xlabel

    @property
    def xlims(self):
        return self._xlims

    @xlims.setter
    def xlims(self, xlims):
        if not is_list_like(xlims):
            raise TypeError(f"xlims should be a list of len 2 but got {xlims}")
        if len(xlims) != 2:
            raise TypeError(f"xlims should be a list of len 2 but got {xlims}")
        self._xlims = xlims

    @property
    def xlims_twiny(self):
        return self._xlims_twiny

    @xlims_twiny.setter
    def xlims_twiny(self, xlims):
        if not is_list_like(xlims):
            raise TypeError(f"xlims should be a list of len 2 but got {xlims}")
        if len(xlims) != 2:
            raise TypeError(f"xlims should be a list of len 2 but got {xlims}")
        self._xlims_twiny = xlims

    @property
    def xtick_labels(self):
        """The xtick labels. Must be of the form
        [(pos, label), (pos2, label2), ...].
        """
        return self._xtick_labels

    @xtick_labels.setter
    def xtick_labels(self, labels):
        # must be a list of two component lists.
        if type(labels) not in (list, tuple):
            raise TypeError("Labels should be a list.")
        # if not an empty list, check that each labels are two components
        if len(labels):
            if not is_list_like(labels[0]) or len(labels[0]) != 2:
                raise TypeError("Each xtick_labels componant is a list of"
                                " (pos, label).")
        self._xtick_labels = labels

    @property
    def xtick_labels_twiny(self):
        return self._xtick_labels_twiny

    def set_xtick_labels_twiny(self, labels):
        self.xtick_labels_twiny = labels

    @xtick_labels_twiny.setter
    def xtick_labels_twiny(self, labels):
        # must be a list of two component lists.
        if type(labels) not in (list, tuple):
            raise TypeError("Labels should be a list.")
        # if not an empty list, check that each labels are two components
        if len(labels):
            if type(labels[0]) not in (list, tuple) or len(labels[0]) != 2:
                raise TypeError("Each xtick_labels componant is a list of"
                                " (pos, label).")
        self._xtick_labels_twiny = labels

    @property
    def xtick_rotation(self):
        return self._xtick_rotation

    @xtick_rotation.setter
    def xtick_rotation(self, rotation):
        self._xtick_rotation = rotation

    @property
    def xtick_rotation_twiny(self):
        return self._xtick_rotation_twiny

    @xtick_rotation_twiny.setter
    def xtick_rotation_twiny(self, rotation):
        self._xtick_rotation_twiny = rotation

    @property
    def ylabel(self):
        return self._ylabel

    @ylabel.setter
    def ylabel(self, label):
        if not isinstance(label, str):
            raise TypeError("Label must be str.")
        self._ylabel = label

    @property
    def ylabel_twinx(self):
        return self._ylabel_twinx

    @ylabel_twinx.setter
    def ylabel_twinx(self, ylabel):
        self._ylabel_twinx = ylabel

    @property
    def ylims(self):
        return self._ylims

    @ylims.setter
    def ylims(self, ylims):
        if not is_list_like(ylims):
            raise TypeError(f"ylims should be a list of len 2 but got {ylims}")
        if len(ylims) != 2:
            raise TypeError(f"ylims should be a list of len 2 but got {ylims}")
        self._ylims = ylims

    @property
    def ylims_twinx(self):
        return self._ylims_twinx

    @ylims_twinx.setter
    def ylims_twinx(self, ylims):
        if not is_list_like(ylims):
            raise TypeError(f"ylims should be a list of len 2 but got {ylims}")
        if len(ylims) != 2:
            raise TypeError(f"ylims should be a list of len 2 but got {ylims}")
        self._ylims_twinx = ylims

    @property
    def zlabel(self):
        return self._zlabel

    @zlabel.setter
    def zlabel(self, zlabel):
        if not isinstance(zlabel, str):
            raise TypeError("zlabel must be a string.")
        self._zlabel = zlabel

    @property
    def zlims(self):
        return self._zlims

    @zlims.setter
    def zlims(self, zlims):
        if not is_list_like(zlims):
            raise TypeError(f"zlims should be a list of len 2 but got {zlims}")
        if len(zlims) != 2:
            raise TypeError(f"zlims should be a list of len 2 but got {zlims}")
        self._zlims = zlims

    @property
    def _need_twinx(self):
        if len(self.curves_twinx) or len(self.fill_between_twinx):
            return True
        return False

    @property
    def _need_twiny(self):
        if len(self.curves_twiny) or len(self.fill_between_twiny):
            return True
        return False

    @property
    def _need_3d_projection(self):
        if len(self.surfaces):
            return True
        for curve in self.curves:
            if curve.zdata is not None:
                return True
        for scatter in self.scatters:
            if scatter.zdata is not None:
                return True
        for text in self.texts:
            if text.zdata is not None:
                return True
        return False

    def add_contour(self, *args, tag=None, **kwargs):
        """Add a contour curve to the plot.

        All parameters except the tag are passed to the Contour class.

        Parameters
        ----------
        tag : str, int, optional
              To specify a tag for the image in order to find it more quickly.
              If None, the tag is the index of the contour curve.
        """
        self.contours.append(Contour(*args, **kwargs), tag=tag)

    def add_curve(self, *args, tag=None, twinx=False, twiny=False, **kwargs):
        """Add a curve to the plot.

        All parameters except tag are passed to the
        :class:`Curve <abisuite.plotters.curves.Curve>` class.

        Parameters
        ----------
        tag : str, int, optional
              To specify a tag for the curve in order to find it more quickly.
              If None, the tag is the index of the curve in the list of curves.
        twinx : bool, optional
                If True, the curves is added to the x twin axis curves instead
                of the main x axis.
        twiny : bool, optional
                If True, the curves is added to the y twin axis curves instead
                of the main y axis.
        """
        if twinx and twiny:
            raise ValueError("twinx and twiny cannot be true at the same time")
        curve = Curve(*args,
                      **kwargs, loglevel=self._logger.level)
        if not (twinx or twiny):
            self.curves.append(curve, tag=tag)
        elif twinx:
            self.curves_twinx.append(curve, tag=tag)
        elif twiny:
            self.curves_twiny.append(curve, tag=tag)

    def add_collection(self, collection, twinx=False, tag=None):
        """Add a collection to the plot.

        Parameters
        ----------
        tag : str, int, optional
              To specify a tag for the collection
              in order to find it more quickly.
              If None, the tag is the index of the collection in the list
              of collections.
        twinx : bool, optional
                If True, the collection is added to the twin axis collections
                instead of the main axis.
        """
        # FIXME: we need to implement a LineCollection instance in curves.py
        raise NotImplementedError()
        if not isinstance(collection, LineCollection):
            raise TypeError(f"Expected LineCollection instance but got"
                            f" {collection}")
        if not twinx:
            self.collections.append(collection, tag=tag)
        else:
            self.collections_twinx.append(collection, tag=tag)

    def add_fill_between(self, *args, tag=None, twinx=False, twiny=False,
                         **kwargs):
        """Add a fill between curve to the plot.

        All parameters except tag are passed to the FillBetween class.

        Parameters
        ----------
        tag : optional
              To specify a tag for the fill_between curve to find it quickly.
              If None, the tag is simply the normal index.
        twinx : bool, optional
                If True, the fill_between curve is added to the x twin axis.
        twiny : bool, optional
                If True, the fill_between curve is added to the y twin axis.
        """
        if twinx and twiny:
            raise ValueError("twinx and twiny cannot be true at the same time")
        fill_between = FillBetween(*args, **kwargs)
        if not (twinx or twiny):
            self.fill_between.append(fill_between, tag=tag)
        elif twinx:
            self.fill_between_twinx.append(fill_between, tag=tag)
        elif twiny:
            self.fill_between_twiny.append(fill_between, tag=tag)

    def add_histogram(self, *args, tag=None, **kwargs):
        """Add a histogram to the plot.

        All parameters except tag are passed to the Histogram class.

        Parameters
        ----------
        tag : optional
              To specify a tag for the histogram to find it quickly.
              If None, the tag is simply the normal index.
        """
        self.histograms.append(
                Histogram(*args, **kwargs, loglevel=self._loglevel), tag=tag)

    def add_image(self, *args, tag=None, **kwargs):
        """Add a pcolormesh to the plot.

        All parameters except the tag are passed to the Image class.

        Parameters
        ----------
        tag : str, int, optional
              To specify a tag for the image in order to find it more quickly.
              If None, the tag is the index of the image in the list of images.
        """
        if len(args) == 1:
            # arg probably a path to a PNG image
            self._logger.debug(
                    "Only a path given to add_image => add PNGImage")
            self.images.append(PNGImage(*args, **kwargs), tag=tag)
        else:
            self.images.append(Image(*args, **kwargs), tag=tag)

    def add_scatter(self, *args, tag=None, **kwargs):
        """Add a scatter plot to the plot.

        All parameters except tag are passed to the Scatter class.

        Parameters
        ----------
        tag: optional
             To specify a tag for the scatter plot in order to find it quickly
             than using integers. If None, no tag is applied.
        """
        scatter = Scatter(*args, **kwargs)
        self.scatters.append(scatter, tag=tag)

    def add_surface(self, *args, tag=None, **kwargs):
        """Add a 3D surface to the plot.

        All parameters except tag are passed to the Surface class.

        Parameters
        ----------
        tag: optional
             To specify a tag for the surface plot in order to find it quickly
             than using integers. If None, no tag is applied.
        """
        surface = Surface(*args, **kwargs)
        self.surfaces.append(surface, tag=tag)

    def add_text(self, *args, tag=None, **kwargs):
        """Adds a text label to the plot.

        All parameters except 'tag' are passed to the Text class.

        Parameters
        ----------
        tag: optional
             To specify a tag for the text label in order to find it quickly
             than using integers. If None, no tag is applied.
        """
        self.texts.append(Text(*args, **kwargs), tag=tag)

    def add_vline(self, *args, tag=None, twiny=False, **kwargs):
        """Add a vertical line.

        All parameters except tag are passed to the VLine class.

        Parameters
        ----------
        tag : optional
              Specifies a tag for the vline to find it quickly.
              If None, the tag is simply the normal index.
        twiny : bool, optional
                If True, the vline is added on the twiny axis.
        """
        vline = VLine(*args, **kwargs)
        if not twiny:
            self.vlines.append(vline, tag=tag)
        else:
            self.vlines_twiny.append(vline, tag=tag)

    def add_hline(self, *args, tag=None, twinx=False, **kwargs):
        """Add a horizontal line.

        All parameters except tag are passed to the HLine class.

        Parameters
        ----------
        tag : optional
              Specifies a tag for the hline to find it quickly.
              If None, the tag is simply the normal index.
        twinx : bool, optional
                If True, the horizontal line is added on the twin axis
                instead of the main axis.
        """
        hline = HLine(*args, **kwargs)
        if not twinx:
            self.hlines.append(hline, tag=tag)
        else:
            self.hlines_twinx.append(hline, tag=tag)

    def clear_contours(self):
        """Remove all contour plots from the plot.
        """
        del self.contours
        self.contours = TaggedList(loglevel=self._logger.level)

    def clear_curves(self, twinx=False, twiny=False):
        """Removes all the curves of the plot.

        Parameters
        ----------
        twinx : bool, optional
                If True, all the twinx curves are removed.
        twiny : bool, optional
                If True, all the twiny curves are removed.
        """
        if twinx and twiny:
            raise ValueError("twinx and twiny cannot be true at the same time")
        if not (twinx or twiny):
            del self.curves
            self.curves = TaggedList(loglevel=self._logger.level)
        elif twinx:
            del self.curves_twinx
            self.curves_twinx = TaggedList(loglevel=self._logger.level)
        elif twiny:
            del self.curves_twiny
            self.curves_twiny = TaggedList(loglevel=self._logger.level)

    def clear_curve_labels(self, twinx=False, twiny=False):
        """Sets the labels of all curves to None.

        Parameters
        ----------
        twinx : bool, optional
                If True, the label clearing is done on the x twin axis curves.
        twiny : bool, optional
                If True, the label clearing is done on the y twin axis curves.
        """
        if twinx and twiny:
            raise ValueError("twinx and twiny cannot be true at the same time")
        if twinx:
            curves = self.curves_twinx
        elif twiny:
            curves = self.curves_twiny
        else:
            curves = self.curves
        for curve in curves:
            curve.clear_label()

    def clear_fill_between(self, twinx=False, twiny=False):
        """Removes all fill_betweens from the plot.

        Parameters
        ----------
        twinx : bool, optional
                If True, all the twinx fill_between are removed.
        twiny : bool, optional
                If True, all the twiny fill_between are removed.
        """
        if twinx and twiny:
            raise ValueError("twinx and twiny cannot be true at the same time")
        if not (twinx or twiny):
            del self.fill_between
            self.fill_between = TaggedList(loglevel=self._logger.level)
        elif twinx:
            del self.fill_between_twinx
            self.fill_between_twinx = TaggedList(loglevel=self._logger.level)
        elif twiny:
            del self.fill_between_twiny
            self.fill_between_twiny = TaggedList(loglevel=self._logger.level)

    def clear_histograms(self):
        """Removes all histograms from the plot object.
        """
        self.histograms.clear()

    def clear_hlines(self, twinx=False):
        """Removes all the hlines of the plot.

        Parameters
        ----------
        twinx : bool, optional
                If True, all the twinx hlines are removed.
        """
        if not twinx:
            del self.hlines
            self.hlines = TaggedList(loglevel=self._logger.level)
        elif twinx:
            del self.hlines_twinx
            self.hlines_twinx = TaggedList(loglevel=self._logger.level)

    def clear_vlines(self, twiny=False):
        """Removes all the hlines of the plot.

        Parameters
        ----------
        twiny : bool, optional
                If True, all the twinx hlines are removed.
        """
        if not twiny:
            del self.vlines
            self.vlines = TaggedList(loglevel=self._logger.level)
        elif twiny:
            del self.vlines_twiny
            self.vlines_twiny = TaggedList(loglevel=self._logger.level)

    def clear_scatters(self):
        """Remove all scatters of the plot.
        """
        del self.scatters
        self.scatters = TaggedList(loglevel=self._logger.level)

    def clear_surfaces(self):
        """Removes all surfaces from 3D plot.
        """
        del self.surfaces
        self.surfaces = TaggedList(loglevel=self._logger.level)

    def clear_texts(self):
        """Removes all texts labels of the plot.
        """
        del self.texts
        self.texts = TaggedList(loglevel=self._loglevel)

    @suppress_warnings
    def final_adjusting(self):
        if self.tick_labelsize is not None:
            # adjust xtick label size
            # https://stackoverflow.com/a/11386056/6362595
            self._ax.tick_params(
                    axis="both", which="both", labelsize=self.tick_labelsize)
            if self._need_twinx:
                self._twinx.tick_params(
                    axis="both", which="both", labelsize=self.tick_labelsize)
        self._ax.set_aspect(self.aspect)
        if self._need_3d_projection:
            if self.zlims is not None:
                self._ax.set_zlim(self.zlims[0], self.zlims[1])
            else:
                mins = [min(c.zdata) for c in self.curves]
                mins += [min(f.zdata) for f in self.fill_between]
                maxs = [max(c.zdata) for c in self.curves]
                maxs += [max(f.zdata) for f in self.fill_between]
                self._automatic_adjust_axis(self._ax.set_zlim, mins, maxs)
        if self.ylims is not None:
            self._ax.set_ylim(self.ylims[0], self.ylims[1])
        else:
            mins = [min(c.ydata) for c in self.curves]
            mins += [min(f.ydata) for f in self.fill_between]
            maxs = [max(c.ydata) for c in self.curves]
            maxs += [max(f.ydata) for f in self.fill_between]
            self._automatic_adjust_axis(self._ax.set_ylim, mins, maxs)
        if self._need_twinx:
            if self.ylims_twinx:
                self._twinx.set_ylim(self.ylims_twinx[0], self.ylims_twinx[1])
            else:
                mins = [min(c.ydata) for c in self.curves_twinx]
                mins += [min(f.ydata) for f in self.fill_between_twinx]
                maxs = [max(c.ydata) for c in self.curves_twinx]
                maxs += [max(f.ydata) for f in self.fill_between_twinx]
                self._automatic_adjust_axis(self._twinx.set_ylim, mins, maxs)
        if self._need_twiny:
            if self.xlims_twiny:
                self._twiny.set_xlim(self.xlims_twiny[0], self.xlims_twiny[1])
            else:
                mins = [min(c.ydata) for c in self.curves_twiny]
                mins += [min(f.ydata) for f in self.fill_between_twiny]
                maxs = [max(c.ydata) for c in self.curves_twiny]
                maxs += [max(f.ydata) for f in self.fill_between_twiny]
                self._automatic_adjust_axis(self._twiny.set_xlim, mins, maxs)
        if self.xlims is not None:
            self._ax.set_xlim(self.xlims[0], self.xlims[1])
        else:
            mins = [min(c.xdata) for c in self.curves]
            mins += [min(f.xdata) for f in self.fill_between]
            maxs = [max(c.xdata) for c in self.curves]
            maxs += [max(f.xdata) for f in self.fill_between]
            self._automatic_adjust_axis(self._ax.set_xlim, mins, maxs)

    def normalize_by_curve(self, curve_tag, twinx=False, twiny=False):
        """Normalize all curves with the maximal value of a specific curve.

        Parameters
        ----------
        curve_tag : int, str
                    The curve tag of the curve to which all curves will be
                    normalized.
        twinx : bool, optional
                If True, the normalization procedure is done on the x twin axis
        twiny : bool, optional
                If True, the normalization procedure is done on the y twin axis
        """
        if twinx and twiny:
            raise ValueError("twinx and twiny cannot be true at the same time")
        # find maximum
        if not (twinx or twiny):
            max_ = self.curves[curve_tag].max
        elif twinx:
            max_ = self.curves_twinx[curve_tag].max
        elif twiny:
            max_ = self.curves_twiny[curve_tag].max
        self.normalize_by_factor(max_, twinx=twinx, twiny=twiny)

    def normalize_by_factor(self, factor, twinx=False, twiny=False):
        """Normalize all curves by a specific factor.

        Parameters
        ----------
        factor : The normalizing factor.
        twinx : bool, optional
                If True, the normalization procedure is done on the x twin axis
        twiny : bool, optional
                If True, the normalization procedure is done on the y twin axis
        """
        if twinx and twiny:
            raise ValueError("twinx and twiny cannot be true at the same time")
        if not (twinx or twiny):
            curves_to_normalize = self.curves
        elif twinx:
            curves_to_normalize = self.curves_twinx
        elif twiny:
            curves_to_normalize = self.curves_twiny
        for curve in curves_to_normalize:
            curve /= factor

    def normalize_by_global_max(self, twinx=False, twiny=False):
        """Normalize all curves by the alltime maximum of all curves.

        Parameters
        ----------
        twinx : bool, optional
                If True, the normalization procedure is done on the x twin axis
        twiny : bool, optional
                If True, the normalization procedure is done on the y twin axis
        """
        # find max of all curves
        if not (twinx or twiny):
            curves = self.curves
        elif twinx:
            curves = self.curves_twinx
        elif twiny:
            curves = self.curves_twiny
        maximums = [curve.max for curve in curves]
        self.normalize_by_factor(max(maximums), twinx=twinx, twiny=twiny)

    def plot(self,
             show=True,
             ax=None,
             random_colors=False):
        """Method to plot the figure. It must be called prior to saving
        the figure.

        Parameters
        ----------
        show : bool, optional
            If False, the plot is not shown but it is stored in an internal
            attribute.
        random_colors : bool, optional
            If True, a random color generator is used to generate
            curve colors.
        """
        self._create_figure()
        # plot curves, lines and collections
        if random_colors:
            c1 = rand_cmap(len(self.curves), type='bright',
                           first_color_black=True,
                           last_color_black=False)
            self.curves_color = [c1(i / len(self.curves))
                                 for i in range(len(self.curves))]
            if len(self.curves_twinx):
                ll = len(self.curves_twinx)
                c2 = rand_cmap(ll,
                               type='bright',
                               first_color_black=False,
                               last_color_black=False)
                self.curves_color_twinx = [c2(i / len(self.curves_twinx))
                                           for i in range(ll)]
            if len(self.curves_twiny):
                ll = len(self.curves_twiny)
                c3 = rand_cmap(len(self.curves_twiny),
                               type='bright',
                               first_color_black=False,
                               last_color_black=False)
                self.curves_color_twiny = [c3(i / len(self.curves_twiny))
                                           for i in range(ll)]
        for item in self.all_items:
            item.plot_on_axis(self._ax)
        if self._need_twinx:
            for item in self.all_items_twinx:
                item.plot_on_axis(self._twinx)
        if self._need_twiny:
            for item in self.all_items_twiny:
                item.plot_on_axis(self._twiny)
        self._set_labels()
        self._set_xticks()
        self._set_legend()
        self._set_axis_colors()
        self._set_colorbars()

        if self.grid:
            self._ax.grid(True)
        # before showing, add tools
        self._add_tools_to_fig(self._fig)
        self.final_adjusting()
        if show:
            plt.show()

    def plot_on_axis(self, fig, axis, **kwargs):
        """Same as plot, but give an axis and a figure Matplotlib object.
        All other kwargs go to the plot method.
        """
        self._fig = fig
        self._ax = axis
        self.plot(**kwargs)

    def randomize_curve_colors(self, twinx=False, colormap="gist_rainbow"):
        """Randomizes the color of each curve such that each curve has a
        different color.

        Parameters
        ----------
        colormap : str, optional
                   States the matplotlib colormap to choose color from.
        twinx : bool, optional
                If True, the color randomization procedure is done on the
                curves of the twinx axis.
        """
        curve_list = self.curves
        if twinx:
            curve_list = self.curves_twinx
        nlines = len(curve_list)
        cmap = plt.get_cmap(colormap)
        color_idx = np.linspace(0, 1, nlines)
        for curve, idx in zip(curve_list, color_idx):
            curve.color = cmap(idx)

    def remove_contour(self, tag):
        """Removes a contour curve from the plot according to its tag
        (or index).

        Parameters
        ----------
        tag : str or int
              The contour tag or index.
        """
        self.contours.remove(tag)

    def remove_curve(self, tag, twinx=False, twiny=False):
        """Removes a curve according to its tag (or index).

        Parameters
        ----------
        tag : str
              The curve tag or index to remove.
        twinx : bool, optional
                If True, the curve is removed from the x twin axis instead
                of the x main axis.
        twiny : bool, optional
                If True, the curve is removed from the y twin axis instead
                of the y main axis.
        """
        if twinx and twiny:
            raise ValueError("twinx and twiny cannot be true at the same time")
        if not (twinx or twiny):
            self.curves.remove(tag)
        elif twinx:
            self.curves_twinx.remove(tag)
        elif twiny:
            self.curves_twiny.remove(tag)

    def remove_fill_between(self, tag, twinx=False, twiny=False):
        """Remove a fill_between curve according to its tag.

        Parameters
        ----------
        tag : the tag/index of the fill_between to remove.
        twinx : bool, optional
                If True, the removal is done on the x twin axis fill_betweens.
        twiny : bool, optional
                If True, the removal is done on the y twin axis fill_betweens.
        """
        if not (twinx or twiny):
            self.fill_between.remove(tag)
        elif twinx:
            self.fill_between_twinx.remove(tag)
        elif twiny:
            self.fill_between_twiny.remove(tag)

    def remove_hline(self, tag, twinx=False):
        """Remove a hline according to its tag or index.

        Parameters
        ----------
        tag : tag/index of the hline to remove.
        twinx : bool, optional
                If True, the removal is done on the twin axis instead of
                the main one.
        """
        if not twinx:
            self.hlines.remove(tag)
        else:
            self.hlines_twinx.remove(tag)

    def remove_vline(self, tag, twiny=False):
        """Remove vline according to its tag or index.

        Parameters
        ----------
        tag : The tag of the vline.
        twiny : bool, optional
                If True, the removal procedure is done on the twin y axis.
        """
        if not twiny:
            self.vlines.remove(tag)
        else:
            self.vlines_twiny.remove(tag)

    def remove_scatter(self, tag):
        """Remove a scatter plot according to its tag or its index.
        """
        self.scatters.remove(tag)

    def remove_surface(self, tag):
        """Remove a surface plot according to its tag or index.
        """
        self.surfaces.remove(tag)

    def remove_text(self, tag):
        """Removes a text label according to its tag or index.
        """
        self.texts.remove(tag)

    def reset(self):
        """Resets the matplotlib attributes.
        """
        if self._fig is not None:
            plt.close(self._fig)
        for attr in ("fig", "ax", "twinx", "twiny", "bar"):
            attr = "_" + attr
            val = getattr(self, attr)
            del val
            setattr(self, attr, None)
        # delete items 'lines' properties which are matplotlib objects
        for item in (self.all_items + self.all_items_twinx +
                     self.all_items_twiny):
            item.reset()

    def save(self, path, overwrite=False):
        """Save plot as a png or pdf image using matplotlib savefig method.

        The Plot object is reset afterwards.

        Parameters
        ----------
        path : str
           The path where the file will be saved. If path has a '.plot'
            or '.pickle' extension, then the 'save_plot' method is used
            instead to pickle the object.
        overwrite: bool, optional
            If True and the png/pdf file already exists, it will be
            overwritten. Otherwise a FileExistsError will be thrown.

        Raises
        ------
        FileExistsError:
            If file already exists and overwrite is False.
        RuntimeError:
            If 'plot' method was not called before saving the figure.
        """
        if path.endswith(".plot") or path.endswith(".pickle"):
            self.save_plot(path)
            return
        if self._fig is None:
            raise RuntimeError("Use the plot method to generate figure.")
        # make directory if it doest not exist
        if not os.path.exists(os.path.dirname(path)):
            mkdir(os.path.dirname(path))
        if os.path.exists(path):
            if overwrite:
                os.remove(path)
            else:
                raise FileExistsError(path)
        self._logger.info(f"Saving plot at: '{path}'.")
        if not self.legend_outside:
            self._fig.tight_layout()
            self._fig.savefig(path)
            return
        # if legend outside, add it to savefig to make sure it is saved too
        legend = self._ax.get_legend()
        self._fig.tight_layout()
        self._fig.savefig(path,
                          bbox_extra_artists=[legend],
                          bbox_inches="tight")

    def save_pickle(self, *args, **kwargs):
        """Alias for :meth:`save_plot <abisuite.plotters.plot.Plot.save_plot>`
        method.
        """
        self.save_plot(*args, **kwargs)

    def save_plot(self, path, overwrite=False):
        """Save the Plot object into a file for easy reloading afterwards.

        This is done using the
        `pickle <https://docs.python.org/3/library/pickle.html>`__
        standard module. To load the saved
        plot, one needs to call the staticmethod
        :meth:`load_plot <abisuite.plotters.plot.Plot.load_plot>`.

        Parameters
        ----------
        path : str
            The path where the pickle object will be saved.
        overwrite: bool, optional
            If any file exists at the given path, it will be overwritten.
        """
        if os.path.exists(path):
            if not overwrite:
                raise FileExistsError(path)
            else:
                # delete file
                os.remove(path)
        # call reset to not have to pickle matplotlib figures
        self.reset()
        self._logger.info(f"Saving plot pickle at: '{path}'.")
        with open(path, "wb") as f:
            pickle.dump(self, f)

    def separate_positive_from_negative(self, color_pos, color_neg,
                                        twinx=False, twiny=False, zero=0.0):
        """Separates the curves according to their value.
        If curve is > 0, its color is set to color_pos, samething for < 0.

        Parameters
        ----------
        color_pos : str
                    The color for the positive part of the curve.
        color_neg : str
                    The color for the negative part of the curve.
        twinx : bool, optional
                If True, this operation is done on the x twin axis.
        twiny : bool, optional
                If True, this operation is done on the y twin axis.
        zero : float, optional
               Defines the zero of the y axis. (default=0)
        """
        # taken from
        # https://matplotlib.org/examples/pylab_examples/multicolored_line.html
        if twinx and twiny:
            raise ValueError("twinx and twiny cannot be true at the same time")
        curves = self.curves
        if twinx:
            curves = self.curves_twinx
        elif twiny:
            curves = self.curves_twiny
        for _tag, curve in curves.iter_tags_items():
            # for every curve introduce a point everytime the curve crosses 0
            xs = list(curve.xdata)
            ys = list(curve.ydata)
            newxs = list(curve.xdata.copy())
            newys = list(curve.ydata.copy())
            for i, (x, y) in enumerate(zip(xs[1:], ys[1:])):
                # check if there is a change of sign
                if (y - zero) * (ys[i - 1] - zero) < 0:
                    # there is a change of sign, introduce new point
                    # whose y value is mathematically exactly 0
                    newx = ((zero *
                            (x - xs[i - 1]) + y * xs[i - 1] - ys[i - 1] * x) /
                            (y - ys[i - 1]))
                    newxs.append(newx)
                    newys.append(zero)
            curve.xdata = newxs
            curve.ydata = newys
            # resort data just to make sure
            curve.sort_data()
            cmap = ListedColormap([color_neg, color_pos])
            norm = BoundaryNorm([curve.min, zero, curve.max], cmap.N)
            points = np.array([curve.xdata, curve.ydata]).T.reshape(-1, 1, 2)
            segments = np.concatenate([points[:-1], points[1:]], axis=1)
            lc = LineCollection(segments, cmap=cmap, norm=norm)
            lc.set_array(curve.ydata)
            self.add_collection(lc)
            # make line transparent
            curve.alpha = 0.0
        # remove all original curves
        # self.clear_curves(twinx=twinx)

    def set_curve_label(self, label, tag, twinx=False, twiny=False):
        """Set the label of a specific curve.

        Parameters
        ----------
        label : str
                The label to apply to the curve.
        tag : The curve tag/index.
        twinx : bool, optional
                If True, the label is applied on the x twin axis.
        twiny : bool, optional
                If True, the label is applied on the y twin axis.
        """
        if twinx and twiny:
            raise ValueError("twinx and twiny cannot be true at the same time")
        if not (twinx or twiny):
            self.curves[tag].label = label
        elif twinx:
            self.curves_twinx[tag].label = label
        elif twiny:
            self.curves_twiny[tag].label = label

    def set_title(self, title):
        self.title = title

    def set_xlabel(self, value):
        self.xlabel = value

    def set_xtick_labels(self, labels):
        self.xtick_labels = labels

    def set_ylabel(self, label):
        self.ylabel = label

    def show(self, *args, **kwargs):
        """Shows the generated plot. It has to be drawn first by calling
        the plot() method.
        """
        if self._fig is None:
            raise RuntimeError("Generate plot first.")
        plt.show(*args, **kwargs)

    def synchronize_labels(self, plot):
        """Synchronize the axis labels such that they are the same as a
        given plot object.

        Parameters
        ----------
        plot: Plot object
            The plot object to copy the axis label from.

        Raises
        ------
        TypeError: if the given object is not a Plot instance.
        """
        if not isinstance(plot, Plot):
            raise TypeError(f"Need to give a Plot object but got '{plot}'.")
        self.xlabel = plot.xlabel
        self.ylabel = plot.ylabel
        if plot.zlabel is not None:
            self.zlabel = plot.zlabel
        if plot.ylabel_twinx is not None:
            self.ylabel_twinx = plot.ylabel_twinx
        if plot.xlabel_twiny is not None:
            self.xlabel_twiny = plot.xlabel_twiny

    def update(self):
        """Update the plot with stored data. Use this if data has changed since
        last drawing.
        """
        if self._fig is None:
            return RuntimeError("Generate plot first.")
        for item in (
                self.all_items + self.all_items_twinx + self.all_items_twiny):
            item.update()
        self._set_legend()
        self._set_labels()
        # the following seems to have a weird side effect
        # that reduce the plot size
        # (when 3D only?!?) FG: 2021/08/30
        # self._remove_colorbars()
        # self._set_colorbars()
        self._fig.canvas.draw()

    @suppress_warnings
    def _add_tools_to_fig(self, fig):
        # check the backend before adding tools
        backend = matplotlib.get_backend().lower()
        if backend == "agg" or backend == "nbagg" or "ipykernel" in backend:
            # agg backend or jupyter-notebook backend
            # don't add tools since they don't support it
            return
        toolmanager = fig.canvas.manager.toolmanager
        toolbar = fig.canvas.manager.toolbar
        # add the ToggleLegend tool
        if toolmanager is None or toolbar is None:
            # in case we use a backend which does not support tools
            return
        toolmanager.add_tool("togglelegend", ToggleLegend)
        toolbar.add_tool(toolmanager.get_tool("togglelegend"), "io")
        # add the ToggleGrid tool
        toolmanager.add_tool("togglegrid", ToggleGrid)
        toolbar.add_tool(toolmanager.get_tool("togglegrid"), "io")
        # add colorcurve to bar
        toolmanager.add_tool("curvecolor", CurveColor)
        toolbar.add_tool(toolmanager.get_tool("curvecolor"), "io")
        # axis font size
        toolmanager.add_tool("tick_font_size", TickFontSize)
        toolbar.add_tool(toolmanager.get_tool("tick_font_size"), "io")
        # axis font size
        toolmanager.add_tool("label_font_size", LabelFontSize)
        toolbar.add_tool(toolmanager.get_tool("label_font_size"), "io")

    def _automatic_adjust_axis(self, adjust_func, mins, maxs):
        if not mins or not maxs:
            return
        range_ = max(maxs) - min(mins)
        fivepercent = 0.05 * range_
        adjust_func(min(mins) - fivepercent, max(maxs) + fivepercent)

    @suppress_warnings
    def _create_figure(self):
        if self._fig is not None or self._ax is not None:
            return
        self._fig = plt.figure()
        if self._need_3d_projection:
            from mpl_toolkits.mplot3d import Axes3D  # noqa: F401 unused import
            ax = self._fig.gca(projection="3d")
        else:
            ax = self._fig.add_subplot(111)
        self._ax = ax
        if self._need_twinx:
            self._twinx = self._ax.twinx()
        if self._need_twiny:
            self._twiny = self._ax.twiny()

    # def _plot_collections(self):
    #     for collection in self.collections:
    #         self._ax.add_collection(collection)
    #     if self._need_twinx:
    #         for collection in self.collections_twinx:
    #             self._twinx.add_collection(collection)
    #     if self._need_twiny:
    #         for collection in self.collections_twiny:
    #             self._twiny.add_collection(collection)

    def _remove_colorbars(self):
        try:
            self._bar.remove()
        except (AttributeError, KeyError):
            pass
        del self._bar
        self._bar = None

    def _set_axis_colors(self):
        # set the color of the axes. useful when using twinx or twiny!
        self._logger.debug("Setting axis and ticks colors.")
        ax = self._ax
        if self._need_twinx:
            ax = self._twinx
        ax.spines["bottom"].set_color(self.bottom_axis_color)
        ax.spines["top"].set_color(self.top_axis_color)
        ax.spines["left"].set_color(self.left_axis_color)
        ax.spines["right"].set_color(self.right_axis_color)
        # also change the ticks and labels to the same color
        self._ax.tick_params(axis="x", colors=self.bottom_axis_color)
        self._ax.tick_params(axis="y", colors=self.left_axis_color)
        self._ax.yaxis.label.set_color(self.left_axis_color)
        self._ax.xaxis.label.set_color(self.bottom_axis_color)
        if self._need_twinx:
            self._twinx.tick_params(axis="y", colors=self.right_axis_color)
            self._twinx.yaxis.label.set_color(self.right_axis_color)
        if self._need_twiny:
            self._twiny.tick_params(axis="x", colors=self.top_axis_color)
            self._twiny.xaxis.label.set_color(self.top_axis_color)

    def _set_colorbar(self, ax_images):
        if not self.colorbar:
            return
        if len(ax_images):
            if len(ax_images) > 1:
                self._logger.warning(
                        "Not sure more than 1 images/surface works...")
                self._logger.warning(
                        "Only plotting 1 colorbar on first image/surface...")
            # for ax_image in ax_images:
            self._bar = self._fig.colorbar(ax_images[0], ax=self._ax)
            self._bar.set_label(self.colorbarlabel)

    def _set_colorbars(self):
        self._set_colorbar([image.ax_image for image in self.images])
        self._set_colorbar([surface.ax_image for surface in self.surfaces])

    def _set_curves_color(self, curves, colors):
        if is_list_like(colors):
            if len(colors) != len(curves):
                raise ValueError(f"{colors} is not same length as curves"
                                 f" ({len(curves)}).")
        else:
            colors = [colors] * len(curves)
        self._logger.debug(f"Setting curves colors to {colors}")
        for color, curve in zip(colors, curves):
            # if not isinstance(color, str):
            #     raise TypeError(f"Color must be a string but got: {color}")
            curve.color = color

    def _set_labels(self):
        # set title and axis labels
        self._ax.set_title(self.title)
        self._ax.set_xlabel(self.xlabel, fontsize=self.xlabel_fontsize)
        self._ax.set_ylabel(self.ylabel, fontsize=self.ylabel_fontsize)
        if self._need_3d_projection:
            self._ax.set_zlabel(self.zlabel, fontsize=self.zlabel_fontsize)
        if self._need_twinx:
            self._twinx.set_ylabel(
                    self.ylabel_twinx, fontsize=self.ylabel_fontsize)
        if self._need_twiny:
            self._twiny.set_ylabel(
                    self.xlabel_twiny, fontsize=self.xlabel_fontsize)

    def _set_legend(self):
        # show legend if needed and if at least one curve_labels is not None
        lines, labels = self._ax.get_legend_handles_labels()
        # set legend on this axis
        ax = self._ax
        if self._need_twinx:
            lines2, labels2 = self._twinx.get_legend_handles_labels()
            lines += lines2
            labels += labels2
            ax = self._twinx
        if self._need_twiny:
            lines3, labels3 = self._twiny.get_legend_handles_labels()
            lines += lines3
            labels += labels3
        if self.contours:
            # contours are handed differently
            # https://github.com/matplotlib/matplotlib/issues/11134#issuecomment-384966888
            all_contour_lines = [
                    c.contour.legend_elements()[0][0] for c in self.contours]
            contour_labels = [c.label for c in self.contours]
            # remove lines for None labels
            lines += [
                    line for line, label in zip(
                        all_contour_lines, contour_labels) if label is not None
                    ]
            labels += [label for label in contour_labels if label is not None]
        if self.legend and labels:
            args = [lines, labels]
            kwargs = {
                    "loc": "best",
                    "fontsize": self.legend_fontsize,
                    "framealpha": self.legend_transparency,
                    }
            if self.legend_outside:
                kwargs["bbox_to_anchor"] = (1, 0.5)
                kwargs["loc"] = "center left"
            legend = ax.legend(*args, **kwargs)
            # make legend draggable
            legend.set_draggable(True)

    def _set_xticks(self):
        # set ticks
        for attr, ax in zip(("xtick_labels", "xtick_labels_twiny"),
                            (self._ax, self._twiny)):
            selftick = getattr(self, attr)
            if selftick is not None:
                ticks_loc = [x[0] for x in selftick]
                ticks_labels = [x[1] for x in selftick]
                ax.set_xticks(ticks_loc)
                ax.set_xticklabels(ticks_labels)
        for attr, ax in zip(("xtick_rotation", "xtick_rotation_twiny"),
                            (self._ax, self._twiny)):
            selfrot = getattr(self, attr)
            if selfrot is not None:
                for tick in ax.get_xticklabels():
                    tick.set_rotation(selfrot)

    @staticmethod
    def load_plot(path, loglevel=None):
        """Load a :class:`Plot <abisuite.plotters.plot.Plot>`
        object from a previously pickled file.

        To save a plot, see the
        :meth:`save_plot <abisuite.plotters.plot.Plot.save_plot>` method.

        Parameters
        ----------
        path : str
            The path to the file where the object is contained.
        loglevel: int, optional
            If not None, sets the loglevel to this value. Otherwise,
            the value of the pickle will be taken since this value was
            pickled like everything else.
        """
        if not os.path.isfile(path):
            raise FileNotFoundError(path)
        with open(path, "rb") as f:
            plot = pickle.load(f)
        if loglevel is not None:
            plot._loglevel = loglevel
        return plot
