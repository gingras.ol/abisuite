from .scf_sequencer import AbinitSCFSequencer
from ..bases import SequenceCalculation
from ...constants import HARTREE_TO_EV
from ...post_processors import DOSPlot, ProjectedDOSPlot


class AbinitDOSSequencer(AbinitSCFSequencer):
    """Sequencer class for a DOS calculation done by the Abinit software.

    This Sequence is basically a SCF calculation + a DOS calculation based
    on the SCF calculation. The DOS calculation is an NSCF calculation
    with a denser kpt grid.

    A DOS plot is also generated once the sequence is completed.
    """
    _all_sequencer_prefixes = ("scf_", "dos_", "plot_", )
    _loggername = "AbinitDOSSequencer"

    def __init__(self, *args, **kwargs):
        AbinitSCFSequencer.__init__(self, *args, **kwargs)
        self._dos_fine_kpoint_grid_variables = None
        self._dos_plot = None
        self._projected_dos_plots = None

    @property
    def dos_input_variables(self):
        if self._dos_input_variables is None:
            raise ValueError("Need to set 'dos_input_variables'.")
        return self._dos_input_variables

    @dos_input_variables.setter
    def dos_input_variables(self, dos):
        if not isinstance(dos, dict):
            raise TypeError(f"dos input vars should be a dict. Got '{dos}'.")
        if dos.get("prtdos", 0) == 0:
            raise ValueError("'prtdos' should be set > 0.")
        # make copy to make sure we don't update something we shouldn't
        dos = dos.copy()
        dos.update(self.dos_fine_kpoint_grid_variables)
        self._dos_input_variables = dos

    @property
    def dos_fine_kpoint_grid_variables(self):
        """The dict of fine kpoint grid variables. This dict updates the dict
        containing all the dos input variables.
        """
        if self._dos_fine_kpoint_grid_variables is None:
            raise ValueError("Need to set 'dos_fine_kpoint_grid_variables'.")
        return self._dos_fine_kpoint_grid_variables

    @dos_fine_kpoint_grid_variables.setter
    def dos_fine_kpoint_grid_variables(self, grid):
        if not isinstance(grid, dict):
            raise TypeError(
                    "'dos_fine_kpoint_grid_variables' should be a dict but got"
                    f": '{grid}'.")
        self._dos_fine_kpoint_grid_variables = grid

    @property
    def dos_plot(self):
        if self._dos_plot is not None:
            return self._dos_plot
        raise RuntimeError("'dos_plot' not generated yet.")

    @property
    def projected_dos_plots(self):
        if self._projected_dos_plots is not None:
            return self._projected_dos_plots
        raise RuntimeError("'projected_dos_plots' not generated.")

    def init_dos_sequence(self):
        scfcalc = self.get_sequence_calculation("scf")
        doscalc = SequenceCalculation(
                scfcalc.calctype, self.dos_workdir,
                self.dos_input_variables,
                self.dos_calculation_parameters,
                loglevel=self._loglevel)
        doscalc.dependencies.append(scfcalc)
        doscalc.link_den_from = scfcalc
        doscalc.load_geometry_from = scfcalc
        self.sequence.append(doscalc)

    def init_sequence(self):
        super().init_sequence()
        self.init_dos_sequence()

    def plot_dos(self):
        """Plots the DOS. If projected DOS_AT files are present, those
        projected dos will also be plot.

        Returns
        -------
        Plot object: If only the DOS is plot
        tuple: (DOS Plot object, list of Projected DOS plots).
        """
        dos_plot = DOSPlot.from_calculation(
                self.dos_workdir, loglevel=self._loglevel)
        # convert everything to eV. units are read in Ha
        dos_plot.energies *= HARTREE_TO_EV
        dos_plot.dos /= HARTREE_TO_EV
        dos_plot.fermi_energy *= HARTREE_TO_EV
        plot = dos_plot.get_plot(
                vertical=True, line_at_fermi=True, xlabel="DOS",
                xunits="electrons / eV / cell", ylabel="Energy", yunits="eV",
                title=(r"Density of States $E_F=$" +
                       f"{dos_plot.fermi_energy:.2f} eV"),
                linewidth=2, color="k")
        self._post_process_plot(plot, name_extension="dos")
        self._dos_plot = plot
        # check if there are projected dos files. if yes, plot them
        self._projected_dos_plots = []
        try:
            projected_dosses = ProjectedDOSPlot.from_calculation(
                    self.dos_workdir, loglevel=self._loglevel)
            for projected_dos in projected_dosses:
                projected_dos.energies *= HARTREE_TO_EV
                atom = projected_dos.atom_number
                for idx, dos in projected_dos.projected_dos.items():
                    projected_dos.projected_dos[idx] = dos / HARTREE_TO_EV
                projected_dos.fermi_energy *= HARTREE_TO_EV
                plots, multi_plot = projected_dos.get_plot(
                        vertical=True, line_at_fermi=True,
                        xlabel="projected DOS",
                        xunits="electrons / eV / cell",
                        ylabel="Energy", yunits="eV",
                        title=(r"Density of States $E_F=$" +
                               f"{projected_dos.fermi_energy:.2f} eV"),
                        linewidth=2)
                for l_idx, plot in enumerate(plots):
                    self._post_process_plot(
                            plot,
                            name_extension=(
                                "projected_dos_atom"
                                f"{projected_dos.atom_number}_l={l_idx}"))
                    self._projected_dos_plots.append(plot)
                self._post_process_plot(
                        multi_plot, name_extension=(
                            f"projected_dos_atom{atom}_multi"))
        except (FileNotFoundError, ValueError):
            return self._dos_plot
        else:
            return self._dos_plot, self._projected_dos_plots

    def post_sequence(self, *args, **kwargs):
        super().post_sequence(*args, **kwargs)
        self.plot_dos()
