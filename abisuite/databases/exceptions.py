class AbiDBRuntimeError(RuntimeError):
    pass


class CalculationDoesNotExistError(FileNotFoundError):
    pass


class CachedAbiDBFileNotFoundError(FileNotFoundError):
    pass


class ColumnDoesNotExistsError(AbiDBRuntimeError):
    pass
