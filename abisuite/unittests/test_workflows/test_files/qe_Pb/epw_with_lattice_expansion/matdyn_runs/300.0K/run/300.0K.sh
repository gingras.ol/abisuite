#!/bin/bash

MPIRUN=""
EXECUTABLE="/home/fgoudreault/Workspace/q-e/bin/matdyn.x"
INPUT=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_workflows/test_files/qe_Pb/epw_with_lattice_expansion/matdyn_runs/300.0K/300.0K.in
LOG=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_workflows/test_files/qe_Pb/epw_with_lattice_expansion/matdyn_runs/300.0K/300.0K.log
STDERR=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_workflows/test_files/qe_Pb/epw_with_lattice_expansion/matdyn_runs/300.0K/300.0K.stderr


$MPIRUN $EXECUTABLE -input $INPUT > $LOG 2> $STDERR
