import os

from .bases import BaseAbisuiteSubScript
from ..colors import Colors
from ..linux_tools import which
from ..routines import check_output_with_retries


class AbisuiteDebugScript(BaseAbisuiteSubScript):
    """Executes the 'debug' subcommand of the abisuite script.
    """
    _loggername = "AbisuiteDebugScript"

    def debug(self, show_input_file=False, force=False):
        """Prints debugging information for all desired calculations.

        Parameters
        ----------
        force: bool, optional
            If True, all calculations go through the script. Otherwise,
            only 'errored' calculation are debugged.
        show_input_file: bool, optional
            If True, the input file's content is printed on screen.
        """
        for calculation in self.calculations:
            print(Colors.bold_text("DEBUGGING:") + f" '{calculation.path}'.")
            if self.skip_this_calculation(
                    calculation, continue_on_error=False,
                    continue_on_unstarted=not force,
                    continue_on_running=not force,
                    continue_on_finished=not force,
                    continue_on_converged=not force):
                continue
            self._print_stderr_content(calculation)
            self._print_crash_file(calculation)
            self._print_pbs_file(calculation)
            if show_input_file:
                self._print_input_file(calculation)
            if calculation.pbs_file.queuing_system == "slurm":
                # print out slurm data
                self._print_slurm_job_report(calculation)
            calculation.clear_content()

    def _print_crash_file(self, calculation):
        # if abinit calc and an __ABI_ABORTFILE__ is present: cat it
        if calculation.calctype.startswith("abinit"):
            with calculation.run_directory as run:
                for handler in run:
                    basename = handler.basename
                    if "ABORTFILE" in basename and not (
                            basename.endswith(".lock")):
                        # self._logger.info(
                        #     "This is an abinit run and a ABORTFILE"
                        #     " is present.")
                        print()
                        print(Colors.bold_text(
                            "Printing ABORTFILE content:"))
                        handler.cat(line_limit=30)
        elif calculation.calctype.startswith("qe_"):
            # check for a CRASH file and expose it
            # it might be in the calcdir or run dir
            with calculation:
                for handler in calculation:
                    if handler.basename == "CRASH":
                        print()
                        print(Colors.bold_text(
                            "Printing CRASH file content:"))
                        handler.cat()
            with calculation.run_directory as run:
                for handler in run:
                    if handler.basename == "CRASH":
                        print()
                        print(Colors.bold_text(
                            "Printing CRASH file content:"))
                        handler.cat()

    def _print_pbs_file(self, calculation):
        with calculation.pbs_file as pbs:
            # print calculation parametersx
            print()
            print(Colors.bold_text("Calculation parameters:"))
            print(pbs)
            print()

    def _print_stderr_content(self, calculation):
        if not calculation.stderr_file.exists:
            return
        print(Colors.bold_text("Printing stderr file content:") +
              f" '{calculation.stderr_file.path}'.")
        calculation.stderr_file.cat(line_limit=30)

    def _print_input_file(self, calc):
        print(Colors.bold_text("Input file content:"))
        calc.input_file.cat()

    def _print_slurm_job_report(self, calculation):
        # get the jobid. get all the ids that was used for this calc and
        # call seff on the highest one
        ids = []
        for filename in os.listdir(calculation.run_directory.path):
            if filename.startswith("slurm"):

                ids.append(int(filename.split("-")[-1].split(".")[0]))
        if not ids:
            return
        if which("seff") is None:
            print(Colors.color_text("ERROR:", "red", "bold") +
                  " we're on a slurm system but seff is not found.")
            return
        print("\n" + Colors.bold_text(
            "Calling 'seff' for this calculation:"))
        print(check_output_with_retries(
            ["seff", str(max(ids))]).decode("utf-8"))
