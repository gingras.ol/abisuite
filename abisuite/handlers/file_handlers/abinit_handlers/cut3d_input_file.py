from ..bases import BaseInputFileHandlerNoInputVariables
from ...file_approvers import AbinitCut3DInputApprover
from ...file_parsers import AbinitCut3DInputParser
from ...file_structures import AbinitCut3DInputStructure
from ...file_writers import AbinitCut3DInputWriter


class AbinitCut3DInputFile(BaseInputFileHandlerNoInputVariables):
    """File handler class for the 'cut3d' input file.
    """
    _approver_class = AbinitCut3DInputApprover
    _loggername = "AbinitCut3DInputFile"
    _parser_class = AbinitCut3DInputParser
    _structure_class = AbinitCut3DInputStructure
    _writer_class = AbinitCut3DInputWriter
