#!/bin/bash

MPIRUN="mpirun -np 4"
EXECUTABLE="/home/fgoudreault/Workspace/q-e/bin/pw.x -npool 4"
INPUT=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_workflows/test_files/qe_Pb/relax_run/relax_full_cell_geometry_2/relax_full_cell_geometry_2.in
LOG=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_workflows/test_files/qe_Pb/relax_run/relax_full_cell_geometry_2/relax_full_cell_geometry_2.log
STDERR=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_workflows/test_files/qe_Pb/relax_run/relax_full_cell_geometry_2/relax_full_cell_geometry_2.stderr


$MPIRUN $EXECUTABLE -input $INPUT > $LOG 2> $STDERR
