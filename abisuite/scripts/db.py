import os
import sys

from .bases import BaseAbisuiteSubScript
from .routines import walltime_to_str
from .utils import CalculationsList
from .. import __USER_CONFIG__
from ..colors import Colors
from ..databases import AbiDB
from ..databases.exceptions import (
        CalculationDoesNotExistError, CachedAbiDBFileNotFoundError,
        )
from ..handlers import CalculationDirectory
from ..routines import full_abspath
from ..utils import TerminalTable


class AbisuiteDBScript(BaseAbisuiteSubScript):
    """Executes the 'db' succommand of the abisuite script."""
    _loggername = "AbisuiteDBScript"

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.db = None

    def add_calculations_to_database(self):
        """Adds calculations to the database.
        """
        for calculation in self.calculations:
            print(Colors.bold_text("ADDDING") +
                  f"calculation: '{calculation}'.")
            self.db.add_calculation(calculation)
        self.success("database insertion.")

    def check_integrity(self):
        """Checks database integrity.
        """
        print(Colors.bold_text("Checking database integrity..."))
        self.db.check_database_integrity(raise_error=False)
        print(Colors.color_text("Integrity checked.", "green", "bold"))

    def clean_database(self):
        """Cleanses the database from calculations that don't exist
        anymore.
        """
        print(Colors.bold_text("CLEANING") + f" database: '{self.db.db}'.")
        try:
            self.db.clean()
        except Exception:
            self._logger.exception()
        else:
            self.success("cleaning.")

    def delete_calculations_from_database(self, force=False):
        """As the name suggests, deletes calculations from the database.

        Parameter
        ---------
        force: bool, optional
            If False, a database integrity check is done before
            deletion. Otherwise, we force deletion even though the database
            contains errors.
        """
        for calculation in self.calculations:
            print(Colors.bold_text("DELETING:") +
                  f" '{calculation}'.")
            try:
                self.db.delete_calculation(
                        calculation, check_integrity=not force)
            except CalculationDoesNotExistError:
                print(Colors.color_text(
                    "DOES NOT EXISTS IN DB", "bold", "yellow"))
            else:
                self.successfull("deletion.")

    def get(self, get_id=False, get_status=False, get_monitored=False,
            get_packaged=False, get_project=False, get_walltime=False,
            ):
        """Prints out requested data from database in a nicely formatted table.

        Parameters
        ----------
        get_id: bool, optional
            If True, the ids of calculations are printed.
        get_status: bool, optional
            If True, the status of calculations are printed.
        get_monitored: bool, optional
            If True, the 'monitored' attribute of calculations are printed.
        get_packaged: bool, optional
            If True, the 'packaged' attribute of calculations are printed.
        get_project: bool, optional
            If True, the 'project' attribute of calculations are printed.
        get_walltime: bool, optional
            If True, the walltime is printed if available.
        """
        if not any((get_id, get_status, get_monitored,
                    get_packaged, get_project, get_walltime)):
            return
        column_names = ["Calculation(s)"]
        column_alignments = ["left"]
        if get_id:
            column_names.append("ID")
            column_alignments.append("right")
        if get_monitored:
            column_names.append("Monitored")
            column_alignments.append("right")
        if get_packaged:
            column_names.append("Packaged")
            column_alignments.append("right")
        if get_project:
            column_names.append("Project")
            column_alignments.append("right")
        if get_walltime:
            column_names.append("Walltime")
            column_alignments.append("right")
        if get_status:
            column_names.append("Status Dict")
            column_alignments.append("right")
        table = TerminalTable(
                column_names, column_alignments=column_alignments,
                bold_column_headers=True, loglevel=self._loglevel)
        pwd = os.getcwd()
        for calculation in self.calculations:
            row_data = [os.path.relpath(calculation, pwd)]
            try:
                if get_id:
                    row_data.append(self.db[calculation])
                if get_monitored:
                    row_data.append(self.db.get_monitored(calculation))
                if get_packaged:
                    row_data.append(self.db.get_packaged(calculation))
                if get_project:
                    row_data.append(self.db.get_project(calculation))
                if get_walltime:
                    walltime = self.db.get_walltime(calculation)
                    if walltime is not None:
                        row_data.append(walltime_to_str(walltime))
                    else:
                        row_data.append("N/A")
                if get_status:
                    row_data.append(self.db.get_status(calculation))
            except CalculationDoesNotExistError:
                self.error("calculation not in database '" +
                           f"{calculation}'.")
            else:
                table.add_row(row_data)
        table.print()

    def load_calculations(self, *args, **kwargs):
        super().load_calculations(*args, return_path=True, **kwargs)

    def load_calculations_from_database(
            self, all_calculations=False, all_monitored_calculations=False):
        """Loads calculations from database.

        Parameters
        ----------
        all_calculations: bool, optional
            If True, loads all calculations from database.
        all_monitored_calculations: bool, optional
            If True, loads all monitored calculations from database.
        """
        if all_calculations and all_monitored_calculations:
            raise ValueError(
                "Make up your mind! Do I fetch all calculations or just "
                "the monitored ones?!?")
        if all_calculations:
            print(Colors.bold_text("LOADING") +
                  ": all calculations from database.")
            self.calculations = CalculationsList(
                    self.db.get_all_calculations(), loglevel=self._loglevel)
        elif all_monitored_calculations:
            print(Colors.bold_text("LOADING") +
                  ": all monitored calculations from database.")
            self.calculations = CalculationsList(
                    self.db.get_all_monitored_calculations(),
                    loglevel=self._loglevel)
        # if not a remote, get full abspaths
        if not self.db.is_remote:
            self.calculations = CalculationsList([
                    full_abspath(calc) for calc in self.calculations],
                    loglevel=self._loglevel)

    def load_database(
            self, db_path, remote=None, download=False,
            refresh=False):
        """Loads the database given.

        Parameters
        ----------
        db_path: str
            Can be 'default', in which case it loads the database set
            in the user's config file. Otherwise it should be a path
            pointing to the database wanted.
        refresh: bool, optional
            If True, the 'status' database entry for selected calculations
            will be refreshed.
        remote: str, optional
            If None, we load the given file path. Otherwise, we look for
            either cached database if it exists or we download it if
            requested.
        download: bool, optional
            If remote is not None, setting this flag to True will make
            the script to download the remote's database.
        """
        if remote is None:
            if db_path == "default":
                db_path = __USER_CONFIG__.DATABASE.path
                print(Colors.bold_text("LOADING default database:") +
                      f"'{db_path}'.")
            self.db = AbiDB.from_file(db_path, loglevel=self._loglevel)
        else:
            if download:
                print(Colors.bold_text("DOWNLOADING remote database:") +
                      f" '{remote}'.")
            else:
                print(Colors.bold_text("LOADING cached remote database for:") +
                      f" '{remote}'.")
            try:
                self.db = AbiDB.from_remote(
                        remote, download=download, refresh=refresh,
                        loglevel=self._loglevel)
            except CachedAbiDBFileNotFoundError:
                self._logger.error(
                    "Cached DB not found. Try using the --download flag.")
                self._logger.exception()
                sys.exit()
        print(Colors.color_text("SUCCESSFULL", "green", "bold") +
              ": database loaded.")

    def init_database(self, db_path):
        """Initialize a database at a given path.

        Parameters
        ----------
        db_path: str
            The path where we want to initialize the database.
        """
        print(Colors.bold_text("Creating database here:") +
              f"'{db_path}'.")
        abidb = AbiDB(loglevel=self._loglevel)
        abidb.create_database(db_path)
        print(Colors.color_text("SUCCESSFULL", "green", "bold") +
              ": database created. If you want to make it the default database"
              ", consider adding this entry in the config file "
              f"('{__USER_CONFIG__.config_path}') under [DATABASE]: "
              f"'path = {db_path}'.")

    def refresh_calculations(self):
        """Refreshes the calculation statuses stored in the database.
        """
        for calculation in self.calculations:
            try:
                self.db.refresh(calculation)
            except CalculationDoesNotExistError:
                self.error(f"not in database '{calculation}'.")
            else:
                self.successfull(
                        f"calculation refreshed -> '{calculation}'.")

    def reset_status(self):
        """Resets the status of the calculations stored in the 'calculations'
        attribute.
        """
        for calculation in self.calculations:
            print(Colors.bold_text("Resetting status of:") +
                  f" '{calculation}'.")
            calc = CalculationDirectory.from_calculation(
                    calculation, loglevel=self._loglevel)
            calc.connect_to_database(database=self.db)
            if not calc.is_in_database:
                print(Colors.bold_text("Adding calculation to database."))
                calc.add_to_database()
            try:
                calc.reset_status()
            except Exception:
                self._logger.exception("reset status failed.")
                self.error("while resetting status.")
            else:
                self.success("status reset.")

    def set(self, set_monitored_true=False, set_monitored_false=False,
            set_project=None, set_packaged_true=False,
            set_packaged_false=False, update_calculation_status=None):
        """Sets data in the database regarding the calculations stored
        in the 'calculations' attribute.

        Parameters
        ----------
        set_monitored_true: bool, optional
            If True, the 'monitored' database entry will be set to True.
        set_monitored_false: bool, optional
            If True, the 'monitored' database entry will be set to False.
        set_project: str, optional
            If not None, will set the 'project' database entry to this string.
        set_packaged_true: bool, optional
            If True, the 'packaged' database entry will be set to True.
        set_packaged_false: bool, optional
            If True, the 'packaged' database entry will be set to False.
        update_calculation_status: dict, optional
            If not None, the calculation status entry will be updated with this
            dictionary.
        """
        if set_monitored_true and set_monitored_false:
            raise ValueError(
                    "Make up your mind about the 'monitored' attribute: "
                    "I cannot set it to both True and False at the same time!")
        if set_packaged_true and set_packaged_false:
            raise ValueError(
                    "Make up your mind about the 'packaged' attribute: "
                    "I cannot set it to both True and False at the same time!")
        if set_monitored_true:
            self.db.update_calculation_monitored(self.calculations, True)
            print(f"Now monitoring: '{self.calculations}'")
        if set_monitored_false:
            self.db.update_calculation_monitored(self.calculations, False)
            print(f"Not monitoring: '{self.calculations}'")
        if set_packaged_true:
            self.db.update_calculation_packaged(self.calculations, True)
            print(f"Now packaged: '{self.calculations}'")
        if set_packaged_false:
            self.db.update_calculation_packaged(self.calculations, False)
            print(f"Not packaged: '{self.calculations}'")
        if set_project is not None:
            self.db.update_calculation_project(self.calculations, set_project)
            print(f"Project '{set_project}' now contains these calculations:" +
                  f"{self.calculations}.")
        if update_calculation_status is not None:
            print(Colors.bold_text("UPDATING calculation status with: ") +
                  str(update_calculation_status))
            self.db.update_calculation_status(
                    self.calculations, update_status=update_calculation_status,
                    recompute=True)
            print("The following calculation(s) have been updated: "
                  f"'{self.calculations}'.")
        self.success("DB update.")

    def show_database(self):
        """Prints the database path.
        """
        print(f"Transacting with database: '{self.db.db}'.")
