from ..bases import BaseInputStructure
from ....variables.wannier90_variables import ALL_WANNIER90_VARIABLES


class Wannier90InputStructure(BaseInputStructure):
    """Structure class for a wannier90 input file.
    """
    _variables_db = ALL_WANNIER90_VARIABLES
