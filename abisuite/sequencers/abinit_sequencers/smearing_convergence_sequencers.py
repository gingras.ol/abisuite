from .kgrid_convergence_sequencers import AbinitKgridConvergenceSequencer
from .individual_phonon_sequencers import AbinitPhononConvergenceSequencer
from ..smearing_convergence_sequencers import (
        SmearingConvergenceSequencer, SmearingPhononConvergenceSequencer,
        )


class AbinitSmearingConvergenceSequencer(
        SmearingConvergenceSequencer, AbinitKgridConvergenceSequencer):
    """Convergence sequencer class for the k-points + smearing parameters
    for Abinit.
    """
    smearings_input_variable_name = "tsmear"
    smearings_input_variable_units = "Ha"
    _loggername = "AbinitSmearingConvergenceSequencer"

    def __init__(self, *args, **kwargs):
        # do kgrid last since base class overwrites the
        # _kgrids_input_variable_name
        SmearingConvergenceSequencer.__init__(self, "abinit", *args, **kwargs)
        AbinitKgridConvergenceSequencer.__init__(self, *args, **kwargs)


class AbinitSmearingPhononConvergenceSequencer(
        AbinitSmearingConvergenceSequencer,
        AbinitPhononConvergenceSequencer,
        SmearingPhononConvergenceSequencer):
    """Convergence sequnecer for the smearing parameters vs the phonon
    frequencies for the Abinit software.
    """
    _all_sequencer_prefixes = ("scf_", "ddk_", "phonons_", "plot_", )
    _loggername = "AbinitSmearingPhononConvergenceSequencer"

    def __init__(self, *args, **kwargs):
        AbinitSmearingConvergenceSequencer.__init__(self, *args, **kwargs)
        AbinitPhononConvergenceSequencer.__init__(self, *args, **kwargs)
        SmearingPhononConvergenceSequencer.__init__(
                self, "abinit", *args, **kwargs)
        # If we do smearing convergence, usually that means we're dealing
        # with a metal and thus don't need to compute electric field response
        # because that is 0 (that also means no LO-TO splitting)
        self.compute_electric_field_response = False

    def compute_convergence(self, *args, **kwargs):
        # Watchout for Deadly Diamond of Death here!!
        return SmearingPhononConvergenceSequencer.compute_convergence(
                self, *args, **kwargs)

    def plot_convergence(self, *args, **kwargs):
        # Watchout for Deadly Diamond of Death here!!
        SmearingPhononConvergenceSequencer.plot_convergence(
                self, *args, **kwargs)

    def _get_phonon_workdir(self, *args, iatpol=None, idir=None, **kwargs):
        # Watchout Deadly Diamond of Death!!!
        # discard iatpol/idir here
        return SmearingPhononConvergenceSequencer._get_phonon_workdir(
                self, *args, **kwargs)
