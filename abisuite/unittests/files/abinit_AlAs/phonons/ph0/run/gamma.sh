#!/bin/bash

MPIRUN=""
EXECUTABLE="/Users/fgoudreault/Workspace/abinit/build/mpi/src/98_main/abinit"
INPUT=/Users/fgoudreault/Workspace/abisuite/abisuite/unittests/files/abinit_AlAs/phonons/ph0/run/gamma.files
LOG=/Users/fgoudreault/Workspace/abisuite/abisuite/unittests/files/abinit_AlAs/phonons/ph0/gamma.log
STDERR=/Users/fgoudreault/Workspace/abisuite/abisuite/unittests/files/abinit_AlAs/phonons/ph0/gamma.stderr


$MPIRUN $EXECUTABLE < $INPUT > $LOG 2> $STDERR
