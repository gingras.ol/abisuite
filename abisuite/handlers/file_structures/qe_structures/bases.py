from ..bases import BaseStructure, BaseLogStructure


class BaseQELogStructure(BaseLogStructure):
    """Base structure class for Quantum Espresso log files.
    """
    pass


class BaseQEBandFreqStructure(BaseStructure):
    """Base structure for all files with dispersion produced
    by the Quantum Espresso scripts.
    """
    all_attributes = ("coordinates", "eigenvalues", "nbands", "npts")
