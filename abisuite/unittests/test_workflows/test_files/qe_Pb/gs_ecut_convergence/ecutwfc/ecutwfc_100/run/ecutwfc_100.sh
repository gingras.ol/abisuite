#!/bin/bash

MPIRUN="mpirun -np 4"
EXECUTABLE="/home/fgoudreault/Workspace/q-e/bin/pw.x -npool 4"
INPUT=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_workflows/test_files/qe_Pb/gs_ecut_convergence/ecutwfc/ecutwfc_100/ecutwfc_100.in
LOG=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_workflows/test_files/qe_Pb/gs_ecut_convergence/ecutwfc/ecutwfc_100/ecutwfc_100.log
STDERR=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_workflows/test_files/qe_Pb/gs_ecut_convergence/ecutwfc/ecutwfc_100/ecutwfc_100.stderr


$MPIRUN $EXECUTABLE -input $INPUT > $LOG 2> $STDERR
