import datetime
import os

from .bases import (
        PathConvertibleFileHandler, BaseApprovableFileHandler,
        )
from ..file_approvers import MetaDataApprover
from ..file_parsers import MetaDataParser
from ..file_structures import MetaDataStructure
from ..file_writers import MetaDataWriter


class MetaDataFile(PathConvertibleFileHandler, BaseApprovableFileHandler):
    """File handler for a meta data file.
    """
    _approver_class = MetaDataApprover
    _loggername = "MetaDataFile"
    _parser_class = MetaDataParser
    _structure_class = MetaDataStructure
    _writer_class = MetaDataWriter

    # if attr is calc_workdir, we need to change all other 'path' attribute to
    # be consistent with 'calc_workdir'.
    def __setattr__(self, attr, value):
        resetpaths = False
        if attr == "calc_workdir" and self.structure._calc_workdir is not None:
            # need to reset all path attributes to relative before resetting it
            self.convert_to_relative_paths()
            resetpaths = True
        super().__setattr__(attr, value)
        if resetpaths:
            # need to reset all path attributes to absolute after setting it
            self.convert_to_absolute_paths()

    def add_child(self, path):
        """Add a calculation directory to children.
        """
        self._add_path_to_container(path, "children")

    def add_copied_file(self, path):
        """Add a file to be copied to the meta data file.
        """
        self._add_path_to_container(path, "copied_files")

    def add_linked_file(self, path):
        """Add a file to be linked to the meta data file.
        """
        self._add_path_to_container(path, "linked_files")

    def add_parent(self, path):
        """Add a calculation directory to parents.
        """
        self._add_path_to_container(path, "parents")

    def move(self, *args, reset_calc_workdir=True, **kwargs):
        PathConvertibleFileHandler.move(self, *args, **kwargs)
        self.repair()

    def repair(self):
        """Attempt to repair a meta data file that might have been moved
        carelessly.
        """
        self.calc_workdir = os.path.dirname(self.path)
        self.write(overwrite=True)

    def write(self, *args, bypass_last_modified_update=False, **kwargs):
        """Writes the MetaDataFile object.

        At the same time, reset the 'last_modified' attribute to NOW.
        All arguments are passed to the mother class.

        Parameters
        ----------
        bypass_last_modified_update : bool, optional
                If True, the last_modified property won't be updated.
        """
        if not bypass_last_modified_update:
            self._logger.debug("Bypassing update of last_modified property.")
            self.last_modified = str(datetime.datetime.now())
        super().write(*args, **kwargs)

    def _add_path_to_container(self, path, container):
        actual_container = getattr(self, container)
        if path in actual_container:
            # don't append twice
            # self._logger.debug(f"Already in {container}: {path}")
            return
        actual_container.append(path)

    @classmethod
    def from_calculation(cls, path, *args, **kwargs):
        """Creates a MetaDataFile object from a calculation directory,
        provided that it has a '.meta' file in it.
        """
        # need to override the classmethod for obvious reasons (super class
        # use a MetaDataFile)
        if not os.path.isdir(path):
            raise NotADirectoryError(path)
        for subfile in os.listdir(path):
            if subfile.endswith(".meta"):
                return cls.from_file(os.path.join(path, subfile), *args,
                                     **kwargs)
        raise FileNotFoundError(f"Could not find meta file in {path}")

    @classmethod
    def from_meta_data_file(cls, metadatafile, *args, **kwargs):
        """Inititialize meta data file from another meta data file object.

        Almost like a copy, but allows to use the init arguments.
        """
        new = cls(*args, **kwargs)
        new.path = metadatafile.path
        new.structure = metadatafile.structure.copy()
        return new

    @classmethod
    def from_launcher(cls, launcher, *args, **kwargs):
        """Return the Meta data file in a launcher with all the launcher's
        data.
        """
        return launcher.meta_data_file
