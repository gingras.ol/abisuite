from .bases import BaseQELogStructure


class QEDynmatLogStructure(BaseQELogStructure):
    """Structure for a dynmat.x log file from Quantum Espresso.
    """
    all_attributes = tuple([])
