from ...bases import BaseSubParser
from .....routines import decompose_line


class AbinitMrgddbLogWalltimeSubParser(BaseSubParser):
    """Parser the the time analysis printed out in the log file of the
    mrgddb script.
    """
    subject = "walltime"
    trigger = "ndividual time (sec):"
    _loggername = "AbinitMrgddbLogWalltimeSubParser"

    def _extract_data_from_lines(self, lines):
        # e.g.:
        # - Proc.   0 individual time (sec): cpu=          0.1  wall=       0.1
        s, i, f = decompose_line(lines[0])
        self.ending_relative_index = 0
        return f[-1]
