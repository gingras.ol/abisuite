import unittest

from .bases import BaseMassLauncherTest
from ..variables_for_tests import (
        abinit_vars, abinit_AlAs_pseudos, abinit_optic_vars)
from ...launchers import AbinitMassLauncher, AbinitOpticMassLauncher


class TestAbinitMassLauncher(BaseMassLauncherTest, unittest.TestCase):
    _calctype = "abinit"
    _common_vars = abinit_vars.copy()
    _jobnames = ["ecut5", "ecut10"]
    _mass_launcher_class = AbinitMassLauncher
    _pseudos = abinit_AlAs_pseudos
    _specific_vars = [{"ecut": 5}, {"ecut": 10}]


class TestAbinitOpticMassLauncher(BaseMassLauncherTest, unittest.TestCase):
    _calctype = "optic"
    _common_vars = abinit_optic_vars.copy()
    _jobnames = ["comp11", "comp22"]
    _mass_launcher_class = AbinitOpticMassLauncher
    _specific_vars = [{"lin_comp": 11}, {"lin_comp": 22}]
