from ..bases import BaseLogFileHandler
from ...file_parsers import QEEpsilonLogParser
from ...file_structures import QEEpsilonLogStructure


class QEEpsilonLogFile(BaseLogFileHandler):
    """Log file handler for a Quantum Espresso epsilon.x log file.
    """
    _loggername = "QEEpsilonLogFile"
    _parser_class = QEEpsilonLogParser
    _structure_class = QEEpsilonLogStructure
