from .bases import BaseAbinitFilesStructure


class AbinitFilesStructure(BaseAbinitFilesStructure):
    """Structure for an abinit input file.
    """
    all_attributes = ("input_data_dir", "tmp_data_dir", "input_data_prefix",
                      "pseudos", "input_file_path", "output_file_path",
                      "output_data_prefix", "output_data_dir",
                      "tmp_data_prefix")
    convertible_path_attributes = ("input_data_dir", "tmp_data_dir",
                                   "input_file_path", "output_file_path",
                                   "output_data_dir")
