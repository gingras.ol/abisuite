.. _linkers_api:

Linkers Module
==============

.. contents::
    :local:

Linkers objects are used to link one calculation to another. It creates
necessary Symlinks or copies necessary files for that purpose.
