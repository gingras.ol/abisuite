from ..plotters import Plot, MultiPlot, Curve
import numpy as np
import os
import tempfile
import unittest


class TestCurve(unittest.TestCase):
    _xdata = [-1, 2, 8]
    _ydata = np.array([0.0, 1.0, 2.3])
    _label = "label"

    def setUp(self):
        self.curve = Curve(self._xdata, self._ydata, label=self._label)

    def tearDown(self):
        del self.curve

    def test_clear_label(self):
        self.assertEqual(self.curve.label, self._label)
        self.curve.clear_label()
        self.assertIs(self.curve.label, None)

    def test_max_min(self):
        self.assertEqual(self.curve.max, np.max(self._ydata))
        self.assertEqual(self.curve.min, np.min(self._ydata))
        self.assertEqual(self.curve.wheremin, -1)
        self.assertEqual(self.curve.wheremax, 8)

    def test_arithmetic_operations(self):
        self.curve *= 2.0
        self.assertListEqual(self.curve.ydata.tolist(),
                             (self._ydata * 2.0).tolist())
        self.curve /= 4.0
        self.assertListEqual(self.curve.ydata.tolist(),
                             (self._ydata * 2.0 / 4.0).tolist())
        self.curve += 3.4
        self.assertListEqual(self.curve.ydata.tolist(),
                             (self._ydata * 2.0 / 4.0 + 3.4).tolist())
        self.curve -= 8.9
        self.assertListEqual(self.curve.ydata.tolist(),
                             (self._ydata * 2.0 / 4.0 + 3.4 - 8.9).tolist())
        with self.assertRaises(TypeError):
            self.curve += "string"
        with self.assertRaises(TypeError):
            self.curve *= "string"
        with self.assertRaises(TypeError):
            self.curve -= "string"
        with self.assertRaises(TypeError):
            self.curve /= "string"


class TestMultiPlot(unittest.TestCase):
    _xdata = [0, 1, 2]
    _ydata = [0, 1, 2]

    def setUp(self):
        self.plot1 = Plot()
        self.plot1.add_curve(self._xdata, self._ydata)
        self.plot2 = Plot()
        self.plot2.add_curve(self._ydata, self._xdata)
        self.multiplot = MultiPlot()
        self.multiplot.add_plot(self.plot1, 0)
        self.multiplot.add_plot(self.plot2, 0)
        self.tempdir = tempfile.TemporaryDirectory()
        self.path = os.path.join(self.tempdir.name, "test_multiplot.png")

    def tearDown(self):
        self.tempdir.cleanup()
        del self.tempdir

    def test_muliplot_creation(self):
        # check that it raises an error if we try to add anything else
        with self.assertRaises(TypeError):
            self.multiplot.add_plot(1, 1)
        self.assertEqual(len(self.multiplot.plot_rows), 1)
        self.assertEqual(len(self.multiplot.plot_rows[0]), 2)
        self.assertEqual(len(self.multiplot.plots), 2)
        # test that plotting process does not raise an error
        self.multiplot.plot(show=False)

    def test_multiplot_save(self):
        # should raise an error if plot not drawn before saving
        with self.assertRaises(RuntimeError):
            self.multiplot.save(self.path)
        self.multiplot.plot(show=False)
        self.multiplot.save(self.path)
        self.assertTrue(os.path.isfile(self.path))


class TestPlot(unittest.TestCase):
    _xdata = np.array([0, 1, 2])
    _ydata = np.array([0, 1, 3])
    _color = "b"
    _linestyle = "--"
    _label = "label"
    _linewidth = 1.5
    _tag = "curve"

    def setUp(self):
        self.plot = Plot()
        self.plot.add_curve(self._xdata, self._ydata, color=self._color,
                            linestyle=self._linestyle, label=self._label,
                            linewidth=self._linewidth, tag=self._tag)
        self.plot.add_vline(1, linestyle=self._linestyle, color=self._color,
                            linewidth=self._linewidth, tag=self._tag)
        self.plot.add_hline(1, linestyle=self._linestyle, color=self._color,
                            linewidth=self._linewidth, tag=self._tag)
        self.plot.add_fill_between(self._xdata, self._ydata - 1,
                                   self._ydata + 1, color=self._color,
                                   tag=self._tag)
        self.temp = tempfile.TemporaryDirectory()
        self.name = os.path.join(self.temp.name, "save.png")

    def tearDown(self):
        self.temp.cleanup()
        del self.temp

    def test_remove_by_tag(self):
        self.plot.remove_curve(self._tag)
        self.plot.remove_vline(self._tag)
        self.plot.remove_hline(self._tag)
        self.plot.remove_fill_between(self._tag)

    def test_remove_by_index(self):
        self.plot.remove_curve(0)
        self.plot.remove_vline(0)
        self.plot.remove_hline(0)
        self.plot.remove_fill_between(0)

    def test_normalize_by_curve(self):
        self.plot.normalize_by_curve(0)
        self.assertListEqual(self.plot.curves[0].ydata.tolist(),
                             (self._ydata / max(self._ydata)).tolist())

    def test_normalize_by_factor(self):
        self.plot.normalize_by_factor(0.5)
        self.assertListEqual(self.plot.curves[0].ydata.tolist(),
                             (self._ydata / 0.5).tolist())

    def test_normalize_by_global_max(self):
        self.plot.normalize_by_global_max()
        self.assertListEqual(self.plot.curves[0].ydata.tolist(),
                             (self._ydata / max(self._ydata)).tolist())

    def test_clear_labels(self):
        # test that the clear labels method works
        for curve in self.plot.curves:
            self.assertEqual(curve.label, self._label)
        self.plot.clear_curve_labels()
        for curve in self.plot.curves:
            self.assertIs(curve.label, None)

    def test_create_plot(self):
        self.assertEqual(self.plot.curves[0].color, self._color)
        self.assertEqual(self.plot.curves[0].linestyle, self._linestyle)
        self.assertEqual(self.plot.curves[0].label, self._label)
        self.assertEqual(self.plot.curves[0].linewidth, self._linewidth)
        self.assertListEqual(self.plot.curves[0].xdata.tolist(),
                             self._xdata.tolist())
        self.assertListEqual(self.plot.curves[0].ydata.tolist(),
                             self._ydata.tolist())
        self.assertListEqual(self.plot.fill_between[0].xdata.tolist(),
                             self._xdata.tolist())
        self.assertListEqual(self.plot.fill_between[0].ydata.tolist(),
                             (self._ydata - 1).tolist())
        self.assertListEqual(self.plot.fill_between[0].ydata2.tolist(),
                             (self._ydata + 1).tolist())
        self.assertEqual(self.plot.fill_between[0].color, self._color)

    def test_save_plot(self):
        # check that an error is raised if saving file before plot is made
        with self.assertRaises(RuntimeError):
            self.plot.save(self.name)
        self.plot.plot(show=False)
        self.plot.save(self.name)
        self.assertTrue(os.path.isfile(self.name))

    def test_add_plot(self):
        plot2 = Plot()
        plot2.add_curve(self._ydata, self._xdata, color="k", label="label2",
                        linestyle=":", linewidth=2.0)
        plot2.add_fill_between(self._ydata, self._xdata - 1,
                               self._xdata + 1, color=self._color)
        plot3 = self.plot + plot2
        self.assertEqual(len(plot3.curves), 2)
        self.assertEqual(len(plot3.hlines), 1)
        self.assertEqual(len(plot3.vlines), 1)
        self.assertListEqual(plot3.curves[0].xdata.tolist(),
                             self._xdata.tolist())
        self.assertListEqual(plot3.curves[0].ydata.tolist(),
                             self._ydata.tolist())
        self.assertListEqual(plot3.curves[1].xdata.tolist(),
                             self._ydata.tolist())
        self.assertListEqual(plot3.curves[1].ydata.tolist(),
                             self._xdata.tolist())
        self.assertListEqual(plot3.fill_between[0].xdata.tolist(),
                             self._xdata.tolist())
        self.assertListEqual(plot3.fill_between[0].ydata.tolist(),
                             (self._ydata - 1).tolist())
        self.assertListEqual(plot3.fill_between[0].ydata2.tolist(),
                             (self._ydata + 1).tolist())
        self.assertListEqual(plot3.fill_between[1].xdata.tolist(),
                             self._ydata.tolist())
        self.assertListEqual(plot3.fill_between[1].ydata.tolist(),
                             (self._xdata - 1).tolist())
        self.assertListEqual(plot3.fill_between[1].ydata2.tolist(),
                             (self._xdata + 1).tolist())
        self.assertEqual(plot3.curves[0].color, self._color)
        self.assertEqual(plot3.curves[0].linestyle, self._linestyle)
        self.assertEqual(plot3.curves[0].label, self._label)
        self.assertEqual(plot3.curves[1].color, "k")
        self.assertEqual(plot3.curves[1].linestyle, ":")
        self.assertEqual(plot3.curves[1].label, "label2")

    def test_set_curve_label(self):
        self.plot.set_curve_label("newlabel", 0)
        self.assertEqual(self.plot.curves[0].label, "newlabel")

    def test_add_vlines(self):
        self.assertEqual(self.plot.vlines[0].position, 1)
        self.assertEqual(self.plot.vlines[0].linestyle, self._linestyle)
        self.assertEqual(self.plot.vlines[0].color, self._color)
        self.assertEqual(self.plot.vlines[0].linewidth, self._linewidth)

    def test_add_hlines(self):
        self.assertEqual(self.plot.hlines[0].position, 1)
        self.assertEqual(self.plot.hlines[0].linestyle, self._linestyle)
        self.assertEqual(self.plot.hlines[0].color, self._color)
        self.assertEqual(self.plot.hlines[0].linewidth, self._linewidth)
