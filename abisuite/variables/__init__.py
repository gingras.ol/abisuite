from .abinit_variables import (
        ALL_ABINIT_VARIABLES, ALL_ABINIT_ANADDB_VARIABLES,
        ALL_ABINITOPTIC_VARIABLES,
        )
from .bases import InputVariableDict
from .generic_variables import GENERIC_INPUT_VARIABLES_DB
from .qe_variables import (
        ALL_QEDOS_VARIABLES,
        ALL_QEDYNMAT_VARIABLES,
        ALL_QEEPSILON_VARIABLES,
        ALL_QEEPW_VARIABLES,
        ALL_QEFS_VARIABLES,
        ALL_QELD1_VARIABLES,
        ALL_QEMATDYN_VARIABLES,
        ALL_QEPH_VARIABLES,
        ALL_QEPP_VARIABLES,
        ALL_QEPROJWFC_VARIABLES,
        ALL_QEPW_VARIABLES,
        ALL_QEPW2WANNIER90_VARIABLES,
        ALL_QEQ2R_VARIABLES,
        )
from .wannier90_variables import (
        ALL_WANNIER90_VARIABLES,
        )


CALCTYPES_TO_INPUT_VARIABLES = {
        "abinit": ALL_ABINIT_VARIABLES,
        "abinit_anaddb": ALL_ABINIT_ANADDB_VARIABLES,
        "abinit_optic": ALL_ABINITOPTIC_VARIABLES,
        "qe_dos": ALL_QEDOS_VARIABLES,
        "qe_dynmat": ALL_QEDYNMAT_VARIABLES,
        "qe_epsilon": ALL_QEEPSILON_VARIABLES,
        "qe_epw": ALL_QEEPW_VARIABLES,
        "qe_fs": ALL_QEFS_VARIABLES,
        "qe_ld1": ALL_QELD1_VARIABLES,
        "qe_matdyn": ALL_QEMATDYN_VARIABLES,
        "qe_ph": ALL_QEPH_VARIABLES,
        "qe_pp": ALL_QEPP_VARIABLES,
        "qe_projwfc": ALL_QEPROJWFC_VARIABLES,
        "qe_pw": ALL_QEPW_VARIABLES,
        "qe_q2r": ALL_QEQ2R_VARIABLES,
        "wannier90": ALL_WANNIER90_VARIABLES,
        }
