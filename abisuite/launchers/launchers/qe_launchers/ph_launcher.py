import os

from .bases import (
                BaseQELauncher_for_outdir_prefix,
                BaseQELauncher_for_fildyn,
                BasePostPWLauncher,
                )
from ....handlers import MetaDataFile, PBSFile, QEPHInputFile
from ....handlers.file_approvers import QEPHInputParalApprover


# Watchout deadly diamond of death with the class below!!
class QEPHLauncher(BaseQELauncher_for_outdir_prefix,
                   BaseQELauncher_for_fildyn,
                   BasePostPWLauncher):
    """Launcher class for a Quantum Espresso Calculation using the ph.x script.
    """
    _calctype = "qe_ph"
    _loggername = "QEPHLauncher"
    _input_file_handler_class = QEPHInputFile
    _paral_approver_class = QEPHInputParalApprover

    def __init__(self, *args, **kwargs):
        # none of the mother classes override the init method
        # so we're safe here
        super().__init__(*args, **kwargs)
        self._is_recover_run = False

    @property
    def is_recover_run(self):
        return self._is_recover_run

    @is_recover_run.setter
    def is_recover_run(self, recover):
        self._is_recover_run = recover

    def _do_add_input_directory(self, dirpath, *args,
                                keep_original_dirname=False, **kwargs):
        keep_original_dirname = False  # force it to false
        if os.path.basename(dirpath).startswith("_ph"):
            # _phX directory, keep dirname and continue
            keep_original_dirname = True
        super()._do_add_input_directory(
                        dirpath, *args,
                        keep_original_dirname=keep_original_dirname,
                        **kwargs)

    def validate(self, *args, **kwargs):
        super().validate(*args, **kwargs)
        if not self.is_recover_run:
            return
        # check that number of pools is identical to parent calculation
        # get previous paral scheme
        with MetaDataFile.from_calculation(self.meta_data_file.parents[0],
                                           loglevel=self._loglevel) as meta:
            pbs = PBSFile.from_meta_data_file(meta, loglevel=self._loglevel)
            pbs.read()
            npool = pbs.command_arguments.get("-npool", 1)
        if npool != self.command_arguments.get("-npool", 1):
            raise ValueError("number of pools should be the same as previous "
                             f"calculation ({npool}).")
        # only one image seems it does not seem to work with more than 1
        # FIXME: check if that's always the case
        if "-nimage" in self.command_arguments:
            raise ValueError("nimage must be one when recovering.")

    def _get_target_name(self, path, *args, **kwargs):
        # special treatment if recovering
        if not self.is_recover_run:
            return super()._get_target_name(path, *args, **kwargs)
        # TODO: The following if statement is probably useless!
        if os.path.isdir(path) and os.path.basename(path).startswith("_ph"):
            # don't change the name of the '_phX' directories
            return os.path.basename(path)
        return super()._get_target_name(path, *args, **kwargs)

    def _preprocess_inputs(self, inputs):
        inputs = BaseQELauncher_for_outdir_prefix._preprocess_inputs(self,
                                                                     inputs)
        inputs = BaseQELauncher_for_fildyn._preprocess_inputs(self, inputs)
        inputs = self._overwrite_input_var(inputs, "title", self.jobname)
        # need to set fildvscf (always the same thing)
        fildvscf = "dvscf"
        inputs = self._overwrite_input_var(inputs, "fildvscf", fildvscf)
        if "recover" in inputs:
            if inputs["recover"]:
                self.is_recover_run = True
        return inputs
