import os
import tempfile
import unittest

from .bases import BaseLauncherTest, BaseLauncherTestNoInputVariables
from ..routines_for_tests import copy_calculation
from ..variables_for_tests import (
        abinit_anaddb_vars, abinit_vars, abinit_optic_vars,
        abinit_AlAs_pseudos, AbinitMrgddbCalc,
        )
from ...handlers.file_approvers.exceptions import PseudosError
from ...handlers.file_parsers import AbinitOpticInputParser
from ...launchers import (
        AbinitAnaddbLauncher, AbinitLauncher, AbinitMrgddbLauncher,
        AbinitOpticLauncher,
        )
from ...routines import full_abspath


class BaseAbinitLauncherTest(BaseLauncherTest):
    """Base test case for abinit launchers.
    """

    def test_launcher_write(self):
        super().test_launcher_write()
        # check files file has been written
        self.assertTrue(os.path.isfile(self.launcher.files_file.path))


class TestAbinitLauncher(BaseAbinitLauncherTest, unittest.TestCase):
    _calctype = "abinit"
    _input_vars = abinit_vars.copy()
    _jobname = "test_launcher"
    _launcher_class = AbinitLauncher
    _pseudos = abinit_AlAs_pseudos

    def setUp(self):
        BaseLauncherTest.setUp(self)
        self.launcher.pseudos = self._pseudos

    def test_raise_pseudos_not_exists(self):
        with self.assertRaises((PseudosError, FileNotFoundError)):
            # create new non existant pseudo
            Hpseudo2 = os.path.expanduser("~/pseudo")
            self.launcher.pseudos = [Hpseudo2]
            self.launcher.queuing_system = "local"
            self.launcher.write()

    def test_linking(self):
        # create file to link
        tempdir2 = tempfile.TemporaryDirectory()
        path = os.path.join(tempdir2.name, "odat_WFK")
        with open(path, "a") as f:
            f.write("test")
        self.launcher.add_input_file(path)
        self.launcher.queuing_system = "local"
        self.launcher.write()
        link = os.path.join(self.workdir, "input_data",
                            "idat_" + self._jobname + "_WFK")
        self.assertTrue(os.path.islink(link))
        tempdir2.cleanup()
        del tempdir2


class TestAbinitAnaddbLauncher(BaseAbinitLauncherTest, unittest.TestCase):
    """Test case for the anaddb launcher.
    """
    _calctype = "abinit_anaddb"
    _input_vars = abinit_anaddb_vars.copy()
    _jobname = "test_abinid_anaddb_launcher"
    _launcher_class = AbinitAnaddbLauncher

    def setUp(self):
        super().setUp()
        # copy calculation in a temp dir and link it
        new_calc = os.path.join(
                self.tempdir.name,
                os.path.basename(AbinitMrgddbCalc))
        copy_calculation(AbinitMrgddbCalc, new_calc)
        self.launcher.link_calculation(new_calc)


class TestAbinitMrgddbLauncher(
        BaseLauncherTestNoInputVariables, unittest.TestCase):
    """Test case for the mrgddb launcher.
    """
    _calctype = "abinit_mrgddb"
    _jobname = "test_abinit_mrgddb_launcher"
    _launcher_class = AbinitMrgddbLauncher

    def setUp(self):
        super().setUp()
        # create temporary DDB files and link them
        self._temp_ddbs = []
        self._ntemp_ddbs = 10
        for i in range(self._ntemp_ddbs):
            temp = tempfile.NamedTemporaryFile(suffix=f"{i}_DDB")
            self.launcher.add_input_file(temp.name)
            self._temp_ddbs.append(temp)

    def tearDown(self):
        for temp in self._temp_ddbs:
            del temp

    def test_ddb_links(self):
        self.assertEqual(self.launcher.input_file.nddb, self._ntemp_ddbs)
        tempnames = [x.name for x in self._temp_ddbs]
        for link in self.launcher.input_data_dir:
            self.assertIn(link.source, tempnames)
            self.assertIn(link.path, self.launcher.input_file.ddb_paths)


class TestAbinitOpticLauncher(BaseAbinitLauncherTest, unittest.TestCase):
    _calctype = "optic"
    _input_vars = abinit_optic_vars.copy()
    _jobname = "test_abinit_optic_launcher"
    _launcher_class = AbinitOpticLauncher

    def test_input_links(self):
        self.launcher.queuing_system = "local"
        # need to link the files manually since we link example files
        for varname in ("ddkfile_1", "ddkfile_2", "ddkfile_3", "wfkfile"):
            if varname != "wfkfile":
                ddk_idx = varname.split("_")[-1]
            else:
                ddk_idx = None
            self.launcher.add_input_file(
                    self.launcher.input_variables[varname].value,
                    ddk_idx=ddk_idx)
        self.launcher.write()
        links = os.listdir(self.launcher.input_data_dir.path)
        if not links:
            raise FileNotFoundError()
        dests = []
        for link in links:
            # check link exists
            path = os.path.join(self.launcher.input_data_dir.path, link)
            dests.append(os.path.abspath(os.readlink(path)))
            self.assertTrue(os.path.islink(path))
        # check that all links are good in input file
        path = self.launcher.input_file.path
        parser = AbinitOpticInputParser.from_file(path)
        parser.read()
        for name in ("ddkfile_1", "ddkfile_2", "ddkfile_3", "wfkfile"):
            self.assertIn(os.path.basename(parser.input_variables[name].value),
                          links)
            # check that linked is good
            self.assertTrue(
                any([full_abspath(abinit_optic_vars[name]) in x
                     for x in dests]))
