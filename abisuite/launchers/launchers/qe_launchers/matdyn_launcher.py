import os

from .bases import BaseQELauncher
from ....handlers import QEMatdynInputFile
from ....handlers.file_approvers.exceptions import InputFileError
from ....routines import is_list_like


class QEMatdynLauncher(BaseQELauncher):
    """Launcher class for a Quantum Espresso calculation using the
    matdyn.x script.
    """
    _loggername = "QEMatdynLauncher"
    _input_file_handler_class = QEMatdynInputFile
    _calctype = "qe_matdyn"

    def _preprocess_inputs(self, inputs):
        # setup the output file flfrq for frequencies
        flfrq = os.path.join(self.output_data_dir.path, self.jobname + ".freq")
        inputs = self._overwrite_input_var(inputs, "flfrq", flfrq)
        # setup the output file flvec and fleig files
        flvec = os.path.join(
                    self.output_data_dir.path, self.jobname + ".modes")
        inputs = self._overwrite_input_var(inputs, "flvec", flvec)
        fleig = os.path.join(self.output_data_dir.path, self.jobname + ".eig")
        inputs = self._overwrite_input_var(inputs, "fleig", fleig)
        return inputs

    def add_input_file(self, path, *args, **kwargs):
        if not is_list_like(path):
            path = (path, )
        for filename in path:
            if filename.endswith(".fc"):
                # this filename must be included in the input variable
                # but first get its path.
                basename = os.path.basename(filename)
                name = self._get_target_name(basename)
                target = os.path.join(self.input_data_dir.path, name)
                try:
                    self.input_variables["flfrc"] = target
                except ValueError as e:
                    # user didn't set input variables yet.
                    self._logger.error("Need to set input variables before "
                                       "adding input files.")
                    raise e
                self._logger.debug(f"Automatically added 'flfrc' input var: "
                                   f"{target}")
            super().add_input_file(filename, *args, **kwargs)

    def write(self, *args, **kwargs):
        try:
            super().write(*args, **kwargs)
        except InputFileError:
            # if an input file error, maybe its because user forgot to
            # add '.fc' file
            self._logger.error("An error with the input file occured. Maybe "
                               "user forgot the '.fc' file in which case "
                               "it can be added with the 'add_input_file "
                               "method.")
            raise

    def validate(self, *args, **kwargs):
        super().validate(*args, **kwargs)
        # check that the fc file has been linked
        self._check_for_input_file(".fc")
