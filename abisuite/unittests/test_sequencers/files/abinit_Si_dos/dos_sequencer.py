from abisuite import AbinitDOSSequencer as Sequencer


sequencer = Sequencer()
# SCF part
sequencer.scf_workdir = "scf_run"
sequencer.scf_input_variables = {
        "ecut": 10,
        "kptopt": 1,
        "ngkpt": [6, 6, 6],
        "nshiftk": 4,
        "shiftk": [[0.5, 0.5, 0.5], [0.5, 0.0, 0.0],
                   [0.0, 0.5, 0.0], [0.0, 0.0, 0.5]],
        "diemac": 12.0,
        "nstep": 10,
        "toldfe": 1e-6,
        "acell": [7.2078778121] * 3,
        "natom": 2,
        "ntypat": 1,
        "rprim": [[0, 0.70710678119, 0.70710678119],
                  [0.70710678119, 0, 0.70710678119],
                  [0.70710678119, 0.70710678119, 0]],
        "typat": [1, 1],
        "xred": [[0, 0, 0], [0.25, 0.25, 0.25]],
        "znucl": 14.0,
        "autoparal": 1,
        }
sequencer.scf_mpi_command = "mpirun -np 4"
sequencer.scf_pseudos = ["../pseudos/Si.psp8"]

# DOS part
sequencer.dos_workdir = "dos_run"
sequencer.dos_fine_kpoint_grid_variables = {
        "ngkpt": [10, 10, 10],
        "nshiftk": 1,
        # don't shift grid (prefered according to abinit docs)
        "shiftk": [[0.0, 0.0, 0.0]],
        "nband": 8,
        }
sequencer.dos_input_variables = {
        "ecut": 10,
        "iscf": -3,
        "prtdos": 2,
        "tolwfr": 1e-12,
        "autoparal": 1,
        }
sequencer.dos_mpi_command = "mpirun -np 4"
sequencer.dos_pseudos = ["../pseudos/Si.psp8"]
sequencer.launch()
