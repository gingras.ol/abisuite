import numpy as np
import os
import unittest

from ..routines import (
        full_abspath, is_list_like, is_scalar_or_str, return_list,
        )


class TestSimpleRoutines(unittest.TestCase):
    def test_is_list_like(self):
        tst_true_cases = ([], (0, 1), np.array((0, 1)), set((0, 1)))
        tst_false_cases = ("string", 0, -1.0, None, True)
        for tst_true_case in tst_true_cases:
            self.assertTrue(is_list_like(tst_true_case))
        for tst_false_case in tst_false_cases:
            self.assertFalse(is_list_like(tst_false_case))

    def test_is_scalar_or_str(self):
        tst_true_cases = ("string", 1, -10.0)
        tst_false_cases = (
                [], (0, 1), set((0, 1)), False, None, np.array((0, 1)))
        for tst_true_case in tst_true_cases:
            self.assertTrue(is_scalar_or_str(tst_true_case))
        for tst_false_case in tst_false_cases:
            self.assertFalse(is_scalar_or_str(tst_false_case))

    def test_full_abspath(self):
        self.assertTrue(os.path.isabs(full_abspath("~")))

    def test_return_list(self):
        self.assertEqual(return_list(None), [])
        self.assertEqual(return_list("string"), ["string"])
        self.assertEqual(return_list(["already_list"]), ["already_list"])
