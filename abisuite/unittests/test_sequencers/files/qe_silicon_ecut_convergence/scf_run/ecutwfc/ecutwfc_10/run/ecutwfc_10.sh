#!/bin/bash

MPIRUN="mpirun -np 4"
EXECUTABLE="/home/fgoudreault/Workspace/q-e/bin/pw.x -npool 4"
INPUT=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_sequencers/files/qe_silicon_ecut_convergence/scf_run/ecutwfc/ecutwfc_10/ecutwfc_10.in
LOG=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_sequencers/files/qe_silicon_ecut_convergence/scf_run/ecutwfc/ecutwfc_10/ecutwfc_10.log
STDERR=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_sequencers/files/qe_silicon_ecut_convergence/scf_run/ecutwfc/ecutwfc_10/ecutwfc_10.stderr


$MPIRUN $EXECUTABLE -input $INPUT > $LOG 2> $STDERR
