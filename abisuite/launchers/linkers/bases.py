import abc

from ... import __IMPLEMENTED_CALCTYPES__
from ...bases import BaseCalctypedUtility
from ...exceptions import DevError
from ...handlers import CalculationDirectory
from ...routines import full_abspath
from ...status_checkers.exceptions import (
        CalculationNotFinishedError, CalculationNotStartedError,
        CalculationNotConvergedError, CalculationFailedError,
        )


class BaseLinker(BaseCalctypedUtility, abc.ABC):
    """Base class for a Linker object. The main purpose of these objects is to
    link one calculation to another.

    Each Linker object is used to link a specific calculation type to another
    based on the input variables of both.

    They are not really a part of the API (even less than the file handlers).
    """
    _link_calctype = None
    """The calctype of the calculation to link."""

    def __init__(self, launcher, *args, **kwargs):
        """Linker base init method.

        Parameters
        ----------
        launcher : Launcher object
            The Launcher object onto which the Linker acts.
        """
        super().__init__(*args, **kwargs)
        if self._link_calctype is None:
            raise DevError("Need to set '_link_calctype'.")
        if self._link_calctype not in __IMPLEMENTED_CALCTYPES__:
            raise DevError(f"Unrecognized calctype: '{self._link_calctype}'.")
        self.launcher = launcher
        self.allow_calculation_not_started = False
        self.allow_calculation_failed = False
        self._calculation_to_link = None
        self._files_to_link = None
        self._files_to_copy = None
        self._directories_to_link = None
        self._directories_to_copy = None

    @property
    def calculation_to_link(self):
        return self._calculation_to_link

    @calculation_to_link.setter
    def calculation_to_link(self, calc):
        if not isinstance(calc, CalculationDirectory):
            # check if calculation to link is ready to be linked
            if not isinstance(calc, str):
                raise TypeError(
                        f"Expected a path but got '{calc}'.")
            calc = full_abspath(calc)
            if not CalculationDirectory.is_calculation_directory(calc):
                raise NotADirectoryError(
                        f"Not a calculation directory: '{calc}'.")
            calc = CalculationDirectory.from_calculation(
                    calc, loglevel=self._loglevel)
        calc.connect_to_database()
        with calc:
            if calc.calctype != self._link_calctype:
                raise TypeError(
                        f"Expected calctype to be '{self._link_calctype}' but "
                        f"got '{calc.calctype}'.")
            raisestarted = (
                    calc.status["calculation_started"] is False and not
                    self.allowed_calculation_not_started
                    )
            raisefailed = (
                    calc.status["calculation_finished"] == "error" and not
                    self.allow_calculation_failed
                    )
            if raisestarted:
                raise CalculationNotStartedError(calc.path)
            if calc.status["calculation_finished"] is False and raisestarted:
                raise CalculationNotFinishedError(calc.path)
            if raisefailed:
                raise CalculationFailedError(calc.path)
            if calc.status.get(
                    "calculation_converged", None) is False and raisestarted:
                raise CalculationNotConvergedError(calc.path)
        self._calculation_to_link = calc

    @property
    def input_variables(self):
        return self.launcher.input_variables

    @property
    def input_variables_set(self):
        """Is True if the Launchers input variables have been set.
        False otherwise.
        """
        try:
            self.input_variables
        except ValueError:
            return False
        else:
            return True

    def add_input_file(self, *args, **kwargs):
        self.launcher.add_input_file(*args, **kwargs)

    def link_calculation(self):
        # check that the launchers input variables have been set
        if not self.input_variables_set:
            raise ValueError(
                    "Need to set input variables before linking a "
                    "calculation.")
        # add children to the meta file in the calculation dir
        with self.calculation_to_link.meta_data_file as meta:
            self._link_files_from_calculation(meta)
        self.launcher.meta_data_file.add_parent(self.calculation_to_link.path)

    @abc.abstractmethod
    def _link_files_from_calculation(self, *args, **kwargs):
        pass
