#!/bin/bash

MPIRUN=""
EXECUTABLE="/home/fgoudreault/Workspace/q-e/bin/wannier90.x -pp"
INPUT=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_sequencers/files/qe_silicon/wannier_run_pp/wannier_run_pp.win
LOG=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_sequencers/files/qe_silicon/wannier_run_pp/wannier_run_pp.log
STDERR=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_sequencers/files/qe_silicon/wannier_run_pp/wannier_run_pp.stderr


$MPIRUN $EXECUTABLE ../wannier_run_pp.win > $LOG 2> $STDERR
