import os

from ..bases import SequenceCalculation
from ..scf_sequencer import SCFSequencer
from ...exceptions import DevError


class DDKSequencer(SCFSequencer):
    """Sequencer for an abinit runs that requires a ddk calculation.
    """
    _all_sequencer_prefixes = ("scf_", "ddk_", )
    _loggername = "DDKSequencer"

    def __init__(self, *args, **kwargs):
        if "ddk_" not in self._all_sequencer_prefixes:
            raise DevError("Need to set 'ddk_' in the prefixes.")
        super().__init__("abinit", *args, **kwargs)
        self._split_ddk_for_each_rfdir = False

    @property
    def ddk_input_variables(self):
        return self._ddk_input_variables

    @ddk_input_variables.setter
    def ddk_input_variables(self, input_vars):
        if not isinstance(input_vars, dict):
            raise TypeError(
                    f"Expected dict for input vars but got: {input_vars}")
        self._set_input_var_to(input_vars, "irdwfk", 1)
        self._set_input_var_to(input_vars, "nqpt", 1)
        self._set_input_var_to(input_vars, "iscf", -3)
        self._set_input_var_to(input_vars, "kptopt", 2)
        self._set_input_var_to(input_vars, "rfphon", 0)
        self._set_input_var_to(input_vars, "rfelfd", 2)
        if "rfdir" not in input_vars:
            raise ValueError("Need to set 'rfdir' in ddk calculation.")
        # compute ddk at gamma
        if "qpt" not in input_vars:
            self._logger.debug("Setting 'qpt' to [0.0, 0.0, 0.0].")
            input_vars["qpt"] = [0.0, 0.0, 0.0]
        if input_vars["qpt"] not in ([0.0, 0.0, 0.0], (0.0, 0.0, 0.0)):
            self._logger.debug("Setting 'qpt' to [0.0, 0.0, 0.0].")
            input_vars["qpt"] = [0.0, 0.0, 0.0]
        self._ddk_input_variables = input_vars

    @property
    def split_ddk_for_each_rfdir(self):
        return self._split_ddk_for_each_rfdir

    @split_ddk_for_each_rfdir.setter
    def split_ddk_for_each_rfdir(self, split):
        self._split_ddk_for_each_rfdir = split

    def init_ddk_sequence(self):
        """Init the ddk sequence calculation if required.
        """
        if self.split_ddk_for_each_rfdir:
            # get rfdir
            rfdir = self.ddk_input_variables.get("rfdir")
            for irfdircomp, rfdircomp in enumerate(rfdir):
                if rfdircomp == 0:
                    # don't compute this component
                    continue
                new_vars = self.ddk_input_variables.copy()
                new_vars["rfdir"] = [0, 0, 0]
                new_vars["rfdir"][irfdircomp] = 1
                seq = SequenceCalculation(
                        "abinit",
                        os.path.join(self.ddk_workdir, f"rfdir{irfdircomp}"),
                        new_vars, self.ddk_calculation_parameters,
                        loglevel=self._loglevel)
                scf = self.get_sequence_calculation("scf")
                seq.dependencies.append(scf)
                seq.load_geometry_from = scf
                seq.load_kpts_from = scf
                self.sequence.append(seq)
        else:
            seq = SequenceCalculation(
                    "abinit", self.ddk_workdir, self.ddk_input_variables,
                    self.ddk_calculation_parameters, loglevel=self._loglevel)
            scf = self.get_sequence_calculation("scf")
            seq.dependencies.append(scf)
            seq.load_geometry_from = scf
            seq.load_kpts_from = scf
            self.sequence.append(seq)

    def init_sequence(self):
        SCFSequencer.init_sequence(self)
        self.init_ddk_sequence()
