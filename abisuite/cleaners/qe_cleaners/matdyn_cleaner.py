from .bases import BaseQECleaner


class QEMatdynCleaner(BaseQECleaner):
    """Cleaner class for a qe_matdyn calculation.

    For now this class cleans nothing!
    """
    _calctype = "qe_matdyn"
    _loggername = "QEMatdynCleaner"

    def get_files_to_delete(self):
        return []
