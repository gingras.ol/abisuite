import abc

from ..bases import BaseCalctypedUtility
from ..status_checkers.exceptions import (
        CalculationNotFinishedError, CalculationNotConvergedError)


class BaseCleaner(BaseCalctypedUtility, abc.ABC):
    """Base class for any cleaner utility.
    """

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._calculation_directory = None
        self.nfiles_cleaned = 0
        self.nlinks_cleaned = 0
        self.ngb_cleaned = 0

    @property
    def calculation_directory(self):
        """The cleaner's CalculationDirectory.
        """
        if self._calculation_directory is not None:
            return self._calculation_directory
        raise ValueError("Need to set the 'calculation_directory'.")

    @calculation_directory.setter
    def calculation_directory(self, calc):
        if isinstance(calc, str):
            from ..handlers import CalculationDirectory
            calc = CalculationDirectory.from_calculation(
                    calc, loglevel=self._loglevel)
        self._calculation_directory = calc

    def clean(self, force=False, dry=False):
        """Cleans the calculation directory of cumbersome files
        (wafefunctions, densities, etc.).

        If calculation has errored or is not finished, an error is thrown.

        Parameters
        ----------
        dry: bool, optional
            If True, dry mode is activated: files that will be deleted will
            only be printed on screen. No files are actually deleted.
        force: bool, optional
            If True, whatever the calculation status, the cleaning process
            will be engaged.
        """
        status = self.calculation_directory.status["calculation_finished"]
        self.nfiles_cleaned = 0
        self.ngb_cleaned = 0
        if status is not True and not force:
            raise CalculationNotFinishedError(self.calculation_directory)
        if "calculation_converged" in self.calculation_directory.status:
            converged = self.calculation_directory.status[
                    "calculation_converged"]
            if not converged and not force:
                raise CalculationNotConvergedError(self.calculation_directory)
        # calculation is ready to clean
        for handler in self.get_files_to_delete():
            if not handler.islink:
                self.ngb_cleaned += handler.file_size_Gb
                self.nfiles_cleaned += 1
            else:
                self.nlinks_cleaned += 1
            if dry:
                print(handler.path)
            else:
                handler.delete()

    def get_files_to_delete(self):
        """Returns the list of file handlers to delete.

        Returns
        -------
        list: The list of file handlers to delete.
        """
        files = []
        for handler in self.calculation_directory.walk(paths_only=False):
            if self.delete_this_file(handler):
                files.append(handler)
        return files

    @abc.abstractmethod
    def delete_this_file(self, handler):
        """Returns True if we want to delete this file.

        Parameters
        ----------
        handler: Handler object
            The file handler to investigate.

        Returns
        -------
        bool: True if we want to delete handler. False otherwise.
        """
        if handler.islink:
            if handler.is_broken_link:
                return True
        if handler.basename.startswith("core."):
            # delete core files
            return True
        return False
