#!/bin/bash

MPIRUN="mpirun -np 4"
EXECUTABLE="/home/fgoudreault/Workspace/q-e/bin/pw.x -npool 4"
INPUT=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_workflows/test_files/qe_Pb/epw_with_lattice_expansion/nscf_runs/600.0K/600.0K.in
LOG=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_workflows/test_files/qe_Pb/epw_with_lattice_expansion/nscf_runs/600.0K/600.0K.log
STDERR=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_workflows/test_files/qe_Pb/epw_with_lattice_expansion/nscf_runs/600.0K/600.0K.stderr


$MPIRUN $EXECUTABLE -input $INPUT > $LOG 2> $STDERR
