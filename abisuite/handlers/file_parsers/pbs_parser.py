from .bases import BaseFormattedCalculationFileParser, SubParsersList
from .subparsers.pbs_subparsers import (
                GridEngineSubParser, ModulestoLoadSubParser,
                ModulestoUnloadSubParser, ModulestoUseSubParser,
                ModulestoSwapSubParser,
                SlurmSubParser, PBSSubParser
                )
from ..file_structures import PBSStructure


SUBPARSERS = (
        GridEngineSubParser, ModulestoLoadSubParser, ModulestoUnloadSubParser,
        ModulestoUseSubParser, ModulestoSwapSubParser, SlurmSubParser,
        PBSSubParser,
        )


class PBSParser(BaseFormattedCalculationFileParser):
    """Parser for a pbs file. The pbs file structure is assumed
    to be the same as the corresponding abilaunch pbs writer.
    """
    _loggername = "PBSParser"
    _expected_ending = ".sh"
    _structure_class = PBSStructure

    def _extract_data(self):
        super()._extract_data(strip_spaces=True, strip_newlines=True)

    def _extract_data_from_lines(self, lines):
        skip = 0
        # remove empty lines
        lines = [line for line in lines if line]
        # init data dict
        data = {"modules_to_load": [], "modules_to_unload": [],
                "modules_to_swap": [], "modules_to_use": [],
                }
        to_remove = []
        subparsers = SubParsersList(SUBPARSERS)
        for j, line in enumerate(lines):
            if j < skip:  # pragma: no cover
                continue
            # interpreter
            if line.startswith("#!"):
                # data["interpreter"] = line.split("!")[-1]
                to_remove.append(line)
                continue
            for trigger, subparser in subparsers.items():
                if trigger in line:
                    s = subparser(lines[j:], loglevel=self._loglevel)
                    data.update(s.data)
                    subparsers.remove(subparser)
                    skip = j + s.ending_relative_index
                    to_remove += lines[j:skip]
                    break
        # if queuing system is not known from here => local
        if "queuing_system" not in data:
            data.setdefault("queuing_system", "local")
            data.setdefault("nodes", 1)
            data.setdefault("ppn", 1)
        for line in to_remove:
            lines.remove(line)
        command = self._parse_command(lines)
        data.update(command)
        return data

    def _parse_command(self, lines):
        # parse the command to get all the paths to executable
        # and input/output files for the abinit command
        command_line_index = None
        other_lines = []
        data = {"lines_before": [],
                "lines_after": []}
        for i, line in enumerate(lines):
            if ">" in line or "$EXECUTABLE" in line:
                command_line_index = i
                data["command_line"] = line
            elif line.startswith("MPIRUN"):
                # MPIRUN="mpirun -npernode=12"
                data["mpi_command"] = line.split('"')[1]
            elif line.startswith("EXECUTABLE"):
                data["command"] = line.split("=")[-1].strip('"')
            elif line.startswith("INPUT"):
                data["input_file_path"] = line.split("=")[-1].strip('"')
            elif line.startswith("LOG"):
                data["log_path"] = line.split("=")[-1].strip('"')
            elif line.startswith("STDERR"):
                data["stderr_path"] = line.split("=")[-1].strip('"')
            else:
                other_lines.append(i)
        if command_line_index is None:  # pragma: no cover
            raise LookupError(f"Could not find executable command line call in"
                              f" {self.path}.")
        for index in other_lines:
            if index < command_line_index:
                data["lines_before"].append(lines[index])
            else:
                data["lines_after"].append(lines[index])
        return data

    @classmethod
    def _filepath_from_meta(cls, meta):
        return meta.pbs_file_path
