import abc


class BaseCalculationError(Exception, abc.ABC):
    """Base class for any calculation error.
    """
    def __init__(self, calc, *args, **kwargs):
        """Custom init method that can create a custom message in case
        a Calculation dir is passed as an argument.

        Parameters
        ----------
        calc: str or CalculationDirectory
            If a string is passed, a regular error is raised.
            Otherwise a custom message is printed out.
        """
        if isinstance(calc, str):
            super().__init__(calc, *args, **kwargs)
            return
        # put import here to prevent circular imports
        from ..handlers import CalculationDirectory
        if not isinstance(calc, CalculationDirectory):
            raise TypeError(calc)
        msg = self._get_custom_msg(calc)
        super().__init__(msg, *args, **kwargs)

    @abc.abstractmethod
    def _get_custom_msg(self, *args, **kwargs):
        pass


class CalculationFailedError(BaseCalculationError):
    """An error raised when a Calculation errored somehow.
    """

    def _get_custom_msg(self, calc):
        return f"Calculation has errored '{calc.calctype}' at '{calc.path}'"


class CalculationNotFinishedError(BaseCalculationError):
    """An error raised when a Calculation is not finished.
    """

    def _get_custom_msg(self, calc):
        return (f"Calculation has not finished '{calc.calctype}' at "
                f"'{calc.path}'")


class CalculationNotConvergedError(BaseCalculationError):
    """An error raised when a Calculation is not converged.
    """

    def _get_custom_msg(self, calc):
        return (f"Calculation has not converged '{calc.calctype}' at "
                f"'{calc.path}'")


class CalculationNotStartedError(BaseCalculationError):
    """An error raised when a Calculation has not started.
    """

    def _get_custom_msg(self, calc):
        return (f"Calculation has not started '{calc.calctype}' at "
                f"'{calc.path}'")
