from ..bases import BaseStructure


class AbinitAnaddbPhfrqStructure(BaseStructure):
    """Structure class for an anaddb PHFRQ file.
    """
    all_attributes = ("frequencies", "nbranches", "nqpts", )
