from ..bases import BaseInputApprover
from ...file_structures import QEEpsilonInputStructure
from ....variables import ALL_QEEPSILON_VARIABLES


class QEEpsilonInputApprover(BaseInputApprover):
    """Class that checkes the input variables for a epsilon.x calculation.
    """
    _loggername = "QEEpsiloninputApprover"
    _variables_db = ALL_QEEPSILON_VARIABLES
    _structure_class = QEEpsilonInputStructure
