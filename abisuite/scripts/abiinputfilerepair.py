import glob
import os
import warnings

from abisuite.handlers import MetaDataFile, CALCTYPES_TO_INPUT_FILE_CLS
from abisuite.routines import full_abspath
from abisuite.variables import CALCTYPES_TO_INPUT_VARIABLES
from abisuite.scripts.bases import BaseAbisuiteArgParser


class AbipbsrepairArgParser(BaseAbisuiteArgParser):
    """Argparser for abipbsrepair script."""

    def create_parser(self):
        parser = super().create_parser()
        parser.add_argument(
                "to_repair", action="store", type=str,
                default=".",
                help=("Calculation directory(ies) containing input"
                      " files to repair."),
                nargs="*")
        parser.add_argument(
                "--dry-run", "-d", action="store_true",
                help="Dry run mode, do nothing. Just print what happens")
        return parser


if __name__ == "__main__":
    parser = AbipbsrepairArgParser()
    args = parser.parse_args()
    to_repairs = args.to_repair
    dry_run = args.dry_run
    loglevel = args.loglevel
    all_paths = []
    for to_repair in to_repairs:
        if "*" in to_repair:
            all_paths += glob.glob(to_repair)
        else:
            all_paths.append(to_repair)
    for to_repair in all_paths:
        here = full_abspath(to_repair)
        if dry_run:
            print(  # noqa: T001
                    f"Trying to repair input file file in: '{here}'")
            continue
        try:
            # first get calctype and input file path
            with MetaDataFile.from_calculation(
                            here, loglevel=loglevel) as meta:
                calctype = meta.calctype
                path = meta.input_file_path
                workdir = meta.calc_workdir
            with CALCTYPES_TO_INPUT_FILE_CLS[calctype].from_file(
                            path, loglevel=loglevel) as input_file:
                print(  # noqa: T001
                        f"Trying to repair input file in: '{here}'")
                # we need to get the new working directory and hope its
                # basename is the same as the old one
                basename = os.path.basename(workdir)
                if basename not in input_file.path:
                    warnings.warn("Unable to repair in: '{here}'.")
                    continue
                # ok can repair from here. get new relative paths
                for varname, var in input_file.input_variables.items():
                    # add the sep. here to make sure the split goes well
                    if CALCTYPES_TO_INPUT_VARIABLES[calctype][varname].get(
                                "path_convertible", False) is False:
                        # this variable cannot be converted
                        continue
                    # this variable can be converted => thus convert it!
                    new = os.path.join(
                            workdir,
                            var.value.split(basename + os.path.sep)[-1])
                    input_file.input_variables[varname] = new
                else:
                    # loop ended, rewrite the input file
                    input_file.write(overwrite=True)
                print("Repairing attempt done.")  # noqa: T001
        except FileNotFoundError:
            # this is not a calculation directory, skip it
            continue
