import abc

from ..bases import BaseWriter, BaseInputWriter


class BaseFilesWriter(BaseWriter, abc.ABC):
    """Base class for files file writers.
    """
    _file_suffix = ".files"

    @property
    def lines(self):
        lines = []
        # first is input file
        lines.append(self.input_file_path + "\n")
        # second is output file
        lines.append(self.output_file_path + "\n")
        return lines


class BaseAbinitInputWriter(BaseInputWriter):
    """Base class for some of the abinit input files writers.
    """
    @property
    def lines(self):
        # actually do the writing
        # gather lines by block
        blocks = self._get_variable_blocks(self.input_variables)
        # create the lines for the file
        lines = []
        # write blocks in alphabetical order
        for block_name, block in sorted(blocks.items()):
            lines.append(f"#== {block_name} ==#\n")
            # write variables in alphabetical order
            for _name, abivar in sorted(block.items()):
                for var in abivar:
                    lines.append(str(var))
            # separate blocks with 1 blank line
            lines.append("\n")
        # remove final blanck space such that file doesnt end with blank line
        lines.pop(-1)
        return lines

    def _get_variable_blocks(self, variables):
        # organize abinit input variables by block
        blocks = {}
        for abivar in variables.values():
            # abivar is an AbinitInputVariable object
            # create block dict if it doesnt exist
            block = blocks.setdefault(abivar.block, {})
            # create a list for each variable as there may be many
            # similar variables because of multidtset mode
            namelist = block.setdefault(abivar.basename, [])
            namelist.append(abivar)
        return blocks
