from ..bases import BaseInputFileHandler
from ...file_approvers import QEEpsilonInputApprover
from ...file_parsers import QEEpsilonInputParser
from ...file_structures import QEEpsilonInputStructure
from ...file_writers import QEEpsilonInputWriter


class QEEpsilonInputFile(BaseInputFileHandler):
    """File handler for a Quantum Espresso epsilon.x input file.
    """
    _approver_class = QEEpsilonInputApprover
    _loggername = "QEEpsilonInputFile"
    _parser_class = QEEpsilonInputParser
    _structure_class = QEEpsilonInputStructure
    _writer_class = QEEpsilonInputWriter
