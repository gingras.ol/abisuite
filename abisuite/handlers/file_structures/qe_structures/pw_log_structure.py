from .bases import BaseQELogStructure
from ..bases import BaseSCFLogStructure


class QEPWLogStructure(BaseQELogStructure, BaseSCFLogStructure):
    """Structure for a pw.x log file.
    """
    all_attributes = (
            "atomic_positions", "crystal_axes", "eigenvalues", "etot",
            "fermi_energy", "final_atomic_positions",
            "final_geometry",
            "input_variables", "k_points", "lattice_parameters",
            "parallelization", "pressure", "stress_tensor",
            "timing", "total_energy"
            )
    optional_attributes = (
            "eigenvalues", "fermi_energy", "final_atomic_positions",
            "final_geometry", "parallelization", "pressure", "stress_tensor",
            "timing", "total_energy",
            )

    @property
    def etot(self):
        return self.total_energy

    @etot.setter
    def etot(self, etot):
        self._etot = etot
