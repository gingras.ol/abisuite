import os

import numpy as np

from .individual_phonon_sequencers import AbinitIndividualPhononSequencer
from ..bases import SequenceCalculation
from ..phonon_dispersion_sequencer import BasePhononDispersionSequencer
from ...constants import HA_TO_CM
from ...handlers import AbinitAnaddbLogFile, AbinitLogFile


class AbinitPhononDispersionSequencer(
        BasePhononDispersionSequencer, AbinitIndividualPhononSequencer):
    """A sequencer that can launch phonon calculations one qpt at a time
    and compute the phonon dispersion with Abinit.
    """
    _all_sequencer_prefixes = (
            "scf_", "phonons_", "ddk_", "mrgddb_", "anaddb_", "plot_", )
    _loggername = "AbinitPhononDispersionSequencer"

    def __init__(self, *args, **kwargs):
        AbinitIndividualPhononSequencer.__init__(self, *args, **kwargs)
        BasePhononDispersionSequencer.__init__(
                self, AbinitIndividualPhononSequencer, *args, **kwargs)

    # FG: 2021/04/21
    # TODO: rebase this with the QEPhononDispersion sequence_completed
    @property
    def sequence_completed(self):
        if not self.sequence:
            # FIXME: code not dry with BaseSequencer class...
            try:
                self.init_sequence()
            except (TypeError, ValueError, KeyError):
                # some variables are not set yet
                return False
            except AttributeError as err:
                self._logger.exception(
                        f"An AttributeError occured within sequencer '{self}'."
                        )
                raise err
        try:
            self.get_sequence_calculation("anaddb_")
        except LookupError:
            # anaddb not present in sequence
            return False
        return AbinitIndividualPhononSequencer.sequence_completed.fget(self)

    @property
    def anaddb_input_variables(self):
        return self._anaddb_input_variables

    @anaddb_input_variables.setter
    def anaddb_input_variables(self, input_vars):
        # for abinit we need to generate explicitely the qpts in the path
        qpts = [list(x.values())[0] for x in self.qpoint_path]
        qph1l = []
        for qpt_start, qpt_end in zip(qpts[:-1], qpts[1:]):
            # go through numpy arrays for math manipulations
            if len(qpt_start) > 3:
                raise ValueError("qpts must be len-3 vectors.")
            delta = (np.array(qpt_end) -
                     np.array(qpt_start)) / self.qpoint_path_density
            for i in range(self.qpoint_path_density):
                qpt = np.array(qpt_start) + i * delta
                qpt = qpt.tolist()
                qpt.append(1.0)  # weight
                qph1l.append(qpt)
        # append last vector as well
        qph1l.append(qpt_end + [1.0])
        input_vars["qph1l"] = qph1l
        input_vars["nph1l"] = len(qph1l)
        # make check for LOTO splitting
        if self.compute_electric_field_response:
            if input_vars.get(
                    "nph2l", 0) == 0 or not len(input_vars.get("qph2l", [])):
                raise ValueError(
                        "Computed electric field response but didnt give any "
                        "direction to compute LO-TO splitting (nph2l, qph2l)."
                        )
        self._set_input_var_to(
                input_vars, "ngqpt", self.phonons_qpoint_grid)
        self._anaddb_input_variables = input_vars

    def init_sequence(self):
        BasePhononDispersionSequencer.init_sequence(self)
        if not self._ready_for_post_phonons_sequence():
            return
        self.init_mrgddb_sequence()
        self.init_anaddb_sequence()

    def init_phonons_sequence(self):
        if self._nphonons is not None:
            AbinitIndividualPhononSequencer.init_phonons_sequence(self)
            return
        # need to determine the qpts for the given qpt grid
        # do this by creating an extra phonon run which only serves the
        # purpose of getting the qpt grid
        # only do 1 shift of 0 to include Gamma
        invars = {
                "kptopt": 1, "ngkpt": self.phonons_qpoint_grid, "nshiftk": 1,
                "shiftk": [0.0, 0.0, 0.0], "prtvol": -1, "nstep": 0,
                "toldfe": 1e-1,  # dummy (not actually used)
                "ecut": 1.0,     # dummy
                }
        calc_params = self.phonons_calculation_parameters.copy()
        calc_params.update(
                {"queuing_system": "local",
                 "mpi_command": "",
                 })

        calc = SequenceCalculation(
                "abinit",
                os.path.join(self.phonons_workdir + "_qgrid_generation"),
                invars, calc_params,
                bypass_convergence_check=True,
                )
        calc.load_geometry_from = self.get_sequence_calculation("scf")
        self.sequence.append(calc)
        # if completed sequence from here, load the generated qpts
        with calc.calculation_directory as calcdir:
            if not calcdir.exists:
                return
            if calcdir.status["calculation_finished"] is True:
                with AbinitLogFile.from_calculation(
                        calc.workdir, loglevel=self._loglevel) as log:
                    self._qpts_list = log.input_variables["kpt"]
                    self.nphonons = len(self._qpts_list)
                self.init_phonons_sequence()

    def init_mrgddb_sequence(self):
        mrgddb_run = SequenceCalculation(
                "abinit_mrgddb", self.mrgddb_workdir,
                {},  # no input vars for mrgddb
                self.mrgddb_calculation_parameters,
                loglevel=self._loglevel)
        # add all phonon calculations as dependencies
        for calc in self.get_sequence_calculation("phonons"):
            # just skip the qgrid generation part and irreps determination
            if "qgrid_generation" in calc.workdir:
                continue
            if "irreps_determination" in calc.workdir:
                continue
            mrgddb_run.dependencies.append(calc)
        self.sequence.append(mrgddb_run)

    def init_anaddb_sequence(self):
        # do the anaddb calc
        anaddb_run = SequenceCalculation(
                "abinit_anaddb", self.anaddb_workdir,
                self.anaddb_input_variables,
                self.anaddb_calculation_parameters,
                loglevel=self._loglevel)
        # only the mrgddb calculation as dependency
        anaddb_run.dependencies.append(
                self.get_sequence_calculation("mrgddb"))
        self.sequence.append(anaddb_run)

    def launch(self, *args, **kwargs):
        BasePhononDispersionSequencer.launch(self, *args, **kwargs)

    def _get_phonon_dispersion_from(self):
        return self.anaddb_workdir

    def _get_phonon_qpt_coordinates(self, iph):
        return self._qpts_list[iph]

    # def _get_phonons_specific_input_variables(self, iph):
    #     # returns specific input variable for phonon number 'iph'.
    #     qpt = self._qpts_list[iph]
    #     if not isinstance(qpt, list):
    #         qpt = list(qpt)
    #     spec = {"kptopt": 3, "rfdir": [1, 1, 1], "rfphon": 1, "irdwfk": 1,
    #             "irdddk": 0, "qpt": qpt, "nqpt": 1}
    #     if qpt == [0.0, 0.0, 0.0]:
    #         # read ddk if electric field response
    #         if self.compute_electric_field_response:
    #             spec["irdddk"] = 1
    #             # compute electric field response only for gamma
    #             spec["rfelfd"] = 3
    #         # set kptopt to 2 instead for Gamma because we can use TR syms
    #         spec["kptopt"] = 2
    #     try:
    #         spec.update(self.phonons_specific_input_variables[iph])
    #     except ValueError:
    #         # don't mind if phonons specific input vars are not set
    #         pass
    #     return spec

    def _post_fix_phonon_dispersion(self, phdisp_obj):
        # need to convert freqs in Hartrees into cm-1
        phdisp_obj.frequencies *= HA_TO_CM
        if not self.compute_electric_field_response:
            # nothing to do
            return
        # need to fix fot the LO-TO splitting
        # get corrected frequencies at Gamma from output file
        with AbinitAnaddbLogFile.from_calculation(
                self.anaddb_workdir, loglevel=self._loglevel) as logfile:
            # get correction
            correction = logfile.non_analytical_gamma_corrections[
                    "eigenvalues"]
        # find gamma vector (if present (usually it is))
        for i, qpt in enumerate(phdisp_obj.qpts):
            if list(qpt) in ([0.0, 0.0, 0.0], [1.0, 1.0, 1.0]):
                phdisp_obj.frequencies[i] = np.array(correction) * HA_TO_CM

    def _ready_for_post_phonons_sequence(self):
        if self._nphonons is None:
            return False
        if self.phonons_qpoints_split_by_irreps is not None:
            for iqpt in self.phonons_qpoints_split_by_irreps:
                if iqpt - 1 not in self._irreps:
                    return False
        return True
