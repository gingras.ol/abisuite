from .bases import BaseWannier90FileHandler
from ..bases import BaseInputFileHandler
from ...file_approvers import Wannier90InputApprover
from ...file_parsers import Wannier90InputParser
from ...file_structures import Wannier90InputStructure
from ...file_writers import Wannier90InputWriter


class Wannier90InputFile(BaseInputFileHandler, BaseWannier90FileHandler):
    """File handler for a wannier90.x input file.
    """
    _approver_class = Wannier90InputApprover
    _loggername = "Wannier90InputFile"
    _parser_class = Wannier90InputParser
    _structure_class = Wannier90InputStructure
    _writer_class = Wannier90InputWriter
