import os
import unittest

from .bases import (
        BaseWriterTest, BaseInputWriterTest,
        )
from ..variables_for_tests import (
        abinit_vars, abinit_anaddb_vars, abinit_optic_vars, Hpseudo,
        )
from ...handlers.file_parsers import (
        AbinitAnaddbFilesParser, AbinitAnaddbInputParser, AbinitInputParser,
        AbinitFilesParser, AbinitMrgddbInputParser, AbinitOpticInputParser,
        AbinitOpticFilesParser,
        )
from ...handlers.file_writers import (
        AbinitAnaddbFilesWriter, AbinitAnaddbInputWriter, AbinitInputWriter,
        AbinitFilesWriter, AbinitMrgddbInputWriter, AbinitOpticInputWriter,
        AbinitOpticFilesWriter,
        )


class BaseFilesWriterTest(BaseWriterTest):
    """Base class for Test cases of abinit files file writer classes.
    """

    def setUp(self, write=True):
        super().setUp(write=False)
        self.writer.path = os.path.join(self.tempdir.name, "test.files")
        self.writer.input_file_path = "test.in"
        self.writer.output_file_path = "test.out"
        if write:
            self.writer.write()


class TestAbinitAnaddbFilesWriter(BaseFilesWriterTest, unittest.TestCase):
    """Test case for the anaddb files file writer.
    """
    _parser_class = AbinitAnaddbFilesParser
    _writer_class = AbinitAnaddbFilesWriter

    def setUp(self):
        super().setUp(write=False)
        self.writer.ddb_file_path = "test_ddb"
        self.writer.band_structure_file_path = "test_band_structure"
        self.writer.gkk_file_path = "test_gkk"
        self.writer.eph_data_prefix = "test_eph_data_prefix"
        self.writer.ddk_file_path = "test_ddk"
        self.writer.write()


class TestAbinitAnaddbInputWriter(BaseInputWriterTest, unittest.TestCase):
    _input_vars = abinit_anaddb_vars
    _parser_class = AbinitAnaddbInputParser
    _writer_class = AbinitAnaddbInputWriter


class TestAbinitFilesWriter(BaseFilesWriterTest, unittest.TestCase):
    _parser_class = AbinitFilesParser
    _writer_class = AbinitFilesWriter

    def setUp(self):
        super().setUp(write=False)
        self.writer.output_data_dir = self.tempdir.name
        self.writer.output_data_prefix = "odat_test"
        self.writer.input_data_prefix = "idat_test"
        self.writer.input_data_dir = self.tempdir.name
        self.writer.tmp_data_dir = self.tempdir.name
        self.writer.tmp_data_prefix = "tmp_test"
        self.writer.pseudos = (Hpseudo, )
        self.writer.write()


class TestAbinitMrgddbInputWriter(BaseWriterTest, unittest.TestCase):
    """Test case for the mrgddb input file writer.
    """
    # NOTE: we use BaseWriterTest instead of BaseInputWriterTest because mrgddb
    # input files don't have input variables.
    _parser_class = AbinitMrgddbInputParser
    _writer_class = AbinitMrgddbInputWriter

    def setUp(self):
        super().setUp(write=False)
        self._properties = {
                "output_file_path": "test_output",
                "title": "test_title",
                "ddb_paths": ["test_ddb_path1", ],
                }
        for attr, value in self._properties.items():
            setattr(self.writer, attr, value)
        self.writer.write()


class TestAbinitOpticFilesWriter(BaseFilesWriterTest, unittest.TestCase):
    """Test case for the optic files file writer.
    """
    _parser_class = AbinitOpticFilesParser
    _writer_class = AbinitOpticFilesWriter

    def setUp(self):
        super().setUp(write=False)
        self.writer.output_data_dir = self.tempdir.name
        self.writer.output_data_prefix = "odat_test"
        self.writer.write()


class TestAbinitInputWriter(BaseInputWriterTest, unittest.TestCase):
    _input_vars = abinit_vars
    _parser_class = AbinitInputParser
    _writer_class = AbinitInputWriter


class TestAbinitOpticInputWriter(BaseInputWriterTest, unittest.TestCase):
    _input_vars = abinit_optic_vars
    _parser_class = AbinitOpticInputParser
    _writer_class = AbinitOpticInputWriter
