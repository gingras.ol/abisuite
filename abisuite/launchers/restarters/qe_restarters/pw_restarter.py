from .bases import BaseQERestarter
from ....handlers import QEPWInputFile, QEPWLogFile


def get_qe_pw_final_geometry(workdir):
    """Returns a dict of variables containing the optimized input variables
    computed by a given workdir.

    Parameters
    ----------
    workdir: str
        The workdir where to pull the final geometry from.
    """
    with QEPWInputFile.from_calculation(workdir) as inp:
        ibrav = inp.input_variables["ibrav"]
        atomic_positions = inp.input_variables["atomic_positions"].copy()
        atomic_species = inp.input_variables["atomic_species"]
        prev_calculation = inp.input_variables["calculation"]
        ntyp = inp.input_variables["ntyp"]
        nat = inp.input_variables["nat"]
        pseudo_dir = inp.input_variables["pseudo_dir"]
    to_rtn = {"atomic_positions": atomic_positions,
              "atomic_species": atomic_species,
              "ibrav": ibrav, "pseudo_dir": pseudo_dir,
              "nat": nat, "ntyp": ntyp,
              }
    with QEPWLogFile.from_calculation(workdir) as log:
        final_positions = log.final_atomic_positions
        atomic_positions["positions"].update(final_positions)
        if prev_calculation == "relax":
            return to_rtn
        # also return new celldm(*)s the computation will depend of the
        # type of bravais lattice since it is rather comfusing how QE
        # prints the final results combined with the way they are input...
        # they basically prints the optimized lattice vectors
        assert prev_calculation == "vc-relax"
        vectors = log.final_geometry["primitive_vectors"]
        # documentation for ibrav:
        # https://www.quantum-espresso.org/Doc/INPUT_PW.html#idm200
        if ibrav == 2:
            # FCC. new celldm(1) is 2 * any non zero element * previous
            celldm = inp.input_variables["celldm(1)"]
            celldm *= 2 * abs(vectors[0][0])
            to_rtn["celldm(1)"] = celldm
            return to_rtn
        if ibrav == 4:
            # hexagonal and trigonal lattices
            # celldm1 = a
            # celldm3 = c/a
            celldm1 = inp.input_variables["celldm(1)"]
            to_rtn["celldm(1)"] = celldm1 * vectors[0][0]
            to_rtn["celldm(3)"] = vectors[2][2]
            return to_rtn
        if ibrav == 7:
            # body centered tetragonal
            # celldm(1) = a
            # celldm(2) = c/a
            celldm1 = inp.input_variables["celldm(1)"]
            to_rtn["celldm(1)"] = celldm1 * vectors[0][0] * 2
            to_rtn["celldm(3)"] = vectors[2][2] * 2
            return to_rtn
        raise NotImplementedError(f"ibrav = {ibrav.value}")


class QEPWRestarter(BaseQERestarter):
    """Restarter class for a qe_pw calculation for the pw.x script of
    Quantum Espresso.
    """
    _calctype = "qe_pw"
    _loggername = "QEPWRestarter"

    def recover_calculation(self, *args, use_last_geometry=False, **kwargs):
        """Recover a calculation that has errored or that one want to restart
        or copy from.

        Parameters
        ----------
        use_last_geometry : bool, optional
            If True, the last atomic geometry is used in the
            case of a 'relax' calculation.
        """
        super().recover_calculation(*args, **kwargs)
        if not use_last_geometry:
            # nothing else to do
            return
        # need to dig for the last geometry
        # for now only works for the 'relax' case.
        updated_vars = get_qe_pw_final_geometry(
                self.calculation_to_recover.path)
        self.launcher.input_variables.update(updated_vars)
