from .bases import BaseQELogTimingSubParser
from ...bases import BaseSubParser
from .....routines import decompose_line


class QEProjwfcLogTimingSubParser(BaseQELogTimingSubParser):
    _loggername = "QEProjwfcLogTimingSubParser"
    trigger = "PROJWFC      :"


class QEProjwfcLogAtomicStatesSubParser(BaseSubParser):
    _loggername = "QEProjwfcLogAtomicStatesSubParser"
    trigger = "Atomic states"
    subject = "atomic_states"

    def _extract_data_from_lines(self, lines):
        states_started = False
        states = []
        for j, line in enumerate(lines):
            if "state #" in line:
                # example: state #   1: atom   1 (Sr ), wfc  1 (l=0 m= 1)
                states_started = True
                atom_number = self._extract_atom_number_from_line(line)
                atom_specie = self._extract_atomic_specie_from_line(line)
                wfc_number = self._extract_wfc_number_from_line(line)
                l, m = self._extract_quantum_numbers_from_line(line)
                states.append({
                        "atom_number": atom_number,
                        "atom_specie": atom_specie,
                        "wfc": wfc_number,
                        "l": l,
                        "m": m,
                        })
                continue
            if "state #" not in line and states_started:
                # reached the end
                self.ending_relative_index = j + 1
                return states

    def _extract_quantum_numbers_from_line(self, line):
        splitted = line.split("l=")[-1].split("m=")
        l_quantum_number = int(splitted[0].strip(" "))
        m_quantum_number = int(splitted[-1].split(")")[0].strip(" "))
        return l_quantum_number, m_quantum_number

    def _extract_wfc_number_from_line(self, line):
        splitted = line.split("wfc")[-1].split("(")[0].strip(" ")
        return int(splitted)

    def _extract_atomic_specie_from_line(self, line):
        return line.split("(")[1].split(")")[0].strip(" ")

    def _extract_atom_number_from_line(self, line):
        splitted = line.split("atom")[-1]
        atom = splitted.split("(")[0].strip(" ")
        return int(atom)


class QEProjwfcLogBandCharacterSubParser(BaseSubParser):
    _loggername = "QEProjwfcLogBandCharacterSubParser"
    trigger = "k = "
    subject = "band_character"

    def _extract_data_from_lines(self, lines):
        data = []
        skip = 0
        for j, line in enumerate(lines):
            if j < skip:
                continue
            if self.trigger in line:
                band_character, end = self._extract_band_character_this_kpt(
                                        lines[j:])
                skip = j + end
                data.append(band_character)
                continue
            if "Lowdin Charges" in line:
                # we're done
                self.ending_relative_index = j
                return data
        else:
            raise LookupError("Something happened while extracting data for "
                              "band characters.")

    def _extract_band_character_this_kpt(self, lines):
        # k =   0.0000000000  0.0000000000  0.0000000000
        # ==== e(   1) =   -62.67241 eV ====
        #  psi = 0.969*[#  11]+0.012*[#   5]+0.012*[#  10]+0.002*[#  26]+0.002*[#  34]+  # noqa
        #       +0.001*[#  21]+0.001*[#  29]+
        # |psi|^2 = 1.000
        # ==== e(   2) =   -32.86919 eV ====
        #  psi = 0.489*[#  13]+0.489*[#  14]+0.010*[#  23]+0.010*[#  32]+
        # |psi|^2 = 1.000
        # .....
        data = {}
        eigs = []
        characters = []
        # first line is the kpt coordinates
        kptline = lines[0]
        s, i, f = decompose_line(kptline)
        if len(f) != 3:
            raise LookupError("Kpt coordinates not of len = 3")
        data["kpt"] = f
        skip = 0
        for j, line in enumerate(lines[1:]):
            if j < skip:
                continue
            if "==== e(" in line:
                # the eigenvalue is on this line
                s, i, f = decompose_line(line)
                eigs.append(f[0])
                chars, end = self._extract_band_character_this_eig(
                                lines[j + 1:])
                characters.append(chars)
                skip = j + end
                continue
            if self.trigger in line or "Lowdin Charges" in line:
                # we're done for this kpt
                data["band_character"] = characters
                data["eigenvalues"] = eigs
                assert len(characters) == len(eigs)
                return data, j + 1
        else:
            raise LookupError("Something happened while extracting kpt "
                              "band character.")

    def _extract_band_character_this_eig(self, lines):
        # ==== e(   1) =   -62.67241 eV ====
        #  psi = 0.969*[#  11]+0.012*[#   5]+0.012*[#  10]+0.002*[#  26]+0.002*[#  34]+  # noqa
        #       +0.001*[#  21]+0.001*[#  29]+
        # |psi|^2 = 1.000
        chars = []
        wfc_numbers = []
        for j, line in enumerate(lines[1:]):
            if "[# " in line:
                # band chars are defined on this line
                # first remove all '[]*+#'
                line = " ".join(sum([x.split("]+")
                                     for x in line.split("*[#")], []))
                s, i, f = decompose_line(line)
                chars += f
                wfc_numbers += i
                assert len(i) == len(f)
                continue
            if "==== e(" in line or "Lowdin Charges" in line:
                # we're done with this eigenvalue
                # it's possible a given eigenvalue has no projection at all?!?
                # if not len(wfc_numbers) or not len(chars):
                #     # self._logger.error(
                #     #     "Failed to extract band character from
                #            these lines: "
                #     #     f"{lines}")
                #     raise LookupError("No band character extracted.")
                return [{"wfc": wfc, "band_character": char}
                        for wfc, char in zip(wfc_numbers, chars)], j
        else:
            raise LookupError("Something happened while extracting band "
                              "character for a given eigenvalue.")
