import argparse
import logging

from .routines import get_all_calculations_recursively
from .utils import CalculationsList
from .. import __SYSTEM_QUEUE__
from ..bases import BaseRemoteClient, BaseUtility
from ..colors import Colors
from ..routines import is_list_like


class BaseAbisuiteSubScript(BaseUtility):
    """Base class for abisuite subcommands script class.

    This class basically implements a load_calculations method based
    on a list of calculation sources.

    Note
    ----
    This class is not meant to be usable directly but only subclassed.
    """
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.calculations = CalculationsList([], loglevel=self._loglevel)

    def cancel_job(self, calculation_directory, dry=False):
        """Cancel a job based on its calculation directory object.

        Parameters
        ----------
        calculation_directory: CalculationDirectory instance
            The CalculationDirectory instance we want to cancel the job of.
        dry: bool, optional
            If True, no job is cancelled but the job id is printed out if
            it would be cancelled otherwise.
        """
        with __SYSTEM_QUEUE__ as queue:
            if calculation_directory not in queue:
                return
            job = queue[calculation_directory]
            if not dry:
                print(Colors.bold_text("CANCELLING") +
                      f" job id: {job.id}.")
                queue.cancel_job(job)
                self.success("job cancellation.")
            else:
                print(Colors.bold_text("Will cancel job id:") +
                      f" job id: {job.id}.")

    def error(self, text):
        """Prints out a given text with a bug 'ERROR' written before it in
        red and bold painted blood.

        Parameters
        ----------
        text: str
            The text to print.
        """
        self._print_this_with(
                text,
                Colors.color_text("ERROR: ", "bold", "red"))

    def load_calculations(
            self, calculations_srcs,
            follow_symlinks=False,
            load_converged=True,
            load_errored=True,
            load_finished=True,
            load_running=True,
            load_unstarted=True,
            return_path=False,
            ):
        """Loads the calculations to clean.

        Parameters
        ----------
        calculations_srcs: list-like
            The list of calculations srcs to compute the total tree
            of calculations to clean.
        load_converged: bool, optional
            If False, don't load converged calculations.
        load_errored: bool, optional
            If False, don't load errored calculations.
        load_finished: bool, optional
            If False, don't load finished calculations.
        load_running: bool, optional
            If False, don't load running calculations.
        load_unstarted: bool, optional
            If False, don't load unstarted calculations.
        follow_symlinks: bool, optional
            If True, symlinks towards other calculations are followed.
        return_path: bool, optional
            If True, instead of CalculationDirectory instances, only
            calculation paths is stored inside the self.calculations
            attribute.
        """
        calculations = get_all_calculations_recursively(
                calculations_srcs, return_path=False,
                follow_symlinks=follow_symlinks,
                loglevel=self._loglevel)
        for calculation in calculations:
            if self.skip_this_calculation(
                    calculation,
                    continue_on_converged=not load_converged,
                    continue_on_error=not load_errored,
                    continue_on_finished=not load_finished,
                    continue_on_running=not load_running,
                    continue_on_unstarted=not load_unstarted,
                    ):
                continue
            if return_path:
                self.calculations.append(calculation.path)
            else:
                self.calculations.append(calculation)

    # TODO: put this method in a kind of iterator which would make
    # more sense than an instance method (FG: 2021/07/26)
    def skip_this_calculation(
            self, calculation, continue_on_error=True,
            continue_on_unstarted=True, continue_on_running=True,
            continue_on_finished=True, continue_on_converged=True,
            ):
        """When iterating over a list of CalculationDirectory object and only
        want to manage some calculations based on their status,
        use this function.

        Parameters
        ----------
        calculation: CalculationDirectory object
            The calculation we want to check if we deal with it.
        continue_on_error: bool, optional
            If True (default), if the calculation errored out,
            we skip it.
        continue_on_unstarted: bool, optional
            If True (default), if the calculation is not started, we skip it.
        continue_on_running: bool, optional
            If True (default), if the calculation is running, we skip it.
        continue_on_finished: bool, optional
            If True (default), if the calculation is finished, we skit it.
        continue_on_converged: bool, optional
            If True (default), if the calculation is finished and converged
            (if defined), we skip it.

        Returns
        -------
        bool: True if we continue in the loop. False otherwise.

        Side Effects
        ------------
        Prints colorful statement based on the status of the calc.
        Also, calculation that will be skipped
        will have their content cleared
        in order to free some memory (useful when there are a lot of files in
        a calculation directory).
        """
        with calculation:
            finished = calculation.status["calculation_finished"]
            started = calculation.status["calculation_started"]
            converged = None
            if "calculation_converged" in calculation.status:
                converged = calculation.status["calculation_converged"]
            if started is False and continue_on_unstarted:
                print("Skipping " +
                      Colors.color_text("UNSTARTED", "cyan", "bold") +
                      f" calculation: '{calculation.path}'.")
                calculation.clear_content()
                return True
            if finished is False and continue_on_running:
                print("Skipping " +
                      Colors.color_text("RUNNING", "yellow", "bold") +
                      f" calculation: '{calculation.path}'.")
                calculation.clear_content()
                return True
            if finished is True:
                if converged is None and continue_on_finished:
                    print("Skipping " +
                          Colors.color_text("FINISHED", "bold", "green") +
                          f" calculation: '{calculation.path}'.")
                    calculation.clear_content()
                    return True
                else:
                    # converged attribute is set
                    if converged is True and continue_on_converged:
                        print("Skipping " +
                              Colors.color_text(
                                  "FINISHED & CONVERGED", "bold", "green") +
                              f" calculation: '{calculation.path}'.")
                        calculation.clear_content()
                        return True
            if finished == "error" and continue_on_error:
                print("Skipping " +
                      Colors.color_text("ERRORED", "red", "bold") +
                      f" calculation: '{calculation.path}'.")
                calculation.clear_content()
                return True
        return False

    def success(self, text):
        """Prints out a given text with a bug 'SUCCESS' written before it in
        green and bold.

        Parameters
        ----------
        text: str
            The text to print.
        """
        self._print_this_with(
                text,
                Colors.color_text("SUCCESS: ", "bold", "green"))

    def successfull(self, text):
        """Prints out a given text with a bug 'SUCCESSFULL' written before it in
        green and bold.

        Parameters
        ----------
        text: str
            The text to print.
        """
        self._print_this_with(
                text,
                Colors.color_text("SUCCESSFULL: ", "bold", "green"))

    def _print_this_with(self, text, label):
        # text: user specified text to print
        # label: something that will label the text (printed before it)
        print(label + text)


class BaseAbisuiteArgParser:
    """Baseclass for any argparser for the abisuite package.
    """
    description = None

    def __init__(self):
        self.parser = self.create_parser()

    def parse_args(self):
        return self.parser.parse_args()

    def create_parser(self):
        parser = argparse.ArgumentParser(description=self.description)
        parser.add_argument("--loglevel", "-l", action="store", type=int,
                            default=logging.INFO,
                            help=("Sets the logging level."))
        return parser


class BaseAbisuiteScriptRemoteClient(BaseRemoteClient):
    """Base class for remote clients defined only in the scripts.
    """

    def remote_script_calculations(self, script, calculations):
        """Use the script remotely on given calculations.

        Parameters
        ----------
        script: str
            The script to use.
        calculations: str or list-like
            The calculations to send to the script. Can be a list.
        """
        assert script == self._check_script_exists_on_connection
        if self.client is None:
            raise RuntimeError(
                    "Need to enter with statement before relaunching remote "
                    "calculations.")
        if not is_list_like(calculations):
            self.remote_script_calculations(script, (calculations, ))
            return
        cmd = f"{script} " + " ".join(calculations)
        stdin, stdout, stderr = self.client.exec_command(cmd)
        stderr = stderr.read().decode("utf-8")
        stdout = stdout.read().decode("utf-8")
        if stdout:
            print(stdout)
        if stderr:
            print(stderr)
