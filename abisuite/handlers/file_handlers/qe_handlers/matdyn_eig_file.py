from ..bases import BaseFileHandler
from ...file_parsers import QEMatdynEigParser
from ...file_structures import QEMatdynEigStructure


class QEMatdynEigFile(BaseFileHandler):
    """File handler class for a '.eig' file produced by the matdyn.x script
    of Quantum Espresso.
    """
    _loggername = "QEMatdynEigFile"
    _parser_class = QEMatdynEigParser
    _structure_class = QEMatdynEigStructure
