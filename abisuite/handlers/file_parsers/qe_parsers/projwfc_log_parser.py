from .bases import BaseQELogParser
from .subparsers.projwfc_log_subparsers import (
                QEProjwfcLogTimingSubParser,
                QEProjwfcLogBandCharacterSubParser,
                QEProjwfcLogAtomicStatesSubParser,
                )
from ...file_structures import QEProjwfcLogStructure


class QEProjwfcLogParser(BaseQELogParser):
    """Parser class for a Quantum Espresso projwfc.x log file.
    """
    _loggername = "QEProjwfcLogParser"
    _structure_class = QEProjwfcLogStructure
    _subparsers = (
            QEProjwfcLogTimingSubParser,
            QEProjwfcLogBandCharacterSubParser,
            QEProjwfcLogAtomicStatesSubParser,
            )

    def _extract_data_from_lines(self, *args, **kwargs):
        data = super()._extract_data_from_lines(*args, **kwargs)
        # we need to post process band characters according to atomic states
        n_atomic_state = len(data["atomic_states"])
        for kpt_chars in data["band_character"]:
            for eig_chars in kpt_chars["band_character"]:
                # loop over eigenvalue characters
                all_wfcs_this_eig = [x["wfc"] for x in eig_chars]
                for i in range(1, n_atomic_state + 1):
                    if i not in all_wfcs_this_eig:
                        eig_chars.append({"wfc": i, "band_character": 0.0})
                assert len(eig_chars) == n_atomic_state
        return data
