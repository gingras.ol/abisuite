Packagers Module
================

.. contents::
    :local:

This module contains the infamous Packager objects. They
are (almost) exclusive to HPC clusters where cleanup must be done
from time to time on some disks (e.g.: a Scratch disk).
Their purpose are to copy
all relevant files from a calculation to another disk (one that is
for more long term storage).

It is possible to define in the config file the `package_src_root` and
the `package_dest_root` which are respectively the root of the source
tree of calculations to package and the root of the destination tree.
These packagers will keep the same file hierarchy at destination as the one defined
in the source. Thus don't need to reorganize everything. As long as your scratch
is organized, the packagers will respect that.

A quick way to use them is through the `abipackage` script.

Base Classes
------------

.. automodule:: abisuite.packagers.bases
    :members:
    :show-inheritance:

Abinit Packagers
----------------

.. automodule:: abisuite.packagers.abinit_packagers.abinit_packager
    :members:
    :show-inheritance:

Quantum Espresso Packagers
--------------------------

.. automodule:: abisuite.packagers.qe_packagers.pw_packager
    :members:
    :show-inheritance:

.. automodule:: abisuite.packagers.qe_packagers.epw_packager
    :members:
    :show-inheritance:
