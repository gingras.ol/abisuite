import os
import shutil


from .bases import BasePostProcTest
from ..bases import TestCase
from ..variables_for_tests import AbinitAnaddbCalc, QEMatdynCalc, QEMatdynfreq
from ...handlers import MetaDataFile, PBSFile
from ...post_processors import PhononDispersion


class BasePhononDispersionTest(BasePostProcTest):
    """Base class for phonon dispersion test cases.
    """
    _post_proc_class = PhononDispersion

    def test_from_calculation(self):
        calc = shutil.copytree(self._from_calc, self._link_tempdir_path,
                               symlinks=True)
        # change meta file there
        with MetaDataFile.from_calculation(calc, loglevel=1) as meta:
            # rename all files from the temporary root
            meta.calc_workdir = calc
            # force write here to make sure all properties are changed
            # according to new calc workdir.
            meta.write(overwrite=True)
            # recast pbs file of moved directory
            with PBSFile.from_meta_data_file(meta, loglevel=1) as pbs:
                pbs.log_path = os.path.join(meta.calc_workdir,
                                            os.path.basename(pbs.log_path))
        super().test_from_calculation()


class TestPhononDispersionFromQECalc(BasePhononDispersionTest, TestCase):
    """Test class for a Phonon Dispersion object created from a Quantum
    Espresso matdyn calculation.
    """
    _from_calc = QEMatdynCalc
    _from_file = QEMatdynfreq

    def test_from_freq_file(self):
        # test from a ".freq" file
        self.postproc = self._post_proc_class.from_qe_matdyn_freq(
                self._from_file)
        self.postproc.get_plot(all_k_labels="none")


class TestPhononDispersionFromAbinitCalculation(
        BasePhononDispersionTest, TestCase):
    """Test case for a phonon dispersion object created from an abinit anaddb
    calculation.
    """
    _from_calc = AbinitAnaddbCalc
    _from_file = ""
