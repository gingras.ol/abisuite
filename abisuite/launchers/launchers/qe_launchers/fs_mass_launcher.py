from .fs_launcher import QEFSLauncher
from ..bases import BaseMassLauncher


class QEFSMassLauncher(BaseMassLauncher):
    """The mass launcher class for Quantum Espresso calculations
    using the fs.x script.
    """
    _launcher_class = QEFSLauncher
    _loggername = "QEFSMassLauncher"
