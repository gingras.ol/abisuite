from ..bases import BaseSCFLogStructure


class AbinitOutputStructure(BaseSCFLogStructure):
    """Structure class for an output file created by Abinit.
    """
    all_attributes = (
            "dtsets", "input_variables", "output_variables",
            "timing", "walltime",
            )
    dict_attributes = ("input_variables", "output_variables", )
    list_attributes = ("dtsets", )
    optional_attributes = ("timing", )

    @property
    def etot(self):
        return self.output_variables["etotal"]

    @etot.setter
    def etot(self, etot):
        self._etot = etot
