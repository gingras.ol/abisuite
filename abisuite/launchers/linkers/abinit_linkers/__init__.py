from .abinit_linkers import AbinitfromAbinitLinker
from .anaddb_linkers import (
        AbinitAnaddbfromAbinitLinker, AbinitAnaddbfromAbinitMrgddbLinker,
        )
from .mrgddb_linkers import AbinitMrgddbfromAbinitLinker
from .optic_linkers import AbinitOpticfromAbinitLinker


__ABINIT_LINKERS__ = {
        "abinit": {
            "abinit": AbinitfromAbinitLinker,
            },
        "abinit_anaddb": {
            "abinit": AbinitAnaddbfromAbinitLinker,
            "abinit_mrgddb": AbinitAnaddbfromAbinitMrgddbLinker,
            },
        "abinit_mrgddb": {
            "abinit": AbinitMrgddbfromAbinitLinker,
            },
        "abinit_optic": {
            "abinit": AbinitOpticfromAbinitLinker,
            },
        }
