#!/bin/bash

MPIRUN=""
EXECUTABLE="/home/fgoudreault/Workspace/abinit/abinit/build/develop/2021-04-20/src/98_main/abinit"
INPUT=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_workflows/test_files/abinit_si/nc/phonon_dispersion/phonons_runs/ph_q7/irreps_determination/run/irreps_determination.files
LOG=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_workflows/test_files/abinit_si/nc/phonon_dispersion/phonons_runs/ph_q7/irreps_determination/irreps_determination.log
STDERR=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_workflows/test_files/abinit_si/nc/phonon_dispersion/phonons_runs/ph_q7/irreps_determination/irreps_determination.stderr


$MPIRUN $EXECUTABLE < $INPUT > $LOG 2> $STDERR
