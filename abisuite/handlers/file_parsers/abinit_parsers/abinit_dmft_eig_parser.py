from .bases import BaseAbinitDMFTParser
from ...file_structures import AbinitDMFTEigStructure
from ....routines import decompose_line
import numpy as np


class AbinitDMFTEigParser(BaseAbinitDMFTParser):
    """Class that reads a .eig file produced by the DMFT module of Abinit.
    """
    _expected_ending = ".eig"
    _loggername = "AbinitDMFTEigParser"
    _structure_class = AbinitDMFTEigStructure

    def _extract_data_from_lines(self, lines):
        # DATA ORGANIZED AS FOLLOWS:
        # META DATA HEADER ...
        # For spin
        #           1
        # For k-point
        #           1
        # 1         1    value
        # 2         1    value
        # ...
        # nband     1    value
        # For k-point
        #           2
        # 1         2    value
        # ...

        # First strip header
        body, header = self._strip_header(lines)
        # extract meta data from header
        data = self._extract_meta_data(header)
        # extract data for each spin, kpt and considered band
        data["eigenvalues"] = np.array(self._extract_eigs(
            body, data["nkpt"], data["nspins"], data["nband"]))
        return data

    def _extract_eigs(self, body, nkpt, nspins, nband):
        self._logger.debug("Starting data extraction.")
        # extract eigenvalues data
        data = []
        end = 0
        for i, line in enumerate(body):
            if i < end:
                # skip lines until end of spin clock
                continue
            if "For spin" in line:
                data_per_spin, rel_end = self._extract_data_per_spin(
                        body[i:], nband)
                # check that number of kpts matches header
                if len(data_per_spin) != nkpt:
                    raise ValueError("Was expecting %i kpts but found %i" %
                                     (nkpt, len(data_per_spin)))
                assert len(data_per_spin) == nkpt
                data.append(data_per_spin)
                end += rel_end
        # check that number of spins matches header
        assert len(data) == nspins
        if len(data) == 1:
            # only one spin; return first block directly
            return data[0]
        return data

    def _extract_data_per_spin(self, lines, nband):
        self._logger.debug("Starting data extraction for one spin.")
        # extract eigenvalues for a spin block
        data = []
        end = 0
        block_end = False
        for i, line in enumerate(lines):
            if i < end:
                continue
            if "For spin" in line:
                if not block_end:
                    continue
                # arrived at end of file or spin block
                return data, i
            if "For k-point" in line:
                data_per_kpt, rel_end = self._extract_data_per_kpt(lines[i:])
                # check that number of eigenvalues matches number of bands
                if len(data_per_kpt) != nband:
                    raise ValueError("Was expecting %i bands but found %i" %
                                     (nband, len(data_per_kpt)))
                block_end = True
                data.append(data_per_kpt)
                end += rel_end
        # arrived at the end of the file / block, return what we got
        # SHOW ME WHAT U GOT !!
        return data, len(lines) - 1

    def _extract_data_per_kpt(self, lines):
        self._logger.debug("Starting data extraction for one kpt.")
        # extract eigenvalues for a kpt block
        eigenvalues = []
        block_end = False
        for index, line in enumerate(lines):
            s, i, f = decompose_line(line)
            if len(f) == 0:
                if not block_end:
                    # no data here skip
                    continue
                if block_end:
                    # arrived at next block, return eigenvalues
                    return eigenvalues, index
            block_end = True
            # divide eigenvalue by 2 here because for some reason,
            # it is multiplied by 2 in the ABINIT code.
            eigenvalues.append(f[0] / 2)
        # arrived at the end of the block return what we have
        return eigenvalues, len(lines) - 1

    def _strip_header(self, lines):
        self._logger.debug("Stripping header.")
        for i, line in enumerate(lines):
            if "For each k-point" in line:
                # skip next line also
                return lines[i + 1:], lines[:i + 1]
        # if here, could not strip header
        raise LookupError("Could not strip header from .eig file...")

    def _extract_meta_data(self, header):
        # extract number of kpts and hamiltonian dimensions from the first
        # line.
        # first get the good line
        line = header[1].strip("\n")
        self._logger.debug("Extracting meta data.")
        s, i, f = decompose_line(line)
        data = {}
        data["nkpt"] = i[2]
        data["nbandtot"] = i[0]  # number of bands (total)
        data["nspins"] = i[1]  # number of spins
        data["dmftbandi"] = i[-2]  # first band considered
        data["dmftbandf"] = i[-1]  # last band considered
        data["nband"] = data["dmftbandf"] - data["dmftbandi"] + 1
        return data
