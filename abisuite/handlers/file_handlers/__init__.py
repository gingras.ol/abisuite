from .abinit_handlers import (
        AbinitAnaddbFilesFile, AbinitAnaddbInputFile, AbinitAnaddbLogFile,
        AbinitAnaddbPhfrqFile, AbinitCut3DInputFile, AbinitDMFTEigFile,
        AbinitDMFTProjectorsFile, AbinitDOSFile,
        AbinitEIGFile, AbinitFatbandFile, AbinitFilesFile, AbinitGSRFile,
        AbinitInputFile,
        AbinitLogFile, AbinitMrgddbInputFile,
        AbinitMrgddbLogFile, AbinitOpticFilesFile, AbinitOpticInputFile,
        AbinitOpticLincompFile, AbinitOpticLogFile,
        AbinitOutputFile, AbinitProcarFile, AbinitProjectedDOSFile,
        )
from .generic_files import GenericFile, GenericInputFile
from abisuite.handlers.file_handlers.meta_data_file import MetaDataFile
from .pbs_file import PBSFile
from .pseudo_file import PseudoFile
from .qe_handlers import (
        QEDOSDOSFile, QEDOSInputFile, QEDOSLogFile,
        QEDynmatInputFile, QEDynmatLogFile,
        QEEpsilonInputFile, QEEpsilonLogFile,
        QEEPWa2FFile, QEEPWBandEigFile, QEEPWConductivityTensorFile,
        QEEPWInputFile,
        QEEPWLogFile, QEEPWPHBandFreqFile, QEEPWPhononSelfEnergyFile,
        QEEPWResistivityFile, QEEPWSpecFunPhonFile,
        QEFSInputFile, QEFSLogFile,
        QELD1InputFile, QELD1LogFile,
        QEMatdynDOSFile, QEMatdynEigFile, QEMatdynFreqFile, QEMatdynInputFile,
        QEMatdynLogFile,
        QEPHDyn0File, QEPHInputFile, QEPHLogFile,
        QEPPInputFile, QEPPLogFile,
        QEProjwfcInputFile, QEProjwfcLogFile, QEProjwfcPDOSFile,
        QEPWInputFile, QEPWLogFile,
        QEPW2Wannier90InputFile, QEPW2Wannier90LogFile,
        QEQ2RInputFile, QEQ2RLogFile,
        )
from .wannier90_handlers import (
        Wannier90BandDatFile, Wannier90BandkptFile,
        Wannier90InputFile, Wannier90OutputFile,
        )
from .slurm_files_to_delete_file import SlurmFilesToDeleteFile
from .stderr_file import StderrFile
from .symlink_file import SymLinkFile


CALCTYPES_TO_LOG_FILE_CLS = {
        "abinit": AbinitLogFile,
        "abinit_anaddb": AbinitAnaddbLogFile,
        "abinit_mrgddb": AbinitMrgddbLogFile,
        "abinit_optic": AbinitOpticLogFile,
        "qe_dos": QEDOSLogFile,
        "qe_dynmat": QEDynmatLogFile,
        "qe_epsilon": QEEpsilonLogFile,
        "qe_epw": QEEPWLogFile,
        "qe_fs": QEFSLogFile,
        "qe_ld1": QELD1LogFile,
        "qe_matdyn": QEMatdynLogFile,
        "qe_ph": QEPHLogFile,
        "qe_pp": QEPPLogFile,
        "qe_projwfc": QEProjwfcLogFile,
        "qe_pw": QEPWLogFile,
        "qe_pw2wannier90": QEPW2Wannier90LogFile,
        "qe_q2r": QEQ2RLogFile,
        "wannier90": Wannier90OutputFile,
        }

CALCTYPES_TO_INPUT_FILE_CLS = {
        "abinit": AbinitInputFile,
        "abinit_anaddb": AbinitAnaddbInputFile,
        "abinit_cut3d": AbinitCut3DInputFile,
        "abinit_mrgddb": AbinitMrgddbInputFile,
        "abinit_optic": AbinitOpticInputFile,
        "qe_dos": QEDOSInputFile,
        "qe_dynmat": QEDynmatInputFile,
        "qe_epsilon": QEEpsilonInputFile,
        "qe_epw": QEEPWInputFile,
        "qe_fs": QEFSInputFile,
        "qe_ld1": QELD1InputFile,
        "qe_matdyn": QEMatdynInputFile,
        "qe_ph": QEPHInputFile,
        "qe_pp": QEPPInputFile,
        "qe_projwfc": QEProjwfcInputFile,
        "qe_pw": QEPWInputFile,
        "qe_pw2wannier90": QEPW2Wannier90InputFile,
        "qe_q2r": QEQ2RInputFile,
        "wannier90": Wannier90InputFile,
        }
