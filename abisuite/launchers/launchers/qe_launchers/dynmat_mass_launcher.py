from .dynmat_launcher import QEDynmatLauncher
from ..bases import BaseMassLauncher


class QEDynmatMassLauncher(BaseMassLauncher):
    """The mass launcher class for Quantum Espresso calculations
    using the dynmat.x script.
    """
    _launcher_class = QEDynmatLauncher
    _loggername = "QEDynmatMassLauncher"
