from .bases import BaseFileHandlerTest
from ...handlers import GenericFile
import tempfile
import unittest


class TestGenericFileHandler(BaseFileHandlerTest, unittest.TestCase):
    _file_handler_class = GenericFile

    def setUp(self):
        self.tempfile = tempfile.NamedTemporaryFile(delete=False)
        self._example_file = self.tempfile.name
        super().setUp()

    def tearDown(self):
        del self.tempfile
        super().tearDown()
