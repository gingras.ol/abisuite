from .bases import BaseQELogStructure


class QEProjwfcLogStructure(BaseQELogStructure):
    """Structure for a projwfc.x log file from Quantum Espresso.
    """
    all_attributes = ("atomic_states", "band_character", "timing", )
