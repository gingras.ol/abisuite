#!/bin/bash

MPIRUN="mpirun -np 4"
EXECUTABLE="/home/fgoudreault/Workspace/abinit/abinit/build/develop/2021-04-16/src/98_main/abinit"
INPUT=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_workflows/test_files/abinit_si/nc/optic/nscf_fbz_run/run/nscf_fbz_run.files
LOG=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_workflows/test_files/abinit_si/nc/optic/nscf_fbz_run/nscf_fbz_run.log
STDERR=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_workflows/test_files/abinit_si/nc/optic/nscf_fbz_run/nscf_fbz_run.stderr


$MPIRUN $EXECUTABLE < $INPUT > $LOG 2> $STDERR
