from .bases import BaseWriter
from ..file_structures import PBSStructure
from ...bases import BaseUtility
import abc


class PBSWriter(BaseWriter):
    """Class that represents and write pbs files which serves to launch
    batch calculations on super clusters or simply on a PC."""
    _loggername = "PBSWriter"
    _structure_class = PBSStructure

    @property
    def lines(self):
        # syntax will depend on the queuing system.
        if self.queuing_system == "local":
            pbsclass = NoQueuingPBS
        elif self.queuing_system == "torque":
            pbsclass = TorquePBS
        elif self.queuing_system == "grid_engine":
            pbsclass = GridEnginePBS
        elif self.queuing_system == "slurm":
            pbsclass = SlurmPBS
        elif self.queuing_system == "pbs_professional":
            pbsclass = PBSProfessionalPBS
        else:
            raise ValueError(f"{self.queuing_system}")
        linesgetter = pbsclass(self, loglevel=self._logger.level)
        try:
            return linesgetter.lines
        except Exception as err:
            self._logger.exception(
                    "An error occured while getting the lines for PBS file.")
            raise err


# TODO: clean the following classes: need more rebasing and modularity
class BaseQueuingHeader(BaseUtility, abc.ABC):
    """Base class for the Header of a pbs file.
    """
    _loggername = "PBSHeaderWriter"

    def __init__(self, pbs_file_writer, **kwargs):
        super().__init__(**kwargs)
        self.pbs = pbs_file_writer
        self._lines = None

    @property
    def lines(self):
        if self._lines is not None:
            return self._lines
        self._lines = self._compute_lines()
        return self.lines

    def _compute_lines(self):
        lines = []
        # first line is bash line
        lines.append("#!/bin/bash\n\n")
        lines += self._compute_header()
        # ENV variables
        # mpirun
        lines.append(f'MPIRUN="{self.pbs.mpi_command}"\n')
        # abinit executable
        lines.append(f'EXECUTABLE="{self.pbs.command}"\n')
        # files file
        lines.append(f"INPUT={self.pbs.input_file_path}\n")
        # log file
        lines.append(f"LOG={self.pbs.log_path}\n")
        # stderr file
        lines.append(f"STDERR={self.pbs.stderr_path}\n\n")
        # modules
        # use modules first
        for module in self.pbs.modules_to_use:
            lines.append(f"module use {module}\n")
        # unload modules
        for module in self.pbs.modules_to_unload:
            lines.append(f"module unload {module}\n")
        # swap modules after unloading the undesired ones
        for module in self.pbs.modules_to_swap:
            if len(module) != 2:
                raise ValueError(
                        "Modules to swap should be a list of 2 elements. Got:"
                        f" {module}")
            lines.append(f"module swap {module[0]} {module[1]}\n")
        # then load the modules we want
        for module in self.pbs.modules_to_load:
            lines.append(f"module load {module}\n")
        lines.append("\n")
        # lines before
        if self.pbs.lines_before:
            for line in self.pbs.lines_before:
                if not line.endswith("\n"):
                    line += "\n"
                lines.append(line)
            lines.append("\n")
        # actual command line
        cmd_line = self.pbs.command_line
        if not cmd_line.endswith("\n"):
            cmd_line += "\n"
        lines.append(cmd_line)
        # lines after
        if self.pbs.lines_after:
            for line in self.pbs.lines_after:
                if not line.endswith("\n"):
                    line += "\n"
                lines.append(line)
            # don't need to add another blank space after
        return lines

    @abc.abstractmethod
    def _compute_header(self, lines):
        pass  # pragma: no cover


class GridEnginePBS(BaseQueuingHeader):
    """Header class when the queuing system is Grid Engine.
    """
    # options can be found here:
    # https://arc.liv.ac.uk/SGE/htmlman/manuals.html
    def _compute_header(self):
        lines = []
        # Add interpreting shell for the job
        lines.append("#$ -S /bin/bash\n")
        # Add queue request
        # For now, this is specific to Kittel...
        if self.pbs.queue is None:
            raise ValueError("In GridEngine, queue cannot be None.")
        lines.append(f"#$ -l qname={self.pbs.queue}\n")
        lines.append(f"#$ -l h_rt={self.pbs.walltime}\n")
        # Add jobname
        lines.append(f"#$ -N {self.pbs.jobname}\n")
        # specify to run the job in the current working directory
        lines.append("#$ -cwd\n")
        # activate parallel environment
        lines.append(f"#$ -pe orte {self.pbs.ppn * self.pbs.nodes}\n")
        lines.append("\n")
        return lines


class NoQueuingPBS(BaseQueuingHeader):
    """Header class when there are no queuing system.
    """
    def _compute_header(self):
        # no header for no queuing system
        return []


class PBSProfessionalPBS(BaseQueuingHeader):
    """Header class for a PBS Professional batch file.
    """
    def _compute_header(self):
        lines = []
        # first line is jobname
        lines.append(f"#PBS -N {self.pbs.jobname}\n")
        # number of nodes
        lines.append(f"#PBS -l select={self.pbs.nodes}\n")
        # walltime
        lines.append(f"#PBS -l walltime={self.pbs.walltime}\n")
        # project code
        lines.append(f"#PBS -A {self.pbs.project_code}\n")
        lines.append("\n")
        return lines


class TorquePBS(BaseQueuingHeader):
    """Header class for a Torque batch file.
    """
    def _compute_header(self):
        lines = []
        # next line is job name
        lines.append(f"#PBS -N {self.pbs.jobname}\n")
        # walltime
        lines.append(f"#PBS -l walltime={self.pbs.walltime}\n")
        # nodes/ppn
        lines.append(f"#PBS -l nodes={self.pbs.nodes}:"
                     f"ppn={self.pbs.ppn}\n")
        lines.append("\n")
        return lines


class SlurmPBS(BaseQueuingHeader):
    """PBS header class for slurm queuing system.
    """
    def _compute_header(self):
        lines = []
        if self.pbs.nodes:
            lines.append(f"#SBATCH --nodes={self.pbs.nodes}\n")
        if self.pbs.ppn is not None:
            if self.pbs.ntasks is not None:
                raise ValueError(
                        "ppn and ntasks cannot be not None at same time.")
            lines.append(f"#SBATCH --ntasks-per-node={self.pbs.ppn}\n")
        else:
            if self.pbs.ntasks is None:
                raise ValueError(
                        "ppn and ntasks cannot be None at same time.")
            lines.append(f"#SBATCH --ntasks={self.pbs.ntasks}\n")
        if self.pbs.cpus_per_task is not None:
            lines.append(f"#SBATCH --cpus-per-task={self.pbs.cpus_per_task}\n")
            # add some lines before if needed
            self._set_omp_num_threads()
        lines.append(f"#SBATCH --time={self.pbs.walltime}\n")
        lines.append(f"#SBATCH --job-name={self.pbs.jobname}\n")
        if self.pbs.project_account is not None:
            lines.append(f"#SBATCH --account={self.pbs.project_account}\n")
        if self.pbs.memory is not None:
            if self.pbs.memory_per_cpu is not None:
                raise ValueError("Cannot set both memory and memory_per_cpu.")
            lines.append(f"#SBATCH --mem={self.pbs.memory}\n")
        else:
            if self.pbs.memory_per_cpu is None:
                raise ValueError(
                        "Need to set either memory or memory_per_cpu.")
            lines.append(f"#SBATCH --mem-per-cpu={self.pbs.memory_per_cpu}\n")
        if self.pbs.queue is not None:
            lines.append(f"#SBATCH --partition={self.pbs.queue}\n")
        if self.pbs.queuep is not None:
            lines.append(f"#SBATCH -p {self.pbs.queuep}\n")
        if self.pbs.quality_of_service is not None:
            lines.append(f"#SBATCH --qos={self.pbs.quality_of_service}\n")
        if self.pbs.constraint is not None:
            lines.append(f"#SBATCH --constraint={self.pbs.constraint}\n")
        lines.append("\n")
        return lines

    def _set_omp_num_threads(self):
        # example taken from
        # https://www.hpc2n.umu.se/documentation/batchsystem/hybrid-examples
        # check if the export is already stored by user
        for line in self.pbs.lines_before:
            if "OMP_NUM_THREADS" in line:
                # assume it is correctly set
                return
        self.pbs.lines_before.append(
                'if [ -n "$SLURM_CPUS_PER_TASK" ]; then\n  omp_threads='
                '$SLURM_CPUS_PER_TASK\nelse\n  omp_threads=1\nfi\n'
                'export OMP_NUM_THREADS=$omp_threads\n'
                )
        # # also add core binding
        # self.pbs.mpi_command_arguments.append("--cpu-bind=cores")
