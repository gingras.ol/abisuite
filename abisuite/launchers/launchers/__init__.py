from .abinit_launchers import (
        AbinitAnaddbLauncher, AbinitCut3DLauncher, AbinitLauncher,
        AbinitMassLauncher,
        AbinitMrgddbLauncher, AbinitOpticLauncher, AbinitOpticMassLauncher,
        )
from .qe_launchers import (
        QEDOSLauncher, QEDOSMassLauncher,
        QEDynmatLauncher, QEDynmatMassLauncher,
        QEEpsilonLauncher, QEEpsilonMassLauncher,
        QEEPWLauncher,
        QEFSLauncher, QEFSMassLauncher,
        QELD1Launcher, QELD1MassLauncher,
        QEMatdynLauncher, QEMatdynMassLauncher,
        QEPHLauncher, QEPHMassLauncher,
        QEPPLauncher, QEPPMassLauncher,
        QEProjwfcLauncher, QEProjwfcMassLauncher,
        QEPWLauncher, QEPWMassLauncher,
        QEPW2Wannier90Launcher,
        QEQ2RLauncher, QEQ2RMassLauncher
        )
from .wannier90_launchers import (
        Wannier90Launcher,
        )


# list to keep order (useful for testing)
__ALL_LAUNCHERS__ = {
        "abinit": AbinitLauncher,
        "abinit_anaddb": AbinitAnaddbLauncher,
        "abinit_cut3d": AbinitCut3DLauncher,
        "abinit_mrgddb": AbinitMrgddbLauncher,
        "abinit_optic": AbinitOpticLauncher,
        "qe_dos": QEDOSLauncher,
        "qe_dynmat": QEDynmatLauncher,
        "qe_epsilon": QEEpsilonLauncher,
        "qe_epw": QEEPWLauncher,
        "qe_fs": QEFSLauncher,
        "qe_ld1": QELD1Launcher,
        "qe_matdyn": QEMatdynLauncher,
        "qe_ph": QEPHLauncher,
        "qe_pp": QEPPLauncher,
        "qe_projwfc": QEProjwfcLauncher,
        "qe_pw": QEPWLauncher,
        "qe_pw2wannier90": QEPW2Wannier90Launcher,
        "qe_q2r": QEQ2RLauncher,
        "wannier90": Wannier90Launcher,
        }
