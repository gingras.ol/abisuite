import os

from .bases import BaseQELauncher_for_outdir_prefix, BasePostPWLauncher
from ....handlers import QEProjwfcInputFile


class QEProjwfcLauncher(BaseQELauncher_for_outdir_prefix,
                        BasePostPWLauncher):
    """Launcher class for a Quantum Espresso Calculation using projwfc.x script.
    """
    _loggername = "QEProjwfcLauncher"
    _input_file_handler_class = QEProjwfcInputFile
    _calctype = "qe_projwfc"

    def _preprocess_inputs(self, inputs):
        inputs = BaseQELauncher_for_outdir_prefix._preprocess_inputs(self,
                                                                     inputs)
        filpdos = os.path.join(self.output_data_dir.path, self.jobname)
        inputs = self._overwrite_input_var(inputs, "filpdos", filpdos)
        filproj = os.path.join(self.output_data_dir.path,
                               self.jobname + ".proj")
        inputs = self._overwrite_input_var(inputs, "filproj", filproj)
        return inputs
