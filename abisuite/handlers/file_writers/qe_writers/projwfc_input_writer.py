from .bases import BaseQEInputWriter
from ...file_structures import QEProjwfcInputStructure


class QEProjwfcInputWriter(BaseQEInputWriter):
    """Class that can write a Quantum Espresso projwfc.x input file.
    """
    _loggername = "QEProjwfcInputWriter"
    _structure_class = QEProjwfcInputStructure

    @property
    def lines(self):
        lines = []
        self._append_var_block(lines, "projwfc")
        return lines
