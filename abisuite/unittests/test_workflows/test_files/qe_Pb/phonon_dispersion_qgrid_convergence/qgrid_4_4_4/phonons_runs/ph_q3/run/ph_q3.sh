#!/bin/bash

MPIRUN="mpirun -np 4"
EXECUTABLE="/home/fgoudreault/Workspace/q-e/bin/ph.x -npool 4"
INPUT=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_workflows/test_files/qe_Pb/phonon_dispersion_qgrid_convergence/qgrid_4_4_4/phonons_runs/ph_q3/ph_q3.in
LOG=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_workflows/test_files/qe_Pb/phonon_dispersion_qgrid_convergence/qgrid_4_4_4/phonons_runs/ph_q3/ph_q3.log
STDERR=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_workflows/test_files/qe_Pb/phonon_dispersion_qgrid_convergence/qgrid_4_4_4/phonons_runs/ph_q3/ph_q3.stderr


$MPIRUN $EXECUTABLE -input $INPUT > $LOG 2> $STDERR
