import os

from .bases import BaseBandFreqParser
from ...file_structures import QEEPWBandEigStructure


class QEEPWBandEigParser(BaseBandFreqParser):
    """A Quantum Espresso band.eig file parser that extracts data from
    a band.eig file produced by the epw.x script.
    """
    _expected_ending = "band.eig"
    _loggername = "QEEPWBandEigParser"
    _structure_class = QEEPWBandEigStructure

    @classmethod
    def _filepath_from_meta(cls, meta, *args, **kwargs):
        return os.path.join(meta.rundir, cls._expected_ending)

    def _extract_data_many_atoms(self, lines):
        # energies are not grouped by 6 like for matdyn...
        return self._extract_data_1_atom(lines)
