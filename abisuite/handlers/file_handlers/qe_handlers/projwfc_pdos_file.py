from ..bases import BaseFileHandler
from ...file_parsers import QEProjwfcPDOSParser
from ...file_structures import QEProjwfcPDOSStructure
import logging


class QEProjwfcPDOSFile(BaseFileHandler):
    """Class that represents a pdos function file produced by a projwfc.x
    calculation from Quantum Espresso.
    """
    _loggername = "QEProjwfcPDOSFile"
    _parser_class = QEProjwfcPDOSParser
    _structure_class = QEProjwfcPDOSStructure

    # need to override from_meta_data since there are many files and we want
    # to be able to choose the ones we want at init time. this is already
    # done in the parser
    @classmethod
    def from_meta_data_file(cls, metadatafile, *args, **kwargs):
        paths = cls._parser_class._filepath_from_meta(
                        metadatafile, *args, **kwargs)
        return {key: cls.from_file(
                            path, loglevel=kwargs.get("loglevel", logging.INFO)
                            )
                for key, path in paths.items()}
