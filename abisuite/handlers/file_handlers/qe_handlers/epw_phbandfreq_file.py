from ..bases import BaseFileHandler
from ...file_parsers import QEEPWPHBandFreqParser
from ...file_structures import QEEPWPHBandFreqStructure


class QEEPWPHBandFreqFile(BaseFileHandler):
    """File handler class for a 'phband.freq' file produced by epw.x script
    from Quantum Espresso.
    """
    _loggername = "QEEPWPHBandFreqFile"
    _parser_class = QEEPWPHBandFreqParser
    _structure_class = QEEPWPHBandFreqStructure
