#!/bin/bash

MPIRUN="mpirun -np 4"
EXECUTABLE="/home/fgoudreault/Workspace/q-e/bin/epw.x -npool 4"
INPUT=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_workflows/test_files/qe_Pb/ibte_fsthick_convergence/fsthick0.1/ibte_run/ibte_run.in
LOG=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_workflows/test_files/qe_Pb/ibte_fsthick_convergence/fsthick0.1/ibte_run/ibte_run.log
STDERR=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_workflows/test_files/qe_Pb/ibte_fsthick_convergence/fsthick0.1/ibte_run/ibte_run.stderr


$MPIRUN $EXECUTABLE -input $INPUT > $LOG 2> $STDERR
