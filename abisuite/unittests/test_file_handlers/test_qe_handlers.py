from .bases import BaseInputFileHandlerTest, BaseFileHandlerTest
from ..variables_for_tests import (
        qe_dosx_vars, QEDOSDOSExample, QEDOSLogExample,
        qe_dynmatx_vars, qe_epsilonx_vars, qe_epwx_vars, qe_fsx_vars,
        qe_ld1x_vars,
        qe_matdynx_vars, qe_phx_dyn0_example, qe_phx_vars, qe_ppx_vars,
        qe_projwfcx_vars, QEProjwfcPDOSExample, QEProjwfcLogExample,
        qe_pwx_vars, qe_pw2wannier90x_vars, qe_q2rx_vars,
        qe_phxlog_example, qe_pwxlog_example,
        )
from ...handlers import (
        QEDOSDOSFile, QEDOSInputFile, QEDOSLogFile,
        QEDynmatInputFile, QEEpsilonInputFile, QEEPWInputFile, QEFSInputFile,
        QELD1InputFile,
        QEMatdynInputFile, QEPHDyn0File, QEPHInputFile, QEPHLogFile,
        QEPPInputFile,
        QEProjwfcInputFile, QEProjwfcLogFile, QEProjwfcPDOSFile,
        QEPWInputFile, QEPWLogFile, QEPW2Wannier90InputFile, QEQ2RInputFile,
        )
import unittest


# #############################################################################
# ########################### dos.x ###########################################
# #############################################################################


class TestQEDOSDOSFile(BaseFileHandlerTest, unittest.TestCase):
    _file_handler_class = QEDOSDOSFile
    _example_file = QEDOSDOSExample

    def test_read(self):
        super().test_read()
        # data directly taken from example file
        length = 401
        for item in (self.handler.energies, self.handler.dos,
                     self.handler.integrated_dos):
            self.assertEqual(len(item), length)
        # check fermi energy is well retrieved
        self.assertEqual(self.handler.fermi_energy, 0.0)


class TestQEDOSInputFile(BaseInputFileHandlerTest, unittest.TestCase):
    _file_handler_class = QEDOSInputFile
    _input_vars = qe_dosx_vars.copy()

    def _change_random_property(self, handler):
        # change Emin
        handler.input_variables["Emin"].value -= 1


class TestQEDOSLogFile(BaseFileHandlerTest, unittest.TestCase):
    _file_handler_class = QEDOSLogFile
    _example_file = QEDOSLogExample


# #############################################################################
# ########################### dynmat.x ########################################
# #############################################################################


class TestQEDynmatInputFile(BaseInputFileHandlerTest, unittest.TestCase):
    _file_handler_class = QEDynmatInputFile
    _input_vars = qe_dynmatx_vars.copy()

    def _change_random_property(self, handler):
        # change q point coordinate
        handler.input_variables["q(1)"] += 0.01

# #############################################################################
# ############################# epsilon.x #####################################
# #############################################################################


class TestQEEpsilonInputFile(BaseInputFileHandlerTest, unittest.TestCase):
    _file_handler_class = QEEpsilonInputFile
    _input_vars = qe_epsilonx_vars.copy()

    def _change_random_property(self, handler):
        handler.input_variables["wmax"] += 1.0

# #############################################################################
# ############################ epw.x ##########################################
# #############################################################################


class TestQEEPWInputFile(BaseInputFileHandlerTest, unittest.TestCase):
    _file_handler_class = QEEPWInputFile
    _input_vars = qe_epwx_vars.copy()

    def _change_random_property(self, handler):
        handler.input_variables["title"] += "_changed"


class TestQEFSInputFile(BaseInputFileHandlerTest, unittest.TestCase):
    _file_handler_class = QEFSInputFile
    _input_vars = qe_fsx_vars.copy()

    def _change_random_property(self, handler):
        # change prefix only
        handler.input_variables["prefix"] += "_new"


class TestQELD1InputFile(BaseInputFileHandlerTest, unittest.TestCase):
    _file_handler_class = QELD1InputFile
    _input_vars = qe_ld1x_vars.copy()

    def _change_random_property(self, handler):
        atom = handler.input_variables["atom"]
        if atom == "H":
            handler.input_variables["atom"] = "He"
        else:
            handler.input_variables["atom"] = "H"


class TestQEMatdynInputFile(BaseInputFileHandlerTest, unittest.TestCase):
    _file_handler_class = QEMatdynInputFile
    _input_vars = qe_matdynx_vars.copy()

    def _change_random_property(self, handler):
        # change prefix only
        handler.input_variables["flfrq"] += "_newfile"


class TestQEPHInputFile(BaseInputFileHandlerTest, unittest.TestCase):
    _file_handler_class = QEPHInputFile
    _input_vars = qe_phx_vars.copy()

    def _change_random_property(self, handler):
        # change the 'title' of computation
        handler.input_variables["title"].value += " changed"


class TestQEPHDyn0File(BaseFileHandlerTest, unittest.TestCase):
    _file_handler_class = QEPHDyn0File
    _example_file = qe_phx_dyn0_example


class TestQEPHLogFile(BaseFileHandlerTest, unittest.TestCase):
    _file_handler_class = QEPHLogFile
    _example_file = qe_phxlog_example


class TestQEPPInputFile(BaseInputFileHandlerTest, unittest.TestCase):
    _file_handler_class = QEPPInputFile
    _input_vars = qe_ppx_vars.copy()

    def _change_random_property(self, handler):
        handler.input_variables["kpoint"] += 1


# #############################################################################
# ########################### projwfc.x #######################################
# #############################################################################

class TestQEProjwfcInputFile(BaseInputFileHandlerTest, unittest.TestCase):
    _file_handler_class = QEProjwfcInputFile
    _input_vars = qe_projwfcx_vars.copy()

    def _change_random_property(self, handler):
        handler.input_variables["degauss"] += 0.01


class TestQEProjwfcLogFile(BaseFileHandlerTest, unittest.TestCase):
    _file_handler_class = QEProjwfcLogFile
    _example_file = QEProjwfcLogExample


class TestQEProjwfcPDOSFile(BaseFileHandlerTest, unittest.TestCase):
    _file_handler_class = QEProjwfcPDOSFile
    _example_file = QEProjwfcPDOSExample

    def test_read(self):
        super().test_read()
        # data directly taken from example file
        length = 2776
        for item in (self.handler.energies, self.handler.ldos, self.handler.pz,
                     self.handler.px, self.handler.py):
            self.assertEqual(len(item), length)


# #############################################################################
# ############################### pw.x ########################################
# #############################################################################

class TestQEPWInputFile(BaseInputFileHandlerTest, unittest.TestCase):
    _file_handler_class = QEPWInputFile
    _input_vars = qe_pwx_vars.copy()

    def _change_random_property(self, handler):
        # change an input variable
        handler.input_variables["ecutwfc"].value += 1.0


class TestQEPWLogFile(BaseFileHandlerTest, unittest.TestCase):
    _file_handler_class = QEPWLogFile
    _example_file = qe_pwxlog_example


# #############################################################################
# ########################## pw2wannier90.x ###################################
# #############################################################################


class TestQEPW2Wannier90InputFile(BaseInputFileHandlerTest, unittest.TestCase):
    _file_handler_class = QEPW2Wannier90InputFile
    _input_vars = qe_pw2wannier90x_vars.copy()

    def _change_random_property(self, handler):
        handler.input_variables["seedname"] += "new"


class TestQEQ2RInputFile(BaseInputFileHandlerTest, unittest.TestCase):
    _file_handler_class = QEQ2RInputFile
    _input_vars = qe_q2rx_vars.copy()

    def _change_random_property(self, handler):
        handler.input_variables["flfrc"] += "_new"
