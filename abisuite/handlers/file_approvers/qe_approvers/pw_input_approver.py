import os

from .bases import BaseQEInputParalApprover
from ..bases import BaseInputApprover
from ...file_structures import QEPWInputStructure
from ....routines import is_vector
from ....variables import ALL_QEPW_VARIABLES


class QEPWInputApprover(BaseInputApprover):
    """Class that checks if a set of input variable is valid
    for a quantum espresso calculation.
    """
    _loggername = "QEPWInputApprover"
    _variables_db = ALL_QEPW_VARIABLES
    _structure_class = QEPWInputStructure

    def validate(self):
        self._check_ecut()
        self._check_geometry()
        self._check_pseudo_dir()
        self._check_lspinorb()
        self._check_ibrav()
        self._check_smearing()
        self._check_verbosity_bandstructure()
        # if we have errors here, stop already
        if self.errors:
            self._has_been_validated = True
            return
        super().validate()

    def _validate_pseudos(self):
        super()._validate_pseudos()
        self._check_soc_for_us_paw_pseudos()

    def _check_ecut(self):
        # ecutrho must be >= ecutwfc
        if "ecutrho" not in self.input_variables:
            return
        if "ecutwfc" not in self.input_variables:
            self.errors.append("Need to set 'ecutwfc'.")
        ecutrho = self.input_variables["ecutrho"]
        ecutwfc = self.input_variables["ecutwfc"]
        if ecutrho < ecutwfc:
            self.errors.append(
                    f"'ecutrho'={ecutrho} must be >= than 'ecutwfc'={ecutwfc}")

    def _check_geometry(self):
        self._logger.debug("Checking geometry")
        for var in ("ntyp", "nat", "atomic_species", "atomic_positions"):
            if var not in self.input_variables:
                self.errors.append(f"'{var}' must be defined.")
        if self.errors:
            return
        ntyp = self.input_variables["ntyp"]
        nat = self.input_variables["nat"]
        at_spec = self.input_variables["atomic_species"]
        if "positions" not in self.input_variables["atomic_positions"]:
            self.errors.append(
                    "'atomic_positions' must have the key 'positions'.")
            return
        at_pos = self.input_variables["atomic_positions"]["positions"]
        if ntyp != len(at_spec):
            self.errors.append(f"ntyp={ntyp} but number of atom defined in"
                               f" atomic_species={len(at_spec)}")
        tot_nat = 0
        for spec, pos in at_pos.items():
            if is_vector(pos):
                # only one vector
                tot_nat += 1
                if len(pos) != 3:
                    self.errors.append(f"Positions ({spec}) must be length"
                                       " 3 vectors.")
            else:
                tot_nat += len(pos)
                for at in pos:
                    if len(at) != 3:
                        self.errors.append("Positions must be length"
                                           " 3 vectors.")
        if tot_nat != nat:
            self.errors.append(f"nat={nat} but total number of positions"
                               f"={tot_nat}")

    def _check_ibrav(self):
        self._logger.debug("Checking ibrav")
        # check that other parameters are present depending of ibrav
        if "ibrav" not in self.input_variables:
            self.errors.append("'ibrav' must be defined.")
            return
        ibrav = self.input_variables["ibrav"]
        if ibrav == 0:
            if "cell_parameters" not in self.input_variables:
                self.errors.append("ibrav=0 but cell_parameters not specified")
        if ibrav == 7:
            # check both celldm(1) and celldm(3) are defined
            if "celldm(1)" not in self.input_variables:
                self.errors.append(
                        f"For ibrav={ibrav.value}, user must give 'celldm(1)'."
                        )
            if "celldm(3)" not in self.input_variables:
                self.errors.append(
                        f"For ibrav={ibrav.value}, user must give 'celldm(3)'."
                        )

    def _check_smearing(self):
        # if occupations == smearing, we need to check that degauss is there
        if "occupations" not in self.input_variables:
            return
        if self.input_variables["occupations"] != "smearing":
            return
        if "degauss" not in self.input_variables:
            self.errors.append(
                    "'occupations'='smearing' but 'degauss' not given.")

    def _check_soc_for_us_paw_pseudos(self):
        # UltraSoft Potentials Full Relativistic that have SOC must be
        # used with lspinorb=True
        # same for PAW pseudos
        # lspinorb default is False
        lspinorb = self.input_variables.get("lspinorb", False)
        for pseudo in self.pseudo_files:
            if not pseudo.has_so:
                # nothing to do
                return
            if pseudo.is_paw and not lspinorb:
                self.errors.append(f"FR PAW pseudos "
                                   f"({os.path.basename(pseudo.path)})"
                                   " must be used with 'lspinorb' = True")
            if pseudo.is_ultrasoft and not lspinorb:
                self.errors.append(f"FR UltraSoft pseudos "
                                   f"({os.path.basename(pseudo.path)}) "
                                   "must be used with 'lspinorb' = True")

    def _check_pseudo_dir(self):
        if "pseudo_dir" not in self.input_variables:
            self.errors.append("'pseudo_dir' must be defined.")
            return
        pseudo_dir = self.input_variables["pseudo_dir"].value
        if not os.path.isdir(pseudo_dir):
            self.errors.append(f"{pseudo_dir} is not a directory.")

    def _check_verbosity_bandstructure(self):
        # check that verbosity is set to high if we ask to compute band
        # structure over more than 100 kpts
        # check if bandstructure calculation
        if self.input_variables.get("calculation", "scf") != "bands":
            return
        # get number of kpts first
        self._logger.debug("'bands' calculation detected. Check verbosity.")
        # default verbosity is low
        verbosity = self.input_variables.get("verbosity", "low")
        k_pts = self.input_variables["k_points"]
        param = k_pts.value["parameter"]
        if param == "crystal_b":
            weights = k_pts.value["weights"]
            nkpts = sum(weights[:-1])  # last value not used
        elif param == "automatic":
            # hard to tell, it will depend on symmetries.
            grid = k_pts.value["k_points"]
            nkpts = grid[0] * grid[1] * grid[2]
            if nkpts >= 100 and verbosity == "low":
                self._logger.warning("#kpts in grid might be >= 100 if not "
                                     "reduced by symmetries. Set verbosity to "
                                     "'high' just in case.")
        else:
            self._logger.error(f"k-points param {param} not implemented yet in"
                               " verbosity check.")
            raise NotImplementedError(f"{param}")
        if nkpts >= 100 and verbosity == "low":
            self.errors.append("Set 'verbosity' to high for band structure"
                               " calculation nkpts >= 100")

    def _check_lspinorb(self):
        # check that the noncolinar core has been activated
        self._logger.debug("Checking lspinorb")
        if "lspinorb" not in self.input_variables:
            return
        if self.input_variables["lspinorb"].value is False:
            return
        if "noncolin" not in self.input_variables:
            self.errors.append("'lspinorb' is True => 'noncolin'"
                               " must be True too")
            return
        if self.input_variables["noncolin"].value is False:
            self.errors.append("'lspinorb' is True => 'noncolin'"
                               " must be True too")


class QEPWInputParalApprover(BaseQEInputParalApprover):
    """Class that cross checks the input parameters of the pw.x script
    with the mpi command settings.
    """
    _loggername = "QEPWInputParalApprover"
    _input_approver_class = QEPWInputApprover
    _structure_class = QEPWInputStructure
