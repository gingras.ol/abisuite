import unittest

from .bases import BaseWriterTest
from ...handlers.file_parsers import MetaDataParser
from ...handlers.file_writers import MetaDataWriter


class TestMetaDataWriter(BaseWriterTest, unittest.TestCase):
    """Test case for the meta data file writer.
    """
    _parser_class = MetaDataParser
    _writer_class = MetaDataWriter

    def setUp(self):
        super().setUp(write=False)
        for prop in self.writer.structure.all_attributes:
            if prop in self.writer.structure.set_attributes:
                setattr(self.writer, prop, set())
                continue
            if prop in self.writer.structure.list_attributes:
                setattr(self.writer, prop, [])
                continue
            setattr(self.writer, prop, prop)
        self.writer.write()
