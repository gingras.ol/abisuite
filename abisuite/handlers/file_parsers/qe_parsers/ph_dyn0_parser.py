import os

from .bases import BaseQEParser
from ...file_structures import QEPHDyn0Structure
from ....routines import decompose_line


class QEPHDyn0Parser(BaseQEParser):
    """Parser for a .dyn0 file that was produced in a ph.x calculation.
    when more than 1 qpt.
    """
    _expected_ending = ".dyn0"
    _loggername = "QEPHDyn0Parser"
    _structure_class = QEPHDyn0Structure

    def _extract_data_from_lines(self, lines):
        # first line is qgrid
        # second line is number of qpts
        # following lines are the qpts coordinates
        s, i, f = decompose_line(lines[0])
        self.qgrid = i
        s, i, f = decompose_line(lines[1])
        self.nqpt = i[0]
        qpts = []
        for line in lines[2:]:
            s, i, f = decompose_line(line)
            if not len(f):
                # probably end of file
                break
            qpts.append(f)
        return {"qpts": qpts}

    # TODO: rebase with abinit DMFT parsers
    @classmethod
    def _filepath_from_meta(cls, meta, *args, **kwargs):
        odd = meta.output_data_dir
        for path in os.listdir(odd):
            if path.endswith(cls._expected_ending):
                return os.path.join(odd, path)
        raise FileNotFoundError(f"No .dyn0 file found in {odd}")
