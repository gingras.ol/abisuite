from .bases import CommonQEInputVariable
from ..bases import (variableDBIntegrityChecker, MANDATORY_VARIABLE_KEYS,
                     OPTIONAL_VARIABLE_KEYS)
import numpy as np


class EPWQpointsVar(CommonQEInputVariable):
    """Variable for the list of qpts at the end of the input file of a
    epw.x calculation.
    """
    @CommonQEInputVariable.value.setter
    def value(self, value):
        parameter = value["parameter"]
        if parameter != "cartesian":
            raise NotImplementedError(self.parameter)
        if "q_points" not in value:
            raise ValueError("Must provide the list of qpts.")
        if "weights" not in value:
            raise ValueError("Must provide the weights of each qpts.")
        nqs = len(value["q_points"])
        if nqs != len(value["weights"]):
            raise ValueError("len of weights does not match nqpts.")
        CommonQEInputVariable.value.fset(self, value)

    def __str__(self):
        string = f"{len(self.value['q_points'])} cartesian\n"
        # convert coords + weights into one single array and write it
        coords = np.array(self.value["q_points"]).tolist().copy()
        for coord, weight in zip(coords, self.value["weights"]):
            coord.append(weight)
        string += self.convert_2d_arr_to_str(coords, add_common_spaces=False)
        return string


class EPWOutputDir(CommonQEInputVariable):
    """Just the output_dir variable. For EPW, it requires to end with '/'.
    """
    @CommonQEInputVariable.value.setter
    def value(self, value):
        if not value.endswith("/"):
            value += "/"
        CommonQEInputVariable.value.fset(self, value)


# #############################################################################
# ################ Quantum Espresso eph.x script variables ####################
# #############################################################################

QEEPW_ALLOWED_BLOCKS = (None, "inputepw")
QEEPW_ALLOWED_CLASSES = (CommonQEInputVariable, EPWQpointsVar, EPWOutputDir)
RAW_QEEPW_VARIABLES = {
        "a2f": {
            "block": "inputepw",
            "class": CommonQEInputVariable,
            "mandatory": False,
            "type": bool,
            },
        "amass(*)": {
            "block": "inputepw",
            "class": CommonQEInputVariable,
            "mandatory": False,
            "type": float
            },
        "asr_typ": {
            "allowed": ("crystal", "one-dim", "simple", "zero-dim"),
            "block": "inputepw",
            "class": CommonQEInputVariable,
            "mandatory": False,
            "type": str,
            },
        "assume_metal": {
            "block": "inputepw",
            "class": CommonQEInputVariable,
            "mandatory": False,
            "type": bool,
            },
        "band_plot": {
            "block": "inputepw",
            "class": CommonQEInputVariable,
            "mandatory": False,
            "type": bool,
            },
        "bands_skipped": {
            "block": "inputepw",
            "class": CommonQEInputVariable,
            "mandatory": False,
            "type": str,
            },
        "carrier": {
            "block": "inputepw",
            "class": CommonQEInputVariable,
            "mandatory": False,
            "type": bool,
            },
        "degaussq": {
            "block": "inputepw",
            "class": CommonQEInputVariable,
            "mandatory": False,
            "min": 0.0,
            "type": float,
            },
        "degaussw": {
            "block": "inputepw",
            "class": CommonQEInputVariable,
            "mandatory": False,
            "min": 0.0,
            "type": float,
            },
        "dis_froz_max": {
            "block": "inputepw",
            "class": CommonQEInputVariable,
            "mandatory": False,
            "type": float,
            },
        "dis_froz_min": {
            "block": "inputepw",
            "class": CommonQEInputVariable,
            "mandatory": False,
            "type": float,
            },
        "dis_win_max": {
            "block": "inputepw",
            "class": CommonQEInputVariable,
            "mandatory": False,
            "type": float,
            },
        "dis_win_min": {
            "block": "inputepw",
            "class": CommonQEInputVariable,
            "mandatory": False,
            "type": float,
            },
        "dvscf_dir": {
            "block": "inputepw",
            "class": CommonQEInputVariable,
            "mandatory": False,
            "type": str,
            },
        "efermi_read": {
            "block": "inputepw",
            "class": CommonQEInputVariable,
            "mandatory": False,
            "type": bool,
            },
        "elecselfen": {
            "block": "inputepw",
            "class": CommonQEInputVariable,
            "mandatory": False,
            "type": bool,
            },
        "elph": {
            "block": "inputepw",
            "class": CommonQEInputVariable,
            "mandatory": False,
            "type": bool,
            },
        "epbread": {
            "block": "inputepw",
            "class": CommonQEInputVariable,
            "mandatory": False,
            "type": bool,
            },
        "epbwrite": {
            "block": "inputepw",
            "class": CommonQEInputVariable,
            "mandatory": False,
            "type": bool,
            },
        "ephwrite": {
            "block": "inputepw",
            "class": CommonQEInputVariable,
            "mandatory": False,
            "type": bool,
            },
        "epmatkqread": {
            "block": "inputepw",
            "class": CommonQEInputVariable,
            "mandatory": False,
            "type": bool,
            },
        "epwread": {
            "block": "inputepw",
            "class": CommonQEInputVariable,
            "mandatory": False,
            "type": bool,
            },
        "epwwrite": {
            "block": "inputepw",
            "class": CommonQEInputVariable,
            "mandatory": False,
            "type": bool,
            },
        "etf_mem": {
            "allowed": (0, 1, 2),
            "block": "inputepw",
            "class": CommonQEInputVariable,
            "mandatory": False,
            "type": int,
            },
        "fermi_energy": {
            "block": "inputepw",
            "class": CommonQEInputVariable,
            "mandatory": False,
            "type": float,
            },
        # don't make the filkf and filqf path convertible as they are very
        # very short and abs path would be too long very often
        "filkf": {
            "block": "inputepw",
            "class": CommonQEInputVariable,
            "mandatory": False,
            "max": 80,
            "type": str,
            },
        "filqf": {
            "block": "inputepw",
            "class": CommonQEInputVariable,
            "mandatory": False,
            "max": 80,
            "type": str,
            },
        "fsthick": {
            "block": "inputepw",
            "class": CommonQEInputVariable,
            "mandatory": False,
            "min": 0.0,
            "type": float,
            },
        "int_mob": {
            "block": "inputepw",
            "class": CommonQEInputVariable,
            "mandatory": False,
            "type": bool,
            },
        "iprint": {
            "block": "inputepw",
            "class": CommonQEInputVariable,
            "mandatory": False,
            "type": int,
            },
        "iterative_bte": {
            "block": "inputepw",
            "class": CommonQEInputVariable,
            "mandatory": False,
            "type": bool,
            },
        "iverbosity": {
            "block": "inputepw",
            "class": CommonQEInputVariable,
            "mandatory": False,
            "max": 3,
            "min": 0,
            "type": int,
            },
        "lifc": {
            "block": "inputepw",
            "class": CommonQEInputVariable,
            "mandatory": False,
            "type": bool,
            },
        "mp_mesh_k": {
            "block": "inputepw",
            "class": CommonQEInputVariable,
            "mandatory": False,
            "type": bool,
            },
        "mp_mesh_q": {
            "block": "inputepw",
            "class": CommonQEInputVariable,
            "mandatory": False,
            "type": bool,
            },
        # this variable has been removed
        # "nbndskip": {
        #    "block": "inputepw",
        #    "class": CommonQEInputVariable,
        #    "mandatory": False,
        #    "min": 0,
        #    "type": int,
        #    },
        "nbndsub": {
            "block": "inputepw",
            "class": CommonQEInputVariable,
            "mandatory": False,
            "min": 0,
            "type": int,
            },
        "ngaussw": {
            "allowed": (0, -1, -99),
            "block": "inputepw",
            "class": CommonQEInputVariable,
            "mandatory": False,
            "type": int,
            },
        "nk1": {
            "block": "inputepw",
            "class": CommonQEInputVariable,
            "mandatory": False,
            "min": 1,
            "type": int
            },
        "nk2": {
            "block": "inputepw",
            "class": CommonQEInputVariable,
            "mandatory": False,
            "min": 1,
            "type": int,
            },
        "nk3": {
            "block": "inputepw",
            "class": CommonQEInputVariable,
            "mandatory": False,
            "min": 1,
            "type": int
            },
        "nkf1": {
            "block": "inputepw",
            "class": CommonQEInputVariable,
            "mandatory": False,
            "min": 1,
            "type": int,
            },
        "nkf2": {
            "block": "inputepw",
            "class": CommonQEInputVariable,
            "mandatory": False,
            "min": 1,
            "type": int,
            },
        "nkf3": {
            "block": "inputepw",
            "class": CommonQEInputVariable,
            "mandatory": False,
            "min": 1,
            "type": int,
            },
        "nqf1": {
            "block": "inputepw",
            "class": CommonQEInputVariable,
            "mandatory": False,
            "min": 1,
            "type": int,
            },
        "nqf2": {
            "block": "inputepw",
            "class": CommonQEInputVariable,
            "mandatory": False,
            "min": 1,
            "type": int,
            },
        "nqf3": {
            "block": "inputepw",
            "class": CommonQEInputVariable,
            "mandatory": False,
            "min": 1,
            "type": int,
            },
        "nq1": {
            "block": "inputepw",
            "class": CommonQEInputVariable,
            "mandatory": False,
            "min": 1,
            "type": int
            },
        "nq2": {
            "block": "inputepw",
            "class": CommonQEInputVariable,
            "mandatory": False,
            "min": 1,
            "type": int,
            },
        "nq3": {
            "block": "inputepw",
            "class": CommonQEInputVariable,
            "mandatory": False,
            "min": 1,
            "type": int
            },
        "nstemp": {
            "block": "inputepw",
            "class": CommonQEInputVariable,
            "mandatory": False,
            "min": 0,
            "type": int,
            },
        "num_iter": {
            "block": "inputepw",
            "class": CommonQEInputVariable,
            "mandatory": False,
            "min": 1,
            "type": int,
            },
        "nw_specfun": {
            "block": "inputepw",
            "class": CommonQEInputVariable,
            "mandatory": False,
            "min": 1,
            "type": int,
            },
        "outdir": {
            "block": "inputepw",
            "class": EPWOutputDir,
            "mandatory": True,
            "type": str
            },
        "phonselfen": {
            "block": "inputepw",
            "class": CommonQEInputVariable,
            "mandatory": False,
            "type": bool,
            },
        "prefix": {
            "block": "inputepw",
            "class": CommonQEInputVariable,
            "mandatory": True,
            "type": str
            },
        "proj(*)": {
            "block": "inputepw",
            "class": CommonQEInputVariable,
            "mandatory": True,
            "type": str,
            },
        "q_points": {
            "block": None,
            "class": EPWQpointsVar,
            "mandatory": True,
            },
        "rand_k": {
            "block": "inputepw",
            "class": CommonQEInputVariable,
            "mandatory": False,
            "type": bool,
            },
        "rand_nk": {
            "block": "inputepw",
            "class": CommonQEInputVariable,
            "mandatory": False,
            "type": int,
            },
        "rand_nq": {
            "block": "inputepw",
            "class": CommonQEInputVariable,
            "mandatory": False,
            "type": int,
            },
        "rand_q": {
            "block": "inputepw",
            "class": CommonQEInputVariable,
            "mandatory": False,
            "type": bool,
            },
        "restart": {
            "block": "inputepw",
            "class": CommonQEInputVariable,
            "mandatory": False,
            "type": bool,
            },
        "restart_step": {
            "block": "inputepw",
            "class": CommonQEInputVariable,
            "mandatory": False,
            "min": 1,
            "type": int,
            },
        "scattering": {
            "block": "inputepw",
            "class": CommonQEInputVariable,
            "mandatory": False,
            "type": bool,
            },
        "scattering_serta": {
            "block": "inputepw",
            "class": CommonQEInputVariable,
            "mandatory": False,
            "type": bool,
            },
        "specfun_el": {
            "block": "inputepw",
            "class": CommonQEInputVariable,
            "mandatory": False,
            "type": bool,
            },
        "specfun_ph": {
            "block": "inputepw",
            "class": CommonQEInputVariable,
            "mandatory": False,
            "type": bool,
            },
        "temps": {
            "block": "inputepw",
            "class": CommonQEInputVariable,
            "mandatory": False,
            "type": list,
            },
        "title": {
            "block": None,
            "class": CommonQEInputVariable,
            "mandatory": True,
            "type": str,
            },
        "vme": {
            "allowed": ("dipole", "wannier"),
            "block": "inputepw",
            "class": CommonQEInputVariable,
            "mandatory": False,
            "type": str,
            },
        "wannierize": {
            "block": "inputepw",
            "class": CommonQEInputVariable,
            "mandatory": False,
            "type": bool,
            },
        "wdata(*)": {
            "block": "inputepw",
            "class": CommonQEInputVariable,
            "mandatory": False,
            "type": str,
            },
        "wmax_specfun": {
            "block": "inputepw",
            "class": CommonQEInputVariable,
            "mandatory": False,
            "type": float,
            },
        "wmin_specfun": {
            "block": "inputepw",
            "class": CommonQEInputVariable,
            "mandatory": False,
            "type": float,
            },
        }


ALL_QEEPW_VARIABLES = variableDBIntegrityChecker(
        RAW_QEEPW_VARIABLES,
        QEEPW_ALLOWED_BLOCKS,
        MANDATORY_VARIABLE_KEYS,
        QEEPW_ALLOWED_CLASSES,
        optional_keys=OPTIONAL_VARIABLE_KEYS)
