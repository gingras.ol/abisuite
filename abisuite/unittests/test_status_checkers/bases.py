import os
import tempfile

from ..routines_for_tests import copy_calculation
from ...exceptions import DevError
from ...handlers import CalculationDirectory


class BaseStatusCheckerTest:
    _calculation = None

    def setUp(self):
        if self._calculation is None:
            raise DevError("Need to set '_calculation'.")
        # create tempdir
        self.calc_tempdir = tempfile.TemporaryDirectory()
        self.calculation_path = os.path.join(self.calc_tempdir.name, "test")
        # copy calculaion into tempdir
        copy_calculation(
                self._calculation, self.calculation_path, new_pseudos=[])
        self.calc_dir = CalculationDirectory.from_calculation(
                            self.calculation_path)

    def tearDown(self):
        if os.path.isdir(self.calculation_path):
            self.calc_tempdir.cleanup()

    def test_status_computation(self):
        # just check the status by calling the status property of the calcdir
        status = self.calc_dir.status
        # calculation should be finished
        for prop in ("calculation_finished", "calculation_started", ):
            self.assertIn(prop, status)
            self.assertTrue(status[prop])


class BaseSCFStatusCheckerTest(BaseStatusCheckerTest):
    """A base class for unittest for status checkers of SCF calculations.
    """
    def test_status_computation(self):
        super().test_status_computation()
        status = self.calc_dir.status
        self.assertIn("calculation_converged", status)
        self.assertTrue(status["calculation_converged"])
