import abc
import itertools
from math import sqrt

import matplotlib.pyplot as plt
import numpy as np

from ..bases import BaseUtility
from ..handlers import CalculationDirectory, QEPWInputFile
from ..plotters import Plot
from ..routines import is_list_like


__SUPPORTED_BRAVAIS_LATTICES__ = ("bct", "fcc", "hex", )
__BRAV_TO_QE_IBRAV__ = {
        "bct": 7,
        "fcc": 2,
        "hex": 4,
        }


class Lattice(BaseUtility):
    """A python object that describes a Lattice. It can be used to define a
    lattice and visualize it. Also, it can be used to convert into specific
    dft package input variables.
    """
    _loggername = "Lattice"

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._bravais_lattice = None
        self._primitive_vectors = None
        self._reciprocal_vectors = None
        self._atomic_positions = None
        self._atomic_types = None
        self._atomic_masses = None
        self._atomic_pseudos = None
        self._lattice_parameters = None

    @property
    def atomic_masses(self):
        if self._atomic_masses is not None:
            return self._atomic_masses
        raise AttributeError("Need to set 'atomic_masses'.")

    @atomic_masses.setter
    def atomic_masses(self, at_mass):
        if isinstance(at_mass, float):
            at_mass = [at_mass]
        if not is_list_like(at_mass):
            raise TypeError(
                    f"Expected a list-like obj but got instead: '{at_mass}'.")
        try:
            if self.ntypat != len(at_mass):
                raise ValueError(
                        f"atomic_masses given: {at_mass} don't match other "
                        "atomic variables.")
        except AttributeError:
            pass
        self._atomic_masses = at_mass

    @property
    def atomic_positions(self):
        """Atomic positions in relative coordinates.
        """
        if self._atomic_positions is None:
            raise ValueError("Need to set 'atomic_positions'.")
        return self._atomic_positions

    @atomic_positions.setter
    def atomic_positions(self, pos):
        # atomic positions is a list of ntyp_at element containing
        # each nat(ityp)
        if not is_list_like(pos):
            raise TypeError(
                    f"Expected a list but got: '{pos}'.")
        try:
            if self.ntypat != len(pos):
                raise ValueError("Positions don't match number of atom types.")
        except AttributeError:
            pass
        new_pos = []
        for pos_this_type in pos:
            if not is_list_like(pos_this_type):
                raise TypeError(
                        f"Expected a list but got: '{pos_this_type}'.")
            new_pos_this_type = []
            for pos_this_at in pos_this_type:
                if not is_list_like(pos_this_type):
                    raise TypeError(
                            f"Expected a list but got: '{pos_this_at}'.")
                if len(pos_this_at) != 3:
                    raise ValueError("Atomic positions must be 3 comp. vec.")
                new_pos_this_type.append(np.array(pos_this_at))
            new_pos.append(np.array(new_pos_this_type))
        self._atomic_positions = new_pos

    @property
    def atomic_positions_cartesian(self):
        try:
            at_pos_cart = []
            for at_type in self.atomic_positions:
                at_pos_this_type = []
                for at_pos in at_type:
                    at_pos_this_type.append(
                            np.dot(self.primitive_vectors.T, at_pos).T)
                at_pos_cart.append(np.array(at_pos_this_type))
            return at_pos_cart
        except ValueError as e:
            self._logger.error("Need to define atomic positions in reduced "
                               "coordinates first.")
            raise e

    @property
    def atomic_pseudos(self):
        if self._atomic_pseudos is not None:
            return self._atomic_pseudos
        raise AttributeError("Need to set 'atomic_pseudos'.")

    @atomic_pseudos.setter
    def atomic_pseudos(self, at_pseudos):
        if isinstance(at_pseudos, str):
            at_pseudos = [at_pseudos]
        if not is_list_like(at_pseudos):
            raise TypeError(
                    f"Expected a list-like but got instead: '{at_pseudos}'.")
        try:
            if self.ntypat != len(at_pseudos):
                raise ValueError(
                        f"atomic_pseudos given: {at_pseudos} don't match other"
                        " atomic variables.")
        except AttributeError:
            pass
        self._atomic_pseudos = at_pseudos

    @property
    def atomic_types(self):
        if self._atomic_types is not None:
            return self._atomic_types
        raise ValueError("Need to set 'atomic_types'.")

    @atomic_types.setter
    def atomic_types(self, atom_types):
        if not is_list_like(atom_types):
            raise TypeError(
                    f"Expected a list but got instead: '{atom_types}'.")
        if self._atomic_positions is not None:
            if len(atom_types) != self.ntypat:
                raise ValueError(
                        f"'atom_types'={atom_types} does not match atomic "
                        "positions.")
        for at in atom_types:
            if not isinstance(at, str):
                raise TypeError("atomic_types must be strings.")
        self._atomic_types = atom_types

    @property
    def bravais_lattice(self):
        if self._bravais_lattice is None:
            raise ValueError("Need to set 'bravais_lattice' before using it.")
        return self._bravais_lattice

    @bravais_lattice.setter
    def bravais_lattice(self, bl):
        if bl not in __SUPPORTED_BRAVAIS_LATTICES__:
            raise NotImplementedError(bl)
        self._bravais_lattice = bl

    @property
    def cell_volume(self):
        return np.dot(self.primitive_vectors[0],
                      np.cross(self.primitive_vectors[1],
                               self.primitive_vectors[2]))

    @property
    def lattice_parameters(self):
        if self._lattice_parameters is not None:
            return self._lattice_parameters
        raise AttributeError("Need to set 'lattice_parameters'.")

    @lattice_parameters.setter
    def lattice_parameters(self, lat):
        if isinstance(lat, float):
            lat = np.array([lat] * 3)
        if not is_list_like(lat):
            raise TypeError(
                    f"Expected a list of 3 numbers or a single number but got "
                    f"'{lat}'.")
        self._lattice_parameters = lat

    @property
    def natoms(self):
        nat = 0
        for attype in self.atomic_positions:
            nat += len(attype)
        return nat

    @property
    def ntypat(self):
        # return the first len given that works
        if self._atomic_positions is not None:
            return len(self.atomic_positions)
        if self._atomic_masses is not None:
            return len(self.atomic_masses)
        if self._atomic_types is not None:
            return len(self.atomic_types)
        if self._atomic_pseudos is not None:
            return len(self.atomic_pseudos)
        raise AttributeError(
            "Need to define some atomic attributes to know 'ntypat'.")

    @property
    def primitive_vectors(self):
        """The three primitive vectors in cartesian coordinates.
        """
        if self._primitive_vectors is None:
            raise ValueError("Need to set 'primitive_vectors'.")
        return self._primitive_vectors

    @primitive_vectors.setter
    def primitive_vectors(self, pv):
        pv = np.array(pv)
        if pv.shape != (3, 3):
            raise ValueError(
                "Primitive vectors must be a list of 3 vectors.")
        self._primitive_vectors = pv

    @property
    def reciprocal_vectors(self):
        if self._reciprocal_vectors is not None:
            return self._reciprocal_vectors
        # compute reciprocal lattice vectors
        a1 = self.primitive_vectors[0]
        a2 = self.primitive_vectors[1]
        a3 = self.primitive_vectors[2]
        a1a2 = np.cross(a1, a2)
        a2a3 = np.cross(a2, a3)
        a3a1 = np.cross(a3, a1)
        prefact = 2 * np.pi / self.cell_volume
        self._reciprocal_vectors = prefact * np.array([a2a3, a3a1, a1a2])
        return self._reciprocal_vectors

    def reduced_to_cartesian_coordinates(
            self, reduced_vector, lattice="reciprocal"):
        """Converts reduced coordinates to cartesian coordinates.

        Parameters
        ----------
        reduced_vectors: vector, list-like
            The reduced coordinates vector. Can be an array of vectors as well.
            If so, the returned array will also be an array of vectors.
        lattice: str, optional, {'reciprocal', 'primitive'}
            The space in which we want to get cartesian coordinates.

        Returns
        -------
        Array of vectors or vector in cartesian coordinates
        depending on inputs.
        """
        if lattice == "reciprocal":
            basis = self.reciprocal_vectors
        elif lattice == "primitive":
            basis = self.primitive_vectors
        else:
            raise ValueError(lattice)
        return reduced_vector.dot(basis)

    def get_qe_input_variables(self):
        """Returns the dictionary of input variables that represents
        this Lattice for Quantum Espresso.
        """
        if self.bravais_lattice == "fcc":
            ibrav = __BRAV_TO_QE_IBRAV__[self.bravais_lattice]
            toreturn = {
                    "ibrav": ibrav,
                    "acell(1)": self.lattice_parameters[0],
                    }
        elif self.bravais_lattice == "bct":
            ibrav = __BRAV_TO_QE_IBRAV__[self.bravais_lattice]
            toreturn = {
                    "ibrav": ibrav,
                    "acell(1)": self.lattice_parameters[0],
                    "acell(3)": self.lattice_parameters[2],
                    }
        else:
            raise NotImplementedError(self.bravais_lattice)
        toreturn["ntyp"] = self.ntypat
        toreturn["nat"] = self.natoms
        toreturn["atomic_positions"] = {
                        "parameter": "alat",
                        "positions": {
                            at: pos for at, pos in zip(
                                self.atomic_types, self.atomic_positions)},
                            }
        toreturn["atomic_species"] = [
                {"atom": atom,
                 "atomic_mass": at_mass,
                 "pseudo": at_pseudo}
                for atom, at_mass, at_pseudo in zip(
                    self.atomic_types,
                    self.atomic_masses,
                    self.atomic_pseudos)]
        return toreturn

    def get_brillouin_zone_plot(self, kpoint_path=None, lines=True):
        """Get the Plot object that contains the BZ boundaries.

        Parameters
        ----------
        kpoint_path: list-like, optional
            If not None, gives a list of points that correspond to a path
            in the BZ. Useful to depict paths for band structures.
        lines: bool, optional
            If True, the points given in kpoint_path arg are linked with
            a line. Otherwise just the points are shown.

        Returns
        -------
        BrillouinZonePlot object
        """
        plot = BrillouinZonePlot(loglevel=self._loglevel)
        plot.lattice = self
        if kpoint_path is not None:
            plot.kpoint_path_lines = lines
            plot.kpoint_path = kpoint_path
        return plot

    def get_primitive_cell_plot(self):
        plot = PrimitiveCellPlot(loglevel=self._loglevel)
        plot.lattice = self
        return plot

    @classmethod
    def from_qe_pw(cls, path, *args, **kwargs):
        """Create a Lattice object from a qe_pw calculation by the pw.x
        script from Quantum Espresso.
        """
        with QEPWInputFile.from_calculation(
                path) as input_file:
            ibrav = input_file.input_variables["ibrav"]
            celldm1 = input_file.input_variables["celldm(1)"].value
            # set atomic positions
            at_pos = input_file.input_variables[
                        "atomic_positions"]["positions"]
            at_spe = input_file.input_variables["atomic_species"]
            return cls.from_qe_input_variables(
                    *args, ibrav=ibrav, celldm1=celldm1,
                    atomic_positions=at_pos, atomic_species=at_spe,
                    **kwargs)

    @classmethod
    def from_abinit_input_variables(
            cls, *args, rprim=None, acell=None, **kwargs):
        """Instanciate Lattice object from a dict of abinit input variables.

        Parameters
        ----------
        rprim: 3x3 array
            The matrix of primitive vectors.
        acell: 1x3 array
            The vector of acells.
        args & kwargs:
            all other args & kwargs go to the init method.
        """
        lattice = Lattice(*args, **kwargs)
        if rprim is not None:
            if acell is None:
                raise ValueError(
                        "Need to set 'acell' to specify primitive vectors.")
            rprimd = np.array(rprim).copy()
            for dim, a in enumerate(acell):
                rprimd[:, dim] *= a
            lattice.primitive_vectors = rprimd
        else:
            if acell is not None:
                raise ValueError(
                        "Need to set 'rprim' to get primitive vectors.")
        return lattice

    @classmethod
    def from_qe_input_variables(
            cls, *args, ibrav=None, atomic_species=None, atomic_positions=None,
            celldm1=None, celldm3=None,
            **kwargs):
        """Instanciate Lattice object from a dict of QE input variables.

        Parameters
        ----------
        atomic_species: dict, optional
            The dict representing the atomic_species variable.
        atomic_positions: dict, optional
            The dict representing the atomic_positions variable.
        celldm1: float
            The 'celldm(1)' variable.
        celldm3: float
            The 'celldm(3)' variable.
        ibrav: int
            The ibrav variable.
        args & kwargs:
            are passed to the Lattice init method.
        """
        inst = cls(*args, **kwargs)
        if atomic_species is not None:
            inst.atomic_types = list(atomic_positions.keys())
            inst.atomic_pseudos = [x["pseudo"] for x in atomic_species]
            inst.atomic_masses = [x["atomic_mass"] for x in atomic_species]
        if atomic_positions is not None:
            inst.atomic_positions = list(atomic_positions.values())
        if celldm1 is None:
            raise ValueError("'celldm1' must be specified.")
        if ibrav == 2:
            inst.bravais_lattice = "fcc"
            acell = celldm1
            inst.primitive_vectors = [
                    [-celldm1/2, 0.0, celldm1/2],
                    [0.0, celldm1/2, celldm1/2],
                    [-celldm1/2, celldm1/2, 0.0]]
            inst.lattice_parameters = np.array([acell, acell, acell])
        elif ibrav == 4:
            inst.bravais_lattice = "hex"
            inst.primitive_vectors = [
                    [celldm1, 0, 0],
                    [-celldm1 / 2, sqrt(3) / 2 * celldm1, 0.0],
                    [0.0, 0.0, celldm1 * celldm3]]
            inst.lattice_parameters = [celldm1, celldm1, celldm1 * celldm3]
        elif ibrav == 7:
            if celldm3 is None:
                raise ValueError("'celldm3' must be specified.")
            inst.bravais_lattice = "bct"
            acell = celldm1
            acell_c = celldm3 * acell
            inst.primitive_vectors = [
                    [acell/2, -acell/2, acell_c/2],
                    [acell/2, acell/2, acell_c/2],
                    [-acell/2, -acell/2, acell_c/2]]
            inst.lattice_parameters = np.array([acell, acell, acell_c])
        else:
            raise NotImplementedError(str(ibrav))
        return inst

    @classmethod
    def from_calculation(cls, path, *args, **kwargs):
        """Create a Lattice object from a calculation.
        """
        with CalculationDirectory.from_calculation(path) as calc:
            if calc.calctype == "qe_pw":
                return cls.from_qe_pw(path, *args, **kwargs)
            else:
                raise NotImplementedError(calc.calctype)


class WignerSeitzCellPlot(Plot, abc.ABC):
    """An object that can plot the Wigner-Seitz cell of a set of
    lattice vectors.
    """
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._lattice = None
        self._ws_lines = []

    @property
    def lattice(self):
        if self._lattice is None:
            raise ValueError("Need to set the Lattice.")
        return self._lattice

    @lattice.setter
    def lattice(self, lattice):
        if not isinstance(lattice, Lattice):
            raise TypeError("Need a Lattice object.")
        self._lattice = lattice
        self.set_wigner_seitz_cell()

    @property
    def _need_3d_projection(self):
        # override this property which is always True for BZ
        return True

    @abc.abstractproperty
    def _lattice_vectors(self):
        pass

    def set_wigner_seitz_cell(self, *args, **kwargs):
        """Set the Wigner-Seitz cell of the given lattice vectors.
        """
        # Algorithm taken from pymatgen:
        # https://github.com/materialsproject/pymatgen/blob/9f9f75306db11df87fde8cc3d044905459e54bd3/pymatgen/core/lattice.py#L951
        # https://github.com/materialsproject/pymatgen/blob/master/pymatgen/electronic_structure/plotter.py#L3817
        rec = self._lattice_vectors
        vec1 = rec[0]
        vec2 = rec[1]
        vec3 = rec[2]
        list_k_points = []
        for i, j, k in itertools.product([-1, 0, 1], [-1, 0, 1], [-1, 0, 1]):
            list_k_points.append(i * vec1 + j * vec2 + k * vec3)
        from scipy.spatial import Voronoi

        tess = Voronoi(list_k_points)
        ws = []
        self._ws_lines = []  # clear
        for r in tess.ridge_dict:
            if r[0] == 13 or r[1] == 13:
                ws.append([tess.vertices[i] for i in tess.ridge_dict[r]])
        # from this point, ws is a list of 'faces' for the Wigner-seitz cell
        # each 'face' is a list of 3D coordinates corresponding to the
        # 'corners' of the 'face'. A line is just the union of 2
        # adjacent corners
        for iface, face in enumerate(ws):
            for line in itertools.combinations(face, 2):
                # pair up each coordinates, don't repeat pairs
                # line is a tuple of 2 points in the face
                for jface, face2 in enumerate(ws):
                    if jface >= iface:
                        # continue if this face has already been checked
                        continue
                    # only draw a line between points who are both
                    # shared by the same face
                    for point in line:
                        if not any(np.all(point == x) for x in face2):
                            break
                    else:
                        # both points are shared by the same face. Draw a line
                        self.add_curve(
                                *zip(*line), color="k", linewidth=1)
                        self._ws_lines.append(self.curves[-1])

    def add_image(self, direction):
        """Adds an image of the Wigner-Seitz cell in the given direction.

        Parameter
        ---------
        direction : list-like
            Must be a list-like of 3 elements giving the direction where
            the cell is replicated.
        """
        if not is_list_like(direction):
            raise TypeError(
                    f"Expected a list but got instead: '{direction}'.")
        if len(direction) != 3:
            raise ValueError("Direction must be of len 3.")
        translate_vec = np.dot(np.array(direction), self._lattice_vectors.T)
        for line in self._ws_lines:
            newline = line.copy()
            newline.xdata += translate_vec[0]
            newline.ydata += translate_vec[1]
            newline.zdata += translate_vec[2]
            newline.color = "b"
            newline.alpha = 0.5
            self.curves.append(newline)


class BrillouinZonePlot(WignerSeitzCellPlot):
    """An object that can plot the Brillouin Zone of a Lattice.
    """
    _loggername = "BrillouinZonePlot"

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.xlabel = r"$k_x$"
        self.ylabel = r"$k_y$"
        self.zlabel = r"$k_z$"
        self._kpoint_path = None
        self.kpoint_path_lines = True

    @property
    def kpoint_path(self):
        return self._kpoint_path

    @kpoint_path.setter
    def kpoint_path(self, kpath):
        if not is_list_like(kpath):
            raise TypeError("'kpoint_path' must be a list.")
        for item in kpath:
            if not isinstance(item, dict):
                raise TypeError("'kpoint_path' must be a list of dicts.")
            if len(item) != 1:
                raise TypeError("'kpoint_path' dicts must have 1 element.")
        self._kpoint_path = kpath
        self.set_kpoint_path()

    @property
    def _lattice_vectors(self):
        return self.lattice.reciprocal_vectors

    def set_kpoint_path(self):
        """Sets the kpoint path in the plot to be displayed.

        Assuming kpoint_path attribute has been set with reduced
        coordinates.
        """
        for kpt1_dict, kpt2_dict in zip(self.kpoint_path[:-1],
                                        self.kpoint_path[1:]):
            # reduced coordinates
            kpt1 = np.array(list(kpt1_dict.values())[0]).copy()
            kpt2 = np.array(list(kpt2_dict.values())[0]).copy()
            # multiply by matrix of reciprocal vectors
            kpt1 = kpt1.dot(self.lattice.reciprocal_vectors)
            kpt2 = kpt2.dot(self.lattice.reciprocal_vectors)
            # for each pair, add a curve that joins the 2 points
            if not self.kpoint_path_lines:
                continue
            self.add_curve(
                    [kpt1[0], kpt2[0]], [kpt1[1], kpt2[1]], [kpt1[2], kpt2[2]],
                    color="k", linewidth=2)
        # add dot at each unique kpt
        kpts_done = []
        for kpt_dict in self.kpoint_path:
            kpt = np.array(list(kpt_dict.values())[0]).copy()
            kpt = kpt.dot(self.lattice.reciprocal_vectors)
            already_done = False
            for kpt_done in kpts_done:
                if all(kpt == kpt_done):
                    already_done = True
                    break
            if already_done:
                continue
            self.add_scatter([kpt[0]], [kpt[1]], [kpt[2]], s=25, color="r")
            # displace text label by 5%
            self.add_text(
                    list(kpt_dict.keys())[0], *(np.array(kpt) + 0.005),
                    color="b", zdir=None)
            kpts_done.append(kpt)


class PrimitiveCellPlot(WignerSeitzCellPlot):
    """An object that can plot the Primitive cell of a Lattice.
    """
    _loggername = "PrimitiveCellPlot"

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._atom_scatters = []
        self.xlabel = r"$x$"
        self.ylabel = r"$y$"
        self.zlabel = r"$z$"

    @property
    def _lattice_vectors(self):
        return self.lattice.primitive_vectors

    def add_image(self, direction):
        super().add_image(direction)
        # also add image of atoms
        translate_vec = np.dot(np.array(direction), self._lattice_vectors.T)
        for scatter in self._atom_scatters:
            newscatter = scatter.copy()
            newscatter.xdata += translate_vec[0]
            newscatter.ydata += translate_vec[1]
            newscatter.zdata += translate_vec[2]
            newscatter.alpha = 0.5
            newscatter.edgecolors = "none"
            newscatter.clear_label()
            self.scatters.append(newscatter)

    def set_wigner_seitz_cell(self):
        super().set_wigner_seitz_cell()
        self.set_atoms()

    def set_atoms(self):
        """Set the atomic positions on the plot.
        """
        # generate random colors for each atom type
        # taken from:
        # https://stackoverflow.com/a/25628397/6362595
        cmap = plt.cm.get_cmap("hsv", self.lattice.ntypat)
        colors = [cmap(x) for x in range(self.lattice.ntypat)]
        for at_type, atom_positions, color in zip(
                self.lattice.atomic_types,
                self.lattice.atomic_positions_cartesian,
                colors):
            self.add_scatter(
                    *atom_positions.T, color=color, s=20, label=at_type,
                    edgecolors="k")
            self._atom_scatters.append(self.scatters[-1])
