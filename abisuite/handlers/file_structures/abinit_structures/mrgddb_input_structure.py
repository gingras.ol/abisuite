from .bases import BaseAbinitFilesStructure


class AbinitMrgddbInputStructure(BaseAbinitFilesStructure):
    """Structure for a mrgddb input file.
    """
    all_attributes = (
                "output_file_path", "title", "ddb_paths",
                )
    convertible_path_attributes = (
                "output_file_path",
                )
    list_attributes = (
            "ddb_paths",
            )

    @property
    def nddb(self):
        return len(self.ddb_paths)

    # define this even if no input vars for this file. it is just for
    # consistency accross the code.
    @property
    def input_variables(self):
        return {}
