from .abinit_status_checkers import (
        AbinitCalculationStatusChecker, AbinitAnaddbCalculationStatusChecker,
        AbinitMrgddbCalculationStatusChecker,
        AbinitOpticCalculationStatusChecker,
        )
from .exceptions import (
        CalculationNotConvergedError,
        CalculationNotFinishedError,
        )
from .qe_status_checkers import (
        QEDOSCalculationStatusChecker,
        QEDynmatCalculationStatusChecker,
        QEEpsilonCalculationStatusChecker,
        QEEPWCalculationStatusChecker,
        QEFSCalculationStatusChecker,
        QELD1CalculationStatusChecker,
        QEMatdynCalculationStatusChecker,
        QEPHCalculationStatusChecker,
        QEPPCalculationStatusChecker,
        QEProjwfcCalculationStatusChecker,
        QEPWCalculationStatusChecker,
        QEPW2Wannier90CalculationStatusChecker,
        QEQ2RCalculationStatusChecker,
        )
from .wannier90_status_checkers import (
        Wannier90StatusChecker,
        )
from .status_tree import StatusTree


CALCTYPES_TO_STATUS_CHECKER_CLS = {
        "abinit": AbinitCalculationStatusChecker,
        "abinit_anaddb": AbinitAnaddbCalculationStatusChecker,
        "abinit_mrgddb": AbinitMrgddbCalculationStatusChecker,
        "abinit_optic": AbinitOpticCalculationStatusChecker,
        "qe_dos": QEDOSCalculationStatusChecker,
        "qe_dynmat": QEDynmatCalculationStatusChecker,
        "qe_epsilon": QEEpsilonCalculationStatusChecker,
        "qe_epw": QEEPWCalculationStatusChecker,
        "qe_fs": QEFSCalculationStatusChecker,
        "qe_ld1": QELD1CalculationStatusChecker,
        "qe_matdyn": QEMatdynCalculationStatusChecker,
        "qe_ph": QEPHCalculationStatusChecker,
        "qe_pp": QEPPCalculationStatusChecker,
        "qe_projwfc": QEProjwfcCalculationStatusChecker,
        "qe_pw": QEPWCalculationStatusChecker,
        "qe_pw2wannier90": QEPW2Wannier90CalculationStatusChecker,
        "qe_q2r": QEQ2RCalculationStatusChecker,
        "wannier90": Wannier90StatusChecker,
        }
