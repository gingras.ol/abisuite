.. _variables_api:

Variables Module
================

.. contents::
    :local:

This module contains the databases of input variables for each codes.
These databases are not necessarily 100% complete as there are
very often new variables added or modified. It thus need to be
updated quite often. If you ever want to use a variable that is
not stored inside a given database, don't hesitate to send me
a pull request!

TODO: make the doc for the API of this module
