import os
import pytest
import tempfile

from abisuite import (
        QEEPWSequencer, QEFermiSurfaceSequencer, QEIndividualPhononSequencer,
        QEPhononDispersionSequencer, QESCFSequencer,
        QEEcutConvergenceSequencer, QEEcutPhononConvergenceSequencer,
        QEEPWIBTESequencer, QEEPWZimanSequencer,
        QEKgridConvergenceSequencer, QEKgridPhononConvergenceSequencer,
        QERelaxationSequencer, QESmearingConvergenceSequencer,
        QESmearingPhononConvergenceSequencer,
        QEThermalExpansionSequencer, )
from .bases import (
        BasePhononSequencerTest, BaseBandStructureSequencerTest,
        BasePositiveKpointsNSCFSequencerTest, BaseSCFSequencerTest,
        BaseEcutConvergenceSequencerTest, BaseKgridConvergenceSequencerTest,
        BaseConvergenceSequencerTest, BasePhononConvergenceSequencerTest,
        BaseEcutPhononConvergenceSequencerTest,
        BaseKgridPhononConvergenceSequencerTest, BaseRelaxationSequencerTest,
        BaseSequencerTest,
        BaseSmearingConvergenceSequencerTest,
        BaseSmearingPhononConvergenceSequencerTest,
        )
from ..bases import TestCase
from ..routines_for_tests import copy_calculation, TemporaryCalculation
from ...exceptions import DevError
from ...handlers import (
    CalculationDirectory, MetaDataFile, CALCTYPES_TO_INPUT_FILE_CLS,
    )


here = os.path.dirname(os.path.abspath(__file__))
SCF_PSEUDO_DIR = os.path.join(here, "files", "pseudos")


class BaseQESCFSequencerTest(BaseSCFSequencerTest):
    """Base class for scf sequencer test cases with Quantum Espresso.
    """
    _scf_pseudo_dir = None
    _scf_script_name = "pw.x"

    def setUp(self):
        super().setUp()
        if self._scf_pseudo_dir is None:
            raise DevError(
                    f"Need to set '_scf_pseudo_dir' in '{self.__class__}'.")

    def _set_scf_done(self):
        BaseSCFSequencerTest._set_scf_done(
                self, pseudo_dir=self._scf_pseudo_dir)


# #############################################################################
# ####################### QERelaxationSequencer ###############################
# #############################################################################

QE_RELAX_EXAMPLE = os.path.join(
        here, "files", "qe_silicon_relax", "relax_run")
QE_RELAX_INPUT_VARIABLES = {
    "ibrav": 2,
    "celldm(1)": 10.3472,
    "ntyp": 1,
    "nat": 2,
    "ecutwfc": 35.0,
    "conv_thr": 1e-8,
    "tprnfor": True,
    "tstress": True,
    "atomic_species": [{"atom": "Si",
                        "pseudo": "Si.pbe-mt_fhi.UPF",
                        "atomic_mass": 28.085, }],
    "atomic_positions": {
        "parameter": "crystal",
        "positions": {"Si": [[0.0, 0.0, 0.0], [0.25, 0.25, 0.25]]}},
    "k_points": {
        "parameter": "automatic",
        "k_points": [4, 4, 4, 0, 0, 0],
        },
    "press_conv_thr": 0.001,
    "forc_conv_thr": 1e-5,
    "etot_conv_thr": 1e-6,
    "pseudo_dir": SCF_PSEUDO_DIR,
    }


@pytest.mark.order("first")
class TestQERelaxationSequencer(
        BaseQESCFSequencerTest, BaseRelaxationSequencerTest, TestCase):
    """Test case for the QERelaxationSequencer.
    """
    _scf_example = QE_RELAX_EXAMPLE
    _scf_input_variables = QE_RELAX_INPUT_VARIABLES.copy()
    _scf_pseudo_dir = SCF_PSEUDO_DIR
    _sequencer_class = QERelaxationSequencer

    def setUp(self):
        super().setUp()
        self.sequencer.maximum_relaxations = 3

    def _set_relax_atoms_done(self):
        copy_calculation(
                os.path.join(self._scf_example, "relax_atoms_only"),
                os.path.join(self.sequencer.scf_workdir, "relax_atoms_only"),
                pseudo_dir=self._scf_pseudo_dir)

    def _set_relax_cell_done(self):
        for basename in os.listdir(self._scf_example):
            if "atoms_only" in basename:
                continue
            ending = int(basename.split("_")[-1])
            if ending > 1:
                depends_on = os.path.join(
                    basename.replace("_" + str(ending), "_" + str(ending - 1)))
            else:
                if self.sequencer.relax_atoms:
                    depends_on = "relax_atoms_only"
                else:
                    depends_on = None
            if depends_on is not None:
                depends_on = [
                        os.path.join(self.sequencer.scf_workdir, depends_on)]
            else:
                depends_on = []
            copy_calculation(
                    os.path.join(self._scf_example, basename),
                    os.path.join(self.sequencer.scf_workdir, basename),
                    pseudo_dir=self._scf_pseudo_dir,
                    new_parents=depends_on)


class BaseQEConvergenceSequencerTest(
        BaseConvergenceSequencerTest, BaseQESCFSequencerTest):
    """Base class for ecut convergence sequencer with QE.
    """

    def _set_scf_done(self):
        BaseEcutConvergenceSequencerTest._set_scf_done(
                self, pseudo_dir=self._scf_pseudo_dir)


class BaseQEPhononSequencerTest(
        BasePhononSequencerTest, BaseQESCFSequencerTest):
    """Base class for phonons sequencer test cases with Quantum Espresso.
    """
    _phonons_script_name = "ph.x"
    _phonons_qpoint_grid = None

    def _set_phonons(self):
        if self._phonons_qpoint_grid is not None:
            self.sequencer.phonons_qpoint_grid = self._phonons_qpoint_grid
        BasePhononSequencerTest._set_phonons(self)


class BaseQEPhononConvergenceSequencerTest(
        BasePhononConvergenceSequencerTest, BaseQEPhononSequencerTest):
    """Base test case class for phonons convergence sequencers with QE.
    """
    pass


class BaseQEBandStructureSequencerTest(
        BaseBandStructureSequencerTest, BaseQESCFSequencerTest):
    """Base class for Band Sructures sequencers test cases with Quantum
    Espresso.
    """
    def _set_band_structure_done(self):
        super()._set_band_structure_done()
        # need to reset pseudo dir when copying calcdir
        with CALCTYPES_TO_INPUT_FILE_CLS["qe_pw"].from_calculation(
                        self.sequencer.band_structure_workdir) as input_file:
            input_file.input_variables["pseudo_dir"] = self._scf_pseudo_dir


# ############################################################################
# ################ SCF sequencer with Quantum Espresso #######################
# ############################################################################

SCF_EXAMPLE = os.path.join(here, "files", "qe_pb", "scf_run")
SCF_INPUT_VARIABLES = {
    "calculation": "scf",
    "ibrav": 2,
    "ntyp": 1,
    "nat": 1,
    "celldm(1)": 9.2225583816,
    "pseudo_dir": SCF_PSEUDO_DIR,
    "ecutwfc": 40,
    "occupations": "smearing",
    "degauss": 0.1,
    "conv_thr": 1.0e-6,
    "atomic_species": [
        {"atom": "Pb",
         "atomic_mass": 207.2,
         "pseudo": "pb_s.UPF",
         }],
    "k_points": {"parameter": "automatic",
                 "k_points": [4, 4, 4, 1, 1, 1],
                 },
    "atomic_positions": {"parameter": "crystal",
                         "positions": {"Pb": [0.0, 0.0, 0.0]}},
    }


@pytest.mark.order("first")
class TestQESCFSequencer(BaseQESCFSequencerTest, TestCase):
    """Test case for the SCF sequencer with Quantum Espresso.
    """
    _scf_example = SCF_EXAMPLE
    _scf_input_variables = SCF_INPUT_VARIABLES
    _scf_pseudo_dir = SCF_PSEUDO_DIR
    _sequencer_class = QESCFSequencer


SCF_NO_ECUT_INPUT_VARIABLES = {
    "ibrav": 2,
    "celldm(1)": 10.3472,
    "ntyp": 1,
    "nat": 2,
    "conv_thr": 1e-8,
    "pseudo_dir": SCF_PSEUDO_DIR,
    "atomic_species": [{"atom": "Si",
                        "pseudo": "Si.pbe-mt_fhi.UPF",
                        "atomic_mass": 28.085, }],
    "atomic_positions": {
        "parameter": "crystal",
        "positions": {"Si": [[0.0, 0.0, 0.0], [0.25, 0.25, 0.25]]}},
    "k_points": {
        "parameter": "automatic",
        "k_points": [4, 4, 4, 0, 0, 0],
        },
    }
ECUTS = [10, 20]
root = os.path.join(
        here, "files", "qe_silicon_ecut_convergence", "scf_run", "ecutwfc")
SCF_ECUTS_EXAMPLES = [os.path.join(root, x) for x in os.listdir(root)]


# ####################### QE Ecut convergence sequencer #######################
@pytest.mark.order("first")
class TestQEEcutConvergenceSequencer(
        BaseEcutConvergenceSequencerTest,
        BaseQEConvergenceSequencerTest, TestCase):
    """Test case for the QE Ecut convergence sequencer.
    """
    _ecuts = ECUTS
    _scf_input_variables = SCF_NO_ECUT_INPUT_VARIABLES.copy()
    _scf_example = SCF_ECUTS_EXAMPLES
    _scf_pseudo_dir = SCF_PSEUDO_DIR
    _sequencer_class = QEEcutConvergenceSequencer

    def _set_scf_done(self):
        BaseQEConvergenceSequencerTest._set_scf_done(self)


PHONON_CONVERGENCE_INPUT_VARIABLES = {
        "amass(1)": 28.085,
        }
root = os.path.join(
        here, "files", "qe_silicon_ecut_convergence", "phonon_run", "ecutwfc")
PHONONS_ECUT_CONVERGENCE_EXAMPLES = [
        os.path.join(root, x) for x in os.listdir(root)]


# ################### QE Ecut phonon convergence sequencer ####################
@pytest.mark.order("first")
class TestQEEcutPhononConvergenceSequencer(
        BaseEcutPhononConvergenceSequencerTest,
        BaseQEPhononConvergenceSequencerTest, TestCase):
    """Test case for the QE ecut phonons convergence sequencer."""
    _ecuts = ECUTS
    _phonons_input_variables = PHONON_CONVERGENCE_INPUT_VARIABLES.copy()
    _phonons_example = PHONONS_ECUT_CONVERGENCE_EXAMPLES
    _scf_input_variables = SCF_NO_ECUT_INPUT_VARIABLES.copy()
    _scf_example = SCF_ECUTS_EXAMPLES
    _scf_pseudo_dir = SCF_PSEUDO_DIR
    _sequencer_class = QEEcutPhononConvergenceSequencer

    def _set_scf_done(self):
        BaseQEConvergenceSequencerTest._set_scf_done(self)


SCF_NO_KGRID_INPUT_VARIABLES = {
    "ibrav": 2,
    "celldm(1)": 10.3472,
    "ntyp": 1,
    "nat": 2,
    "ecutwfc": 10.0,
    "conv_thr": 1e-8,
    "pseudo_dir": SCF_PSEUDO_DIR,
    "atomic_species": [{"atom": "Si",
                        "pseudo": "Si.pbe-mt_fhi.UPF",
                        "atomic_mass": 28.085, }],
    "atomic_positions": {
        "parameter": "crystal",
        "positions": {"Si": [[0.0, 0.0, 0.0], [0.25, 0.25, 0.25]]}},
    }
KGRIDS = [[4, 4, 4, 0, 0, 0], [5, 5, 5, 0, 0, 0]]
SCF_KGRIDS_EXAMPLES = [
        os.path.join(
            here, "files", "qe_silicon_kgrid_convergence/scf_run/",
            f"k_points_{x[0]}_{x[1]}_{x[2]}")
        for x in KGRIDS]
PHONONS_KGRID_CONVERGENCE_EXAMPLES = [
        os.path.join(
            here, "files", "qe_silicon_kgrid_convergence/ph_runs/",
            f"k_points_{x[0]}_{x[1]}_{x[2]}")
        for x in KGRIDS]


# ################ QE Kgrid Convergence Sequencer #############################
@pytest.mark.order("first")
class TestQEKgridConvergenceSequencer(
        BaseKgridConvergenceSequencerTest,
        BaseQEConvergenceSequencerTest, TestCase):
    """Test case for the QE Kgrid convergence sequencer.
    """
    _kgrids = KGRIDS
    _scf_input_variables = SCF_NO_KGRID_INPUT_VARIABLES.copy()
    _scf_example = SCF_KGRIDS_EXAMPLES
    _scf_pseudo_dir = SCF_PSEUDO_DIR
    _sequencer_class = QEKgridConvergenceSequencer

    def _set_scf_done(self):
        BaseQEConvergenceSequencerTest._set_scf_done(self)


# ########################## QE Kgrid Phonon Convergence Sequencer ############
@pytest.mark.order("first")
class TestQEKgridPhononConvergenceSequencer(
        BaseKgridPhononConvergenceSequencerTest,
        BaseQEPhononConvergenceSequencerTest, TestCase,
        ):
    """Test case for the QE kgrids phonon convergence sequencer."""
    _kgrids = KGRIDS
    _phonons_input_variables = PHONON_CONVERGENCE_INPUT_VARIABLES.copy()
    _phonons_example = PHONONS_KGRID_CONVERGENCE_EXAMPLES
    _scf_input_variables = SCF_NO_KGRID_INPUT_VARIABLES.copy()
    _scf_example = SCF_KGRIDS_EXAMPLES
    _scf_pseudo_dir = SCF_PSEUDO_DIR
    _sequencer_class = QEKgridPhononConvergenceSequencer

    def _set_scf_done(self):
        BaseQEConvergenceSequencerTest._set_scf_done(self)


# ############### QE smearing convergence sequencer ###########################
QE_SMEARING_KGRIDS = [[x, x, x, 1, 1, 1] for x in [2, 4, 8]]
QE_SMEARING_INPUT_VARS = {
    "calculation": "scf",
    "ibrav": 2,
    "ntyp": 1,
    "nat": 1,
    "celldm(1)": 9.2225583816,
    "ecutwfc": 40,
    "pseudo_dir": SCF_PSEUDO_DIR,
    "occupations": "smearing",
    "conv_thr": 1.0e-6,
    "atomic_species": [
        {"atom": "Pb",
         "atomic_mass": 207.2,
         "pseudo": "pb_s.UPF",
         }],
    "atomic_positions": {"parameter": "crystal",
                         "positions": {"Pb": [0.0, 0.0, 0.0]}},
    }
QE_SMEARING_EXAMPLES = os.path.join(
            here, "files", "qe_pb_smearing_convergence", "degauss")
QE_SMEARINGS = [0.1, 0.01]


@pytest.mark.order("first")
class TestQESmearingConvergenceSequencer(
        BaseSmearingConvergenceSequencerTest,
        BaseQEConvergenceSequencerTest, TestCase):
    """Test case for the QE smearing convergence sequencer.
    """
    _kgrids = QE_SMEARING_KGRIDS
    _scf_input_variables = QE_SMEARING_INPUT_VARS.copy()
    _scf_example = QE_SMEARING_EXAMPLES
    _scf_pseudo_dir = SCF_PSEUDO_DIR
    _sequencer_class = QESmearingConvergenceSequencer
    _smearings = QE_SMEARINGS
    _scf_convergence_criterion = 10

    def setUp(self):
        BaseSmearingConvergenceSequencerTest.setUp(self)
        BaseQEConvergenceSequencerTest.setUp(self)

    def _set_scf_done(self):
        BaseSmearingConvergenceSequencerTest._set_scf_done(
                self, pseudo_dir=self._scf_pseudo_dir)


QE_SMEARING_PHONONS_EXAMPLES = os.path.join(
            here, "files", "qe_pb_smearing_convergence",
            "phonons_runs", "degauss")


@pytest.mark.order("first")
class TestQESmearingPhononConvergenceSequencer(
        BaseSmearingPhononConvergenceSequencerTest,
        BaseQEPhononConvergenceSequencerTest, TestCase):
    """Test case for the QE smearing convergence sequencer.
    """
    _kgrids = QE_SMEARING_KGRIDS
    _smearings = QE_SMEARINGS
    _nphonons = 1
    _phonons_example = QE_SMEARING_PHONONS_EXAMPLES
    _phonons_input_variables = {"tr2_ph": 1e-8}
    _scf_input_variables = QE_SMEARING_INPUT_VARS.copy()
    _scf_example = QE_SMEARING_EXAMPLES
    _scf_pseudo_dir = SCF_PSEUDO_DIR
    _sequencer_class = QESmearingPhononConvergenceSequencer

    def setUp(self):
        BaseSmearingPhononConvergenceSequencerTest.setUp(self)
        BaseQEPhononConvergenceSequencerTest.setUp(self)

    def _set_scf_done(self):
        BaseSmearingConvergenceSequencerTest._set_scf_done(
                self, pseudo_dir=self._scf_pseudo_dir)

    def _set_phonons_done(self):
        BaseSmearingPhononConvergenceSequencerTest._set_phonons_done(
                self, new_pseudos=self._pseudos)


# #############################################################################
# ##################### QE NSCF Sequencers test cases #########################
# #############################################################################


class BaseQEPositiveKpointsNSCFSequencerTest(
        BasePositiveKpointsNSCFSequencerTest, BaseQESCFSequencerTest):
    """Base class for positive kpoints sequencer test cases with QE.
    """

    def _set_nscf_done(self):
        BasePositiveKpointsNSCFSequencerTest._set_nscf_done(
                self, pseudo_dir=self._scf_pseudo_dir)


# ################ QE Fermi Surface Sequencer Test ############################
QE_FERMI_SURFACE_SCF_INPUT_VARIABLES = {
    "calculation": "scf",
    "ibrav": 2,
    "ntyp": 1,
    "nat": 1,
    "celldm(1)": 9.2225583816,
    "ecutwfc": 40,
    "pseudo_dir": SCF_PSEUDO_DIR,
    "occupations": "smearing",
    "conv_thr": 1.0e-6,
    "atomic_species": [
        {"atom": "Pb",
         "atomic_mass": 207.2,
         "pseudo": "pb_s.UPF",
         }],
    "atomic_positions": {"parameter": "crystal",
                         "positions": {"Pb": [0.0, 0.0, 0.0]}},
    "k_points": {"parameter": "automatic",
                 "k_points": [10, 10, 10, 1, 1, 1]},
    "smearing": "methfessel-paxton",
    "degauss": 0.01,
    }
QE_FERMI_SURFACE_SCF_EXAMPLE = os.path.join(
        here, "files", "qe_pb_fermi_surface", "scf_run")
QE_FERMI_SURFACE_NSCF_KGRID = [30, 30, 30]
QE_FERMI_SURFACE_NSCF_INPUT_VARIABLES = {
    # "ibrav": 2,
    # "ntyp": 1,
    # "nat": 1,
    # "celldm(1)": 9.2225583816,
    # "ecutwfc": 40,
    # "pseudo_dir": SCF_PSEUDO_DIR,
    # "occupations": "smearing",
    # "conv_thr": 1.0e-6,
    # "atomic_species": [
    #     {"atom": "Pb",
    #      "atomic_mass": 207.2,
    #      "pseudo": "pb_s.UPF",
    #      }],
    # "atomic_positions": {"parameter": "crystal",
    #                      "positions": {"Pb": [0.0, 0.0, 0.0]}},
    # "k_points": {"parameter": "automatic",
    #              "k_points": [30, 30, 30, 0, 0, 0]},
    # "smearing": "methfessel-paxton",
    # "degauss": 0.01,
    }
QE_FERMI_SURFACE_NSCF_EXAMPLE = os.path.join(
        here, "files", "qe_pb_fermi_surface", "nscf_run")


@pytest.mark.order("first")
class TestQEFermiSurfaceSequencer(
        BaseQEPositiveKpointsNSCFSequencerTest, TestCase):
    """Test case for the fermi surface sequencer with Quantum Espresso.
    """
    _nscf_example = QE_FERMI_SURFACE_NSCF_EXAMPLE
    _nscf_kgrid = QE_FERMI_SURFACE_NSCF_KGRID
    _nscf_input_variables = QE_FERMI_SURFACE_NSCF_INPUT_VARIABLES.copy()
    _scf_example = QE_FERMI_SURFACE_SCF_EXAMPLE
    _scf_input_variables = QE_FERMI_SURFACE_SCF_INPUT_VARIABLES.copy()
    _scf_pseudo_dir = SCF_PSEUDO_DIR
    _sequencer_class = QEFermiSurfaceSequencer

    def setUp(self):
        super().setUp()
        self.sequencer.plot_show = False

    def test_starting_from_scratch(self):
        """Test that starting from scratch works.
        """
        self._set_scf()
        self.sequencer.write()
        self._check_scf_written()
        self.assertFalse(self.sequencer.sequence_completed)
        # cleanup and set done
        self.scf_tempdir.cleanup()
        self._set_scf_done()
        self._set_nscf()
        self.sequencer.write()
        self._check_nscf_written()
        self.nscf_tempdir.cleanup()
        self._set_nscf_done()
        self.sequencer.clear_sequence()
        self.assertTrue(self.sequencer.sequence_completed)

# #############################################################################
# ########## QE EPW Sequencer Test Case #######################################
# #############################################################################


NSCF_EXAMPLE = os.path.join(here, "files", "qe_pb", "nscf_run")
NSCF_INPUT_VARIABLES = {
    "tprnfor": True,
    "tstress": True,
    "ecutwfc": 40,
    "occupations": "smearing",
    "pseudo_dir": SCF_PSEUDO_DIR,
    "degauss": 0.1,
    "nbnd": 10,
    "conv_thr": 1.0e-8,
    }
NSCF_KGRID = [8] * 3
BAND_STRUCTURE_KPOINT_PATH = [
    {"L": [0.0, 0.5, 0.0]},
    {r"$\Gamma$": [0.0, 0.0, 0.0]},
    {"X": [0.0, 0.5, 0.5]},
    {"U": [0.0, 0.625, 0.375]},
    {"K": [-0.375, 0.375, 0.0]},
    {r"$\Gamma$": [0.0, 0.0, 0.0]},
    ]
BAND_STRUCTURE_KPOINT_PATH_DENSITY = 20
BAND_STRUCTURE_INPUT_VARIABLES = {
    "conv_thr": 1e-8,
    "diagonalization": "cg",
    "diago_full_acc": True,
    "pseudo_dir": SCF_PSEUDO_DIR,
    "ecutwfc": 40.0,
    "nbnd": 10,
    }
BAND_STRUCTURE_EXAMPLE = os.path.join(
        here, "files", "qe_pb", "band_structure_run")
PHONONS_EXAMPLE = os.path.join(here, "files", "qe_pb", "phonons_run", "ph_q")
NPHONONS = 3
PHONONS_INPUT_VARIABLES = {
        "ldisp": True,
        "nq1": 2,
        "nq2": 2,
        "nq3": 2,
        "tr2_ph": 1.0e-8,
        }
EPW_EXAMPLE = os.path.join(here, "files", "qe_pb", "epw_run")
EPW_INPUT_VARIABLES = {
        "amass(1)": 207.2,
        "elph": True,
        "epbwrite": True,
        "epbread": False,
        "epwwrite": True,
        "epwread": False,
        "nbndsub": 4,
        "bands_skipped": "exclude_bands = 1:5",
        "wannierize": True,
        "num_iter": 300,
        "dis_win_max": 21,
        "dis_win_min": -3,
        "dis_froz_min": -3,
        "dis_froz_max": 13.5,
        "proj(1)": "PB:sp3",
        "iverbosity": 0,
        "elecselfen": False,
        "phonselfen": True,
        "fsthick": 6,
        "degaussw": 0.1,
        "a2f": True,
        "nk1": 8,
        "nk2": 8,
        "nk3": 8,
        "nkf1": 10, "nkf2": 10, "nkf3": 10,
        "nqf1": 10, "nqf2": 10, "nqf3": 10,
        "vme": "wannier",
        }


@pytest.mark.order("first")
class TestQEEPWSequencer(
        BaseQEPhononSequencerTest, BaseQEPositiveKpointsNSCFSequencerTest,
        BaseQEBandStructureSequencerTest, TestCase):
    """Test case for the QEEPW sequencer.
    """
    _band_structure_example = BAND_STRUCTURE_EXAMPLE
    _band_structure_input_variables = BAND_STRUCTURE_INPUT_VARIABLES.copy()
    _band_structure_kpoint_path = BAND_STRUCTURE_KPOINT_PATH
    _band_structure_kpoint_path_density = BAND_STRUCTURE_KPOINT_PATH_DENSITY
    _nscf_example = NSCF_EXAMPLE
    _nscf_kgrid = NSCF_KGRID
    _nscf_input_variables = NSCF_INPUT_VARIABLES.copy()
    _nphonons = NPHONONS
    _phonons_example = PHONONS_EXAMPLE
    _phonons_input_variables = PHONONS_INPUT_VARIABLES.copy()
    _phonons_script_name = "ph.x"
    _scf_example = SCF_EXAMPLE
    _scf_input_variables = SCF_INPUT_VARIABLES.copy()
    _scf_pseudo_dir = SCF_PSEUDO_DIR
    _sequencer_class = QEEPWSequencer

    def setUp(self):
        BaseQEPhononSequencerTest.setUp(self)
        BaseQEPositiveKpointsNSCFSequencerTest.setUp(self)
        BaseQEBandStructureSequencerTest.setUp(self)
        # setting up temporary epw command
        self.epw_tempdir = tempfile.TemporaryDirectory()
        self.epw_workdir = os.path.join(self.epw_tempdir.name, "epw_run")
        self.epwx_command_file = tempfile.NamedTemporaryFile(suffix="epw.x")
        self.epwx_command = self.epwx_command_file.name
        # setting up sequencer object
        self.sequencer.epw_workdir = self.epw_workdir
        self.sequencer.epw_command = self.epwx_command
        self.sequencer.epw_queuing_system = "local"
        # just a fix for the files in place.
        self.phonons_workdir = os.path.join(
                self.phonons_tempdir.name, "ph_q")
        self.sequencer.phonons_workdir = self.phonons_workdir

    def tearDown(self):
        BaseQEPhononSequencerTest.tearDown(self)
        BaseQEPositiveKpointsNSCFSequencerTest.tearDown(self)
        BaseQEBandStructureSequencerTest.tearDown(self)
        if os.path.isdir(self.epw_tempdir.name):
            self.epw_tempdir.cleanup()
        if os.path.isfile(self.epwx_command):
            self.epwx_command_file.close()

    def test_whole_sequence(self):
        self._set_scf()
        self._set_nscf()
        self._set_band_structure()
        self._set_phonons()
        self._set_epw()
        self.sequencer.write()
        # check scf has been written and nothing else
        self._check_scf_written()
        self._check_nscf_not_written()
        self._check_phonons_not_written()
        self._check_band_structure_not_written()
        self._check_epw_not_written()
        self.assertFalse(self.sequencer.sequence_completed)
        # copy SCF and relaunch. check that all post scf calcs are written
        self.scf_tempdir.cleanup()
        self._set_scf_done()
        self.sequencer.write()
        self._check_nscf_written()
        self._check_phonons_written()
        self._check_band_structure_written()
        self._check_epw_not_written()
        self.assertFalse(self.sequencer.sequence_completed)
        # copy all post scf calc and relaunch. check epw written
        self.nscf_tempdir.cleanup()
        self.band_structure_tempdir.cleanup()
        self.phonons_tempdir.cleanup()
        self._set_nscf_done()
        self._set_band_structure_done()
        self._set_phonons_done()
        self.sequencer.write()
        self._check_epw_written()
        self.assertFalse(self.sequencer.sequence_completed)
        # copy epw calc and relaunch. check calculation is completed
        self.epw_tempdir.cleanup()
        self._set_epw_done()
        self.sequencer.write()
        self.assertTrue(self.sequencer.sequence_completed)

    def test_starting_before_epw(self):
        self._set_scf()
        self._set_scf_done()
        self._set_band_structure()
        self._set_band_structure_done()
        self._set_nscf()
        self._set_nscf_done()
        self._set_phonons()
        self._set_phonons_done()
        self._set_epw()
        self.sequencer.write()
        self._check_epw_written()
        self.assertFalse(self.sequencer.sequence_completed)
        # restart with SCF+PH vars
        self._set_scf()
        self._set_phonons()
        self.epw_tempdir.cleanup()
        self.sequencer.write()
        self._check_epw_written()
        self.assertFalse(self.sequencer.sequence_completed)

    def test_starting_from_scf_with_phonons_and_band_structure(self):
        self._set_scf_done()
        self._set_band_structure_done()
        self._set_nscf()
        self._set_phonons()
        self._set_phonons_done()
        self.sequencer.write()
        self._check_nscf_written()
        self._check_epw_not_written()
        self.assertFalse(self.sequencer.sequence_completed)
        # restart with SCF+PH+BS vars
        self._set_scf()
        self._set_band_structure()
        self._set_phonons()
        self.nscf_tempdir.cleanup()
        self.sequencer.write()
        self._check_nscf_written()
        self._check_epw_not_written()
        self.assertFalse(self.sequencer.sequence_completed)

    def test_starting_from_scf_with_nscf_and_band_structure(self):
        self._set_scf_done()
        self._set_band_structure_done()
        self._set_nscf_done()
        self._set_phonons()
        self.sequencer.write()
        self._check_phonons_written()
        self._check_epw_not_written()
        self.assertFalse(self.sequencer.sequence_completed)
        # restart with SCF+NSCF+BS vars
        self._set_scf()
        self._set_nscf()
        self._set_band_structure()
        self.phonons_tempdir.cleanup()
        self.sequencer.write()
        self._check_nscf_written()
        self._check_phonons_written()
        self._check_band_structure_written()
        self._check_epw_not_written()
        self.assertFalse(self.sequencer.sequence_completed)

    def test_starting_from_scf_with_nscf_and_phonons(self):
        self._set_scf_done()
        self._set_band_structure()
        self._set_nscf_done()
        self._set_nscf()
        self._set_band_structure()
        self._set_epw()
        self._set_phonons()
        self._set_phonons_done()
        self.sequencer.write()
        self._check_band_structure_written()
        # epw should be written
        self._check_epw_written()
        self.assertFalse(self.sequencer.sequence_completed)
        # restart with SCF+PH+NSCF vars
        self._set_scf()
        self._set_phonons()
        self._set_nscf()
        self.band_structure_tempdir.cleanup()
        self.epw_tempdir.cleanup()
        self.sequencer.write()
        self._check_band_structure_written()
        self._check_epw_written()
        self.assertFalse(self.sequencer.sequence_completed)

    def test_starting_from_scf_with_phonons(self):
        self._set_scf_done()
        self._set_band_structure()
        self._set_nscf()
        self._set_phonons()
        self._set_phonons_done()
        self.sequencer.write()
        self._check_nscf_written()
        self._check_band_structure_written()
        self._check_epw_not_written()
        self.assertFalse(self.sequencer.sequence_completed)
        # restart with SCF+phonons vars
        self._set_scf()
        self._set_phonons()
        self.band_structure_tempdir.cleanup()
        self.nscf_tempdir.cleanup()
        self.sequencer.write()
        self._check_nscf_written()
        self._check_band_structure_written()
        self._check_epw_not_written()
        self.assertFalse(self.sequencer.sequence_completed)

    def test_starting_from_scf_with_band_structure(self):
        self._set_scf_done()
        self._set_band_structure_done()
        self._set_nscf()
        self._set_phonons()
        self.sequencer.write()
        self._check_nscf_written()
        self._check_phonons_written()
        self._check_epw_not_written()
        self.assertFalse(self.sequencer.sequence_completed)
        # restart with SCF+BS vars
        self._set_scf()
        self._set_band_structure()
        self.phonons_tempdir.cleanup()
        self.nscf_tempdir.cleanup()
        self.sequencer.write()
        self._check_nscf_written()
        self._check_phonons_written()
        self._check_epw_not_written()
        self.assertFalse(self.sequencer.sequence_completed)

    def test_starting_from_scf_with_nscf(self):
        self._set_scf_done()
        self._set_nscf_done()
        self._set_band_structure()
        self._set_phonons()
        self.sequencer.write()
        self._check_phonons_written()
        self._check_band_structure_written()
        self._check_epw_not_written()
        self.assertFalse(self.sequencer.sequence_completed)
        # restart with SCF+NSCF vars
        self._set_scf()
        self._set_nscf()
        self.band_structure_tempdir.cleanup()
        self.phonons_tempdir.cleanup()
        self.sequencer.write()
        self._check_phonons_written()
        self._check_band_structure_written()
        self._check_epw_not_written()
        self.assertFalse(self.sequencer.sequence_completed)

    def test_starting_from_scf(self):
        self._set_scf_done()
        self._set_band_structure()
        self._set_nscf()
        self._set_phonons()
        self.sequencer.write()
        self._check_nscf_written()
        self._check_phonons_written()
        self._check_band_structure_written()
        self._check_epw_not_written()
        self.assertFalse(self.sequencer.sequence_completed)
        # restart with SCF vars
        self._set_scf()
        self.band_structure_tempdir.cleanup()
        self.phonons_tempdir.cleanup()
        self.nscf_tempdir.cleanup()
        self.sequencer.write()
        self._check_nscf_written()
        self._check_phonons_written()
        self._check_band_structure_written()
        self._check_epw_not_written()
        self.assertFalse(self.sequencer.sequence_completed)

    def test_starting_from_scratch(self):
        self._set_scf()
        self._set_phonons()
        self.sequencer.write()
        self._check_scf_written()
        self._check_nscf_not_written()
        self._check_phonons_not_written()
        self._check_epw_not_written()
        self._check_band_structure_not_written()
        self.assertFalse(self.sequencer.sequence_completed)

    def _check_epw(self, func):
        func(CalculationDirectory.is_calculation_directory(self.epw_workdir))

    def _check_epw_written(self):
        self._check_epw(self.assertTrue)

    def _check_epw_not_written(self):
        self._check_epw(self.assertFalse)

    def _set_epw(self):
        self.sequencer.epw_input_variables = EPW_INPUT_VARIABLES.copy()

    def _set_epw_done(self):
        copy_calculation(EPW_EXAMPLE,
                         self.epw_workdir)
        with MetaDataFile.from_calculation(
                self.epw_workdir) as meta:
            meta.parents = [self.nscf_workdir]
            for i in range(1, self._nphonons + 1):
                meta.parents.append(self.phonons_workdir + str(i))
        with CALCTYPES_TO_INPUT_FILE_CLS["qe_epw"].from_calculation(
                self.epw_workdir) as input_file:
            # need to reset dvscf_dir
            input_file.input_variables["dvscf_dir"] = meta.input_data_dir + "/"


# ############################ POST EPW SEQUENCERS ############################
class BasePostQEEPWSequencerTest(BaseSequencerTest):
    """Base class for tests involving post epw sequencers.
    """
    def setUp(self):
        super().setUp()
        self.post_epw_tempdir = tempfile.TemporaryDirectory()
        self.post_epw_workdir = os.path.join(
                self.post_epw_tempdir.name, "post_epw_run")
        self.epwx_command_file = tempfile.NamedTemporaryFile(suffix="epw.x")
        self.epwx_command = self.epwx_command_file.name
        prefix = self.sequencer._post_qe_epw_prefix
        setattr(self.sequencer, f"{prefix}workdir", self.post_epw_workdir)
        setattr(self.sequencer, f"{prefix}command", self.epwx_command)
        setattr(self.sequencer, f"{prefix}queuing_system", "local")
        # copy whole calculation tree leading to EPW calc into temp location
        self.temp_parents = tempfile.TemporaryDirectory()
        # scf
        self.temp_scf = TemporaryCalculation(
                SCF_EXAMPLE, copy_on_creation=True,
                copy_tempdir=self.temp_parents,
                )
        # phonons
        self.temp_phonons = []
        for ph in os.listdir(os.path.dirname(PHONONS_EXAMPLE)):
            self.temp_phonons.append(
                    TemporaryCalculation(
                        os.path.join(os.path.dirname(PHONONS_EXAMPLE), ph),
                        copy_on_creation=True, copy_tempdir=self.temp_parents,
                        new_parents=[self.temp_scf.path],
                        )
                    )
        # nscf
        self.temp_nscf = TemporaryCalculation(
                NSCF_EXAMPLE, copy_on_creation=True,
                copy_tempdir=self.temp_parents,
                new_parents=[self.temp_scf.path],
                )
        # epw
        self.temp_epw_interpolation = TemporaryCalculation(
                EPW_EXAMPLE, copy_on_creation=True,
                new_parents=[self.temp_nscf.path] + [
                    x.path for x in self.temp_phonons],
                copy_tempdir=self.temp_parents,
                )

    def tearDown(self):
        super().tearDown()
        self.temp_parents.cleanup()

    def test_starting_from_scratch(self):
        self._set_post_qe_epw()
        self.sequencer.write()
        self._check_post_qe_epw_written()
        self.assertFalse(self.sequencer.sequence_completed)

    def test_whole_sequence(self):
        self._set_post_qe_epw()
        self._set_post_qe_epw_done()
        self.sequencer.run()
        self.assertTrue(self.sequencer.sequence_completed)

    def _check_post_qe_epw_written(self):
        self.assertTrue(CalculationDirectory.is_calculation_directory(
            self.post_epw_workdir))

    def _set_post_qe_epw(self):
        self.sequencer.epw_interpolation_calculation = (
                self.temp_epw_interpolation.path)
        prefix = self.sequencer._post_qe_epw_prefix
        setattr(self.sequencer, f"{prefix}input_variables",
                self._post_qe_epw_input_variables.copy())
        self.sequencer.plot_show = False

    def _set_post_qe_epw_done(self):
        copy_calculation(
                self._post_qe_epw_example,
                self.post_epw_workdir,
                )


# QE IBTE Sequencer Test Case
class TestQEEPWIBTESequencer(BasePostQEEPWSequencerTest, TestCase):
    """Test case for the QEEPWIBTESequencer class.
    """
    _sequencer_class = QEEPWIBTESequencer
    _post_qe_epw_example = os.path.join(here, "files", "qe_pb", "ibte_run")
    _post_qe_epw_input_variables = {
                "assume_metal": True,
                "degaussw": 0.0,  # automatic smearing
                "iverbosity": 1,
                "etf_mem": 0,
                "fsthick": 0.2,
                "ngaussw": -99,  # FD
                "vme": "wannier",
                "temps": [100],
                "restart_step": 100,
                "nstemp": 1,
                "nkf1": 10, "nkf2": 10, "nkf3": 10,
                "nqf1": 10, "nqf2": 10, "nqf3": 10,
                }


# QE EPW Ziman Sequencer
class TestQEEPWZimanSequencer(BasePostQEEPWSequencerTest, TestCase):
    """Test case for the QEEPWZimanSequencer class.
    """
    _sequencer_class = QEEPWZimanSequencer
    _post_qe_epw_example = os.path.join(here, "files", "qe_pb", "ziman_run")
    _post_qe_epw_input_variables = {
            "a2f": True, "phonselfen": True, "degaussw": 0.005, "ngaussw": -99,
            "fsthick": 0.2, "rand_nk": 10 ** 3, "rand_nq": 10 ** 3,
            "rand_q": True, "rand_k": True, "vme": "wannier",
            }


# #############################################################################
# ########## QE Individual Phonon Sequencer Test Case #########################
# #############################################################################

SCF_INDIVIDUAL_PHONONS_EXAMPLE = os.path.join(
        here, "files", "qe_silicon_phonons", "scf_run")
PHONONS_INDIVIDUAL_PHONONS_EXAMPLE = os.path.join(
        here, "files", "qe_silicon_phonons", "ph_run_q")
SCF_INDIVIDUAL_PHONONS_INPUT_VARIABLES = {
    "ibrav": 2,
    "celldm(1)": 10.3472,
    "ntyp": 1,
    "nat": 2,
    "ecutwfc": 25.0,
    "conv_thr": 1e-8,
    "pseudo_dir": SCF_PSEUDO_DIR,
    "atomic_species": [{"atom": "Si",
                        "pseudo": "Si.pbe-mt_fhi.UPF",
                        "atomic_mass": 28.085, }],
    "atomic_positions": {
        "parameter": "crystal",
        "positions": {"Si": [[0.0, 0.0, 0.0], [0.25, 0.25, 0.25]]}},
    "k_points": {
        "parameter": "automatic",
        "k_points": [4, 4, 4, 0, 0, 0],
        },
    }
PHONONS_INDIVIDUAL_PHONONS_INPUT_VARIABLES = {
    "amass(1)": 28.086,
    "nq1": 2,
    "nq2": 2,
    "nq3": 2,
    "ldisp": True,
    "tr2_ph": 1e-8,
    }
NPHONONS_INDIVIDUAL_PHONONS = 3


@pytest.mark.order("first")
class TestQEIndividualPhononSequencer(
        BaseQEPhononSequencerTest, TestCase):
    """TestCase for the QEIndividualPhononSequencer class.
    """
    _nphonons = NPHONONS_INDIVIDUAL_PHONONS
    _phonons_example = PHONONS_INDIVIDUAL_PHONONS_EXAMPLE
    _phonons_input_variables = PHONONS_INDIVIDUAL_PHONONS_INPUT_VARIABLES
    _phonons_script_name = "ph.x"
    _scf_example = SCF_INDIVIDUAL_PHONONS_EXAMPLE
    _scf_input_variables = SCF_INDIVIDUAL_PHONONS_INPUT_VARIABLES.copy()
    _scf_pseudo_dir = SCF_PSEUDO_DIR
    _sequencer_class = QEIndividualPhononSequencer


# #############################################################################
# ########## QE Phonon dispersion sequencer test case #########################
# #############################################################################


Gamma = [0.0, 0.0, 0.0]
X = [-0.5, 0.0, 0.0]  # [-0.5, 1.0, 0.0]
W = [-0.75, -0.5, -0.25]  # [-0.75, 1.5, -0.25]
K = [-0.75, -0.5, -3/8]  # [-0.75, 1.5, -3/8]
L = [-0.5, -0.5, -0.5]  # [-0.5, 1.5, -0.5]
U = [-5/8, -0.5, -0.25]  # [-5/8, 1.5, -0.25]
# PATH
MATDYN_QPTS = [{r"$\Gamma$": Gamma}, {"X": X}, {"W": W}, {"K": K},
               {r"$\Gamma$": Gamma}, {"L": L}, {"U": U}, {"W": W},
               {"L": L}, {"K": K},
               {"U": U}, {"X": X}]
MATDYN_INPUT_VARIABLES = {
        "asr": "simple",
        }
MATDYN_DENSITY = 20
MATDYN_EXAMPLE = os.path.join(here, "files", "qe_silicon_phonons",
                              "matdyn_run")
Q2R_EXAMPLE = os.path.join(here, "files", "qe_silicon_phonons", "q2r_run")
Q2R_INPUT_VARIABLES = {}


@pytest.mark.order("first")
class TestQEPhononDispersionSequencer(
        BaseQEPhononSequencerTest, TestCase):
    """TestCase for the QE Phonon Dispersion Sequencer class.
    """
    _phonons_example = PHONONS_INDIVIDUAL_PHONONS_EXAMPLE
    _phonons_input_variables = PHONONS_INDIVIDUAL_PHONONS_INPUT_VARIABLES
    _scf_example = SCF_INDIVIDUAL_PHONONS_EXAMPLE
    _scf_pseudo_dir = SCF_PSEUDO_DIR
    _scf_input_variables = SCF_INDIVIDUAL_PHONONS_INPUT_VARIABLES.copy()
    _sequencer_class = QEPhononDispersionSequencer

    def setUp(self):
        super().setUp()
        self.sequencer.phonons_qpoint_grid = [
                self._phonons_input_variables[f"nq{i}"] for i in range(1, 4)]
        self.q2r_tempdir = tempfile.TemporaryDirectory()
        self.q2r_workdir = os.path.join(self.q2r_tempdir.name, "q2r_run")
        self.matdyn_tempdir = tempfile.TemporaryDirectory()
        self.matdyn_workdir = os.path.join(
                self.matdyn_tempdir.name, "matdyn_run")
        self.q2rx_command_file = tempfile.NamedTemporaryFile(suffix="q2r.x")
        self.q2rx_command = self.q2rx_command_file.name
        self.matdynx_command_file = tempfile.NamedTemporaryFile(
                suffix="matdyn.x")
        self.matdynx_command = self.matdynx_command_file.name
        # set sequencer
        self.sequencer.q2r_queuing_system = "local"
        self.sequencer.matdyn_queuing_system = "local"
        self.sequencer.q2r_workdir = self.q2r_workdir
        self.sequencer.matdyn_workdir = self.matdyn_workdir
        self.sequencer.q2r_command = self.q2rx_command
        self.sequencer.matdyn_command = self.matdynx_command
        self.sequencer.plot_show = False
        self.sequencer.plot_save = os.path.join(
                self.matdyn_workdir, "plot.pdf")

    def test_whole_sequence(self):
        self._set_scf_done()
        self._set_phonons()
        self._set_phonons_done()
        self._set_q2r()
        self._set_matdyn()
        self.sequencer.write()
        self._check_q2r_written()
        self._check_matdyn_not_written()
        self.assertFalse(self.sequencer.sequence_completed)
        self.q2r_tempdir.cleanup()
        self._set_q2r_done()
        self.sequencer.write()
        self._check_matdyn_written()
        self.assertFalse(self.sequencer.sequence_completed)
        self.matdyn_tempdir.cleanup()
        self._set_matdyn_done()
        self.sequencer.launch()
        self.assertTrue(self.sequencer.sequence_completed)
        self._check_plot_written()

    def test_start_from_q2r(self):
        self._set_scf_done()
        self._set_phonons()
        self._set_phonons_done()
        self._set_q2r_done()
        self._set_matdyn()
        self.sequencer.write()
        self._check_matdyn_written()
        self._check_plot_not_written()
        self.assertFalse(self.sequencer.sequence_completed)
        # copy matdyn and check plot is run afterwards
        # this is done in test whole sequence no need to rerun

    def test_start_from_phonons(self):
        self._set_scf_done()
        self._set_phonons()
        self._set_phonons_done()
        self._set_q2r()
        self.sequencer.write()
        self._check_q2r_written()
        self._check_matdyn_not_written()
        self._check_plot_not_written()
        self.assertFalse(self.sequencer.sequence_completed)

    def test_starting_from_scf(self):
        self._set_scf_done()
        self._set_phonons()
        self.sequencer.write()
        self._check_scf_written()
        self._check_phonons_written()
        self._check_q2r_not_written()
        self._check_matdyn_not_written()
        self._check_plot_not_written()
        self.assertFalse(self.sequencer.sequence_completed)
        # set scf input variables, and restart. check its good
        self.phonons_tempdir.cleanup()
        self._set_scf()
        self.sequencer.write()
        self._check_scf_written()
        self._check_phonons_written()
        self._check_q2r_not_written()
        self._check_matdyn_not_written()
        self._check_plot_not_written()
        self.assertFalse(self.sequencer.sequence_completed)

    def test_starting_from_scratch(self):
        self._set_scf()
        self._set_phonons()
        self.sequencer.write()
        self._check_scf_written()
        self._check_phonons_not_written()
        self._check_q2r_not_written()
        self._check_matdyn_not_written()
        self._check_plot_not_written()
        self.assertFalse(self.sequencer.sequence_completed)

    def _check_matdyn(self, func):
        func(CalculationDirectory.is_calculation_directory(
                    self.sequencer.matdyn_workdir))

    def _check_matdyn_written(self):
        self._check_matdyn(self.assertTrue)

    def _check_matdyn_not_written(self):
        self._check_matdyn(self.assertFalse)

    def _check_q2r(self, func):
        func(CalculationDirectory.is_calculation_directory(
                    self.sequencer.q2r_workdir))

    def _check_q2r_written(self):
        self._check_q2r(self.assertTrue)

    def _check_q2r_not_written(self):
        self._check_q2r(self.assertFalse)

    def _check_plot(self, func):
        func(os.path.isfile(self.sequencer.plot_save))

    def _check_plot_written(self):
        self._check_plot(self.assertTrue)

    def _check_plot_not_written(self):
        self._check_plot(self.assertFalse)

    def _set_matdyn(self):
        self.sequencer.qpoint_path = MATDYN_QPTS
        self.sequencer.qpoint_path_density = MATDYN_DENSITY
        self.sequencer.matdyn_input_variables = MATDYN_INPUT_VARIABLES.copy()

    def _set_matdyn_done(self):
        copy_calculation(MATDYN_EXAMPLE, self.matdyn_workdir)
        with MetaDataFile.from_calculation(self.matdyn_workdir) as meta:
            meta.parents = [self.q2r_workdir]
        with CALCTYPES_TO_INPUT_FILE_CLS["qe_matdyn"].from_calculation(
                self.matdyn_workdir) as input_file:
            newout = meta.output_data_dir
            newin = meta.input_data_dir
            # paths to output data dir
            for varname in ("flfrq", "flvec", "fleig"):
                oldname = os.path.basename(
                     input_file.input_variables[varname].value)
                input_file.input_variables[varname] = os.path.join(
                        newout, oldname)
            # paths to input data dir
            for varname in ("flfrc", ):
                oldname = os.path.basename(
                     input_file.input_variables[varname].value)
                input_file.input_variables[varname] = os.path.join(
                        newin, oldname)

    def _set_q2r(self):
        self.sequencer.q2r_input_variables = Q2R_INPUT_VARIABLES

    def _set_q2r_done(self):
        newparents = []
        for calc in self.sequencer.sequence:
            if calc.calctype != "qe_ph":
                continue
            newparents.append(calc.workdir)
        copy_calculation(
                Q2R_EXAMPLE, self.q2r_workdir, new_parents=newparents)


# #############################################################################
# ################## Thermal Expansion Sequencer ##############################
# #############################################################################


THERMAL_EXPANSION_SCF_INPUT_VARIABLES = {
    "calculation": "scf",
    "ibrav": 2,
    "ntyp": 1,
    "nat": 1,
    "celldm(1)": 9.2225583816,
    "ecutwfc": 40,
    "pseudo_dir": SCF_PSEUDO_DIR,
    "occupations": "smearing",
    "degauss": 0.1,
    "conv_thr": 1.0e-8,
    "atomic_species": [
        {"atom": "Pb",
         "atomic_mass": 207.2,
         "pseudo": "pb_s.UPF",
         }],
    "k_points": {"parameter": "automatic",
                 "k_points": [6, 6, 6, 1, 1, 1],
                 },
    "atomic_positions": {"parameter": "crystal",
                         "positions": {"Pb": [0.0, 0.0, 0.0]}},
    }
THERMAL_EXPANSION_PHONONS_INPUT_VARIABLES = {
        "ldisp": True,
        "tr2_ph": 1e-12,
        }
THERMAL_DELTAS = [0, -1, 1, -3, 3, -5, 5]
endings = [f"_vol+{delta}percent"
           if delta >= 0 else f"_vol{delta}percent"
           for delta in THERMAL_DELTAS]
THERMAL_EXPANSION_SCF_EXAMPLES = [os.path.join(
        here, "files", "qe_pb_thermal_expansion", "scf_runs", "scf" + x)
        for x in endings]
THERMAL_EXPANSION_PHONONS_EXAMPLES = [os.path.join(
        here, "files", "qe_pb_thermal_expansion", "ph_runs", "ph" + x)
        for x in endings]
THERMAL_EXPANSION_Q2R_EXAMPLES = [os.path.join(
        here, "files", "qe_pb_thermal_expansion", "q2r_runs", "q2r" + x)
        for x in endings]
THERMAL_EXPANSION_MATDYN_EXAMPLES = [os.path.join(
        here, "files", "qe_pb_thermal_expansion", "matdyn_runs", "matdyn" + x)
        for x in endings]


@pytest.mark.order("first")
class TestQEThermalExpansionSequencer(BaseQEPhononSequencerTest, TestCase):
    """Test case for the thermal expansion sequencer of Quantum Espresso.
    """
    _phonons_example = THERMAL_EXPANSION_PHONONS_EXAMPLES
    _phonons_input_variables = THERMAL_EXPANSION_PHONONS_INPUT_VARIABLES.copy()
    _phonons_script_name = "ph.x"
    _phonons_qpoint_grid = [2, 2, 2]
    _scf_example = THERMAL_EXPANSION_SCF_EXAMPLES
    _scf_input_variables = THERMAL_EXPANSION_SCF_INPUT_VARIABLES.copy()
    _scf_pseudo_dir = SCF_PSEUDO_DIR
    _sequencer_class = QEThermalExpansionSequencer

    def setUp(self):
        BaseQEPhononSequencerTest.setUp(self)
        # just a fix for the files in place.
        self.scf_workdir = os.path.join(
                self.scf_tempdir.name, "scf_runs")
        self.sequencer.scf_workdir = os.path.join(
                self.scf_workdir, "scf")
        self.phonons_workdir = os.path.join(
                self.phonons_tempdir.name, "ph_runs", "ph")
        self.sequencer.phonons_workdir = self.phonons_workdir
        self.q2r_tempdir = tempfile.TemporaryDirectory()
        # use normpath because for windows, you can't mix forward and
        # backwards slashes
        self.q2r_workdir = os.path.normpath(
            os.path.join(self.q2r_tempdir.name, "q2r_runs/q2r"))
        self.matdyn_tempdir = tempfile.TemporaryDirectory()
        self.matdyn_workdir = os.path.normpath(os.path.join(
                self.matdyn_tempdir.name, "matdyn_runs/matdyn"))
        self.q2rx_command_file = tempfile.NamedTemporaryFile(suffix="q2r.x")
        self.q2rx_command = self.q2rx_command_file.name
        self.matdynx_command_file = tempfile.NamedTemporaryFile(
                suffix="matdyn.x")
        self.matdynx_command = self.matdynx_command_file.name
        # set sequencer
        self.sequencer.q2r_queuing_system = "local"
        self.sequencer.matdyn_queuing_system = "local"
        self.sequencer.q2r_workdir = self.q2r_workdir
        self.sequencer.matdyn_workdir = self.matdyn_workdir
        self.sequencer.q2r_command = self.q2rx_command
        self.sequencer.matdyn_command = self.matdynx_command
        self.sequencer.plot_show = False
        self.sequencer.plot_save = os.path.join(
                os.path.dirname(self.matdyn_workdir), "plot.pdf")
        self.sequencer.temperatures = [100, 200]
        self.sequencer.bulk_modulus_initial_guess = 37
        self.sequencer.relaxed_lattice_parameter = 9.210587579
        self.sequencer.asr = "crystal"
        self.sequencer.deltas_volumes = THERMAL_DELTAS
        self.sequencer.q2r_input_variables = {}

    def test_starting_from_scratch(self):
        self._set_scf()
        self._set_phonons()
        self.sequencer.write()
        self._check_scf_written()
        self._check_phonons_not_written()
        self._check_q2r_not_written()
        self._check_matdyn_not_written()
        self._check_plot_not_written()
        self.assertFalse(self.sequencer.sequence_completed)

    def test_starting_from_scf(self):
        self._set_scf()
        self._set_scf_done()
        self._set_phonons()
        self.sequencer.write()
        # check 1 phonon have been written for each temperature
        self._check_phonons_written(nphonons=1)
        self._check_q2r_not_written()
        self._check_matdyn_not_written()
        self._check_plot_not_written()
        self.assertFalse(self.sequencer.sequence_completed)

    def test_starting_from_phonons(self):
        self._set_scf()
        self._set_scf_done()
        self._set_phonons()
        self._set_phonons_done()
        self._set_q2r()
        self.sequencer.write()
        self._check_q2r_written()
        self._check_matdyn_not_written()
        self._check_plot_not_written()
        self.assertFalse(self.sequencer.sequence_completed)

    def test_starting_from_q2r(self):
        self._set_scf()
        self._set_scf_done()
        self._set_phonons()
        self._set_phonons_done()
        self._set_q2r()
        self._set_q2r_done()
        self._set_matdyn()
        self.sequencer.write()
        self._check_matdyn_written()
        self._check_plot_not_written()
        self.assertFalse(self.sequencer.sequence_completed)

    def test_whole_sequence(self):
        self._set_scf()
        self._set_scf_done()
        self._set_phonons()
        self._set_phonons_done()
        self._set_q2r()
        self._set_q2r_done()
        self._set_matdyn()
        self._set_matdyn_done()
        self.sequencer.launch()
        self._check_plot_written()
        self.assertTrue(self.sequencer.sequence_completed)
        self.assertEqual(
                len(self.sequencer.extended_lattice_volumes),
                len(self.sequencer.temperatures))

    def _check_scf_written(self):
        # check that there is one scf calc for each delta
        self.assertEqual(len(os.listdir(self.scf_workdir)),
                         len(self.sequencer.deltas_volumes))

    def _check_phonons_written(self, nphonons=None):
        # check that there are NPHONONS calculations written for each delta
        root = os.path.dirname(self.phonons_workdir)
        self.assertEqual(len(os.listdir(root)),
                         len(self.sequencer.deltas_volumes))
        for voldirname in os.listdir(root):
            this_vol_root = os.path.join(root, voldirname)
            self.assertEqual(
                    len(os.listdir(this_vol_root)),
                    nphonons if nphonons is not None else self._nphonons)

    def _set_scf_done(self):
        # many scf calculations to copy
        for example in self._scf_example:
            basename = os.path.basename(example)
            dest = os.path.join(self.scf_workdir, basename)
            copy_calculation(example, dest)
            with CALCTYPES_TO_INPUT_FILE_CLS["qe_pw"].from_calculation(
                    dest) as input_file:
                input_file.input_variables["pseudo_dir"] = self._scf_pseudo_dir

    def _set_phonons_done(self):
        # list all calcs to copy
        full_paths = []
        rel_paths = []
        for path in self._phonons_example:
            full_paths += [
                    os.path.join(path, x) for x in os.listdir(path)]
            rel_paths += [
                    os.path.join(
                        os.path.basename(path), x) for x in os.listdir(path)]
        for path, rel_path in zip(full_paths, rel_paths):
            dest = os.path.join(os.path.dirname(self.phonons_workdir),
                                rel_path)
            scf_ext = os.path.dirname(rel_path).split("_")[-1]
            copy_calculation(
                    path, dest,
                    new_parents=[self.sequencer.scf_workdir + "_" + scf_ext])

    def _check_matdyn_written(self):
        self.assertEqual(
                len(os.listdir(os.path.dirname(self.matdyn_workdir))),
                len(self.sequencer.deltas_volumes))

    def _check_matdyn_not_written(self):
        self.assertFalse(os.path.exists(os.path.dirname(self.matdyn_workdir)))

    def _check_q2r_written(self):
        self.assertEqual(
                len(os.listdir(os.path.dirname(self.q2r_workdir))),
                len(self.sequencer.deltas_volumes))

    def _check_q2r_not_written(self):
        self.assertFalse(os.path.exists(os.path.dirname(self.q2r_workdir)))

    def _check_plot_written(self):
        # check that there are 2 plots created
        root = os.path.dirname(self.sequencer.plot_save)
        all_files = os.listdir(root)
        print(root)
        print(all_files)
        self.assertEqual(len([x for x in all_files if x.endswith(".pdf")]), 3)

    def _check_plot_not_written(self):
        # there is a few plots so check that there are no files ending with pdf
        root = os.path.dirname(self.sequencer.plot_save)
        if not os.path.exists(root):
            return
        all_files = os.listdir(root)
        self.assertFalse(any([x.endswith(".pdf") for x in all_files]))

    def _set_matdyn(self):
        pass

    def _set_matdyn_done(self):
        # many matdyn calculations to copy
        for example in THERMAL_EXPANSION_MATDYN_EXAMPLES:
            basename = os.path.basename(example)
            dest = os.path.join(os.path.dirname(self.matdyn_workdir), basename)
            copy_calculation(example, dest)
            # need to relink q2r dependencies
            ext = "_" + basename.split("_")[-1]
            with MetaDataFile.from_calculation(dest) as meta:
                meta.parents = [self.q2r_workdir + ext]
            with CALCTYPES_TO_INPUT_FILE_CLS["qe_matdyn"].from_calculation(
                    dest) as input_file:
                newout = meta.output_data_dir

                newout = meta.output_data_dir
                newin = meta.input_data_dir
                # paths to output data dir
                for varname in ("flfrq", "flvec", "fleig"):
                    oldname = os.path.basename(
                         input_file.input_variables[varname].value)
                    input_file.input_variables[varname] = os.path.join(
                            newout, oldname)
                # paths to input data dir
                for varname in ("flfrc", ):
                    oldname = os.path.basename(
                         input_file.input_variables[varname].value)
                    input_file.input_variables[varname] = os.path.join(
                            newin, oldname)

    def _set_q2r(self):
        self.sequencer.q2r_input_variables = {}

    def _set_q2r_done(self):
        # many q2r calculations to copy
        for example in THERMAL_EXPANSION_Q2R_EXAMPLES:
            basename = os.path.basename(example)
            dest = os.path.join(os.path.dirname(self.q2r_workdir), basename)
            copy_calculation(example, dest)
            # need to relink phonons dependencies
            ext = "_" + basename.split("_")[-1]
            ph_parent_root = self.phonons_workdir + ext
            newparents = [os.path.join(ph_parent_root, x)
                          for x in os.listdir(ph_parent_root)]
            with MetaDataFile.from_calculation(dest) as meta:
                meta.parents = newparents
            with CALCTYPES_TO_INPUT_FILE_CLS["qe_q2r"].from_calculation(
                    dest) as input_file:
                newout = meta.output_data_dir
                for varname in ("fildyn", "flfrc"):
                    fname = os.path.basename(
                            input_file.input_variables[varname].value)
                    input_file.input_variables[varname] = os.path.join(
                            newout, fname)

    def tearDown(self):
        BaseQEPhononSequencerTest.tearDown(self)
