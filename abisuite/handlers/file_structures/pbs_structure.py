from .bases import PathConvertibleBaseStructure, BaseStructureWithMPISupport
from ... import __IMPLEMENTED_QUEUING_SYSTEMS__


class PBSStructure(PathConvertibleBaseStructure, BaseStructureWithMPISupport):
    """Data container for a PBS file.
    """
    all_attributes = (
        "command", "command_arguments", "command_line", "constraint",
        "cpus_per_task",
        "input_file_path", "log_path",
        "walltime", "jobname", "nodes", "ppn",
        "modules_to_load", "modules_to_unload", "modules_to_swap",
        "modules_to_use",
        "lines_before", "lines_after", "project_account", "project_code",
        "quality_of_service",
        "queue", "queuep", "queuing_system",
        "memory", "memory_per_cpu", "mpi_command", "mpi_command_arguments",
        "ntasks", "stderr_path", "total_ncpus",
        )
    allowed_attributes = {"queuing_system": __IMPLEMENTED_QUEUING_SYSTEMS__}
    convertible_path_attributes = (
            "input_file_path", "stderr_path", "log_path"
            )
    dict_attributes = ("command_arguments", "mpi_command_arguments")
    list_attributes = (
        "lines_before", "lines_after", "modules_to_load", "modules_to_unload",
        "modules_to_use", "modules_to_swap",
        )
    optional_attributes = (
        "constraint", "cpus_per_task", "memory", "memory_per_cpu",
        "project_account",
        "project_code",
        "quality_of_service",
        "queue", "queuep", "ppn", "nodes", "ntasks",
        )
    relative_to_path_attribute = "path"

    def __init__(self, *args, **kwargs):
        # execute both init methods
        PathConvertibleBaseStructure.__init__(self, *args, **kwargs)
        BaseStructureWithMPISupport.__init__(self, *args, **kwargs)
        # default
        self._command_line = "$MPIRUN $EXECUTABLE < $INPUT > $LOG 2> $STDERR"

    @property
    def ntasks(self):
        return self._ntasks

    @ntasks.setter
    def ntasks(self, ntasks):
        self._ntasks = ntasks
        if ntasks is None:
            return
        # also automatically nodes = ppn = None
        self.nodes, self.ppn = None, None

    @property
    def ntasks_per_node(self):
        return self.ppn

    @ntasks_per_node.setter
    def ntasks_per_node(self, ntasks_per_node):
        self.ppn = ntasks_per_node

    # alias
    @property
    def qos(self):
        return self.quality_of_service

    @qos.setter
    def qos(self, qos):
        self.quality_of_service = qos

    def get_relevant_properties(self):
        # return list of properties needed depending of queuing system
        # command arguments are retrieved directly from corresp. cmd
        relevant = ["command", "command_line", "input_file_path",
                    "lines_after", "lines_before",
                    "log_path", "modules_to_load", "modules_to_use",
                    "modules_to_unload", "modules_to_swap", "mpi_command",
                    "queuing_system", "stderr_path",
                    ]
        if self.queuing_system != "local":
            relevant += ["jobname", "walltime"]
        if self.queuing_system in ("torque", "slurm", ):
            relevant += ["ppn", "nodes", "total_ncpus"]
        if self.queuing_system == "grid_engine":
            relevant += ["nodes", "ppn", "total_ncpus"]
        if self.queuing_system == "pbs_professional":
            relevant += ["project_code", "nodes"]
        if self.queuing_system == "slurm":
            relevant += ["quality_of_service", "project_account", "memory",
                         "memory_per_cpu", "ntasks", "cpus_per_task"]
        if self.queuing_system in ("grid_engine", "slurm"):
            relevant += ["queue"]
        if self.queuing_system in ("slurm"):
            relevant += ["queuep", "constraint"]
        return relevant
