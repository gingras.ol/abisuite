import json

from .bases import BaseStreamLineWriter
from ..file_structures import MetaDataStructure
from ...utils import AbisuiteJSONEncoder


class MetaDataWriter(BaseStreamLineWriter):
    """Writer class for a Meta data file.
    """
    _loggername = "MetaDataWriter"
    _structure_class = MetaDataStructure

    def _do_write(self):
        # create data dict from structure
        data = {}
        for attr_name in self._structure_class.all_attributes:
            attr = getattr(self.structure, attr_name)
            data[attr_name] = attr
        with open(self.path, "w") as f:
            json.dump(data, f, indent=4, cls=AbisuiteJSONEncoder)
            # indent = 4 to make it human readable
