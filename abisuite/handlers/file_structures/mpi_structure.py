from .bases import BaseStructureWithMPISupport
from ... import __IMPLEMENTED_QUEUING_SYSTEMS__


class MPIStructure(BaseStructureWithMPISupport):
    """File structure for a file needing mpi parameters.
    """
    all_attributes = (
            "command", "command_arguments", "cpus_per_task", "mpi_command",
            "mpi_command_arguments", "nodes", "ntasks", "ppn",
            "queuing_system", "total_ncpus",
            )
    allowed_attributes = {"queuing_system": __IMPLEMENTED_QUEUING_SYSTEMS__}
    optional_attributes = ("nodes", "ntasks", "ppn", "cpus_per_task", )
    dict_attributes = ("command_arguments", "mpi_command_arguments", )

    def get_relevant_properties(self):
        if self.queuing_system == "local":
            return ["queuing_system", "command", "mpi_command"]
        # else return all properties
        return self.all_attributes
