class DevError(Exception):
    pass


class DirectoryExistsError(FileExistsError):
    pass


class IsAFileError(FileExistsError):
    pass


class NotACalculationDirectoryError(FileNotFoundError):
    """Error raised when we try to access a directory which is thought to
    be a calculation directory but it is not.
    """
    pass


class CalculationNotFoundError(NotACalculationDirectoryError):
    """Alias of the base exception with a different name.
    """
    pass


class LengthError(ValueError):
    """Error used to denotate that a list-like object doesn't have a good length.
    """
    pass
