from ..bases import BaseInputFileHandler
from ...file_approvers import QEQ2RInputApprover
from ...file_parsers import QEQ2RInputParser
from ...file_structures import QEQ2RInputStructure
from ...file_writers import QEQ2RInputWriter


class QEQ2RInputFile(BaseInputFileHandler):
    """File handler for a Quantum Espresso q2r.x input file.
    """
    _approver_class = QEQ2RInputApprover
    _loggername = "QEQ2RInputFile"
    _parser_class = QEQ2RInputParser
    _structure_class = QEQ2RInputStructure
    _writer_class = QEQ2RInputWriter
