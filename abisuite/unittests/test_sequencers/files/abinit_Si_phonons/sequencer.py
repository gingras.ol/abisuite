from abisuite import AbinitPhononDispersionSequencer as Sequencer


sequencer = Sequencer()
sequencer.scf_workdir = "scf_run"
sequencer.scf_input_variables = {
        "tolvrs": 1.0e-18,
        "acell": [10.61] * 3,
        "rprim": [[0.0, 0.5, 0.5], [0.5, 0.0, 0.5], [0.5, 0.5, 0.0]],
        "ntypat": 1,
        "znucl": [14],
        "natom": 2,
        "typat": [1, 1],
        "xred": [[0.0, 0.0, 0.0], [0.25, 0.25, 0.25]],
        "ecut": 5.0,
        "ngkpt": [4, 4, 4],
        "nshiftk": 4,
        "shiftk": [[0.0, 0.0, 0.5], [0.0, 0.5, 0.0],
                   [0.5, 0.0, 0.0], [0.5, 0.5, 0.5]],
        "nstep": 25,
        "diemac": 12.0
        }
sequencer.scf_pseudos = ["../pseudos/Si.psp8"]
sequencer.scf_mpi_command = "mpirun"
sequencer.scf_mpi_command_arguments = "-np 4"

# ddk
sequencer.compute_electric_field_response = True
sequencer.ddk_workdir = "ddk"
sequencer.ddk_input_variables = {
        "tolwfr": 1.0e-22,
        "nstep": 25,
        "diemac": 12.0,
        "rfdir": [1, 1, 1],
        }
sequencer.ddk_mpi_command = "mpirun -np 4"
sequencer.phonons_qpoint_grid = [4, 4, 4]
sequencer.phonons_workdir = "ph_runs/ph_run"
sequencer.phonons_input_variables = {
            "rfatpol": [1, 2],
            "tolvrs": 1.0e-8,
            "diemac": 12.0,
            "nstep": 25,
            }
sequencer.phonons_mpi_command = "mpirun -np 4"

# mrgddb
sequencer.mrgddb_workdir = "mrgddb"

# anaddb
sequencer.anaddb_workdir = "anaddb"
sequencer.qpoint_path_density = 10
sequencer.qpoint_path = [
        {r"$\Gamma$": [0.0, 0.0, 0.0]},
        {"K": [0.3750, 0.3750, 0.7500]},
        {"X": [0.5, 0.5, 1.0]},
        {r"$\Gamma$": [1.0, 1.0, 1.0]},
        {"L": [0.5, 0.5, 0.5]},
        {"X": [0.5, 0.0, 0.5]},
        {"W": [0.5, 0.25, 0.75]},
        {"L": [0.5, 0.5, 0.5]},
        ]
sequencer.anaddb_input_variables = {
        "ifcflag": 1,
        "ifcout": 0,
        "brav": 2,
        "nqshft": 1,
        "q1shft": [0.0] * 3,
        "chneut": 1,
        "dipdip": 1,
        "eivec": 4,
        "nph2l": 1,
        "qph2l": [1.0, 0.0, 0.0, 0.0],
        }

sequencer.launch()
