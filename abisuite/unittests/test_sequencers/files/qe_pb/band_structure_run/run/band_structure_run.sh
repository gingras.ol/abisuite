#!/bin/bash

MPIRUN="mpirun -np 4"
EXECUTABLE="/home/fgoudreault/Workspace/q-e/bin/pw.x -npool 4"
INPUT=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_sequencers/files/qe_pb/band_structure_run/band_structure_run.in
LOG=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_sequencers/files/qe_pb/band_structure_run/band_structure_run.log
STDERR=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_sequencers/files/qe_pb/band_structure_run/band_structure_run.stderr


$MPIRUN $EXECUTABLE -input $INPUT > $LOG 2> $STDERR
