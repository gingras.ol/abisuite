from ..bases import BaseInputApprover
from ...file_structures import QEPPInputStructure
from ....variables import ALL_QEPP_VARIABLES


class QEPPInputApprover(BaseInputApprover):
    """Class that checkes the input variables for a pp.x calculation.
    """
    _loggername = "QEPPinputApprover"
    _variables_db = ALL_QEPP_VARIABLES
    _structure_class = QEPPInputStructure
