from .bases import BaseQEPackager


class QEQ2RPackager(BaseQEPackager):
    """Packager class for a q2r.x calculation from the Quantume Espresso
    software suite.
    """
    _calctype = "qe_q2r"
    _loggername = "QEQ2RPackager"

    def keep_file(self, handler, *args, **kwargs):
        if handler.basename.endswith(".fc"):
            return True
        return super().keep_file(handler, *args, **kwargs)
