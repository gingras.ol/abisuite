from ..bases import BaseStructure


class AbinitOpticLogStructure(BaseStructure):
    """Structure class for an optic log file.
    """
    all_attributes = tuple([])

    @property
    def walltime(self):
        # walltime is not printed out in the optic log file.
        return None
