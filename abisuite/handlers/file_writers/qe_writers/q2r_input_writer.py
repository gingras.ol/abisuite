from .bases import BaseQEInputWriter
from ...file_structures import QEQ2RInputStructure


class QEQ2RInputWriter(BaseQEInputWriter):
    """Class that can write a Quantum Espresso q2r.x input file.
    """
    _loggername = "QEQ2RInputWriter"
    _structure_class = QEQ2RInputStructure

    @property
    def lines(self):
        lines = []
        self._append_var_block(lines, "input")
        return lines
