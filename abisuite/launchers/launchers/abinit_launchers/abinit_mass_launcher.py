from .abinit_launcher import AbinitLauncher
from ..bases import BaseMassLauncher
from ....routines import is_list_like


class AbinitMassLauncher(BaseMassLauncher):
    """Mass Launcher for Abinit calculations.
    """
    _loggername = "AbinitMassLauncher"
    _launcher_class = AbinitLauncher

    def __init__(self, *args, **kwargs):
        """Mass launcher input parameters.

        Parameters
        ----------
        All args are passed directly to the base class.
        """
        super().__init__(*args, **kwargs)
        self._common_pseudos = None
        self._specific_pseudos = None

    @property
    def common_pseudos(self):
        if self._common_pseudos is not None:
            return self._common_pseudos
        raise ValueError("Need to set the common pseudos for each calculation")

    @common_pseudos.setter
    def common_pseudos(self, common_pseudos):
        if isinstance(common_pseudos, str):
            # only one pseudo
            common_pseudos = (common_pseudos, )
        if not is_list_like(common_pseudos):
            raise TypeError("common pseudos should be a list of pseudos but"
                            f" received: {common_pseudos}.")
        self._common_pseudos = common_pseudos
        for launcher in self.launchers:
            launcher.pseudos = self.common_pseudos

    @property
    def specific_pseudos(self):
        if self._specific_pseudos is not None:
            return self._specific_pseudos
        # if none, assume there is no specific pseudos
        self._specific_pseudos = [[], ] * self.n_jobs
        return self.specific_pseudos

    @specific_pseudos.setter
    def specific_pseudos(self, specific_pseudos):
        if not is_list_like(specific_pseudos):
            raise TypeError(f"specific pseudos must be a list be received:"
                            f" {specific_pseudos} instead.")
        if len(specific_pseudos) != self.n_jobs:
            raise ValueError("Specific pseudos should be the same len as"
                             f" number of jobs ({self.n_jobs}).")
        self._specific_pseudos = specific_pseudos
        for launcher, spec_ps in zip(self.launchers, self.specific_pseudos):
            spec_ps = list(spec_ps)
            launcher.pseudos += spec_ps
