from abisuite import QERelaxationSequencer as Sequencer


sequencer = Sequencer()

# set scf parameters
sequencer.relax_atoms = True
sequencer.relax_cell = True
sequencer.scf_input_variables = {
    "ibrav": 2,
    "celldm(1)": 10.3472,
    "ntyp": 1,
    "nat": 2,
    "ecutwfc": 35.0,
    "conv_thr": 1e-8,
    "pseudo_dir": "../pseudos/",
    "atomic_species": [{"atom": "Si",
                        "pseudo": "Si.pbe-mt_fhi.UPF",
                        "atomic_mass": 28.085, }],
    "atomic_positions": {
        "parameter": "crystal",
        "positions": {"Si": [[0.0, 0.0, 0.0], [0.25, 0.25, 0.25]]}},
    "k_points": {
        "parameter": "automatic",
        "k_points": [4, 4, 4, 0, 0, 0],
        },
    "press_conv_thr": 0.001,
    "forc_conv_thr": 1e-5,
    "etot_conv_thr": 1e-6,
    }
sequencer.scf_workdir = "relax_run"
sequencer.scf_command_arguments = "-npool 4"
sequencer.scf_mpi_command = "mpirun -np 4"
sequencer.maximum_relaxations = 3

sequencer.launch()
