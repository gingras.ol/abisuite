from ..bases import BaseInputStructure
from ....variables.qe_variables import ALL_QELD1_VARIABLES


class QELD1InputStructure(BaseInputStructure):
    """Structure for a Quantum Espresso ld1.x input file.
    """
    _variables_db = ALL_QELD1_VARIABLES
