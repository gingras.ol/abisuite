import unittest

from .bases import BaseApproverTest
from ..variables_for_tests import BASIC_MPI_VARIABLES
from ...handlers.file_approvers import MPIApprover
from ...handlers.file_approvers.exceptions import MPIError


class TestMPIApprover(BaseApproverTest, unittest.TestCase):
    _approver_class = MPIApprover
    _exception = MPIError

    def setUp(self):
        super().setUp()
        self.approver.mpi_command = BASIC_MPI_VARIABLES["mpi_command"]
        self.approver.ppn = BASIC_MPI_VARIABLES["ppn"]
        self.approver.nodes = BASIC_MPI_VARIABLES["nodes"]
        self.approver.queuing_system = "torque"

    def test_nothing_happens_if_no_mpi_command(self):
        approver = MPIApprover()
        approver.validate()
        self.assertTrue(approver.is_valid)

    def test_nothing_happens_if_no_npernode_specified(self):
        self.approver.mpi_command = "mpirun"
        self.approver.validate()
        self.assertTrue(self.approver.is_valid)

        # test with -np instead of -npernode
        self.approver.mpi_command = "mpirun -np 4"
        self.approver.validate()
        self.assertTrue(self.approver.is_valid)
        # test that an empty command returns None as npernodes
        self.assertIs(self.approver.extract_npernode(None), None)

    def test_nodes(self):
        # test that an integer works instead of a str
        self.approver.nodes = 1
        self.approver.mpi_command = "mpirun"
        self.approver.validate()
        self.assertTrue(self.approver.is_valid, msg=self.approver.errors)
        # test that a nodes string works too
        self.approver.nodes = "1"
        self.approver.validate()
        self.assertTrue(self.approver.is_valid)

    def test_ask_too_much_npernode_for_ppn(self):
        self.approver.mpi_command = f"mpirun -npernode {self.approver.ppn + 1}"
        self.approver.validate()
        self.assertFalse(self.approver.is_valid)
        self.assertEqual(len(self.approver.errors), 1)
        with self.assertRaises(MPIError):
            self.approver.raise_errors()
