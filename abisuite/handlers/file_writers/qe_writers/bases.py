import abc

from ..bases import BaseInputWriter


class BaseQEInputWriter(BaseInputWriter, abc.ABC):
    """Base class for Quantum Espresso input writers.
    """

    def _append_var_block(self, lines, varblock):
        lines.append(f"&{varblock}\n")  # section title
        for variable in self.input_variables.values():
            if variable.block == varblock:
                lines.append(f" {str(variable)}")
        lines.append("/\n")

    def _append_others(self, lines, input_variables=None):
        if input_variables is None:
            input_variables = self.input_variables
        for variable in input_variables.values():
            if variable.block is None:
                lines.append(str(variable))
