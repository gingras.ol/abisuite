from ..bases import BaseStructure


class QEEPWSpecFunPhonStructure(BaseStructure):
    """Structure class for a phonon spectral function file produced by the
    epw.x executable from Quantum Espresso.
    """
    all_attributes = ("energies", "spectral_function", )
