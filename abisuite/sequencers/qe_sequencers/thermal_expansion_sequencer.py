import os

import matplotlib.pyplot as plt
import numpy as np
from scipy.optimize import curve_fit

from .individual_phonon_sequencers import QEPhononComparatorSequencer
from ..bases import SequenceCalculation
from ...constants import (
        CM_TO_EV, KELVIN_TO_EV, GPA_TO_EV_per_BOHR3, RYDBERG_TO_EV,
        )
from ...exceptions import LengthError
from ...handlers import (
        QEPWInputFile, QEPWLogFile, QEMatdynFreqFile, CalculationDirectory,
        )
from ...plotters import Plot
from ...routines import is_list_like, is_vector
from ...status_checkers.exceptions import CalculationFailedError


def birch_murnaghan_eq_of_state(V, E0, V0, B0, B0prime):
    """https://en.wikipedia.org/wiki/Birch%E2%80%93Murnaghan_equation_of_state

    Birch-Murnaghan equation of state to fit.
    """
    quotient = np.divide(V0, V)
    power = np.power(quotient, 2/3)
    terme1 = power - 1
    terme2 = np.power(terme1, 3) * B0prime
    terme3 = np.power(terme1, 2)
    terme4 = 6 - 4 * power
    return E0 + 9 * V0 * B0 / 16 * (terme2 + terme3 * terme4)


def free_energy(frequencies, ground_state_energy, temperature):
    """Free energy computed from phonon frequencies and GS energy.

    All parameters have units of energy and should have the same units.
    Gamma frequencies are ignored (set to 0).

    Parameters
    ----------
    frequencies: array
        (nqpts x nbranches) array containing all frequencies for a given
        system.
    """
    F0 = 0.5 * np.sum(frequencies)
    exp = np.exp(-frequencies / temperature)
    # we ignore gamma frequencies because they make the log diverge
    gamma_indices = []
    for ifreq, freqs in enumerate(frequencies):
        if freqs[0] == 0.0 and freqs[1] == 0.0 and freqs[2] == 0.0:
            gamma_indices.append(ifreq)
    exp[gamma_indices] = 0.0
    if (exp > 1.0).any():
        # debug which indices and which branch is at fault.
        where = np.where(exp > 1.0)
        raise ValueError(
                "Large negative frequencies at indices at "
                f"[qpt index, branch index]: {where}. The freqs are: "
                f"{frequencies[where]}.")
    log = np.log(1 - exp)
    f_thermal = temperature * np.sum(log) / len(frequencies)
    return F0 + f_thermal + ground_state_energy


# TODO: Rebase this class with a future PhononDispersionComparator sequencer.
# TODO: implement non-cubic system
class QEThermalExpansionSequencer(QEPhononComparatorSequencer):
    """Thermal expansion sequencer for Quantum Espresso.

    Here we use a PhononConvergenceSequencer base class because it's almost
    the same thing as converging a SCF parameter onto phonon frequencies,
    as a starting point.

    For now, only one parameter is used to control volume and a fit with the
    Birch-Murnaghan equation of state is used.
    THUS: IT ONLY WORKS FOR CUBIC SYSTEM AS OF NOW

    deltas_volumes are the variation (in %) of the cell volume computed
    either from celldm(1) given in the scf_input_variables or in the
    celldm(1) written in the calculation stated in scf_load_geometry_from
    """
    _all_sequencer_prefixes = ("scf_", "phonons_", "q2r_", "matdyn_", "plot_")
    _loggername = "QEThermalExpansionSequencer"

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._deltas_volumes = None
        self._q2r_input_variables = None
        self._temperatures = None
        self._asr = None
        self._bulk_modulus_initial_guess = None
        self._relaxed_lattice_parameter = None
        # the following parameter is internally set when making the plots
        self._extended_lattice_volumes = []
        self._phonons_qpoint_grid = None

    @property
    def asr(self):
        if self._asr is None:
            raise ValueError("Need to set 'asr'.")
        return self._asr

    @asr.setter
    def asr(self, asr):
        self._asr = asr

    @property
    def bulk_modulus_initial_guess(self):
        """The bulk modulus value of the material in GPa. Used to help fit the
        Birch-Murnaghan Equation of state to get the lattice expansion.
        """
        if self._bulk_modulus_initial_guess is None:
            raise ValueError("Need to set 'bulk_modulus_initial_guess'.")
        return self._bulk_modulus_initial_guess

    @bulk_modulus_initial_guess.setter
    def bulk_modulus_initial_guess(self, b):
        self._bulk_modulus_initial_guess = b

    @property
    def extended_lattice_volumes(self):
        if not self.sequence_completed:
            raise RuntimeError("Cannot compute 'extended_lattice_volume' "
                               "if sequence is not complete.")
        return self._extended_lattice_volumes

    @property
    def deltas_volumes(self):
        """List containing the percentage of change of volumes to compute.

        For instance, if we want to compute volumes with 0%, +/- 1% and +/- 2%
        we need to set deltas_volumes to [-2, -1, 0, 1, 2].
        """
        if self._deltas_volumes is None:
            raise ValueError("Need to set 'deltas_volumes'.")
        return self._deltas_volumes

    @deltas_volumes.setter
    def deltas_volumes(self, deltas):
        if not is_list_like(deltas):
            raise TypeError(
                    f"Expected list-like object but got '{deltas}'.")
        if len(deltas) <= 1:
            raise LengthError("Deltas len should be > than 1.")
        self._deltas_volumes = deltas

    @property
    def n_scf_calculations(self):
        # this parameter determines the number of calculations
        return len(self.deltas_volumes)

    @property
    def phonons_input_variables(self):
        return QEPhononComparatorSequencer.phonons_input_variables.fget(self)

    @phonons_input_variables.setter
    def phonons_input_variables(self, invars):
        QEPhononComparatorSequencer.phonons_input_variables.fset(self, invars)
        if not isinstance(invars, dict):
            raise TypeError("'phonons_input_variabels' must be a dict.")
        self._set_input_var_to(
                self.phonons_input_variables, "ldisp", True)
        for inq, nq in enumerate(self.phonons_qpoint_grid):
            self._set_input_var_to(
                    self.phonons_input_variables, f"nq{inq + 1}", nq)

    @property
    def phonons_qpoint_grid(self):
        if self._phonons_qpoint_grid is None:
            raise ValueError("Need to set 'phonons_qpoint_grid'.")
        return self._phonons_qpoint_grid

    @phonons_qpoint_grid.setter
    def phonons_qpoint_grid(self, grid):
        if not is_vector(grid, length=3):
            raise TypeError(
                    "'phonons_qpoint_grid' must be a list of length 3.")
        self._phonons_qpoint_grid = grid

    @property
    def q2r_input_variables(self):
        return self._q2r_input_variables

    @q2r_input_variables.setter
    def q2r_input_variables(self, ins):
        self._q2r_input_variables = ins

    @property
    def temperatures(self):
        """Temperatures to compute the lattice expansion.
        In Kelvin.
        """
        if self._temperatures is None:
            raise ValueError("Need to set 'temperatures'.")
        return self._temperatures

    @temperatures.setter
    def temperatures(self, temperatures):
        if not is_list_like(temperatures):
            raise TypeError(
                    f"Expected list-like object but got '{temperatures}'.")
        self._temperatures = temperatures

    @property
    def relaxed_lattice_parameter(self):
        if self._relaxed_lattice_parameter is None:
            raise ValueError("Need to set 'relaxed_lattice_parameter'.")
        return self._relaxed_lattice_parameter

    @relaxed_lattice_parameter.setter
    def relaxed_lattice_parameter(self, param):
        if not isinstance(param, float):
            # possibly a Custom variable class.
            param = param.value
        self._relaxed_lattice_parameter = param

    @property
    def scf_specific_input_variables(self):
        """Specific input variables for each scf calculations. The new
        celldm(1) is computed from the deltas_volumes property.
        """
        if self._scf_specific_input_variables is not None:
            # already computed
            return self._scf_specific_input_variables
        # first get the reference celldm(1)
        if self.scf_load_geometry_from is not None:
            with QEPWInputFile.from_calculation(
                    self.scf_load_geometry_from,
                    loglevel=self._loglevel) as inp:
                celldm = inp.input_variables["celldm(1)"]
        else:
            # get reference from input variables
            celldm = self.scf_input_variables["celldm(1)"]
        V0 = celldm ** 3  # cubic system only for now
        specifics = []
        for delta in self.deltas_volumes:
            if abs(delta) >= 100:
                # make sure we have decent percentages
                raise ValueError("Cannot exceed 100% volume expansion!")
            if delta == 0 or delta == 0.0:
                celldm_prime = celldm
            else:
                Vprime = V0 * (1 + delta / 100)
                celldm_prime = Vprime ** (1/3)
            specifics.append({"celldm(1)": celldm_prime})
        self._scf_specific_input_variables = specifics
        return specifics

    # TODO: When rebasing with a future phonon dispersion comparator sequencer
    # this property will dry out.
    @property
    def sequence_completed(self):
        # override this property since we add calculations after some of them
        # are finished (like the QERelaxationSequencer).
        # check if a matdyn calculation is present. if yes, return super value
        if not self.sequence:
            # FIXME: code not dry with BaseSequencer class...
            try:
                self.init_sequence()
            except (TypeError, ValueError, KeyError):
                # some variables are not set yet
                return False
            except AttributeError as err:
                self._logger.exception(
                        f"An AttributeError occured within sequencer '{self}'."
                        )
                raise err
        try:
            calcs = self.get_sequence_calculation("matdyn_")
            if not is_list_like(calcs):
                # only one matdyn calc so far
                return False
            if len(calcs) != len(self.deltas_volumes):
                # not all matdyn calcs are written
                return False
        except LookupError:
            # matdyn not present in sequence
            return False
        return QEPhononComparatorSequencer.sequence_completed.fget(self)

    def init_scf_sequence(self):
        super().init_scf_sequence()
        # for each scf sequence calculation, keep the geometry variables
        # if we load geometry
        # i.e. keep the specifics since we expand the geometry variables
        for scfcalc, specifics in zip(
                self.sequence, self.scf_specific_input_variables):
            scfcalc.keep_variables_after_loading_geometry = specifics

    def init_sequence(self):
        super().init_sequence()
        # base class init sequence will launch scf and phonons
        # now we need to launch q2r and matdyn in order to incorporate acoustic
        # sum rules for phonon frequencies
        # only continue if nphonons have been set
        if self._nphonons is None:
            return
        self.init_q2r_and_matdyn_sequence()

    def init_q2r_and_matdyn_sequence(self):
        # iterate over the whole sequence (from here includes the scf and ph)
        # for each scf calcs, make a q2r and matdyn calc
        q2r_calcs = []
        matdyn_calcs = []
        for calc in self.sequence:
            if calc.calctype != "qe_pw":
                # not a scf calc
                continue
            # create q2r calculation
            q2r_calc = SequenceCalculation(
                    "qe_q2r",
                    self._get_q2r_workdir_single_scf(calc),
                    self.q2r_input_variables,
                    self.q2r_calculation_parameters,
                    loglevel=self._loglevel,
                    )
            # dependencies are all the phonon calculations for this scf calc
            for calc2 in self.sequence:
                if calc in calc2.dependencies:
                    q2r_calc.dependencies.append(calc2)
            q2r_calcs.append(q2r_calc)
            # create matdyn sequence at the same time
            matdyn_calc = SequenceCalculation(
                    "qe_matdyn",
                    self._get_matdyn_workdir_single_scf(calc),
                    self._get_matdyn_input_variables(q2r_calc),
                    self.matdyn_calculation_parameters,
                    loglevel=self._loglevel)
            matdyn_calc.dependencies.append(q2r_calc)
            matdyn_calcs.append(matdyn_calc)
        self.sequence += q2r_calcs + matdyn_calcs

    def plot_comparision(self):
        """Post process data from phonon calculations to get thermal expansion
        curves.
        """
        # make a separate plot for gs energy to check if we are near the
        # true energy minimum
        err = False
        for name, func in zip(
                ("GS energy curve", "Free energy curves",
                 "Lattice expansion curve"),
                (self.plot_gs_energy_curve,
                 self.plot_free_energy_curves,
                 self.plot_lattice_expansion)):
            try:
                func()
            except Exception as e:
                err = True
                self._logger.error(
                        f"Something happened while computing {name}.")
                self._logger.exception(e)
        if err:
            raise LookupError("Could not make post process plots.")

    def plot_gs_energy_curve(self):
        """Plots the gs energy curve.
        """
        gs_plot = Plot()
        energies, frequencies, volumes = self._collect_data()
        gs_plot.add_curve(volumes, energies, linewidth=2, linestyle="-",
                          color="k", marker="s", markerfacecolor="k",
                          markersize=5)
        gs_plot.xlims = [0.99 * min(volumes), 1.01 * max(volumes)]
        gs_plot.add_vline(
                self.relaxed_lattice_parameter ** 3,
                linestyle="--", linewidth=2)
        gs_plot.xlabel = r"Volume [bohr$^3$]"
        gs_plot.ylabel = r"$E^{GS}$"
        gs_plot.title = "Ground State Energy vs volume"
        self._post_process_plot(gs_plot, name_extension="_gs_energy")

    def plot_lattice_expansion(self):
        """Plots the lattice expansion in function of temperature.
        """
        # plot using full fit to check convergence
        assert len(self._extended_lattice_volumes) == len(self.temperatures)
        plot = Plot()
        ndata = len(self.deltas_volumes)
        plot.add_curve(
                self.temperatures, np.power(
                    self._extended_lattice_volumes, 1/3),
                marker="s", markerfacecolor="k", markersize=5, color="k",
                linewidth=2, linestyle="-", label=f"Full ({ndata})")
        # if number of volumes >= 8, plot using half of values
        # need more than 8 cause they are 4 parameters to fit (need >=4 vals)
        if len(self.deltas_volumes) >= 8:
            range_ = len(self.deltas_volumes) // 4
            ndata = len(self.deltas_volumes) - 2 * range_
            try:
                ext_vols = self._get_extended_volumes(
                        parameter_range=range_)
                plot.add_curve(
                        self.temperatures, np.power(ext_vols, 1/3), marker="s",
                        markerfacecolor="b", markersize=5, color="b",
                        linewidth=2,
                        label=f"Half ({ndata})")
            except RuntimeError:
                # could not find optimal parameters
                # that's not big deal
                self._logger.error(
                        "Could not make the half-range curve for "
                        "lattice expansion plot.")
        plot.xlabel = "Temperature [K]"
        plot.ylabel = r"Lattice parameter [bohr]"
        plot.title = "Lattice parameter thermal expansion"
        plot.add_hline(
                self.relaxed_lattice_parameter, linestyle="--", linewidth=2)
        self._post_process_plot(plot)

    def plot_free_energy_curves(self):
        """Make the free energy curves and plot them for each temperatures.
        """
        free_energy_plot = Plot()
        n_temps = len(self.temperatures)
        # make a rainbow of colors for each temperature curves
        cmap = plt.get_cmap("gist_rainbow", n_temps)
        colors = [cmap(x / (n_temps - 1)) for x in range(0, n_temps)]
        # we need to collect volumes, phonon frequencies and gs energies
        # for each delta volumes
        energies, frequencies, volumes = self._collect_data()
        self._extended_lattice_volumes = []
        for temperature, color in zip(self.temperatures, colors):
            # for each tarmperature, make a free energy array vs volume
            free_energy_vs_volume = self._get_free_energy_vs_volume(
                    temperature * KELVIN_TO_EV, frequencies, energies, volumes)
            free_energy_plot.add_curve(
                    volumes, free_energy_vs_volume, color=color,
                    label=f"T={temperature}K",
                    marker="s", markersize=5, markerfacecolor=color,
                    linewidth=2)
            # plot birch murnaghan fit as well
            # initial guess on V0
            try:
                fit_parameters = self._get_fit_parameters(
                        volumes, free_energy_vs_volume, energies)
                finer_volumes = np.linspace(
                        0.95 * min(volumes), 1.05 * max(volumes), 100)
                free_energy_plot.add_curve(
                        finer_volumes,
                        birch_murnaghan_eq_of_state(
                            finer_volumes, *fit_parameters),
                        color=color, linestyle="--", linewidth=2)
                self._extended_lattice_volumes.append(fit_parameters[1])
            except RuntimeError:
                # something happened durig the fit...
                # don't add the curve fit
                self._extended_lattice_volumes.append(0)
                self._logger.error(
                        "Could not fit free energy to Birch-Murghanan model "
                        f"for T={temperature}K")
        free_energy_plot.ylabel = "Free Energy [eV]"
        free_energy_plot.xlabel = "Volume [bohr" + r"$^3$]"
        free_energy_plot.title = "Free energy vs volume"
        free_energy_plot.xlims = [0.99 * min(volumes), 1.01 * max(volumes)]
        free_energy_plot.add_vline(
                self.relaxed_lattice_parameter ** 3,
                linestyle="--", linewidth=2)
        self._post_process_plot(
                free_energy_plot, name_extension="_free_energy")

    def _collect_data(self):
        """Collects GS energies, phonon frequencies for all volumes computed.
        """
        gs_energies = []
        frequencies = []
        volumes = []
        for idelta, delta in enumerate(self.deltas_volumes):
            # get volume for this delta
            scf_workdir = self._get_scf_workdir_single_calculation(idelta)
            with QEPWLogFile.from_calculation(
                    scf_workdir,
                    loglevel=self._loglevel) as log:
                gs_energies.append(log.total_energy * RYDBERG_TO_EV)
            with QEPWInputFile.from_calculation(
                    scf_workdir,
                    loglevel=self._loglevel) as input_file:
                volume = input_file.input_variables["celldm(1)"].value ** 3
                volumes.append(volume)
            with QEMatdynFreqFile.from_calculation(
                    self._get_matdyn_workdir_single_scf(scf_workdir),
                    loglevel=self._loglevel) as freq_file:
                frequencies.append(freq_file.eigenvalues)
            if (np.array(freq_file.eigenvalues) < 0.0).any():
                self._logger.warning(
                        f"Phonon frequencies < 0.0 for delta = {delta}%")
                for iqpt, freqs_qpt in enumerate(freq_file.eigenvalues):
                    for ibranch, freq in enumerate(freqs_qpt):
                        if freq < 0.0:
                            self._logger.warning(
                                    f"w = {freq} < 0.0 at qpt = {iqpt + 1}, "
                                    f"branch = {ibranch}")
        # order data
        freqs = np.array(frequencies) * CM_TO_EV
        energies = np.array(gs_energies)
        volumes = np.array(volumes)
        order = np.argsort(volumes)
        return energies[order], freqs[order], volumes[order]

    def _get_extended_volumes(self, **kwargs):
        """Returns the list of expanded volumes.
        """
        ext_vols = []
        energies, frequencies, volumes = self._collect_data()
        for temperature in self.temperatures:
            # for each tarmperature, make a free energy array vs volume
            free_energy_vs_volume = self._get_free_energy_vs_volume(
                    temperature * KELVIN_TO_EV, frequencies, energies, volumes)
            # plot birch murnaghan fit as well
            # initial guess on V0
            fit_parameters = self._get_fit_parameters(
                    volumes, free_energy_vs_volume, energies,
                    **kwargs)
            ext_vols.append(fit_parameters[1])
        return np.array(ext_vols)

    def _get_fit_parameters(
            self, volumes, free_energy_vs_volume, gs_energies,
            parameter_range=None):
        """For a given temperature, fit the equation of state and return the
        git parameters.

        If 'parameter_range' is not None, specify a range for the fit
        i.e.: how many volumes
        are inclued in the fit. useful for convergence studies.
        """
        V0_index = int(len(volumes) // 2)  # middle (assume ordered data)
        V0_guess = volumes[V0_index]
        E0_guess = gs_energies[V0_index]
        if parameter_range is not None:
            # check range is good
            assert isinstance(parameter_range, int)
            assert parameter_range < V0_index and parameter_range > 0
            volumes = volumes[
                    V0_index - parameter_range:V0_index + parameter_range + 1]
            free_energy_vs_volume = free_energy_vs_volume[
                    V0_index - parameter_range:V0_index + parameter_range + 1]
            gs_energies = gs_energies[
                    V0_index - parameter_range:V0_index + parameter_range + 1]
        params, pcov = curve_fit(
                birch_murnaghan_eq_of_state,
                volumes,
                free_energy_vs_volume,
                p0=[E0_guess, V0_guess,
                    self.bulk_modulus_initial_guess * GPA_TO_EV_per_BOHR3,
                    4])  # B0 prime initial guess
        return params

    def _get_free_energy_vs_volume(
            self, temperature, ph_freqs, gs_energies, volumes):
        """Returns the free energy vs volume curve for a given temperature.
        """
        free_energy_vs_volume = []
        # order data first
        order = np.argsort(volumes)
        volumes = volumes[order]
        ph_freqs = ph_freqs[order]
        gs_energies = gs_energies[order]
        for ph_freq, gs_energy in zip(ph_freqs, gs_energies):
            free_energy_vs_volume.append(
                    free_energy(ph_freq, gs_energy,
                                temperature))
        return np.array(free_energy_vs_volume)

    def _get_matdyn_input_variables(self, q2r_calc):
        """From the q2r calculation, determine the matdyn input variables.
        """
        inp = {"asr": self.asr,
               "q_in_cryst_coord": True,
               "q_in_band_form": False,
               }
        # need to get the qpts. from here, just iterate over dependencies
        # of the q2r calc, extract the qpts coordinates and append them
        # to the input variables
        qpts = []
        for ph_calc in q2r_calc.dependencies:
            if not CalculationDirectory.is_calculation_directory(
                    ph_calc.workdir):
                # calculation does not exist yet
                return inp
            with CalculationDirectory.from_calculation(
                    ph_calc.workdir, loglevel=self._loglevel) as calc:
                calc.connect_to_database()
                if calc.status["calculation_finished"] is False:
                    # calculation not finished, cannot compute input vars
                    return {}
                elif calc.status["calculation_finished"] == "error":
                    raise CalculationFailedError(calc)
                with calc.log_file as log:
                    # use index 0 as there should be only 1 qpt per Ph calc
                    # round to 10 decimals to avoid numerical rounding errors
                    # 10 decimals should be enough
                    qpts.append(
                            [round(x, 10)
                             for x in log.phonon_frequencies[0]["q_point"]])
        inp["q_points"] = {
                "qpts": [[round(q, 10) for q in qpt] for qpt in qpts],
                "nqpts": [1] * len(qpts)}
        return inp

    def _get_matdyn_workdir_single_scf(self, scf_calc):
        if isinstance(scf_calc, SequenceCalculation):
            workdir = scf_calc.workdir
        else:
            workdir = scf_calc
        scf_ext = "_" + workdir.split("_")[-1]
        return self.matdyn_workdir + scf_ext

    def _get_q2r_workdir_single_scf(self, scf_calc):
        scf_ext = "_" + scf_calc.workdir.split("_")[-1]
        return self.q2r_workdir + scf_ext

    def _get_phonon_workdir(self, iph, scf_calc):
        # override this to include a sub ph directory
        scf_ext = "_" + scf_calc.workdir.split("_")[-1]
        # Samething as BaseIndividualPhononSequencer
        base_workdir = os.path.join(self.phonons_workdir + scf_ext, "ph")
        if self._phonons_workdir_zfill is not None:
            return base_workdir + str(iph + 1).zfill(
                    self._phonons_workdir_zfill)
        return base_workdir + str(iph + 1)

    def _get_scf_workdir_single_calculation(self, icalc):
        delta = self.deltas_volumes[icalc]
        if delta >= 0:
            ext = f"_vol+{delta}percent"
        else:
            ext = f"_vol-{abs(delta)}percent"
        return self.scf_workdir + ext
