#!/bin/bash

MPIRUN="mpirun -np 4"
EXECUTABLE="/home/fgoudreault/Workspace/abinit/abinit/build/develop/2021-04-20/src/98_main/abinit"
INPUT=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_sequencers/files/abinit_Si_phonons/ph_runs/ph_run6/run/ph_run6.files
LOG=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_sequencers/files/abinit_Si_phonons/ph_runs/ph_run6/ph_run6.log
STDERR=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_sequencers/files/abinit_Si_phonons/ph_runs/ph_run6/ph_run6.stderr


$MPIRUN $EXECUTABLE < $INPUT > $LOG 2> $STDERR
