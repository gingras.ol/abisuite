from .bases import BaseFormattedFileParser
from ..file_structures import SlurmFilesToDeleteStructure


class SlurmFilesToDeleteParser(BaseFormattedFileParser):
    """Parser calss for the Slurm file that lists files to be deleted on the
    next purge.
    """
    _expected_ending = ""
    _loggername = "SlurmFilesToDeleteParser"
    _structure_class = SlurmFilesToDeleteStructure

    def _extract_data(self):
        super()._extract_data(strip_newlines=True, strip_spaces=True)

    def _extract_data_from_lines(self, lines):
        files_to_delete = []
        # skip first line since this is the header
        for line in lines[1:]:
            path = line.split(",")[0].strip('"')
            files_to_delete.append(path)
        return {"files_to_delete": files_to_delete}
