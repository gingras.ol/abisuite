#!/bin/bash

MPIRUN="mpirun -np 4"
EXECUTABLE="/home/fgoudreault/Workspace/abinit/abinit/build/develop/2021-04-16/src/98_main/abinit"
INPUT=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_sequencers/files/abinit_Al_smearing_convergence/scf_runs/tsmear0.1/ngkpt5_5_5/run/ngkpt5_5_5.files
LOG=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_sequencers/files/abinit_Al_smearing_convergence/scf_runs/tsmear0.1/ngkpt5_5_5/ngkpt5_5_5.log
STDERR=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_sequencers/files/abinit_Al_smearing_convergence/scf_runs/tsmear0.1/ngkpt5_5_5/ngkpt5_5_5.stderr


$MPIRUN $EXECUTABLE < $INPUT > $LOG 2> $STDERR
