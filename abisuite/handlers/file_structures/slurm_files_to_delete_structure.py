from .bases import BaseStructure


class SlurmFilesToDeleteStructure(BaseStructure):
    """Structure class for a slurm file listing the files to be deleted
    on the next cleanup period.
    """
    all_attributes = (
            "files_to_delete",
            )
