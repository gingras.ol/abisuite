import os

from .subparsers import (
        DtsetSubParser, HeaderParser,
        FooterParser,
        )
from .subparsers.abinit_log_subparsers import (
        AbinitLogWalltimeSubParser,
        )
from .subparsers.abinit_output_subparsers import (
        AbinitOutputTimingSubParser,
        )
from ..bases import BaseParserWithSubParsers
from ...file_structures import AbinitOutputStructure


class AbinitOutputParser(BaseParserWithSubParsers):
    """An ABINIT output file parser that gets data from an output file.

    Parameters
    ----------
    filepath : str
               The path to the output file.
    """
    _expected_ending = (".abo", ".out")
    _loggername = "AbinitOutputParser"
    _structure_class = AbinitOutputStructure
    _subparsers = (
            DtsetSubParser, HeaderParser, FooterParser,
            AbinitLogWalltimeSubParser, AbinitOutputTimingSubParser,
            )

    @classmethod
    def _filepath_from_meta(cls, meta):
        # usually, output file in workdir
        for expected_ending in cls._expected_ending:
            path = os.path.join(meta.calc_workdir,
                                meta.jobname + expected_ending)
            if not os.path.isfile(path):
                continue
            return path
        raise FileNotFoundError(
                f"{os.path.join(meta.calc_workdir, meta.jobname)}"
                f"{cls._expected_ending}")
