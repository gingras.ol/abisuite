from .bases import BaseQELogParser
from .subparsers.ph_log_subparsers import (
        QEPHLogNPhononsSubParser,
        QEPHLogPhononFreqSubParser, QEPHLogTimingSubParser,
        )
from ...file_structures import QEPHLogStructure


class QEPHLogParser(BaseQELogParser):
    """A Parser class that can parse a ph.x log file.
    """
    _loggername = "QEPHLogParser"
    _structure_class = QEPHLogStructure
    _subparsers = (
            QEPHLogNPhononsSubParser,
            QEPHLogPhononFreqSubParser,
            QEPHLogTimingSubParser,
            )
