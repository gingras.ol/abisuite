from .optic_launcher import AbinitOpticLauncher
from ..bases import BaseMassLauncher


class AbinitOpticMassLauncher(BaseMassLauncher):
    """The Mass Launcher class for Optic calculations.
    """
    _loggername = "AbinitOpticMassLauncher"
    _launcher_class = AbinitOpticLauncher
