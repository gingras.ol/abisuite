#!/bin/bash

MPIRUN="mpirun -np 4"
EXECUTABLE="/home/fgoudreault/Workspace/q-e/bin/pw.x -npool 4"
INPUT=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_sequencers/files/qe_pb_thermal_expansion/scf_runs/scf_vol-1percent/scf_vol-1percent.in
LOG=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_sequencers/files/qe_pb_thermal_expansion/scf_runs/scf_vol-1percent/scf_vol-1percent.log
STDERR=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_sequencers/files/qe_pb_thermal_expansion/scf_runs/scf_vol-1percent/scf_vol-1percent.stderr


$MPIRUN $EXECUTABLE -input $INPUT > $LOG 2> $STDERR
