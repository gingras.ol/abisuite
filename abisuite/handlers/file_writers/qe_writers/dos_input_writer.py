from .bases import BaseQEInputWriter
from ...file_structures import QEDOSInputStructure


class QEDOSInputWriter(BaseQEInputWriter):
    """Class that can write a Quantum Espresso dos.x input file.
    """
    _loggername = "QEDOSInputWriter"
    _structure_class = QEDOSInputStructure

    @property
    def lines(self):
        lines = []
        self._append_var_block(lines, "dos")
        return lines
