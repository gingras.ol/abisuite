Workflows
=========

Workflows are a collection of sequencers. The manage what sequence of calculations to do in order
to match what is required by the __init__ parameters.
