import datetime
import json
import os

from .bases import BaseAbisuiteSubScript
from ..colors import Colors
from ..handlers import MetaDataFile, PBSFile
from ..routines import command_to_calctype, full_abspath


class AbisuiteMetaDataFileRepairScript(BaseAbisuiteSubScript):
    """Executes the 'metadatarepair' part of the abisuite script.

    Attempts at repairing a possibly broken meta data file in a calculation
    directory.
    """
    _loggername = "AbisuiteMetaDataFileRepairScript"

    def load_calculations(self, calculations_srcs):
        """Loads the calculations to attemp repair into the 'calculations'
        attribute.

        This method overrides the mother's class method since meta data file
        can be broken, the aforemetioned method is likely to fail.

        Parameters
        ----------
        calculations_srcs: list
            The list of calculations root sources. It will recursively
            load calculations path from these sources.
        """
        for src in calculations_srcs:
            src = full_abspath(src)
            # check if there is a meta data file
            dir_content = os.listdir(src)
            for filename in dir_content:
                if filename.startswith(".") and filename.endswith(".meta"):
                    self.calculations.append(src)
                    break
            else:
                # not a calcdir => recursively check in it for deeper calc dirs
                for name in dir_content:
                    if os.path.isdir(name):
                        self.load_calculations((os.path.join(src, name), ))

    def repair(self, dry_run=False):
        """Actually does the repair.

        Parameters
        ----------
        dry_run: bool, optional
            If True, a dry run is done: only what calculations that would
            be affected are printed out.
        """
        if dry_run:
            print("Meta data file repair: dry run only.")
        for calculation in self.calculations:
            print(Colors.bold_text("Attempt repairing:") +
                  f" '{calculation}'")
            if dry_run:
                continue
            try:
                with MetaDataFile.from_calculation(
                                calculation, loglevel=self._loglevel) as meta:
                    meta.calc_workdir = calculation
                    print("Repairing attempt done.")  # noqa: T001
            except FileNotFoundError:
                # this is not a calculation directory, skip it
                continue
            except json.decoder.JSONDecodeError:
                # meta data file needs to be generated again
                self.regenerate_meta_data_file(calculation)
            except Exception:
                print(Colors.color_text("ERROR:", "bold", "red"))
                self._logger.exception()

    def regenerate_meta_data_file(self, calculation):
        """Regenerated completely the meta data file for this calculation.
        """
        print(Colors.bold_text("REGENERATING") + " meta data file.")
        meta = MetaDataFile.from_calculation(
                calculation, loglevel=self._loglevel)
        meta.delete()
        meta.calc_workdir = calculation
        # need to get the calctype
        # do this by reading the pbs file and getting the command
        for filename in os.listdir(os.path.join(calculation, "run")):
            if filename.endswith(".sh"):
                break
        else:
            print(Colors.color_text("Error", "bold", "red") +
                  " while looking for pbs file: could not find it.")
            return
        with PBSFile.from_file(
                os.path.join(calculation, "run", filename),
                loglevel=self._loglevel) as pbs:
            meta.jobname = pbs.basename.rstrip(".sh")
            meta.calctype = command_to_calctype(pbs.command)
            meta.pbs_file_path = pbs.path
        # put all the following to empty lists as we can't know for sure
        # which files were linked to this calculation or are linked from
        # this calculation
        meta.children = []
        meta.copied_files = []
        meta.parents = []
        meta.linked_files = []
        meta.input_data_dir = os.path.join(calculation, "input_data")
        meta.rundir = os.path.join(calculation, "run")
        meta.output_data_dir = os.path.join(calculation, "run", "output_data")
        meta.submit_time = str(datetime.datetime.now())
        meta.last_modified = str(datetime.datetime.now())
        # determine input file name
        for filename in os.listdir(calculation):
            if filename.endswith(".in") or (filename.endswith(".abi") or
                                            filename.endswith(".win")):
                meta.input_file_path = os.path.join(calculation, filename)
                break
        else:
            print(Colors.color_text("Error:", "bold", "red") +
                  " could not find input file.")
            return
        if meta.calctype.startswith("abinit"):
            # need to get files file
            for filename in os.listdir(os.path.join(calculation, "run")):
                if filename.endswith(".files"):
                    break
            else:
                print(Colors.color_text("ERROR:", "bold", "red") +
                      " could not find files file for abinit calc.")
                return
            meta.script_input_file_path = os.path.join(
                    calculation, "run", filename)
        else:
            meta.script_input_file_path = meta.input_file_path
        meta.write()
        print(Colors.color_text("Successfully", "bold", "green") +
              " regenerated meta data file.")
