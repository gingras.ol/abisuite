from ..bases import BaseLogFileHandler
from ...file_parsers import AbinitLogParser
from ...file_structures import AbinitLogStructure


class AbinitLogFile(BaseLogFileHandler):
    """File handler class for an abinit log file.
    """
    _loggername = "AbinitLogFile"
    _parser_class = AbinitLogParser
    _structure_class = AbinitLogStructure
