#!/bin/bash

MPIRUN="mpirun -np 4"
EXECUTABLE="/home/fgoudreault/Workspace/q-e/bin/matdyn.x -npool 4"
INPUT=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_sequencers/files/qe_pb_thermal_expansion/matdyn_runs/matdyn_vol-1percent/matdyn_vol-1percent.in
LOG=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_sequencers/files/qe_pb_thermal_expansion/matdyn_runs/matdyn_vol-1percent/matdyn_vol-1percent.log
STDERR=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_sequencers/files/qe_pb_thermal_expansion/matdyn_runs/matdyn_vol-1percent/matdyn_vol-1percent.stderr


$MPIRUN $EXECUTABLE -input $INPUT > $LOG 2> $STDERR
