from .bases import CommonQEInputVariable
from ..bases import (MANDATORY_VARIABLE_KEYS, OPTIONAL_VARIABLE_KEYS,
                     variableDBIntegrityChecker)


# #############################################################################
# ################# Quantum Espresso q2r.x script variables ###################
# #############################################################################

# No doc on the input variables on the QE website. Need to go directly
# into the source code for the docs.

QELD1_ALLOWED_BLOCKS = ("input", "test", )
QELD1_ALLOWED_CLASSES = (CommonQEInputVariable, )
RAW_QELD1_VARIABLES = {
        "atom": {
            "allowed": (
                "H", "He", "Li", "Be", "B", "C", "N", "O", "F", "Ne", "Na",   #
                "Mg", "Al", "Si", "P", "S", "Cl", "Ar", "K", "Ca", "Sc", "Ti",
                "V", "Cr", "Mn", "Fe", "Co", "Ni", "Cu", "Zn", "Ga", "Ge",
                "As", "Se", "Br", "Kr", "Rb", "Sr", "Y", "Zr", "Nb", "Mo",
                "Tc", "Ru", "Rh", "Pd", "Ag", "Cd", "In", "Sn", "Sb", "Te",
                "I", "Xe", "Cs", "Ba", "La", "Ce", "Pr", "Nd", "Pm", "Sm",
                "Eu", "Gd", "Tb", "Dy", "Ho", "Er", "Tm", "Yb", "Lu", "Hf",
                "Ta", "W", "Re", "Os", "Ir", "Pt", "Au", "Hg", "Tl", "Pb",
                "Bi", "Po", "As", "Rn", "Fr", "Ra", "Ac", "Th", "Pa", "U",
                "Np", "Pu", "Am", "Cm", "Bk", "Cf", "Es", "Fm", "Md", "No",
                "Lr", "Rf", "Db", "Sg", "Bh", "Hs", "Mt", "Ds", "Rg", "Cn",
                "Nh", "Fl", "Mc", "Lv", "Ts", "Og"
                ),
            "block": "input",
            "class": CommonQEInputVariable,
            "mandatory": True,
            "type": str,
            },
        "config": {
            "block": "input",
            "class": CommonQEInputVariable,
            "mandatory": True,
            "type": str,
            },
        "configts(*)": {
            "block": "test",
            "class": CommonQEInputVariable,
            "mandatory": False,
            "type": str,
            },
        "dft": {
            "allowed": (
                "PZ", "PW91", "BP", "PBE", "BLYP",
                ),
            "block": "input",
            "class": CommonQEInputVariable,
            "mandatory": True,
            "type": str,
            },
        "decut": {
            "block": "test",
            "class": CommonQEInputVariable,
            "mandatory": True,
            "type": float,
            },
        "ecutmax": {
            "block": "test",
            "class": CommonQEInputVariable,
            "mandatory": True,
            "type": float,
            },
        "ecutmin": {
            "block": "test",
            "class": CommonQEInputVariable,
            "mandatory": True,
            "type": float,
            },
        "file_pseudo": {
            "block": "test",
            "class": CommonQEInputVariable,
            "mandatory": False,
            "type": str,
            },
        "iswitch": {
            "allowed": (1, 2, 3, 4),
            "block": "input",
            "class": CommonQEInputVariable,
            "mandatory": False,
            "type": int,
            },
        "nconf": {
            "block": "test",
            "class": CommonQEInputVariable,
            "mandatory": False,
            "type": int,
            },
        }

ALL_QELD1_VARIABLES = variableDBIntegrityChecker(
        RAW_QELD1_VARIABLES,
        QELD1_ALLOWED_BLOCKS,
        MANDATORY_VARIABLE_KEYS,
        QELD1_ALLOWED_CLASSES,
        optional_keys=OPTIONAL_VARIABLE_KEYS)
