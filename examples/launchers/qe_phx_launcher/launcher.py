from abilaunch import QEPHLauncher as Launcher


# very simple example
variables = {"amass(1)": 28.086,
             "q_points": [0.0, 0.0, 0.0],
            }

launcher = Launcher("silicon")
launcher.workdir = "."
launcher.command = "ph.x"
launcher.input_variables = variables
launcher.link_calculation("../qe_pwx_launcher")
launcher.write()
launcher.run()
