from .bases import BaseQELogTimingSubParser


class QEDOSLogTimingSubParser(BaseQELogTimingSubParser):
    _loggername = "QEDOSLogTimingSubParser"
    trigger = "DOS          :"
