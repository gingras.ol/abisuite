set style data dots
set nokey
set xrange [0: 6.22903]
set yrange [ -2.22494 : 17.79061]
set arrow from  1.24626,  -2.22494 to  1.24626,  17.79061 nohead
set arrow from  1.86939,  -2.22494 to  1.86939,  17.79061 nohead
set arrow from  2.75063,  -2.22494 to  2.75063,  17.79061 nohead
set arrow from  3.51381,  -2.22494 to  3.51381,  17.79061 nohead
set arrow from  4.83567,  -2.22494 to  4.83567,  17.79061 nohead
set xtics ("G"  0.00000,"X"  1.24626,"W"  1.86939,"L"  2.75063,"K"  3.51381,"G"  4.83567,"W"  6.22903)
 plot "600.0K_band.dat"
