import logging
import unittest

from ..bases import BaseUtility
from ..exceptions import DevError


class TestBaseUtility(unittest.TestCase):
    def test_BaseUtility(self):
        # test forgot loggername raises an error
        class test_base_class(BaseUtility):
            pass
        with self.assertRaises(DevError):
            test_base_class()
        loggername = "test_loggername"
        loglevel = 17
        # test loglevel is set at the right level

        class test_base_class(BaseUtility):
            _loggername = loggername
        instance = test_base_class(loglevel=loglevel)
        self.assertEqual(instance._logger.level, loglevel)
        self.assertEqual(instance._logger.name, loggername)

    def test_loglevel_property(self):
        class test_base_class(BaseUtility):
            _loggername = "test"
        loglevel = logging.INFO
        instance = test_base_class(loglevel=loglevel)
        self.assertEqual(instance._loglevel, loglevel)
        # check that changing the loglevel works
        newloglevel = logging.ERROR
        instance._loglevel = newloglevel
        self.assertEqual(instance._loglevel, newloglevel)
