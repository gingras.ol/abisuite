import unittest

from .bases import BaseCleanerTest
from ..bases import TestCase
from ..variables_for_tests import (
        QEMatdynCalc, qe_matdynx_vars, QEPHCalc, qe_phx_vars,
        QEEPWCalc, qe_epwx_vars, QEPWCalc, qe_pwx_vars,
        QEQ2RCalc, qe_q2rx_vars,
        )
from ...cleaners import (
        QEEPWCleaner, QEMatdynCleaner, QEPHCleaner, QEPWCleaner, QEQ2RCleaner,
        )


class BaseQECleanerTest(BaseCleanerTest):
    """Base class for QE cleaners test cases.
    """
    pass


class TestQEEPWCleaner(BaseQECleanerTest, TestCase):
    """Test case for the 'qe_epw' cleaner class.
    """
    _cleaner_cls = QEEPWCleaner
    _calculation_example = QEEPWCalc
    _calculation_example_input_variables = qe_epwx_vars
    _calculation_example_parents = []  # TODO: IMPLEMENT ME
    _script_name = "epw.x"

    @unittest.skip("Not implemented...")
    def test_clean_abort_if_calculation_not_started(self):
        BaseQECleanerTest.test_clean_abort_if_calculation_not_started(self)


class TestQEMatdynCleaner(BaseQECleanerTest, TestCase):
    """Test case for the 'qe_matdyn' cleaner class.
    """
    _cleaner_cls = QEMatdynCleaner
    _calculation_example = QEMatdynCalc
    _calculation_example_input_variables = qe_matdynx_vars
    _calculation_example_parents = [QEQ2RCalc]
    _script_name = "matdyn.x"


class TestQEPHCleaner(BaseQECleanerTest, TestCase):
    """Test case for the 'qe_ph' cleaner class.
    """
    _cleaner_cls = QEPHCleaner
    _calculation_example = QEPHCalc
    _calculation_example_input_variables = qe_phx_vars
    _calculation_example_parents = [QEPWCalc]
    _script_name = "ph.x"


class TestQEPWCleaner(BaseQECleanerTest, TestCase):
    """Test case for the 'qe_pw' cleaner class.
    """
    _cleaner_cls = QEPWCleaner
    _calculation_example = QEPWCalc
    _calculation_example_input_variables = qe_pwx_vars
    _script_name = "pw.x"


class TestQRQ2RCleaner(BaseQECleanerTest, TestCase):
    """Test case for the 'qe_q2r' cleaner class.
    """
    _cleaner_cls = QEQ2RCleaner
    _calculation_example = QEQ2RCalc
    _calculation_example_input_variables = qe_q2rx_vars
    _calculation_example_parents = [QEPHCalc]
    _script_name = "q2r.x"
