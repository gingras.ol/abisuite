from ..bases import BaseInputFileHandler
from ...file_approvers import QELD1InputApprover
from ...file_parsers import QELD1InputParser
from ...file_structures import QELD1InputStructure
from ...file_writers import QELD1InputWriter


class QELD1InputFile(BaseInputFileHandler):
    """File handler for a Quantum Espresso ld1.x input file.
    """
    _approver_class = QELD1InputApprover
    _loggername = "QELD1InputFile"
    _parser_class = QELD1InputParser
    _structure_class = QELD1InputStructure
    _writer_class = QELD1InputWriter
