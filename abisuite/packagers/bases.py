import abc
import os

from .routines import compute_packaging_path, get_packaging_roots
from ..bases import BaseCalctypedUtility


class BasePackager(BaseCalctypedUtility, abc.ABC):
    """Base class for all Packagers objects.
    """

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._calculation_directory = None
        self.nfiles_saved = 0
        self.nGbs_saved = 0

    @property
    def calculation_directory(self):
        """The packager's CalculationDirectory.
        """
        if self._calculation_directory is not None:
            return self._calculation_directory
        raise ValueError("Need to set the 'calculation_directory'.")

    @calculation_directory.setter
    def calculation_directory(self, calc):
        if isinstance(calc, str):
            from ..handlers import CalculationDirectory
            calc = CalculationDirectory.from_calculation(
                    calc, loglevel=self._loglevel)
        self._calculation_directory = calc

    @abc.abstractmethod
    def keep_file(self, handler):
        """Tells if a file must be packaged or not.

        Parameters
        ----------
        handler: FileHandler object
            The file handler to check.

        Returns
        -------
        bool: True if we package file, False if not.
        """
        name = handler.basename
        if name.endswith(".in"):
            return True
        if name.endswith(".log"):
            return True
        if ".log_" in name:
            # also copy backups of logs
            return True
        if name.endswith(".meta"):
            return True
        if name.endswith(".sh"):
            return True
        return False

    def package(
            self, overwrite=False, replace_with_symlink=False,
            keep_density=False, keep_wavefunctions=False,
            **kwargs):
        """Packages the calculation directory attached to the packager object.
        It basically copies relevant files useful for safekeeping and
        ignores everything else.

        It also keeps the directory tree intact. That means it will keep the
        subdirectories hierarchy of the src w.r.t. a given root and create/keep
        the same hierarchy w.r.t. the dest root.

        Parameters
        ----------
        overwrite: bool, optional
            If True and files with the same names are present at packaging
            destination, those files will be overwritten.
        replace_with_symlink: bool, optional
            If True, the calculation is replaced with a symlink towards the
            packaged calculation.
        keep_density: bool, optional
            Density files are not usually packaged unless this flag is True.
        keep_wavefunctions: bool, optional
            Wavefunction files are not usually packaged unless
            this flag is True.

        all other kwargs are passed to the `get_packaging_roots` method.

        Raises
        ------
        ValueError:
            - If the calculation directory to package lies outside the
              src root.
        """
        src_root, dest_root = get_packaging_roots(
                _logger=self._logger, **kwargs)
        self._logger.debug(
                f"Packaging '{self.calculation_directory.path}' with src_root="
                f"'{src_root}' and dest_root='{dest_root}'.")
        # compare src root with calculation directory
        calcdirrelpath = os.path.relpath(
                self.calculation_directory.path, src_root)
        if calcdirrelpath.startswith(".."):
            raise ValueError(
                    f"Directory '{self.calculation_directory.path}' "
                    f"is outside package src root: '{src_root}'.")
        if not self.calculation_directory.is_connected_to_database:
            self.calculation_directory.connect_to_database()
        if self.calculation_directory.is_in_database:
            if self.calculation_directory.is_packaged and not overwrite:
                self._logger.info("Already packaged, do nothing else.")
        for handler in self.calculation_directory.walk(paths_only=False):
            if not handler.exists:
                continue
            if self.keep_file(
                    handler, keep_wavefunctions=keep_wavefunctions,
                    keep_density=keep_density,
                    ):
                newpath = compute_packaging_path(
                        handler.path, src_root, dest_root)
                handler.copy(newpath, overwrite=overwrite)
                self.nfiles_saved += 1
                self.nGbs_saved += handler.file_size_Gb
        # If we're here, we replace the whole calculation with a symlink
        # once the calculation has been packaged, repair the copied meta file
        # and change the db entry
        new_meta = compute_packaging_path(
                self.calculation_directory.meta_data_file.path, src_root,
                dest_root)
        new_workdir = os.path.dirname(new_meta)
        # update new meta data file
        self.calculation_directory.update_new_meta_data_file(
                new_workdir, self.calculation_directory.path)
        # change db
        if not self.calculation_directory.is_in_database:
            # add to database
            self.calculation_directory.add_to_database()
        # change the packaged attribute to True
        self.calculation_directory.update_database_packaged(True)
        if not replace_with_symlink:
            return
        # now delete the calculation directory to replace it with a symlink
        self.calculation_directory.delete()
        # replace with symlink
        from ..handlers import SymLinkFile
        symlink = SymLinkFile(loglevel=self._loglevel)
        symlink.source = new_workdir
        symlink.path = self.calculation_directory.path
        symlink.write()
        # update db entry
        id_ = self.calculation_directory.database[
                self.calculation_directory.path]
        self.calculation_directory.database.update_calculation(
                id_, new_workdir)
