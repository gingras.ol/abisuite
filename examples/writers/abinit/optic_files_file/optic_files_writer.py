from abilaunch.writers import OpticFilesFileWriter


path = "optic_example.files"

writer = OpticFilesFileWriter()
writer.path = path
writer.input_file_path = "optic_example.in"
writer.output_file_path = "optic_example.out"
writer.output_data_prefix = "odat_optic_example"
writer.output_data_dir = "."
writer.write(overwrite=True)
