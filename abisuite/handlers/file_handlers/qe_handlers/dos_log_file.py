from ..bases import BaseLogFileHandler
from ...file_parsers import QEDOSLogParser
from ...file_structures import QEDOSLogStructure


class QEDOSLogFile(BaseLogFileHandler):
    """Log file handler for a Quantum Espresso dos.x log file.
    """
    _loggername = "QEDOSLogFile"
    _parser_class = QEDOSLogParser
    _structure_class = QEDOSLogStructure
