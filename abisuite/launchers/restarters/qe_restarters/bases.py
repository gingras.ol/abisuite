from ..bases import BaseRestarter


class BaseQERestarter(BaseRestarter):
    """Base class for any QE Restarter class.
    """
    pass
