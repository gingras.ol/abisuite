import abc

from ..bases import BaseSubParser
from ....routines import decompose_line


class BasePBSSubParser(BaseSubParser, abc.ABC):
    """Base class for PBS subparsers
    """
    def _extract_data_from_lines(self, lines):
        data = {}
        # FIXME: This is weird!!!
        for j, line in enumerate(lines):
            if not line.startswith(self.trigger):
                self.ending_relative_index = j
                break
            self._extract_data_from_line(line, data)
        if self.subject not in (
                        "modules_to_load", "modules_to_unload",
                        "modules_to_use", "modules_to_swap"):
            if "queuing_system" not in data:
                data["queuing_system"] = self.subject
        return data

    @abc.abstractmethod
    def _extract_data_from_line(self, line, data):
        pass  # pragma: no cover


class BaseModulesSubParser(BasePBSSubParser):
    """base class for modules subparsers in PBS file.
    """

    def _extract_data_from_line(self, line, data):
        s, i, f = decompose_line(line)
        data.setdefault(self.subject, [])
        data[self.subject].append(s[-1])


class ModulestoLoadSubParser(BaseModulesSubParser):
    """Subparser for pbs files for modules to load.
    """
    _loggername = "ModulestoLoadSubParser"
    subject = "modules_to_load"
    trigger = "module load"


class ModulestoUnloadSubParser(BaseModulesSubParser):
    """Subparser for pbs files for modules to unload.
    """
    _loggername = "ModulestoUnloadSubParser"
    subject = "modules_to_unload"
    trigger = "module unload"


class ModulestoUseSubParser(BaseModulesSubParser):
    """Subparser for pbs files for modules to use.
    """
    _loggername = "ModulestoUseSubParser"
    subject = "modules_to_use"
    trigger = "module use"


class ModulestoSwapSubParser(BaseModulesSubParser):
    """Subparser for pbs files for modules to swap.
    """
    _loggername = "ModulestoSwapSubParser"
    subject = "modules_to_swap"
    trigger = "module swap"

    def _extract_data_from_line(self, line, data):
        s, i, f = decompose_line(line)
        data.setdefault(self.subject, [])
        data[self.subject].append([s[-2], s[-1]])


class SlurmSubParser(BasePBSSubParser):
    """Subparser for a pbs file for a slurm queuing system.
    """
    _loggername = "SlurmSubParser"
    subject = "slurm"
    trigger = "#SBATCH"

    def _extract_data_from_line(self, line, data):
        # SBATCH command
        s, i, f = decompose_line(line)
        if "--job-name" in line:
            data["jobname"] = s[-1].split("=")[-1]
        elif "--nodes" in line:
            nodes = s[-1].split("=")[-1]
            try:
                nodes = int(nodes)
            except ValueError:
                # not an integer => 1:m24G or something like that
                pass
            data["nodes"] = nodes
        elif "--cpus-per-task" in line:
            data["cpus_per_task"] = s[-1].split("=")[-1]
        elif "--account" in line:
            data["project_account"] = s[-1].split("=")[-1]
        elif "--ntasks-per-node" in line:
            data["ppn"] = int(s[-1].split("=")[-1])
        elif "--ntasks=" in line or "--ntasks " in line:
            data["ntasks"] = int(s[-1].split("=")[-1])
        elif "--time" in line:
            data["walltime"] = s[-1].split("=")[-1]
        elif "--partition" in line:
            data["queue"] = s[-1].split("=")[-1]
        elif "--qos" in line:
            data["quality_of_service"] = s[-1].split("=")[-1]
        elif "--mem" in line and "--mem-per-cpu" not in line:
            data["memory"] = s[-1].split("=")[-1]
            # try converting to int if possible
            try:
                data["memory"] = int(data["memory"])
            except ValueError:
                # nevermind
                pass
        elif "--mem-per-cpu" in line:
            data["memory_per_cpu"] = s[-1].split("=")[-1]


class GridEngineSubParser(BasePBSSubParser):
    """Subparser for a pbs file for a GridEngine queuing system.
    """
    _loggername = "GridEngineSubParser"
    subject = "grid_engine"
    trigger = "#$"

    def _extract_data_from_line(self, line, data):
        s, i, f = decompose_line(line)
        if "qname" in line:
            data["queue"] = s[-1].split("=")[-1]
        elif "h_rt" in line:
            data["walltime"] = s[-1].split("=")[-1]
        elif "-N" in line:
            # jobname
            data["jobname"] = s[-1]
        elif "-pe" in line:
            data["total_ncpus"] = i[0]
            # only one node in grid engine???
            data["ppn"] = data["total_ncpus"]
            data["nodes"] = 1


class PBSSubParser(BasePBSSubParser):
    """Subparser for a pbs file for a torque or PBS professional
    queuing system.
    """
    _loggername = "PBSSubParser"
    subject = "pbs"
    trigger = "#PBS"

    def _extract_data_from_line(self, line, data):
        s, i, f = decompose_line(line)
        if "-N" in line:
            # jobname
            data["jobname"] = s[-1]
        elif "walltime" in line:
            # PBS -l walltime=1:00:00
            data["walltime"] = s[-1].split("=")[-1]
        elif "nodes" in line:
            # TORQUE ONLY !!!!!
            # PBS -l nodes=1:m24G:ppn=12
            # m24G might not be there (optional)
            split = s[-1].split("=")
            # split = [nodes, 1:m24G:ppn, 12]
            data["nodes"] = split[1]
            if data["nodes"].endswith("ppn"):
                data["nodes"] = data["nodes"][:-4]
            data["ppn"] = split[-1]
            data["queuing_system"] = "torque"
        elif "-l select" in line:
            # PBS Professional ONLY !!!!!
            # #PBS -l select=3
            split = s[-1].split("=")
            data["nodes"] = split[-1]
            data["queuing_system"] = "pbs_professional"
        elif "-A" in line:
            # PBS Professional ONLY !!!!!
            # #PBS -A [project code]
            data["project_code"] = s[-1]
