from .bases import Wannier90InputVariable
from ..bases import (
            variableDBIntegrityChecker, MANDATORY_VARIABLE_KEYS,
            OPTIONAL_VARIABLE_KEYS,
            )
from ...routines import is_vector


class Wannier90AtomicPositionsVariable(Wannier90InputVariable):
    """Special variable that represents the atomic positions.
    """
    _loggername = "Wannier90AtomicPositionsVariable"

    def __str__(self):
        string = f"\nbegin {self.value['parameter']}\n"
        toconvert = []
        for atom, positions in self.value["positions"].items():
            for position in positions:
                line = [atom] + list(position)
                toconvert.append(line)
        string += self.convert_2d_arr_to_str(toconvert)
        string += f"end {self.value['parameter']}\n"
        return string

    @Wannier90InputVariable.value.setter
    def value(self, value):
        # value should be a dict by this point
        for mandatory_key in ("parameter", "positions"):
            if mandatory_key not in value:
                raise KeyError(f"Unit cell variable should have parameter:"
                               f"'{mandatory_key}'")
        if value["parameter"].lower() not in ("atoms_frac", ):
            raise KeyError("Unsupported unit cell parameter: "
                           f"{value['parameter']}")
        # positions should be a dict whose keys are the atom species
        # and values are 2d arrays of the positions
        if not isinstance(value["positions"], dict):
            raise TypeError(f"'positions' should be a dict but got: "
                            f"{value['positions']}")
        for positions in value["positions"].values():
            if self.is_value_a_vector(value=positions):
                value["positions"]["atom"] = [positions]
                continue
            if not self.is_value_a_2d_array(value=positions):
                raise TypeError(f"Atomic positions should be a 2d array but "
                                f"got: {positions}")
        Wannier90InputVariable.value.fset(self, value)


class Wannier90KpointPathVariable(Wannier90InputVariable):
    """Special variable that represents the Kpoints_path variable.
    """
    _loggername = "Wannier90KpointPathVariable"

    def __str__(self):
        string = "\nbegin Kpoint_Path\n"
        toconvert = []
        for path in self.value:
            # path are list of 2 kpt which define the start and end of
            # a kpt line in the path
            # each kpt is a dict whose value is the label and value the coords
            label1 = list(path[0].keys())[0]
            label2 = list(path[1].keys())[0]
            line = [label1] + path[0][label1] + [label2] + path[1][label2]
            toconvert.append(line)
        string += self.convert_2d_arr_to_str(toconvert)
        string += "end Kpoint_Path\n"
        return string

    @Wannier90InputVariable.value.setter
    def value(self, value):
        # value should be a list by this point
        for path in value:
            if len(path) != 2:
                raise TypeError("Each kpt path should be a list of 2 items.")
            for kpt in path:
                if not isinstance(kpt, dict):
                    raise TypeError("kpt should be a dict whose key is the "
                                    "kpt label and value the coords.")
                if len(kpt) != 1:
                    raise TypeError("kpt should be a dict whose key is the "
                                    "kpt label and value the coords.")
                label = list(kpt.keys())[0]
                coords = kpt[label]
                if not is_vector(coords):
                    raise TypeError("Kpt coords should be a vector.")
                if len(coords) != 3:
                    raise TypeError("Kpt coords should be 3 components.")
                # round to 5 decimals, should be enough
                # We do this to prevent having too long lines.
                coords = [round(x, 5) for x in coords]
                kpt[label] = coords
        Wannier90InputVariable.value.fset(self, value)


class Wannier90KpointsVariable(Wannier90InputVariable):
    """Special variable that represents the kpoints variable.
    """
    _loggername = "Wannier90KpointsVariable"

    def __str__(self):
        string = "\nbegin kpoints\n"
        string += self.convert_2d_arr_to_str(self.value)
        string += "end kpoints\n"
        return string

    @Wannier90InputVariable.value.setter
    def value(self, value):
        # value should be a list by this point
        for kpt in value:
            if len(kpt) != 3:
                raise TypeError("kpt should be len 3.")
        Wannier90InputVariable.value.fset(self, value)


class Wannier90ProjectionsVariable(Wannier90InputVariable):
    """Special variable that represents the projections.
    """
    _loggername = "Wannier90ProjectionsVariable"

    def __str__(self):
        string = "\nbegin projections\n"
        for proj in self.value:
            string += str(proj) + "\n"
        string += "end projections\n"
        return string


class Wannier90UnitCellVectorsVariable(Wannier90InputVariable):
    """Special variable that represents the unit cell vectors.
    """
    _loggername = "Wannier90UnitCellVectorsVariable"

    def __str__(self):
        string = f"\nbegin {self.value['parameter']}\n"
        if "units" in self.value:
            # optional parameter
            string += f"{self.value['units']}\n"
        string += self.convert_2d_arr_to_str(self.value["vectors"])
        string += f"end {self.value['parameter']}\n"
        return string

    @Wannier90InputVariable.value.setter
    def value(self, value):
        # value should be a dict by this point
        for mandatory_key in ("parameter", "vectors", ):
            if mandatory_key not in value:
                raise KeyError(f"Unit cell variable should have parameter:"
                               f"'{mandatory_key}'")
        if value["parameter"].lower() not in ("unit_cell_cart", ):
            raise KeyError("Unsupported unit cell parameter: "
                           f"{value['parameter']}")
        if "units" in value:
            if value["units"] not in ("bohr", "ang"):
                raise KeyError("Unsupported unit cell units: "
                               f"{value['units']}")
        if not self.is_value_a_2d_array(value=value["vectors"]):
            raise TypeError(f"'vectors' should be a 2D array but got: "
                            f"{value['vectors']}")
        Wannier90InputVariable.value.fset(self, value)


# For now don't separate variables into blocks
WANNIER90_ALLOWED_BLOCKS = ("control", "kpoints", "system", )
WANNIER90_ALLOWED_CLASSES = (
                Wannier90InputVariable, Wannier90AtomicPositionsVariable,
                Wannier90KpointPathVariable, Wannier90KpointsVariable,
                Wannier90ProjectionsVariable,
                Wannier90UnitCellVectorsVariable,
                )
RAW_WANNIER90_VARIABLES = {
        "atomic_positions": {
            "block": "system",
            "class": Wannier90AtomicPositionsVariable,
            "mandatory": True,
            "type": dict,
            },
        "bands_num_points": {
            "block": "control",
            "class": Wannier90InputVariable,
            "mandatory": False,
            "type": int,
            },
        "bands_plot": {
            "block": "control",
            "class": Wannier90InputVariable,
            "mandatory": False,
            "type": bool,
            },
        "conv_tol": {
            "block": "control",
            "class": Wannier90InputVariable,
            "mandatory": False,
            "type": float,
            },
        "conv_window": {
            "block": "control",
            "class": Wannier90InputVariable,
            "mandatory": False,
            "type": int,
            },
        "dis_froz_max": {
            "block": "control",
            "class": Wannier90InputVariable,
            "mandatory": False,
            "type": float,
            },
        "dis_froz_min": {
            "block": "control",
            "class": Wannier90InputVariable,
            "mandatory": False,
            "type": float,
            },
        "dis_mix_ratio": {
            "block": "control",
            "class": Wannier90InputVariable,
            "mandatory": False,
            "type": float,
            },
        "dis_num_iter": {
            "block": "control",
            "class": Wannier90InputVariable,
            "mandatory": False,
            "type": int,
            },
        "dis_win_max": {
            "block": "control",
            "class": Wannier90InputVariable,
            "mandatory": False,
            "type": float,
            },
        "dis_win_min": {
            "block": "control",
            "class": Wannier90InputVariable,
            "mandatory": False,
            "type": float,
            },
        "exclude_bands": {
            "block": "system",
            "class": Wannier90InputVariable,
            "mandatory": False,
            "type": str,
            },
        "fermi_energy": {
            "block": "system",
            "class": Wannier90InputVariable,
            "mandatory": False,
            "type": float,
            },
        "guiding_centres": {
            "block": "control",
            "class": Wannier90InputVariable,
            "mandatory": False,
            "type": bool,
            },
        "iprint": {
            "block": "control",
            "class": Wannier90InputVariable,
            "mandatory": False,
            "type": int,
            },
        "kpoint_path": {
            "block": "kpoints",
            "class": Wannier90KpointPathVariable,
            "mandatory": False,
            "type": list,
            },
        "kpoints": {
            "block": "kpoints",
            "class": Wannier90KpointsVariable,
            "mandatory": True,
            "type": "2darr",
            },
        "mp_grid": {
            "block": "kpoints",
            "class": Wannier90InputVariable,
            "len": 3,
            "mandatory": True,
            "type": "vector",
            },
        "num_bands": {
            "block": "control",
            "class": Wannier90InputVariable,
            "mandatory": False,
            "type": int,
            },
        "num_iter": {
            "block": "control",
            "class": Wannier90InputVariable,
            "mandatory": True,
            "type": int,
            },
        "num_print_cycles": {
            "block": "control",
            "class": Wannier90InputVariable,
            "mandatory": False,
            "type": int,
            },
        "num_wann": {
            "block": "control",
            "class": Wannier90InputVariable,
            "mandatory": True,
            "type": int,
            },
        "projections": {
            "block": "system",
            "class": Wannier90ProjectionsVariable,
            "mandatory": True,
            "type": "vector",
            },
        "restart": {
            "allowed": ("plot", ),
            "block": "control",
            "class": Wannier90InputVariable,
            "mandatory": False,
            "type": str,
            },
        "spinors": {
            "block": "system",
            "class": Wannier90InputVariable,
            "mandatory": False,
            "type": bool,
            },
        "unit_cell": {
            "block": "system",
            "class": Wannier90UnitCellVectorsVariable,
            "mandatory": True,
            "type": dict,
            },
        "wannier_plot": {
            "block": "control",
            "class": Wannier90InputVariable,
            "mandatory": False,
            "type": bool,
            },
        "wannier_plot_supercell": {
            "block": "control",
            "class": Wannier90InputVariable,
            "mandatory": False,
            "type": int,
            },
        "write_bvec": {
            "block": "control",
            "class": Wannier90InputVariable,
            "mandatory": False,
            "type": bool,
            },
        "write_hr": {
            "block": "control",
            "class": Wannier90InputVariable,
            "mandatory": False,
            "type": bool,
            },
        }


ALL_WANNIER90_VARIABLES = variableDBIntegrityChecker(
            RAW_WANNIER90_VARIABLES,
            WANNIER90_ALLOWED_BLOCKS,
            MANDATORY_VARIABLE_KEYS,
            WANNIER90_ALLOWED_CLASSES,
            optional_keys=OPTIONAL_VARIABLE_KEYS,
            )
