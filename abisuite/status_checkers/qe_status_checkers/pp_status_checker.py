from .bases import BaseQECalculationStatusChecker


class QEPPCalculationStatusChecker(BaseQECalculationStatusChecker):
    """Status checker for a pp.x calculation from Quantum Espresso.
    """
    _loggername = "QEPPCalculationStatusChecker"
