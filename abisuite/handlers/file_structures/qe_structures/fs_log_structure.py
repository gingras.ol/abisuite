from .bases import BaseQELogStructure


class QEFSLogStructure(BaseQELogStructure):
    """Structure for a fs.x log file from Quantum Espresso.
    """
    all_attributes = tuple([])
