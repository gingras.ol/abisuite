from ..bases import BaseInputFileHandler
from ...file_approvers import QEFSInputApprover
from ...file_parsers import QEFSInputParser
from ...file_structures import QEFSInputStructure
from ...file_writers import QEFSInputWriter


class QEFSInputFile(BaseInputFileHandler):
    """File handler for a Quantum Espresso fs.x input file.
    """
    _approver_class = QEFSInputApprover
    _loggername = "QEFSInputFile"
    _parser_class = QEFSInputParser
    _structure_class = QEFSInputStructure
    _writer_class = QEFSInputWriter
