import datetime

from tabulate import tabulate

from .queue_parsers import PBSQueueParser, SlurmQueueParser
from .job import QueuedJob
from .. import __USER_CONFIG__
from ..bases import BaseUtility
from ..handlers import CalculationDirectory


__QUEUING_SYSTEM_TO_QUEUE_PARSER__ = {
                "local": None,
                None: None,  # weird but it works...
                "slurm": SlurmQueueParser,
                "pbs_professional": PBSQueueParser,
                "torque": PBSQueueParser,
                }
__TIME_BETWEEN_CHECKS__ = 120  # in seconds


class Queue(BaseUtility):
    """Object that represents the queue of a queuing system.
    """
    _loggername = "Queue"

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.jobs = []
        self.njobs = 0
        self.has_been_obtained = False
        self.has_been_read_at = None
        self._username = None
        self._queue_command = None
        self.queuing_system = __USER_CONFIG__.SYSTEM.queuing_system

    def __contains__(self, item):
        if isinstance(item, QueuedJob):
            return item in self.jobs
        elif isinstance(item, str):
            # probably a path
            return item in [j.workdir for j in self.jobs]
        elif isinstance(item, CalculationDirectory):
            return item.run_directory.path in [j.workdir for j in self.jobs]
        return False

    def __enter__(self):
        if not self.has_been_obtained:
            self.read_queue()
        delta = (datetime.datetime.now() - self.has_been_read_at).seconds
        if delta > __TIME_BETWEEN_CHECKS__:
            self.read_queue()
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        pass

    def __getitem__(self, item):
        if item not in self:
            raise KeyError(item)
        if isinstance(item, CalculationDirectory):
            for job in self.jobs:
                if job.workdir == item.run_directory.path:
                    return job
        elif isinstance(item, str):
            for job in self.jobs:
                if job.workdir == item:
                    return job
        raise KeyError(item)

    @property
    def queue_parser(self):
        qp = __QUEUING_SYSTEM_TO_QUEUE_PARSER__[self.queuing_system]
        if qp is None:
            raise NotImplementedError(self.queuing_system)
        return qp

    @property
    def username(self):
        if self._username is not None:
            return self._username
        # set default username
        username = __USER_CONFIG__.SYSTEM.username
        if username is None:
            raise ValueError(
                    "username in config file is None. Set it or set a username"
                    " to the Queue object.")
        self.username = username
        return self.username

    @username.setter
    def username(self, username):
        self._username = username

    def cancel_job(self, job):
        """Cancels a running job.

        Parameters
        ----------
        job: QueuedJob instance, str or CalculationDirectory instance
            The job to cancel.
        """
        if isinstance(job, QueuedJob):
            id_ = job.id
        else:
            id_ = self[job].id
        parser = self.queue_parser(loglevel=self._loglevel)
        parser.cancel_job(id_)

    def print(self, **kwargs):  # noqa: T002
        """Print the queue to the terminal in a fancy table (using the
        tabulate module).

        Parameters
        ----------
        All kwargs are passed to the tabulate object.
        """
        tbl = self._get_table(**kwargs)
        print(tbl)  # noqa: T001

    def read_queue(self):
        """Read the queue and store queuing data.
        """
        self._logger.info("Reading system queue.")
        parser = self.queue_parser(loglevel=self._loglevel)
        parser.username = self.username
        parser.read_queue()
        self.jobs = parser.jobs
        self.njobs = len(self.jobs)
        self.has_been_read_at = datetime.datetime.now()
        self.has_been_obtained = True

    def _get_table(self, **kwargs):
        # actually create the table
        with self:
            table = [[job.id, job.jobname, job.status, job.time_left,
                      job.nodes, job.proc, job.workdir] for job in self.jobs]
        return tabulate(
                table,
                headers=["id", "jobname", "status", "time_left",
                         "nodes", "cpus", "workdir"],
                **kwargs)
