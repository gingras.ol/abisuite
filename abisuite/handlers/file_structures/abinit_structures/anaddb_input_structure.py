from ..bases import BaseInputStructure
from ....variables import ALL_ABINIT_ANADDB_VARIABLES


class AbinitAnaddbInputStructure(BaseInputStructure):
    """Structure for an abinit abaddb input file.
    """
    _variables_db = ALL_ABINIT_ANADDB_VARIABLES
