from .bases import BaseAbinitFilesStructure


class AbinitOpticFilesStructure(BaseAbinitFilesStructure):
    """Structure for an optic input file.
    """
    all_attributes = (
            "input_file_path", "output_file_path", "output_data_prefix",
            "output_data_dir",
            )
    convertible_path_attributes = (
            "input_file_path", "output_file_path", "output_data_dir",
            )
