#!/bin/bash

MPIRUN="mpirun -np 4"
EXECUTABLE="/home/fgoudreault/Workspace/abinit/abinit/build/develop/30oct2020/src/98_main/abinit"
INPUT=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_sequencers/files/abinit_AlAs_relaxation/relax_run/relax_full_cell_geometry/run/relax_full_cell_geometry.files
LOG=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_sequencers/files/abinit_AlAs_relaxation/relax_run/relax_full_cell_geometry/relax_full_cell_geometry.log
STDERR=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_sequencers/files/abinit_AlAs_relaxation/relax_run/relax_full_cell_geometry/relax_full_cell_geometry.stderr


$MPIRUN $EXECUTABLE < $INPUT > $LOG 2> $STDERR
