from ..bases import (
        BaseCalculationStatusChecker, BaseSCFCalculationStatusChecker
        )


# aliases
_cckt = "JOB DONE"
_ekt = "Error"


class BaseQECalculationStatusChecker(BaseCalculationStatusChecker):
    """Base class for Quantum Espresso Calculation Status Checker.
    """
    _calculation_completed_keyword_trigger = _cckt
    _error_keyword_trigger = _ekt


class BaseSCFQECalculationStatusChecker(BaseSCFCalculationStatusChecker):
    """Base class for Quantum Espresso SCF calculation status checker.
    """
    _calculation_completed_keyword_trigger = _cckt
    _error_keyword_trigger = _ekt
