from .bases import BaseQELauncher
from ....handlers import QELD1InputFile


class QELD1Launcher(BaseQELauncher):
    """Launcher class for a Quantum Espresso calculation using
    the ld1.x script.
    """
    _loggername = "QELD1Launcher"
    _input_file_handler_class = QELD1InputFile
    _calctype = "qe_ld1"

    def _preprocess_inputs(self, *args, **kwargs):
        # nothing to do
        return super()._preprocess_inputs(*args, **kwargs)
