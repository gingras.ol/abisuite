import os
import shutil
import tempfile


from abisuite import AbinitMassLauncher, QEPWMassLauncher
from .bases import BaseDirectoryHandlerWithPBSTest
from ..bases import TestCase
from ..routines_for_tests import copy_calculation
from ..variables_for_tests import (
        abinit_vars, abinit_AlAs_pseudos, qe_phxlog_example,
        qe_phx_vars, qe_pwx_vars, qe_pwxlog_example,
        qe_phx_output_data_dir_example, qe_pwx_output_data_dir_example,
        QEPWCalc
        )
from ...databases import AbiDB
from ...handlers import (
        CalculationDirectory, CalculationTree, OutputDataDirectory,
        GenericInputFile,  # test with generic input file
        )
from ...launchers import QEPWLauncher, QEPHLauncher
from ...linux_tools import touch, mkdir


class TestCalculationDirectory(BaseDirectoryHandlerWithPBSTest,
                               TestCase):
    _directory_handler_class = CalculationDirectory
    _directory_handler_init_args = [GenericInputFile]

    def setUp(self):
        super().setUp()
        self.handler.meta_data_file.path = os.path.join(self.handler.path,
                                                        "meta.meta")
        self.handler.input_file.path = os.path.join(self.handler.path,
                                                    "inputfile.in")
        # add a random input var
        self.handler.input_file.input_variables = {"generic_variable": 1}
        self.input_symlink_path = os.path.join(
                self.handler.input_data_directory.path, "input_symlink")
        self.input_copiedfile_path = os.path.join(
                self.handler.input_data_directory.path, "input_copied_file")
        self.handler.add_symlink(self.input_symlink_path,
                                 self._file
                                 )
        self.handler.add_copied_file(self.input_copiedfile_path,
                                     self._subfile)
        self.handler.pbs_file.path = os.path.join(
                self.handler.run_directory.path,
                os.path.basename(self.handler.pbs_file.path))
        self.handler.input_file.input_variables = {
                "generic_variable": "generic_value"}
        self.handler.meta_data_file.calctype = "calctype"
        self.handler.meta_data_file.jobname = "jobname"
        self.handler.meta_data_file.submit_time = "time"

    def test_write(self):
        super().test_write()
        self.assertTrue(os.path.isfile(self.handler.input_file.path))
        self.assertTrue(os.path.isdir(self.handler.input_data_directory.path))
        self.assertTrue(os.path.isdir(self.handler.run_directory.path))
        self.assertTrue(
                os.path.isdir(
                    self.handler.run_directory.output_data_directory.path)
                )
        self.assertTrue(os.path.islink(self.input_symlink_path))
        self.assertTrue(os.path.isfile(self.input_copiedfile_path))
        self.assertTrue(
            os.path.samefile(os.readlink(self.input_symlink_path), self._file))


class BaseCalculationManagementTest:
    _scriptname = None
    _jobname = None
    _launcher_class = None
    _input_vars = None
    _log_example = None
    _output_data_dir_example = None

    def setUp(self):
        # prepare a finished calculation in a temporary directory
        self.tempdir = tempfile.TemporaryDirectory()
        self.workdir = self.tempdir.name
        self.script_file = tempfile.NamedTemporaryFile(suffix=self._scriptname)
        # create calc
        self.launcher = self._set_launcher(
                self._launcher_class,
                self._input_vars)
        self.launcher.write()
        # create artificial log file and stderr file
        self._copy_log_stderr(self._log_example)
        # copy artificial output
        self._copy_output_dir(self._output_data_dir_example)
        # file are written, delete launcher and use calc directory now
        del self.launcher

    def tearDown(self):
        if os.path.exists(self.tempdir.name):
            self.tempdir.cleanup()
        if hasattr(self, "tempdir_new"):
            if os.path.exists(self.tempdir_new.name):
                self.tempdir_new.cleanup()

    def test_move_one_time_with_db(self):
        # create db
        db_tempdir = tempfile.TemporaryDirectory()
        db_path = os.path.join(db_tempdir.name, "test.db")
        tempdir_new = tempfile.TemporaryDirectory()
        try:
            db = AbiDB()
            db.create_database(db_path)
            db.add_calculation(self.workdir)
            oldID = db[self.workdir]
            # move calculation and check it is still inside the db
            # with the new path instead of the previous one
            calc = CalculationDirectory.from_calculation(self.workdir)
            calc.connect_to_database(database=db)
            calc.move(tempdir_new.name)
            self.assertIn(tempdir_new.name, db)
            self.assertEqual(db[oldID], tempdir_new.name)
            self.assertEqual(db[tempdir_new.name], oldID)
            self.assertNotIn(self.workdir, db)
        finally:
            tempdir_new.cleanup()
            db_tempdir.cleanup()

    def test_move_one_time(self):
        calc = CalculationDirectory.from_calculation(
                self.workdir)
        # move to a new temporary dir
        self.tempdir_new = tempfile.TemporaryDirectory()
        # move calculation
        calc.move(self.tempdir_new.name)
        # create new calc dir and check everything is kept intact
        self.calc = CalculationDirectory.from_calculation(
                self.tempdir_new.name)
        self.calc.read()
        # check meta data file
        with self.calc.meta_data_file as meta:
            self.assertEqual(meta.calc_workdir, self.tempdir_new.name)
            for path in [meta.pbs_file_path, meta.input_file_path,
                         meta.script_input_file_path]:
                self.assertTrue(os.path.isfile(path))
            for path in [meta.calc_workdir, meta.rundir, meta.output_data_dir,
                         meta.input_data_dir]:
                self.assertTrue(os.path.isdir(path), msg=f"{path}")
                self.assertTrue(path.startswith(self.tempdir_new.name))
        # check pbs file
        with self.calc.pbs_file as pbs:
            for path in [pbs.log_path, pbs.stderr_path]:
                self.assertTrue(
                        os.path.isfile(path),
                        msg=(f"Calc dir {self.tempdir_new.name} content: "
                             f"{os.listdir(self.tempdir_new.name)}, "
                             f"file checked: {path}, "
                             f"Old workdir: {self.workdir}."))
        self.assertEqual(
                self.calc, calc,
                msg=f"\n{self.calc.content}\n!=\n{calc.content}")
        self._check_input_variables()
        self._check_parents_and_children()

    def test_move_back_and_forth(self):
        calc = CalculationDirectory.from_calculation(self.workdir)
        calc_moved = CalculationDirectory.from_calculation(self.workdir)
        self.tempdir_new = tempfile.TemporaryDirectory()
        with calc_moved:
            calc_moved.move(self.tempdir_new.name)
            calc_moved.move(self.workdir)
        self.assertEqual(
                calc, calc_moved,
                msg=f"{calc}\n!=\n{calc_moved}",
                )

    def _copy_log_stderr(self, example_log):
        shutil.copy2(example_log, self.launcher.pbs_file.log_path)
        touch(self.launcher.pbs_file.stderr_path)

    def _copy_output_dir(self, output_example):
        handler = OutputDataDirectory()
        handler.path = output_example
        handler.read()
        for path in handler.walk():
            relpath = os.path.relpath(path, start=output_example)
            newpath = os.path.join(self.launcher.output_data_dir.path, relpath)
            if not os.path.isdir(os.path.dirname(newpath)):
                mkdir(os.path.dirname(newpath))
            if os.path.islink(path):
                # create new link instead of copying
                source = os.readlink(path)
                # if source is inside input data dir, relink everything
                if "input_data" in source:
                    path_inside_idd = source.split("input_data/")[-1]
                    source = os.path.join(self.launcher.input_data_dir.path,
                                          path_inside_idd)
                    # linked data in the input, it is supposed to be linked
                    # already so skip it
                    continue
                os.symlink(source, newpath,
                           target_is_directory=os.path.isdir(source))
            else:
                shutil.copy2(path, newpath)

    def _check_input_variables(self):
        pass  # by default nothing

    def _check_parents_and_children(self):
        pass

    def _set_launcher(self, launcher_cls, input_vars):
        launcher = launcher_cls(self._jobname)
        launcher.workdir = self.workdir
        launcher.input_variables = input_vars.copy()
        launcher.queuing_system = "local"
        launcher.command = self.script_file.name
        launcher.mpi_command = "mpirun -np 1"
        return launcher


# test that moving a calculation directory will update the file content
class TestQEPWCalculationManagement(BaseCalculationManagementTest,
                                    TestCase):
    _scriptname = "pw.x"
    _jobname = "pw_run"
    _launcher_class = QEPWLauncher
    _input_vars = qe_pwx_vars
    _log_example = qe_pwxlog_example
    _output_data_dir_example = qe_pwx_output_data_dir_example

    def _check_input_variables(self):
        infile = self.calc.input_file
        self.assertEqual(infile.input_variables["outdir"],
                         self.calc.output_data_directory.path)


class TestQEPHCalculationManagement(BaseCalculationManagementTest,
                                    TestCase):
    _scriptname = "ph.x"
    _jobname = "ph_run"
    _launcher_class = QEPHLauncher
    _input_vars = qe_phx_vars
    _log_example = qe_phxlog_example
    _output_data_dir_example = qe_phx_output_data_dir_example

    def test_moving_parent(self):
        parent_calc = CalculationDirectory.from_calculation(
                self.linked_calculation)
        # move to a new temporary dir
        self.tempdir_new = tempfile.TemporaryDirectory()
        # move PARENT calculation
        parent_calc.move(self.tempdir_new.name)
        # create calc dir and check everything is kept intact
        self.calc = CalculationDirectory.from_calculation(
                self.tempdir.name)
        # check that all linked files and copied files correctly points to
        # something
        with self.calc.meta_data_file as meta:
            for copied in meta.copied_files:
                self.assertTrue(os.path.isfile(copied))
            for linked in meta.linked_files:
                self.assertTrue(os.path.isfile(linked))
        # now look in input dir for this link and make sure it points
        # to the same file
        for path in self.calc.input_data_directory.walk(paths_only=True):
            if os.path.islink(path):
                # check that all links are unbroken
                readlink = os.readlink(path)
                exists = os.path.exists(readlink)
                self.assertTrue(exists, msg=f"'{readlink}' doesn't exists...")

    def _check_input_variables(self):
        infile = self.calc.input_file
        self.assertEqual(infile.input_variables["outdir"],
                         self.calc.output_data_directory.path)
        for i in range(4):
            # there is 4 dynfiles
            dyn = infile.input_variables["fildyn"].value + str(i)
            self.assertTrue(
                    os.path.isfile(dyn),
                    msg=f"{dyn} not found in {self.calc.output_data_directory}"
                    )

    def _check_parents_and_children(self):
        # check the PWCalc has changed its child
        parent_calc = CalculationDirectory.from_calculation(
                self.linked_calculation)
        parent_meta = parent_calc.meta_data_file
        childpath = parent_meta.children[-1]  # supposed to be the last one...
        self.assertEqual(childpath, self.calc.path)
        # check the calc parent points to the same place
        parentpath = self.calc.meta_data_file.parents[0]
        self.assertEqual(parentpath, parent_meta.calc_workdir)

    def _set_launcher(self, *args, **kwargs):
        launcher = super()._set_launcher(*args, **kwargs)
        self.linked_calculation = self._copy_calc_to_link(QEPWCalc)
        launcher.link_calculation(self.linked_calculation)
        return launcher

    def _copy_calc_to_link(self, calc_to_link):
        # need to modify meta files because meta file are LOCAL
        # do this in a temporary directory to not modify file inplace
        self._link_tempdir = tempfile.TemporaryDirectory()
        whereto = os.path.join(self._link_tempdir.name, "calc")
        copy_calculation(calc_to_link, whereto)
        return whereto


class TestCalculationTree(TestCase):
    def setUp(self):
        # create two different series of random calculations
        # tempdir and temp command file
        self.top_dir = tempfile.TemporaryDirectory()
        self.abinit_command_file = tempfile.NamedTemporaryFile(suffix="abinit")
        self.qepw_command_file = tempfile.NamedTemporaryFile(suffix="pw.x")
        # first one
        self.njobs1 = 5
        self.launcher1 = AbinitMassLauncher([f"abinit{i}"
                                             for i in range(self.njobs1)])
        self.launcher1.workdir = os.path.join(self.top_dir.name, "abinit")
        self.launcher1.command = self.abinit_command_file.name
        self.launcher1.common_input_variables = abinit_vars.copy()
        self.launcher1.specific_input_variables = [{}] * self.njobs1
        self.launcher1.queuing_system = "local"
        self.launcher1.common_pseudos = abinit_AlAs_pseudos
        self.launcher1.write()
        # second job
        self.njobs2 = 3
        self.launcher2 = QEPWMassLauncher([f"qepw{i}"
                                           for i in range(self.njobs2)])
        self.launcher2.workdir = os.path.join(self.top_dir.name, "qepw")
        self.launcher2.command = self.qepw_command_file.name
        self.launcher2.common_input_variables = qe_pwx_vars.copy()
        self.launcher2.specific_input_variables = [{}] * self.njobs2
        self.launcher2.queuing_system = "local"
        self.launcher2.write()
        # create tree
        self.tree = CalculationTree(loglevel=1)
        self.tree.path = self.top_dir.name

    def tearDown(self):
        if os.path.isdir(self.top_dir.name):
            self.top_dir.cleanup()
            del self.top_dir
        self.abinit_command_file.close()
        self.qepw_command_file.close()

    def test_read(self):
        self.tree.build_tree()
        self.assertEqual(len(self.tree.calculations),
                         self.njobs1 + self.njobs2)
        self.assertEqual(len(self.tree.content), 2)
        self.assertIn("abinit", self.tree)
        self.assertIn("qepw", self.tree)

    def test_status(self):
        self.tree.read()
        for calcstatus in self.tree.status:
            self.assertFalse(calcstatus["calculation_started"])
            self.assertFalse(calcstatus["calculation_finished"])
            self.assertFalse(calcstatus["calculation_converged"])
