from .bases import BaseAbisuiteSubScript
from ..colors import Colors
from ..launchers import __ALL_LAUNCHERS__


class AbisuiteRelaunchScript(BaseAbisuiteSubScript):
    """Executes the 'relaunch' subcommand of the abisuite script."""
    _loggername = "AbisuiteRelaunchScript"

    # TODO: split this method to make it shorter and more readable
    # (FG: 2021/10/04)
    def relaunch(
            self,
            build=None,
            command_arguments=None,
            dry=False,
            input_variables_pop=None,
            input_variables_update=None,
            jobname=None,
            memory_per_cpu=None,
            mpi_command=None,
            nodes=None,
            ntasks=None,
            ppn=None,
            project_account=None,
            queuing_system=None,
            walltime=None,
            ):
        """Relaunches the calculations stored in the 'calculations' attribute.

        Parameters
        ----------
        build: str, optional
            If not None, will relaunch the calculations using this build
            defined in the user's config file.
        command_arguments: str, optional
            If not None, relaunches calculations using these command arguments.
        dry: bool, optional
            If True, dry mode is activated and only what will be relaunched
            will be printed on screen.
        input_variables_pop: list-like, optional
            If not None, gives the list of input variables to pop when
            relaunching the calculations.
        input_variables_update: dict, optional
            If not None, gives the dict of input variables that updates the
            input variables already there when relaunching calculations.
        jobname: str, optional
            If not None, states the new jobname for the job.
        memory_per_cpu: str, optional
            If not None, will relaunch the calculations using this memory
            per cpu attribute.
        mpi_command: str, optional
            If not None, will relaunch calculations using this mpi command.
        nodes: int, optional
            If not None, will relaunch calculations using this number of nodes.
            Is not compatible with the ntasks argument.
        ntasks: int, optional
            If not None, will relaunch calculations using this number of tasks.
            Not compatible with the nodes+ppn arguments.
        ppn: int, optional
            If not None, will relaunch calculations using this number of tasks
            per nodes. Not compatible with the ntasks argument.
        project_account: str, optional
            If not None, relaunch the calculations using this project account.
        queuing_system: str, optional
            If not None, will relaunch calculations using this queuing system.
            Usefull when relaunching in a 'local' way.
        walltime: str, optional
            If not None, will relaunch calculations using this walltime.
        """
        for calculation in self.calculations:
            # relaunch a calculation
            # get the real full_abspath from the meta data file
            # in case user has shortcuts or different storages (e.g.: beluga)
            with calculation as calc:
                if dry:
                    print(Colors.bold_text("Will relaunch:") +
                          f" '{calc.path}'.")
                    continue
                else:
                    print(Colors.bold_text("Relaunching") +
                          f": '{calc.path}'")
                # cancel job if needed
                self.cancel_job(calc)
                launcher_cls = __ALL_LAUNCHERS__[calc.calctype]
                launcher = launcher_cls(calc.jobname, loglevel=self._loglevel)
                launcher.calculation_directory = calc
                prt = "Relaunching with:"
                if project_account is not None:
                    with calc.pbs_file as pbs:
                        pbs.project_account = project_account
                        prt += f" 'project_account'={project_account}'"
                if memory_per_cpu is not None:
                    with calc.pbs_file as pbs:
                        prt += (" 'memory_per_cpu="
                                f"{memory_per_cpu}'")
                        pbs.memory_per_cpu = memory_per_cpu
                if build is not None:
                    launcher.build = build
                    launcher.pbs_file.write(overwrite=True)
                    prt += f" 'build'={build}"
                if jobname is not None:
                    with calc.pbs_file as pbs:
                        pbs.jobname = jobname
                    prt += (f" 'jobname'={jobname}")
                if mpi_command is not None:
                    with calc.pbs_file as pbs:
                        prt += (f" 'mpi_command={mpi_command}'")
                        pbs.mpi_command = mpi_command
                if ntasks is not None:
                    with calc.pbs_file as pbs:
                        prt += (f" 'ntasks'={ntasks}")
                        pbs.ntasks = int(ntasks)
                if nodes is not None:
                    with calc.pbs_file as pbs:
                        prt += (f" 'nodes'={nodes}")
                        prt += (f" 'ppn'={ppn}")
                        pbs.nodes = int(nodes)
                        pbs.ppn = int(ppn)
                        pbs.ntasks = None
                if command_arguments is not None:
                    with calc.pbs_file as pbs:
                        prt += (" 'command_arguments'="
                                f"{command_arguments}")
                        pbs.command_arguments = command_arguments
                if queuing_system is not None:
                    with calc.pbs_file as pbs:
                        if pbs.queuing_system == "local":
                            # need to supply some more info
                            if project_account is None:
                                raise ValueError(
                                        "Need to set 'project_account'.")
                            if jobname is None:
                                raise ValueError("Need to set 'jobname'.")
                            if walltime is None:
                                raise ValueError("Need to set 'walltime'.")
                            if memory_per_cpu is None:
                                raise ValueError(
                                        "Need to set 'memory'.")
                            if ntasks is None and ppn is None:
                                raise ValueError(
                                        "Need to set 'ntasks' or 'ppn+nodes'.")
                        if project_account is not None:
                            pbs.project_account = project_account
                        if jobname is not None:
                            pbs.jobname = jobname
                        if walltime is not None:
                            pbs.walltime = walltime
                        if ntasks is not None:
                            pbs.ntasks = ntasks
                        if memory_per_cpu is not None:
                            pbs.memory_per_cpu = memory_per_cpu
                        pbs.queuing_system = queuing_system
                    prt += f" 'queuing_system'={queuing_system}"
                if walltime is not None:
                    with calc.pbs_file as pbs:
                        prt += f" 'walltime'={walltime}"
                        pbs.walltime = walltime
                if input_variables_update is not None:
                    with calc.input_file as inp:
                        try:
                            inp.input_variables.update(input_variables_update)
                        except ValueError as err:
                            self.error("while updating variables.")
                            launcher._logger.exception(err)
                            continue
                    prt += (" updated input variables = "
                            f"{input_variables_update}")
                if input_variables_pop is not None:
                    with calc.input_file as inp:
                        for var in input_variables_pop:
                            if var not in inp.input_variables:
                                continue
                            inp.input_variables.pop(var)
                    prt += f" popped input variables = {input_variables_pop}"
                options = (
                        build,
                        command_arguments,
                        input_variables_pop,
                        input_variables_update,
                        jobname,
                        memory_per_cpu,
                        mpi_command,
                        nodes,
                        ntasks,
                        ppn,
                        queuing_system,
                        walltime,
                        )
                if any([x is not None for x in options]):
                    print(prt)
            try:
                if any([input_variables_update is not None,
                        input_variables_pop is not None]):
                    # if we use abinit we need to define pseudo potential files
                    if calc.calctype == "abinit":
                        from abisuite.handlers import AbinitFilesFile
                        with AbinitFilesFile.from_calculation(
                                calc, loglevel=self._loglevel) as files:
                            launcher.pseudos = files.pseudos
                    from abisuite.handlers.file_approvers.exceptions import (
                            InputFileError,
                            )
                    try:
                        # only validate input file
                        launcher.input_file.validate()
                    except InputFileError as err:
                        self.error("while validating input file.")
                        launcher._logger.exception(err)
                        continue
                self._clear_files_before_relaunch(calc)
                launcher.launch()
            except Exception as e:
                self.error(f"while relaunching: '{calc.path}'.")
                launcher._logger.exception(e)
            else:
                self.successfull(f"relaunch '{calc.path}'.")
            calculation.clear_content()

    def _clear_files_before_relaunch(self, calc):
        # if a log file is present, delete it
        if calc.log_file.exists:
            calc.log_file.bump_copy()
        # also delete stderr file
        if calc.stderr_file.exists:
            calc.stderr_file.bump_copy()
        for handler in calc.walk(paths_only=False):
            # delete core files if there are any
            if handler.basename.startswith("core."):
                handler.delete()
            # delete CRASH file if it exists
            if handler.basename == "CRASH":
                handler.delete()
