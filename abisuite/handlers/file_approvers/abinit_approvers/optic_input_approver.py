from ..bases import BaseInputApprover, BaseInputParalApprover
from ..exceptions import InputFileError
from ...file_structures import AbinitOpticInputStructure
from ....variables import ALL_ABINITOPTIC_VARIABLES


class AbinitOpticInputApprover(BaseInputApprover):
    """Optic input file approver (TODO: not implemented yet.)
    """
    _loggername = "AbinitOpticInputApprover"
    _exception = InputFileError
    _structure_class = AbinitOpticInputStructure
    _variables_db = ALL_ABINITOPTIC_VARIABLES


# TODO: implement this class also.
class AbinitOpticInputParalApprover(BaseInputParalApprover):
    """Class that compares the parallelization variables of optic with the
    mpi settings.
    """
    _loggername = "AbinitOpticParalApprover"
    _input_approver_class = AbinitOpticInputApprover
    _exceptions = InputFileError
    _structure_class = AbinitOpticInputStructure

    def validate(self):
        super().validate()
        self._logger.warning("Optic paral input file approver not"
                             " implemented yet.")
