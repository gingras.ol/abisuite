from ..bases import BaseFileHandler
from ...file_parsers import QEEPWSpecFunPhonParser
from ...file_structures import QEEPWSpecFunPhonStructure


class QEEPWSpecFunPhonFile(BaseFileHandler):
    """Class that represents a phonon spectral function file produced by a
    epw.x calculation from Quantum Espresso.
    """
    _loggername = "QEEPWSpecFunPhonFile"
    _parser_class = QEEPWSpecFunPhonParser
    _structure_class = QEEPWSpecFunPhonStructure
