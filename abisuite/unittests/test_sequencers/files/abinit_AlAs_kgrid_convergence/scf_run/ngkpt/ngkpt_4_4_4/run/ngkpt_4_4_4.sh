#!/bin/bash

MPIRUN="mpirun -np 4"
EXECUTABLE="/home/fgoudreault/Workspace/abinit/abinit/build/develop/2021-04-16/src/98_main/abinit"
INPUT=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_sequencers/files/abinit_AlAs_kgrid_convergence/scf_run/ngkpt/ngkpt_4_4_4/run/ngkpt_4_4_4.files
LOG=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_sequencers/files/abinit_AlAs_kgrid_convergence/scf_run/ngkpt/ngkpt_4_4_4/ngkpt_4_4_4.log
STDERR=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_sequencers/files/abinit_AlAs_kgrid_convergence/scf_run/ngkpt/ngkpt_4_4_4/ngkpt_4_4_4.stderr


$MPIRUN $EXECUTABLE < $INPUT > $LOG 2> $STDERR
