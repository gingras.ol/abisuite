from .scf_sequencer import AbinitSCFSequencer
from ..nscf_sequencers import NSCFSequencer


class AbinitNSCFSequencer(NSCFSequencer, AbinitSCFSequencer):
    """NSCF sequencer for abinit calculations.
    """
    _all_sequencer_prefixes = ("scf_", "nscf_", )
    _loggername = "AbinitNSCFSequencer"

    def __init__(self, *args, **kwargs):
        AbinitSCFSequencer.__init__(self, *args, **kwargs)
        NSCFSequencer.__init__(self, "abinit", *args, **kwargs)
        self._nscf_link_density_from_scf = False
        self._nscf_load_geometry_from_scf = False

    @property
    def nscf_link_density_from_scf(self):
        return self._nscf_link_density_from_scf

    @nscf_link_density_from_scf.setter
    def nscf_link_density_from_scf(self, link):
        if link not in (True, False, 1, 0):
            raise ValueError(link)
        self._nscf_link_density_from_scf = link

    @property
    def nscf_load_geometry_from_scf(self):
        return self._nscf_load_geometry_from_scf

    @nscf_load_geometry_from_scf.setter
    def nscf_load_geometry_from_scf(self, load):
        if load not in (True, False, 1, 0):
            raise ValueError(load)
        self._nscf_load_geometry_from_scf = load

    def init_sequence(self):
        NSCFSequencer.init_sequence(self)

    def init_nscf_sequence(self):
        NSCFSequencer.init_nscf_sequence(self)
        nscfcalc = self.get_sequence_calculation("nscf")
        scfcalc = self.get_sequence_calculation("scf")
        if nscfcalc.input_variables.get("irdden", 1) == 1 or (
                self.nscf_link_density_from_scf):
            nscfcalc.link_density_from = scfcalc
            nscfcalc.dependencies.append(scfcalc)
        if self.nscf_load_geometry_from_scf:
            nscfcalc.load_geometry_from = scfcalc
            nscfcalc.dependencies.append(scfcalc)
