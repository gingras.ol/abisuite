from ..bases import BaseLinker


class AbinitfromAbinitLinker(BaseLinker):
    """Linker to link an abinit calculation to another one.
    """
    _calctype = "abinit"
    _link_calctype = "abinit"
    _loggername = "AbinitfromAbinitLinker"

    def _link_files_from_calculation(self, *args, **kwargs):
        # link the files from one calculation to the other.
        # the files to link will depend on the input variables
        irdwfk = self.input_variables.get("irdwfk", 0).value
        irdddk = self.input_variables.get("irdddk", 0).value
        irdden = self.input_variables.get("irdden", 0).value
        if irdwfk != 0:
            # link wfk files. usually they are stored in the output data dir
            self.launcher.link_wfk_from(self.calculation_to_link.path)
        if irdddk != 0:
            # link ddk files. usually they are stored in the output data dir
            self.launcher.link_ddk_from(self.calculation_to_link.path)
        if irdden != 0:
            # link DEN file.
            self.launcher.link_den_from(self.calculation_to_link.path)
