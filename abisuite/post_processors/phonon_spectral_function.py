import os

import numpy as np

from .bases import BasePostProcClass
from ..constants import mEV_TO_EV
from ..handlers import QEEPWPhononSelfEnergyFile, QEEPWSpecFunPhonFile
from ..routines import is_list_like, is_2d_array


class PhononSpectralFunction(BasePostProcClass):
    """Class that process and plot phonon spectral functions.
    """
    _loggername = "PhononSpectralFunction"

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._energies = None
        self._nqpts = None
        self._spectral_function = None
        self._eigenvalues = None

    @property
    def eigenvalues(self):
        return self._eigenvalues

    @eigenvalues.setter
    def eigenvalues(self, eigs):
        if self._spectral_function is not None:
            if self.spectral_function.shape[0] != len(eigs):
                raise AssertionError(
                        f"Len of eigs ({len(eigs)}) don't match the size of "
                        f"the spectral function ("
                        f"{self.spectral_function.shape[0]}).")
        self._eigenvalues = eigs

    @property
    def energies(self):
        if self._energies is not None:
            return self._energies
        raise ValueError("Need to set 'energies'.")

    @energies.setter
    def energies(self, energies):
        if not is_list_like(energies):
            raise TypeError("Energies should be 'list-like'.")
        # check consistency
        if self._spectral_function is not None:
            if len(energies) != self.spectral_function.shape[-1]:
                raise AssertionError(
                        f"Len of energies ({len(energies)} doesn't match the "
                        f"size of the spectral function ("
                        f"{self.spectral_function.shape[-1]}).")
        self._energies = energies

    @property
    def nqpts(self):
        return self._nqpts

    @property
    def spectral_function(self):
        if self._spectral_function is not None:
            return self._spectral_function
        raise ValueError("Need to set 'spectral_function'.")

    @spectral_function.setter
    def spectral_function(self, spec):
        if not is_2d_array(spec):
            raise TypeError(
                    "The spectral function should be a 2D array "
                    "(nqpts x nfreqs).")
        if self._energies is not None:
            if len(self.energies) != spec.shape[-1]:
                raise AssertionError(
                        f"Size of spectral function ({spec.shape[-1]}) does "
                        f"not match the length of energies ("
                        f"{len(self.energies)}).")
        self._spectral_function = spec
        self._nqpts = spec.shape[0]

    def get_plot(
            self, *args, plot_dispersion=False, dispersion_color="k",
            dispersion_linewidth=1,
            **kwargs):
        """Get the spectral function plot.

        Parameters
        ----------
        plot_dispersion : bool, optional
            If True and the eigenvalues are stored in the SpectalFunction
            object, then the dispersion is added on top.
        dispersion_color : str, optional
            The color of the phonon dispersion in case it is plotted.
        dispersion_linewidth : float, optional
            The linewidth of the dispersion curves in case they are plotted.
        """
        kw = {}
        kw["xlabel"] = kwargs.pop("xlabel", "")
        kw["xunits"] = kwargs.pop("xunits", None)
        kw["ylabel"] = kwargs.pop("ylabel", "")
        kw["yunits"] = kwargs.pop("yunits", None)
        kw["title"] = kwargs.pop("title", "")
        plot = super().get_plot(*args, **kw)
        # xdata is just range(0, nqpts)
        plot.add_image(
                list(range(0, self.nqpts)),
                self.energies,
                self.spectral_function.T, **kwargs)
        if self.eigenvalues is not None and plot_dispersion:
            for branch in self.eigenvalues.T:
                plot.add_curve(
                        list(range(len(branch))), branch,
                        color=dispersion_color,
                        linewidth=dispersion_linewidth)
        return plot

    @classmethod
    def from_file(cls, path, *args, **kwargs):
        if not os.path.isfile(path):
            raise FileNotFoundError(path)
        if os.path.basename(path) == "specfun.phon":
            return cls.from_qe_epw_spec_fun_phon_file(path, *args, **kwargs)
        elif os.path.basename(path) == "specfun_sup.phon":
            return cls.from_qe_epw_phonon_self_energy_file(
                    path, *args, **kwargs)
        # else not implemented
        raise NotImplementedError(path)

    @classmethod
    def from_handler(
            cls, handler, *args, broadening=0, turn_off_self_energy=False,
            **kwargs):
        """Create a PhononSpectralFunction object from a file handler.

        Parameters
        ----------
        broadening : float, optional
            Artificial broadening of the spectral function (in eV).
        turn_off_self_energy : bool, optional
            If True, computes spectral function without self energy
            (useful for debugging).
        """
        instance = cls(*args, **kwargs)
        with handler:
            if isinstance(handler, QEEPWPhononSelfEnergyFile):
                # the spectral function is nqpts x nfreq
                # but self energy depends of the
                # modes. we sum at the end on the modes
                # self energy is nqpts x nmodes x nfreqs
                instance._logger.info(
                        "Computing spectral functions from self energy.")
                self_energies = handler.self_energy
                spectral_functions = []
                freqs = handler.energies
                for iq in range(self_energies.shape[0]):
                    specs_this_q = []
                    for ibranch in range(self_energies.shape[1]):
                        wqv = handler.eigenvalues[iq, ibranch]
                        # G_qv = 2wqv / (w^2 - wqv^2 + 2iwqv x broad)
                        denom = freqs ** 2 - wqv ** 2 + 2.0j * wqv * broadening
                        if not turn_off_self_energy:
                            # self energy units are in meV
                            # while frequencies are in eV
                            denom -= (2 * wqv * self_energies[iq, ibranch] *
                                      mEV_TO_EV)
                        denom[denom == 0.0] = broadening
                        # don't divide by 0 plz
                        gf = np.divide(2 * wqv, denom)
                        if not ibranch:
                            specs_this_q.append(-np.imag(gf) / np.pi)
                        else:
                            specs_this_q[-1] += -np.imag(gf) / np.pi
                    spectral_functions.append(np.array(specs_this_q[0]))
                instance.spectral_function = np.array(spectral_functions)
                instance.eigenvalues = handler.eigenvalues
            else:
                instance.spectral_function = handler.spectral_function
            instance.energies = handler.energies
        return instance

    @classmethod
    def from_qe_epw_spec_fun_phon_file(cls, path, *args, **kwargs):
        with QEEPWSpecFunPhonFile.from_file(path) as spec:
            return cls.from_handler(spec, *args, **kwargs)

    @classmethod
    def from_qe_epw_phonon_self_energy_file(cls, path, *args, **kwargs):
        """Extract phonon spectral function from a phonon self energy file
        produced by the epw.x script from Quantum Espresso.

        Parameters
        ----------
        path : str
            The path to the file.
        """
        with QEEPWPhononSelfEnergyFile.from_file(path) as selfe:
            return cls.from_handler(selfe, *args, **kwargs)
