from .bases import BasePackagerTest
from ..bases import TestCase
from ..variables_for_tests import (
        QEEPWCalc, QEEPWCalc_IBTE, QEMatdynCalc, QEPHCalc, QEPWCalc, QEQ2RCalc,
        )
from ...packagers import (
        QEEPWPackager, QEMatdynPackager, QEPHPackager, QEPWPackager,
        QEQ2RPackager,
        )


class TestQEEPWPackager(BasePackagerTest, TestCase):
    """Test case for the QEEPWPackager object.
    """
    _example = QEEPWCalc
    _packager_cls = QEEPWPackager

    def test_package_ibte(self):
        """Extra test to package an IBTE calculation to make sure it works.
        """
        super().test_package(calc_example=QEEPWCalc_IBTE)


class TestQEMatdynPackager(BasePackagerTest, TestCase):
    """Test case for the QEMatdynPackager object.
    """
    _example = QEMatdynCalc
    _packager_cls = QEMatdynPackager


class TestQEPHPackager(BasePackagerTest, TestCase):
    """Test case for the QEPHPackager object.
    """
    _example = QEPHCalc
    _packager_cls = QEPHPackager


class TestQEPWPackager(BasePackagerTest, TestCase):
    """Test case for the QEPWPackager object.
    """
    _example = QEPWCalc
    _packager_cls = QEPWPackager


class TestQEQ2RPackager(BasePackagerTest, TestCase):
    _example = QEQ2RCalc
    _packager_cls = QEQ2RPackager
