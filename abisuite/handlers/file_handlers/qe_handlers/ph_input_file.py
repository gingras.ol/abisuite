from ..bases import BaseInputFileHandler
from ...file_approvers import QEPHInputApprover
from ...file_parsers import QEPHInputParser
from ...file_structures import QEPHInputStructure
from ...file_writers import QEPHInputWriter


class QEPHInputFile(BaseInputFileHandler):
    """File handler for a Quantum Espresso ph.x input file.
    """
    _approver_class = QEPHInputApprover
    _loggername = "QEPHInputFile"
    _parser_class = QEPHInputParser
    _structure_class = QEPHInputStructure
    _writer_class = QEPHInputWriter
