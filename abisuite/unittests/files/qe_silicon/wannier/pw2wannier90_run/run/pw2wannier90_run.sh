#!/bin/bash

MPIRUN="mpirun -np 4"
EXECUTABLE="/home/fgoudreault/Workspace/q-e/bin/pw2wannier90.x"
INPUT=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/files/qe_silicon/wannier/pw2wannier90_run/pw2wannier90_run.in
LOG=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/files/qe_silicon/wannier/pw2wannier90_run/pw2wannier90_run.log
STDERR=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/files/qe_silicon/wannier/pw2wannier90_run/pw2wannier90_run.stderr


$MPIRUN $EXECUTABLE < $INPUT > $LOG 2> $STDERR
