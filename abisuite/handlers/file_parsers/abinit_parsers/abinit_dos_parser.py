import os

import numpy as np

from .bases import BaseAbinitDOSParser
from ...file_structures import AbinitDOSStructure


class AbinitDOSParser(BaseAbinitDOSParser):
    """Parser class for a _DOS file produced by the Abinit software.
    """
    _expected_ending = ("_DOS", "_DOS_TOTAL")
    _loggername = "AbinitDOSParser"
    _structure_class = AbinitDOSStructure

    @classmethod
    def _filepath_from_meta(cls, meta):
        """Returns the file path from the meta data file handler.
        """
        # dos plot is located inside output data dir
        with meta:
            out = meta.output_data_dir
        for filename in os.listdir(out):
            for ending in cls._expected_ending:
                if filename.endswith(ending):
                    return os.path.join(out, filename)
        raise FileNotFoundError(f"Couldn't find the DOS file in '{out}'.")

    def _get_core_data(self):
        # data is organized in 3 columns:
        # energies - DOS - integrated DOS
        data = np.loadtxt(self.path)
        return {"energies": data[:, 0], "dos": data[:, 1],
                "integrated_dos": data[:, 2],
                }
