import logging
import os
import tempfile
import unittest

from .bases import BaseLauncherTest, BaseLauncherWithParentsTest
from ..variables_for_tests import (
        qe_dosx_vars,
        qe_dynmatx_vars,
        qe_epsilonx_vars, qe_epwx_vars, qe_fsx_vars, qe_ld1x_vars,
        qe_matdynx_vars,
        qe_phx_vars, qe_ppx_vars, qe_projwfcx_vars,
        qe_pwx_vars,
        qe_q2rx_vars,
        # pseudo_dir,
        POSITIVE_KS_NSCF_CALC,
        QEPHCalc,
        QEPWCalc,
        QEQ2RCalc
        )
from ...handlers.file_approvers.exceptions import InputFileError
from ...handlers import MetaDataFile
from ...launchers import (
        QEDOSLauncher,
        QEDynmatLauncher,
        QEEpsilonLauncher, QEEPWLauncher, QEFSLauncher, QELD1Launcher,
        QEMatdynLauncher,
        QEPHLauncher, QEPPLauncher, QEProjwfcLauncher,
        QEPWLauncher,
        QEQ2RLauncher
        )


class BaseQELauncherTest(BaseLauncherTest):
    """Class for QE Launchers unittests.
    """
    def test_recover(self):
        # check that after writing launcher, one can recover it
        # test for input variables and batch file parameters
        self.launcher.write()
        newlauncher = self._launcher_class(self._jobname)
        # use same workdir for simplicity with comparisions
        newlauncher.workdir = self.workdir
        newlauncher.recover_from(self.launcher.workdir)
        self.assertEqual(self.launcher.input_file,
                         newlauncher.input_file)
        self.assertEqual(self.launcher.pbs_file,
                         newlauncher.pbs_file,
                         msg=(f"{self.launcher.pbs_file} != "
                              f"{newlauncher.pbs_file}"))


class BaseRequiredInputQELauncherTest(
        BaseQELauncherTest, BaseLauncherWithParentsTest):
    """Class that adds tests for Launchers that require some input files.
    """
    def test_raise_when_no_inputdata(self):
        self.launcher.clear_input_files()
        with self.assertRaises(FileNotFoundError):
            self.launcher.validate()


class TestQEDOSLauncher(BaseRequiredInputQELauncherTest, unittest.TestCase):
    _calctype = "dos.x"
    _input_vars = qe_dosx_vars.copy()
    _jobname = "test_qedos_launcher"
    _launcher_class = QEDOSLauncher
    _link_calculation = QEPWCalc
    _mpi_command = ""
    _nodes = 1
    _ppn = 1


class TestQEDynmatLauncher(BaseRequiredInputQELauncherTest, unittest.TestCase):
    _calctype = "dynmat.x"
    _input_vars = qe_dynmatx_vars.copy()
    _jobname = "test_qedynmat_launcher"
    _launcher_class = QEDynmatLauncher
    _link_calculation = QEPHCalc


class TestQEEpsilonLauncher(BaseRequiredInputQELauncherTest,
                            unittest.TestCase):
    _calctype = "epsilon.x"
    _input_vars = qe_epsilonx_vars.copy()
    _jobname = "test_qeepsilon_launcher"
    _launcher_class = QEEpsilonLauncher
    _link_calculation = QEPWCalc


class TestQEEPWLauncher(BaseRequiredInputQELauncherTest,
                        unittest.TestCase):
    _calctype = "epw.x"
    _input_vars = qe_epwx_vars.copy()
    _jobname = "test_qeepw_launcher"
    _launcher_class = QEEPWLauncher
    _link_calculation = [QEPHCalc, POSITIVE_KS_NSCF_CALC]


class TestQEFSLauncher(BaseRequiredInputQELauncherTest,
                       unittest.TestCase):
    _calctype = "fs.x"
    _input_vars = qe_fsx_vars.copy()
    _jobname = "test_qefs_launcher"
    _launcher_class = QEFSLauncher
    _link_calculation = QEPWCalc


class TestQELD1Launcher(BaseQELauncherTest, unittest.TestCase):
    _calctype = "ld1.x"
    _input_vars = qe_ld1x_vars.copy()
    _jobname = "test_qeld1_launcher"
    _launcher_class = QELD1Launcher


class TestQEMatdynLauncher(BaseRequiredInputQELauncherTest, unittest.TestCase):
    _calctype = "matdyn.x"
    _input_vars = qe_matdynx_vars.copy()
    _jobname = "test_matdyn_launcher"
    _launcher_class = QEMatdynLauncher
    _link_calculation = QEQ2RCalc


class TestQEPHLauncher(BaseRequiredInputQELauncherTest, unittest.TestCase):
    _calctype = "ph.x"
    _input_vars = qe_phx_vars.copy()
    _jobname = "test_qeph_launcher"
    _launcher_class = QEPHLauncher
    _link_calculation = QEPWCalc

    def test_recover(self):
        # recover test is done using another class
        pass


# simpler to make a new test class to test recovery
class TestQEPHLauncher_Recover(
        BaseQELauncherTest, BaseLauncherWithParentsTest, unittest.TestCase):
    _calctype = "ph.x"
    _jobname = "test_qephrecover_launcher"
    _launcher_class = QEPHLauncher
    _link_calculation = [QEPHCalc]  # not PWCalc

    def setUp(self):
        # same as base class
        self.command_file = tempfile.NamedTemporaryFile(suffix=self._calctype)
        self.command = self.command_file.name
        self.tempdir = tempfile.TemporaryDirectory()
        self.workdir = os.path.join(self.tempdir.name, self._jobname)
        self.launcher = self._launcher_class(self._jobname,
                                             loglevel=logging.DEBUG)
        self.launcher.workdir = self.workdir
        self._recover_from = self._copy_calc_to_link(
                self._link_calculation)[0]
        self._linked_calc = self._recover_from
        self.launcher.recover_from(self._recover_from)
        # hack needed to force executable path
        self.launcher.pbs_file.structure._command = self.command

    def test_linked_calculation(self):
        # not really linking a calculation, just recovering from it
        # i'm using the already existing machinery for this unittest
        pass

    def test_clear_calculations(self):
        # not really linking calculations either
        pass

    def test_linking_against_database(self):
        # same as above
        pass

    def test_recover(self):
        # check that recover has been inserted
        self.assertIs(self.launcher.input_variables["recover"].value, True)
        # check that input variables are the same
        self.assertEqual(self.launcher.input_variables["amass(1)"].value,
                         28.086)  # directly read from corresponding input
        # check prefix is new prefix
        self.assertEqual(self.launcher.input_variables["prefix"].value,
                         self.launcher.jobname)
        # check cmd args are the same
        self.assertDictEqual(self.launcher.mpi_command_arguments,
                             {"-np": 4})
        self.assertDictEqual(self.launcher.command_arguments,
                             {"-npool": 4})

    @unittest.skipIf(os.cpu_count() < 4, "Not enough cpu for this test.")
    def test_clean_workdir(self):
        # skip if not enough proc (e.g. on gitlab)
        super().test_clean_workdir()

    @unittest.skipIf(os.cpu_count() < 4, "Not enough cpu for this test.")
    def test_raise_when_changing_npool_or_nimage(self):
        # force one mpi process
        self.launcher.command_arguments["-npool"] += 1
        with self.assertRaises((ValueError, InputFileError)):
            self.launcher.validate()
        self.launcher.command_arguments["-npool"] -= 1
        self.launcher.command_arguments["-nimage"] = 2
        with self.assertRaises((ValueError, InputFileError)):
            self.launcher.validate()

    @unittest.skipIf(os.cpu_count() < 4, "Not enough cpu for this test.")
    def test_launcher_write(self):
        # skip if not enough proc (e.g. on gitlab)
        super().test_launcher_write()

    @unittest.skipIf(os.cpu_count() < 4, "Not enough cpu for this test.")
    def test_link_raise(self):
        super().test_link_raise()

    def test_basic_properties(self):
        # just skip this test for now as it is not relevant
        # TODO: try to find a cleaner way to do this...
        #    pass
        self._modules = []
        self._ppn = None
        self._nodes = None
        self.command += " -npool 4"
        self._mpi_command += " -np 4"
        with MetaDataFile.from_calculation(self._recover_from) as meta:
            ifcls = self._launcher_class._input_file_handler_class
            with ifcls.from_meta_data_file(meta) as inputfile:
                inputfile.read()
                self.input_variables = inputfile.input_variables.todict()
        super().test_basic_properties()

    @unittest.skipIf(os.cpu_count() < 4, "Not enough cpu for this test.")
    def test_raise_when_no_variables(self):
        # skip if not enough poc (e.g. on gitlab)
        super().test_raise_when_no_variables()


class TestQEPPLauncher(BaseRequiredInputQELauncherTest, unittest.TestCase):
    _calctype = "pp.x"
    _input_vars = qe_ppx_vars.copy()
    _jobname = "test_qepp_launcher"
    _launcher_class = QEPPLauncher
    _link_calculation = QEPWCalc


class TestQEProjwfcLauncher(BaseRequiredInputQELauncherTest,
                            unittest.TestCase):
    _calctype = "projwfc.x"
    _input_vars = qe_projwfcx_vars.copy()
    _jobname = "test_qeprojwfc_launcher"
    _launcher_class = QEProjwfcLauncher
    _link_calculation = QEPWCalc


class TestQEPWLauncher(BaseQELauncherTest, unittest.TestCase):
    _calctype = "pw.x"
    _input_vars = qe_pwx_vars.copy()
    _jobname = "test_qepw_launcher"
    _launcher_class = QEPWLauncher

    # def test_launcher_write(self):
    #     self.launcher.pseudo_dir = pseudo_dir
    #     super().test_launcher_write()


class TestQEQ2RLauncher(BaseRequiredInputQELauncherTest, unittest.TestCase):
    _calctype = "q2r.x"
    _input_vars = qe_q2rx_vars.copy()
    _jobname = "test_q2r_launcher"
    _launcher_class = QEQ2RLauncher
    _link_calculation = QEPHCalc
