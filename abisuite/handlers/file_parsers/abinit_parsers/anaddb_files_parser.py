from .bases import BaseFilesParser
from ...file_structures import AbinitAnaddbFilesStructure


class AbinitAnaddbFilesParser(BaseFilesParser):
    """Parser that parses an Abinit anaddb files file.
    """
    _loggername = "AbinitAnaddbFilesParser"
    _structure_class = AbinitAnaddbFilesStructure

    def _extract_data_from_lines(self, lines):
        return {"input_file_path": lines[0],
                "output_file_path": lines[1],
                "ddb_file_path": lines[2],
                "band_structure_file_path": lines[3],
                "gkk_file_path": lines[4],
                "eph_data_prefix": lines[5],
                "ddk_file_path": lines[6],
                }
