#!/bin/bash

MPIRUN=""
EXECUTABLE="/home/fgoudreault/Workspace/q-e/bin/q2r.x"
INPUT=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_workflows/test_files/qe_Pb/lattice_expansion/q2r_runs/q2r_vol+2percent/q2r_vol+2percent.in
LOG=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_workflows/test_files/qe_Pb/lattice_expansion/q2r_runs/q2r_vol+2percent/q2r_vol+2percent.log
STDERR=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_workflows/test_files/qe_Pb/lattice_expansion/q2r_runs/q2r_vol+2percent/q2r_vol+2percent.stderr


$MPIRUN $EXECUTABLE -input $INPUT > $LOG 2> $STDERR
