import unittest

from .bases import BaseStatusCheckerTest, BaseSCFStatusCheckerTest
from ..variables_for_tests import (
        QEDOSCalc, QEMatdynCalc, QEPHCalc, QEProjwfcCalc, QEPWCalc,
        QEPWNscfPositiveKptsCalc, QEPWBandCalc, QEQ2RCalc,
        )


class TestQEDOSStatusChecker(BaseStatusCheckerTest, unittest.TestCase):
    _calculation = QEDOSCalc


class TestQEMatdynStatusChecker(BaseStatusCheckerTest, unittest.TestCase):
    _calculation = QEMatdynCalc


class TestQEPHStatusChecker(BaseSCFStatusCheckerTest, unittest.TestCase):
    _calculation = QEPHCalc


class TestQEProjwfcStatusChecker(BaseStatusCheckerTest, unittest.TestCase):
    _calculation = QEProjwfcCalc


class TestQEPWStatusChecker(BaseSCFStatusCheckerTest, unittest.TestCase):
    _calculation = QEPWCalc


class TestQEPWBandStructureStatusChecker(
        BaseStatusCheckerTest, unittest.TestCase):
    _calculation = QEPWBandCalc


class TestQEPWNSCFStatusChecker(BaseStatusCheckerTest, unittest.TestCase):
    _calculation = QEPWNscfPositiveKptsCalc


class TestQEQ2RStatusChecker(BaseStatusCheckerTest, unittest.TestCase):
    _calculation = QEQ2RCalc
