from ..bases import BaseFileHandler
from ...file_parsers import AbinitFatbandParser
from ...file_structures import AbinitFatbandStructure


class AbinitFatbandFile(BaseFileHandler):
    """File handler class for an abinit _FATBAND_* file.
    """
    _loggername = "AbinitFatbandFile"
    _parser_class = AbinitFatbandParser
    _structure_class = AbinitFatbandStructure
