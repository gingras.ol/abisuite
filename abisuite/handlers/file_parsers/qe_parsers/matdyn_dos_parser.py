import os

from .bases import BaseQEDOSParser
from ...file_structures import QEMatdynDOSStructure


class QEMatdynDOSParser(BaseQEDOSParser):
    """Parser class for a dos file produced by the matdyn.x script from the
    Quantum Espresso package.
    """
    _expected_ending = ".dos"
    _loggername = "QEMatdynDOSParser"
    _structure_class = QEMatdynDOSStructure

    def _extract_data_from_array(self, data):
        return {
            "energies": data[:, 0],
            "dos": data[:, 1:],
            }

    @classmethod
    def _filepath_from_meta(cls, meta, *args, **kwargs):
        """Returns the path to the dos file from the meta data file.
        """
        with meta:
            for filepath in os.listdir(meta.rundir):
                if filepath.endswith(cls._expected_ending):
                    return os.path.join(meta.rundir, filepath)
            else:
                raise FileNotFoundError(
                        f"Could not find the matdyn dos file from "
                        f"{meta.calc_workdir}")
