from ..bases import BaseFileHandler
from ...file_parsers import AbinitDMFTEigParser
from ...file_structures import AbinitDMFTEigStructure


class AbinitDMFTEigFile(BaseFileHandler):
    """File Handler class for an abinit eig file.
    """
    _loggername = "AbinitDMFTEigFile"
    _parser_class = AbinitDMFTEigParser
    _structure_class = AbinitDMFTEigStructure
