import os

from .scf_sequencer import AbinitSCFSequencer
from ..bases import SequenceCalculation
from ..relaxation_sequencers import RelaxationSequencer
from ...handlers import AbinitInputFile, AbinitLogFile
from ...launchers.launchers.abinit_launchers.abinit_launcher import (
        __ALL_ABINIT_GEOMETRY_VARIABLES__,
        __ABINIT_ONE_FROM_THESE_GEOMETRY_VARIABLES__,
        )


class AbinitRelaxationSequencer(AbinitSCFSequencer, RelaxationSequencer):
    """Relaxation Sequencer for abinit.

    This is just an SCF calculation with some specific input variables.
    """
    _loggername = "AbinitRelaxationSequencer"

    def __init__(self, *args, **kwargs):
        AbinitSCFSequencer.__init__(self, *args, **kwargs)
        RelaxationSequencer.__init__(self, "abinit", *args, **kwargs)

    @property
    def scf_input_variables(self):
        return self._scf_input_variables

    @scf_input_variables.setter
    def scf_input_variables(self, inputs):
        if not isinstance(inputs, dict):
            raise TypeError(
                    "'scf_input_variables' should be a dict.")
        # mandatory variables
        for var in ("ionmov", "ntime", ):
            if var not in inputs:
                raise ValueError(f"'{var}' should be specified.")
        # ionmov should be >= 2
        ionmov = inputs.get("ionmov")
        if ionmov < 2:
            raise ValueError(
                    f"ionmov should be >= 2 while it is set to '{ionmov}'.")
        self._scf_input_variables = inputs

    def init_sequence(self):
        """Makes the relaxation sequence.

        If `self.relax_atoms` is True, we start by optimizing atoms
        using optcell = 0.
        Then, `if self.relax_cell` is True, we relax the cell
        using optcell = 2. If `relax_atoms` is False, we straight do
        the full cell relaxation (optcell = 2) (not recommended by
        Abinit).
        """
        if self.relax_atoms:
            invars = self.scf_input_variables.copy()
            invars.update({"optcell": 0})
            seq = SequenceCalculation(
                    "abinit", self._get_relax_atoms_workdir(),
                    invars, self.scf_calculation_parameters,
                    loglevel=self._loglevel)
            self.sequence.append(seq)
        if self.relax_cell:
            invars = self.scf_input_variables.copy()
            invars.update({"optcell": 2})
            seq = SequenceCalculation(
                    "abinit", self._get_relax_cell_workdir(),
                    invars, self.scf_calculation_parameters,
                    loglevel=self._loglevel)
            if self.relax_atoms:
                seq.recover_from = self.sequence[0]
                seq.recover_from_other_kwargs["use_last_geometry"] = True
                seq.recover_from_update_variables = {"optcell": 2}
                seq.dependencies.append(self.sequence[0])
            self.sequence.append(seq)

    def _get_relax_atoms_workdir(self):
        return os.path.join(self.scf_workdir, "relax_atoms_only")

    def _get_relax_cell_workdir(self):
        return os.path.join(self.scf_workdir, "relax_full_cell_geometry")

    def _get_relaxed_geometry_variables(self):
        # this method is called only if sequence is completed
        # if geometry variables undefined in input variables,
        # load them from output file of the relaxation calculation
        if self.relax_cell:
            workdir = self._get_relax_cell_workdir()
        else:
            workdir = self._get_relax_atoms_workdir()
        variables = {}
        with AbinitLogFile.from_calculation(
                workdir) as log, AbinitInputFile.from_calculation(
                        workdir) as inp:
            output = log.output_variables
            inputs = inp.input_variables
        for var in __ALL_ABINIT_GEOMETRY_VARIABLES__:
            AOFTGV = __ABINIT_ONE_FROM_THESE_GEOMETRY_VARIABLES__
            if any([var in sublist for sublist in AOFTGV]):
                # deal with them later
                continue
            if var not in output:
                if var != "rprim":
                    raise LookupError(f"'{var}' not in relaxation output.")
                else:
                    # var == rprim, we treat it differently since it has a
                    # default value which, when used, is not echoed.
                    # check first if it is defined in the input file
                    if var in inputs:
                        variables[var] = inputs[var]
                    else:
                        # use default (identity matrix)
                        variables[var] = [
                                [1.0, 0.0, 0.0],
                                [0.0, 1.0, 0.0],
                                [0.0, 0.0, 1.0]]
            else:
                variables[var] = output[var]
        for sublist in __ABINIT_ONE_FROM_THESE_GEOMETRY_VARIABLES__:
            n_present_in_output = 0
            for var in sublist:
                if var in output:
                    n_present_in_output += 1
            if n_present_in_output == 1:
                # just load this one
                for var in sublist:
                    if var not in output:
                        continue
                    variables[var] = output[var]
            elif n_present_in_output > 1:
                # more than one var is defined in output. take the same
                # one that is defined in input
                # btw there should be only one var in input technically
                for var in sublist:
                    if var in inp.input_variables:
                        variables[var] = output[var]
                        break
            else:
                natom = inputs["natom"]
                if "xred" in sublist and natom == 1:
                    # if only 1 atom => it was not displaced and thus may
                    # not have been reported in output. By default take
                    # the input xred or other coords
                    for var in sublist:
                        if var in inputs:
                            variables[var] = inputs[var]
                            break
                    else:
                        raise LookupError(
                            f"None of '{sublist}' defined in output and inputs"
                            )
                else:
                    raise LookupError(
                            f"None of '{sublist}' defined in output...")
        return variables
