from .bases import BaseAbisuiteSubScript
from .. import __USER_CONFIG__
from ..colors import Colors
from ..exceptions import DevError


class AbisuiteConfigScript(BaseAbisuiteSubScript):
    """Handles the 'config' sub script of abisuite.
    """
    _loggername = "AbisuiteConfigScript"

    def load_calculations(self, *args, **kwargs):
        raise DevError("Not relevant for this script.")

    def show_config(self, show_build=None, show_builds=False):
        """Print out on screen the abisuite config info.

        Parameters
        ----------
        show_build: list, optional
            If not None, shows the properties of all builds listed in this
            list.
        show_builds: bool, optional
            If True, the list of different builds stored in the config file are
            printed out.
        """
        if show_builds:
            print(Colors.bold_text("Printing builds:"))
            print("\n".join([f"- {b}" for b in __USER_CONFIG__.list_builds()]))
        if show_build:
            for build in show_build:
                print(Colors.bold_text("Printing build info for: ") +
                      f"'{build}'.")
                if build not in __USER_CONFIG__:
                    self.error("not a build defined in config file.")
                    continue
                print(__USER_CONFIG__[build])
