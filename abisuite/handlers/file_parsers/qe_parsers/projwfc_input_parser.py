from .bases import BaseQEInputParser
from ...file_structures import QEProjwfcInputStructure


class QEProjwfcInputParser(BaseQEInputParser):
    """Class that can parse a Quantum Espresso projwfc.x input File.
    """
    _loggername = "QEProjwfcInputParser"
    _structure_class = QEProjwfcInputStructure
