#!/bin/bash

MPIRUN=""
EXECUTABLE="/home/fgoudreault/Workspace/q-e/bin/ph.x"
INPUT=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_sequencers/files/qe_silicon_phonons/ph_run_q2/ph_run_q2.in
LOG=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_sequencers/files/qe_silicon_phonons/ph_run_q2/ph_run_q2.log
STDERR=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_sequencers/files/qe_silicon_phonons/ph_run_q2/ph_run_q2.stderr


$MPIRUN $EXECUTABLE < $INPUT > $LOG 2> $STDERR
