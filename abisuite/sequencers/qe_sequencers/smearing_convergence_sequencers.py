from .individual_phonon_sequencers import (
        QEPhononConvergenceSequencer,
        )
from .kgrid_convergence_sequencers import QEKgridConvergenceSequencer
from ..smearing_convergence_sequencers import (
        SmearingConvergenceSequencer,
        SmearingPhononConvergenceSequencer,
        )


class QESmearingConvergenceSequencer(
        SmearingConvergenceSequencer, QEKgridConvergenceSequencer):
    """Convergence sequencer for the k-points + smearing parameters
    for Quantum Espresso software.
    """
    smearings_input_variable_name = "degauss"
    smearings_input_variable_units = "Ry"
    _loggername = "QESmearingConvergenceSequencer"

    def __init__(self, *args, **kwargs):
        # do kgrid last since base class overwrites the
        # _kgrids_input_variable_name
        SmearingConvergenceSequencer.__init__(self, "qe", *args, **kwargs)
        QEKgridConvergenceSequencer.__init__(self, *args, **kwargs)

    @property
    def scf_input_variables(self):
        return SmearingConvergenceSequencer.scf_input_variables.fget(self)

    @scf_input_variables.setter
    def scf_input_variables(self, ins):
        SmearingConvergenceSequencer.scf_input_variables.fset(self, ins)
        # add occupations as well! Since we test smearing we should set it on!
        self._set_input_var_to(
                self.scf_input_variables, "occupations", "smearing")

    def compute_convergence(self, *args, **kwargs):
        SmearingConvergenceSequencer.compute_convergence(self, *args, **kwargs)
        # add 'occupations' and 'smearing' if needed
        self._scf_converged_parameter["occupations"] = (
                self.scf_input_variables["occupations"])
        if "smearing" in self.scf_input_variables:
            self._scf_converged_parameter[
                    "smearing"] = self.scf_input_variables.get(
                    "smearing")


class QESmearingPhononConvergenceSequencer(
        QESmearingConvergenceSequencer,
        QEPhononConvergenceSequencer,
        SmearingPhononConvergenceSequencer):
    """Convergence sequencer for the smearing parameters vs the phonon
    frequencies.
    """
    _loggername = "QESmearingPhononConvergenceSequencer"

    def __init__(self, *args, **kwargs):
        QESmearingConvergenceSequencer.__init__(self, *args, **kwargs)
        QEPhononConvergenceSequencer.__init__(self, *args, **kwargs)
        SmearingPhononConvergenceSequencer.__init__(
                self, "qe", *args, **kwargs)

    def compute_convergence(self, *args, **kwargs):
        # Watchout for Deadly Diamond of Death here!!
        SmearingPhononConvergenceSequencer.compute_convergence(
                self, *args, **kwargs)
        if self._scf_converged_parameter is None:
            return
        # add occupations and smearings if necessary
        self._scf_converged_parameter["occupations"] = (
                self.scf_input_variables["occupations"])
        if "smearing" in self.scf_input_variables:
            self._scf_converged_parameter[
                    "smearing"] = self.scf_input_variables.get(
                "smearing", "gaussian")

    def plot_convergence(self, *args, **kwargs):
        SmearingPhononConvergenceSequencer.plot_convergence(
                self, *args, **kwargs)

    def _get_phonon_workdir(self, *args, **kwargs):
        # Watchout Deadly Diamond of Death!!!
        return SmearingPhononConvergenceSequencer._get_phonon_workdir(
                self, *args, **kwargs)
