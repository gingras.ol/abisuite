from ..bases import BaseLogFileHandler
from ...file_parsers import QEQ2RLogParser
from ...file_structures import QEQ2RLogStructure


class QEQ2RLogFile(BaseLogFileHandler):
    """Log file handler for a Quantum Espresso q2r.x log file.
    """
    _loggername = "QEQ2RLogFile"
    _parser_class = QEQ2RLogParser
    _structure_class = QEQ2RLogStructure
