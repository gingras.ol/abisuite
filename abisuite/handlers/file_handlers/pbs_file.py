from .bases import BaseMPIApprovableFileHandler, PathConvertibleFileHandler
from ..file_approvers import PBSApprover
from ..file_parsers import PBSParser
from ..file_structures import PBSStructure
from ..file_writers import PBSWriter


class PBSFile(BaseMPIApprovableFileHandler, PathConvertibleFileHandler):
    """Class that represents and write pbs files which serves to launch
    batch calculations on super clusters or simply on a PC."""
    _loggername = "PBSFile"
    _approver_class = PBSApprover
    _parser_class = PBSParser
    _structure_class = PBSStructure
    _writer_class = PBSWriter

    def __init__(self, *args, **kwargs):
        BaseMPIApprovableFileHandler.__init__(self, *args, **kwargs)
        # The following might not be needed
        # mpi command is an empty string by default
        self.mpi_command = ""
        # self.nodes = ""  # no nodes by default if mpi_command is ""
        # self.ppn = 1  # only one processor by default if mpi_command is ""

    def __eq__(self, pbs):
        # check in both bases
        if not PathConvertibleFileHandler.__eq__(self, pbs):
            # this one will convert everything in absolute paths
            return False
        if not BaseMPIApprovableFileHandler.__eq__(self, pbs):
            return False
        return True

    def move(self, *args, **kwargs):
        PathConvertibleFileHandler.move(self, *args, **kwargs)

    def read(self, *args, **kwargs):
        return BaseMPIApprovableFileHandler.read(self, *args, **kwargs)
