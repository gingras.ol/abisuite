from ..utils import TaggedList
import unittest


class TestTaggedList(unittest.TestCase):
    _item = [1, 2, 3]
    _tag = "normal list"

    def setUp(self):
        self.tagged_list = TaggedList()
        self.tagged_list.append(self._item, tag=self._tag)

    def tearDown(self):
        del self.tagged_list

    def test_append(self):
        # check the item is accessible both by its tag and index
        self.assertEqual(self.tagged_list[self._tag], self._item)
        self.assertEqual(self.tagged_list[0], self._item)

    def test_remove_by_tag(self):
        self.tagged_list.remove(self._tag)
        with self.assertRaises(KeyError):
            self.tagged_list.remove(self._tag)

    def test_remove_by_index(self):
        self.tagged_list.remove(0)
        with self.assertRaises(KeyError):
            self.tagged_list.remove(0)

    def test_set_item(self):
        newitem = self._item + [4]
        # set by tag
        self.tagged_list[self._tag] = newitem
        self.assertEqual(self.tagged_list[self._tag], newitem)
        with self.assertRaises(KeyError):
            self.tagged_list["wrong key"] = newitem
        # set by index
        newitem = newitem + [5]
        self.tagged_list[0] = newitem
        self.assertEqual(self.tagged_list[0], newitem)
        with self.assertRaises(KeyError):
            # wrong index
            self.tagged_list[3] = newitem

    def test_change_tag(self):
        newtag = "new tag"
        self.tagged_list.change_tag(newtag, self._tag)
        self.assertEqual(self.tagged_list.get_tag_from_index(0), newtag)
        newtag = "new tag2"
        self.tagged_list.change_tag(newtag, 0)
        self.assertEqual(self.tagged_list.get_tag_from_index(0), newtag)

        with self.assertRaises(KeyError):
            self.tagged_list.change_tag(newtag, "wrong tag")
        with self.assertRaises(KeyError):
            self.tagged_list.change_tag(newtag, 12)

    def test_get_tag_from_index(self):
        self.assertEqual(self.tagged_list.get_tag_from_index(0), self._tag)
        with self.assertRaises(KeyError):
            self.tagged_list.get_tag_from_index(12)

    def test_tolist(self):
        # test that the tolist method returns a copied list
        list_ = self.tagged_list.tolist()
        self.assertIsInstance(list_, list)
        self.assertIsNot(list_, self.tagged_list.items)
