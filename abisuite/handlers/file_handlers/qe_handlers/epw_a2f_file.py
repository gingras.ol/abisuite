from ..bases import BaseFileHandler
from ...file_parsers import QEEPWa2FParser
from ...file_structures import QEEPWa2FStructure


class QEEPWa2FFile(BaseFileHandler):
    """Class that represents a a2F function file produced by a epw.x
    calculation from Quantum Espresso.
    """
    _loggername = "QEEPWa2FFile"
    _parser_class = QEEPWa2FParser
    _structure_class = QEEPWa2FStructure
