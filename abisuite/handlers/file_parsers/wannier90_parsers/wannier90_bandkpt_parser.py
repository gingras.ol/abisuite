import os

import numpy as np

from ..bases import BaseCalculationFileParser
from ...file_structures import Wannier90BandkptStructure
from ....routines import decompose_line


class Wannier90BandkptParser(BaseCalculationFileParser):
    """Parser class for a seedname_band.kpt file produced by wannier90 when
    bands_plot=True.
    """
    _expected_ending = "_band.kpt"
    _loggername = "Wannier90BandkptParser"
    _structure_class = Wannier90BandkptStructure

    def _extract_data(self):
        # this file is the list of kpts. The first line is the number of kpts
        # there is a 4th column which is the weight
        with open(self.path, "r") as f:
            lines = f.readlines()
        s, i, f = decompose_line(lines[0])
        nkpts = i[0]
        kpts = np.loadtxt(self.path, skiprows=1, usecols=(0, 1, 2))
        if len(kpts) != nkpts:
            raise ValueError(f"Number of kpts read ({len(kpts)}) does not "
                             f"match number of kpts ({nkpts}).")
        self.nkpt = nkpts
        self.kpts = kpts

    @classmethod
    def _filepath_from_meta(cls, meta, *args, **kwargs):
        # this file is located next to the '.wout' file in main calc dir.
        return os.path.join(
                meta.calc_workdir, meta.jobname + cls._expected_ending)
