from .ph_launcher import QEPHLauncher
from ..bases import BaseMassLauncher


class QEPHMassLauncher(BaseMassLauncher):
    """The mass launcher class for Quantum Espresso calculations
    using the ph.x script.
    """
    _launcher_class = QEPHLauncher
    _loggername = "QEPHMassLauncher"
