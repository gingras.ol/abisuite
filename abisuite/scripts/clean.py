from .bases import BaseAbisuiteSubScript
from ..colors import Colors


class AbisuiteCleanScript(BaseAbisuiteSubScript):
    """Executes the 'clean' subcommand of the abisuite script.
    """
    _loggername = "AbisuiteCleanScript"

    def clean(self, dry=False, **kwargs):
        """This method actually executes the cleaning process.

        Parameters
        ----------
        dry: bool, optional
            If True, a dry run is done and only what will be cleaned will
            be shown.
        """
        total_nfiles_cleaned = 0
        total_ngbs_cleaned = 0
        total_nlinks_cleaned = 0
        if dry:
            print(Colors.bold_text("DRY RUN ONLY:") +
                  " printing files to delete.")
        for calculation in self.calculations:
            print(Colors.bold_text("CLEANING") + f" '{calculation.path}'.")
            try:
                calculation.clean(dry=dry, **kwargs)
                cleaner = calculation.cleaner
                total_nfiles_cleaned += cleaner.nfiles_cleaned
                total_nlinks_cleaned += cleaner.nlinks_cleaned
                total_ngbs_cleaned += cleaner.ngb_cleaned
                self.print_cleaning_info(
                        cleaner.nfiles_cleaned,
                        cleaner.nlinks_cleaned,
                        cleaner.ngb_cleaned,
                        dry=dry, total=False)
            except Exception:
                self._logger.exception(Colors.color_text("FAILED", "red"))
        self.print_cleaning_info(
                total_nfiles_cleaned, total_nlinks_cleaned, total_ngbs_cleaned,
                dry=dry, total=True)

    def print_cleaning_info(
            self, nfiles_cleaned, nlinks_cleaned, ngb_cleaned, dry=False,
            total=False):
        """Prints summary cleaning info on screen.

        Parameters
        ----------
        nfiles_cleaned: int
            Number of files cleaned.
        nlinks_cleaned: int
            Number of links pruned.
        ngb_cleaned: float
            Total file size cleaned (in Gb). This number, if more than a Tb,
            will be converted to Tb before printing.
        dry: bool, optional
            If True, a dry run was executed. Changes format of print message.
            If total is False and dry is True => nothing is printed.
        total: bool, optional
            If True, the total summary of the cleaning run is printed.
            This option just changes the printing format.
        """
        if not total and dry:
            return
        ngb_units = "Gb"
        if nfiles_cleaned >= 1000 and nfiles_cleaned:
            nfiles_cleaned = str(nfiles_cleaned // 1000) + "k"
        if nlinks_cleaned >= 1000:
            nlinks_cleaned = str(nlinks_cleaned // 1000) + "k"
        if ngb_cleaned >= 1024:
            ngb_cleaned /= 1024
            ngb_units = "Tb"
        if not total:
            print(Colors.color_text("SUCCESSFULLY CLEANED", "green") +
                  f" {nfiles_cleaned} files deleted, "
                  f" {nlinks_cleaned} links pruned and "
                  f"{ngb_cleaned:.2f} {ngb_units} cleaned.")
        else:
            print("---------------------------------------")
            if dry:
                first = "Will clean in total:"
                second = "Will free:"
            else:
                first = "Cleaned in total:"
                second = "Freed:"
            print(Colors.bold_text(first) +
                  f" {nfiles_cleaned} files and " +
                  f"{nlinks_cleaned} links."
                  )
            print(Colors.bold_text(second) +
                  f" {ngb_cleaned:.2f} {ngb_units} of disk space.")
