#!/bin/bash

MPIRUN="mpirun -np 4"
EXECUTABLE="/home/fgoudreault/Workspace/abinit/abinit/build/develop/2021-04-16/src/98_main/abinit"
INPUT=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_workflows/test_files/abinit_si/paw/gs_kgrid_convergence/ngkpt_6_6_6/run/ngkpt_6_6_6.files
LOG=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_workflows/test_files/abinit_si/paw/gs_kgrid_convergence/ngkpt_6_6_6/ngkpt_6_6_6.log
STDERR=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_workflows/test_files/abinit_si/paw/gs_kgrid_convergence/ngkpt_6_6_6/ngkpt_6_6_6.stderr


$MPIRUN $EXECUTABLE < $INPUT > $LOG 2> $STDERR
