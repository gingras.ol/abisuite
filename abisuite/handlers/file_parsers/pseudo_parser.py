import abc
import os
import xml.etree.ElementTree as ET

from .bases import BaseFormattedCalculationFileParser
from ..file_structures import PseudoStructure
from ...bases import BaseUtility
from ...routines import decompose_line


ELEMENT_TO_ATOMIC_NUMBER = {
        "O": 8,
        "Mg": 12,
        "Si": 14,
        "Cu": 29,
        "Zn": 30,
        "Sr": 38,
        "Ru": 44,
        "Cd": 48,
        "La": 57,
        "Pb": 82,
        }


# NOTE:
# the pseudo parser calls other pseudo parsers depending of the type of pseudo
# we have. The reason is that we want the same File handler class for any type
# of pseudo to not be bothered to create all the utilities
# (structure, handler, etc.) for each type of
# pseudos. That way, we only have to implement a new class of pseudo parser
# for each new pseudo type we want to implement.


class PseudoParser(BaseFormattedCalculationFileParser):
    """Parser for a pseudo potential file.
    """
    _expected_ending = ""
    _loggername = "PseudoParser"
    _structure_class = PseudoStructure

    def _extract_data(self):
        super()._extract_data(strip_newlines=True)

    def _extract_data_from_lines(self, lines):
        if self.path.lower().endswith(".upf"):
            _pseudo_parser_cls = UPFPseudoParser
        elif self.path.lower().endswith("pspgth"):
            _pseudo_parser_cls = PSPGTHPseudoParser
        elif self.path.lower().endswith(".fhi"):
            _pseudo_parser_cls = FHIPseudoParser
        elif self.path.lower().endswith(".pspnc"):
            _pseudo_parser_cls = PSPNCPseudoParser
        elif self.path.lower().endswith(".psp8"):
            _pseudo_parser_cls = PSP8PseudoParser
        elif self.path.lower().endswith(".xml"):
            _pseudo_parser_cls = XMLPseudoParser
        else:
            raise NotImplementedError(
                    "No parser for this type of pseudo: "
                    f"{os.path.basename(self.path)}")
        parser_cls = _pseudo_parser_cls(loglevel=self._loglevel)
        try:
            data = parser_cls.extract_data(lines)
        except (ET.ParseError, LookupError) as err:
            self._logger.error(
                    "An error occured while extracting data from: "
                    f"'{self.path}'.")
            raise err
        return data

    @classmethod
    def _filepath_from_meta(cls, metadatafile):
        # return depends of type of calc
        if metadatafile.calctype == "abinit":
            # can get pseudos from files file
            from .abinit_parsers import AbinitFilesParser
            parser = AbinitFilesParser.from_metadatafile(metadatafile)
            parser.read()
            return parser.pseudos
        elif metadatafile.calctype == "qe_pw":
            # can get pseudos from the input file
            from .qe_parsers import QEPWInputParser
            parser = QEPWInputParser.from_metadatafile(metadatafile)
            parser.read()
            species = parser.input_variables["atomic_species"]
            return [atom["pseudo"] for atom in species]
        else:
            raise NotImplementedError(metadatafile.calctype)


class BasePseudoParser(BaseUtility, abc.ABC):
    """Base parser class for any type of pseudos.
    """
    def extract_data(self, lines):
        data = {}
        # what type of functional was used to generate the pseudo
        data["ixc"] = self._get_ixc(lines)
        # is full relativistic?
        data["is_full_relativistic"] = self._is_full_relativistic(lines)
        # is ultrasoft?
        data["is_ultrasoft"] = self._is_ultrasoft(lines)
        # is paw?
        data["is_paw"] = self._is_paw(lines)
        # has so?
        data["has_so"] = self._has_so(lines)
        # zion and zatom
        data["zion"] = self._get_zion(lines)
        data["zatom"] = self._get_zatom(lines)
        return data

    @abc.abstractmethod
    def _get_ixc(self, *args, **kwargs):
        pass

    @abc.abstractmethod
    def _get_zatom(self, *args, **kwargs):
        pass

    @abc.abstractmethod
    def _get_zion(self, *args, **kwargs):
        pass

    @abc.abstractmethod
    def _has_so(self, *args, **kwargs):
        pass

    @abc.abstractmethod
    def _is_full_relativistic(self, *args, **kwargs):
        pass

    @abc.abstractmethod
    def _is_paw(self, *args, **kwargs):
        pass

    @abc.abstractmethod
    def _is_ultrasoft(self, *args, **kwargs):
        pass

    def _dig_pseudo(self, lines, trigger):
        # returns the line containing the trigger
        for line in lines:
            if trigger in line:
                return line
        raise LookupError(f"Could not find '{trigger}'")

    def _dig_bool_property(self, lines, prop):
        # dig pseudo file for a boolean property
        line = self._dig_pseudo(lines, prop)
        if '"T"' in line or '"true"' in line.lower():
            return True
        return False


class FHIPseudoParser(BasePseudoParser):
    """Parser class for a fhi pseudo file.
    """
    _loggername = "FHIPseudoParser"

    def _get_ixc(self, lines):
        # like for psp8 files
        s, i, f = decompose_line(self._dig_pseudo(lines, "pspxc"))
        return i[1]

    def _get_zatom(self, lines):
        # zatom is located in a line that looks like this:
        # Aluminum, fhi98PP : Hamann-type, LDA CA PerdewWang, l=2 local
        #  13.000  3.000    981214              zatom,zion,pspdat
        line = self._dig_pseudo(lines, "zatom")
        s, i, f = decompose_line(line)
        return f[0]

    def _get_zion(self, lines):
        # zatom is located in a line that looks like this:
        # Aluminum, fhi98PP : Hamann-type, LDA CA PerdewWang, l=2 local
        #  13.000  3.000    981214              zatom,zion,pspdat
        line = self._dig_pseudo(lines, "zion")
        s, i, f = decompose_line(line)
        return f[1]

    def _has_so(self, lines):
        # for now I just assume fhi don't support SOC
        return False

    def _is_full_relativistic(self, lines):
        # for now I just assume fhi don't support SOC
        return False

    def _is_paw(self, lines):
        # not sure if we can have FHI PAW pseudos
        return False

    def _is_ultrasoft(self, lines):
        # not sure if we can have ultrasoft fhi pseudos
        return False


class PSPNCPseudoParser(BasePseudoParser):
    """Parser class for a .pspnc pseudo file.
    """
    _loggername = "PSPNCPseudoParser"

    def _get_ixc(self, lines):
        # like for psp8 files
        s, i, f = decompose_line(self._dig_pseudo(lines, "pspxc"))
        return i[1]

    def _get_zatom(self, lines):
        # zatom is located in a line that looks like this:
        # Troullier-Martins psp for element  As   Thu Oct 27 17:37:14 EDT 1994
        #  33.00000   5.00000    940714                zatom, zion, pspdat
        line = self._dig_pseudo(lines, "zatom")
        s, i, f = decompose_line(line)
        return f[0]

    def _get_zion(self, lines):
        # zatom is located in a line that looks like this:
        # Troullier-Martins psp for element  As   Thu Oct 27 17:37:14 EDT 1994
        #  33.00000   5.00000    940714                zatom, zion, pspdat
        line = self._dig_pseudo(lines, "zion")
        s, i, f = decompose_line(line)
        return f[1]

    def _has_so(self, lines):
        # for now I just assume pspnc don't support SOC
        return False

    def _is_full_relativistic(self, lines):
        # for now I just assume pspnc don't support SOC
        return False

    def _is_paw(self, lines):
        # not sure if we can have pspnc PAW pseudos
        return False

    def _is_ultrasoft(self, lines):
        # PSPNC files are norm-conserving so just not ultrasoft
        return False


class PSP8PseudoParser(BasePseudoParser):
    """Parser calss for a .psp8 pseudo file.

    See this link for further info on this file type:
    https://docs.abinit.org/developers/psp8_info/
    """
    _loggername = "PSP8PseudoParser"

    def _get_ixc(self, lines):
        # ixc number is located in the following line:
        # 8 -116133   2     4   600     0    pspcod,pspxc,lmax,lloc,mmax,r2well
        line = self._dig_pseudo(lines, "pspxc")
        s, i, f = decompose_line(line)
        return i[1]

    def _get_zatom(self, lines):
        # zatom is located in a line that looks like this:
        #  46.0000     18.0000      170510    zatom,zion,pspd
        line = self._dig_pseudo(lines, "zatom")
        s, i, f = decompose_line(line)
        return f[0]

    def _get_zion(self, lines):
        # zatom is located in a line that looks like this:
        #  46.0000     18.0000      170510    zatom,zion,pspd
        line = self._dig_pseudo(lines, "zion")
        s, i, f = decompose_line(line)
        return f[1]

    def _has_so(self, lines):
        line = self._dig_pseudo(lines, "extension_switch")
        # if extension swich is set to 2 (or 3) then there are additional lines
        # in the pseudo to describe the number of projectors which means
        # the pseudo supports SOC
        s, i, f = decompose_line(line)
        return i[0] in (2, 3)

    def _is_full_relativistic(self, lines):
        return self._has_so(lines)

    def _is_paw(self, lines):
        # not sure if we can have psp8 PAW pseudos
        return False

    def _is_ultrasoft(self, lines):
        # For now not sure if psp8 files can be ultrasoft
        return False


class PSPGTHPseudoParser(BasePseudoParser):
    """Pseudo parser class for a pspgth pseudo file.
    """
    _loggername = "PSPGTHPseudoParser"

    def _get_ixc(self, lines):
        # like for psp8 files
        s, i, f = decompose_line(self._dig_pseudo(lines, "pspxc"))
        return i[1]

    def _get_zatom(self, lines):
        return self._dig_int_property(lines, "zatom")

    def _get_zion(self, lines):
        return self._dig_int_property(lines, "zion")

    def _has_so(self, lines):
        self._logger.warning(
                "'has_so' property finder in pspgth pseudo not implemented...")
        return False  # TODO: to implement later

    def _is_full_relativistic(self, lines):
        self._logger.warning(
                "'is_full_relativistic' property finder in pspgth pseudo not "
                "implemented...")
        return False  # TODO: to implement later

    def _is_paw(self, lines):
        self._logger.warning(
                "'is_paw' property finder in pspgth pseudo not "
                "implemented...")
        return False  # TODO: to implement later

    def _is_ultrasoft(self, lines):
        self._logger.warning(
                "'is_ultrasoft' property finder in pspgth pseudo not "
                "implemented...")
        return False  # TODO: to implement later

    def _dig_int_property(self, lines, trigger):
        # return an int from a line with multiple integers
        # e.g: 31.0000     13.0000      170504    zatom,zion,pspd
        line = self._dig_pseudo(lines, trigger)
        splitted = line.split()
        prop_index = splitted[-1].split(",").index(trigger)
        return int(splitted[prop_index])


class XMLPseudoParser(BasePseudoParser):
    """Pseudo parser class for an xml pseudo file.
    """
    _loggername = "XMLPseudoParser"

    def _get_ixc(self, tree_list):
        element = self._get_xml_element(
                tree_list, "xc_functional").get("name")
        return element

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.tree = None

    def extract_data(self, lines):
        # generate XML element tree from lines
        string = "\n".join(lines)
        lines = list(ET.fromstring(string))
        return super().extract_data(lines)

    def _get_zatom(self, tree_list):
        return float(self._get_xml_element(tree_list, "atom").get("Z"))

    def _get_zion(self, tree_list):
        return float(self._get_xml_element(tree_list, "atom").get("valence"))

    def _has_so(self, tree_list):
        # not sure for now how to check this
        return True

    def _is_full_relativistic(self, tree_list):
        return "full" in self._get_xml_element(
                tree_list, "generator").get("type")

    def _is_paw(self, tree_list):
        return "atompaw" in self._get_xml_element(
                tree_list, "generator").get("name")

    def _is_ultrasoft(self, tree_list):
        # by default it is not ultrasoft since its PAW!
        return False

    def _get_xml_element(self, tree_list, element_name):
        # return the xml element with the tagName 'element_name'
        for element in tree_list:
            if element.tag == element_name:
                return element
        raise LookupError(f"Could not get '{element_name}'.")


class UPFPseudoParser(XMLPseudoParser):
    """Parser class for a upf pseudo file.

    UPF files are XML files actually.
    """
    _loggername = "UPFPseudoParser"

    # def _get_zatom(self, lines):
    #     # zatom is located in a line that looks like this:
    #     # # atsym  z   nc   nv     iexc    psfile
    #     # Si 14.00    3    2       4      upf
    #     # or
    #     # Si 14.00    3    2       4      both
    #     for keyword in ("upf", "both"):
    #         try:
    #             line = self._dig_pseudo(lines, keyword)
    #         except LookupError:
    #             # could not find the line
    #             continue
    #         else:
    #             s, i, f = decompose_line(line)
    #             if len(f) == 1:
    #                 return int(f[0])
    #     # that did not work. Try to get element en get the atomic number from
    #     # table
    #     # e.g.: element="Si"
    #     element_line = self._dig_pseudo(lines, "element")
    #     if "PP_HEADER" in element_line:
    #         element = element_line.split(
    #                 "element")[1].split("=")[1].split()[0].strip('"')
    #     else:
    #         element_line = element_line.split("=")[-1]
    #         element = element_line.strip("'").strip('"')
    #     if element not in ELEMENT_TO_ATOMIC_NUMBER:
    #         raise NotImplementedError(element)
    #     return ELEMENT_TO_ATOMIC_NUMBER[element]

    # def _has_so(self, lines):
    #     line = self._dig_pseudo(lines, "has_so")
    #     if "PP_HEADER" in line:
    #         return (
    #             "false" not in line.split(
    #                 "has_so")[1].split("=")[1].split()[0])
    #     else:
    #         return self._dig_bool_property(lines, "has_so")

    # def _is_ultrasoft(self, lines):
    #     return self._dig_bool_property(lines, "is_ultrasoft")

    def _get_ixc(self, tree_list):
        element = self._get_xml_element(
                tree_list, "PP_HEADER").get("functional")
        return element

    def _get_zatom(self, tree_list):
        element = self._get_xml_element(tree_list, "PP_HEADER").get("element")
        if element == "Xx":
            # Virtual atom (VCA). just return 0
            return 0
        return ELEMENT_TO_ATOMIC_NUMBER[element.strip()]

    def _get_zion(self, tree_list):
        return float(
                self._get_xml_element(tree_list, "PP_HEADER").get("z_valence"))

    def _is_full_relativistic(self, tree_list):
        return "full" in self._get_xml_element(
                tree_list, "PP_HEADER").get("relativistic")

    def _is_paw(self, tree_list):
        return (self._get_xml_element(
                    tree_list, "PP_HEADER").get("is_paw") == "true")
