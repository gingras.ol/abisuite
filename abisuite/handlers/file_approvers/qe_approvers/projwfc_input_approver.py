from ..bases import BaseInputApprover
from ...file_structures import QEProjwfcInputStructure
from ....variables import ALL_QEPROJWFC_VARIABLES


class QEProjwfcInputApprover(BaseInputApprover):
    """Class that checkes the input variables for a projwfc.x calculation.
    """
    _loggername = "QEProjwfcInputApprover"
    _variables_db = ALL_QEPROJWFC_VARIABLES
    _structure_class = QEProjwfcInputStructure
