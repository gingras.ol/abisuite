# This file contains the sub-subparsers for the dtset subparser
from .anaddb_log_subparsers import (
        AbinitAnaddbLogNonAnalyticalGammaCorrectionsSubParser,
        )
from ...bases import BaseSubParser
from .....routines import decompose_line


class AbinitDtsetFermiEnergySubParser(BaseSubParser):
    """Dtset subparser for the fermi level.
    """
    trigger = "fermie"
    subject = "fermi_energy"
    _loggername = "AbinitDtsetFermiEnergySubParser"

    def _extract_data_from_lines(self, lines):
        # fermie    :   2.06955281E-01
        self.ending_relative_index = 0
        s, i, f = decompose_line(lines[0])
        return f[0]


class AbinitDtsetPhononFrequenciesSubParser(BaseSubParser):
    """Subparser for phonon frequencies.
    """
    trigger = "phonon wavelength (reduced coordinates)"
    subject = "phonon_frequencies"
    _loggername = "AbinitDtsetPhononFrequenciesSubParser"

    def _extract_data_from_lines(self, lines):
        # lines looks exactly like for anaddb phonon frequencies
        # phonon wavelength (reduced coordinates) , norm, and energies in hartree              # noqa: E501
        # 0.00 0.00 0.00 1.00
        # 2.542183335E-06  2.542183695E-06  2.542184774E-06  1.568559216E-03  1.568559216E-03  # noqa: E501
        # 1.568559216E-03
        # FIXME: this is not really beautiful haha
        cls = AbinitAnaddbLogNonAnalyticalGammaCorrectionsSubParser
        return cls._extract_data_from_lines(self, lines)
