from .bases import BaseQECalculationStatusChecker


class QELD1CalculationStatusChecker(BaseQECalculationStatusChecker):
    """Status checker for a ld1.x calculation from Quantum Espresso.
    """
    _loggername = "QELD1CalculationStatusChecker"
