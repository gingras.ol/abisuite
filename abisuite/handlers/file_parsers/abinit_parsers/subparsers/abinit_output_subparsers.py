from ...bases import BaseSubParser
from .....routines import decompose_line


# timing section looks like this:
#
# - Total cpu        time (s,m,h):       7007.1      116.79      1.946
# - Total wall clock time (s,m,h):       7048.3      117.47      1.958
# -
# - For major independent code sections, cpu and wall times (sec),
# -  as well as % of the time and number of calls for node 0
# -
# -<BEGIN_TIMER mpi_nprocs = 16, omp_nthreads = 1, mpi_rank = 0>
# - cpu_time =          421.7, wall_time =          440.5
# -
# - routine                            cpu     %          wall     %      number of calls Gflops    Speedup Efficacity  # noqa
# -                                                                        (-1=no count)                                # noqa
# - nonlop(apply)                   204.745   2.9       206.559   2.9           1920      -1.00        0.99       0.99  # noqa
# - fourwf%(pot)                     96.362   1.4        96.768   1.4           1920      -1.00        1.00       1.00  # noqa
# - others (136)                     50.737   0.7        51.430   0.7             -1      -1.00        0.99       0.99  # noqa
# -<END_TIMER>
# -
# - subtotal                        351.845   5.0       354.757   5.0                                  0.99       0.99  # noqa
#
# - For major independent code sections, cpu and wall times (sec),
# - as well as % of the total time and number of calls
#
# -<BEGIN_TIMER mpi_nprocs = 16, omp_nthreads = 1, mpi_rank = world>
# - cpu_time =        7007.1, wall_time =        7048.3
# -
# - routine                            cpu     %          wall     %      number of calls Gflops    Speedup Efficacity  # noqa
# -                                                                        (-1=no count)                                # noqa
# - nonlop(apply)                  3157.344  45.1      3166.996  44.9          30720      -1.00        1.00       1.00  # noqa
# - fourwf%(pot)                   1479.505  21.1      1483.452  21.0          30720      -1.00        1.00       1.00  # noqa
# - nonlop(forces)                  300.553   4.3       301.551   4.3           6144      -1.00        1.00       1.00  # noqa
# - xc:pot/=fourdp                  111.578   1.6       111.843   1.6            176      -1.00        1.00       1.00  # noqa
# - fourdp                           79.051   1.1        79.379   1.1            864      -1.00        1.00       1.00  # noqa
# - vtowfk(contrib)                  60.132   0.9        62.677   0.9            160      -1.00        0.96       0.96  # noqa
# - stress                           58.745   0.8        58.896   0.8             16      -1.00        1.00       1.00  # noqa
# - pspini                           55.198   0.8        55.445   0.8             16      -1.00        1.00       1.00  # noqa
# - others (130)                    138.185   2.0       139.189   2.0             -1      -1.00        0.99       0.99  # noqa
# -<END_TIMER>
#
# - subtotal                       5440.291  77.6      5459.428  77.5                                  1.00       1.00  # noqa


class AbinitOutputTimingSubParser(BaseSubParser):
    """Subparser class for the timing section if it was printed out.
    """
    trigger = "<BEGIN_TIMER"
    subject = "timing"
    _loggername = "AbinitOutputTimingSubParser"

    def _extract_data_from_lines(self, lines):
        # there are 2 blocks: 1 for a single cpu and the other one for the
        # total.
        data = {}
        skip = 0
        for iline, line in enumerate(lines):
            if iline < skip:
                continue
            if self.trigger in line:
                if not data:
                    data["mpi_rank_0"], rel_skip = self._extract_timing(
                            lines[iline:])
                    skip = iline + rel_skip
                    continue
                else:
                    data["mpi_rank_world"], rel_skip = self._extract_timing(
                            lines[iline:])
                    self.ending_relative_index = iline + rel_skip
                    return data
        raise LookupError("Could not find timing.")

    def _extract_timing(self, lines):
        data = {}
        start_parsing = False
        for iline, line in enumerate(lines):
            if "<END_TIMER>" in line:
                return data, iline
            if "routine" in line:
                start_parsing = True
                continue
            if not start_parsing:
                continue
            if "others (" in line:
                # special case since the parenthesis fucks up everything
                line = line.replace("others (", "others_(")
            s, i, f = decompose_line(line.lstrip("-"))
            if not f:
                continue
            for string in s:
                if "others" in string:
                    s = ["others"]
                    break
            data[" ".join(s)] = {
                    "cpu_time": f[0],
                    "cpu_time_%": f[1],
                    "wall_time": f[2],
                    "wall_time_%": f[3],
                    "n_calls": i[0],
                    "Gflops": f[4],
                    "speedup": f[5],
                    "efficacity": f[6],
                    }
