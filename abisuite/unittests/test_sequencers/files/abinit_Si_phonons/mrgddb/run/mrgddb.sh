#!/bin/bash

MPIRUN=""
EXECUTABLE="/home/fgoudreault/Workspace/abinit/abinit/build/develop/2021-04-20/src/98_main/mrgddb"
INPUT=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_sequencers/files/abinit_Si_phonons/mrgddb/mrgddb.in
LOG=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_sequencers/files/abinit_Si_phonons/mrgddb/mrgddb.log
STDERR=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_sequencers/files/abinit_Si_phonons/mrgddb/mrgddb.stderr


$MPIRUN $EXECUTABLE < $INPUT > $LOG 2> $STDERR
