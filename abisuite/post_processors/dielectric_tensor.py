import numpy as np

from .bases import BasePostProcClass
from ..handlers import AbinitOpticLincompFile, CalculationDirectory
from ..plotters import MultiPlot
from ..routines import is_list_like


class DielectricTensor(BasePostProcClass):
    """Object that represents a Dielectric tensor. It reads all the linear
    optic files from a calculation and can plot the tensor.
    """
    _loggername = "DielectricTensor"

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._energies = None
        self._dielectric_tensor = None

    @property
    def dielectric_tensor(self):
        if self._dielectric_tensor is not None:
            return self._dielectric_tensor
        raise ValueError(
                "Need to set 'dielectric_tensor'.")

    @dielectric_tensor.setter
    def dielectric_tensor(self, tensor):
        self._dielectric_tensor = tensor

    @property
    def energies(self):
        if self._energies is not None:
            return self._energies
        raise ValueError("Need to set 'energies'.")

    @energies.setter
    def energies(self, energies):
        self._energies = energies

    # This is an alias
    @property
    def frequencies(self):
        return self.energies

    @frequencies.setter
    def frequencies(self, freqs):
        self.energies = freqs

    def get_plot(
            self, elements=None, linestyle="-", label="", color=None,
            linewidth=2, multiplot=True, **kwargs):
        """Create the dielectric tensor plot. Either a MultiPlot is
        generated or two separated plot depending of the given
        parameters.

        Parameters
        ----------
        elements: list, optional
            Gives which elements of the tensor to plot. It should be a list
            of two-elements-lists which contains the indices to plot
            (0, 1 or 2). If None, all elements are plotted.
        linestyle: str, optional
            Gives the linestyles of the lines.
        linewidth: int, optional
            The linewidth of the curves.
        label: str, optional
            Extra label to add to the tensor labels.
        color: str or list, optional
            Forces a curve color. If a list is given, it is the color for
            each tensor elements plotted. Thus it must be the same length
            as the 'elements' kwarg.
        multiplot: bool, optional
            If True, a MultiPlot is returned instead of two separated plots.
            In that case, the plots are aligned vertically and the upper
            part is the real part of the dielectric tensor and the lower
            part is its imaginary part.
        """
        plot_re = super().get_plot(
                xlabel=kwargs.pop("xlabel", r"$\omega$"),
                xunits="eV",
                ylabel=kwargs.pop("ylabel", r"Re$[\epsilon(\omega)]$"),
                **kwargs
                )
        plot_im = super().get_plot(
                xlabel=kwargs.pop("xlabel", r"$\omega$"),
                xunits="eV",
                ylabel=kwargs.pop("ylabel", r"Im$[\epsilon(\omega)]$"),
                **kwargs
                )
        ydatas = []
        labels = []
        colors = ["r", "m", "orange", "b", "c", "g"]
        symbol = r"$\epsilon$"
        ylabels = [
                [symbol + r"$_{xx}$",
                 symbol + r"$_{xy}$",
                 symbol + r"$_{xz}$"],
                [symbol + r"$_{yx}$",
                 symbol + r"$_{yy}$",
                 symbol + r"$_{yz}$"],
                [symbol + "$_{zx}$",
                 symbol + "$_{zy}$",
                 symbol + r"$_{zz}$"]]
        if elements is None:
            indices = [(0, 0), (0, 1), (0, 2), (1, 1), (1, 2), (2, 2)]
        else:
            colors = colors[:len(elements)]
            if color is not None:
                if not is_list_like(color):
                    colors = [color] * len(colors)
                else:
                    colors = color
                if len(colors) != len(elements):
                    raise ValueError(
                            "List of colors must be same length as number of "
                            " tensor elements to plot.")
            indices = elements
        if label and not label.startswith(" "):
            # add beginning space if necessary
            label = " " + label
        for index in indices:
            i = index[0]
            j = index[1]
            ydatas.append(self.dielectric_tensor[i, j, :])
            labels.append(ylabels[i][j] + label)
        for ydata, ylabel, color in zip(ydatas, labels, colors):
            plot_re.add_curve(
                    self.frequencies, ydata.real, label=ylabel,
                    linewidth=linewidth,
                    color=color, linestyle=linestyle,
                    )
            plot_re.grid = True
            plot_im.add_curve(
                    self.frequencies, ydata.imag, label=ylabel,
                    linewidth=linewidth,
                    color=color, linestyle=linestyle,
                    )
            plot_im.grid = True
        if multiplot:
            multiplot = MultiPlot(loglevel=self._loglevel)
            # remove the xlabel from the top one
            plot_re.xlabel = ""
            multiplot.add_plot(plot_re, 0)
            multiplot.add_plot(plot_im, 1)
            return multiplot
        return plot_re, plot_im

    @classmethod
    def from_handlers(cls, handlers, *args, **kwargs):
        """Create a DielectricTensor object from a list of abinit file handlers
        containing the linear optical dielectric tensor components.

        Parameters
        ----------
        files: list-like
            The list of file handlers.
        """
        if isinstance(handlers, AbinitOpticLincompFile):
            # single item, create list
            handlers = (handlers, )
        if not is_list_like(handlers):
            raise TypeError(
                    f"Need a list of handlers but got '{handlers}'.")
        obj = cls(*args, **kwargs)
        tensor = None
        for handler in handlers:
            with handler as lin:
                freqs = lin.frequencies
                if tensor is None:
                    tensor = np.zeros((3, 3, len(freqs)), dtype=np.complex128)
                # component is a string e.g.: '11', '12', ...
                # need to convert to actual indices
                component = lin.component
                idx = int(component[0]) - 1
                idy = int(component[1]) - 1
                # this is a complex tensor
                tensor[idx, idy] = lin.epsilon
        obj.dielectric_tensor = tensor
        obj.frequencies = freqs
        return obj

    @classmethod
    def from_calculation(cls, path, *args, **kwargs):
        """Creates a DielectricTensor object from a calculation. This methods
        fills the attributes accordingly.

        Currently, only an 'abinit_optic' run is supported.

        Parameters
        ----------
        path: str
            The path to the calculation to load the DielectricTensor from.
        """
        with CalculationDirectory.from_calculation(path) as calc:
            # first check the calctype is supported
            if calc.calctype != "abinit_optic":
                raise NotImplementedError(calc.calctype)
            # there might be more than one files within a single calculation
            # this, inititate from calculation and iterate through the list
            files_ = AbinitOpticLincompFile.from_calculation(calc)
            return cls.from_handlers(files_, *args, **kwargs)

    @classmethod
    def from_file(cls, path, *args, **kwargs):
        """Creates a DielectricTensor object from a file.

        Currently, only 'linopt' files from an 'abinit_optic' calculation is
        supported.

        Parameters
        ----------
        path: str
            The file path.
        """
        return cls.from_handlers(
                (AbinitOpticLincompFile.from_file(path)), *args, **kwargs)
