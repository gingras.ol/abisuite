import os

import numpy as np
import tqdm

from .bases import BaseAbisuiteSubScript
from .routines import walltime_to_str
from .. import __SYSTEM_QUEUE__
from ..colors import Colors
from ..status_checkers.bases import (
        BaseSCFCalculationStatusChecker, __ERROR_STRING__,
        )
from ..utils import TerminalTable
from ..routines import is_scalar


class AbisuiteStatusScript(BaseAbisuiteSubScript):
    """Executes the 'status' subcommand of the abisuite script.

    Basically prints out a nicely formatted table of a given set
    of calculations.
    """
    _loggername = "AbisuiteStatusScript"

    def load_calculations(self, *args, **kwargs):
        super().load_calculations(*args, follow_symlinks=True, **kwargs)

    # TODO: shorten this method
    def status(self, sort_by_walltime=False):
        """Prints out a nicely formatted table of the given set of
        calculations.

        Parameters
        ----------
        sort_by_walltime: bool, optional
            If True, the table will be sorted by walltimes.
        """
        pwd = os.getcwd()
        columns = ["Calculation(s)", "status", "convergence", "walltime"]
        column_alignments = ["left", "center", "center", "center"]
        data = []
        max_walltime = 0  # maximum walltime
        print("Computing status table. This may take a while...")
        for calculation in tqdm.tqdm(
                self.calculations, desc="Getting status", unit="calcs",
                total=len(self.calculations)):
            isscf = isinstance(
                    calculation.status_checker,
                    BaseSCFCalculationStatusChecker)
            # connect to db in order to load status from db in case it was
            # previously computed
            calculation.connect_to_database()
            with calculation as calc:
                if isscf:
                    if calc.status["calculation_converged"] == "non-scf":
                        # exceptional cases =>
                        # BaseSCFCalculationStatusChecker inst
                        # but non-scf in this entry
                        isscf = False
                row = [os.path.relpath(calc.path, pwd)]
                if calc.status["calculation_started"] is False:
                    row.append(
                            Colors.color_text("NOT STARTED", "cyan", "bold"))
                else:
                    # calculation started
                    if calc.status["calculation_finished"] is False:
                        row.append(Colors.color_text(
                            "NOT FINISHED", "yellow", "bold"))
                    elif (calc.status["calculation_finished"] ==
                          __ERROR_STRING__):
                        # check if calc is also running
                        msg = Colors.color_text("ERROR", "red", "bold")
                        with __SYSTEM_QUEUE__ as queue:
                            if calc in queue:
                                msg += "/" + Colors.color_text(
                                        "RUNNING", "yellow", "bold")
                        row.append(msg)
                    else:
                        row.append(
                                Colors.color_text(
                                    "COMPLETED", "green", "bold"))
                if not isscf:
                    row.append(Colors.color_text("not SCF", "blue", "bold"))
                else:
                    if calc.status["calculation_finished"] is not True:
                        row.append(Colors.color_text("not available", "bold"))
                    else:
                        # get convergence
                        if calc.status["calculation_converged"] is True:
                            row.append(Colors.color_text(
                                "REACHED", "green", "bold"))
                        elif (calc.status["calculation_converged"] ==
                              __ERROR_STRING__):
                            row.append(Colors.color_text(
                                "ERROR", "red", "bold"))
                        else:
                            row.append(Colors.color_text(
                                "NOT REACHED", "red", "bold"))
                # compute walltime
                if calc.status["calculation_finished"] is not True:
                    # walltime not available
                    row.append(Colors.bold_text("not available"))
                else:
                    try:
                        walltime = calc.walltime
                    except Exception:
                        self._logger.error(
                                "An error occured while getting walltime for "
                                f"Calc: {calc.path}.")
                        walltime = __ERROR_STRING__
                    if walltime is None:
                        # for some reason, walltime is not available
                        row.append(Colors.bold_text("not available"))
                    elif walltime == __ERROR_STRING__:
                        row.append(
                                Colors.color_text(
                                    "not available", "yellow", "bold"))
                    else:
                        row.append(walltime)
                        max_walltime = max((max_walltime, walltime))
                data.append(row)
            calculation.clear_content()
        # iterate over walltimes to format it in a more readable way
        walltimes = [row[-1] for row in data]
        # MANAGE SORTING HERE
        if sort_by_walltime:
            sorted_indices = np.argsort(walltimes)
            print("Sorting by walltimes.")
        else:
            sorted_indices = list(range(len(walltimes)))
        for row, walltime in zip(data, walltimes):
            if not is_scalar(walltime):
                continue
            row[-1] = walltime_to_str(walltime, max_walltime=max_walltime)
        table = TerminalTable(
                columns, column_alignments=column_alignments,
                bold_column_headers=True, loglevel=self._loglevel)
        for index in sorted_indices:
            table.add_row(data[index])
        table.print()
