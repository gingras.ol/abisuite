from .bases import BaseQEInputParalApprover
from ..bases import BaseInputApprover
from ...file_structures import QEPHInputStructure
from ....variables import ALL_QEPH_VARIABLES


class QEPHInputApprover(BaseInputApprover):
    """Class that checks the input variables for a ph.x calculation.
    """
    _loggername = "QEPHInputApprover"
    _variables_db = ALL_QEPH_VARIABLES
    _structure_class = QEPHInputStructure

    def validate(self):
        super().validate()
        self._check_qpts_splitting()
        self._check_ldisp()

    def _check_ldisp(self):
        ldisp = self.input_variables.get("ldisp", False).value
        if ldisp:
            for nqvar in ["nq1", "nq2", "nq3"]:
                nq = self.input_variables.get(nqvar, 0).value
                if not nq:
                    self.errors.append(
                            f"ldisp = True => '{nqvar}' must be > 0.")

    def _check_qpts_splitting(self):
        iv = self.input_variables
        if "start_q" not in iv and "last_q" not in iv:
            # nothing to do
            return
        if "start_q" not in iv:
            self.errors.append("'start_q' undefined whereas 'last_q is.")
            return
        if "last_q" not in iv:
            self.errors.append("'last_q' undefined whereas 'start_q is.")
            return
        start_q = iv["start_q"]
        last_q = iv["last_q"]
        if start_q > last_q:
            self.errors.append(f"start_q={start_q}>{last_q}=last_q")
            return
        if "nq1" not in iv or "nq2" not in iv or "nq3" not in iv:
            self.errors.append("missing one or more 'nq*'")
            return
        maxnqs = iv["nq1"] * (iv["nq2"] * iv["nq3"])
        if start_q > maxnqs or last_q > maxnqs:
            self.errors.append("start_q and last_q must be less than total"
                               f" possible # of qpts which is {maxnqs}")
        ldisp = iv.get("ldisp", None)
        if ldisp is None or ldisp is False:
            self.errors.append("ldisp must be set to True with last_q and "
                               "start_q to work.")


class QEPHInputParalApprover(BaseQEInputParalApprover):
    """Class that cross checks the input parameters of the ph.x script
    with the mpi command settings.
    """
    _loggername = "QEPHInputParalApprover"
    _input_approver_class = QEPHInputApprover
    _structure_class = QEPHInputStructure

    def validate(self, *args, **kwargs):
        super().validate(*args, **kwargs)
        self._check_nimage_vs_dvscf()

    def _check_nimage_vs_dvscf(self):
        # see:
        # https://gitlab.com/QEF/q-e/blob/develop/PHonon/PH/phq_readin.f90#L739
        if self.command_arguments.get("-nimage", 1) > 1:
            if self.input_variables.get("fildvscf", " ") != " ":
                self.errors.append(
                        "Multiple images not surported when writing dvscf "
                        "files.")
