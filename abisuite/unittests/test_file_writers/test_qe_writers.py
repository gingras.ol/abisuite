import unittest

from .bases import BaseInputWriterTest
from ..variables_for_tests import (
        qe_dosx_vars,
        qe_dynmatx_vars,
        qe_epsilonx_vars, qe_epwx_vars,
        qe_fsx_vars, qe_ld1x_vars, qe_matdynx_vars,
        qe_phx_vars, qe_ppx_vars, qe_projwfcx_vars,
        qe_pwx_vars, qe_pw2wannier90x_vars, qe_q2rx_vars,
        )
from ...handlers.file_parsers import (
        QEDOSInputParser, QEDynmatInputParser,
        QEEpsilonInputParser, QEEPWInputParser, QEFSInputParser,
        QELD1InputParser,
        QEMatdynInputParser,
        QEPHInputParser, QEPPInputParser, QEProjwfcInputParser,
        QEPWInputParser, QEPW2Wannier90InputParser, QEQ2RInputParser,
        )
from ...handlers.file_writers import (
        QEDOSInputWriter, QEDynmatInputWriter, QEEpsilonInputWriter,
        QEEPWInputWriter,
        QEFSInputWriter, QELD1InputWriter, QEMatdynInputWriter,
        QEPHInputWriter, QEPPInputWriter, QEProjwfcInputWriter,
        QEPWInputWriter, QEPW2Wannier90InputWriter, QEQ2RInputWriter,
        )


class TestQEDOSInputWriter(BaseInputWriterTest, unittest.TestCase):
    _input_vars = qe_dosx_vars.copy()
    _parser_class = QEDOSInputParser
    _writer_class = QEDOSInputWriter


class TestQEDynmatInputWriter(BaseInputWriterTest, unittest.TestCase):
    _input_vars = qe_dynmatx_vars.copy()
    _parser_class = QEDynmatInputParser
    _writer_class = QEDynmatInputWriter


class TestQEEpsilonInputWriter(BaseInputWriterTest, unittest.TestCase):
    _input_vars = qe_epsilonx_vars.copy()
    _parser_class = QEEpsilonInputParser
    _writer_class = QEEpsilonInputWriter


class TestQEEPWInputWriter(BaseInputWriterTest, unittest.TestCase):
    _input_vars = qe_epwx_vars.copy()
    _parser_class = QEEPWInputParser
    _writer_class = QEEPWInputWriter


class TestQEFSInputWriter(BaseInputWriterTest, unittest.TestCase):
    _input_vars = qe_fsx_vars.copy()
    _parser_class = QEFSInputParser
    _writer_class = QEFSInputWriter


class TestQELD1InputWriter(BaseInputWriterTest, unittest.TestCase):
    _input_vars = qe_ld1x_vars.copy()
    _parser_class = QELD1InputParser
    _writer_class = QELD1InputWriter


class TestQEMatdynInputWriter(BaseInputWriterTest, unittest.TestCase):
    _input_vars = qe_matdynx_vars.copy()
    _parser_class = QEMatdynInputParser
    _writer_class = QEMatdynInputWriter


class TestQEPHInputWriter(BaseInputWriterTest, unittest.TestCase):
    _input_vars = qe_phx_vars.copy()
    _parser_class = QEPHInputParser
    _writer_class = QEPHInputWriter


class TestQEPPInputWriter(BaseInputWriterTest, unittest.TestCase):
    _input_vars = qe_ppx_vars.copy()
    _parser_class = QEPPInputParser
    _writer_class = QEPPInputWriter


class TestQEProjwfcInputWriter(BaseInputWriterTest, unittest.TestCase):
    _input_vars = qe_projwfcx_vars.copy()
    _parser_class = QEProjwfcInputParser
    _writer_class = QEProjwfcInputWriter


class TestQEPWInputWriter(BaseInputWriterTest, unittest.TestCase):
    _input_vars = qe_pwx_vars.copy()
    _parser_class = QEPWInputParser
    _writer_class = QEPWInputWriter


class TestQEPW2Wannier90InputWriter(BaseInputWriterTest, unittest.TestCase):
    _input_vars = qe_pw2wannier90x_vars.copy()
    _parser_class = QEPW2Wannier90InputParser
    _writer_class = QEPW2Wannier90InputWriter


class TestQEQ2RInputWriter(BaseInputWriterTest, unittest.TestCase):
    _input_vars = qe_q2rx_vars.copy()
    _parser_class = QEQ2RInputParser
    _writer_class = QEQ2RInputWriter
