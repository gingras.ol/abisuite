from .bases import BaseQEInputParser
from ...file_structures import QEDOSInputStructure


class QEDOSInputParser(BaseQEInputParser):
    """Class that can parse a Quantum Espresso dos.x input File.
    """
    _loggername = "QEDOSInputParser"
    _structure_class = QEDOSInputStructure
