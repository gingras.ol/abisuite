import abc
import os
import subprocess

from .job import QueuedJob, __ALL_JOB_ATTRIBUTES__
from ..bases import BaseUtility
from ..exceptions import DevError
from ..routines import check_output_with_retries
from ..linux_tools import which


__JOBID_KEY__ = "id"  # define this var cause it's used elsewhere
__SLURM_QUEUE_HEADER_TITLES__ = {
    __JOBID_KEY__: "JOBID",
    "username": "USER",
    "account": "ACCOUNT",
    "jobname": "NAME",
    "status": "STATE",
    "time_left": "TIME_LEFT",
    "nodes": "NODES",
    "proc": "CPUS",
    "workdir": "WORK_DIR"
    }


class BaseQueueParser(BaseUtility, abc.ABC):
    """Base class for all queue parsers.
    """
    cancel_command = None
    queue_command = None
    _header_titles = None

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.username = None
        self.jobs = []
        if self.cancel_command is None:
            raise DevError("Need to set the 'cancel_command' attribute.")
        if self.queue_command is None:
            raise DevError("Need to set 'queue_command' class attribute.")
        if self._header_titles is None:
            raise DevError("Need to set 'header_titles' class attribute.")
        # check the dict to make sure the API is consistent
        for attr in self._header_titles:
            if attr not in __ALL_JOB_ATTRIBUTES__:
                raise DevError(f"Invalid job attribute: '{attr}'.")
        # check command queue command is accessible
        if not os.path.isabs(self.queue_command):
            if which(self.queue_command) is None:
                raise FileNotFoundError(f"'{self.queue_command}' not found.")
        else:
            if not os.path.isfile(self.queue_command):
                raise FileNotFoundError(f"'{self.queue_command}' not found.")

    @property
    def njobs(self):
        return len(self.jobs)

    def read_queue(self):
        if self.username is None:
            raise AttributeError("Need to set username!")
        args = self.get_queue_cmd_args()
        output = check_output_with_retries(
                [self.queue_command, *args],
                allowed_exceptions=[subprocess.CalledProcessError]
                ).decode("utf-8")
        self.analyse_queue_output(output)

    def analyse_queue_output(self, output):
        """Analyse the queue output.

        Parameters
        ----------
        output : str
            The queue command output.
        """
        splitted = output.split("\n")
        splitted = [x.strip("\n").strip().strip('"').strip()
                    for x in splitted]  # clean lines
        splitted = [x for x in splitted if len(x)]  # remove empty lines
        if len(splitted) <= 1:
            # no jobs
            return []
        return self._do_job_analysis(splitted)

    def cancel_job(self, job_id):
        """Cancels a job by its id.

        Parameter
        ---------
        job_id: int/str
            The job id to cancel.
        """
        check_output_with_retries(
                [self.cancel_command, str(job_id)],
                allowed_exceptions=[subprocess.CalledProcessError])

    def _get_col_indices(self, hdr_line):
        # E.G.:
        # JOBID     USER      ACCOUNT           NAME  ST  TIME_LEFT NODES CPUS       GRES MIN_MEM NODELIST (REASON)  # noqa
        split = hdr_line.split()
        cols = {}
        for tag, title in self._header_titles.items():
            if title is None:
                # don't look for this title
                continue
            try:
                cols[tag] = split.index(title)
            except ValueError:
                # title name not found in queue
                self._logger.warning(
                        f"Could not find column indice of: '{tag}:{title}' "
                        "in queue."
                        )
                # set to 'unknown' if we can't find it
                cols[tag] = "unknown"
        return cols

    def _get_hdr_line(self, lines):
        # return the header line
        for ind, line in enumerate(lines):
            if self._header_titles[__JOBID_KEY__] in line:
                return line, ind
        raise LookupError("Could not find the header line in the queue string."
                          )

    def _do_job_analysis(self, lines):
        # exemple on slurm:
        # squeue --user=####### -o "%.18i %a %.8j %.8u %.2L %.10M %.6D %R %Z"
        # will produce output like:
        # JOBID     USER      ACCOUNT           NAME  ST  TIME_LEFT NODES CPUS       GRES MIN_MEM NODELIST (REASON)  # noqa
        # 2292966    olimt rrg-cotemich             sh   R    2:58:27     1    1     (null)   1000M blg8419 (None)   # noqa
        # 2292714 fgoudrea rrg-cotemich             sh  PD    5:00:00     1    4     (null)   1000M  (Resources)     # noqa
        # 2292951 dgendron rrg-cotemich           bs00  PD    4:00:00     2   32     (null)      4G  (Resources)     # noqa
        # 2292846    olimt def-cotemich    para_2_1024  PD 1-00:00:00     1    1 gpu:v100:2     15G  (Resources)     # noqa
        # 2292922    olimt def-cotemich           ani1  PD 2-00:00:00     1    1 gpu:v100:1     40G  (Priority)      # noqa
        # the queue list starts after the line with 'JOBID'
        # parse colunns (they might be swapped due to user config)
        # first find the header line
        hdr_line, hdr_index = self._get_hdr_line(lines)
        # get column indices
        cols = self._get_col_indices(hdr_line)
        for line in lines[hdr_index + 1:]:
            # split job line and create job object
            split = line.split()
            job = QueuedJob(loglevel=self._loglevel)
            for attr in self._header_titles:
                to_set = cols[attr]
                if isinstance(to_set, int):
                    to_set = split[to_set]
                setattr(job, attr, to_set)
            self.jobs.append(job)

    @abc.abstractmethod
    def get_queue_cmd_args(self, *args, **kwargs):
        pass


class PBSQueueParser(BaseQueueParser):
    """Queue parser for a pbs queuing system.
    """
    queue_command = "qstat"
    _loggername = "PBSQueueParser"


class SlurmQueueParser(BaseQueueParser):
    """Queue parser for a slurm queuing system.
    """
    cancel_command = "scancel"
    queue_command = "squeue"
    _loggername = "SlurmQueueParser"
    _header_titles = __SLURM_QUEUE_HEADER_TITLES__

    def get_queue_cmd_args(self):
        return [f'--user={self.username}',
                r'-o "%.18i %a %j %u %T %L %.10M %.6D %C %Z"']
