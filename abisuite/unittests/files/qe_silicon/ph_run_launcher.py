from abisuite import QEPHLauncher as Launcher


variables = {"amass(1)": 28.086,
             "ldisp": True,
             "nq1": 2,
             "nq2": 2,
             "nq3": 2}

launcher = Launcher("ph_run")
launcher.workdir = "ph_run"
launcher.link_calculation("pw_run")
launcher.mpi_command = "mpirun -np 4"
launcher.command_arguments = "-npool 4"
launcher.input_variables = variables
launcher.write()
launcher.run()
