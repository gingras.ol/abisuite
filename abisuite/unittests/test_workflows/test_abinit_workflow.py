import os
import pytest
import shutil
import tempfile

from abisuite import AbinitWorkflow
from .bases import BaseWorkflowTest, __TEST_WORKFLOW_ROOT__
from ..bases import TestCase
from ..routines_for_tests import copy_calculation
from ...handlers import CalculationDirectory, is_calculation_directory


# ############################  GS ############################################
ABINIT_GS_INPUT_VARS = {
        "acell": [7.2078778121] * 3,
        "rprim": [[0.0, 0.70710678119, 0.70710678119],
                  [0.70710678119, 0.0, 0.70710678119],
                  [0.70710678119, 0.70710678119, 0.0]],
        "ntypat": 1,
        "znucl": [14],
        "natom": 2,
        "typat": [1, 1],
        "xred": [[0.0, 0.0, 0.0], [1/4, 1/4, 1/4]],
        "ecut": 10.0,
        "kptopt": 1,
        "ngkpt": [6, 6, 6],
        "nshiftk": 4,
        "shiftk": [[0.5, 0.5, 0.5], [0.5, 0.0, 0.0],
                   [0.0, 0.5, 0.0], [0.0, 0.0, 0.5]],
        "nstep": 10,
        "toldfe": 1.0e-6,
        "diemac": 12.0,
        }
ABINIT_PSEUDOS = [os.path.join(
    __TEST_WORKFLOW_ROOT__, "test_files", "pseudos", "Si.psp8")
    ]
ABINIT_GS_EXAMPLE = os.path.join(
        __TEST_WORKFLOW_ROOT__,
        "test_files", "abinit_si", "nc", "gs_run")

# ############################# GS ECUT CONVERGENCE ###########################
# Make the test with PAW since there is 2 convergence to do
ABINIT_GS_ECUT_CONVERGENCE_INPUT_VARIABLES = {
        "acell": [10.18] * 3,
        "rprim": [[0.0, 0.5, 0.5], [0.5, 0.0, 0.5], [0.5, 0.5, 0.0]],
        "ntypat": 1,
        "znucl": [14],
        "natom": 2,
        "typat": [1, 1],
        "xred": [[0.0, 0.0, 0.0], [1/4, 1/4, 1/4]],
        "kptopt": 1,
        "pawecutdg": 30.0,
        "ngkpt": [2, 2, 2],
        "nshiftk": 4,
        "shiftk": [[0.5, 0.5, 0.5], [0.5, 0.0, 0.0],
                   [0.0, 0.5, 0.0], [0.0, 0.0, 0.5]],
        "nstep": 50,
        "tolvrs": 1.0e-10,
        "diemac": 12.0,
        }
ABINIT_GS_ECUT_CONVERGENCE_ECUTS = [10, 20, 30]
ABINIT_GS_ECUT_CONVERGENCE_PAWECUTS = [20, 30, 50]
ABINIT_GS_ECUT_CONVERGENCE_CRITERION = 10
ABINIT_GS_PAWECUT_CONVERGENCE_CRITERION = 10
ABINIT_GS_ECUT_CONVERGENCE_EXAMPLE = os.path.join(
        __TEST_WORKFLOW_ROOT__,
        "test_files", "abinit_si", "paw", "gs_ecut_convergence")
ABINIT_GS_PAWECUT_CONVERGENCE_EXAMPLE = os.path.join(
        __TEST_WORKFLOW_ROOT__,
        "test_files", "abinit_si", "paw", "gs_ecut_convergence", "pawecutdg")

# ######################## GS KGRID CONVERGENCE ###############################
ABINIT_GS_KGRID_CONVERGENCE_INPUT_VARS = {
         "acell": [10.18] * 3,
         "rprim": [[0.0, 0.5, 0.5], [0.5, 0.0, 0.5], [0.5, 0.5, 0.0]],
         "ntypat": 1,
         "znucl": [14],
         "natom": 2,
         "typat": [1, 1],
         "xred": [[0.0, 0.0, 0.0], [1/4, 1/4, 1/4]],
         "kptopt": 1,
         "nshiftk": 4,
         "shiftk": [[0.5, 0.5, 0.5], [0.5, 0.0, 0.0],
                    [0.0, 0.5, 0.0], [0.0, 0.0, 0.5]],
         "nstep": 10,
         "ecut": 20,
         "toldfe": 1.0e-6,
         "diemac": 12.0,
         }
ABINIT_GS_KGRID_CONVERGENCE_CRITERION = 1
ABINIT_GS_KGRID_CONVERGENCE_KGRIDS = [[2, 2, 2], [4, 4, 4],
                                      [6, 6, 6], [8, 8, 8]]
ABINIT_GS_KGRID_CONVERGENCE_VARNAME = "ngkpt"
ABINIT_GS_KGRID_CONVERGENCE_EXAMPLE = os.path.join(
        __TEST_WORKFLOW_ROOT__, "test_files", "abinit_si", "nc",
        "kgrid_convergence")

# ######################## SMEARING CONVERGENCE ###############################
ABINIT_GS_SMEARING_CONVERGENCE_INPUT_VARS = {
        "acell": [7.6] * 3,
        "rprim": [[0, 0.5, 0.5], [0.5, 0, 0.5], [0.5, 0.5, 0]],
        "ntypat": 1,
        "ecut": 20,
        "znucl": [13],
        "natom": 1,
        "typat": 1,
        "xred": [0, 0, 0],
        "nshiftk": 4,
        "shiftk": [[0.5, 0.5, 0.5], [0.5, 0.0, 0.0], [0, 0.5, 0], [0, 0, 0.5]],
        "nstep": 10,
        "toldfe": 1e-6,
        "occopt": 4,
        }
ABINIT_GS_SMEARING_CONVERGENCE_CRITERION = 10
ABINIT_GS_SMEARING_CONVERGENCE_KGRIDS = [[x, x, x] for x in [2, 4, 8]]
ABINIT_GS_SMEARING_CONVERGENCE_VARNAME = "ngkpt"
ABINIT_GS_SMEARING_CONVERGENCE_SMEARINGS = [0.1, 0.01, 0.001]
ABINIT_GS_SMEARING_CONVERGENCE_EXAMPLE = os.path.join(
        __TEST_WORKFLOW_ROOT__, "test_files", "abinit_Al",
        "gs_smearing_convergence")
ABINIT_GS_SMEARING_CONVERGENCE_PSEUDOS = [os.path.join(
    __TEST_WORKFLOW_ROOT__, "test_files", "pseudos", "Al.psp8")
    ]

# ############################ RELAXATION #####################################
ABINIT_RELAXATION_INPUT_VARIABLES = {
        "acell": [10.18] * 3,
        "rprim": [[0.0, 0.5, 0.5], [0.5, 0.0, 0.5],
                  [0.5, 0.5, 0.0]],
        "ntypat": 1,
        "znucl": [14],
        "natom": 2,
        "typat": [1, 1],
        "xred": [[0.0, 0.0, 0.0], [1/4, 1/4, 1/4]],
        "kptopt": 1,
        "nshiftk": 4,
        "shiftk": [[0.5, 0.5, 0.5], [0.5, 0.0, 0.0],
                   [0.0, 0.5, 0.0], [0.0, 0.0, 0.5]],
        "ngkpt": [6, 6, 6],
        "ecut": 20.0,
        "nstep": 10,
        "tolvrs": 1.0e-10,
        "diemac": 12.0,
        "optcell": 1,  # only modify acell
        "ionmov": 22,
        "ntime": 20,
        "tolmxf": 5e-04,
        "ecutsm": 0.5,
        "dilatmx": 1.1,
        }
ABINIT_RELAXATION_EXAMPLE = os.path.join(
        __TEST_WORKFLOW_ROOT__, "test_files", "abinit_si", "nc", "relax_run")


# ######################### PHONON ECUT CONVERGENCE PART ######################
# Al example
ABINIT_PHONON_ECUT_CONVERGENCE_PSEUDOS = [os.path.join(
    __TEST_WORKFLOW_ROOT__, "test_files", "pseudos", "Al.psp8")
    ]
ABINIT_PHONON_ECUT_CONVERGENCE_CRITERION = 10
ABINIT_PHONON_ECUT_CONVERGENCE_SCF_INPUT_VARS = {
            "acell": [5.326968652] * 3,
            "rprim": [[0.0, 0.70710678119, 0.70710678119],
                      [0.70710678119, 0.0, 0.70710678119],
                      [0.70710678119, 0.70710678119, 0.0]],
            "ntypat": 1,
            "occopt": 4,
            "znucl": [13],
            "natom": 1,
            "typat": [1],
            "xred": [[0.0, 0.0, 0.0]],
            "tsmear": 0.1,
            "nshiftk": 4,
            "shiftk": [[0.5, 0.5, 0.5], [0.5, 0.0, 0.0],
                       [0.0, 0.5, 0.0], [0.0, 0.0, 0.5]],
            "nstep": 10,
            "toldfe": 1.0e-6,
            "ngkpt": [4, 4, 4],
            }
ABINIT_PHONON_ECUT_CONVERGENCE_PHONONS_INPUT_VARS = {
            "rfatpol": [1, 1],
            "rfdir": [1, 1, 1],
            "tolvrs": 1e-8,
            "nstep": 25,
            }
ABINIT_PHONON_ECUT_CONVERGENCE_ECUTS = [10, 30, 50]
ABINIT_PHONON_ECUT_CONVERGENCE_PHONON_QPT = [0.0, 0.0, 0.0]
ABINIT_PHONON_ECUT_CONVERGENCE_EXAMPLE = os.path.join(
        __TEST_WORKFLOW_ROOT__, "test_files", "abinit_Al",
        "phonon_ecut_convergence")
ABINIT_PHONON_ECUT_CONVERGENCE_OTHER_KWARGS = {
        "compute_electric_field_response": False,
        }

# #################### PHONON DISPERSION QGRID CONVERGENCE ####################
ABINIT_PHONON_DISPERSION_QGRID_CONVERGENCE_EXAMPLE = os.path.join(
        __TEST_WORKFLOW_ROOT__, "test_files", "abinit_si", "nc",
        "phonon_dispersion_qgrid_convergence")
ABINIT_PHONON_DISPERSION_QGRID_CONVERGENCE_QGRIDS = [[x] * 3 for x in [2, 6]]

# ########################## PHONON DISPERSION ################################
ABINIT_PHONON_DISPERSION_QPOINT_PATH = [
            {"L": [0.5, 0.0, 0.0]},
            {r"$\Gamma$": [0.0, 0.0, 0.0]},
            {"X": [0.0, 0.5, 0.5]},
            {r"$\Gamma_2$": [1.0, 1.0, 1.0]}]
ABINIT_PHONON_DISPERSION_QPOINT_PATH_DENSITY = 20
ABINIT_PHONON_DISPERSION_PHONONS_INPUT_VARS = {
            "rfatpol": [1, 2],
            "tolvrs": 1e-8,
            "diemac": 12.0,
            "nstep": 25,
            }
ABINIT_PHONON_DISPERSION_QGRID = [6] * 3
ABINIT_PHONON_DISPERSION_EXAMPLE = os.path.join(
        __TEST_WORKFLOW_ROOT__, "test_files", "abinit_si", "nc",
        "phonon_dispersion")
ABINIT_PHONON_DISPERSION_PSEUDOS = [
    os.path.join(__TEST_WORKFLOW_ROOT__, "test_files", "pseudos", "Si.psp8"),
    ]


# ################### PHONON SMEARING CONVERGENCE #############################
ABINIT_PHONON_SMEARING_CONVERGENCE_SCF_INPUT_VARS = {
            "nshiftk": 4,
            "shiftk": [[0.5, 0.5, 0.5], [0.5, 0.0, 0.0],
                       [0, 0.5, 0], [0, 0, 0.5]],
            "nstep": 10,
            "toldfe": 1e-6,
            "occopt": 4,
            "acell": [5.326968652] * 3,
            "natom": 1,
            "ntypat": 1,
            "rprim": [[0.0, 0.70710678119, 0.70710678119],
                      [0.70710678119, 0.0, 0.70710678119],
                      [0.70710678119, 0.70710678119, 0.0]],
            "typat": 1,
            "ecut": 30,
            "xred": [0, 0, 0],
            "znucl": [13.0],
            }
ABINIT_PHONON_SMEARING_CONVERGENCE_PHONONS_INPUT_VARS = {
            "rfatpol": [1, 1],
            "rfdir": [1, 1, 1],
            "tolvrs": 1e-8,
            "nstep": 25}
ABINIT_PHONON_SMEARING_CONVERGENCE_EXAMPLE = os.path.join(
        __TEST_WORKFLOW_ROOT__, "test_files", "abinit_Al",
        "phonon_smearing_convergence")
ABINIT_PHONON_SMEARING_CONVERGENCE_SMEARINGS = [0.1, 0.01, 0.001]
ABINIT_PHONON_SMEARING_CONVERGENCE_KGRIDS = [[x, x, x] for x in [2, 4, 8]]
ABINIT_PHONON_SMEARING_CONVERGENCE_OTHER_KWARGS = {
        "scf_kgrids_input_variable_name": "ngkpt"}
ABINIT_PHONON_SMEARING_CONVERGENCE_CRITERION = 10
ABINIT_PHONON_SMEARING_CONVERGENCE_PHONONS_QPT = [0.0, 0.0, 0.0]


# ########################### BAND STRUCTURE ##################################
ABINIT_BAND_STRUCTURE_EXAMPLE = os.path.join(
        __TEST_WORKFLOW_ROOT__, "test_files", "abinit_si", "nc",
        "band_structure", "band_structure")
ABINIT_BAND_STRUCTURE_INPUT_VARIABLES = {
        "ecut": 20,
        "iscf": -2,
        "nband": 8,
        "tolwfr": 1e-12,
        "autoparal": 1,
        "diemac": 12.0,
        }
ABINIT_BAND_STRUCTURE_KPOINT_PATH = [
        {"L": [0.5, 0.0, 0.0]},
        {r"$\Gamma$": [0.0, 0.0, 0.0]},
        {"X": [0.0, 0.5, 0.5]},
        {r"$\Gamma_2$": [1.0, 1.0, 1.0]}]
ABINIT_BAND_STRUCTURE_KPOINT_PATH_DENSITY = 20

# ######################## OPTIC NSCF CONVERGENCE #############################
ABINIT_OPTIC_NSCF_CONVERGENCE_EXAMPLE = os.path.join(
        __TEST_WORKFLOW_ROOT__, "test_files", "abinit_si", "nc",
        "optic_nscf_convergence",
        )
ABINIT_OPTIC_NSCF_CONVERGENCE_NSCF_KPOINT_GRIDS = [
        [x, x, x] for x in [6, 8]]
ABINIT_OPTIC_NSCF_CONVERGENCE_EXPERIMENTAL_BANDGAP = 1.14
ABINIT_OPTIC_NSCF_CONVERGENCE_NSCF_KPOINT_GRIDS_INPUT_VAR_NAME = "ngkpt"
ABINIT_OPTIC_NSCF_CONVERGENCE_NSCF_INPUT_VARIABLES = {
        "nstep": 20, "tolwfr": 1e-14, "nbdbuf": 2, "nband": 10,
        }
ABINIT_OPTIC_NSCF_CONVERGENCE_DDK_INPUT_VARIABLES = {
        "rfdir": [1, 1, 1],
        }
ABINIT_OPTIC_NSCF_CONVERGENCE_OPTIC_INPUT_VARIABLES = {
        "num_lin_comp": 6, "lin_comp": [11, 12, 13, 22, 23, 33],
        "num_nonlin_comp": 0, "num_nonlin2_comp": 0, "num_linel_comp": 0,
        "broadening": 1e-3, "domega": 0.1, "maxomega": 1,
        # manually set scissor as it can cause problems when testing
        # with different version of pythons or on different machines
        # most likely due to rounding errors (FG: 2021/07/28)
        "scissor": 0.02524746980237357,
        "tolerance": 1e-3}
ABINIT_OPTIC_NSCF_CONVERGENCE_PSEUDOS = ABINIT_PSEUDOS


@pytest.mark.order("first")
class TestAbinitWorkflow(BaseWorkflowTest, TestCase):
    """Test case for AbinitWorkflow class.
    """
    _band_structure_example = ABINIT_BAND_STRUCTURE_EXAMPLE
    _band_structure_input_variables = ABINIT_BAND_STRUCTURE_INPUT_VARIABLES
    _band_structure_kpoint_path = ABINIT_BAND_STRUCTURE_KPOINT_PATH
    _band_structure_kpoint_path_density = (
            ABINIT_BAND_STRUCTURE_KPOINT_PATH_DENSITY)
    _gs_ecut_convergence_criterion = ABINIT_GS_ECUT_CONVERGENCE_CRITERION
    _gs_ecut_convergence_input_vars = (
            ABINIT_GS_ECUT_CONVERGENCE_INPUT_VARIABLES)
    _gs_ecut_convergence_ecuts = ABINIT_GS_ECUT_CONVERGENCE_ECUTS
    _gs_ecut_convergence_example = ABINIT_GS_ECUT_CONVERGENCE_EXAMPLE
    _gs_ecut_convergence_other_kwargs = {
            "scf_pawecutdg": ABINIT_GS_ECUT_CONVERGENCE_PAWECUTS,
            "scf_pawecutdg_convergence_criterion": (
                ABINIT_GS_ECUT_CONVERGENCE_CRITERION),
            }
    _gs_kgrid_convergence_criterion = ABINIT_GS_KGRID_CONVERGENCE_CRITERION
    _gs_kgrid_convergence_example = ABINIT_GS_KGRID_CONVERGENCE_EXAMPLE
    _gs_kgrid_convergence_input_vars = ABINIT_GS_KGRID_CONVERGENCE_INPUT_VARS
    _gs_kgrid_convergence_kgrids = ABINIT_GS_KGRID_CONVERGENCE_KGRIDS
    _gs_kgrid_convergence_other_kwargs = {
            "scf_kgrids_input_variable_name": (
                ABINIT_GS_KGRID_CONVERGENCE_VARNAME),
            }
    _gs_smearing_convergence_criterion = (
            ABINIT_GS_SMEARING_CONVERGENCE_CRITERION)
    _gs_smearing_convergence_example = ABINIT_GS_SMEARING_CONVERGENCE_EXAMPLE
    _gs_smearing_convergence_input_vars = (
            ABINIT_GS_SMEARING_CONVERGENCE_INPUT_VARS.copy())
    _gs_smearing_convergence_kgrids = ABINIT_GS_SMEARING_CONVERGENCE_KGRIDS
    _gs_smearing_convergence_smearings = (
            ABINIT_GS_SMEARING_CONVERGENCE_SMEARINGS)
    _gs_smearing_convergence_other_kwargs = {
            "scf_kgrids_input_variable_name": (
                ABINIT_GS_SMEARING_CONVERGENCE_VARNAME),
            }
    _gs_example = ABINIT_GS_EXAMPLE
    _gs_input_vars = ABINIT_GS_INPUT_VARS
    _phonon_ecut_convergence_criterion = (
            ABINIT_PHONON_ECUT_CONVERGENCE_CRITERION)
    _phonon_ecut_convergence_scf_input_vars = (
            ABINIT_PHONON_ECUT_CONVERGENCE_SCF_INPUT_VARS)
    _phonon_ecut_convergence_phonons_input_vars = (
            ABINIT_PHONON_ECUT_CONVERGENCE_PHONONS_INPUT_VARS)
    _phonon_ecut_convergence_ecuts = (
            ABINIT_PHONON_ECUT_CONVERGENCE_ECUTS)
    _phonon_ecut_convergence_example = (
            ABINIT_PHONON_ECUT_CONVERGENCE_EXAMPLE)
    _phonon_ecut_convergence_phonon_qpt = (
            ABINIT_PHONON_ECUT_CONVERGENCE_PHONON_QPT)
    _phonon_ecut_convergence_other_kwargs = (
            ABINIT_PHONON_ECUT_CONVERGENCE_OTHER_KWARGS)
    _phonon_dispersion_example = ABINIT_PHONON_DISPERSION_EXAMPLE
    _phonon_dispersion_phonons_input_vars = (
            ABINIT_PHONON_DISPERSION_PHONONS_INPUT_VARS)
    _phonon_dispersion_qgrid = ABINIT_PHONON_DISPERSION_QGRID
    _phonon_dispersion_qpoint_path = ABINIT_PHONON_DISPERSION_QPOINT_PATH
    _phonon_dispersion_qpoint_path_density = (
            ABINIT_PHONON_DISPERSION_QPOINT_PATH_DENSITY)
    _phonon_dispersion_qgrid_convergence_example = (
            ABINIT_PHONON_DISPERSION_QGRID_CONVERGENCE_EXAMPLE)
    _phonon_dispersion_qgrid_convergence_qgrids = (
            ABINIT_PHONON_DISPERSION_QGRID_CONVERGENCE_QGRIDS)
    _phonon_smearing_convergence_criterion = (
            ABINIT_PHONON_SMEARING_CONVERGENCE_CRITERION)
    _phonon_smearing_convergence_scf_input_vars = (
            ABINIT_PHONON_SMEARING_CONVERGENCE_SCF_INPUT_VARS)
    _phonon_smearing_convergence_phonons_input_vars = (
            ABINIT_PHONON_SMEARING_CONVERGENCE_PHONONS_INPUT_VARS)
    _phonon_smearing_convergence_example = (
            ABINIT_PHONON_SMEARING_CONVERGENCE_EXAMPLE)
    _phonon_smearing_convergence_other_kwargs = (
            ABINIT_PHONON_SMEARING_CONVERGENCE_OTHER_KWARGS)
    _phonon_smearing_convergence_phonon_qpt = (
            ABINIT_PHONON_SMEARING_CONVERGENCE_PHONONS_QPT)
    _phonon_smearing_convergence_smearings = (
            ABINIT_PHONON_SMEARING_CONVERGENCE_SMEARINGS)
    _phonon_smearing_convergence_kgrids = (
            ABINIT_PHONON_SMEARING_CONVERGENCE_KGRIDS)
    _phonons_script_name = "abinit"
    _relaxation_input_vars = ABINIT_RELAXATION_INPUT_VARIABLES
    _set_gs_calculation_kwargs = None
    _scf_script_name = "abinit"
    _workflow_cls = AbinitWorkflow
    _workflow_init_kwargs = {"pseudos": ABINIT_PSEUDOS}

    def setUp(self):
        super().setUp()
        self.mrgddb_command_file = tempfile.NamedTemporaryFile(
                suffix="mrgddb")
        self.anaddb_command_file = tempfile.NamedTemporaryFile(
                suffix="anaddb")
        self.optic_command_file = tempfile.NamedTemporaryFile(
                suffix="optic")

    def test_band_structure_convergence(self):
        workflow = self._workflow_cls(
                band_structure_convergence=True,
                **self._workflow_init_kwargs)
        self._set_band_structure_convergence(workflow)
        workflow.write()
        # make sure scf calcs were written
        seq = workflow.band_structure_convergence_sequencer
        ncalcs = len(seq.scf_specific_input_variables)
        dir_ = os.path.dirname(seq.scf_workdir)
        self.assertEqual(len(os.listdir(dir_)), ncalcs)
        # set done and check workflow completed
        shutil.rmtree(dir_)
        self._set_band_structure_convergence_done(workflow)
        workflow.run()
        # make sure there are 2ncalcs + 2 files in results dir
        results_dir = os.path.join(
                os.path.dirname(os.path.dirname(seq.scf_workdir)), "results")
        self.assertEqual(len(os.listdir(results_dir)), 2 * ncalcs + 2)
        self.assertTrue(workflow.workflow_completed)

    def test_dos(self):
        workflow = self._workflow_cls(
                dos=True,
                gs=True,
                band_structure=True,
                **self._workflow_init_kwargs)
        self._set_dos(workflow)
        workflow.write()
        # make sure dos calc was written
        self.assertTrue(
                CalculationDirectory.is_calculation_directory(
                    workflow.dos_sequencer.dos_workdir))
        # set done and make sure plots were written
        shutil.rmtree(workflow.dos_sequencer.dos_workdir)
        self._set_dos_done(workflow)
        workflow.run()
        results_dir = os.path.join(
                os.path.dirname(workflow.dos_sequencer.dos_workdir), "results")
        self.assertGreaterEqual(len(os.listdir(results_dir)), 4)

    def test_optic_nscf_convergence(self):
        workflow = self._workflow_cls(
                gs=True, band_structure=True,
                optic_nscf_convergence=True,
                **self._workflow_init_kwargs,
                )
        self._set_gs(workflow)
        self._set_gs_done(workflow)
        self._set_band_structure(workflow)
        self._set_band_structure_done(workflow)
        self._set_optic_nscf_convergence(workflow)
        workflow.write()
        sequencers = workflow.optic_nscf_convergence_sequencer

        def get_and_check(seq_part):
            dirs = [getattr(seq, f"{seq_part}_workdir") for seq in sequencers]
            for dir_ in dirs:
                self.assertTrue(is_calculation_directory(dir_))
                shutil.rmtree(dir_)

        get_and_check("nscf")
        self._set_optic_nscf_convergence_done(workflow, nscf=True)
        workflow.write()
        get_and_check("nscffbz")
        self._set_optic_nscf_convergence_done(workflow, nscffbz=True)
        workflow.write()
        get_and_check("ddk")
        self._set_optic_nscf_convergence_done(workflow, ddk=True)
        workflow.write()
        get_and_check("optic")
        self._set_optic_nscf_convergence_done(workflow, optic=True)
        workflow.optic_nscf_convergence_sequencer.clear_sequence()
        self.assertTrue(workflow.workflow_completed)
        workflow.run()
        main_res_dir = os.path.join(
                os.path.dirname(os.path.dirname(
                    sequencers[0].nscf_workdir)),
                "results")
        self.assertEqual(len(os.listdir(main_res_dir)), 24)

    def _check_phonon_dispersion_post_phonons(self, workflow):
        dir_ = os.path.join(
                    self.workflow_dir.name,
                    "phonon_dispersion")
        # make sure q2r calculation is written
        self.assertIn("mrgddb_run", os.listdir(dir_))
        shutil.rmtree(os.path.join(dir_, "mrgddb_run"))
        self._set_phonon_dispersion_done(workflow, mrgddb=True)
        workflow.write()
        # make sure the matdyn calc was written
        self.assertIn("anaddb_run", os.listdir(dir_))
        shutil.rmtree(os.path.join(dir_, "anaddb_run"))
        self._set_phonon_dispersion_done(workflow, anaddb=True)

    def _check_phonon_dispersion_qgrid_convergence_post_phonons(
            self, workflow):
        for seq in workflow.phonon_dispersion_qgrid_convergence_sequencer:
            dir_ = os.path.join(
                        self.workflow_dir.name,
                        "phonon_dispersion_qgrid_convergence",
                        os.path.basename(
                            os.path.dirname(
                                os.path.dirname(seq.phonons_workdir))))
            # make sure q2r calculation is written
            self.assertIn("mrgddb_run", os.listdir(dir_))
            shutil.rmtree(os.path.join(dir_, "mrgddb_run"))
        self._set_phonon_dispersion_qgrid_convergence_done(
                workflow, mrgddb=True)
        workflow.write()
        for seq in workflow.phonon_dispersion_qgrid_convergence_sequencer:
            dir_ = os.path.join(
                        self.workflow_dir.name,
                        "phonon_dispersion_qgrid_convergence",
                        os.path.basename(
                            os.path.dirname(
                                os.path.dirname(seq.phonons_workdir))))
            # make sure the matdyn calc was written
            self.assertIn("anaddb_run", os.listdir(dir_))
            shutil.rmtree(os.path.join(dir_, "anaddb_run"))
        self._set_phonon_dispersion_qgrid_convergence_done(
                workflow, anaddb=True)
        workflow.run()

    def _set_band_structure_convergence(self, workflow):
        calculation_parameters = {
                "command": self.scf_command_file.name,
                "queuing_system": "local",
                }
        workflow.set_band_structure_convergence(
                root_workdir=os.path.join(
                    self.workflow_dir.name, "band_structure_convergence"),
                scf_specific_input_variables=[
                    {"ecut": ecut} for ecut in [10, 30]],
                scf_input_variables={
                    "acell": [7.2078778121] * 3,
                    "rprim": [[0.0, 0.70710678119, 0.70710678119],
                              [0.70710678119, 0.0, 0.70710678119],
                              [0.70710678119, 0.70710678119, 0.0]],
                    "ntypat": 1,
                    "znucl": [14],
                    "natom": 2,
                    "typat": [1, 1],
                    "xred": [
                        [0.0, 0.0, 0.0],
                        [1/4, 1/4, 1/4]],
                    "kptopt": 1,
                    "ngkpt": [6, 6, 6],
                    "nshiftk": 4,
                    "shiftk": [[0.5, 0.5, 0.5], [0.5, 0.0, 0.0],
                               [0.0, 0.5, 0.0], [0.0, 0.0, 0.5]],
                    "nstep": 50,
                    "tolvrs": 1.0e-10,
                    "diemac": 12.0,
                    },
                scf_calculation_parameters=calculation_parameters,
                band_structure_input_variables={
                    "iscf": -2,
                    "nband": 8,
                    "tolwfr": 1e-12,
                    "diemac": 12.0,
                    "autoparal": 1,
                    },
                band_structure_calculation_parameters=calculation_parameters,
                band_structure_kpoint_path=[
                    {"L": [0.5, 0.0, 0.0]},
                    {r"$\Gamma$": [0.0, 0.0, 0.0]},
                    {"X": [0.0, 0.5, 0.5]},
                    {r"$\Gamma_2$": [1.0, 1.0, 1.0]}],
                band_structure_kpoint_path_density=20,
                plot_calculation_parameters={"show": False},
                )

    def _set_band_structure_convergence_done(self, workflow):
        # there are scf calculations and band structure calculations to copy
        seq = workflow.band_structure_convergence_sequencer
        scf_root = os.path.dirname(seq.scf_workdir)
        bs_root = os.path.dirname(seq.band_structure_workdir)
        SCF_ROOT_EXAMPLES = os.path.join(
                __TEST_WORKFLOW_ROOT__, "test_files", "abinit_si", "nc",
                "band_structure_ecut_convergence", "scf_runs")
        BS_ROOT_EXAMPLES = os.path.join(
                __TEST_WORKFLOW_ROOT__, "test_files", "abinit_si", "nc",
                "band_structure_ecut_convergence", "band_structure_runs")
        for scf_calc, bs_calc in zip(
                sorted(os.listdir(SCF_ROOT_EXAMPLES)),
                sorted(os.listdir(BS_ROOT_EXAMPLES))):
            target_scf = os.path.join(scf_root, scf_calc)
            copy_calculation(
                    os.path.join(SCF_ROOT_EXAMPLES, scf_calc),
                    target_scf,
                    new_pseudos=workflow.pseudos)
            copy_calculation(
                    os.path.join(BS_ROOT_EXAMPLES, bs_calc),
                    os.path.join(bs_root, bs_calc),
                    new_parents=[target_scf],
                    new_pseudos=workflow.pseudos)

    def _set_band_structure(self, *args):
        super()._set_band_structure(*args, fatbands=True)

    def _set_band_structure_done(self, workflow):
        super()._set_band_structure_done(
                workflow, new_pseudos=workflow.pseudos)

    def _set_dos(self, workflow):
        self._set_gs(workflow)
        self._set_gs_done(workflow)
        self._set_band_structure(workflow)
        self._set_band_structure_done(workflow)
        workflow.set_dos(
                root_workdir=os.path.join(
                    self.workflow_dir.name, "dos"),
                use_gs=True,
                dos_fine_kpoint_grid_variables={
                    "ngkpt": [10, 10, 10],
                    "nshiftk": 1,
                    "shiftk": [[0.0, 0.0, 0.0]],
                    "nband": 8,
                    },
                dos_input_variables={
                    "ecut": 10,
                    "iscf": -3,
                    "prtdos": 2,
                    "tolwfr": 1e-12,
                    "autoparal": 1,
                    },
                dos_calculation_parameters={
                    "command": self.scf_command_file.name,
                    "queuing_system": "local",
                    },
                plot_calculation_parameters={"show": False},
                )

    def _set_dos_done(self, workflow):
        copy_calculation(
                os.path.join(__TEST_WORKFLOW_ROOT__, "test_files", "abinit_si",
                             "nc", "dos", "dos_run"),
                workflow.dos_sequencer.dos_workdir,
                new_pseudos=workflow.pseudos,
                new_parents=[workflow.dos_sequencer.scf_workdir],
                )

    def _set_gs_done(self, workflow):
        copy_calculation(
                self._gs_example, workflow.gs_sequencer.scf_workdir,
                new_pseudos=workflow.pseudos)

    def _set_gs_ecut_convergence_done(self, workflow, varname):
        # 2 sets of calculations to copy: 'ecut' and 'pawecutdg'
        # start with ECUT example
        super()._set_gs_ecut_convergence_done(
                workflow, varname, new_pseudos=workflow.pseudos)
        # reset workflow
        # at this point there should be only one sequencer
        workflow.gs_ecut_convergence_sequencer.clear_sequence()
        self._set_gs_ecut_convergence(workflow)
        seq = workflow.gs_ecut_convergence_sequencer[1]
        varname = seq.ecuts_input_variable_name
        for calcdir in os.listdir(ABINIT_GS_PAWECUT_CONVERGENCE_EXAMPLE):
            copy_calculation(
                    os.path.join(
                        ABINIT_GS_PAWECUT_CONVERGENCE_EXAMPLE, calcdir),
                    os.path.join(
                        workflow.gs_ecut_convergence_sequencer[1].scf_workdir,
                        varname, calcdir),
                    new_pseudos=workflow.pseudos)

    def _set_gs_kgrid_convergence_done(self, workflow, *args, **kwargs):
        super()._set_gs_kgrid_convergence_done(
                workflow, *args, new_pseudos=workflow.pseudos)

    def _set_gs_smearing_convergence(self, workflow, *args, **kwargs):
        # for this test we use a different material and thus different pseudo
        workflow.pseudos = ABINIT_GS_SMEARING_CONVERGENCE_PSEUDOS
        super()._set_gs_smearing_convergence(workflow, *args, **kwargs)

    def _set_gs_smearing_convergence_done(self, *args, **kwargs):
        # for this test we use a different material and thus different pseudo
        super()._set_gs_smearing_convergence_done(
                *args, new_pseudos=ABINIT_GS_SMEARING_CONVERGENCE_PSEUDOS,
                **kwargs)

    def _set_optic_nscf_convergence(self, workflow):
        name = ABINIT_OPTIC_NSCF_CONVERGENCE_NSCF_KPOINT_GRIDS_INPUT_VAR_NAME
        workflow.set_optic_nscf_convergence(
                root_workdir=os.path.join(
                    self.workflow_dir.name, "optic_nscf_convergence"),
                experimental_bandgap=(
                    ABINIT_OPTIC_NSCF_CONVERGENCE_EXPERIMENTAL_BANDGAP),
                compute_non_linear_optical_response=False,
                nscf_input_variables=(
                    ABINIT_OPTIC_NSCF_CONVERGENCE_NSCF_INPUT_VARIABLES),
                nscf_calculation_parameters={
                    "queuing_system": "local",
                    "command": self.scf_command_file.name,
                    },
                nscffbz_calculation_parameters={
                    "queuing_system": "local",
                    "command": self.scf_command_file.name,
                    },
                nscf_kpoint_grids=(
                    ABINIT_OPTIC_NSCF_CONVERGENCE_NSCF_KPOINT_GRIDS),
                nscf_kpoint_grids_input_variable_name=name,
                ddk_input_variables=(
                    ABINIT_OPTIC_NSCF_CONVERGENCE_DDK_INPUT_VARIABLES),
                ddk_calculation_parameters={
                    "queuing_system": "local",
                    "command": self.scf_command_file.name,
                    },
                optic_input_variables=(
                    ABINIT_OPTIC_NSCF_CONVERGENCE_OPTIC_INPUT_VARIABLES),
                optic_calculation_parameters={
                    "queuing_system": "local",
                    "command": self.optic_command_file.name,
                    },
                use_gs=True,
                plot_calculation_parameters={"show": False},
                )

    def _set_optic_nscf_convergence_done(
            self, workflow, nscf=False, nscffbz=False, ddk=False, optic=False):
        def copy(sequencer, prefix, new_parents):
            target = getattr(sequencer, f"{prefix}_workdir")
            src = os.path.join(
                    ABINIT_OPTIC_NSCF_CONVERGENCE_EXAMPLE,
                    os.path.basename(os.path.dirname(target)),
                    os.path.basename(target))
            copy_calculation(
                    src, target,
                    new_parents=new_parents,
                    new_pseudos=ABINIT_OPTIC_NSCF_CONVERGENCE_PSEUDOS,
                    )

        for seq in workflow.optic_nscf_convergence_sequencer:
            if nscf:
                copy(seq, "nscf",
                     new_parents=[workflow.gs_sequencer.scf_workdir]
                     )
            if nscffbz:
                copy(seq, "nscffbz",
                     new_parents=[
                         seq.nscf_workdir, workflow.gs_sequencer.scf_workdir])
            if ddk:
                copy(seq, "ddk", new_parents=[seq.nscffbz_workdir])
            if optic:
                copy(seq, "optic",
                     new_parents=[
                         seq.ddk_workdir, seq.nscffbz_workdir])

    def _set_phonon_ecut_convergence(self, workflow, *args, **kwargs):
        # use the Al example
        workflow.pseudos = ABINIT_PHONON_ECUT_CONVERGENCE_PSEUDOS
        super()._set_phonon_ecut_convergence(workflow, *args, **kwargs)

    def _set_phonon_ecut_convergence_done(self, workflow, *args, **kwargs):
        super()._set_phonon_ecut_convergence_done(
                workflow, *args,
                _copy_scf_extra_kwargs={
                    "new_pseudos": ABINIT_PHONON_ECUT_CONVERGENCE_PSEUDOS},
                _copy_phonons_extra_kwargs={
                    "new_pseudos": ABINIT_PHONON_ECUT_CONVERGENCE_PSEUDOS},
                **kwargs)

    def _set_phonon_dispersion(self, *args, **kwargs):
        super()._set_phonon_dispersion(
                *args,
                compute_electric_field_response=True,
                ddk_input_variables={
                    "rfdir": [1, 1, 1],
                    "tolwfr": 1e-16,
                    "nstep": 25,
                    "diemac": 12.0,
                    },
                ddk_calculation_parameters={
                    "command": self.phonons_command_file.name,
                    "queuing_system": "local"},
                mrgddb_calculation_parameters={
                    "command": self.mrgddb_command_file.name,
                    "queuing_system": "local",
                    },
                anaddb_input_variables={
                    "ifcflag": 1, "ifcout": 0, "brav": 2,
                    "q1shft": [0.0] * 3, "nqshft": 1, "chneut": 1, "dipdip": 1,
                    "eivec": 4, "nph2l": 1, "qph2l": [1.0, 0.0, 0.0, 0.0],
                    },
                anaddb_calculation_parameters={
                    "command": self.anaddb_command_file.name,
                    "queuing_system": "local",
                    },
                phonons_qpoints_split_by_irreps=[7],
                **kwargs)

    def _set_phonon_dispersion_done(
            self, workflow,
            all_phonons=False, first_phonon=False, mrgddb=False,
            ddk=False,
            anaddb=False, **kwargs):
        super()._set_phonon_dispersion_done(
                workflow,
                new_pseudos=ABINIT_PHONON_DISPERSION_PSEUDOS,
                **kwargs)
        seq = workflow.phonon_dispersion_sequencer
        root = self._phonon_dispersion_example
        if first_phonon:
            copy_calculation(
                    os.path.join(
                        root, "phonons_runs", "ph_q_qgrid_generation"),
                    os.path.join(
                        os.path.dirname(seq.phonons_workdir),
                        "ph_q_qgrid_generation"),
                    new_pseudos=ABINIT_PHONON_DISPERSION_PSEUDOS)
            # also copy ddk
            self._set_phonon_dispersion_done(workflow, ddk=True)
        if ddk:
            copy_calculation(
                    os.path.join(root, "ddk_runs"),
                    seq.ddk_workdir,
                    new_pseudos=ABINIT_PHONON_DISPERSION_PSEUDOS,
                    new_parents=[seq.scf_workdir])
        if all_phonons:
            # also copy again ddk
            self._set_phonon_dispersion_done(workflow, ddk=True)
            for calcdir in os.listdir(os.path.join(root, "phonons_runs")):
                new_parents = [seq.scf_workdir]
                if calcdir == "ph_q1":
                    new_parents.append(seq.ddk_workdir)
                if any(["atom_pol" in x for x in os.listdir(
                        os.path.join(root, "phonons_runs", calcdir))]):
                    # copy subdirs
                    for subcalcdir in os.listdir(
                            os.path.join(root, "phonons_runs", calcdir)):
                        copy_calculation(
                                os.path.join(
                                    root, "phonons_runs", calcdir, subcalcdir),
                                os.path.join(
                                    os.path.dirname(seq.phonons_workdir),
                                    calcdir, subcalcdir),
                                new_parents=new_parents,
                                new_pseudos=ABINIT_PHONON_DISPERSION_PSEUDOS,
                                **kwargs)
                else:
                    copy_calculation(
                        os.path.join(root, "phonons_runs", calcdir),
                        os.path.join(
                            os.path.dirname(seq.phonons_workdir), calcdir),
                        new_parents=new_parents,
                        new_pseudos=ABINIT_PHONON_DISPERSION_PSEUDOS,
                        **kwargs)
        if mrgddb:
            phroot = os.path.dirname(seq.phonons_workdir)
            new_parents = []
            for calcdir in os.listdir(phroot):
                if any(["atom_pol" in x
                        for x in os.listdir(os.path.join(phroot, calcdir))]):
                    # add all subcalcdir as parents
                    for subcalcdir in os.listdir(
                            os.path.join(phroot, calcdir)):
                        if "irreps" in subcalcdir:
                            continue
                        new_parents.append(
                                os.path.join(phroot, calcdir, subcalcdir))
                elif "qgrid_gen" in calcdir:
                    continue
                else:
                    new_parents.append(os.path.join(phroot, calcdir))
            copy_calculation(
                    os.path.join(root, "mrgddb_run"),
                    seq.mrgddb_workdir,
                    new_parents=new_parents,
                    )
        if anaddb:
            copy_calculation(
                    os.path.join(root, "anaddb_run"),
                    seq.anaddb_workdir,
                    new_parents=[seq.mrgddb_workdir])

    def _set_phonon_dispersion_qgrid_convergence(self, *args, **kwargs):
        super()._set_phonon_dispersion_qgrid_convergence(
                *args,
                compute_electric_field_response=True,
                ddk_input_variables={
                    "rfdir": [1, 1, 1],
                    "tolwfr": 1e-16,
                    "nstep": 25,
                    "diemac": 12.0,
                    },
                ddk_calculation_parameters={
                    "command": self.phonons_command_file.name,
                    "queuing_system": "local"},
                mrgddb_calculation_parameters={
                    "command": self.mrgddb_command_file.name,
                    "queuing_system": "local",
                    },
                anaddb_input_variables={
                    "ifcflag": 1, "ifcout": 0, "brav": 2,
                    "q1shft": [0.0] * 3, "nqshft": 1, "chneut": 1, "dipdip": 1,
                    "eivec": 4, "nph2l": 1, "qph2l": [1.0, 0.0, 0.0, 0.0],
                    },
                anaddb_calculation_parameters={
                    "command": self.anaddb_command_file.name,
                    "queuing_system": "local",
                    },
                **kwargs)

    def _set_phonon_dispersion_qgrid_convergence_done(
            self, workflow,
            all_phonons=False, first_phonon=False, mrgddb=False,
            ddk=False,
            anaddb=False, **kwargs):
        super()._set_phonon_dispersion_qgrid_convergence_done(
                workflow,
                new_pseudos=ABINIT_PHONON_DISPERSION_PSEUDOS,
                **kwargs)
        if all_phonons:
            # also copy again ddk
            self._set_phonon_dispersion_qgrid_convergence_done(
                    workflow, ddk=True)
        for seq in workflow.phonon_dispersion_qgrid_convergence_sequencer:
            root = os.path.join(
                    self._phonon_dispersion_qgrid_convergence_example,
                    os.path.basename(
                        os.path.dirname(os.path.dirname(seq.phonons_workdir))))
            if first_phonon:
                copy_calculation(
                        os.path.join(
                            root, "phonons_runs", "ph_q_qgrid_generation"),
                        os.path.join(
                            os.path.dirname(seq.phonons_workdir),
                            "ph_q_qgrid_generation"),
                        new_pseudos=ABINIT_PHONON_DISPERSION_PSEUDOS)
            if ddk:
                copy_calculation(
                        os.path.join(root, "ddk_runs"),
                        seq.ddk_workdir,
                        new_pseudos=ABINIT_PHONON_DISPERSION_PSEUDOS,
                        new_parents=[seq.scf_workdir])
            if all_phonons:
                for calcdir in os.listdir(os.path.join(root, "phonons_runs")):
                    new_parents = [seq.scf_workdir]
                    if calcdir == "ph_q1":
                        new_parents.append(seq.ddk_workdir)
                    copy_calculation(
                        os.path.join(root, "phonons_runs", calcdir),
                        os.path.join(
                            os.path.dirname(seq.phonons_workdir), calcdir),
                        new_parents=new_parents,
                        new_pseudos=ABINIT_PHONON_DISPERSION_PSEUDOS,
                        **kwargs)
            if mrgddb:
                phroot = os.path.dirname(seq.phonons_workdir)
                copy_calculation(
                        os.path.join(root, "mrgddb_run"),
                        seq.mrgddb_workdir,
                        new_parents=[
                            os.path.join(phroot, calc)
                            for calc in os.listdir(phroot)
                            if "qgrid_gen" not in calc],
                        )
            if anaddb:
                copy_calculation(
                        os.path.join(root, "anaddb_run"),
                        seq.anaddb_workdir,
                        new_parents=[seq.mrgddb_workdir])
        if first_phonon:
            # also copy ddk
            self._set_phonon_dispersion_qgrid_convergence_done(
                    workflow, ddk=True)

    def _set_phonon_smearing_convergence(self, workflow, *args, **kwargs):
        # use the Al example
        workflow.pseudos = ABINIT_PHONON_ECUT_CONVERGENCE_PSEUDOS
        super()._set_phonon_smearing_convergence(workflow, *args, **kwargs)

    def _set_phonon_smearing_convergence_done(self, workflow, *args, **kwargs):
        super()._set_phonon_smearing_convergence_done(
                workflow, *args,
                copy_scf_extra_kwargs={
                    "new_pseudos": ABINIT_PHONON_ECUT_CONVERGENCE_PSEUDOS},
                copy_phonons_extra_kwargs={
                    "new_pseudos": ABINIT_PHONON_ECUT_CONVERGENCE_PSEUDOS},
                **kwargs)

    def _set_relaxation_done(self, workflow):
        dirlist = os.listdir(ABINIT_RELAXATION_EXAMPLE)
        for icalc, calcdir in enumerate(dirlist):
            new_parents = []
            if calcdir == os.path.basename(
                    workflow.relaxation_sequencer._get_relax_cell_workdir()):
                # need to link the other calc
                basename = dirlist[0] if icalc == 1 else dirlist[1]
                new_parents = [
                        os.path.join(
                            workflow.relaxation_sequencer.scf_workdir,
                            basename)]
            copy_calculation(
                    os.path.join(ABINIT_RELAXATION_EXAMPLE, calcdir),
                    os.path.join(
                        workflow.relaxation_sequencer.scf_workdir, calcdir),
                    new_pseudos=workflow.pseudos,
                    new_parents=new_parents)
