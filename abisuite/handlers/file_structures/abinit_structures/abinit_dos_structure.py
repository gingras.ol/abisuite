from ..bases import BaseStructure


class AbinitDOSStructure(BaseStructure):
    """Structure class for a _DOS file produced by the abinit software.
    """
    all_attributes = (
            "dos", "energies", "fermi_energy", "integrated_dos", )
