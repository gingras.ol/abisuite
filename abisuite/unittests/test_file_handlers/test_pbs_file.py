import os
import random
import string

from .bases import BasePathConvertibleFileHandlerTest
from ..bases import TestCase
from ..routines_for_tests import create_pbs_test_writer
from ...handlers import PBSFile
from ...handlers.file_approvers.exceptions import MPIError
from ...linux_tools import touch


class TestPBSFile(BasePathConvertibleFileHandlerTest, TestCase):
    _file_handler_class = PBSFile

    # TODO: rebase this test with the meta data writer test
    def setUp(self):
        super().setUp(write=False)
        create_pbs_test_writer(self.handler)
        # create artificial script file
        self._command_file = os.path.join(
            self.tempdir.name, "abinit")
        touch(self._command_file)
        self.handler.command = self._command_file
        self.handler.lines_before = ["cd $PBS_O_WORKDIR"]
        self.handler.write()

    def test_validating_errors(self):
        self.handler.nodes = 1
        self.handler.ppn = 1
        self.handler.queuing_system = "torque"
        self.handler.mpi_command = "mpirun -np 2"
        with self.assertRaises(MPIError):
            self.handler.validate()

    def test_commands_and_arguments(self):
        # test the command and arguments properties and setters
        base_cmd = "/cmd"
        base_mpi_cmd = "/mpi_cmd"
        cmd_args = {"-arg1": 1, "--arg2": 2, "-arg3": "", "-arg4=": 9}
        mpi_cmd_args = cmd_args.copy()
        for cmd, args, cmd_attr, arg_attr in zip([base_cmd, base_mpi_cmd],
                                                 [cmd_args, mpi_cmd_args],
                                                 ["command", "mpi_command"],
                                                 ["command_arguments",
                                                  "mpi_command_arguments"]):
            tojoin = [cmd]
            for arg, value in args.items():
                value = str(value)
                if "=" in arg:
                    tojoin.append(arg + str(value))
                    continue
                if not len(value):
                    tojoin.append(arg)
                    continue
                else:
                    tojoin.append(arg + " " + str(value))
                    continue
            full_cmd = " ".join(tojoin)
            # test command
            setattr(self.handler, cmd_attr, full_cmd)
            self.assertEqual(getattr(self.handler, cmd_attr), full_cmd)
            self.assertDictEqual(getattr(self.handler, arg_attr), args)
            # test string handling for args
            # pop an element and check if command is respected
            args.pop(list(args.keys())[0])
            setattr(self.handler, arg_attr, args)
            self.assertEqual(getattr(self.handler, arg_attr), args)
            argsstr = []
            for arg, value in args.items():
                if "=" in arg:
                    argsstr.append(arg + str(value))
                    continue
                if not len(str(value)):
                    argsstr.append(arg)
                    continue
                else:
                    argsstr.append(arg + " " + str(value))
                    continue
            argsstr = " ".join(argsstr)
            setattr(self.handler, arg_attr, argsstr)
            self.assertDictEqual(getattr(self.handler, arg_attr), args)
            self.assertEqual(getattr(self.handler, cmd_attr),
                             cmd + " " + argsstr)

    def _change_random_property(self, handler):
        # change the command as it is a property shared by any pbs file
        # generate random jobname. Taken from:
        # https://stackoverflow.com/a/2257449/6362595
        allchars = string.ascii_letters + string.digits
        handler.command = ''.join(random.sample(allchars, 10))
