from .bases import BasePackagerTest
from ..bases import TestCase
from ..variables_for_tests import (
        AbinitCalc, abinit_AlAs_pseudos, AbinitOpticCalc,
        )
from ...packagers import AbinitPackager, AbinitOpticPackager


class TestAbinitPackager(BasePackagerTest, TestCase):
    """Test case for the AbinitPackager.
    """
    _example = AbinitCalc
    _packager_cls = AbinitPackager

    def setUp(self):
        super().setUp()
        self.calc.new_pseudos = abinit_AlAs_pseudos


class TestAbinitOpticPackager(BasePackagerTest, TestCase):
    """Test case for the AbinitOpticPackager.
    """
    _example = AbinitOpticCalc
    _packager_cls = AbinitOpticPackager
