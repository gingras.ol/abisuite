from .bases import BaseQELogParser
from ...file_structures import QEPPLogStructure


class QEPPLogParser(BaseQELogParser):
    """Parser class for a Quantum Espresso pp.x log file.
    """
    _loggername = "QEPPLogParser"
    _structure_class = QEPPLogStructure
