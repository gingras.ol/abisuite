import tempfile
import unittest

from .bases import BaseApproverTest
from ...handlers.file_approvers import SymLinkApprover
from ...handlers.file_approvers.exceptions import SymLinkError


class TestSymLinkApprover(BaseApproverTest, unittest.TestCase):
    _approver_class = SymLinkApprover
    _exception = SymLinkError

    def setUp(self):
        super().setUp()
        # create file
        self.source = tempfile.NamedTemporaryFile()
        self.approver.source = self.source.name

    def test_non_existant_source(self):
        self.approver.source = "non_existant_source"
        self.approver.validate()
        self.assertFalse(self.approver.is_valid)
        self.assertEqual(len(self.approver.errors), 1)
        with self.assertRaises(SymLinkError):
            self.approver.raise_errors()
