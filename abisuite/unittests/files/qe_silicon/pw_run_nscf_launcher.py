from abisuite import QEPWLauncher as Launcher


# very simple example
qe_input_vars = {
        "calculation": "nscf",
        "nbnd": 12,
        "ecutwfc": 50.0,
        "k_points": {"parameter": "automatic",
                     "k_points": [8, 8, 8, 1, 1, 1]}
        }

launcher = Launcher("nscf")
launcher.workdir = "pw_run_nscf"
launcher.mpi_command = "mpirun -np 4"
launcher.input_variables = qe_input_vars
launcher.load_geometry_from("pw_run")
launcher.link_calculation("pw_run")
launcher.write()
launcher.run()
