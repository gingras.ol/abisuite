from .bases import BaseFileHandler
from ..file_parsers import SlurmFilesToDeleteParser
from ..file_structures import SlurmFilesToDeleteStructure


class SlurmFilesToDeleteFile(BaseFileHandler):
    """FIle handler class for the slurm file that lists the files to be deleted
    during the next purge.
    """
    _loggername = "SlurmFilesToDeleteFile"
    _parser_class = SlurmFilesToDeleteParser
    _structure_class = SlurmFilesToDeleteStructure

    @classmethod
    def from_calculation(cls, *args, **kwargs):
        raise RuntimeError(
                "This file is not related to any calculation.")

    @classmethod
    def from_meta_data_file(cls, *args, **kwargs):
        raise RuntimeError(
                "This file is not related to any calculation.")
