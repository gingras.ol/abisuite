#!/bin/bash

MPIRUN="mpirun -np 4"
EXECUTABLE="/home/fgoudreault/Workspace/q-e/bin/pw.x -npool 4"
INPUT=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_sequencers/files/qe_pb_smearing_convergence/scf_runs/degauss0.1/k_points8_8_8/k_points8_8_8.in
LOG=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_sequencers/files/qe_pb_smearing_convergence/scf_runs/degauss0.1/k_points8_8_8/k_points8_8_8.log
STDERR=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_sequencers/files/qe_pb_smearing_convergence/scf_runs/degauss0.1/k_points8_8_8/k_points8_8_8.stderr


$MPIRUN $EXECUTABLE -input $INPUT > $LOG 2> $STDERR
