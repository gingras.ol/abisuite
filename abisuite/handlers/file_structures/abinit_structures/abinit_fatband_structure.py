from ..bases import BaseStructure


class AbinitFatbandStructure(BaseStructure):
    """Structure for a _FATBANDS_* file created by abinit.
    """
    all_attributes = ("characters", "eigenvalues", "nbands", "nkpts", )
