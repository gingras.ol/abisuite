import json
import os
import random
import string
import tempfile

from .bases import BasePathConvertibleFileHandlerTest
from ..bases import TestCase
from ...handlers import MetaDataFile
from ...handlers.file_structures import MetaDataStructure
from ...launchers import __ALL_LAUNCHERS__
from ...linux_tools import mkdir


class TestMetaDataFile(BasePathConvertibleFileHandlerTest, TestCase):
    """Test case for the meta data file handler.
    """
    _file_handler_class = MetaDataFile

    def setUp(self):
        super().setUp(write=False)
        for prop in MetaDataStructure.all_attributes:
            if prop in MetaDataStructure.set_attributes:
                continue
            if prop in MetaDataStructure.list_attributes:
                continue
            setattr(self.handler, prop, prop)
        self.handler.write()

    def test_path_reconstruction(self):
        # check path reconstruction by changing the calc_workdir
        self.handler.calc_workdir = os.path.join(self.tempdir.name,
                                                 "new_workdir")
        for attr in self.handler.structure.convertible_path_attributes:
            handler_attr = getattr(self.handler, attr)
            self.assertEqual(os.path.commonpath((self.handler.calc_workdir,
                                                 handler_attr)),
                             self.handler.calc_workdir,
                             msg=f"{attr}: {handler_attr}")

    def test_last_modified_property(self):
        # check that last_modified property changes when rewritting file
        with self._file_handler_class.from_file(self.handler.path) as meta:
            meta.read()
            last_modified = meta.last_modified
            self._change_random_property(meta)
        with self._file_handler_class.from_file(self.handler.path) as meta:
            meta.read()
            self.assertNotEqual(meta.last_modified, last_modified)

    def test_init_from_launcher_or_calculation(self):
        for calctype, launcher_class in __ALL_LAUNCHERS__.items():
            # cut3d does not require meta data file
            if calctype == "abinit_cut3d":
                continue
            workdir = os.path.join(
                self.tempdir.name,
                str(launcher_class).replace(
                    " ", "_").replace(
                    "'", "").replace("<", "").replace(">", ""))
            jobname = "test"
            launcher = launcher_class(jobname)
            # create fake script
            suffix = launcher.calctype
            if "abinit" not in suffix:
                suffix += ".x"
            script = tempfile.NamedTemporaryFile(suffix=suffix)
            launcher.workdir = workdir
            launcher.command = script.name
            meta = self._file_handler_class.from_launcher(launcher)
            self.assertEqual(meta.calc_workdir, launcher.workdir)
            self.assertEqual(meta.path,
                             os.path.join(launcher.workdir,
                                          "." + launcher.jobname +
                                          meta._parser_class._expected_ending))
            del meta
            # check that trying to get meta file from non existent calculation
            # raises an error
            with self.assertRaises(NotADirectoryError):
                self._file_handler_class.from_calculation(launcher.workdir)
            # just create the top directory
            mkdir(launcher.workdir)
            with self.assertRaises(FileNotFoundError):
                self._file_handler_class.from_calculation(launcher.workdir)
            launcher.meta_data_file.submit_time = "now"
            launcher.meta_data_file.input_file_path = launcher.input_file.path
            launcher.meta_data_file.pbs_file_path = launcher.pbs_file.path
            sifp = launcher.pbs_file.input_file_path
            launcher.meta_data_file.script_input_file_path = sifp
            launcher.meta_data_file.write()
            wd = launcher.workdir
            with self._file_handler_class.from_calculation(wd) as meta:
                self.assertEqual(meta.calctype, launcher.calctype)

    def test_data_integrity_checker(self):
        # test not a dict error in meta file
        with open(self.handler.path, "w") as f:
            json.dump(1, f)
        with self.assertRaises(TypeError):
            self.handler.read()
        # test that an unknown variable raises an error
        with open(self.handler.path, "w") as f:
            json.dump({"unexpected key": "wtf"}, f)
        with self.assertRaises(ValueError):
            self.handler.read()

    def _change_random_property(self, handler):
        # change the jobname only
        # generate random jobname. Taken from:
        # https://stackoverflow.com/a/2257449/6362595
        allchars = string.ascii_letters + string.digits
        handler.jobname = ''.join(random.sample(allchars, 10))
