from ..bases import BaseInputWriter
from ...file_structures import AbinitOpticInputStructure


class AbinitOpticInputWriter(BaseInputWriter):
    """Class that can write an optic input file.
    """
    _loggername = "AbinitOpticInputWriter"
    _structure_class = AbinitOpticInputStructure

    @property
    def lines(self):
        # start with files
        lines = []
        self._append_var_block(lines, "files")
        # parameters
        self._append_var_block(lines, "parameters")
        # computations
        self._append_var_block(lines, "computations")
        return lines

    def _append_var_block(self, lines, section, end_newline=True):
        lines.append(f"&{section.upper()}\n")
        for variable in self.input_variables.values():
            if variable.block == section:
                lines.append(str(variable))
        lines.append("/\n")
