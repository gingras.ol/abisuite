from ..bases import BaseInputStructure
from ....variables.qe_variables import ALL_QEDOS_VARIABLES


class QEDOSInputStructure(BaseInputStructure):
    """Structure class for a Quantum Espresso dos.x input file.
    """
    _variables_db = ALL_QEDOS_VARIABLES
