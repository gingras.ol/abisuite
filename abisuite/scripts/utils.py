from ..bases import BaseUtility
from ..handlers import CalculationDirectory
from ..routines import is_list_like


class CalculationsList(BaseUtility, list):
    """A simple calculation list utility for calculculations.
    """
    _loggername = "CalculationsList"

    def __init__(self, list_of_obj, *args, **kwargs):
        """CalculationsList init method.

        Parameters
        ----------
        list_of_obj: list-like
            The initial list of calculations.
        """
        BaseUtility.__init__(self, *args, **kwargs)
        if not is_list_like(list_of_obj):
            raise TypeError(list_of_obj)
        list.__init__(self, list(self._check(list_of_obj)))

    def __add__(self, obj):
        if not is_list_like(obj):
            return TypeError(f"Cannot add non-list objects like '{obj}'.")
        for item in obj:
            self.append(item)

    # def __iter__(self):
    #     for item in self._container:
    #         yield item

    def __repr__(self):
        str_ = "< CalculationsList :\n"
        for calc in self:
            str_ += f"    - '{calc}'\n"
        return str_ + ">"

    def append(self, calc):
        """Appends a calculation to the list.

        Parameters
        ----------
        calc: str or CalculationDirectory instance
            The calculation to append.
        """
        super().append(self._check(calc))

    def _check(self, calc, recursive=True):
        """Checks that the given (list of) item(s) is either a str
        or a CalculationDirectory instance.

        Parameters
        ----------
        calc: obj
            The item to check out.
        recursive: bool, optional
            If True, if a list of item is given, checks that all items
            it in are good.

        Returns
        -------
        What was given in argument if everything is good.
        """
        if is_list_like(calc) and recursive:
            for item in calc:
                self._check(item, recursive=False)
            return calc
        if not isinstance(calc, str) and not isinstance(
                calc, CalculationDirectory):
            raise TypeError(calc)
        return calc
