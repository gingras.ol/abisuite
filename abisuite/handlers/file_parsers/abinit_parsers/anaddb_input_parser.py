from .bases import BaseAbinitInputParser
from ...file_structures import AbinitAnaddbInputStructure


class AbinitAnaddbInputParser(BaseAbinitInputParser):
    """Parser for an abinit anaddb input file.
    """
    _loggername = "AbinitAnaddbInputParser"
    _structure_class = AbinitAnaddbInputStructure
