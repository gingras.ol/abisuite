from .pp_launcher import QEPPLauncher
from ..bases import BaseMassLauncher


class QEPPMassLauncher(BaseMassLauncher):
    """The mass launcher class for Quantum Espresso calculations
    using the pp.x script.
    """
    _launcher_class = QEPPLauncher
    _loggername = "QEPPMassLauncher"
