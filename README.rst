.. image:: logo/large_logo.png
   :alt: Welcome to abisuite!
   :target: https://gitlab.com/groupe-cote-udem/abisuite
.. image:: https://img.shields.io/badge/licence-MIT-blue.svg
   :alt: Licence
   :target: https://gitlab.com/groupe-cote-udem/abisuite/blob/master/LICENSE
.. image:: https://img.shields.io/gitlab/pipeline/groupe-cote-udem/abisuite/master.svg
   :alt: Build status
   :target: https://gitlab.com/groupe-cote-udem/abisuite/pipelines/
.. image:: https://gitlab.com/groupe-cote-udem/abisuite/badges/master/coverage.svg
   :alt: Coverage percentage.
   :target: https://gitlab.com/groupe-cote-udem/abisuite/pipelines/
.. image:: https://img.shields.io/badge/docs-click%20here-lightgrey
   :alt: Click here for docs.
   :target: https://groupe-cote-udem.gitlab.io/abisuite

====================
Welcome to AbiSuite!
====================

This is a python module to easily write input files, launch calculations,
parse output files, post-process data and plot data from quantum abinitio softwares.

Installation
------------

Download repository using git::

 git clone git@github.com:abiproject/abisuite.git
 cd abisuite

Execute setup script and install requirements::
  
 pip install -r requirements.txt
 pip install .

For a development installation (to modify the code without having to execute
the setup script each time). Use the ``-e`` flag for the ``pip install``
command.

To read netCDF4 files, one needs to install netCDF4 and the corresponding
python interface. Good luck with that! In any case, the import will be
tried only where needed thus the major part of the code won't be affected
if netCDF4 is not installed.

Tests
-----

Before running the tests, you need to install the requirements::

  pip install -r test-requirements.txt
  
Then, just use pytest in main repository::

 pytest
 

Documentation
-------------

There is documentation available for abisuite
`here <https://groupe-cote-udem.gitlab.io/abisuite>`__.

The documentation is hosted thanks to GitLab pages!
If you want to modify the documentation, you can rebuild it by using sphinx.
First, you need to install the required packages::

  pip install -r docs-requirements.txt
  
Then, cd into the doc directory and build the docs::

  cd doc
  make clean html
  
When you push modifications to the gitlab repo, the building of the docs will
be tested and (if it succeeds), it will be pushed on the gitlab

Examples
--------

*TODO: Remake the examples. For now these are deprecated (some of them). See the unittests
test files for working examples.*

There are some examples in the "/examples" directory.

Configuration File
------------------

abisuite requires a config file. A default one is created upon installation at:
`~/.config/abisuite`. It contains default values that probably needs to be
edited. If any entry in the config file is deleted, the next time the package
needs the config file, the default parameter will be put back. Also, if there
is an update of the software where new config file entries have been inserted,
their default values will be automatically set.

Database
--------

To initialize a new database at the path ``$path_db``, use::

  abisuite db --init-db $path_db

You can look at the database by using::

  abisuite db --show-db

Contributors
------------

- `Felix Goudreault <https://gitlab.com/fgoudreault>`__
- Daniel Gendron
- Olivier Gingras
