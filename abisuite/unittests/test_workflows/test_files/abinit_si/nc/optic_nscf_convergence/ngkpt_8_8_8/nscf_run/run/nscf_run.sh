#!/bin/bash

MPIRUN="mpirun -np 4"
EXECUTABLE="/home/fgoudreault/Workspace/abinit/abinit/build/develop/2021-04-20/src/98_main/abinit"
INPUT=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_workflows/test_files/abinit_si/nc/optic_nscf_convergence/ngkpt_8_8_8/nscf_run/run/nscf_run.files
LOG=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_workflows/test_files/abinit_si/nc/optic_nscf_convergence/ngkpt_8_8_8/nscf_run/nscf_run.log
STDERR=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_workflows/test_files/abinit_si/nc/optic_nscf_convergence/ngkpt_8_8_8/nscf_run/nscf_run.stderr


$MPIRUN $EXECUTABLE < $INPUT > $LOG 2> $STDERR
