import numpy as np
import os

from .individual_phonon_sequencers import BasePhononConvergenceSequencer
from .kgrid_convergence_sequencers import BaseKgridConvergenceSequencer
from ..exceptions import DevError
from ..plotters import Plot, MultiPlot, rand_cmap
from ..routines import is_list_like


class SmearingConvergenceSequencer(BaseKgridConvergenceSequencer):
    """Convergence class for the k-poitns + smearing parameters.

    This is basically a KgridConvergenceSequencer but we do multiple runs
    for each smearing parameter.
    """
    smearings_input_variable_name = None
    smearings_input_variables_units = None

    def __init__(self, *args, **kwargs):
        BaseKgridConvergenceSequencer.__init__(self, *args, **kwargs)
        self._scf_smearings = None
        if self.smearings_input_variable_name is None:
            raise DevError("Need to set 'smearings_input_variable_name'.")
        if self.smearings_input_variable_units is None:
            raise DevError("Need to set 'smearings_input_variable_units'.")

    @property
    def converged_smearing_variables(self):
        return self.scf_converged_parameter

    @property
    def scf_input_variables(self):
        return BaseKgridConvergenceSequencer.scf_input_variables.fget(self)

    @scf_input_variables.setter
    def scf_input_variables(self, ins):
        BaseKgridConvergenceSequencer.scf_input_variables.fset(self, ins)

    @property
    def scf_smearings(self):
        if self._scf_smearings is not None:
            return self._scf_smearings
        raise ValueError("Need to set 'scf_smearings'.")

    @scf_smearings.setter
    def scf_smearings(self, smearings):
        if not is_list_like(smearings):
            raise TypeError(
                    "'scf_smearings' need to be list-like.")
        self._scf_smearings = smearings

    @property
    def scf_specific_input_variables(self):
        if self._scf_specific_input_variables is not None:
            return self._scf_specific_input_variables
        # build matrix of calculations
        specs = []
        for smearing in self.scf_smearings:
            for k_points in self.scf_kgrids:
                specs.append(
                        {self.smearings_input_variable_name: smearing,
                         self.kgrids_input_variable_name: k_points})
        self._scf_specific_input_variables = specs
        return self.scf_specific_input_variables

    @scf_specific_input_variables.setter
    def scf_specific_input_variables(self, spec):
        raise RuntimeError("Cannot set 'scf_specific_input_variables'.")

    def compute_convergence(self):
        # gather all deltas for each smearings
        # to determine converged parameter: we want the coarsest grid
        # for which we achieve convergence. If more than one smearing parameter
        # achieves convergence for same kpt grid, take the smallest one
        # (the smallest is more physical than the others).
        lowest_smearing = None
        lowest_kgrid_index = None
        for smearing in self.scf_smearings:
            kgrids, deltas, etots = (
                    BaseKgridConvergenceSequencer.compute_convergence(
                        self,
                        rootdir=self._get_scf_smearing_root_dir(smearing)))
            for idelta, delta in enumerate(deltas):
                if delta <= self.scf_convergence_criterion:
                    # convergence achieved
                    if lowest_smearing is None:
                        lowest_smearing = smearing
                        lowest_kgrid_index = idelta
                    else:
                        # check if kgrid index is lower.
                        # if yes, this kgrid is coarser.
                        # if equal, take smallest smearing
                        # if greater, discard
                        if idelta > lowest_kgrid_index:
                            continue
                        if idelta == lowest_kgrid_index:
                            lowest_smearing = min((lowest_smearing, smearing))
                            continue
                        if idelta < lowest_kgrid_index:
                            lowest_smearing = lowest_smearing
                            lowest_kgrid_index = idelta
        # if lowest smearing is None, no convergence was acheived
        if lowest_smearing is None:
            self._logger.warning("Smearing convergence was not achieved for "
                                 "any smearing / kgrid.")
            return
        self._scf_converged_parameter = {
                self.smearings_input_variable_name: lowest_smearing,
                self.kgrids_input_variable_name: (
                    self.scf_kgrids[lowest_kgrid_index]),
                }

    def plot_convergence(self):
        # override this method to make 2 plots:
        # one for etots and one for deltas
        # start by creating the 2 plots
        etot_plot = Plot(loglevel=self._loglevel)
        delta_plot = Plot(loglevel=self._loglevel)
        xdata = self._add_xdata_to_plot(etot_plot)
        self._add_xdata_to_plot(delta_plot)
        colors = rand_cmap(len(self.scf_specific_input_variables))
        for ismear, smearing in enumerate(self.scf_smearings):
            _, deltas, etots = (
                    BaseKgridConvergenceSequencer.compute_convergence(
                        self,
                        rootdir=self._get_scf_smearing_root_dir(smearing)))
            color = colors(ismear)
            etot_plot.add_curve(
                    xdata, etots,
                    label=(f"{self.smearings_input_variable_name}={smearing}"
                           f"{self.smearings_input_variable_units}"),
                    linewidth=2, color=color, marker="s", markersize=5,
                    markerfacecolor=color)
            delta_plot.add_curve(
                    xdata[:-1], deltas, color=color,
                    linewidth=2, marker="o", markersize=5,
                    semilogy=True,
                    label=(f"{self.smearings_input_variable_name}={smearing}"
                           f"{self.smearings_input_variable_units}"),
                    markerfacecolor=color)
        etot_plot.title = r"$E_{tot}$"
        delta_plot.title = r"$\Delta E_{tot}$"
        # criterion threshold
        delta_plot.add_hline(
                self.scf_convergence_criterion, color="k",
                linestyle="--",
                label=(
                    r"$\Delta E_{tot}=$" +
                    f"{self.scf_convergence_criterion}meV/at"),
                linewidth=2)
        delta_plot.grid = self.plot_calculation_parameters.get("grid", True)
        etot_plot.grid = self.plot_calculation_parameters.get("grid", True)
        etot_plot.xlabel = f"{self._parameter_to_converge_input_variable_name}"
        delta_plot.xlabel = (
                f"{self._parameter_to_converge_input_variable_name}")
        if self._parameter_to_converge_units is not None:
            delta_plot.xlabel += " [" + self._parameter_to_converge_units + "]"
            etot_plot.xlabel += " [" + self._parameter_to_converge_units + "]"
        etot_plot.ylabel = r"$E_{tot}$ [meV/at]"
        delta_plot.ylabel = r"$\Delta E_{tot}$ [meV/at]"
        self._post_process_plot(etot_plot, name_extension="etot")
        self._post_process_plot(delta_plot, name_extension="delta")
        multi = MultiPlot(loglevel=self._loglevel)
        multi.add_plot(etot_plot, 0)
        multi.add_plot(delta_plot, 0)
        self.plot_calculation_parameters["show_legend_on"] = [[0, 1]]
        self._post_process_plot(multi, name_extension="combined")
        self.compute_convergence()

    def _get_scf_input_variables_single_calculation(self, icalc):
        vars_ = self.scf_input_variables.copy()
        vars_.update(self.scf_specific_input_variables[icalc])
        return vars_

    def _get_scf_smearing_root_dir(self, smearing):
        return os.path.join(
                self.scf_workdir,
                f"{self.smearings_input_variable_name}{smearing}")

    def _get_scf_workdir_single_calculation(self, iparam):
        params = self.scf_specific_input_variables[iparam]
        smearing = params[self.smearings_input_variable_name]
        return os.path.join(
                self._get_scf_smearing_root_dir(smearing),
                self.kgrids_input_variable_name +
                self._convert_param_to_str(
                    params[self.kgrids_input_variable_name], iparam))


class SmearingPhononConvergenceSequencer(
        SmearingConvergenceSequencer,
        BasePhononConvergenceSequencer):
    """Convergence sequencer for the smearing parameters vs the phonon
    frequencies.
    """
    _all_sequencer_prefixes = ("scf_", "phonons_", "plot_", )

    def __init__(self, *args, **kwargs):
        SmearingConvergenceSequencer.__init__(self, *args, **kwargs)
        BasePhononConvergenceSequencer.__init__(self, *args, **kwargs)

    def compute_convergence(self, *args, **kwargs):
        """Computes phonon smearing convergence.
        """
        smearing_dirs = [
                self._get_phonons_smearing_root_dir(smearing)
                for smearing in self.scf_smearings]
        optimal_params = None
        for smearing_dir, smearing in zip(smearing_dirs, self.scf_smearings):
            frequencies, deltas = self._get_phonon_frequencies(
                    rootdir=smearing_dir)
            criterion = self.phonons_convergence_criterion
            for ikgrid, deltas_this_kgrid in enumerate(deltas):
                if all(deltas_this_kgrid <= criterion):
                    # converged this kgrid
                    if optimal_params is None:
                        optimal_params = {
                                self.smearings_input_variable_name: smearing,
                                "ikgrid": ikgrid}
                    else:
                        # check if ikgrid is lower. it not, continue
                        # assuming kpt grids are ordered from coarser to denser
                        if ikgrid <= optimal_params["ikgrid"] and (
                                smearing <= optimal_params[
                                    self.smearings_input_variable_name]):
                            optimal_params[
                                self.smearings_input_variable_name] = smearing
                            optimal_params["ikgrid"] = ikgrid
                    break
            if optimal_params is not None:
                self._scf_converged_parameter = {
                    self.smearings_input_variable_name: optimal_params[
                        self.smearings_input_variable_name],
                    self.kgrids_input_variable_name: (
                        self.scf_kgrids[optimal_params["ikgrid"]]),
                    }

    # TODO: DRY this method with the 'compute_convergence' method.
    def plot_convergence(self, *args, **kwargs):
        smearing_dirs = [
                self._get_phonons_smearing_root_dir(smearing)
                for smearing in self.scf_smearings]
        optimal_params = None
        for smearing_dir, smearing in zip(smearing_dirs, self.scf_smearings):
            multi_plot = BasePhononConvergenceSequencer.plot_convergence(
                    self, rootdir=smearing_dir, post_process=False)
            multi_plot.title = (
                    f"{self.smearings_input_variable_name} = "
                    f"{smearing}{self.smearings_input_variable_units}")
            all_deltas = np.transpose(
                    [curve.ydata for curve in multi_plot.plots[1].curves])
            # if at some point, all deltas are below convergence criterion
            # consider these frequencies converged
            criterion = self.phonons_convergence_criterion
            for ikgrid, deltas_this_kgrid in enumerate(all_deltas):
                if all(deltas_this_kgrid <= criterion):
                    # converged this kgrid
                    if optimal_params is None:
                        optimal_params = {
                                self.smearings_input_variable_name: smearing,
                                "ikgrid": ikgrid}
                    else:
                        # check if ikgrid is lower. it not, continue
                        # assuming kpt grids are ordered from coarser to denser
                        if ikgrid <= optimal_params["ikgrid"] and (
                                smearing <= optimal_params[
                                    self.smearings_input_variable_name]):
                            optimal_params[
                                self.smearings_input_variable_name] = smearing
                            optimal_params["ikgrid"] = ikgrid
                    break
            if optimal_params is not None:
                self._scf_converged_parameter = {
                    self.smearings_input_variable_name: optimal_params[
                        self.smearings_input_variable_name],
                    self.kgrids_input_variable_name: (
                        self.scf_kgrids[optimal_params["ikgrid"]]),
                    }
            self._post_process_plot(
                    multi_plot,
                    name_extension=(
                        f"{self.smearings_input_variable_name}{smearing}"))

    def _get_phonons_smearing_root_dir(self, smearing):
        return os.path.join(
                self.phonons_workdir,
                f"{self.smearings_input_variable_name}{smearing}")

    def _get_phonon_workdir(self, iph, scf_calc):
        icalc = self.sequence.index(scf_calc)
        params = self.scf_specific_input_variables[icalc]
        smearing = params[self.smearings_input_variable_name]
        return os.path.join(
            self._get_phonons_smearing_root_dir(smearing),
            self.kgrids_input_variable_name +
            self._convert_param_to_str(
                params[self.kgrids_input_variable_name], icalc))
