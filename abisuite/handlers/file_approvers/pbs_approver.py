import os

from .bases import BaseApprover
from .exceptions import PBSError
from ..file_structures import PBSStructure
from ...linux_tools import which


class PBSApprover(BaseApprover):
    """Class that checks if the variables given to build a pbs batch file
    are ok.
    """
    _loggername = "PBSApprover"
    _exception = PBSError
    _structure_class = PBSStructure

    def validate(self):
        super().validate()
        self._check_cmd()
        self._check_mem()

    def _check_cmd(self):
        # validate command script
        full_cmd = self.command
        split = full_cmd.split(" ")
        cmd = split[0]  # cmd without args
        if os.path.isfile(cmd):
            # command exists and is ready to use
            return
        # check if command exist in local environment
        if which(cmd) is not None:
            if self.queuing_system == "local":
                return
            else:
                self._logger.warning("Command '{cmd}' is found in env but we "
                                     "are not on local cluster: might not be "
                                     "available during runtime. "
                                     "Assume expert user.")
                return
        self.errors.append(f"Command '{cmd}' not found!")
        # check that we specify a command if we specify command arguements!
        if self.command_arguments and not self.command:
            self.errors.append(
                    "Need to specify a command if we specify cmd args!")

    def _check_mem(self):
        if self.nodes is not None:
            # nothing to check
            return
        # no nodes specified => need to specify memory-per-cpu on slurm
        if self.queuing_system != "slurm":
            return
        if self.memory_per_cpu is None:
            self.errors.append(
                    "If no nodes specified, need to specify memory_per_cpu.")
