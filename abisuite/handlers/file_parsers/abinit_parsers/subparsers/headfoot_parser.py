import abc

from ...bases import BaseSubParser
from .....bases import BaseUtility
from .....variables.abinit_variables import (
        ALL_ABINIT_VARIABLES, ALL_ABINIT_ANADDB_VARIABLES)
from .....routines import decompose_line, is_list_like, is_2d_arr, is_vector


class HeaderFooterBaseParser(BaseSubParser, abc.ABC):
    """Base class for the header and footer parser classes.
    """
    _skiplines = None
    _trigger_end = None

    def _extract_data_from_lines(self, lines):
        # self._logger.debug(f"=== Parsing {self.subject} ===")
        data = {}
        end = None
        skip = 0
        # alias because func name is too long
        var_start_here = VariableSubParser.variable_def_start_here
        for index, line in enumerate(lines):
            if index < skip or index == 0:
                # skip first line as it is confusing for the VarSubParser
                continue
            if line in self._skiplines:
                continue
            # Other exceptional lines to skip...
            if "These variables are accessible" in line:
                continue
            if "outvar_i_n" in line:
                continue
            if "prtocc" in line:
                continue
            if "outvars :" in line:
                continue
            if self._trigger_end in line:
                # data end here
                # self._logger.debug(f"Found end trigger for {self.subject}"
                #                    f" in {line}.")
                end = index - 1
                break
            elif var_start_here(
                    line, ignore_str=["-", "P"]):
                # parse variable
                # self._logger.debug(f"A variable start found in: {line}.")
                varparser = VariableSubParser(lines[index:])
                data[varparser.name] = varparser.value
                skip += varparser.ending_relative_index
                continue
        if end is None:
            raise LookupError("Could not find the end header/footer")
        self._ending_relative_index = end
        return data


class FooterParser(HeaderFooterBaseParser):
    """Class that parses the abinit output file's footer.
    """
    _loggername = "FooterParser"
    subject = "output_variables"
    trigger = "-outvars: echo values of variables"
    _trigger_end = "=========================================="
    _skiplines = []


class HeaderParser(HeaderFooterBaseParser):
    """Class that parses the abinit output file's header.
    """
    _loggername = "HeaderParser"
    subject = "input_variables"
    trigger = "-outvars: echo values of preprocessed"
    _trigger_end = "=========================================="
    _skiplines = []


class VariableSubParser(BaseUtility):
    """Input variable subparser. Parses lines that starts with a certain
    keyword and returns the associated value.
    """
    _loggername = "VariableSubParser"

    def __init__(self, lines, **kwargs):
        """VariableSubParser init method.

        Parameters
        ----------
        lines : list
                The list of the input file lines. The first line starts
                with a specific abinit input variable keyword.
        """
        super().__init__(**kwargs)
        lines = self.check_star_def(lines)
        name, value, rel_end = self._process(lines)
        self.name = name
        self.value = value
        self.ending_relative_index = rel_end

    @staticmethod
    def variable_def_start_here(line, ignore_str=None):
        """Checks that a variable is defined on this line.

        Parameters
        ----------
        line : str
               The line to check if a variable start here.
        ignore_str : list, optional
                     Ignore strings from this list.
        """
        # check if there is a star definition
        line = VariableSubParser.check_star_def([line])[0]
        s, i, f = decompose_line(line)
        if ignore_str is not None:
            if isinstance(ignore_str, str):
                ignore_str = [ignore_str]
            for string in ignore_str:
                if string in s:
                    s.remove(string)
        # varname value(strings, ints, floats) units
        if not s:
            # no string
            return False
        # if only one string in line => variable start here
        if len(s) == 1 and (not i and not f):
            return True
        # if only one string with other stuff, if the line starts with
        # the one string, it is the variable's name
        if len(s) == 1 and (len(i) or len(f)) and line.startswith(s[0]):
            return True
        # if more than one string, but line starts with first => var start here
        if len(s) > 1 and line.startswith(s[0]):
            return True
        # for everything else return false
        return False

    @staticmethod
    def check_star_def(lines):
        # TODO: Maybe there is a cleaner way to deal with this than
        #       preprocessing the lines.
        # sometimes, in inputs, there is variables defined with a '*'.
        # e.g.: acell 3*1.11 = acell 1.11 1.11 1.11
        # we need to preprocess those lines first before getting the variables
        for i, line in enumerate(lines):
            if "*" not in line:
                # nothing to worry about this line
                continue
            # EXCEPTION FOR istwfk which can be set to '*1' which is not
            # a star def
            if "istwfk" in line:
                continue
            # if we are here, there is a '*' in the line. change it
            newline = ""
            strings = line.split(" ")
            # the first string in the list is either a variable keyword or
            # or a float (possibly with a * in it).
            for s in strings:
                # if it can't be eval, it is a variable name
                if not len(s):
                    continue
                try:
                    eval(s)
                except (NameError, SyntaxError):
                    # it is a variable name or other things
                    pass
                else:
                    # it is a number. check if it has a '*'
                    if "*" not in s:
                        pass
                    else:
                        # it has a '*' and it is a number, convert it to
                        # a sequence of strings
                        splitted = s.strip().split("*")
                        N = int(splitted[0])
                        torepeat = splitted[-1]
                        s = " ".join([torepeat] * N)
                newline += s + " "
            lines[i] = newline
        return lines

    def _process(self, lines):
        # get the inputs variables in the input file
        # first line should contain the variable name
        s, i, f = decompose_line(lines[0])
        name = s[0]
        # EXCEPTION FOR istwfk which can be set to '*1' which is a string
        if name == "istwfk":
            elements = lines[0].split(" ")
            return name, elements[-1], 1
        value = []
        # get all data in order
        for i, line in enumerate(lines):
            if self.variable_def_start_here(line) and i > 0:
                # new variable start here, we've come to an end
                # break loop
                break
            # split line by spaces
            split = line.split(" ")
            subdata = []
            # append all data
            for element in split:
                try:
                    i = int(element)
                    subdata.append(i)
                except ValueError:
                    # not an int
                    try:
                        f = float(element)
                        subdata.append(f)
                    except ValueError:
                        # not a float either, do nothing else
                        pass
            # append array to full value of variable
            # if no data to append, line contained only var def
            if subdata:
                value.append(subdata)
        return name, self._format_value(name, value), i - 1

    def _format_value(self, name, value):
        # format value to the proper way if needed
        if is_list_like(value):
            if len(value) == 1:
                return self._format_value(name, value[0])
        # check vartypes as defined in the variables_db.
        # cast value accordingly
        # first do exceptions
        if name == "etotal":
            return float(value)
        if name in ("fcart", "strten", "xangst", ):
            return value
        # we should not do this like this in case the same variable
        # appears in both DBs...
        # FIXME: (2021/04/17)
        present = (name in ALL_ABINIT_VARIABLES or
                   name in ALL_ABINIT_ANADDB_VARIABLES)
        if not present:
            return value
        try:
            type_ = ALL_ABINIT_VARIABLES[name]["type"]
        except KeyError:
            type_ = ALL_ABINIT_ANADDB_VARIABLES[name]["type"]
        # TODO: Generalize this...
        if type_ == "vector" and not is_list_like(value):
            return (value, )
        if type_ == "2darr":
            if not is_list_like(value):
                return ((value, ))
            if is_vector(value):
                return (value, )
            if not is_2d_arr(value):
                raise LookupError(value)
        return value
