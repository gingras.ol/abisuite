from .bases import BaseQECalculationStatusChecker


class QEPW2Wannier90CalculationStatusChecker(BaseQECalculationStatusChecker):
    """Status checker for a pw2wannier90.x calculation from Quantum Espresso.
    """
    _loggername = "QEPW2Wannier90CalculationStatusChecker"
