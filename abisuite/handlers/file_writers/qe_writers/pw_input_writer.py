from .bases import BaseQEInputWriter
from ...file_structures import QEPWInputStructure


class QEPWInputWriter(BaseQEInputWriter):
    """Class that can write Quantum Espresso pw.x input files.
    """
    _loggername = "QEPWInputWriter"
    _structure_class = QEPWInputStructure

    @property
    def lines(self):
        # start with control variables
        lines = []
        self._append_var_block(lines, "control")
        self._append_var_block(lines, "system")
        self._append_var_block(lines, "electrons")
        # the next blocks are only written if needed.
        self._append_var_block(lines, "ions")
        self._append_var_block(lines, "cell")
        # atomic species should appear in first
        if "atomic_species" not in self.input_variables:
            raise ValueError("Need to set 'atomic_species'.")
        input_vars = self.input_variables.copy()
        atomic_species = {"atomic_species": input_vars.pop("atomic_species")}
        self._append_others(lines, input_variables=atomic_species)
        self._append_others(lines, input_variables=input_vars)
        return lines

    def _append_var_block(self, lines, varblock):
        if varblock == "ions" or varblock == "cell":
            # append only if needed
            calc = self.input_variables.get("calculation", "scf")
            if calc.value not in ("relax", "vc-relax"):
                # don't need the variable block => return
                return
        super()._append_var_block(lines, varblock)
