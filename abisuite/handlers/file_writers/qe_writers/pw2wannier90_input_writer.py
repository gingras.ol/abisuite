from .bases import BaseQEInputWriter
from ...file_structures import QEPW2Wannier90InputStructure


class QEPW2Wannier90InputWriter(BaseQEInputWriter):
    """Class that can write a Quantum Espresso pw2wannier90.x input file.
    """
    _loggername = "QEPW2Wannier90InputWriter"
    _structure_class = QEPW2Wannier90InputStructure

    @property
    def lines(self):
        lines = []
        self._append_var_block(lines, "inputpp")
        return lines
