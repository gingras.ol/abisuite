from .bases import BaseQEBandFreqStructure


class QEMatdynFreqStructure(BaseQEBandFreqStructure):
    """Structure for a '.freq' file created by Quantum Espresso.
    """
