.. abisuite documentation master file, created by
   sphinx-quickstart on Fri May 17 12:24:29 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to abisuite's documentation!
====================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   README
   installation
   config_file
   doc_api/abisuite
   doc_tutorials/tutorials

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

Documentation TODO
==================
These are the missing parts of the documentation

   tutorials
   examples
   contribution
