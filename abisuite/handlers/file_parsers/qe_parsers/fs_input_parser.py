from .bases import BaseQEInputParser
from ...file_structures import QEFSInputStructure


class QEFSInputParser(BaseQEInputParser):
    """Class that can parse a Quantum Espresso fs.x input File.
    """
    _loggername = "QEFSInputParser"
    _structure_class = QEFSInputStructure
