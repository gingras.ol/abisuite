import os
import tempfile

from abisuite.databases import AbiDB
from abisuite.databases.abi_db import (
        __CALCULATIONS_TABLE_COLUMNS__, __CALCULATIONS_TABLE_NAME__,
        )
from abisuite.databases.exceptions import CalculationDoesNotExistError
from ..bases import TestCase
from ..routines_for_tests import copy_calculation
from ..variables_for_tests import QEPWCalc


class TestAbiDB(TestCase):
    """Test case for an Abisuite Database.
    """
    def setUp(self):
        self.tempdbdir = tempfile.TemporaryDirectory()
        self.dbpath = os.path.join(self.tempdbdir.name, "test.db")
        self.db = AbiDB(loglevel=10)
        self.db.create_database(self.dbpath)

    def tearDown(self):
        self.tempdbdir.cleanup()

    def test_delete_calculation(self):
        tempdir = tempfile.TemporaryDirectory()
        newdir = os.path.join(tempdir.name, "test")
        try:
            copy_calculation(QEPWCalc, newdir)
            self.db.add_calculation(newdir)
            self.assertEqual(len(self.db.select("*")), 1)
            self.assertIn(newdir, self.db)
            # check deleting by calculation path
            self.db.delete_calculation(newdir)
            self.assertNotIn(newdir, self.db)
            self.assertEqual(len(self.db.select("*")), 0)
            # check deleting by ID
            self.db.add_calculation(newdir)
            self.db.delete_id(self.db[newdir])
            self.assertNotIn(newdir, self.db)
            self.assertEqual(len(self.db.select("*")), 0)
        finally:
            tempdir.cleanup()

    def test_add_calculation(self):
        self.assertEqual(len(self.db.select("*")), 0)  # supposed to be empty
        # test adding a real calculation (e.g.: a QE_PW calculation)
        tempdir = tempfile.TemporaryDirectory()
        newdir = os.path.join(tempdir.name, "test")
        try:
            copy_calculation(QEPWCalc, newdir)
            self.db.add_calculation(newdir)
            selection = self.db.select("*")
            self.assertEqual(len(selection), 1)
            calc = selection[0]
            self.assertEqual(calc[0], self.db.last_calculation_id)
            self.assertEqual(calc[1], newdir)
            # check that the db integrity check does not raise an error
            self.db.check_database_integrity()
            # check retriving data
            self.assertEqual(self.db[newdir], 1)
            self.assertEqual(self.db[1], newdir)
        finally:
            tempdir.cleanup()
        # check retrieving wrong data raises errors
        with self.assertRaises(CalculationDoesNotExistError):
            self.db[2]
        with self.assertRaises(CalculationDoesNotExistError):
            self.db["non_existent_calculation"]

    def test_create_database(self):
        self.assertEqual(len(self.db.tables), 1)
        self.assertEqual(len(self.db.columns[__CALCULATIONS_TABLE_NAME__]),
                         len(__CALCULATIONS_TABLE_COLUMNS__) - 1)
        for column in __CALCULATIONS_TABLE_COLUMNS__:
            if column == "primary_key":
                continue
            self.assertIn(column, self.db.columns[__CALCULATIONS_TABLE_NAME__])
