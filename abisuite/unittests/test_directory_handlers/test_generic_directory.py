from .bases import BaseDirectoryHandlerTest
from ...handlers import GenericDirectory
import unittest


class TestGenericDirectory(BaseDirectoryHandlerTest, unittest.TestCase):
    _directory_handler_class = GenericDirectory
