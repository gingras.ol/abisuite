set style data dots
set nokey
set xrange [0: 4.46680]
set yrange [ -6.79451 : 16.87636]
set arrow from  0.99377,  -6.79451 to  0.99377,  16.87636 nohead
set arrow from  2.14128,  -6.79451 to  2.14128,  16.87636 nohead
set arrow from  2.54699,  -6.79451 to  2.54699,  16.87636 nohead
set arrow from  3.24969,  -6.79451 to  3.24969,  16.87636 nohead
set xtics (" L "  0.00000," G "  0.99377," X "  2.14128," U "  2.54699," K "  3.24969," G "  4.46680)
 plot "../wannier_run_band.dat"
