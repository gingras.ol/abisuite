import abc
import logging
import os

import numpy as np

from .bases import BasePostProcClass_w_Fermi, BasePostProcClass
from ..handlers import (
        AbinitDOSFile, AbinitProjectedDOSFile, CalculationDirectory,
        MetaDataFile, QEDOSDOSFile, QEMatdynDOSFile, QEPWLogFile,
        QEProjwfcPDOSFile,
        )
from ..plotters import MultiPlot
from ..routines import is_list_like, is_scalar_or_str


class BaseDOSPlot(BasePostProcClass_w_Fermi, abc.ABC):
    """Base class for DOS post processing classes.

    This class is not meant to be used directly.
    """

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._energies = None

    @property
    def energy(self):
        # just an alias
        return self.energies

    @property
    def energies(self):
        if self._energies is not None:
            return self._energies
        raise ValueError("Set 'energies' before using them.")

    @energies.setter
    def energies(self, energies):
        if not is_list_like(energies):
            raise TypeError(f"energies must be list like but got: {energies}")
        if not isinstance(energies, np.ndarray):
            energies = np.array(energies)
        self._energies = energies

    def get_plot(self, *args, line_at_fermi=True, vertical=True, **kwargs):
        """Returns the DOS plot object.

        Parameters
        ----------
        line_at_fermi : bool, optional
                        If True, a line is drawn at the Fermi level.
        other kwargs:
            are passed as arguments for the bands (e.g.: color,
            linewidth, linestyle, etc.) or to create xlabels, xunits title
        """
        plot = super().get_plot(*args, **kwargs)
        if line_at_fermi:
            if vertical:
                plot.add_hline(0, linestyle="--", color="k")
            else:
                plot.add_vline(0, linestyle="--", color="k")
        self._plot = plot
        return plot


# FIXME: this class is not aging well for some part. It could use a refresher
# FG (2021/05/10)
class DOSPlot(BaseDOSPlot):
    """Class that can plot a DOS graph.
    """
    _loggername = "DOSPlot"

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._dos = None

    @property
    def dos(self):
        if self._dos is not None:
            return self._dos
        raise ValueError("Set 'dos' before using it.")

    @dos.setter
    def dos(self, dos):
        if not is_list_like(dos):
            raise TypeError(f"'dos' must be list like but got: {dos}")
        if not isinstance(dos, np.ndarray):
            dos = np.array(dos)
        self._dos = dos

    def get_plot(self, *args, vertical=True, line_at_fermi=True, **kwargs):
        """Returns the DOS plot object.

        Parameters
        ----------
        vertical : bool, optional
            If True, the DOS is plotted vertically (to compare with
            band structure).
        line_at_fermi : bool, optional
                        If True, a line is drawn at the Fermi level.
        other kwargs:
            are passed as arguments for the bands (e.g.: color,
            linewidth, linestyle, etc.) or to create xlabels, xunits title
        """
        try:
            fermi_energy = self.fermi_energy
        except ValueError:
            self._logger.warning("Fermi energy not set, assuming it is 0...")
            fermi_energy = 0
        plot_kwargs = self._extract_main_plot_params_from_kwargs(kwargs)
        plot = super().get_plot(
                line_at_fermi=line_at_fermi,
                *args, **plot_kwargs)
        if vertical:
            ys = self.energies - fermi_energy
            xs = self.dos
        else:
            ys = self.dos
            xs = self.energies - fermi_energy
        plot.add_curve(xs, ys, **kwargs)
        return plot

    @classmethod
    def from_abinit_calculation(cls, calc, *args, **kwargs):
        """Returns a DOSPlot object from a path or a CalculationDirectory
        instance representing an abinit calculation.
        """
        if isinstance(calc, CalculationDirectory):
            path = calc.path
        else:
            path = calc
        with AbinitDOSFile.from_calculation(path) as dos:
            return cls.from_handler(dos)

    @classmethod
    def from_handler(cls, handler, *args, **kwargs):
        """Create DOSPlot object using a DOS file handler.
        """
        instance = cls(*args, **kwargs)
        with handler:
            instance.energies = handler.energies
            instance.dos = handler.dos
            instance.fermi_energy = handler.fermi_energy
        return instance

    @classmethod
    def from_qedos(cls, path, *args, **kwargs):
        """Create DOSPlot object using data extracted using a QEDOSParser.

        Parameters
        ----------
        path : str
               path to the file containing the DOS.
        """
        lvl = kwargs.get("loglevel", logging.INFO)
        with QEDOSDOSFile.from_file(path, loglevel=lvl) as dos:
            return cls.from_handler(dos, *args, **kwargs)

    @classmethod
    def from_qematdyn(cls, path, *args, dos=0, **kwargs):
        """Create a DOSPlot object from a matdyn.x calculation from the
        Quantum Espresso package.

        Parameters
        ----------
        path : str
            The path to the matdyn calculation.
        dos : int, optional
            The dos column to use for the plot. By default it is the first one
            (0th) as it is the total DOS.
        """
        instance = cls(*args, **kwargs)
        with QEMatdynDOSFile.from_calculation(
                path, loglevel=instance._loglevel) as matdyn:
            instance.energies = matdyn.energies
            instance.dos = matdyn.dos[dos]
            instance.fermi_energy = 0  # phonons don't have a fermi lvl duh
        return instance

    @classmethod
    def from_calculation(cls, path, *args, **kwargs):
        """Create a DOSPlot object from a calculation.

        The calculation directory must contain a .meta file.
        """
        lvl = kwargs.get("loglevel", logging.INFO)
        if not os.path.isdir(path):
            raise NotADirectoryError(path)
        with CalculationDirectory.from_calculation(path, loglevel=lvl) as calc:
            calctype = calc.calctype
        if calctype == "qe_dos":
            return cls.from_qedos(path, *args, **kwargs)
        elif calctype == "qe_matdyn":
            # phonon dos
            return cls.from_qematdyn(path, *args, **kwargs)
        elif calctype == "abinit":
            return cls.from_abinit_calculation(calc, *args, **kwargs)
        # else, raise a not implemented error
        raise NotImplementedError(calctype)


class ProjectedDOSPlot(BaseDOSPlot):
    """Post processing class for a projected DOS plot.
    """
    _loggername = "ProjectedDOSPlot"

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._projected_dos = None
        self.atom_number = None

    @property
    def projected_dos(self):
        if self._projected_dos is not None:
            return self._projected_dos
        raise ValueError("Need to set 'projected_dos'.")

    @projected_dos.setter
    def projected_dos(self, dos):
        if not isinstance(dos, dict):
            raise TypeError("'projected_dos' should be a dict.")
        self._projected_dos = dos

    def get_plot(self, *args, vertical=True, line_at_fermi=True, **kwargs):
        """Construct the plot of projected dos.

        Parameters
        ----------
        vertical: bool, optional
            If True, the plot will be made vertical.
        line_at_fermi : bool, optional
                        If True, a line is drawn at the Fermi level.
        Other args:
            are passed to the mother's class method and the
            'add_curve' method of the Plot object.

        Returns
        -------
        tuple of 2 elements:
            1 - list of Plot objects, one for each l
            2 - MultiPlot object of all these plots
        """
        try:
            fermi_energy = self.fermi_energy
        except ValueError:
            self._logger.warning("Fermi energy not set, assuming it is 0...")
            fermi_energy = 0
        plot_params = self._extract_main_plot_params_from_kwargs(kwargs)
        plots = []
        multi_plot = MultiPlot()
        # there are 5 projected dos curves (l=0..4) (s, p, d, f, g)
        colors = ["r", "g", "b", "m", "c", "orange", "gold", "brown",
                  "fuchsia"]
        if "title" not in plot_params:
            og_title = ""
        else:
            og_title = plot_params["title"] + " "
        for orbital_idx in range(5):
            title = og_title + f"atom={self.atom_number} l={orbital_idx}"
            plot_params["title"] = title
            plot = super().get_plot(
                *args, line_at_fermi=line_at_fermi, **plot_params)
            plots.append(plot)
            multi_plot.add_plot(plot, row=orbital_idx // 2)
            color_idx = 0
            for curve_label, projected_dos in self.projected_dos.items():
                l_idx = int(curve_label.split("=")[1][0])
                if l_idx != orbital_idx:
                    continue
                # retain this curve
                if vertical:
                    ys = self.energies - fermi_energy
                    xs = projected_dos
                else:
                    ys = projected_dos
                    xs = self.energies - fermi_energy
                if curve_label.startswith("l="):
                    color = "k"
                else:
                    color = colors[color_idx]
                    color_idx += 1
                plot.add_curve(
                        xs, ys, color=color, label=curve_label,
                        **kwargs)
        return plots, multi_plot

    @classmethod
    def from_abinit_projected_dos_handler(cls, handler, *args, **kwargs):
        """Create a ProjectedDOSPlot object from an AbinitProjectedDOSFile
        handler.

        Parameters
        ----------
        handler: AbinitProjectedFile object
            The file handler for a given DOS_AT file.
        The other args and kwargs:
            are passed to the init method.

        Raises
        ------
        ValueError: if the handler is not an AbinitProjectedFile

        Returns
        -------
        ProjectedDOSPlot object
        """
        if not isinstance(handler, AbinitProjectedDOSFile):
            raise ValueError(handler)
        dos = cls(*args, **kwargs)
        with handler:
            dos.energies = handler.energies
            dos.projected_dos = handler.projected_dos
            dos.fermi_energy = handler.fermi_energy
            dos.atom_number = int(handler.path.split("_DOS_AT")[-1])
        return dos

    @classmethod
    def from_calculation(cls, path, *args, **kwargs):
        """Instanciate the ProjectedDOSPlot object(s) from a calculation path.

        Parameters
        ----------
        path: str
            The calculation directory path.

        Raises
        ------
        NotImplementedError:
            If not an abinit calculation (only this one is
            supported for now).
        ValueError:
            If the calculation is not a projected DOS calculation.

        Returns
        -------
        list: The list of ProjectedDOSPlot objects. One for each DOS_AT file.
        """
        with CalculationDirectory.from_calculation(path) as calc:
            calctype = calc.calctype
        if calctype != "abinit":
            raise NotImplementedError(calctype)
        with calc.input_file as inp:
            prtdos = inp.input_variables.get("prtdos", 0).value
            prtdosm = inp.input_variables.get("prtdosm", 0).value
            if prtdos != 3 or prtdosm != 1:
                raise ValueError(f"Not a projected DOS calculation: '{path}'.")
        projected_dos = AbinitProjectedDOSFile.from_calculation(path)
        # projected dos is a list of AbinitProjectedDOSFile here
        # return a list of projected dos plot objects as well
        return [cls.from_abinit_projected_dos_handler(x, *args, **kwargs)
                for x in projected_dos]


# TODO: (FG 2021/05/19) this class should be deleted i'm not sure it
# is still used anymore.
class MassDOSPlot(BasePostProcClass):
    """Class that can plot multiple DOS graphs.
    """
    _loggername = "MassDOSPlot"

    def get_plot(self, *args, **kwargs):
        plots = []
        for label, dos in self.dos.items():
            plots.append(dos.get_plot(*args, label=label, **kwargs))
        if not len(plots):
            raise LookupError("No plots to return...")
        if len(plots) == 1:
            return plots[0]
        plot = sum(plots[1:], plots[0])
        return plot

    @property
    def energies(self):
        if self._dos is None:
            raise ValueError("Set energies before using them.")
        return {label: dos.energies for label, dos in self.dos.items()}

    @energies.setter
    def energies(self, energies):
        if not is_list_like(energies):
            raise TypeError(f"Expected list but got '{energies}'")
        if len(self.dos) != len(energies):
            raise ValueError("Len of DOS does not match len of energies")
        for energie, dos in zip(energies, self.dos.values()):
            dos.energies = energie

    @property
    def dos(self):
        if self._dos is None:
            raise ValueError("Set dos before.")
        return self._dos

    @dos.setter
    def dos(self, dos):
        if not isinstance(dos, dict):
            raise TypeError(f"Expected dict but got '{dos}'")
        if self._dos is None:
            self._dos = {label: DOSPlot(loglevel=self._logger.level)
                         for label in dos}
        for dos_values, dos_obj in zip(dos.values(), self.dos.values()):
            dos_obj.dos = dos_values

    @property
    def fermi_energies(self):
        if self._dos is None:
            raise ValueError("Set fermi_energies before using them.")
        return {label: dos.fermi_energy for label, dos in self.dos.items()}

    @fermi_energies.setter
    def fermi_energies(self, energies):
        if is_scalar_or_str(energies):
            energies = [energies] * len(self.dos)
        if not is_list_like(energies):
            raise TypeError(f"Expected list but got '{energies}'")
        if len(self.dos) != len(energies):
            raise ValueError("Len of DOS does not match len of fermi_energies")
        for energie, dos in zip(energies, self.dos.values()):
            dos.fermi_energy = energie

    @classmethod
    def from_calculation(cls, path, *args, **kwargs):
        """Initiate MultiDOSPlot instance from a calculation directory.

        Useful for projected DOS calculation where a lot of DOS are to
        plot.
        """
        lvl = kwargs.get("loglevel", logging.INFO)
        if not os.path.isdir(path):
            raise NotADirectoryError(path)
        with MetaDataFile.from_calculation(path, loglevel=lvl) as meta:
            calctype = meta.calctype
            parents = meta.parents
        if calctype != "qe_projwfc":
            raise NotImplementedError(calctype)
        # Get DOS data.
        handlers = QEProjwfcPDOSFile.from_calculation(path, *args, **kwargs)
        instance = cls(loglevel=lvl)
        instance._logger.info(f"Got {len(handlers)} DOS to process.")
        all_dos = {}
        all_energies = []
        for label, handler in handlers.items():
            handler.read()
            for key in handler.structure.get_relevant_attributes():
                if key in ("energies", "ldos"):
                    continue
                dos = getattr(handler, key)
                if dos is None:
                    continue
                all_dos[label + f"_{key}"] = dos
                all_energies.append(handler.energies)
        instance.dos = all_dos
        instance.energies = all_energies

        # try to get fermi energy from parent calculation. Because projwfc
        # doesn't print fermi energy anywhere
        with QEPWLogFile.from_calculation(parents[0], loglevel=lvl) as log:
            if log.fermi_energy is not None:
                instance.fermi_energies = log.fermi_energy
                return instance
        # if parent does not hold the fermi energy, try its parent
        with MetaDataFile.from_calculation(parents[0], loglevel=lvl) as meta:
            parent = meta.parents[0]
        with QEPWLogFile.from_calculation(parent, loglevel=lvl) as log:
            if log.fermi_energy is not None:
                instance.fermi_energies = log.fermi_energy
                return instance
        raise ValueError("Could not get fermi energy from parents...")
