from .individual_phonon_sequencers import (
        AbinitPhononConvergenceSequencer,
        )
from ..kgrid_convergence_sequencers import (
        BaseKgridConvergenceSequencer,
        )
from ...routines import is_2d_arr, is_vector


class AbinitKgridConvergenceSequencer(BaseKgridConvergenceSequencer):
    """Convergence sequencer for the ngkpt parameter with abinit.
    """
    _loggername = "AbinitKgridConvergenceSequencer"

    def __init__(self, *args, **kwargs):
        BaseKgridConvergenceSequencer.__init__(self, "abinit", *args, **kwargs)

    def _add_xtick_labels_to_convergence_plot(self, plot):
        BaseKgridConvergenceSequencer._add_xtick_labels_to_convergence_plot(
                self, plot)
        # check dimension of this parameter
        kgrid0 = self.scf_kgrids[0]
        if is_vector(kgrid0):
            plot.xtick_labels = [(i, "x".join([str(k) for k in x]))
                                 for i, x in enumerate(self.scf_kgrids)]
        elif is_2d_arr(kgrid0):
            # don't print whole variable (too long)
            # instead print 'name #X'
            plot.xtick_labels = [
                    (i, f"{self.kgrids_input_variable_name} #{i}")
                    for i in range(len(self.scf_kgrids))]
        else:
            raise LookupError(kgrid0)


class AbinitKgridPhononConvergenceSequencer(
        AbinitKgridConvergenceSequencer,
        AbinitPhononConvergenceSequencer,
        ):
    """Convergence sequencer for the 'ngkpt' SCF parameter vs the
    phonon frequencies for abinit.
    """
    _all_sequencers_prefix = ("scf_", "phonons_", "plot_", )
    _loggername = "AbinitKgridPhononConvergenceSequencer"

    def __init__(self, *args, **kwargs):
        AbinitPhononConvergenceSequencer.__init__(self, *args, **kwargs)
        AbinitKgridConvergenceSequencer.__init__(self, *args, **kwargs)

    def init_sequence(self):
        AbinitPhononConvergenceSequencer.init_sequence(self)
