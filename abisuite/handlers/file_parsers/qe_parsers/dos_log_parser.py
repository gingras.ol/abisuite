from .bases import BaseQELogParser
from .subparsers.dos_log_subparsers import (
                QEDOSLogTimingSubParser,
                )
from ...file_structures import QEDOSLogStructure


class QEDOSLogParser(BaseQELogParser):
    """Parser class for a Quantum Espresso dos.x log file.
    """
    _loggername = "QEDOSLogParser"
    _structure_class = QEDOSLogStructure
    _subparsers = (
            QEDOSLogTimingSubParser,
            )
