from ..bases import BaseCleaner


class AbinitCleaner(BaseCleaner):
    """Cleaner class for abinit calculations.
    """
    _calctype = "abinit"
    _loggername = "AbinitCleaner"

    def delete_this_file(self, handler):
        for suffix in ("_WFK", "_DEN", "_1WF"):
            if suffix in handler.basename:
                return True
        return super().delete_this_file(handler)
