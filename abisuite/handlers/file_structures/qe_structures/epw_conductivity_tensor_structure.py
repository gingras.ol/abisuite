from ..bases import BaseStructure


class QEEPWConductivityTensorStructure(BaseStructure):
    """Structure class for a conductivity tensor file produced by the epw.x
    script of Quantum Espresso.
    """
    all_attributes = (
            "temperatures", "conductivity_tensor", "chemical_potentials")
