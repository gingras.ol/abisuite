#!/bin/bash


MPIRUN=""
EXECUTABLE=/home/fgoudreault/Workspace/abinit/build/master/vanilla/src/98_main/optic
INPUT=/home/fgoudreault/Workspace/abilaunch/examples/optic_mass_launcher/optic_comp_11/run/optic_comp_11.files
LOG=/home/fgoudreault/Workspace/abilaunch/examples/optic_mass_launcher/optic_comp_11/optic_comp_11.log
STDERR=/home/fgoudreault/Workspace/abilaunch/examples/optic_mass_launcher/optic_comp_11/optic_comp_11.stderr

$MPIRUN $EXECUTABLE < $INPUT > $LOG 2> $STDERR
