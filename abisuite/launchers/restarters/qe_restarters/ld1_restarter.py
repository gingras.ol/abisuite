from .bases import BaseQERestarter


class QELD1Restarter(BaseQERestarter):
    """Restarter class for a qe_ld1 calculation for the ld1.x script
    of Quantum Espresso.
    """
    _calctype = "qe_ld1"
    _loggername = "QELD1Restarter"
