import abc
import os

from ..bases import BaseUtility
from ..colors import Colors
from ..exceptions import DevError
from ..plotters import Plot, rand_cmap
from ..sequencers.exceptions import (
        SequenceNotConvergedError, SequenceNotCompletedError,
        )
from ..routines import is_list_like, is_vector


class BaseWorkflow(BaseUtility, abc.ABC):
    """Base class for workflows. All functionalities implemented in this base
    class are implemented in subclasses as well.

    For now supports:
      - GS ecut convergence
      - GS kgrid convergence
      - GS smearing convergence
      - Lattice + Atomic relaxation
      - Phonon frequencies ecut convergence
      - Phonon frequencies smearing convergence
      - GS calculation
      - Band Structure calculation
      - Phonon dispersion qpoint grid convergence
      - Phonon dispersion

    Note
    ----
    This class is abstract which means it cannot be instanciated directly.
    Use the corresponding subclass for the software you want to use.
    """
    _all_geometry_variables = None
    _band_structure_sequencer_cls = None
    _gs_sequencer_cls = None
    _gs_ecut_convergence_sequencer_cls = None
    _gs_kgrid_convergence_sequencer_cls = None
    _gs_smearing_convergence_sequencer_cls = None
    _phonon_ecut_convergence_sequencr_cls = None
    _phonon_smearing_convergence_sequencer_cls = None
    _phonon_dispersion_sequencer_cls = None
    _relaxation_sequencer_cls = None
    workflow = None
    _base_implemented_workflows = (
            "gs_ecut_convergence", "gs_kgrid_convergence",
            "gs_smearing_convergence", "relaxation",
            "phonon_ecut_convergence",
            "phonon_smearing_convergence", "gs", "band_structure",
            "phonon_dispersion_qgrid_convergence", "phonon_dispersion",
            )

    def __init__(
            self,
            band_structure=False,
            # bypass_convergence_check=False,
            # bypass_sequence_comparison=False,
            converged_gs_ecut=None,
            converged_gs_kgrid=None,
            converged_gs_smearing=None,
            converged_phonon_ecut=None,
            converged_phonon_smearing=None,
            gs=False,
            gs_ecut_convergence=False,
            gs_kgrid_convergence=False,
            gs_smearing_convergence=False,
            phonon_ecut_convergence=False,
            phonon_dispersion=False,
            phonon_dispersion_qgrid_convergence=False,
            phonon_smearing_convergence=False,
            relaxation=False,
            relaxed_geometry=None,
            **kwargs):
        """Workflow init method.

        Parameters
        ----------
        band_structure: bool, optional
            If True, will compute the band structure.
        converged_gs_ecut: dict, optional
            The dict of variables representing the converged ecut parameters.
            These variables are used if the gs_ecut_convergence is not
            computed.
        converged_gs_kgrid: dict, optional
            The dict of variables representing the converged kgrid parameters.
            These variables are used if the gs_kgrid_convergence is not
            computed.
        converged_gs_smearing: dict, optional
            The dict of variables representing
            the converged smearing parameters for gs.
            These variables are used
            if gs_smearing_convergence is not computed.
        converged_phonon_ecut: dict, optional
            The dict of variables representing
            the converged ecut parameters for phonons.
            These variables are used
            if phonon_ecut_convergence is not computed.
        converged_phonon_smearing: dict, optional
            The dict of variables representing
            the converged smearing parameters for phonons.
            These variables are used
            if phonon_smearing_convergence is not computed.
        gs: bool, optional
            If True, requires to compute a GS calculation.
        gs_ecut_convergence: bool, optional
            If True, the ecut convergence with respect to etot will be
            inspected.
        gs_kgrid_convergence: bool, optional
            If True, the kgrid convergence with respect to etot will
            be inspected.
        gs_smearing_convergence: bool, optional
            If True, the smearing convergence will be done.
        phonon_ecut_convergence: bool, optional
            If True, the phonon ecut convergence is added to the workflow.
        phonon_dispersion: bool, optional
            If True, the phonon dispersion is added to the workflow.
        phonon_smearing_convergence: bool, optional
            If True, the phonon smearing convergence is added to the workflow.
        phonon_dispersion_qgrid_convergence: bool, optional
            If True, the phonon dispersion qgrid convergence is added to the
            workflow.
        relaxation: bool, optional
            If True, the atomic / bond lengths will be computed. This will
            set the geometry for all calculations if not specified.
        relaxed_geometry: dict, optional
            The dict of variables representing the relaxed geometry parameters.
            These variables are used if the relaxation is not
            computed.
        """
        # FG: 2021/04/30 I removed these global parameters in profit of fine
        # tuning them on each sequencers
        # bypass_convergence_check: bool, optional
        #     If True, we don't care if a calculation in the Workflow
        #     is not converged. Otherwise errors are thrown whenever
        #     calculations are not converged.
        # bypass_sequence_comparison: bool, optional
        #     If True, the sequence calculations comparisons is bypassed in
        #     order to get results faster. Useful when all calculations are
        #     successfull and we don't want to check that everything is the
        #     same each time we rerun the Workflow.
        # self.bypass_convergence_check = bypass_convergence_check
        # self.bypass_sequence_comparison = bypass_sequence_comparison
        super().__init__(**kwargs)
        # GENERAL PART
        if self.workflow is None:
            raise DevError("'workflow cls attr must be defined.")
        for workflow in self._base_implemented_workflows:
            if workflow not in self.workflow:
                raise DevError(
                        f"'{workflow}' is implemented in base class but is not"
                        " in 'workflow'.")
        self._stop_at_workflow = None
        # GS ECUT CONVERGENCE PART
        if self._gs_ecut_convergence_sequencer_cls is None:
            raise DevError("Need to set '_gs_ecut_convergence_sequencer_cls'.")
        self._compute_gs_ecut_convergence = gs_ecut_convergence
        self._gs_ecut_convergence_sequencer = None
        self.converged_gs_ecut = converged_gs_ecut
        # GS KGRID CONVERGENCE PART
        if self._gs_kgrid_convergence_sequencer_cls is None:
            raise DevError(
                    "Need to set '_gs_kgrid_convergence_sequencer_cls'.")
        self._compute_gs_kgrid_convergence = gs_kgrid_convergence
        self._gs_kgrid_convergence_sequencer = None
        self.converged_gs_kgrid = converged_gs_kgrid
        # GS SMEARING CONVERGENCE PART
        if self._gs_smearing_convergence_sequencer_cls is None:
            raise DevError(
                "Need to set '_gs_smearing_convergence_sequencer_cls'.")
        self._compute_gs_smearing_convergence = gs_smearing_convergence
        self.converged_gs_smearing = converged_gs_smearing
        self._gs_smearing_convergence_sequencer = None
        # RELAXATION PART
        if self._relaxation_sequencer_cls is None:
            raise DevError("'_relaxation_sequencer_cls' need to be specified.")
        if self._all_geometry_variables is None:
            raise DevError("'_all_geometry_variables' needs to be specified.")
        self._compute_relaxation = relaxation
        self._relaxation_sequencer = None
        self.relaxed_geometry = relaxed_geometry
        # PHONON ECUT CONVERGENCE PART
        if self._phonon_ecut_convergence_sequencer_cls is None:
            raise DevError(
                    "'_phonon_ecut_convergence_sequencer_cls' needs to be"
                    "specified.")
        self._compute_phonon_ecut_convergence = phonon_ecut_convergence
        self._phonon_ecut_convergence_sequencer = None
        self.converged_phonon_ecut = converged_phonon_ecut
        # PHONON SMEARING CONVERGENCE PART
        if self._phonon_smearing_convergence_sequencer_cls is None:
            raise DevError(
                    "'_phonon_smearing_convergence_sequencer_cls' needs to be "
                    "specified.")
        self._compute_phonon_smearing_convergence = phonon_smearing_convergence
        self._phonon_smearing_convergence_sequencer = None
        self.converged_phonon_smearing = converged_phonon_smearing
        # GS PART
        if self._gs_sequencer_cls is None:
            raise DevError("'_gs_sequencer_cls' need to be specified.")
        self._compute_gs = gs
        self._gs_sequencer = None
        # BAND STRUCTURE PART
        if self._band_structure_sequencer_cls is None:
            raise DevError(
                    "'_band_structure_sequencer_cls' must be specified.")
        self._compute_band_structure = band_structure
        self._band_structure_sequencer = None
        # PHONON DISPERSION QGRID CONVERGENCE PART
        self._compute_phonon_dispersion_qgrid_convergence = (
                phonon_dispersion_qgrid_convergence)
        self._phonon_dispersion_qgrid_convergence_sequencer = None
        # PHONON DISPERSION PART
        if self._phonon_dispersion_sequencer_cls is None:
            raise DevError(
                    "'_phonon_dispersion_sequencer_cls' must be specified.")
        self._compute_phonon_dispersion = phonon_dispersion
        self._phonon_dispersion_sequencer = None

    @property
    def all_sequencers(self):
        """The list of all sequencers objects attached to the current workflow.

        Returns
        -------
        SequencersList instance.
        """
        sequencers = SequencersList([])
        for workflow in self.workflow:
            if getattr(self, f"_compute_{workflow}"):
                sequencers.append(getattr(self, f"{workflow}_sequencer"))
        return sequencers

    @property
    def band_structure_sequencer(self):
        """The corresponding sequencer for the 'band_structure' part of the
        workflow.
        """
        if self._band_structure_sequencer is not None:
            return self._band_structure_sequencer
        self._band_structure_sequencer = self._band_structure_sequencer_cls(
                loglevel=self._loglevel)
        return self.band_structure_sequencer

    @property
    def gs_sequencer(self):
        """The corresponding sequencer for the 'gs' part of the
        workflow.
        """
        if self._gs_sequencer is not None:
            return self._gs_sequencer
        self._gs_sequencer = self._gs_sequencer_cls(loglevel=self._loglevel)
        return self.gs_sequencer

    @property
    def gs_ecut_convergence_sequencer(self):
        """The corresponding sequencer for the 'gs_ecut_convergence' part of the
        workflow.
        """
        if self._gs_ecut_convergence_sequencer is not None:
            return self._gs_ecut_convergence_sequencer
        seq = self._gs_ecut_convergence_sequencer_cls(loglevel=self._loglevel)
        self._gs_ecut_convergence_sequencer = seq
        return self.gs_ecut_convergence_sequencer

    @property
    def gs_kgrid_convergence_sequencer(self):
        """The corresponding sequencer for the 'gs_kgrid_convergence' part of the
        workflow.
        """
        if self._gs_kgrid_convergence_sequencer is not None:
            return self._gs_kgrid_convergence_sequencer
        sequencer = self._gs_kgrid_convergence_sequencer_cls(
                loglevel=self._loglevel)
        self._gs_kgrid_convergence_sequencer = sequencer
        return self.gs_kgrid_convergence_sequencer

    @property
    def gs_smearing_convergence_sequencer(self):
        """The corresponding sequencer for the 'gs_smearing_convergence'
        part of the workflow.
        """
        if self._gs_smearing_convergence_sequencer is not None:
            return self._gs_smearing_convergence_sequencer
        seq = self._gs_smearing_convergence_sequencer_cls(
                loglevel=self._loglevel)
        self._gs_smearing_convergence_sequencer = seq
        return self.gs_smearing_convergence_sequencer

    @property
    def phonon_ecut_convergence_sequencer(self):
        """The corresponding sequencer for the 'phonon_ecut_convergence'
        part of the workflow.
        """
        if self._phonon_ecut_convergence_sequencer is not None:
            return self._phonon_ecut_convergence_sequencer
        self._phonon_ecut_convergence_sequencer = (
                self._phonon_ecut_convergence_sequencer_cls(
                    loglevel=self._loglevel))
        return self.phonon_ecut_convergence_sequencer

    @property
    def phonon_dispersion_sequencer(self):
        """The corresponding sequencer for the 'phonon_dispersion'
        part of the workflow.
        """
        if self._phonon_dispersion_sequencer is not None:
            return self._phonon_dispersion_sequencer
        self._phonon_dispersion_sequencer = (
                self._phonon_dispersion_sequencer_cls(
                    loglevel=self._loglevel))
        return self.phonon_dispersion_sequencer

    @property
    def phonon_dispersion_qgrid_convergence_sequencer(self):
        """The corresponding sequencer for the
        'phonon_dispersion_qgrid_convergence' part of the workflow.
        """
        if self._phonon_dispersion_qgrid_convergence_sequencer is not None:
            return self._phonon_dispersion_qgrid_convergence_sequencer
        self._phonon_dispersion_qgrid_convergence_sequencer = (
                SequencersList([], loglevel=self._loglevel))
        return self.phonon_dispersion_qgrid_convergence_sequencer

    @property
    def phonon_smearing_convergence_sequencer(self):
        """The corresponding sequencer for the 'phonon_smearing_convergence'
        part of the workflow.
        """
        if self._phonon_smearing_convergence_sequencer is not None:
            return self._phonon_smearing_convergence_sequencer
        self._phonon_smearing_convergence_sequencer = (
                self._phonon_smearing_convergence_sequencer_cls(
                    loglevel=self._loglevel))
        return self.phonon_smearing_convergence_sequencer

    @property
    def relaxation_sequencer(self):
        """The corresponding sequencer for the 'relaxation'
        part of the workflow.
        """
        if self._relaxation_sequencer is not None:
            return self._relaxation_sequencer
        self._relaxation_sequencer = self._relaxation_sequencer_cls(
                loglevel=self._loglevel)
        return self.relaxation_sequencer

    @property
    def stop_at_workflow(self):
        """The workflow will stop at this part.
        """
        return self._stop_at_workflow

    @stop_at_workflow.setter
    def stop_at_workflow(self, stop):
        """Sets where the Workflow will stop.
        """
        if stop is None:
            # reset
            self._stop_at_workflow = None
            return
        if stop not in self.workflow:
            raise ValueError(f"'{stop}' not in workflow.")
        # get index number for current stop. if new stop is after previous
        # stop we don't change. otherwise we set new stop
        if self.stop_at_workflow is None:
            # no previous stop set this one
            self._stop_at_workflow = stop
            return
        if self.stop_at_workflow == stop:
            # same
            return
        previous_stop_idx = list(self.workflow).index(self.stop_at_workflow)
        new_stop_idx = list(self.workflow).index(stop)
        if new_stop_idx < previous_stop_idx:
            self._stop_at_workflow = stop

    @property
    def workflow_completed(self):
        """True if all the workflow is finished. False otherwise.
        """
        return self.all_sequencers.sequence_completed

    def clean(self):
        """Cleans all parts of the workflow that are supposed to be computed.
        """
        self._logger.info("Cleaning whole workflow.")
        for workflow in self.workflow:
            if not getattr(self, f"_compute_{workflow}"):
                continue
            self._logger.info(f"Cleaning '{workflow}' part.")
            try:
                getattr(self, "clean_{workflow}")()
            except SequenceNotCompletedError:
                self._logger.info(
                        "'{workflow}' part not completed: abort clean.")
                continue

    def clean_band_structure(self):
        """Cleans the 'band_structure' part of the workflow only.
        """
        self.band_structure_sequencer.clean()

    def clean_gs(self):
        """Cleans the 'gs' part of the workflow only.
        """
        self.gs_sequencer.clean()

    def clean_gs_ecut_convergence(self):
        """Cleans the 'gs_ecut_convergence' part of the workflow only.
        """
        self.gs_ecut_convergence_sequencer.clean()

    def clean_gs_kgrid_convergence(self):
        """Cleans the 'gs_kgrid_convergence' part of the workflow only.
        """
        self.gs_kgrid_convergence_sequencer.clean()

    def clean_gs_smearing_convergence(self):
        """Cleans the 'gs_smearing_convergence' part of the workflow only.
        """
        self.gs_smearing_convergence_sequencer.clean()

    def clean_phonon_ecut_convergence(self):
        """Cleans the 'phonon_ecut_convergence' part of the workflow only.
        """
        self.phonon_ecut_convergence_sequencer.clean()

    def clean_phonon_dispersion(self):
        """Cleans the 'phonon_dispersion' part of the workflow only.
        """
        self.phonon_dispersion_sequencer.clean()

    def clean_phonon_dispersion_qgrid_convergence(self):
        """Cleans the 'phonon_dispersion_qgrid_convergence' part of the workflow only.
        """
        self.phonon_dispersion_qgrid_convergence_sequencer.clean()

    def clean_phonon_smearing_convergence(self):
        """Cleans the 'phonon_smearing_convergence' part of the workflow only.
        """
        self.phonon_smearing_convergence_sequencer.clean()

    def clean_relaxation(self):
        """Cleans the 'relaxation' part of the workflow only.
        """
        self.relaxation_sequencer.clean()

    def get_sequencer(self, workflow_part):
        """Gets the sequencer associated with the given workflow part.

        Parameters
        ----------
        workflow_part: str
            The workflow part for which we want the sequencer.

        Raises
        ------
        ValueError: If the workflow_part is invalid.

        Returns
        -------
        Sequencer object: the sequencer object associated with the part.
        """
        if workflow_part not in self.workflow:
            raise ValueError(f"Invalid workflow part: '{workflow_part}'.")
        return getattr(self, f"{workflow_part}_sequencer")

    # just an alias
    def launch(self, *args, **kwargs):
        """See the `run` method.
        """
        self.run(*args, **kwargs)

    def run(self):
        """Runs the workflow by running all requested sequencers.
        """
        self._do_run_or_write("run")

    def run_band_structure(self):
        """Runs the 'band_structure' part of the workflow only.
        """
        self.band_structure_sequencer.run()

    def run_gs(self):
        """Runs the 'gs' part of the workflow only.
        """
        self.gs_sequencer.run()

    def run_gs_ecut_convergence(self):
        """Runs the 'gs' part of the workflow only.
        """
        self.gs_ecut_convergence_sequencer.run()

    def run_gs_kgrid_convergence(self):
        """Runs the 'gs_kgrid_convergence' part of the workflow only.
        """
        self.gs_kgrid_convergence_sequencer.run()

    def run_gs_smearing_convergence(self):
        """Runs the 'gs_smearing_convergence' part of the workflow only.
        """
        self.gs_smearing_convergence_sequencer.run()

    def run_phonon_ecut_convergence(self):
        """Runs the 'phonon_ecut_convergence' part of the workflow only.
        """
        self.phonon_ecut_convergence_sequencer.run()

    def run_phonon_dispersion(self):
        """Runs the 'phonon_dispersion' part of the workflow only.
        """
        self.phonon_dispersion_sequencer.run()

    def run_phonon_dispersion_qgrid_convergence(self):
        """Runs the 'phonon_dispersion_qgrid_convergence'
        part of the workflow only.
        """
        self.phonon_dispersion_qgrid_convergence_sequencer.run()
        seqs = self.phonon_dispersion_qgrid_convergence_sequencer
        if not seqs.sequence_completed:
            return
        self._post_process_phonon_dispersion_qgrid_convergence()

    def run_phonon_smearing_convergence(self):
        """Runs the 'phonon_smearing_convergence' part of the workflow only.
        """
        self.phonon_smearing_convergence_sequencer.run()

    def run_relaxation(self):
        """Runs the 'relaxation' part of the workflow only.
        """
        self.relaxation_sequencer.run()

    def set_band_structure(
            self, root_workdir=None, scf_input_variables=None,
            scf_calculation_parameters=None,
            band_structure_input_variables=None,
            band_structure_calculation_parameters=None,
            band_structure_kpoint_path=None,
            band_structure_kpoint_path_density=None,
            use_gs_converged_ecut=False,
            use_gs_converged_kgrid=False,
            use_gs=False,
            use_phonon_converged_ecut=False,
            use_relaxed_geometry=False,
            plot_calculation_parameters=None):
        """Sets the BandStructureSequencer

        Parameters
        ----------
        root_workdir: str
            The root workdir for the band structure sequence.
        scf_input_variables: dict, optional
            The scf input variables.
        scf_calculation_parameters: dict, optional
            The scf calculation parameters.
        band_structure_input_variables: dict
            The dict of the band structure input variables.
        band_structure_calculation_parameters: dict
            The dict of the calculation parameters for the band structure
            calculation.
        band_structure_kpoint_path: list
            The list of kpoints that forms the band structure.
        band_structure_kpoint_path_density: int
            The number of points between kpoints in the list of
            the kpoints that forms the band structure.
        use_gs: bool, optional
            If True, the 'gs' part of the workflow is used instead of
            running a new scf calculation. This option is incompatible
            with the other 'use_*' flags and will thus override them.
        use_gs_converged_ecut: bool, optional
            If True, will use the converged ecut from the convergence study.
            'gs_ecut_convergence' must be set to True at init.
        use_gs_converged_kgrid: bool, optional
            If True, will use the converged kgrid from the convergence study.
            'gs_kgrid_convergence' must be set to True at init.
        use_phonon_converged_ecut: bool, optional
            If True, will use the converged ecut from the convergence study.
            'phonon_ecut_convergence' must be set to True at init.
        use_relaxed_geometry: bool, optional
            If True, will use the relaxed geometry as computed by the
            relaxation run. 'relaxation' must be set to True at init.
        plot_calculation_parameters: dict, optional
            The dict of plot parameters.
        """
        if not self._compute_band_structure:
            return
        sequencer = self.band_structure_sequencer
        if root_workdir is None:
            raise ValueError("Need to set 'root_workdir'.")
        # SCF PART
        self._use_converged_calculations(
                sequencer, use_gs=use_gs,
                use_gs_converged_ecut=use_gs_converged_ecut,
                use_gs_converged_kgrid=use_gs_converged_kgrid,
                use_phonon_converged_ecut=use_phonon_converged_ecut,
                use_relaxed_geometry=use_relaxed_geometry,
                scf_input_variables=scf_input_variables,
                scf_calculation_parameters=scf_calculation_parameters,
                scf_workdir=os.path.join(root_workdir, "gs"),
                )
        # BAND STRUCTURE PART
        sequencer.band_structure_workdir = os.path.join(
                root_workdir, "band_structure")
        if band_structure_kpoint_path is None:
            raise ValueError("Need to set 'band_structure_kpoint_path'.")
        sequencer.band_structure_kpoint_path = band_structure_kpoint_path
        if band_structure_kpoint_path_density is None:
            raise ValueError(
                    "Need to set 'band_structure_kpoint_path_density'.")
        bskpd = band_structure_kpoint_path_density
        sequencer.band_structure_kpoint_path_density = bskpd
        if band_structure_input_variables is None:
            raise ValueError("Need to set 'band_structure_input_variables'.")
        self._use_converged_quantities(
                band_structure_input_variables,
                use_gs_converged_ecut=use_gs_converged_ecut,
                use_gs_converged_kgrid=use_gs_converged_kgrid,
                use_phonon_converged_ecut=use_phonon_converged_ecut,
                use_relaxed_geometry=use_relaxed_geometry,
                )
        sequencer.band_structure_input_variables = (
                band_structure_input_variables.copy())
        self._add_calculation_parameters(
                "band_structure_",
                band_structure_calculation_parameters, sequencer)
        if plot_calculation_parameters is None:
            plot_calculation_parameters = {}
        plot_calculation_parameters["title"] = "Band Structure"
        plot_calculation_parameters["save"] = os.path.join(
                root_workdir, "results", "band_structure.pdf")
        plot_calculation_parameters["save_pickle"] = os.path.join(
                root_workdir, "results", "band_structure.pickle")
        self._add_calculation_parameters(
                "plot_", plot_calculation_parameters, sequencer)

    def set_gs(
            self, root_workdir=None, scf_input_variables=None,
            scf_calculation_parameters=None,
            use_gs_converged_ecut=False,
            use_gs_converged_kgrid=False,
            use_gs_converged_smearing=False,
            use_phonon_converged_ecut=False,
            use_phonon_converged_smearing=False,
            use_relaxed_geometry=False,
            **kwargs):
        """Sets the gs SCF sequencer.

        Parameters
        ----------
        root_workdir: str
            The root workdir where the calculation will be ran.
        scf_input_variables: dict
            The dict of input variables.
        scf_calculation_parameters: dict
            The dict of calculation parameters.
        use_gs_converged_ecut: bool, optional
            If True, will use the converged ecut from the convergence study.
            'gs_ecut_convergence' must be set to True at init.
        use_gs_converged_kgrid: bool, optional
            If True, will use the converged kgrid from the convergence study.
            'gs_kgrid_convergence' must be set to True at init.
        use_gs_converged_smearing: bool, optional
            If True, will use the converged smearing
            from the convergence study.
            'gs_smearing_convergence' must be set to True at init.
        use_phonon_converged_ecut: bool, optional
            If True, will use the converged ecut from the convergence study.
            'phonon_ecut_convergence' must be set to True at init.
        use_phonon_converged_smearing: bool, optional
            If True, will use the converged smaring+kgrid from the
            corresponding convergence study.
            'phonon_smearing_convergence' must be set to True at init.
        use_relaxed_geometry: bool, optional
            If True, will use the relaxed geometry as computed by the
            relaxation run. 'relaxation' must be set to True at init.

        Note
        ----
        other kwargs are passed to the '_use_converged_quantities' method.
        """
        if not self._compute_gs:
            return
        if root_workdir is None:
            raise ValueError("Need to set 'root_workdir'.")
        self.gs_sequencer.scf_workdir = root_workdir
        if scf_calculation_parameters is None:
            raise ValueError("Need to set 'scf_calculation_parameters'.")
        self._add_calculation_parameters(
                "scf_", scf_calculation_parameters, self.gs_sequencer)
        if scf_input_variables is None:
            raise ValueError("Need to set 'scf_input_variables'.")
        self._use_converged_quantities(
                scf_input_variables,
                use_gs_converged_ecut=use_gs_converged_ecut,
                use_relaxed_geometry=use_relaxed_geometry,
                use_gs_converged_kgrid=use_gs_converged_kgrid,
                use_gs_converged_smearing=use_gs_converged_smearing,
                use_phonon_converged_ecut=use_phonon_converged_ecut,
                use_phonon_converged_smearing=use_phonon_converged_smearing,
                **kwargs)
        self.gs_sequencer.scf_input_variables = scf_input_variables

    def set_gs_ecut_convergence(
            self,
            bypass_sequence_comparison=False,
            root_workdir=None, scf_input_variables=None,
            scf_calculation_parameters=None,
            scf_convergence_criterion=None,
            scf_ecuts=None,
            plot_calculation_parameters=None,
            _sequencer=None,
            ):
        """Sets the gs_ecut_convergence workflow.

        Parameters
        ----------
        bypass_sequence_comparison: bool, optional
            If True, the workflow bypasses the check on sequences. That means
            if the calculations are different we don't care.
        root_workdir: str
            The workdir where the ecut convergence will be rooted.
        scf_input_variables: dict
            The dictionary of input variables.
        scf_calculation_parameters
            The ecut convergence calculation parameters.
        scf_convergence_criterion: float
            The convergence criterion to reach in meV/atom.
        scf_ecuts: list-like
            The list of ecuts to use for the convergence test.
        plot_calculation_parameters: dict, optional
            The dict of plot parameters.
        """
        if not self._compute_gs_ecut_convergence:
            return
        # the _sequencer variable lets choose the sequencer to utilize
        # by default take the default one
        # useful when doing mutltiple ecut convergences for different cutoff
        # variables
        if _sequencer is None:
            sequencer = self.gs_ecut_convergence_sequencer
        else:
            sequencer = _sequencer
        if root_workdir is None:
            raise ValueError("Need to set 'root_workdir'.")
        sequencer.scf_workdir = os.path.join(root_workdir, "scf_runs")
        if scf_input_variables is None:
            raise ValueError("Need to set  'scf_input_variables'.")
        sequencer.scf_input_variables = scf_input_variables
        if scf_calculation_parameters is None:
            raise ValueError("Need to set 'scf_calculation_parameters'.")
        self._add_calculation_parameters(
                "scf_", scf_calculation_parameters, sequencer)
        sequencer.scf_ecuts = scf_ecuts
        if scf_convergence_criterion is None:
            raise ValueError("Need to set 'scf_convergence_criterion'.")
        sequencer.scf_convergence_criterion = scf_convergence_criterion
        if plot_calculation_parameters is None:
            plot_calculation_parameters = {}
        varname = sequencer.ecuts_input_variable_name
        plot_calculation_parameters["save"] = os.path.join(
                root_workdir, "results",
                f"gs_{varname}_convergence.pdf")
        plot_calculation_parameters["save_pickle"] = os.path.join(
                root_workdir, "results",
                f"gs_{varname}_convergence.pickle")
        self._add_calculation_parameters(
                "plot_", plot_calculation_parameters, sequencer)
        sequencer.bypass_sequence_comparison = bypass_sequence_comparison

    def set_gs_kgrid_convergence(
            self,
            root_workdir=None,
            scf_input_variables=None,
            scf_calculation_parameters=None,
            scf_convergence_criterion=None,
            scf_kgrids=None,
            plot_calculation_parameters=None,
            use_gs_converged_ecut=False,
            use_geometry_from=None,
            ):
        """Sets the gs_kgrid_convergence workflow.

        Parameters
        ----------
        root_workdir: str
            The root workdir where the kgrid convergence will be rooted.
        scf_input_variables: dict
            The dict of input variables.
        scf_calculation_parameters: dict
            The dict of calculation parameters.
        scf_convergence_criterion: float
            The convergence criterion to reach in meV / atom.
        scf_kgrids: list-like
            The list of kgrids to test.
        use_gs_converged_ecut: bool, optional
            If True, the ecut used will be the converged one.
            'gs_ecut_convergence' must be set to True or
            the 'converged_gs_ecut' attribute must be set.
        use_geometry_from: str, optional
            If not None, gives the workflow part whose scf part contains
            the geometry we want to use for the scf part of the
            current workflow part. E.g.: if we want to use
            the 'gs_ecut_convergence' geometry, just set this argument
            to it.
        plot_calculation_parameters: dict, optional
            The dict of plot parameters.
        """
        if not self._compute_gs_kgrid_convergence:
            return
        sequencer = self.gs_kgrid_convergence_sequencer
        if root_workdir is None:
            raise ValueError("Need to set 'root_workdir'.")
        sequencer.scf_workdir = os.path.join(root_workdir, "scf_runs")
        if scf_input_variables is None:
            raise ValueError("Need to set 'scf_input_variables'.")
        self._use_converged_quantities(
                scf_input_variables, sequencer=sequencer,
                use_gs_converged_ecut=use_gs_converged_ecut,
                use_geometry_from=use_geometry_from)
        sequencer.scf_input_variables = scf_input_variables
        if scf_kgrids is None:
            raise ValueError("Need to set 'scf_kgrids'.")
        sequencer.scf_kgrids = scf_kgrids
        if scf_convergence_criterion is None:
            raise ValueError("Need to set 'scf_convergence_criterion'.")
        sequencer.scf_convergence_criterion = scf_convergence_criterion
        self._add_calculation_parameters(
                "scf_", scf_calculation_parameters, sequencer)
        if plot_calculation_parameters is None:
            plot_calculation_parameters = {}
        plot_calculation_parameters["save"] = os.path.join(
                root_workdir, "results", "gs_kgrid_convergence.pdf")
        plot_calculation_parameters["save_pickle"] = os.path.join(
                root_workdir, "results", "gs_kgrid_convergence.pickle")
        self._add_calculation_parameters(
                "plot_", plot_calculation_parameters, sequencer)

    def set_gs_smearing_convergence(
            self,
            root_workdir=None,
            scf_input_variables=None,
            scf_convergence_criterion=None,
            scf_kgrids=None,
            scf_smearings=None,
            scf_calculation_parameters=None,
            plot_calculation_parameters=None,
            use_gs_converged_ecut=False,
            use_geometry_from=None,
            ):
        """Sets the gs_smearing_convergence part of the workflow.

        Parameters
        ----------
        root_workdir: str
            The path where the convergence will be rooted.
        scf_input_variables: dict, optional
            The dict of main input variables.
        scf_convergence_criterion: float
            The convergence criterion (in meV/at).
        scf_kgrids: list
            The list of kgrids to test.
        scf_smearings: list
            The list of smearing parameters to test.
        scf_calculation_parameters: dict
            The dict of calculation parameters.
        plot_calculation_parameters: dict, optional
            The dict of plot parameters.
        use_gs_converged_ecut: bool, optional
            If True, the ecut used will be the converged one.
            'gs_ecut_convergence' must be set to True upon init or
            the 'converged_gs_ecut' attribute must be set.
        use_geometry_from: str, optional
            If not None, gives the workflow part whose scf part contains
            the geometry we want to use for the scf part of the
            current workflow part. E.g.: if we want to use
            the 'gs_ecut_convergence' geometry, just set this argument
            to it.
        """
        if not self._compute_gs_smearing_convergence:
            return
        sequencer = self.gs_smearing_convergence_sequencer
        if root_workdir is None:
            raise ValueError("Need to set 'root_workdir'.")
        sequencer.scf_workdir = root_workdir
        if scf_input_variables is None:
            raise ValueError("Need to set 'scf_input_variables'.")
        self._use_converged_quantities(
                scf_input_variables, sequencer=sequencer,
                use_geometry_from=use_geometry_from,
                use_gs_converged_ecut=use_gs_converged_ecut)
        sequencer.scf_input_variables = scf_input_variables
        if scf_convergence_criterion is None:
            raise ValueError("Need to set 'scf_convergence_criterion'.")
        sequencer.scf_convergence_criterion = scf_convergence_criterion
        if scf_kgrids is None:
            raise ValueError("Need to set 'scf_kgrids'.")
        sequencer.scf_kgrids = scf_kgrids
        if scf_smearings is None:
            raise ValueError("Need to set 'scf_smearings'.")
        sequencer.scf_smearings = scf_smearings
        self._add_calculation_parameters(
                "scf_", scf_calculation_parameters, sequencer)
        if plot_calculation_parameters is None:
            plot_calculation_parameters = {}
        plot_calculation_parameters["save"] = os.path.join(
                root_workdir, "results", "gs_smearing_convergence.pdf")
        plot_calculation_parameters["save_pickle"] = os.path.join(
                root_workdir, "results", "gs_smearing_convergence.pickle")
        self._add_calculation_parameters(
                "plot_", plot_calculation_parameters, sequencer)

    def set_phonon_ecut_convergence(
            self, phonons_input_variables=None,
            phonons_calculation_parameters=None,
            phonons_convergence_criterion=None,
            phonons_qpt=None,
            plot_calculation_parameters=None,
            root_workdir=None,
            scf_input_variables=None, scf_ecuts=None,
            scf_calculation_parameters=None,
            use_gs_converged_kgrid=False,
            use_gs_converged_smearing=False,
            use_relaxed_geometry=False,
            _sequencer=None):
        """Sets the phonon ecut convergence part of the workflow.

        Parameters
        ----------
        phonons_calculation_parameters: dict
            The dict of phonons calculations parameters.
        phonons_convergence_criterion: float
            The phonons convergence criterion.
        phonons_input_variables: dict. optional
            The dict of phonons input variables.
        phonons_qpt: list
            The phonon qpt to study convergence against.
        plot_calculation_parameters: dict, optional
            The dict of plot parameters.
        root_workdir: str
            The root workdir for the scf and phonon calculations.
        scf_ecuts: list
            The list of ecuts to test.
        scf_input_variables: dict
            The dict of the scf calculation variables.
        scf_calculation_parameters: dict
            The dict of calculation parameters.
        use_gs_converged_kgrid: bool, optional
            If True, the gs converged kgrid variables are added to the
            scf_input_variables. 'gs_kgrid_convergence' must be set to True or
            the 'converged_gs_kgrid' attribute must be set.
        use_gs_converged_smearing: bool, optional
            If True, the gs converged kgrid+smearing variables are added to the
            scf_input_variables. 'gs_smearing_convergence'
            must be set to True or
            the 'converged_gs_smearing' attribute must be set.
        use_relaxed_geometry: bool, optional
            If True, the relaxed geometry will be used.
        """
        if not self._compute_phonon_ecut_convergence:
            return
        # the _sequencer variable lets choose the sequencer to utilize
        # by default take the default one
        # useful when doing mutltiple ecut convergences for different cutoff
        # variables
        if _sequencer is None:
            seq = self.phonon_ecut_convergence_sequencer
        else:
            seq = _sequencer
        if root_workdir is None:
            raise ValueError("Need to set 'root_workdir'.")
        seq.scf_workdir = os.path.join(root_workdir, "scf_runs")
        seq.phonons_workdir = os.path.join(root_workdir, "phonons_runs")
        if scf_input_variables is None:
            raise ValueError("Need to set 'scf_input_variables'.")
        self._use_converged_quantities(
                scf_input_variables,
                use_gs_converged_kgrid=use_gs_converged_kgrid,
                use_gs_converged_smearing=use_gs_converged_smearing,
                use_relaxed_geometry=use_relaxed_geometry)
        seq.scf_input_variables = scf_input_variables
        if scf_ecuts is None:
            raise ValueError("Need to set 'scf_ecuts'.")
        seq.scf_ecuts = scf_ecuts
        self._add_calculation_parameters(
                "scf_", scf_calculation_parameters, seq)
        if phonons_input_variables is None:
            phonons_input_variables = {}
        seq.phonons_input_variables = phonons_input_variables
        if phonons_qpt is None:
            raise ValueError("Need to set 'phonons_qpt'.")
        seq.phonons_qpt = phonons_qpt
        if phonons_convergence_criterion is None:
            raise ValueError("Need to set 'phonons_convergence_criterion'.")
        seq.phonons_convergence_criterion = phonons_convergence_criterion
        self._add_calculation_parameters(
                "phonons_", phonons_calculation_parameters, seq)
        if plot_calculation_parameters is None:
            plot_calculation_parameters = {}
        plot_calculation_parameters["save"] = os.path.join(
                root_workdir, "results",
                f"phonon_{seq.ecuts_input_variable_name}_convergence.pdf")
        plot_calculation_parameters["save_pickle"] = os.path.join(
                root_workdir, "results",
                f"phonon_{seq.ecuts_input_variable_name}_convergence.pickle")
        self._add_calculation_parameters(
                "plot_", plot_calculation_parameters, seq)

    def set_phonon_dispersion(
            self, root_workdir=None,
            scf_calculation_parameters=None,
            scf_input_variables=None,
            phonons_calculation_parameters=None,
            phonons_input_variables=None,
            phonons_qpoint_grid=None,
            phonons_qpoints_split_by_irreps=None,
            qpoint_path=None,
            qpoint_path_density=None,
            plot_calculation_parameters=None,
            use_gs=False,
            use_phonon_converged_ecut=False,
            use_phonon_converged_smearing=False,
            use_relaxed_geometry=False,
            ):
        """Sets the phonon dispersion part of the workflow.

        Parameters
        ----------
        root_workdir: str
            The root workdir where the calculations will run.
        scf_calculation_parameters: dict, optional
            The dict of scf calculation parameters.
        scf_input_variables: dict, optional
            The dict of scf input variables.
        phonons_calculation_parameters: dict
            The dict of phonons calculation parameters.
        phonons_input_variables: dict
            The dict of phonons input variables.
        phonons_qpoint_grid: list
            The qpoint grid with which the phonon dispersion will be computed.
        phonons_qpoints_split_by_irreps: list, optional
            A list of qpts number (startin' at 1) that we want to split by
            irreducible perturbations.
            Useful for qpts that are very hard to compute
            or for very large system with a lot of atoms.
        plot_calculation_parameters: dict, optional
            The dict of plot parameters.
        qpoint_path: list
            The list of qpoints that forms the phonon dispersion.
        qpoint_path_density: int
            The number of qpoints between the points given in the qpoint path.
        use_phonon_converged_ecut: bool, optional
            If True, the phonon converged ecut will be used. The
            'phonon_ecut_convergence' part of the workflow must be activated
            or the 'converged_phonon_ecut' must be set upon init.
        use_phonon_converged_smearing: bool, optional
            If True, the phonon converged smearing will be used. The
            'phonon_smearing_convergence' part of the workflow must be
            activated or the 'converged_phonon_smearing' must be set upon init.
        use_gs: bool, optional
            If True, the 'gs' part of the workflow is used. This option is
            incompatible with 'use_*' flags and will thus override them.
        use_relaxed_geometry: bool, optional
            If True, the relaxed geometry will be used. The 'relaxation' part
            of the workflow must be activated or the 'relaxed_geometry'
            attribute must be set upon init.
        """
        if not self._compute_phonon_dispersion:
            return
        sequencer = self.phonon_dispersion_sequencer
        if root_workdir is None:
            raise ValueError("The 'root_workdir' must be set.")
        # SCF
        self._use_converged_calculations(
                sequencer, use_gs=use_gs,
                scf_input_variables=scf_input_variables,
                scf_calculation_parameters=scf_calculation_parameters,
                scf_workdir=os.path.join(root_workdir, "scf_run"),
                use_phonon_converged_ecut=use_phonon_converged_ecut,
                use_phonon_converged_smearing=use_phonon_converged_smearing,
                use_relaxed_geometry=use_relaxed_geometry)
        # PHONONS
        sequencer.phonons_workdir = os.path.join(
                root_workdir, "phonons_runs", "ph_q")
        if phonons_qpoint_grid is None:
            raise ValueError("Need to set 'phonons_qpoint_grid'.")
        sequencer.phonons_qpoint_grid = phonons_qpoint_grid
        if phonons_input_variables is None:
            raise ValueError("Need to set 'phonons_input_variables'.")
        sequencer.phonons_input_variables = phonons_input_variables
        if phonons_qpoints_split_by_irreps is not None:
            sequencer.phonons_qpoints_split_by_irreps = (
                    phonons_qpoints_split_by_irreps)
        self._add_calculation_parameters(
                "phonons_", phonons_calculation_parameters, sequencer)
        # PHONON DISPERSION PART
        if qpoint_path is None:
            raise ValueError("Need to set 'qpoint_path'.")
        sequencer.qpoint_path = qpoint_path
        if qpoint_path_density is None:
            raise ValueError("Need to set 'qpoint_path_density'.")
        sequencer.qpoint_path_density = qpoint_path_density
        # PLOT
        if plot_calculation_parameters is None:
            plot_calculation_parameters = {}
        plot_calculation_parameters["save"] = os.path.join(
                root_workdir, "results", "phonon_dispersion.pdf")
        plot_calculation_parameters["save_pickle"] = os.path.join(
                root_workdir, "results", "phonon_dispersion.pickle")
        self._add_calculation_parameters(
                "plot_", plot_calculation_parameters, sequencer)

    def set_phonon_dispersion_qgrid_convergence(
            self, root_workdir=None,
            scf_calculation_parameters=None,
            scf_input_variables=None,
            phonons_calculation_parameters=None,
            phonons_input_variables=None,
            phonons_qpoint_grids=None,
            qpoint_path=None,
            qpoint_path_density=None,
            plot_calculation_parameters=None,
            use_gs=False,
            use_phonon_converged_ecut=False,
            use_phonon_converged_smearing=False,
            use_relaxed_geometry=False,
            ):
        """Sets the phonon dispersion qpoint grid convergence part of the
        workflow. Launches a bunch of phonon dispersion sequencers
        to test out many qpoint grids.

        Parameters
        ----------
        root_workdir: str
            The root workdir where the calculations will run.
        scf_calculation_parameters: dict, optional
            The dict of scf calculation parameters.
        scf_input_variables: dict, optional
            The dict of scf input variables.
        phonons_calculation_parameters: dict
            The dict of phonons calculation parameters.
        phonons_input_variables: dict
            The dict of phonons input variables.
        phonons_qpoint_grids: list
            List of qpoint grids to test.
        qpoint_path: list
            The list of qpoints that forms the phonon dispersion.
        qpoint_path_density: int
            The number of qpoints between the points given in the qpoint path.
        plot_calculation_parameters: dict, optional
            The dict of plot parameters.
        use_phonon_converged_ecut: bool, optional
            If True, the phonon converged ecut will be used. The
            'phonon_ecut_convergence' part of the workflow must be activated
            or the 'converged_phonon_ecut' must be set upon init.
        use_phonon_converged_smearing: bool, optional
            If True, the phonon converged smearing will be used. The
            'phonon_smearing_convergence' part of the workflow must be
            activated or the 'converged_phonon_smearing' must be set upon init.
        use_gs: bool, optional
            If True, the 'gs' part of the workflow is used. This option is
            incompatible with 'use_*' flags and will thus override them.
        use_relaxed_geometry: bool, optional
            If True, the relaxed geometry will be used. The 'relaxation' part
            of the workflow must be activated or the 'relaxed_geometry'
            attribute must be set upon init.
        """
        if not self._compute_phonon_dispersion_qgrid_convergence:
            return
        # need to add Sequencers here
        if phonons_qpoint_grids is None:
            raise ValueError("Need to set 'phonons_qpoint_grids'.")
        if not is_list_like(phonons_qpoint_grids):
            raise ValueError(
                    "'phonons_qpoint_grids' must be a list of qpoint grids.")
        if phonons_input_variables is None:
            raise ValueError("Need to set 'phonons_input_variables'.")
        if qpoint_path is None:
            raise ValueError("Need to set 'qpoint_path'.")
        if qpoint_path_density is None:
            raise ValueError("Need to set 'qpoint_path_density'.")
        for phonons_qpoint_grid in phonons_qpoint_grids:
            sequencer = self._phonon_dispersion_sequencer_cls(
                    loglevel=self._loglevel)
            if not is_vector(phonons_qpoint_grid, length=3):
                raise ValueError(
                        "Phonons qpoint grid must be a list of len = 3.")
            # SCF
            subroot_workdir = os.path.join(
                    root_workdir, "qgrid_" + "_".join(
                        [str(x) for x in phonons_qpoint_grid]))
            self._use_converged_calculations(
                    sequencer, use_gs=use_gs,
                    scf_input_variables=scf_input_variables,
                    scf_calculation_parameters=scf_calculation_parameters,
                    scf_workdir=os.path.join(subroot_workdir, "scf_run"),
                    use_phonon_converged_ecut=use_phonon_converged_ecut,
                    use_phonon_converged_smearing=(
                        use_phonon_converged_smearing),
                    use_relaxed_geometry=use_relaxed_geometry)
            # PHONONS
            sequencer.phonons_workdir = os.path.join(
                    subroot_workdir, "phonons_runs", "ph_q")
            sequencer.phonons_qpoint_grid = phonons_qpoint_grid
            # copy very important here since setting input vars will modify
            # the dict (internal sequencers setting methods)"
            sequencer.phonons_input_variables = phonons_input_variables.copy()
            self._add_calculation_parameters(
                    "phonons_", phonons_calculation_parameters, sequencer)
            # PHONON DISPERSION
            sequencer.qpoint_path = qpoint_path
            sequencer.qpoint_path_density = qpoint_path_density
            # PLOT
            if plot_calculation_parameters is None:
                plot_calculation_parameters = {}
            plot_calculation_parameters["save"] = os.path.join(
                    subroot_workdir, "results", "phonon_dispersion.pdf")
            plot_calculation_parameters["save_pickle"] = os.path.join(
                    subroot_workdir, "results", "phonon_dispersion.pickle")
            self._add_calculation_parameters(
                    "plot_", plot_calculation_parameters, sequencer)
            self.phonon_dispersion_qgrid_convergence_sequencer.append(
                    sequencer)

    def set_phonon_smearing_convergence(
            self, phonons_input_variables=None,
            phonons_calculation_parameters=None,
            phonons_convergence_criterion=None,
            phonons_qpt=None,
            plot_calculation_parameters=None,
            root_workdir=None,
            scf_input_variables=None,
            scf_calculation_parameters=None,
            scf_smearings=None,
            scf_kgrids=None,
            use_gs_converged_smearing=False,
            use_phonon_converged_ecut=False,
            use_relaxed_geometry=False):
        """Sets the phonon smearing convergence part of the workflow.

        Parameters
        ----------
        phonons_calculation_parameters: dict
            The dict of phonons calculations parameters.
        phonons_convergence_criterion: float
            The phonons convergence criterion.
        phonons_input_variables: dict. optional
            The dict of phonons input variables.
        phonons_qpt: list
            The phonon qpt to study convergence against.
        plot_calculation_parameters: dict, optional
            The dict of plot parameters.
        root_workdir: str
            The root workdir for the scf and phonon calculations.
        scf_calculation_parameters: dict
            The dict of calculation parameters.
        scf_input_variables: dict
            The dict of the scf calculation variables.
        scf_kgrids: list
            The list of kgrids to test.
        scf_smearings: list
            The list of smearings to test.
        use_phonon_converged_ecut: bool, optional
            If True, the phonon converged ecut variables are added to the
            scf_input_variables. 'phonon_ecut_convergence'
            must be set to True or
            the 'converged_phonon_ecut' attribute must be set.
        use_relaxed_geometry: bool, optional
            If True, the relaxed geometry will be used.
        """
        if not self._compute_phonon_smearing_convergence:
            return
        seq = self.phonon_smearing_convergence_sequencer
        if root_workdir is None:
            raise ValueError("Need to set 'root_workdir'.")
        seq.scf_workdir = os.path.join(root_workdir, "scf_runs")
        seq.phonons_workdir = os.path.join(root_workdir, "phonons_runs")
        if scf_input_variables is None:
            raise ValueError("Need to set 'scf_input_variables'.")
        try:
            self._use_converged_quantities(
                    scf_input_variables,
                    use_phonon_converged_ecut=use_phonon_converged_ecut,
                    use_relaxed_geometry=use_relaxed_geometry)
        except SequenceNotConvergedError as e:
            self._logger.exception(e)
            self._logger.error("Convergence not achieved: abort set.")
            if use_relaxed_geometry:
                self.stop_at_workflow = "relaxation"
            if use_phonon_converged_ecut:
                # need to reset if the one above was set
                self.stop_at_workflow = None
                self.stop_at_workflow = "phonon_ecut_convergence"
            return
        seq.scf_input_variables = scf_input_variables
        if scf_kgrids is None:
            raise ValueError("Need to set 'scf_kgrids'.")
        seq.scf_kgrids = scf_kgrids
        if scf_smearings is None:
            raise ValueError("Need to set 'scf_smearings'.")
        seq.scf_smearings = scf_smearings
        self._add_calculation_parameters(
                "scf_", scf_calculation_parameters, seq)
        if phonons_input_variables is None:
            phonons_input_variables = {}
        seq.phonons_input_variables = phonons_input_variables
        if phonons_qpt is None:
            raise ValueError("Need to set 'phonons_qpt'.")
        seq.phonons_qpt = phonons_qpt
        if phonons_convergence_criterion is None:
            raise ValueError("Need to set 'phonons_convergence_criterion'.")
        seq.phonons_convergence_criterion = phonons_convergence_criterion
        self._add_calculation_parameters(
                "phonons_", phonons_calculation_parameters, seq)
        if plot_calculation_parameters is None:
            plot_calculation_parameters = {}
        plot_calculation_parameters["save"] = os.path.join(
                root_workdir, "results", "phonon_smearing_convergence.pdf")
        plot_calculation_parameters["save_pickle"] = os.path.join(
                root_workdir, "results", "phonon_smearing_convergence.pickle")
        self._add_calculation_parameters(
                "plot_", plot_calculation_parameters, seq)

    def set_relaxation(
            self,
            bypass_sequence_comparison=False,
            root_workdir=None, scf_input_variables=None,
            scf_calculation_parameters=None,
            relax_atoms=None,
            relax_cell=None,
            **kwargs):
        """Sets the relaxation sequencer.

        Parameters
        ----------
        bypass_sequence_comparison: bool, optional
            If True, the workflow bypasses the check on sequences. That means
            if the calculations are different we don't care.
        relax_atoms: bool
            If True, the atomic positions will be relaxed prior to the
            cell optimization (if relax_cell is True). Otherwise,
            only the atoms relaxation is done.
        relax_cell: bool
            If True, the cell optimization is done after a first
            atomic relaxation (if relax_atoms is True). Otherwise,
            only the cell optimization is done.
        root_workdir: str
            The root directory where the relaxation run(s) will be executed.
        scf_input_variables: dict
            The input variables dictionary.
        scf_calculation_parameters: dict
            The dict of calculation parameters.

        Note
        ----
        other kwargs are passed to the '_use_converged_quantities' method.
        """
        sequencer = self.relaxation_sequencer
        if not self._compute_relaxation:
            return
        if root_workdir is None:
            raise ValueError("Need to set 'root_workdir'.")
        sequencer.scf_workdir = root_workdir
        if relax_atoms is None:
            raise ValueError("Need to set 'relax_atoms'.")
        sequencer.relax_atoms = relax_atoms
        if relax_cell is None:
            raise ValueError("Need to set 'relax_cell'.")
        sequencer.relax_cell = relax_cell
        if scf_input_variables is None:
            raise ValueError("Need to set 'scf_input_variables'.")
        self._use_converged_quantities(
                scf_input_variables, sequencer=sequencer, **kwargs)
        sequencer.scf_input_variables = scf_input_variables
        if scf_calculation_parameters is None:
            raise ValueError("Need to set 'scf_calculation_parameters'.")
        self._add_calculation_parameters(
                "scf_", scf_calculation_parameters, sequencer)
        sequencer.bypass_sequence_comparison = bypass_sequence_comparison

    def workflow_part_completed(self, workflow_part):
        """Checks that the given workflow part has completed.

        If not, the workflow will be stopped at this part. If
        the part is not computed, an error is raised.

        Parameters
        ----------
        workflow_part: str
            The part of the workflow in question.

        Raises
        ------
        ValueError: If the workflow part is invalid.
        RuntimeError: If the workflow part is not computed.

        Returns
        -------
        bool: True if the part is completed. False otherwise.
        """
        if workflow_part not in self.workflow:
            raise ValueError(f"Invalid workflow: '{workflow_part}'.")
        if not self.workflow_part_computed(workflow_part):
            raise RuntimeError(f"Not computed workflow: '{workflow_part}'.")
        if not self.get_sequencer(workflow_part).sequence_completed:
            self.stop_at_workflow = workflow_part
            return False
        return True

    def workflow_part_computed(self, workflow_part):
        """Checks if workflow part is computed.

        Parameters
        ----------
        workflow_part: str
            The part of the workflow in question.

        Raises
        ------
        ValueError: if the workflow part is invalid.

        Returns
        -------
        bool: True if the part is computed. False otherwise.
        """
        if workflow_part not in self.workflow:
            raise ValueError(f"Invalid workflow: '{workflow_part}'.")
        return getattr(self, f"_compute_{workflow_part}")

    def write(self):
        """Only writes the workflow files as far as it can go.
        """
        self._do_run_or_write("write")

    def write_band_structure(self):
        """Writes the 'band_structure' part of the workflow only.
        """
        self.band_structure_sequencer.write()

    def write_gs(self):
        """Writes the 'gs' part of the workflow only.
        """
        self.gs_sequencer.write()

    def write_gs_ecut_convergence(self):
        """Writes the 'gs_ecut_convergence' part of the workflow only.
        """
        self.gs_ecut_convergence_sequencer.write()

    def write_gs_kgrid_convergence(self):
        """Writes the 'gs_kgrid_convergence' part of the workflow only.
        """
        self.gs_kgrid_convergence_sequencer.write()

    def write_gs_smearing_convergence(self):
        """Writes the 'gs_smearing_convergence' part of the workflow only.
        """
        self.gs_smearing_convergence_sequencer.write()

    def write_phonon_ecut_convergence(self):
        """Writes the 'phonon_ecut_convergence' part of the workflow only.
        """
        self.phonon_ecut_convergence_sequencer.write()

    def write_phonon_dispersion(self):
        """Writes the 'phonon_dispersion' part of the workflow only.
        """
        self.phonon_dispersion_sequencer.write()

    def write_phonon_dispersion_qgrid_convergence(self):
        """Writes the 'phonon_dispersion_qgrid_convergence' part of the workflow only.
        """
        self.phonon_dispersion_qgrid_convergence_sequencer.write()

    def write_phonon_smearing_convergence(self):
        """Writes the 'phonon_smearing_convergence' part of the workflow only.
        """
        self.phonon_smearing_convergence_sequencer.write()

    def write_relaxation(self):
        """Writes the 'write_relaxation' part of the workflow only.
        """
        self.relaxation_sequencer.write()

    @staticmethod
    def _add_calculation_parameters(prefix, parameters, sequencer):
        """Adds the calculation parameters from a given prefix to the
        given sequencer.

        Parameters
        ----------
        prefix: str
            The calculation type prefix. Should end with underscore.
        parameters: dict
            The dictionary of parameters.
        sequencer: Sequencer object
            The Sequencer object on which we want to apply the given
            calculation parameters.
        """
        if not prefix.endswith("_"):
            raise DevError("prefix should end with '_'.")
        if parameters is None:
            raise ValueError(
                    f"Need to set '{prefix}calculation_parameters'.")
        for name, value in parameters.copy().items():
            if not name.startswith(prefix):
                name = prefix + name
            setattr(sequencer, name, value)

    def _check_workflow(self):
        """Checks that the workflow object has all required functions and
        attributes defined.
        """
        for workflow in self.workflow:
            compute_attr = f"_compute_{workflow}"
            if not hasattr(self, compute_attr):
                raise DevError(f"Need to set '{compute_attr}'.")
            seq_attr = f"{workflow}_sequencer"
            if seq_attr not in dir(self):
                raise DevError(f"Need to set '{seq_attr}'.")
            for functype in ("clean", "run", "set", "write"):
                func_attr = f"{functype}_{workflow}"
                if not hasattr(self, func_attr):
                    raise DevError(f"Need to set '{func_attr}'.")
                func = getattr(self, func_attr)
                if not callable(func):
                    raise DevError(f"'{func_attr}' should be callable.")

    def _do_run_or_write(self, run_or_write):
        """Actually run or write the workflow.

        Paramaters
        ----------
        run_or_write: str
            The action to execute.
        """
        if run_or_write not in ("run", "write"):
            raise DevError(run_or_write)
        # self._set_global_sequencers_attributes()
        self._check_workflow()
        for workflow_part in self.workflow:
            # do we compute this workflow?
            if getattr(self, f"_compute_{workflow_part}"):
                if run_or_write == "run":
                    action = "Running"
                else:
                    action = "Writing"
                self._logger.info(
                    f"{action} '{workflow_part}' "
                    "part of the workflow.")
                # actually call the function to run or write workflow_part
                getattr(self, f"{run_or_write}_{workflow_part}")()
            # do we stop at this workflow?
            if self.stop_at_workflow == workflow_part:
                self._logger.info(
                        "Stopping prematuraly because "
                        f"'{self.stop_at_workflow}' has/had to be completed. "
                        "Please rerun workflow script once everything is "
                        "done.")
                return
        if self.workflow_completed:
            self._logger.info(
                    Colors.color_text("Workflow completed!", "green", "bold"))
        else:
            self._logger.info(
                    "Workflow not completed, please rerun workflow script if "
                    "it stopped or "
                    "check if something bad happened.")

    # FG: 2021/04/30 I removed this method in profit for fine tuning these
    # parameters on each sequencers instead of globally
    # def _set_global_sequencers_attributes(self):
    #     """Sets common properties to all sequencers if needed.
    #     """
    #     for attr in ("bypass_convergence_check",
    #                  "bypass_sequence_comparison"):
    #         setattr(self.all_sequencers, attr, getattr(self, attr))

    def _post_process_phonon_dispersion_qgrid_convergence(self):
        # plot final results
        all_plots = []
        seqs = self.phonon_dispersion_qgrid_convergence_sequencer
        colors = rand_cmap(len(seqs))
        for iseq, sequencer in enumerate(seqs):
            plot = Plot.load_plot(sequencer.plot_save_pickle)
            plot.curves[0].label = (
                    r"$N_q = $" + " ".join(
                        [str(x) for x in sequencer.phonons_qpoint_grid]))
            for curve in plot.curves:
                curve.color = colors(iseq / len(seqs))
            all_plots.append(plot)
        final_plot = sum(all_plots[1:], all_plots[0])
        show = sequencer.plot_calculation_parameters.get("show", True)
        final_plot.plot(show=show)
        results_dir = os.path.join(
                os.path.dirname(
                    os.path.dirname(
                        os.path.dirname(
                            sequencer.phonons_workdir))),
                "results")
        final_plot.save(
                os.path.join(
                    results_dir,
                    "phonon_dispersion_qpoint_grid_convergence.pdf"),
                overwrite=True,
                )
        final_plot.save_pickle(
                os.path.join(
                    results_dir,
                    "phonon_dispersion_qpoint_grid_convergence.pickle"),
                overwrite=True)

    def _use_converged_calculations(
            self, sequencer,
            use_gs=False,
            scf_input_variables=None,
            scf_calculation_parameters=None,
            scf_workdir=None,
            use_gs_converged_ecut=False,
            use_gs_converged_kgrid=False,
            use_gs_converged_smearing=False,
            use_phonon_converged_ecut=False,
            use_phonon_converged_smearing=False,
            use_relaxed_geometry=False,
            ):
        """Sets up or not a converged calculation to the given sequencer.

        Parameters
        ----------
        sequencer: Sequencer object
            The sequencer to set up in question.
        use_gs: bool, optional
            If True, the gs part of the workflow will be used as the scf part
            of the sequencer. This parameter will override the other 'use_*'
            kwargs.
        scf_input_variables: dict, optional
            If 'use_gs' is False, this dict is used for the input variables.
        scf_calculation_parameters: dict, optional
            If 'use_gs' is False, this dict is used for the scf calculation
            parameters.
        scf_workdir: str, optional
            If 'use_gs' is False, this path is used as the scf workdir.
        use_gs_converged_ecut: bool, optional
            If True, the gs converged ecut is used.
        use_gs_converged_kgrid: bool, optional
            If True, use the gs converged kgrid.
        use_gs_converged_smearing: bool, optional
            If True, use the gs converged kgrid and smearing.
        use_phonon_converged_ecut: bool, optional
            If True, the phonon converged ecut is used.
        use_phonon_converged_smearing: bool, optional
            If True, the phonon converged smearing is used.
        use_relaxed_geometry: bool, optional
            If True, the relaxed geometry will be added to the input variables.
        """
        if use_gs:
            if any([use_gs_converged_ecut, use_gs_converged_kgrid,
                    use_gs_converged_smearing, use_relaxed_geometry,
                    use_phonon_converged_ecut, use_phonon_converged_smearing]):
                raise ValueError(
                        "'use_gs' cannot be used in combination with the other"
                        " 'use_* flags.")
            if not self.workflow_part_completed("gs"):
                return
            sequencer.scf_workdir = self.gs_sequencer.scf_workdir
            sequencer.scf_input_variables = (
                    self.gs_sequencer.scf_input_variables.copy())
            return
        if scf_workdir is None:
            raise DevError("Need to set 'scf_workdir'.")
        sequencer.scf_workdir = scf_workdir
        if scf_input_variables is None:
            raise ValueError("Need to set the 'scf_input_variables'.")
        self._use_converged_quantities(
                scf_input_variables,
                use_gs_converged_ecut=use_gs_converged_ecut,
                use_gs_converged_kgrid=(
                    use_gs_converged_kgrid),
                use_gs_converged_smearing=use_gs_converged_smearing,
                use_phonon_converged_ecut=use_phonon_converged_ecut,
                use_phonon_converged_smearing=use_phonon_converged_smearing,
                use_relaxed_geometry=use_relaxed_geometry)
        sequencer.scf_input_variables = scf_input_variables
        self._add_calculation_parameters(
                "scf_", scf_calculation_parameters, sequencer)

    # FG: 2021/04/16
    # TODO: this method is wayyyy to bigggg... find a way to make it smaller
    # and (most importantly) DRYer!
    def _use_converged_quantities(
            self, input_variables,
            sequencer=None,
            use_gs_converged_ecut=False,
            use_gs_converged_kgrid=False,
            use_gs_converged_smearing=False,
            use_phonon_converged_ecut=False,
            use_phonon_converged_smearing=False,
            use_relaxed_geometry=False,
            use_geometry_from=None,
            ):
        """Adds some converged quantities to the given input variables dict.

        Parameters
        ----------
        input_variables: dict
            The dict of input variables to manipulate.
        sequencer: Sequencer object, optional
            The Sequencer object that the input vars will be acted upon.
            Needed for some parameters.
        use_gs_converged_ecut: bool, optional
            If True, the gs converged ecut is used.
        use_gs_converged_kgrid: bool, optional
            If True, use the gs converged kgrid.
        use_gs_converged_smearing: bool, optional
            If True, use the gs converged smearing variables.
        use_relaxed_geometry: bool, optional
            If True, the relaxed geometry will be added to the input variables.
        use_phonon_converged_ecut: bool, optional
            If True, use the phonon converged ecut variables.
        use_phonon_converged_smearing: bool, optional
            If True, use the phonon converged smearing variables.
        use_geometry_from: str, optional
            If not None, specifies the workflow sequence (it's scf part)
            whose geometry we want to use (through the
            'scf_load_geometry_from' sequencer's property).
        """
        if not isinstance(input_variables, dict):
            raise TypeError(
                    "'input_variables' should be a dict but got "
                    f"'{input_variables}'.")
        if use_gs_converged_ecut and use_phonon_converged_ecut:
            raise ValueError(
                    "'use_gs_converged_ecut' cannot be used in conjunction "
                    "with 'use_phonon_converged_ecut'. Which ecut to take?")
        if use_gs_converged_ecut:
            if not self._compute_gs_ecut_convergence:
                if self.converged_gs_ecut is not None:
                    input_variables.update(self.converged_gs_ecut)
                else:
                    raise ValueError(
                        "Need to set 'gs_ecut_convergence' to True or 'converg"
                        "ed_gs_ecut' upon init.")
            else:
                if not self.gs_ecut_convergence_sequencer.sequence_completed:
                    self._logger.info(
                            "Need to wait for 'gs_ecut_convergence' to "
                            "complete.")
                    self.stop_at_workflow = "gs_ecut_convergence"
                    return
                if isinstance(
                        self.gs_ecut_convergence_sequencer, SequencersList):
                    for ecut_seq in self.gs_ecut_convergence_sequencer:
                        varname = ecut_seq.ecuts_input_variable_name
                        varvalue = ecut_seq.scf_converged_ecut
                        input_variables[varname] = varvalue
                else:
                    ecut_seq = self.gs_ecut_convergence_sequencer
                    varname = ecut_seq.ecuts_input_variable_name
                    varvalue = ecut_seq.scf_converged_ecut
                    input_variables[varname] = varvalue
        if use_gs_converged_kgrid:
            if not self._compute_gs_kgrid_convergence:
                if self.converged_gs_kgrid is not None:
                    input_variables.update(self.converged_gs_kgrid)
                else:
                    raise ValueError(
                        "Need to set 'gs_kgrid_convergence' to True or "
                        "'gs_converged_kgrid' upon init."
                        )
            else:
                if not self.gs_kgrid_convergence_sequencer.sequence_completed:
                    self._logger.info(
                            "Need to wait for 'gs_kgrid_convergence' to "
                            "complete.")
                    self.stop_at_workflow = "gs_kgrid_convergence"
                    return
                sequencer = self.gs_kgrid_convergence_sequencer
                varname = sequencer.kgrids_input_variable_name
                value = sequencer.scf_converged_kgrid
                input_variables[varname] = value
        if use_gs_converged_kgrid and use_gs_converged_smearing:
            raise ValueError(
                    "'use_gs_converged_kgrid' is not compatible with 'use_gs_"
                    "converged_smearing'. Because we determine converged kgrid"
                    " when we compute the smearing convergence.")
        if use_gs_converged_smearing and use_phonon_converged_smearing:
            raise ValueError(
                    "'use_gs_converged_smearing' cannot be used in conjunction"
                    " with 'use_phonon_converged_smearing'. "
                    "Which smearing to take?")
        if use_gs_converged_smearing:
            if not self._compute_gs_smearing_convergence:
                if self.converged_gs_smearing is not None:
                    input_variables.update(self.converged_gs_smearing)
                else:
                    raise ValueError(
                        "Need to set 'gs_smearing_convergence' to True "
                        "or 'converged_gs_smearing' upon init.")
            else:
                seq = self.gs_smearing_convergence_sequencer
                if not seq.sequence_completed:
                    self._logger.info(
                            "Need to wait for 'gs_smearing_convergence' to "
                            "complete.")
                    self.stop_at_workflow = "gs_smearing_convergence"
                    return
                seq = self.gs_smearing_convergence_sequencer
                input_variables.update(seq.converged_smearing_variables)
        # relaxation comes before phonon convergences
        if use_relaxed_geometry or use_geometry_from is not None:
            if use_geometry_from is None:
                use_geometry_from = "relaxation"
            if use_relaxed_geometry and use_geometry_from != "relaxation":
                raise ValueError(
                        "'use_geometry_from' != 'relaxation' is incompatible "
                        "with 'use_relaxed_geometry' option.")
            self._use_geometry_from(
                    use_geometry_from, input_variables, sequencer)
        if use_phonon_converged_ecut:
            if not self._compute_phonon_ecut_convergence:
                if self.converged_phonon_ecut is not None:
                    input_variables.update(self.converged_phonon_ecut)
                else:
                    raise ValueError(
                        "Need to set 'phonon_ecut_convergence' to True "
                        "or 'converged_phonon_ecut' upon init.")
            else:
                seq = self.phonon_ecut_convergence_sequencer
                if not seq.sequence_completed:
                    self._logger.info(
                            "Need to wait for 'phonon_ecut_convergence' to "
                            "complete.")
                    self.stop_at_workflow = "phonon_ecut_convergence"
                    return
                if isinstance(
                        self.phonon_ecut_convergence_sequencer,
                        SequencersList):
                    for ecut_seq in self.phonon_ecut_convergence_sequencer:
                        varname = ecut_seq.ecuts_input_variable_name
                        varvalue = ecut_seq.scf_converged_ecut
                        input_variables[varname] = varvalue[varname]
                else:
                    varname = seq.ecuts_input_variable_name
                    varvalue = seq.scf_converged_ecut
                    input_variables[varname] = varvalue[varname]
        if use_phonon_converged_smearing:
            if not self._compute_phonon_smearing_convergence:
                if self.converged_phonon_smearing is not None:
                    input_variables.update(self.converged_phonon_smearing)
                else:
                    raise ValueError(
                        "Need to set 'phonon_smearing_convergence' to True "
                        "or 'converged_phonon_smearing' upon init.")
            else:
                seq = self.phonon_smearing_convergence_sequencer
                if not seq.sequence_completed:
                    self._logger.info(
                            "Need to wait for 'phonon_smearing_convergence' to"
                            " complete.")
                    self.stop_at_workflow = "phonon_smearing_convergence"
                    return
                input_variables.update(seq.converged_smearing_variables)

    def _use_geometry_from(self, workflow_part, input_variables, sequencer):
        if workflow_part == "relaxation":
            if not self._compute_relaxation:
                if self.relaxed_geometry is not None:
                    input_variables.update(self.relaxed_geometry)
                    return
                else:
                    raise ValueError(
                            "Need to set 'relaxation' to True or "
                            "'relaxed_geometry' upon init.")
            if not self.workflow_part_completed("relaxation"):
                return
            # if we're here, relaxation part of the workflow successfully done
            # pop out all geometry variables in case they are not updated
            for var in self._all_geometry_variables:
                if var in input_variables:
                    input_variables.pop(var)
            input_variables.update(
                    self.relaxation_sequencer.relaxed_geometry_variables)
        else:
            if sequencer is None:
                raise DevError("Need sequencer.")
            # we use different workflow part for geometry
            # assume we load from scf part
            prev_sequencer = self.get_sequencer(workflow_part)
            if "scf_" not in (
                    prev_sequencer._all_sequencer_prefixes) or not hasattr(
                    prev_sequencer, "scf_load_geometry_from"):
                raise ValueError(
                        f"Cannot load geometry from '{workflow_part}'.")
            # TODO: generalize loading geometry from to any other calc
            if not hasattr(sequencer, "scf_load_geometry_from"):
                raise NotImplementedError(
                        f"{sequencer} does not support the 'scf_load_"
                        "geometry_from' property.")
            if prev_sequencer.scf_load_geometry_from is not None:
                # use this instead
                sequencer.scf_load_geometry_from = (
                        prev_sequencer.scf_load_geometry_from)
            else:
                prev_sequencer.init_sequence()
                # take geometry from first scf calc in case there are many
                scf_calcs = prev_sequencer.get_sequence_calculation(
                        "scf_", always_return_list=True)
                sequencer.scf_load_geometry_from = scf_calcs[0]


class SequencersList(BaseUtility):
    """Container for sequencers. Can launch all sequencers in it and access
    and set a bunch of global properties to all sequencers it contains.
    """
    _loggername = "SequencersList"

    def __init__(self, sequencers, **kwargs):
        super().__init__(**kwargs)
        if not is_list_like(sequencers):
            raise TypeError(
                    f"Expected list-like but got: '{sequencers}'.")
        self.sequencers = sequencers

    def __iter__(self):
        for sequencer in self.sequencers:
            yield sequencer

    def __add__(self, sequencers_list):
        # adding a list of sequencers to this one
        if isinstance(sequencers_list, SequencersList):
            sequencers_list = sequencers_list.sequencers
        return SequencersList(self.sequencers + sequencers_list)

    def __len__(self):
        return len(self.sequencers)

    def __getitem__(self, item):
        return self.sequencers[item]

    def __radd__(self, sequencers_list):
        # adding this object to another list
        if isinstance(sequencers_list, SequencersList):
            sequencers_list = sequencers_list.sequencers
        return SequencersList(sequencers_list + self.sequencers)

    def __repr__(self):
        return ("< SequencersList: [\n" + "\n".join(
            [repr(x) for x in self]) +
            "\n] >")

    def __setitem__(self, item, value):
        self.sequencers[item] = value

    @property
    def bypass_convergence_check(self):
        return self[0].bypass_convergence_check

    @bypass_convergence_check.setter
    def bypass_convergence_check(self, bypass):
        for sequencer in self:
            sequencer.bypass_convergence_check = bypass

    @property
    def bypass_sequence_comparison(self):
        return self[0].bypass_sequence_comparison

    @bypass_sequence_comparison.setter
    def bypass_sequence_comparison(self, bypass):
        for sequencer in self:
            sequencer.bypass_sequence_comparison = bypass

    @property
    def sequence_completed(self):
        return all([seq.sequence_completed for seq in self])

    def append(self, sequencer):
        self.sequencers.append(sequencer)

    def clean(self):
        """Cleans all sequencers in the list.
        """
        for seq in self:
            seq.clean()

    def clear_sequence(self):
        """Clears sequence for all Sequencers in the list.
        """
        for seq in self:
            seq.clear_sequence()

    def init_sequence(self):
        for seq in self:
            seq.init_sequence()

    def write(self):
        for sequencer in self:
            sequencer.write()

    def run(self):
        for sequencer in self:
            sequencer.run()
