import os
import tempfile
import unittest

from ..config import ConfigFileParser


class TestConfigFile(unittest.TestCase):
    def setUp(self):
        self.tempdir = tempfile.TemporaryDirectory()
        # use a subclass of the config file parser to change it's path
        self.configdir = os.path.join(self.tempdir.name, "config")
        self.configpath = os.path.join(self.configdir, "abiproject")

        class ConfigFileParserTestClass(ConfigFileParser):
            config_path = self.configpath

        self.config = ConfigFileParserTestClass()
        # self.config.set_all_defaults()

    def tearDown(self):
        self.tempdir.cleanup()
        del self.tempdir

    def test_config_parser_works(self):
        # check that a 'none' value returns None
        # by default, abinit_default_build is "abinit_default_build
        self.assertIs(self.config.ABINIT.default_build, "abinit_default_build")
        # check that a 'false' value returns False
        # by default, the abinit_default_build don't have mpi
        self.assertIs(self.config.abinit_default_build.has_mpi, False)
        # samething for true but set it in the default config first
        # use force for tests to prevent the raise of an error
        # cause WE SHOULD NOT BE ABLE TO MODIFY THE CONFIG FILE USING THE API
        # EXCEPT WHEN SETTING DEFAULTS
        self.config.write("has_mpi", True, "abinit_default_build", _force=True)
        self.assertIs(self.config.abinit_default_build.has_mpi, True)
        # check that a list config entry is well read
        for prop in ("modules_to_load", "modules_to_unload", "modules_to_use"):
            self.assertEqual(
                    getattr(self.config.abinit_default_build, prop), [])
            modules = ['test_module', 'test_module_2']
            self.config.write(prop, modules, "abinit_default_build",
                              _force=True)
            self.assertEqual(
                    getattr(self.config.abinit_default_build, prop), modules)

    def test_config_entries(self):
        # test that a config entry can be represented (printed)
        # should not raise an error
        repr(self.config.ABINIT)
        # test that we can get a build entry from the special method
        entry = self.config.get_build("abinit_default_build")
        self.assertIn("build_path", entry)
        # if accessing a non-existent entry, it should raise an error
        with self.assertRaises(ValueError):
            self.config.get_build("non-existent-build")
