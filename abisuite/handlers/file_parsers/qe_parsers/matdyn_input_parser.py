from .bases import BaseQEInputParser
from ...file_structures import QEMatdynInputStructure
from ....routines import decompose_line


class QEMatdynInputParser(BaseQEInputParser):
    """Class that can parse a Quantum Espresso matdyn.x input File.
    """
    _loggername = "QEMatdynInputParser"
    _structure_class = QEMatdynInputStructure

    def _check_for_special_input_var(self, i, lines):
        lowered = lines[i].lower()
        if "&" in lowered or "/" in lowered or "=" in lowered:
            return super()._check_for_special_input_var()
        # we're supposed to be at the end of file
        # first line is number of qpts
        s, ints, f = decompose_line(lowered)
        total_qpts = ints[0]  # total number of qpts written in file
        end = lines[i + 1:]
        qpts = []  # qpts coordinates
        nqpts = []  # divisions between qpts
        for line in end:
            s, ints, f = decompose_line(line)
            qpts.append(f)
            nqpts.append(ints[0])
            if len(ints) > 1:
                raise LookupError(
                    f"Detected more than 1 int on line: '{line}'"
                    " Cannot distinguish qpt coords!")
        assert total_qpts == len(qpts)
        return {"q_points": {"qpts": qpts, "nqpts": nqpts}}, len(lines)
