#!/bin/bash

MPIRUN=""
EXECUTABLE="/home/fgoudreault/Workspace/abinit/abinit/build/develop/2021-04-20/src/98_main/anaddb"
INPUT=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_workflows/test_files/abinit_si/nc/phonon_dispersion/anaddb_run/run/anaddb_run.files
LOG=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_workflows/test_files/abinit_si/nc/phonon_dispersion/anaddb_run/anaddb_run.log
STDERR=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_workflows/test_files/abinit_si/nc/phonon_dispersion/anaddb_run/anaddb_run.stderr


$MPIRUN $EXECUTABLE < $INPUT > $LOG 2> $STDERR
