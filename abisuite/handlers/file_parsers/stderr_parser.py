from .bases import BaseFormattedCalculationFileParser
from ..file_structures import StderrStructure


class StderrParser(BaseFormattedCalculationFileParser):
    """Parser for a stderr file. Doesn't actually parse anything for now.
    Only counts number of lines.
    """
    _loggername = "StderrParser"
    _expected_ending = ".stderr"
    _structure_class = StderrStructure

    def _extract_data_from_lines(self, lines):
        return {"nlines": len(lines)}

    @classmethod
    def _filepath_from_meta(cls, meta, *args, **kwargs):
        from ..file_handlers import PBSFile
        with PBSFile.from_meta_data_file(meta) as pbs:
            if pbs.exists:
                return pbs.stderr_path
        raise FileNotFoundError("stderr")
        # # pbs does not exists, extrapolate path from meta data file
        # with meta:
        #     return os.path.join(
        #             meta.workdir, f"{meta.jobname}.stderr")
