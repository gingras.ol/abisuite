from abisuite import AbinitRelaxationSequencer as Sequencer


sequencer = Sequencer()
sequencer.scf_workdir = "relax_run"
sequencer.scf_input_variables = {
        "autoparal": 1,
        "toldfe": 1e-10,
        "acell": [10.61] * 3,
        "rprim": [[0.0, 0.5, 0.5], [0.5, 0.0, 0.5], [0.5, 0.5, 0.0]],
        "ntypat": 2,
        "znucl": [13, 33],
        "natom": 2,
        "typat": [1, 2],
        "xred": [[0.0, 0.0, 0.0], [0.25, 0.25, 0.25]],
        "nband": 4,
        "ixc": 1,
        "ngkpt": [4, 4, 4],
        "nshiftk": 4,
        "shiftk": [[0.0, 0.0, 0.5], [0.0, 0.5, 0.0],
                   [0.5, 0.0, 0.0], [0.5, 0.5, 0.5]],
        "nstep": 25,
        "diemac": 9.0,
        "tolmxf": 1e-5,
        "ntime": 20,
        "ionmov": 22,
        "ecutsm": 0.5,
        "ecut": 20.0,
        }
sequencer.scf_pseudos = ["13al.981214.fhi", "33as.pspnc"]
sequencer.scf_mpi_command = "mpirun"
sequencer.scf_mpi_command_arguments = "-np 4"
sequencer.relax_atoms = True
sequencer.relax_cell = True
sequencer.launch()
