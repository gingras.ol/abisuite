from .bases import BaseQEPackager


class QEPHPackager(BaseQEPackager):
    """Packager class for a 'ph.x' calculation from the Quantum Espresso
    software suite.
    """
    _calctype = "qe_ph"
    _loggername = "QEPHPackager"

    def keep_file(self, handler, *args, **kwargs):
        if ".dyn" in handler.basename:
            return True
        # save files used for epw interpolation calculations
        if handler.basename.endswith("dvscf1"):
            return True
        if "_ph0" in handler.path and ".phsave" in handler.path:
            return True
        return super().keep_file(handler, *args, **kwargs)
