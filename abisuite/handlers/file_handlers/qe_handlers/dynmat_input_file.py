from ..bases import BaseInputFileHandler
from ...file_approvers import QEDynmatInputApprover
from ...file_parsers import QEDynmatInputParser
from ...file_structures import QEDynmatInputStructure
from ...file_writers import QEDynmatInputWriter


class QEDynmatInputFile(BaseInputFileHandler):
    """File handler for a Quantum Espresso dynmat.x input file.
    """
    _approver_class = QEDynmatInputApprover
    _loggername = "QEDynmatInputFile"
    _parser_class = QEDynmatInputParser
    _structure_class = QEDynmatInputStructure
    _writer_class = QEDynmatInputWriter
