from .bases import BaseQELogStructure


class QEMatdynLogStructure(BaseQELogStructure):
    """Structure for a Quantum Espresso matdyn.x log file.
    """
    all_attributes = ("timing", )
