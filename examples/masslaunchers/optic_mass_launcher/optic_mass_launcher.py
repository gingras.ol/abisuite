from abilaunch import OpticMassLauncher as Launcher


common_vars = {"ddkfile_1": "../example_data/odat_example_1WF7",
               "ddkfile_2": "../example_data/odat_example_1WF8",
               "ddkfile_3": "../example_data/odat_example_1WF9",
               "wfkfile": "../example_data/odat_example_WFK",
               "broadening": 0.002,
               "maxomega": 0.3,
               "domega": 0.0003,
               "tolerance": 0.002,
               "num_lin_comp": 1,
               "num_nonlin_comp": 0,
               "num_linel_comp": 0,
               "num_nonlin2_comp": 0,
               }

specific_variables = [{"lin_comp": 11}, {"lin_comp": 22}]
jobnames = ["optic_comp_11", "optic_comp_22"]

l = Launcher(jobnames)
l.workdir = "."
l.common_input_variables = common_vars
l.specific_input_variables = specific_variables
l.command = "~/Workspace/abinit/build/master/vanilla/src/98_main/optic"
l.write(overwrite=True)
l.run()
