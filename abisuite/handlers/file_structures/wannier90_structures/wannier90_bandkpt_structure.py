from ..bases import BaseStructure


class Wannier90BandkptStructure(BaseStructure):
    """Structure class for a Wannier90 seedname_band.kpt file produced by
    wannier90.x when bands_plot=True.
    """
    all_attributes = ("kpts", "nkpt", )
