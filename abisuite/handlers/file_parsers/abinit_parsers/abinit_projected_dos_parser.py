import os

import numpy as np

from .bases import BaseAbinitDOSParser
from ...file_structures import AbinitProjectedDOSStructure


class AbinitProjectedDOSParser(BaseAbinitDOSParser):
    """Parser class for a _DOS_AT* file produced by Abinit.
    """
    _expected_ending = "_DOS_AT"
    _loggername = "AbinitProjectedDOSParser"
    _structure_class = AbinitProjectedDOSStructure

    @classmethod
    def _filepath_from_meta(cls, meta):
        """Returns the file paths of all projected DOS files from
        the meta data file handler.
        """
        filepaths = []
        with meta:
            out = meta.output_data_dir
        for filename in os.listdir(out):
            if cls._expected_ending in filename:
                filepaths.append(os.path.join(out, filename))
        if not filepaths:
            raise FileNotFoundError("Could not find any _DOS_AT* files.")
        return filepaths

    def _get_core_data(self):
        # data is organized as follows:
        # energies - DOS (for l=0-) - DOS (l=1) ... - DOS (l=4) -
        # Integrated DOS (for l=0) ... Integrated DOS for l=4.
        # The next columns I'm not sure...
        data = np.loadtxt(self.path)
        return {"energies": data[:, 0],
                "projected_dos": {
                    "l=0": data[:, 1], "l=1": data[:, 2], "l=2": data[:, 3],
                    "l=3": data[:, 4], "l=4": data[:, 5],
                    "lm=0 0": data[:, 11], "lm=1-1": data[:, 12],
                    "lm=1 0": data[:, 13], "lm=1 1": data[:, 14],
                    "lm=2-2": data[:, 15], "lm=2-1": data[:, 16],
                    "lm=2 0": data[:, 17], "lm=2 1": data[:, 18],
                    "lm=2 2": data[:, 19], "lm=3-3": data[:, 20],
                    "lm=3-2": data[:, 21], "lm=3-1": data[:, 22],
                    "lm=3 0": data[:, 23], "lm=3 1": data[:, 24],
                    "lm=3 2": data[:, 25], "lm=3 3": data[:, 26],
                    "lm=4-4": data[:, 27], "lm=4-3": data[:, 28],
                    "lm=4-2": data[:, 29], "lm=4-1": data[:, 30],
                    "lm=4 0": data[:, 31], "lm=4 1": data[:, 32],
                    "lm=4 2": data[:, 33], "lm=4 3": data[:, 34],
                    "lm=4 4": data[:, 35],
                    },
                "projected_integrated_dos": {
                    "l=0": data[:, 6], "l=1": data[:, 7], "l=2": data[:, 8],
                    "l=3": data[:, 9], "l=4": data[:, 10],
                    },
                }
