from .bases import BaseQECleaner


class QEPHCleaner(BaseQECleaner):
    """Cleaner class for a 'qe_ph' calculation.
    """
    _calctype = "qe_ph"
    _loggername = "QEPHCleaner"
