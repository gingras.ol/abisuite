from .bases import BaseQECalculationStatusChecker


class QEProjwfcCalculationStatusChecker(BaseQECalculationStatusChecker):
    """Status checker for a projwfc.x calculation from Quantum Espresso.
    """
    _loggername = "QEProjwfcCalculationStatusChecker"
