import json
import os

from .bases import DB
from .exceptions import (
        AbiDBRuntimeError, CalculationDoesNotExistError,
        CachedAbiDBFileNotFoundError, ColumnDoesNotExistsError,
        )
from .. import USER_CONFIG
from ..bases import BaseRemoteClient
from ..exceptions import IsAFileError
from ..handlers import CalculationDirectory, is_calculation_directory
from ..linux_tools import mkdir
from ..routines import full_abspath, is_list_like


__CALCULATIONS_TABLE_NAME__ = "calculations"
__CALCULATIONS_TABLE_COLUMNS__ = {
    # use this SQL data type to make sure id is an
    # alias to the rowid column
    "id": "INTEGER",
    # a path
    "calculation": str,
    # shall be a json dictionary
    "status": "TEXT",
    "walltime": float,
    # not a column but is used to tell DM mother class
    # which column is the primary key
    "primary_key": "id",
    "monitored": bool,
    # True or False, just a quick reference index to store which calculation
    # we want to monitor
    "packaged": bool,
    # True if calculation was packaged at some point. False otherwise.
    "project": "TEXT",
    # a project tag to make calculations more easily classified
    }
__ABIDB_DEFAULT_VALUES_OF_COLUMNS__ = {
        "packaged": False,
        "project": "",
        "monitored": True,
        "status": None,
        "walltime": None,
        }
__ABIDB_COLUMNS_CAN_BE_NONE__ = (
        "status", "walltime",
        )


class AbiDB(DB):
    """Class that is used to handle an abisuite database.

    An abisuite database contains at least one table named 'calculations' that
    contains the following columns:

    - id (int)
    - calculation (path)
    - status (dict) [stored in the DB as a json string]
    - monitored (bool)
    - packaged (bool)
    - project (str)
    - walltime (float) (in seconds)
    """
    _loggername = "AbiDB"

    def __init__(self, *args, remote_data=None, **kwargs):
        super().__init__(*args, **kwargs)
        self._is_remote = remote_data is not None
        self._remote_data = remote_data
        self._remote_client = None
        self._integrity_checked = False
        self.default_values_of_columns = __ABIDB_DEFAULT_VALUES_OF_COLUMNS__
        self.columns_can_be_none = __ABIDB_COLUMNS_CAN_BE_NONE__

    def __contains__(self, item):
        all_data = self.select("*")
        all_ids = [x[0] for x in all_data]
        all_calcs = [x[1] for x in all_data]
        if isinstance(item, int):
            return item in all_ids
        if isinstance(item, str):
            item = full_abspath(item)
            return item in all_calcs
        if isinstance(item, CalculationDirectory):
            return item.path in all_calcs
        raise TypeError(item)

    def __getitem__(self, item):
        if isinstance(item, int):
            return self.get_calculation_from_id(item)
        elif isinstance(item, str):
            return self.get_id_from_calculation(item)
        elif isinstance(item, CalculationDirectory):
            return self.get_id_from_calculation(item.path)
        else:
            raise KeyError(item)

    @property
    def hostname(self):
        if not self.is_remote:
            raise AttributeError("No hostname when there are no remotes!")
        return self.remote_data["hostname"]

    @property
    def remote_data(self):
        return self._remote_data

    @property
    def remote_client(self):
        if self._remote_client is not None:
            return self._remote_client
        if not self.is_remote:
            raise ValueError("AbiDB is not remote so no remote_client.")
        # create remote client
        self._remote_client = AbiDBRemoteClient(
                self.remote_data, loglevel=self._loglevel)
        return self.remote_client

    @property
    def is_remote(self):
        return self._is_remote

    @property
    def last_calculation_id(self):
        all_ids = self.select("id")
        if not len(all_ids):
            return None
        return max([x[0] for x in all_ids])

    def add_calculation(self, *args, **kwargs):
        # alias for insert override
        self.insert(*args, **kwargs)

    def add_columns(self, **kwargs):
        super().add_columns(__CALCULATIONS_TABLE_NAME__, **kwargs)

    def check_database_integrity(self, raise_error=True):
        """Check that all calculations exists.

        Also check that all columns are present. If not add them.
        """
        if self._integrity_checked:
            # don't recheck
            return
        if not self.is_remote:
            # check that all paths exists
            all_calcs = self.select("*", check_integrity=False)
            all_ids = []
            for entry in all_calcs:
                id_ = entry[0]
                # TBH the following check should never fail.
                # otherwise sqlite3 is broken xD
                if id_ in all_ids:
                    msg = (f"Calculation ID '{id_}' appears more"
                           " than once in "
                           f"the database: '{self.db}'.")
                    if raise_error:
                        raise ValueError(msg)
                    else:
                        print(msg)
                all_ids.append(id_)
                path = entry[1]
                if not CalculationDirectory.is_calculation_directory(path):
                    msg = (f"DB contains a calculation that does not exists: "
                           f"'{path}'.")
                    if raise_error:
                        raise NotADirectoryError(msg)
                    else:
                        print(msg)
        for col, cast in __CALCULATIONS_TABLE_COLUMNS__.items():
            if col == "primary_key":
                continue
            if col not in self.columns[__CALCULATIONS_TABLE_NAME__]:
                self.add_columns(**{col: cast})
        self._integrity_checked = True

    def clean(self):
        """Removes all calculations that don't exist anymore.
        """
        if self.is_remote:
            raise NotImplementedError()
        calcs = self.get_all_calculations()
        for calc in calcs:
            if not is_calculation_directory(calc):
                self.delete_calculation(calc, check_integrity=False)

    def create_database(self, path, *args, **kwargs):
        # create database and automatically add a 'calculation' table in it.
        super().create_database(path, *args, **kwargs)
        self.create_table(
                __CALCULATIONS_TABLE_NAME__,
                **__CALCULATIONS_TABLE_COLUMNS__)

    def delete_calculation(self, calc, check_integrity=False):
        """Delete a calculation from the database based on it's path.

        Parameters
        ----------
        calc : str
            The calculation to delete.
        check_integrity: bool, optional
            If True, checks that the calculation is inside database before
            deleting.
        """
        if not isinstance(calc, str):
            raise TypeError(
                    f"Expected a path but got: '{calc}'.")
        calc = full_abspath(calc)
        if check_integrity:
            if calc not in self:
                self._logger.info(
                        "Calculation not in database. "
                        f"Nothing deleted: '{calc}'"
                        )
                return
        self._logger.debug(f"Deleting calculation: '{calc}'")
        self.delete_row(
                __CALCULATIONS_TABLE_NAME__, f"calculation == '{calc}'"
                )

    def delete_column(self, column):
        # this should not be called ever...
        super().delete_column(__CALCULATIONS_TABLE_NAME__, column)

    def delete_id(self, id_to_delete):
        """Delete a calculation from the database based on it's ID.

        Parameters
        ----------
        id_to_delete : int
            The ID of the calculation to delete.
        """
        if not isinstance(id_to_delete, int):
            raise TypeError(
                    f"Expected integer but got: '{id_to_delete}'.")
        if id_to_delete not in self:
            self._logger.info(
                    f"ID {id_to_delete} not in database. Nothing deleted.")
            return
        self._logger.debug("Deleting ID: {id_to_delete}.")
        self.delete_row(__CALCULATIONS_TABLE_NAME__, f"id == {id_to_delete}")

    def download(self, refresh=True):
        """If abidb object is taken from a remote machine, (re)download the
        db file.

        Parameters
        ----------
        refresh: bool, optional
            If True, performs an AbiDB refresh on the remote machine.
        """
        if not self.is_remote:
            raise RuntimeError("Not a remote.")
        db_path = AbiDB.get_cached_db_path(self.remote_data["hostname"])
        self.remote_client.download_database(db_path, refresh=refresh)
        self.db = db_path

    def get_all_calculations(self):
        """Returns the list of all calculations.
        """
        return [x[0] for x in self.select("calculation")]

    def get_all_monitored_calculations(self):
        """Returns the list of all monitored calculations.
        """
        return [x[0] for x in self.select(
            "calculation", condition="monitored = 1")]

    def get_calculation_from_id(self, id_to_get):
        """Returns the calculation from the given id.

        Parameters
        ----------
        id_to_get : int
            The calculation id.
        """
        if not isinstance(id_to_get, int):
            raise TypeError(
                    f"Expected integer for ID but got: '{id_to_get}'.")
        all_ids = [x[0] for x in self.select("id")]
        if id_to_get not in all_ids:
            raise CalculationDoesNotExistError(
                    f"ID='{id_to_get}' not in database.")
        return self.execute(
            f"SELECT calculation FROM {__CALCULATIONS_TABLE_NAME__} "
            f"WHERE id = {id_to_get}")[0][0]

    def get_id_from_calculation(self, calculation):
        """Returns the id from a given calculation path.

        Parameters
        ----------
        calculation : str
            The path to the calculation.
        """
        if not isinstance(calculation, str):
            raise TypeError(
                    f"Expected a path but got: '{calculation}'.")
        all_calcs = self.get_all_calculations()
        calculation = full_abspath(calculation)
        if calculation not in all_calcs:
            raise CalculationDoesNotExistError(
                    f"Calculation doesn't exist in database: '{calculation}'.")
        id_ = self.execute(
                f"SELECT id FROM {__CALCULATIONS_TABLE_NAME__} WHERE "
                f"calculation = '{calculation}';")[0][0]
        return id_

    def get_packaged(self, calculation):
        """Return the packaged tag of a calculation.

        Parameters
        ----------
        calculation: int or str
            The calculation id or path.

        Returns
        -------
        bool: The packaged tag.
        """
        packaged = self.select_from_row("packaged", calculation)
        if packaged is not None:
            return bool(packaged)
        # for some reason the default is not set. apply default
        default = __ABIDB_DEFAULT_VALUES_OF_COLUMNS__["packaged"]
        self.update_calculation_packaged(calculation, default)
        return default

    def get_project(self, calculation):
        """Return the project tag of a calculation.

        Parameters
        ----------
        calculation: int or str
            The calculation id or path.

        Returns
        -------
        str: The project tag.
        """
        return self.select_from_row("project", calculation)

    def get_monitored(self, calculation):
        """Checks if a calculation is monitored.

        Returns the value of the 'monitored' column.

        Parameters
        ----------
        calculation: int or str
            The calculation id or path.

        Returns
        -------
        bool: True or False for monitored calculations.
        """
        return bool(self.select_from_row("monitored", calculation))

    def get_status(self, entry, recompute=False):
        """Returns the status of a calculation based either on its ID or its
        path.

        Parameters
        ----------
        entry: int or str
            The calculation entry to get the status from the DB.
        recompute: bool, optional
            If True and the status loaded provides a 'calculation_finished' to
            False, the status will be recomputed and put back into the DB.
        """
        status = self.select_from_row("status", entry)
        if status is None:
            if self.is_remote:
                # cannot update calculation status unless we redownload
                return None
            if recompute is False:
                return status
            self.update_calculation_status(
                    entry, newstatus=None, recompute=True)
            return self.get_status(entry)
        if status["calculation_finished"] is False:
            # self.refresh(entry)  # WHY THIS? FG 2020/05/26
            # recompute computation status but return directly what we get
            if self.is_remote:
                # just return this
                return status
            # if not remote, recompute it
            if recompute:
                status = self._compute_new_calculation_status(entry)
                # update status but return this one
                self.update_calculation_status(entry, new_status=status)
            return status
        return status

    def get_walltime(self, calculation, force=False, refresh=False):
        """Return the walltime of a calculation.

        Parameters
        ----------
        calculation: int or str
            The calculation id or path.
        refresh: bool, optional
            If True and walltime is None or force is True, the walltime
            entry is refreshed.

        Returns
        -------
        float: The walltime of the calculation. Can be None.
        """
        walltime = self.select_from_row("walltime", calculation)
        if walltime is None:
            if not refresh:
                return walltime
        else:
            # walltime is not None. only refresh if forced
            if not refresh:
                return walltime
            if not force:
                return walltime
        # refresh it
        self.update_walltime(calculation)
        return self.get_walltime(calculation, refresh=False)

    def insert(self, calculation, check_integrity=False, **kwargs):
        """Add a calculation to the database.

        Parameters
        ----------
        calculation: str or CalculationDirectory instance
            The calculation to insert into the DB.
        check_integritry: bool, optional
            If True, the database integrity is checked beforehand.
        Other kwargs are passed to the mother's class method and can be used
        to override default column values.

        Raises
        ------
        NotADirectoryError: If 'calculation' is not a path to a valid
            calculation directory.
        """
        if check_integrity:
            # before inserting check db integrity
            self.check_database_integrity()
        if not isinstance(calculation, CalculationDirectory):
            if not CalculationDirectory.is_calculation_directory(
                            calculation, loglevel=self._loglevel):
                raise NotADirectoryError(
                        f"Not a calculation directory: '{calculation}'.")
            calculation = CalculationDirectory.from_calculation(calculation)
        # don't insert if calculation already exists
        all_calcs = self.get_all_calculations()
        if calculation.path in all_calcs:
            self._logger.info(
                f"Calculation already in database: '{calculation.path}'. "
                "Nothing added.")
            return
        # with calculation:
        #     status = calculation.status
        defaults = __ABIDB_DEFAULT_VALUES_OF_COLUMNS__.copy()
        defaults.update(kwargs)
        super().insert(
                __CALCULATIONS_TABLE_NAME__,
                calculation=calculation.path,
                # status=json.dumps(status),  # add calc status
                **defaults,
                )

    def refresh(self, calculations):
        """Refresh the status of the given calculations.

        Only necessary calculations will be refreshed i.e. the ones that
        don't have the 'calculation_finished' flag set to True in their status.

        Parameters
        ----------
        calculations: list_like or str
            A calculation or a list of calculations to refresh.
        """
        if isinstance(calculations, int) or isinstance(calculations, str):
            self.refresh((calculations, ))
            return
        for calculation in calculations:
            self.update_calculation_status(calculation, recompute=True)
            self.update_walltime(calculation)

    def refresh_monitored(self):
        """Same as refresh but it is done for each monitored calculations.
        """
        self.refresh(self.get_all_monitored_calculations())

    def select(self, column, check_integrity=False, **kwargs):
        """Select a column from the 'calculations' table.

        Parameters
        ----------
        column: str
            The column name to select.
        check_integrity: bool, optional
            If True, the database integrity is checked beforehand.
        """
        # check db integrity before reading
        if check_integrity:
            self.check_database_integrity()
        return super().select(column, __CALCULATIONS_TABLE_NAME__, **kwargs)

    def select_from_row(self, column, calculation):
        """Returns the value of a specific column in a row.

        Parameters
        ----------
        calculation: int or str
            The calculation path.
        """
        self._logger.debug(f"Selecting '{column}' for row '{calculation}'.")
        if column not in __CALCULATIONS_TABLE_COLUMNS__:
            raise ValueError(f"Not a database column: '{column}'.")
        if not isinstance(calculation, int):
            entry = self[calculation]
        else:
            entry = calculation
        try:
            data = self.select(column, condition=f"id = {entry}")[0][0]
        except ColumnDoesNotExistsError:
            self.add_columns(
                    **{column: __CALCULATIONS_TABLE_COLUMNS__[column]})
            data = self.select(column, condition=f"id = {entry}")[0][0]
        if isinstance(data, bytes):
            data = data.decode("utf-8")
        if isinstance(data, str):
            # if its a dict, need to decode using json
            if "{" in data and "}" in data:
                return json.loads(data)
            # samething if its a list
            if "[" in data and "]" in data:
                return json.loads(data)
            # or a tuple
            if data.startswith("(") and data.endswith(")"):
                return json.loads(data)
        # everything else, return data and that's it
        return data

    def update(self, **kwargs):
        super().update(__CALCULATIONS_TABLE_NAME__, **kwargs)

    def update_calculation(self, id_to_update, new_calculation, force=False):
        """Update the calculation path for a given ID.

        Parameters
        ----------
        id_to_update : int
            The ID of the database entry to update.
        new_calculation : str
            The new path to the calculation. Must be a valid calculation path.
        force: bool, optional
            If True, the new_calculation is forced into the db even though
            it is not a valid calculation path.

        Raises
        ------
        ValueError: If new_calculation already in database.
        NotADirectoryError: If new_calculation is not a directory unless
            the 'force' option is used.
        """
        new_calculation = full_abspath(new_calculation)
        if new_calculation in self:
            raise ValueError(
                    f"Calculation already in database: '{new_calculation}'.")
        if not CalculationDirectory.is_calculation_directory(
                new_calculation) and not force:
            raise NotADirectoryError(
                    f"Not a calculation directory: '{new_calculation}'.")
        self.update_column(id_to_update, "calculation", new_calculation)

    def update_calculation_monitored(self, entry, monitored):
        """Update the monitored column for a given entry.

        Parameters
        ----------
        entry: int, str or list-like
            If an int or str, gives the ID or path of the calculation to
            update. If a list, it must be a list of int or str for the
            same reason. For a list, the update is done for each one.
        monitored: bool
            The new monitored status. If entry is a list, this would be
            the new monitored status for each calculation in the list.
        """
        if self.is_remote:
            # send ssh command instead
            with self.remote_client as client:
                if monitored:
                    command = "--set-monitored-true"
                else:
                    command = "--set-monitored-false"
                entries = entry
                if is_list_like(entry):
                    # update each entries
                    entries = " ".join(entry)
                client.exec_command(f"abidb {entries} {command}")
        # also do it locally
        if is_list_like(entry):
            # do it for each one
            for calc in entry:
                self.update_column(calc, "monitored", monitored)
        else:
            self.update_column(entry, "monitored", monitored)

    def update_calculation_packaged(self, entry, packaged):
        """Updates the packaged attribute of a database entry.

        Parameters
        ----------
        entry: int, str or list-like
            If an int or str, gives the ID or path of the calculation to
            update. If a list, it must be a list of int or str for the
            same reason. For a list, the update is done for each one.
        project: str
            The project tag for the entries.

        Raises
        ------
        NotImplementedError: If database is remote.
        TypeError: If packaged is not True or False.
        """
        if packaged is not True and packaged is not False:
            raise TypeError("'packaged' must be True or False.")
        if self.is_remote:
            raise NotImplementedError()
        # also do it locally
        if is_list_like(entry):
            # do it for each one
            for calc in entry:
                self.update_column(calc, "packaged", packaged)
        else:
            self.update_column(entry, "packaged", packaged)

    # TODO: rebase with the method above
    def update_calculation_project(self, entry, project):
        """Update the project tag of a given entry in the database.

        Parameters
        ----------
        entry: int, str or list-like
            If an int or str, gives the ID or path of the calculation to
            update. If a list, it must be a list of int or str for the
            same reason. For a list, the update is done for each one.
        project: str
            The project tag for the entries.
        """
        if not isinstance(project, str):
            raise TypeError("Project must be a string.")
        if self.is_remote:
            # send ssh command instead
            with self.remote_client as client:
                command = f"--set-project={project}"
                entries = entry
                if is_list_like(entry):
                    entries = " ".join(entry)
                client.exec_command(f"abidb {entries} {command}")
        # also do it locally
        if is_list_like(entry):
            # do it for each one
            for calc in entry:
                self.update_column(calc, "project", project)
        else:
            self.update_column(entry, "project", project)

    def update_calculation_status(
            self, entry, new_status=None,
            update_status=None,
            recompute=False):
        """Update one calculation status based on its path or id.

        Parameters
        ----------
        entry: int or str or list-like.
            The computation path or ID to update. If a list is given, this
            function is called for each if its item.
        new_status: dict, optional
            If dict, this should be the (complete) calculation status. If None,
            the computation status will be (re)computed if 'recompute' is True
            or nothing is stored inside the DB.
        update_status: dict, optional
            If not None, updates the previous entry with this dict. Useful
            when updating only one item of the status dict.
        """
        if is_list_like(entry):
            for item in entry:
                self.update_calculation_status(
                        item, new_status=new_status,
                        update_status=update_status, recompute=recompute)
            return
        if new_status is None:
            if recompute:
                # (re)compute status
                new_status = self._compute_new_calculation_status(entry)
            else:
                new_status = self.get_status(entry)
        if update_status is not None:
            new_status.update(update_status)
        if isinstance(new_status, dict):
            # convert into string using json
            new_status = json.dumps(new_status)
        self.update_column(entry, "status", new_status)

    def update_walltime(self, entry, new_walltime=None, force=False):
        """Updates the walltime of a calculation entry.

        Parameters
        ----------
        entry: str or int
            The calculation entry.
        new_walltime: float, optional
            If not None, states the new walltime to use. If None, it will be
            computed.
        force: bool, optional
            If new_walltime is None and force is True, then the updated
            walltime will be forced to None.
        """
        if new_walltime is not None or force:
            self.update_column(entry, "walltime", new_walltime)
            return
        with CalculationDirectory.from_calculation(
                entry, loglevel=self._loglevel) as calc:
            finished = calc.status["calculation_finished"]
            if finished is not True:
                self.update_column(entry, "walltime", None)
                return
            # compute new walltime since calc is finished
            self.update_calculation_status(
                    entry, new_status=calc.status, recompute=False)
            with calc.log_file as log:
                walltime = log.walltime
                if isinstance(walltime, float):
                    self.update_walltime(entry, new_walltime=walltime)

    def update_column(self, entry, column, value):
        """Update the attribute in a given column for a given entry.
        """
        if entry not in self:
            raise CalculationDoesNotExistError(
                    f"Calculation not in database: '{entry}'.")
        if column not in __CALCULATIONS_TABLE_COLUMNS__:
            raise ValueError(f"Invalid column name: '{column}'.")
        if column not in self.columns[__CALCULATIONS_TABLE_NAME__]:
            if self.is_remote:
                raise RuntimeError("Cannot add column for remote db.")
            self.add_columns(
                    **{column: __CALCULATIONS_TABLE_COLUMNS__[column]})
        if not isinstance(entry, int):
            entry = self[entry]
        condition = f"id == {entry}"
        self.update(condition=condition, **{column: value})

    @classmethod
    def from_remote(
            cls, hostname, *args, download=False, refresh=True, **kwargs):
        """Create an AbiDB object from a remote AbiDB. If the 'download' flag
        is set to True, the db file will be (re)downloaded.

        Parameters
        ----------
        hostname: str
            The hostname of the remote. Must be specified in the config file.
        download: bool, optional
            If True, the db file will be (re)downloaded. If False, the method
            will look into the cache dir if the db file is present. If not an
            error is raised.
        refresh: bool, optional
            If True, a refresh will be done on monitored calculations on the
            remote machine.
        """
        remotes = USER_CONFIG.REMOTES.remotes
        for remote in remotes:
            if hostname == remote["hostname"]:
                break
        else:
            hostnames = [remote["hostname"] for remote in remotes]
            raise ValueError(
                    f"'{hostname}' not in the list of hostnames: {hostnames}.")
        # hostname exists, if download is False, look into the cache dir
        db_path = cls.get_cached_db_path(hostname)
        if not download:
            if os.path.isfile(db_path):
                # cached db exists, use it
                abidb = cls.from_file(
                        db_path, *args, remote_data=remote, **kwargs)
            else:
                # cached db doesn't exist, raise error
                raise CachedAbiDBFileNotFoundError(
                        "Cached DB doesn't exist. Set 'download=True'.")
        else:
            abidb = cls(*args, remote_data=remote, **kwargs)
            abidb.download(refresh=refresh)
        return abidb

    @staticmethod
    def get_cached_db_path(hostname):
        """Returns the db path in the cache directory.
        """
        cache = USER_CONFIG.REMOTES.cache_directory
        if os.path.isfile(cache):
            raise IsAFileError(cache)
        if not os.path.isdir(cache):
            mkdir(cache)
        return os.path.join(cache, f"{hostname}.db")

    def _compute_new_calculation_status(self, entry):
        # compute new status
        # if entry is id get path
        if isinstance(entry, int):
            entry = self[entry]
        self._logger.info(f"Computing calculation status: '{entry}'.")
        # get calcdir and set new status
        with CalculationDirectory.from_calculation(entry) as calcdir:
            # compute status and update database
            return calcdir.status


class AbiDBRemoteClient(BaseRemoteClient):
    """Remote client class to handle connections to remote databases.
    """
    _check_script_exists_on_connection = "abidb"
    _loggername = "AbiDBRemoteClient"

    def download_database(self, path, refresh=False):
        """Downloads the remote database to a given path.

        Parameters
        ----------
        path: str
            The path where to put the downloaded database.
        refresh: bool, optional
            If True, a refresh will be issued on remote server to refresh
            the remote database.
        """
        with self as client:
            # get db path on remote
            stdin, stdout, stderr = client.exec_command("abidb --show-db")
            output = stdout.read().decode("utf-8").strip("\n")
            # output should look like this:
            # 'Transacting with Database: '/path/to/database''
            try:
                remote_db_path = output.split("'")[1]
            except IndexError:
                # could not get DB path for some reason even though abisuite
                # is installed and connection could be made
                # perhaps connection was shut down (e.g.: when account is
                # expired).
                raise AbiDBRuntimeError(
                    "Could not get the AbiDB file path on remote "
                    f"'{self.hostname}'")
            if refresh:
                # refresh remote db before downloading
                client.exec_command("abidb --all-monitored --refresh")
            # get sftp client and transfer file
            sftp = client.open_sftp()
            sftp.get(remote_db_path, path)
            # close sftp
            sftp.close()
