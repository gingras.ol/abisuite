from .abinit_packagers import AbinitPackager, AbinitOpticPackager
from .qe_packagers import (
        QEEPWPackager, QEMatdynPackager, QEPHPackager, QEPWPackager,
        QEQ2RPackager,
        )


__CALCTYPES_TO_PACKAGER_CLS__ = {
        "abinit": AbinitPackager,
        "abinit_optic": AbinitOpticPackager,
        "qe_epw": QEEPWPackager,
        "qe_matdyn": QEMatdynPackager,
        "qe_ph": QEPHPackager,
        "qe_pw": QEPWPackager,
        "qe_q2r": QEQ2RPackager,
        }
