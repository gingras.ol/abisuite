from ..bases import BaseInputStructure
from ....variables.qe_variables import ALL_QEDYNMAT_VARIABLES


class QEDynmatInputStructure(BaseInputStructure):
    """Structure class for a QuantumEspresso dynmat.x input file.
    """
    _variables_db = ALL_QEDYNMAT_VARIABLES
