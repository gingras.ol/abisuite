from .epsilon_launcher import QEEpsilonLauncher
from ..bases import BaseMassLauncher


class QEEpsilonMassLauncher(BaseMassLauncher):
    """The mass launcher class for Quantum Espresso calculations
    using the epsilon.x script.
    """
    _launcher_class = QEEpsilonLauncher
    _loggername = "QEEpsilonMassLauncher"
