from ..bases import BaseInputFileHandler, BaseFileWithPseudosHandler
from ...file_approvers import QEPWInputApprover
from ...file_parsers import QEPWInputParser
from ...file_structures import QEPWInputStructure
from ...file_writers import QEPWInputWriter


class QEPWInputFile(BaseInputFileHandler, BaseFileWithPseudosHandler):
    """File handler for a Quantum Espresso pw.x input file.
    """
    _approver_class = QEPWInputApprover
    _loggername = "QEPWInputFile"
    _parser_class = QEPWInputParser
    _structure_class = QEPWInputStructure
    _writer_class = QEPWInputWriter
