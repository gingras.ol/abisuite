from abisuite import PhononDispersionSequencer as Sequencer


sequencer = Sequencer("qe")

# set scf parameters
sequencer.scf_input_variables = {
    "ibrav": 2,
    "celldm(1)": 10.3472,
    "ntyp": 1,
    "nat": 2,
    "ecutwfc": 25.0,
    "conv_thr": 1e-8,
    "pseudo_dir": "../pseudos",
    "atomic_species": [{"atom": "Si",
                        "pseudo": "Si.pbe-mt_fhi.UPF",
                        "atomic_mass": 28.085, }],
    "atomic_positions": {
        "parameter": "crystal",
        "positions": {"Si": [[0.0, 0.0, 0.0], [0.25, 0.25, 0.25]]}},
    "k_points": {
        "parameter": "automatic",
        "k_points": [4, 4, 4, 0, 0, 0],
        },
    }
sequencer.scf_workdir = "scf_run"
sequencer.scf_command_arguments = "-npool 4"
sequencer.scf_mpi_command = "mpirun -np 4"

# set phonons parameters
sequencer.phonons_input_variables = {
    "amass(1)": 28.086,
    "nq1": 2,
    "nq2": 2,
    "nq3": 2,
    "ldisp": True,
    "tr2_ph": 1e-8,
    }
sequencer.phonons_workdir = "ph_run_q"

# q2r parameters
sequencer.q2r_input_variables = {}
sequencer.q2r_command_arguments = "-npool 4"
sequencer.q2r_mpi_command = "mpirun -np 4"
sequencer.q2r_workdir = "q2r_run"

# matdyn parameters
# PATH POINTS
Gamma = [0.0, 0.0, 0.0]
X = [-0.5, 0.0, 0.0]  # [-0.5, 1.0, 0.0]
W = [-0.75, -0.5, -0.25]  # [-0.75, 1.5, -0.25]
K = [-0.75, -0.5, -3/8]  # [-0.75, 1.5, -3/8]
L = [-0.5, -0.5, -0.5]  # [-0.5, 1.5, -0.5]
U = [-5/8, -0.5, -0.25]  # [-5/8, 1.5, -0.25]
# PATH
qpts = [{r"$\Gamma$": Gamma}, {"X": X}, {"W": W}, {"K": K},
        {r"$\Gamma$": Gamma}, {"L": L}, {"U": U}, {"W": W}, {"L": L}, {"K": K},
        {"U": U}, {"X": X}]
sequencer.matdyn_qpoint_path = qpts
sequencer.matdyn_qpoint_path_density = 20
sequencer.matdyn_input_variables = {
        "asr": "simple",
        }
sequencer.matdyn_command_arguments = "-npool 4"
sequencer.matdyn_mpi_command = "mpirun -np 4"
sequencer.matdyn_workdir = "matdyn_run"

# launch sequencer
sequencer.launch()
