from ..bases import BaseStructure


class AbinitEIGStructure(BaseStructure):
    """Structure for a '_EIG' file created by Abinit.
    """
    all_attributes = ("k_points", "eigenvalues")
