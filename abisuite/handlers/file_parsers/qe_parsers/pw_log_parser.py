from .bases import BaseQELogParser
from .subparsers.pw_log_subparsers import (
                ParallelizationSubParser,
                EigenvaluesSubParser,
                EnergySubParser,
                QEPWLogCrystalAxesSubParser,
                QEPWLogLatticeParametersSubParser,
                QEPWLogPressureStressSubParser,
                QEPWLogFermiEnergySubParser,
                QEPWLogFinalAtomicPositionsSubParser,
                QEPWLogFinalGeometrySubParser,
                QEPWLogInputParametersSubParser,
                QEPWLogTimingSubParser, KGridSubParser,
                AtomicPositionsSubParser
                )
from ...file_structures import QEPWLogStructure


class QEPWLogParser(BaseQELogParser):
    """A Quantum Espresso log file parser that extracts data from a
    log file.
    """
    _loggername = "QEPWLogParser"
    _structure_class = QEPWLogStructure
    _subparsers = (
            ParallelizationSubParser, EnergySubParser,
            EigenvaluesSubParser, QEPWLogCrystalAxesSubParser,
            QEPWLogLatticeParametersSubParser, QEPWLogFermiEnergySubParser,
            QEPWLogPressureStressSubParser,
            QEPWLogFinalAtomicPositionsSubParser,
            QEPWLogFinalGeometrySubParser,
            QEPWLogInputParametersSubParser, QEPWLogTimingSubParser,
            KGridSubParser,
            AtomicPositionsSubParser,
            )
