import os

from .bases import PathConvertibleBaseStructure


class SymLinkStructure(PathConvertibleBaseStructure):
    all_attributes = ("source", )
    convertible_path_attributes = ("source", )
    relative_to_path_attribute = "path"

    def __eq__(self, other):
        # override this method since on windows, reading links is treacherous
        if not isinstance(other, self.__class__):
            raise TypeError("Cannot compare two different classes: "
                            f"{self.__class__}, {other.__class__}")
        for attr in ("source", ):
            a1 = getattr(self, attr)
            a2 = getattr(other, attr)
            exists = [os.path.exists(x) for x in (a1, a2)]
            if any(exists) and not all(exists):
                return False
            if not all(exists):
                # no source exists
                return a1 == a2
            # else, both path exists, check they point to same file/dir
            if not os.path.samefile(a1, a2):
                return False
        return True

    @property
    def source(self):
        if self._source is not None:
            return self._source
        raise ValueError("Need to set 'source' before using it.")

    @source.setter
    def source(self, source):
        if os.path.islink(source):
            source = os.readlink(source)
        self._source = source

    @property
    def source_is_directory(self):
        return os.path.isdir(self.source)
