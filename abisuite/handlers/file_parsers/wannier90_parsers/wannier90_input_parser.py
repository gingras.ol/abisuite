from ..bases import BaseInputParser
from ...file_structures import Wannier90InputStructure
from ....routines import decompose_line


class Wannier90InputParser(BaseInputParser):
    """Class that can parse a wannier90.x input File.
    """
    _expected_ending = ".win"
    _loggername = "Wannier90InputParser"
    _structure_class = Wannier90InputStructure

    def _extract_data(self):
        super()._extract_data(ignore_fortran_comments=True)

    def _extract_data_from_lines(self, lines):
        data = {}
        skip = 0
        for nline, line in enumerate(lines):
            if nline < skip:
                continue
            if "=" in line or ":" in line:
                # regular variable
                name, value = self._extract_regular_variable(line)
                data[name] = value
                continue
            if "begin unit_cell" in line.lower():
                data["unit_cell"], toskip = self._extract_unit_cell(
                                lines[nline:])
                skip = nline + toskip
                continue
            if "begin atoms" in line.lower():
                (data["atomic_positions"],
                 toskip) = self._extract_atomic_positions(
                                 lines[nline:])
                skip = nline + toskip
                continue
            if "begin projections" in line.lower():
                data["projections"], toskip = self._extract_projections(
                                lines[nline:])
                skip = nline + toskip
                continue
            if "begin kpoint_path" in line.lower():
                (data["kpoint_path"],
                 toskip) = self._extract_kpoint_path(lines[nline:])
                skip = nline + toskip
                continue
            if ":" in line:
                data["mp_grid"] = self._extract_mp_grid(line)
                continue
            if "begin kpoints" in line.lower():
                (data["kpoints"],
                 toskip) = self._extract_kpoints(lines[nline:])
                continue
        return {"input_variables": data}

    def _extract_kpoints(self, lines):
        kpts = []
        for nline, line in enumerate(lines[1:]):
            if "end" in line.lower():
                return kpts, nline + 1
            s, i, f = decompose_line(line)
            kpts.append(f)
        else:
            raise LookupError(f"Could not find the end of 'kpoints' block in "
                              f"{self.path}")

    def _extract_kpoint_path(self, lines):
        # kpt path is a list of lists containing 2 dict each representing a
        # kpt coordinate
        data = []
        for nline, line in enumerate(lines[1:]):
            line = line.strip(" ")
            if not len(line):
                # empty line => skip
                continue
            if "end" in line:
                return data, nline + 1
            s, i, f = decompose_line(line)
            data.append([{s[0]: f[:3]}, {s[1]: f[3:]}])
        else:
            raise LookupError()

    def _extract_projections(self, lines):
        # first line is begin
        data = []
        for nline, line in enumerate(lines[1:]):
            if "end" in line:
                return data, nline + 1
            data.append(line)
        else:
            raise LookupError()

    def _extract_atomic_positions(self, lines):
        # first line is parameter
        data = {}
        s, i, f = decompose_line(lines[0])
        data["parameter"] = s[-1]
        toskip = 1
        positions = {}
        for line in lines[1:]:
            if "end" in line.lower():
                data["positions"] = positions
                return data, toskip
            toskip += 1
            s, i, f = decompose_line(line)
            atom = s[0]
            positions.setdefault(atom, [])
            positions[atom].append(f)
        else:
            raise LookupError()

    def _extract_unit_cell(self, lines):
        # first line is the parameter
        data = {}
        s, i, f = decompose_line(lines[0])
        data["parameter"] = s[-1]
        # second line is unit if there is some
        s, i, f = decompose_line(lines[1])
        start_of_vectors = 1
        if len(s) and not len(f):
            start_of_vectors = 2
            data["units"] = s[0]
        # rest of the lines are unit cell vectors
        vectors = []
        for nline, line in enumerate(lines[start_of_vectors:]):
            if "end" in line.lower():
                data["vectors"] = vectors
                return data, nline + 2
            s, i, f = decompose_line(line)
            vectors.append(f)
        else:
            raise LookupError(f"Could not find end of unit cell block in: "
                              f"{self.path}")

    def _extract_regular_variable(self, line):
        # regular variable is something like:
        # dis_win_min     = 4.0
        if "=" in line:
            split = line.split("=")
        elif ":" in line:
            split = line.split(":")
        name = split[0].strip(" ")
        s, i, f = decompose_line(split[1])
        if name == "mp_grid":
            # exception since this is a vector
            return name, i
        if len(f):
            return name, f[0]
        if len(i):
            return name, i[0]
        if "true" in s[0]:
            return name, True
        if "false" in s[0]:
            return name, False
        # just a regular string
        return name, s[0]
