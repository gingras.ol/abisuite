import os

from .bases import BaseQELinker


class QEDynmatfromQEPHLinker(BaseQELinker):
    """Linker class to link a qe_q2r calculation to a qe_matdyn calculation
    from Quantum Espresso.
    """
    _calctype = "qe_dynmat"
    _link_calctype = "qe_ph"
    _loggername = "QEDynmatfromQEPHLinker"

    def _link_files_from_calculation(self, metadata):
        # only need to link the '.dyn' file from a previous ph.x calculation
        outdir = metadata.output_data_dir
        self.add_input_file(os.path.join(outdir, "*.dyn*"))
