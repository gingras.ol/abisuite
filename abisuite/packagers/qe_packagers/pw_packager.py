from .bases import BaseQEPackager


class QEPWPackager(BaseQEPackager):
    """Packager class for a pw.x calculation by Quantum Espresso.
    """
    _calctype = "qe_pw"
    _loggername = "QEPWPackager"

    def keep_file(self, *args, **kwargs):
        return super().keep_file(*args, **kwargs)
