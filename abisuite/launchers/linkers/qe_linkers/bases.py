import os

from ..bases import BaseLinker
from ....handlers import PBSFile, QEPHInputFile


class BaseQELinker(BaseLinker):
    """Base linker class for all Quantum Espresso linkers.
    """

    def _search_for_save_dir(self, outdir):
        # search for the ".save" directory in the output data directory.
        for subfile in os.listdir(outdir):
            if subfile.endswith(".save"):
                # found it
                return os.path.join(outdir, subfile)
        raise FileNotFoundError("Could not find the 'save' directory"
                                f" in {outdir}...")


class BasePostQEPWLinker(BaseQELinker):
    """Base Linker class for a calculation that are linked with a qe_pw
    calculation.
    """
    def _link_files_from_calculation(self, metadata):
        """Link a previous pw calculation to this launcher.
        The relevant files will be linked.
        """
        outdir = metadata.output_data_dir
        # find the name of the 'save' directory
        savedirpath = self._search_for_save_dir(outdir)
        # need to link everything in the '.save' output directory
        self.launcher.add_input_file(savedirpath, keep_original_filename=True)


class BasePostQEPHLinker(BaseQELinker):
    """Base Linker class for calculation that are linked with a qe_ph
    calculation.
    """

    def _link_files_from_calculation(self, metadata):
        """Link a previous ph calculation to this launcher.
        The relevant files will be linked.
        """
        # only need to link the 'dyn' files
        outdir = metadata.output_data_dir
        self.launcher.add_input_file(os.path.join(outdir, "*.dyn*"))
        # check if previous run needs to be recovered
        # it is the case if -nimage > 1 and recover = False
        with PBSFile.from_file(
                metadata.pbs_file_path,
                loglevel=self._loglevel) as pbs:
            nimage = pbs.command_arguments.get("-nimage", 1)
        with QEPHInputFile.from_file(
                metadata.input_file_path,
                loglevel=self._loglevel) as inputfile:
            recover = inputfile.input_variables.get("recover", False)
        if nimage > 1 and not recover:
            raise ValueError(f"Need to make a recovery run of the previous PH"
                             f" run first since -nimage = {nimage} > 1 and "
                             f" recover = {recover} != True")
