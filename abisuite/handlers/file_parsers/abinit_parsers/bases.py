import abc
import os

from .subparsers import VariableSubParser
from ..bases import (
        BaseFormattedCalculationFileParser, BaseInputParser,
        BaseCalculationFileParser,
        )
from ....routines import decompose_line, is_list_like


class BaseFilesParser(BaseFormattedCalculationFileParser, abc.ABC):
    """Base class for files file parsers.
    """
    _expected_ending = ".files"

    @classmethod
    def _filepath_from_meta(cls, meta):
        # files file usually in rundir
        return os.path.join(meta.rundir, meta.jobname + cls._expected_ending)

    def _extract_data(self):
        super()._extract_data(
                strip_spaces=True, strip_newlines=True,
                ignore_python_comments=True,
                ignore_empty_lines=True)


class BaseAbinitDMFTParser(BaseFormattedCalculationFileParser):
    """Base class for an Abinit DMFT file parser.
    """

    @classmethod
    def _filepath_from_meta(cls, meta):
        out = meta.output_data_dir
        # look for an .eig file
        for outfile in os.listdir(out):
            if outfile.endswith(cls._expected_ending):
                return os.path.join(out, outfile)
        raise FileNotFoundError(f"Could not locate DMFT file in {out}")


class BaseAbinitInputParser(BaseInputParser):
    """Base class for some of the abinit input parser classes.
    """
    _expected_ending = (".in", ".abi", )

    def _extract_data(self, *args, **kwargs):
        super()._extract_data(
                ignore_python_comments=True,
                strip_spaces=True, strip_newlines=True,
                ignore_empty_lines=True)

    def _extract_data_from_lines(self, lines):
        data = {}
        next_end = 0
        for i, line in enumerate(lines):
            if i < next_end:
                continue
            if VariableSubParser.variable_def_start_here(line):
                var = VariableSubParser(lines[i:],
                                        loglevel=self._logger.level)
                next_end += var.ending_relative_index
                data[var.name] = var.value
        return {"input_variables": data}


class BaseAbinitNCParser(BaseFormattedCalculationFileParser, abc.ABC):
    """Base parser class for abinit '.nc' files.

    A particularity of abinit is that some files can be written with netcdf or
    not (user's choice). We thus need to check if they are written that way
    or not. Files written by netcdf will be happended by the .nc extension.

    Note: the '_expected_ending' class attribute should not contain the '.nc'
          extention.
    """
    @classmethod
    def _filepath_from_meta(cls, meta, *args, **kwargs):
        # the EIG file is located inside the output data dir.
        endings = cls._expected_ending
        if not is_list_like(endings):
            endings = (endings, )
        for filename in os.listdir(meta.output_data_dir):
            for ending in endings:
                if filename.endswith(ending) or filename.endswith(
                        ending + ".nc"):
                    return os.path.join(meta.output_data_dir, filename)
        raise FileNotFoundError(
                f"Could not find the {cls._expected_ending} "
                f"file for '{meta.workdir}'.")

    def _extract_data(self):
        if not self.path.endswith(".nc"):
            return super()._extract_data()
        # else we need to use netcdf module
        self._set_attributes_from_data(
                self._extract_data_from_netcdf())

    def _extract_data_from_netcdf(self):
        try:
            from netCDF4 import Dataset
        except ImportError as e:
            self._logger.error(
                "netCDF4 module must be installed in order to read"
                "netCDF4 files. Please do so in order to continue.")
            self._logger.exception(e)
            raise e
        with Dataset(self.path, "r", format="NETCDF4") as dtset:
            return self._extract_data_from_netcdf_dataset(dtset)

    @abc.abstractmethod
    def _extract_data_from_netcdf_dataset(self, dataset):
        pass


class BaseAbinitDOSParser(BaseCalculationFileParser, abc.ABC):
    """Base parser class for DOS files.
    """

    def _extract_data(self):
        data = {}
        data.update(self._get_header_data())
        data.update(self._get_core_data())
        self._set_attributes_from_data(data)

    @abc.abstractmethod
    def _get_core_data(self, *args, **kwargs):
        pass

    def _get_header_data(self):
        # once we hit a 'non-comment' line we've reached the end of header
        data = {}
        with open(self.path, "r") as f:
            for line in f:
                if not line.startswith("#"):
                    return data
                if "Fermi" in line:
                    s, i, f = decompose_line(
                            line.strip("#").strip("\n").strip())
                    data["fermi_energy"] = f[0]
        # if we're here there was a problem
        raise LookupError(self.path)
