import abc

from .band_structure_sequencer import QEBandStructureSequencer
from .individual_phonon_sequencers import QEIndividualPhononSequencer
from .phonon_dispersion_sequencer import QEPhononDispersionSequencer
from ..bases import SequenceCalculation, BaseSequencer
from ..nscf_sequencers import PositiveKpointsNSCFSequencer
from ...constants import mEV_TO_CM
from ...exceptions import DevError
from ...handlers import CalculationDirectory
from ...status_checkers.exceptions import CalculationNotFinishedError
from ...post_processors import (
        BandStructure, PhononDispersion, ConductivityTensor, Resistivity,
        )
from ...routines import is_list_like


class BaseQEEPWSequencer(
        QEBandStructureSequencer, PositiveKpointsNSCFSequencer, abc.ABC,
        ):
    """Base class for a QEEPWSequencer. It uses a bandstructure sequencer and a
    positive kpt nscf sequencer.
    """
    def __init__(self, *args, **kwargs):
        """Call both of the mother classes' init methods.
        """
        self._init_phonons_sequencer(*args, **kwargs)
        QEBandStructureSequencer.__init__(self, *args, **kwargs)
        PositiveKpointsNSCFSequencer.__init__(self, "qe", *args, **kwargs)
        if "epw_" not in self._all_sequencer_prefixes:
            raise DevError(
                    "'epw_' must be set in '_all_sequencer_prefixes'.")

    @property
    def epw_input_variables(self):
        return self._epw_input_variables

    @epw_input_variables.setter
    def epw_input_variables(self, input_vars):
        if not isinstance(input_vars, dict):
            raise TypeError(
                    "Expected a dict for input vars but got: "
                    f"{input_vars}")
        # set kpoint path for wannierization according to values
        # in band structure calculation
        kpts_path = []

        def replace_latex_chars(label, latex_symbol, replace_with):
            if label.startswith("$" + latex_symbol):
                return label.replace(
                        "$" + latex_symbol, replace_with).strip("$")
            return label

        # add kpoint path to input variables. For EPW, they are given through
        # the variables 'wdata(x)' that are passed directly to wannier90
        for i, kpoint in enumerate(self.band_structure_kpoint_path[:-1]):
            label1 = list(kpoint.keys())[0]
            coords1 = kpoint[label1]
            label2 = list(self.band_structure_kpoint_path[i + 1].keys())[0]
            coords2 = self.band_structure_kpoint_path[i + 1][label2]
            for latex_symbol, replacement in zip(
                            (r"\Gamma", r"\Sigma"), ("G", "S")):
                label1 = replace_latex_chars(label1, latex_symbol, replacement)
                label2 = replace_latex_chars(label2, latex_symbol, replacement)
            coords1 = " ".join([str(round(x, 5)) for x in coords1])
            coords2 = " ".join([str(round(x, 5)) for x in coords2])
            kpts_path.append(f"{label1} {coords1} {label2} {coords2}")
        n_wdata = 0
        set_bands_plot = True
        for var, value in input_vars.items():
            if var.startswith("wdata("):
                n_wdata += 1
                wdata = value.lower()
                if "bands_plot" in wdata and "true" in wdata:
                    set_bands_plot = False
        input_vars[f"wdata({n_wdata + 1})"] = "begin kpoint_path"
        for i, kpt in enumerate(kpts_path):
            input_vars[f"wdata({n_wdata + 2 + i})"] = kpt
        input_vars[
                f"wdata({n_wdata + 2 + len(kpts_path)})"] = "end kpoint_path"
        if set_bands_plot:
            input_vars[
                f"wdata({n_wdata + 3 + len(kpts_path)})"] = "bands_plot = True"
        # set the nscf kpoint grid
        for i in range(1, 4):
            if f"nk{i}" in input_vars:
                self._logger.debug("Overwriting 'nkX' to match NSCF "
                                   "calculation.")
            input_vars[f"nk{i}"] = self.nscf_kgrid[i - 1]
        self._epw_input_variables = input_vars

    def init_sequence(self, *args, **kwargs):
        # SCF, Phonons and band structure are set in mother classes
        # first start with SCF + PHONONS
        self._init_phonons_sequence(*args, **kwargs)
        # Start with Band Structure
        QEBandStructureSequencer.init_sequence(self, *args, **kwargs)
        # then do NSCF run
        PositiveKpointsNSCFSequencer.init_sequence(self, *args, **kwargs)
        # the sequencer contains 2 times the same scf calculation but its
        # fine it will just double check it for nothing (no big deal)
        phonons_calc = self.get_sequence_calculation("phonons")
        # make sure it's iterable since we iterate over it!
        if not is_list_like(phonons_calc):
            phonons_calc = (phonons_calc, )
        nscfcalc = self.get_sequence_calculation("nscf")
        epw_calc = SequenceCalculation(
                "qe_epw", self.epw_workdir, self.epw_input_variables,
                self.epw_calculation_parameters, loglevel=self._loglevel)
        epw_calc.dependencies += [nscfcalc] + [x for x in phonons_calc]
        self.sequence.append(epw_calc)

    def plot_band_structure(self):
        # Plot the band structure with the wannier interpolation on top of it.
        # pop the plot parameters to make sure the bs plot is not shown/saved
        show = self.plot_calculation_parameters.pop("show", True)
        self.plot_show = False
        save = self.plot_calculation_parameters.pop("save", None)
        self.plot_save = None
        # same with save pickle
        save_pickle = self.plot_calculation_parameters.pop("save_pickle", None)
        self.plot_save_pickle = None
        # W90 prints it's own path density we need to adjust axis
        # FIXME: This is disabled for now as I think it only works for cubic
        # systems
        # self.plot_calculation_parameters["adjust_axis"] = True
        self.plot_calculation_parameters["adjust_axis"] = False
        dft_plot = super().plot_band_structure(name_extension="band_structure")
        # reset parameters
        self.plot_show = show
        self.plot_save = save
        self.plot_save_pickle = save_pickle
        self._logger.info("Plotting Wannier Interpolation...")

        labels = self._get_band_structure_labels()
        n = self.band_structure_kpoint_path_density
        all_k_labels = [[label, coord]
                        for label, coord in zip(
                            labels, range(0, len(labels) * (n + 1), n))]
        # last kpt not included usually
        all_k_labels[-1][1] -= 1
        wannier_bs = BandStructure.from_calculation(
                self.epw_workdir, loglevel=self._loglevel)
        adjust = self.plot_calculation_parameters["adjust_axis"]
        if self.epw_input_variables.get("filkf", None) is not None:
            wannier_plot = wannier_bs.get_plot(
                    yunits="eV",
                    ylabel="Energy",
                    color="r",
                    linewidth=2,
                    symmetry="none",
                    adjust_axis=adjust,
                    all_k_labels=all_k_labels)
        else:
            wannier_plot = wannier_bs.get_plot(
                    yunits="eV",
                    ylabel="Energy",
                    color="r",
                    linewidth=2,
                    symmetry="none",
                    adjust_axis=adjust,
                    all_k_labels="none")
        final_plot = dft_plot + wannier_plot
        # apply other properties to plot
        for param, value in self.plot_calculation_parameters.items():
            if param in ("show", "save"):
                # postfix parameters
                continue
            setattr(final_plot, param, value)
        final_plot.curves[0].label = "dft"
        final_plot.curves[-1].label = "wannier"
        self._post_process_plot(
                final_plot,
                name_extension="band_structure_with_wannier_interpolation")
        return final_plot

    @abc.abstractmethod
    def _init_phonons_sequence(self, *args, **kwargs):
        pass

    @abc.abstractmethod
    def _init_phonons_sequencer(self, *args, **kwargs):
        pass


class QEEPWSequencer(BaseQEEPWSequencer, QEIndividualPhononSequencer):
    """Sequencer for an EPW calculation including wannier interpolation
    of the band structure. Use this for a one shot epw calculation.
    """
    _all_sequencer_prefixes = (
            "scf_", "phonons_", "nscf_", "band_structure_", "epw_",
            "plot_")
    _loggername = "QEEPWSequencer"

    def _init_phonons_sequence(self, *args, **kwargs):
        QEIndividualPhononSequencer.init_sequence(self, *args, **kwargs)

    def _init_phonons_sequencer(self, *args, **kwargs):
        QEIndividualPhononSequencer.__init__(self, *args, **kwargs)


class QEEPWWithPhononInterpolationSequencer(
        BaseQEEPWSequencer, QEPhononDispersionSequencer):
    """Same as QEEPWSequencer but also produces matdyn and q2r run in order to
    get the phonon dispersion as well as the phonon interpolation of EPW to
    compare.
    """
    _all_sequencer_prefixes = (
            "scf_", "phonons_", "matdyn_", "nscf_", "band_structure_", "epw_",
            "plot_", "q2r_")
    _loggername = "QEEPWWithPhononInterpolationSequencer"

    @BaseQEEPWSequencer.epw_input_variables.setter
    def epw_input_variables(self, invars):
        BaseQEEPWSequencer.epw_input_variables.fset(self, invars)
        # make sure band_plot is set to True
        self.epw_input_variables["band_plot"] = True

    def init_sequence(self, *args, **kwargs):
        BaseQEEPWSequencer.init_sequence(self, *args, **kwargs)
        # link the matdyn and band_structure calculation to epw to generate
        # the filqf and filkf files.
        if self._nphonons is None:
            # nphonons not yet determined
            return
        epwseq = self.get_sequence_calculation("epw")
        q2rseq = self.get_sequence_calculation("q2r")
        matdynseq = self.get_sequence_calculation("matdyn")
        bsseq = self.get_sequence_calculation("band_structure")
        epwseq.dependencies += [q2rseq, matdynseq, bsseq]

    def plot_phonon_dispersion(self):
        # Plot the phonon dispersion with the wannier interpolation on
        # top of it.
        # pop the plot parameters to make sure the bs plot is not shown/saved
        show = self.plot_calculation_parameters.pop("show", True)
        self.plot_show = False
        save = self.plot_calculation_parameters.pop("save", None)
        self.plot_save = None
        save_pickle = self.plot_calculation_parameters.pop("save_pickle", None)
        self.plot_save_pickle = None
        # EPW prints phonon wavevectors in cartesian coordinates
        # don't shrink/expand qpts axis
        self.plot_calculation_parameters["adjust_axis"] = False
        phdisp_plot = super().plot_phonon_dispersion()
        # reset parameters
        self.plot_show = show
        self.plot_save = save
        self.plot_save_pickle = save_pickle
        # # nevermind the rest for now
        wannier_phdisp = PhononDispersion.from_calculation(
                self.epw_workdir, loglevel=self._loglevel)
        # EPW usually prints eig freqs in meV. Transfer to CM-1
        wannier_phdisp.frequencies *= mEV_TO_CM
        wannier_plot = wannier_phdisp.get_plot(
                yunits=r"cm$^{-1}$",
                ylabel="Energy",
                color="r",
                linewidth=2,
                symmetry="none",
                all_k_labels="none",
                adjust_axis=self.plot_calculation_parameters["adjust_axis"])
        final_plot = phdisp_plot + wannier_plot
        final_plot.curves[0].label = "dft"
        final_plot.curves[-1].label = "wannier"
        final_plot = self._post_process_plot(
                final_plot,
                name_extension="phonon_dispersion_with_wannier_interpolation"
                )
        return final_plot

    def _init_phonons_sequence(self, *args, **kwargs):
        QEPhononDispersionSequencer.init_sequence(self, *args, **kwargs)

    def _init_phonons_sequencer(self, *args, **kwargs):
        QEPhononDispersionSequencer.__init__(self, *args, **kwargs)


class _BasePostQEEPWSequencer(BaseSequencer, abc.ABC):
    """Base class for post EPW interpolation sequencers.

    This is a base class and is not meant to be directly instanciated.
    """
    _mandatory_input_variables = None
    _input_variables_should_be_there = None
    _post_qe_epw_prefix = None

    def __init__(self, *args, **kwargs):
        super().__init__("qe", *args, **kwargs)
        self._epw_interpolation_calculation = None
        if self._post_qe_epw_prefix is None:
            raise DevError("Need to set '_post_qe_epw_prefix'.")
        if not self._post_qe_epw_prefix.endswith("_"):
            raise DevError("Need '_post_qe_epw_prefix' to end with '_'.")
        if self._mandatory_input_variables is None:
            raise DevError("Need to set '_mandatory_input_variables'.")
        if self._input_variables_should_be_there is None:
            raise DevError("Need to set '_input_variables_should_be_there'.")

    @property
    def epw_interpolation_calculation(self):
        if self._epw_interpolation_calculation is None:
            raise ValueError("Needs to set 'epw_interpolation_calculation'.")
        return self._epw_interpolation_calculation

    @epw_interpolation_calculation.setter
    def epw_interpolation_calculation(self, epw):
        with CalculationDirectory.from_calculation(
                epw, loglevel=self._loglevel) as calc:
            if calc.status["calculation_finished"] is not True:
                raise CalculationNotFinishedError(epw)
            self._epw_interpolation_calculation = calc.path

    def init_sequence(self, *args, **kwargs):
        super().init_sequence(*args, **kwargs)
        self.init_post_qe_epw_interpolation_sequence()

    def init_post_qe_epw_interpolation_sequence(self, *args, **kwargs):
        postseq = SequenceCalculation(
                "qe_epw",
                getattr(self, f"{self._post_qe_epw_prefix}workdir"),
                getattr(self, f"{self._post_qe_epw_prefix}input_variables"),
                getattr(
                    self, f"{self._post_qe_epw_prefix}calculation_parameters"),
                loglevel=self._loglevel)
        postseq.recover_from = self.epw_interpolation_calculation
        postseq.recover_from_update_variables = {
                 "epbwrite": False,
                 "epbread": False,
                 "epwwrite": False,
                 "epwread": True,
                 "wannierize": False,
                }
        postseq.recover_from_pop_variables = ["filqf", "filkf", "band_plot"]
        for var in ("q", "k"):
            if postseq.input_variables.get(f"rand_{var}", False) is True:
                for nk in (f"n{var}f1", f"n{var}f2", f"n{var}f3"):
                    postseq.recover_from_pop_variables.append(nk)
        self.sequence.append(postseq)

    def post_sequence(self, *args, **kwargs):
        super().post_sequence(*args, **kwargs)
        self.plot_transport()

    def _set_mandatory_input_variables(self, input_vars):
        if not isinstance(input_vars, dict):
            raise TypeError(
                    "Expected a dict for input vars but got: "
                    f"{input_vars}")
        for varname, varvalue in self._mandatory_input_variables.items():
            self._set_input_var_to(input_vars, varname, varvalue)
        # make sure some input vars are set
        for varname in self._input_variables_should_be_there:
            if varname not in input_vars:
                raise ValueError(
                        f"'{varname}' should be in the input variables.")

    @abc.abstractmethod
    def plot_transport(self):
        pass


class QEEPWIBTESequencer(_BasePostQEEPWSequencer):
    """Simple sequencer that requires to be launched after a full QEEPW
    sequencer has completed the interpolation.

    This sequencer is not a continuation of the epw sequencer since normally
    we want to check the interpolation before continuing.
    """
    _all_sequencer_prefixes = ("ibte_", "plot_", )
    _post_qe_epw_prefix = "ibte_"
    _mandatory_input_variables = {
                "a2f": False,
                "int_mob": True,
                "iterative_bte": True,
                "scattering": True,
                "scattering_serta": True,
                "carrier": False,
                "phonselfen": False,
                "elph": True,
                "etf_mem": 0,
                "mp_mesh_k": True,
                "ephwrite": False,
                "restart": True,
                }
    _input_variables_should_be_there = (
                "assume_metal", "restart_step", "etf_mem", "degaussw",
                "temps", "nstemp", "vme",
                "nkf1", "nkf2", "nkf3", "nqf1", "nqf2", "nqf3", "fsthick",
                "ngaussw",
                )
    _loggername = "QEIBTESequencer"

    @property
    def ibte_input_variables(self):
        return self._ibte_input_variables

    @ibte_input_variables.setter
    def ibte_input_variables(self, input_vars):
        self._set_mandatory_input_variables(input_vars)
        self._ibte_input_variables = input_vars

    def plot_transport(self):
        # make multiple plots. one for resistivity, conductivity, with serta
        # on both as well and each component on each
        # get each component to plot, by default it's all of them
        elements = self.plot_calculation_parameters.get("elements", None)
        with CalculationDirectory.from_calculation(
                self.ibte_workdir, loglevel=self._loglevel) as calc:
            log = calc.log_file.path
        conductivity_tensor = ConductivityTensor.from_file(log)
        for serta in [True, False]:
            for resistivity, factor, yunits, title, name in zip(
                    [True, False],
                    [1e6, 1e-6],
                    [r"$\mu\Omega$cm", r"$[\mu\Omega$cm]$^{-1}$"],
                    ["Resistivity tensor", "Conductivty tensor"],
                    ["resistivity", "conductivity"]):
                if serta:
                    title += " (SERTA)"
                    name += "_serta"
                else:
                    title += " (IBTE)"
                    name += "_ibte"
                plot = conductivity_tensor.get_plot(
                        resistivity=resistivity, conversion_factor=factor,
                        xunits="K", yunits=yunits, linestyle="-",
                        linewidth=2, elements=elements, serta=serta,
                        title=title,
                        )
                self._post_process_plot(plot, name_extension=name)


class QEEPWZimanSequencer(_BasePostQEEPWSequencer):
    """Simple sequencer that requires to be launched after a full QEEPW
    sequencer has completed the interpolation. Used for Ziman resistivity
    calculations.

    This sequencer is not a continuation of the epw sequencer since normally
    we want to check the interpolation before continuing.
    """
    _all_sequencer_prefixes = ("ziman_", "plot_", )
    _post_qe_epw_prefix = "ziman_"
    _loggername = "QEZimanSequencer"
    _mandatory_input_variables = {
                "a2f": True,
                "phonselfen": True,
                }
    _input_variables_should_be_there = (
                "degaussw", "ngaussw",
                "vme",
                "fsthick",
                )

    @property
    def ziman_input_variables(self):
        return self._ziman_input_variables

    @ziman_input_variables.setter
    def ziman_input_variables(self, input_vars):
        self._set_mandatory_input_variables(input_vars)
        self._ziman_input_variables = input_vars

    def plot_transport(self):
        resistivity = Resistivity.from_calculation(
                self.ziman_workdir, loglevel=self._loglevel)
        plot = resistivity.get_plot()
        self._post_process_plot(plot, name_extension="ziman_resistivity")
