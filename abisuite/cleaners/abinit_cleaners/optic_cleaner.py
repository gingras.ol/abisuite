from ..bases import BaseCleaner


class AbinitOpticCleaner(BaseCleaner):
    """Cleaner class for an optic calculation from the abinit
    software.
    """
    _calctype = "abinit_optic"
    _loggername = "AbinitOpticCleaner"

    def delete_this_file(self, handler):
        for suffix in ("_1WF", "_WFK", ):
            if suffix in handler.basename:
                return True
        return super().delete_this_file(handler)
