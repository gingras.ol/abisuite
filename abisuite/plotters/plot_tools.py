import os

from matplotlib import colors
from matplotlib.backend_tools import ToolToggleBase
import tkinter
from tkinter.colorchooser import askcolor
from tkinter.simpledialog import askinteger

from ..routines import suppress_warnings


IMAGES_DIR = os.path.join(os.path.dirname(__file__), "tool_buttons_images")


class BaseCustomToggleTool(ToolToggleBase):
    """Just for rebasing same methods.
    """
    def enable(self, event):
        self.visibility(True)

    def disable(self, event):
        self.visibility(False)


# Taken from:
# https://github.com/fariza/pycon2017/blob/master/toolmanager.pdf
class ToggleLegend(BaseCustomToggleTool):
    description = "Turn on/off the legend."
    default_toggled = True
    image = os.path.join(IMAGES_DIR, "toggle_legend.png")

    def visibility(self, state):
        for leg in list(self.figure.legends):
            leg.set_visible(state)
        for a in self.figure.get_axes():
            leg = a.get_legend()
            if leg:
                leg.set_visible(state)
        self.figure.canvas.draw_idle()


class ToggleGrid(BaseCustomToggleTool):
    description = "Turn on/off grid."
    default_toggled = False
    image = os.path.join(IMAGES_DIR, "toggle_grid.png")

    def visibility(self, state):
        # set the grid on the main axis
        self.figure.axes[0].grid(b=state)
        self.figure.canvas.draw_idle()


# taken from:
# https://github.com/fariza/pycon2017/blob/master/code/5-color.py
class CurveColor(ToolToggleBase):
    description = "Change line color."
    image = os.path.join(IMAGES_DIR, "curve_color.png")

    @suppress_warnings
    def __init__(self, *args, **kwargs):
        self.pid = None
        super().__init__(*args, **kwargs)
        self.converter = colors.ColorConverter()

    def enable(self, *args, **kwargs):
        self.toolmanager.message_event("enable")
        self.pid = self.figure.canvas.mpl_connect("pick_event", self.onpick)

    def disable(self, *args, **kwargs):
        self.figure.canvas.mpl_disconnect(self.pid)
        self.pid = None

    def onpick(self, event):
        thisline = event.artist
        original = thisline.get_color()
        if original is None:
            original = thisline.get_markerfacecolor()
        original = colors.rgb2hex(original)
        color = askcolor(original)[1]
        thisline.set_color(color)
        # update legend too
        line_label = thisline.get_label()
        figure = event.artist.get_figure()
        legends = figure.axes[0].get_legend()
        if legends is not None:
            for legend in legends.legendHandles:
                if legend.get_label() == line_label:
                    legend.set_color(color)
                    break
        self.figure.canvas.draw()


class TickFontSize(ToolToggleBase):
    description = "Choose font size for ticks."

    def enable(self, *args, **kwargs):
        self.toolmanager.message_event("enable")
        widget = tkinter.Tk()
        value = askinteger(
                "Choose font size for axes:", "Font size:", parent=widget)
        widget.destroy()
        for ax in self.figure.axes:
            for item in ax.get_xticklabels() + ax.get_yticklabels():
                item.set_fontsize(value)
        self.figure.canvas.draw()
        self.disable()


class LabelFontSize(ToolToggleBase):
    description = "Choose font size for labels."

    def enable(self, *args, **kwargs):
        self.toolmanager.message_event("enable")
        widget = tkinter.Tk()
        value = askinteger(
                "Choose font size for axes:", "Font size:", parent=widget)
        widget.destroy()
        for ax in self.figure.axes:
            ax.set_xlabel(ax.get_xlabel(), fontsize=value)
            ax.set_ylabel(ax.get_ylabel(), fontsize=value)
        self.figure.canvas.draw()
        self.disable()
