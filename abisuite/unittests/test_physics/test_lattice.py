from abisuite.physics import Lattice
from ..bases import TestCase
from ..routines_for_tests import TemporaryCalculation
from ..variables_for_tests import QEPWCalc


class TestLattice(TestCase):
    """Test case for a Lattice object.
    """
    def setUp(self):
        self._qepwcalc = TemporaryCalculation(QEPWCalc)

    def tearDown(self):
        self._qepwcalc.cleanup()

    def test_with_qe(self):
        # test with the Silicon example
        self.lattice = Lattice.from_calculation(self._qepwcalc.path)
        self.assertEqual(self.lattice.bravais_lattice, "fcc")
        self.assertEqual(self.lattice.primitive_vectors.shape, (3, 3))
        self.assertEqual(self.lattice.reciprocal_vectors.shape, (3, 3))
        self.assertIn("Si", self.lattice.atomic_types)
        self.assertEqual(len(self.lattice.atomic_types), 1)
        self.assertEqual(len(self.lattice.atomic_masses), 1)
        self.assertEqual(len(self.lattice.atomic_pseudos), 1)
        self.assertEqual(self.lattice.natoms, 2)
        for attr in ("atomic_positions", "atomic_positions_cartesian"):
            self.assertEqual(len(getattr(self.lattice, attr)), 1)
            self.assertEqual(len(getattr(self.lattice, attr)[0]), 2)  # 2 Si
