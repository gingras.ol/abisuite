#!/bin/bash


MPIRUN=""
EXECUTABLE=/home/fgoudreault/Workspace/abinit/build/master/vanilla/src/98_main/abinit
INPUT=/home/fgoudreault/Workspace/abilaunch/examples/launchers/abinit_launcher/run/example.files
LOG=/home/fgoudreault/Workspace/abilaunch/examples/launchers/abinit_launcher/example.log
STDERR=/home/fgoudreault/Workspace/abilaunch/examples/launchers/abinit_launcher/example.stderr

$MPIRUN $EXECUTABLE < $INPUT > $LOG 2> $STDERR
