import unittest

from .bases import BaseInputFileHandlerTest
from ..variables_for_tests import (
        wannier90_vars,
        )
from ...handlers import (
        Wannier90InputFile,
        )


class TestWannier90InputFile(BaseInputFileHandlerTest, unittest.TestCase):
    """Test case for a wannier90 input file handler.
    """
    _file_handler_class = Wannier90InputFile
    _input_vars = wannier90_vars.copy()

    def _change_random_property(self, handler):
        handler.input_variables["dis_win_max"] += 1
