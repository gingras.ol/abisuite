from .bases import BaseFileParser
from ..file_structures import SymLinkStructure
import os


class SymLinkParser(BaseFileParser):
    """Parser for a symlink file or a directory containing symlinked files.
    """
    _expected_ending = ""
    _loggername = "SymLinkParser"
    _structure_class = SymLinkStructure

    def _extract_data(self):
        if not os.path.islink(self.path):
            raise FileNotFoundError(f"Not a symlink: {self.path}")
        self.source = os.readlink(self.path)
