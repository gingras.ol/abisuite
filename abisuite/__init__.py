# LOAD CONFIG FILE FIRST
from .config import ConfigFileParser
USER_CONFIG = ConfigFileParser()
__USER_CONFIG__ = USER_CONFIG

# Define implementation restrictions
__IMPLEMENTED_QUEUING_SYSTEMS__ = (
                "local", "grid_engine", "pbs_professional", "slurm", "torque",
                )
__IMPLEMENTED_CALCTYPES__ = (
        "abinit", "abinit_anaddb", "abinit_cut3d", "abinit_mrgddb",
        "abinit_optic", "qe_dos",
        "qe_dynmat", "qe_epsilon", "qe_epw", "qe_fs", "qe_ld1", "qe_matdyn",
        "qe_ph", "qe_pp", "qe_projwfc", "qe_pw", "qe_pw2wannier90", "qe_q2r",
        "wannier90",
        )

from .constants import *
from .databases import AbiDB
from .handlers import (
        AbinitDMFTEigFile,
        AbinitDMFTProjectorsFile,
        AbinitOutputFile,
        MetaDataFile,
        QEPHInputFile,
        QEPWLogFile, QEPWInputFile
        )
from .launchers import (
        AbinitAnaddbLauncher, AbinitCut3DLauncher, AbinitLauncher,
        AbinitMassLauncher,
        AbinitMrgddbLauncher, AbinitOpticLauncher, AbinitOpticMassLauncher,
        QEDOSLauncher, QEDOSMassLauncher,
        QEDynmatLauncher,
        QEEpsilonLauncher, QEEpsilonMassLauncher,
        QEEPWLauncher,
        QEFSLauncher, QEFSMassLauncher,
        QELD1Launcher, QELD1MassLauncher,
        QEMatdynLauncher, QEMatdynMassLauncher,
        QEPHLauncher, QEPHMassLauncher,
        QEPPLauncher, QEPPMassLauncher,
        QEProjwfcLauncher, QEProjwfcMassLauncher,
        QEPWLauncher, QEPWMassLauncher,
        QEPW2Wannier90Launcher,
        QEQ2RLauncher, QEQ2RMassLauncher,
        Wannier90Launcher,
        )
from .plotters import Plot, MultiPlot
from .post_processors import (
        a2F, BandStructure, ConductivityTensor, DielectricTensor, DOSPlot,
        Epsilon, FatBand, Fit, get_all_k_labels, ConvergenceFit,
        MassDOSPlot, PhononDispersion,
        PhononSpectralFunction, PolynomialFit,
        CubicSplineInterpolation, Resistivity,
        )
from .routines import full_abspath, sort_data
from .sequencers import (
        AbinitBandStructureSequencer, AbinitBandStructureComparatorSequencer,
        AbinitDOSSequencer,
        AbinitEcutConvergenceSequencer, AbinitEcutPhononConvergenceSequencer,
        AbinitKgridConvergenceSequencer, AbinitKgridPhononConvergenceSequencer,
        AbinitIndividualPhononSequencer, AbinitNSCFSequencer,
        AbinitOpticSequencer,
        AbinitPhononDispersionSequencer, AbinitRelaxationSequencer,
        AbinitSCFSequencer,
        AbinitSmearingConvergenceSequencer,
        AbinitSmearingPhononConvergenceSequencer,
        QEEcutConvergenceSequencer, QEEcutPhononConvergenceSequencer, 
        QEIndividualPhononSequencer,
        QEKgridConvergenceSequencer, QEKgridPhononConvergenceSequencer,
        QEPhononDispersionSequencer,
        QEEPWSequencer, QEEPWWithPhononInterpolationSequencer,
        QEEPWIBTESequencer, QEEPWZimanSequencer,
        QEFermiSurfaceSequencer,
        QERelaxationSequencer, QESCFSequencer, QESmearingConvergenceSequencer,
        QESmearingPhononConvergenceSequencer, QEThermalExpansionSequencer,
        Wannier90InterpolationSequencer,
        )
from .status_checkers import StatusTree
from .system import Queue
from .workflows import AbinitWorkflow, QEWorkflow


SYSTEM_QUEUE = Queue()
__SYSTEM_QUEUE__ = SYSTEM_QUEUE
