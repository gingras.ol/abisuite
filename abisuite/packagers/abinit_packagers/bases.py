import abc

from ..bases import BasePackager


class BaseAbinitPackager(BasePackager, abc.ABC):
    """Base class for abinit packagers.

    This class is not meant to be instanciated directly.
    """

    def keep_file(self, handler):
        if handler.basename.endswith(".abi"):
            return True
        if handler.basename.endswith(".abo"):
            return True
        if ".abo_" in handler.basename:
            # also package backups of output files
            return True
        if handler.basename.endswith(".files"):
            return True
        return super().keep_file(handler)
