import os

from .bases import BaseFilesWriter
from ...file_structures import AbinitOpticFilesStructure


class AbinitOpticFilesWriter(BaseFilesWriter):
    """Class that writes an Optic files file.
    """
    _loggername = "AbinitOpticFilesWriter"
    _structure_class = AbinitOpticFilesStructure

    @property
    def lines(self):
        lines = super().lines
        # third line is output data prefix
        lines.append(os.path.join(self.output_data_dir,
                                  self.output_data_prefix) + "\n")
        return lines
