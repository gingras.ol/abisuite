from abisuite import AbinitBandStructureSequencer


sequencer = AbinitBandStructureSequencer()
sequencer.scf_workdir = "scf_run/scf_ecut10"
sequencer.scf_input_variables = {
        "ecut": 10,
        "kptopt": 1,
        "ngkpt": [6, 6, 6],
        "nshiftk": 4,
        "shiftk": [[0.5, 0.5, 0.5], [0.5, 0.0, 0.0],
                   [0.0, 0.5, 0.0], [0.0, 0.0, 0.5]],
        "diemac": 12.0,
        "nstep": 10,
        "toldfe": 1e-6,
        "acell": [7.2078778121] * 3,
        "natom": 2,
        "ntypat": 1,
        "rprim": [[0, 0.70710678119, 0.70710678119],
                  [0.70710678119, 0, 0.70710678119],
                  [0.70710678119, 0.70710678119, 0]],
        "typat": [1, 1],
        "xred": [[0, 0, 0], [0.25, 0.25, 0.25]],
        "znucl": 14.0,
        "autoparal": 1,
        }
sequencer.scf_mpi_command = "mpirun -np 4"
sequencer.scf_pseudos = ["../pseudos/Si.psp8"]
sequencer.band_structure_workdir = "band_structure/bs_ecut10"
sequencer.band_structure_kpoint_path = [
            {"L": [0.5, 0.0, 0.0]},
            {r"$\Gamma$": [0.0, 0.0, 0.0]},
            {"X": [0.0, 0.5, 0.5]},
            {r"$\Gamma_2$": [1.0, 1.0, 1.0]}]
sequencer.band_structure_kpoint_path_density = 20
sequencer.band_structure_input_variables = {
        "ecut": 10,
        "iscf": -2,
        "nband": 8,
        "diemac": 12.0,
        "tolwfr": 1e-12,
        "autoparal": 1,
        }
sequencer.band_structure_mpi_command = "mpirun -np 4"
sequencer.band_structure_pseudos = ["../pseudos/Si.psp8"]
sequencer.launch()
