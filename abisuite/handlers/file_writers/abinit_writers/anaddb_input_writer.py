from .bases import BaseAbinitInputWriter
from ...file_structures import AbinitAnaddbInputStructure


class AbinitAnaddbInputWriter(BaseAbinitInputWriter):
    """Class that can write an anaddb Input file."""
    _loggername = "AbinitAnaddbInputWriter"
    _structure_class = AbinitAnaddbInputStructure
