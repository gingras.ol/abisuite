from .bases import BasePostQEPHLinker


class QEQ2RfromQEPHLinker(BasePostQEPHLinker):
    """Linker class for a q2r.x calculation linked with a qe_ph calculation.
    """
    _calctype = "qe_q2r"
    _link_calctype = "qe_ph"
    _loggername = "QEQ2RfromQEPHLinker"
