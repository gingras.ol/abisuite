from .bases import BaseDirectoryHandlerTest
from ...handlers import InputDataDirectory
import os
import tempfile
import unittest


class TestInputDataDirectory(BaseDirectoryHandlerTest, unittest.TestCase):
    _directory_handler_class = InputDataDirectory

    def test_write(self):
        # take the file already existing from the directory handler in setUp
        # and copy/link them in a new directory
        newpath = tempfile.TemporaryDirectory()
        newdir = InputDataDirectory()
        newdir.path = newpath.name
        link1 = os.path.join(newdir.path, "link1")
        copy1 = os.path.join(newdir.path, "copy1")
        link2 = os.path.join(newdir.path, "subdir/link2")
        copy2 = os.path.join(newdir.path, "subdir/copy2")
        for link in (link1, link2):
            newdir.add_symlink(link, self._file)
        for copy in (copy1, copy2):
            newdir.add_copied_file(copy, self._subfile)
        newdir.write()
        for link in (link1, link2):
            self.assertTrue(os.path.islink(link))
            self.assertTrue(os.path.samefile(os.readlink(link), self._file))
        for copy in (copy1, copy2):
            self.assertTrue(os.path.isfile(copy))
