from abisuite import QEPWLauncher as Launcher


# very simple example
qe_input_vars = {
        "calculation": "scf",
        "ibrav": 2,
        "celldm(1)": 10.28,
        "nat": 2,
        "ntyp": 1,
        "ecutwfc": 50.0,
        "conv_thr": 1e-10,
        "pseudo_dir": "../pseudos",
        "atomic_species": [{"atom": "Si", "atomic_mass": 28.086,
                            "pseudo": "Si.upf"}],
        "atomic_positions": {"parameter": "alat",
                             "positions": {"Si": [[0.00, 0.00, 0.00],
                                                  [0.25, 0.25, 0.25]],
                                           }},
        "k_points": {"parameter": "automatic",
                     "k_points": [8, 8, 8, 0, 0, 0]}
        }

launcher = Launcher("pw")
launcher.command = "pw.x"
launcher.workdir = "pw_run"
launcher.input_variables = qe_input_vars
launcher.write()
launcher.run()
