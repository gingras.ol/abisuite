#!/bin/bash

MPIRUN="mpirun -np 4"
EXECUTABLE="/Users/fgoudreault/Workspace/abinit/build/mpi/src/98_main/abinit"
INPUT=/Users/fgoudreault/Workspace/abisuite/abisuite/unittests/files/abinit_AlAs/phonons/ph6/run/ph6.files
LOG=/Users/fgoudreault/Workspace/abisuite/abisuite/unittests/files/abinit_AlAs/phonons/ph6/ph6.log
STDERR=/Users/fgoudreault/Workspace/abisuite/abisuite/unittests/files/abinit_AlAs/phonons/ph6/ph6.stderr


$MPIRUN $EXECUTABLE < $INPUT > $LOG 2> $STDERR
