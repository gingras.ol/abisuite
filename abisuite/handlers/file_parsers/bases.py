import abc
import logging
import os

from file_read_backwards import FileReadBackwards

from ..bases import BaseStructuredObject
from ...bases import BaseUtility
from ...exceptions import DevError
from ...routines import is_list_like


class BaseFileParser(BaseStructuredObject, abc.ABC):
    """Base class for all parsers for files which belongs in a calculation
    directory or not.
    """
    _expected_ending = None

    def __init__(self, *args, **kwargs):
        super().__init__(**kwargs)
        if self._expected_ending is None:
            raise DevError("Need to set _expected_ending.")
        self._path = None

    @property
    def is_empty(self):
        """False if file contains at least one line of data.
        """
        lines = self.get_lines_from_file(self.path, line_limit=1)
        return bool(lines)

    @property
    def path(self):
        return self._path

    @path.setter
    def path(self, path):
        # if not os.path.exists(path):
        #     raise FileNotFoundError(f"Nothing found here: {path}.")
        # only raise an error when reading
        # if not os.path.isfile(path):
        #     raise FileNotFoundError(f"Not a file: {path}.")
        if self._expected_ending is not None:
            if not is_list_like(self._expected_ending):
                endings = (self._expected_ending, )
            else:
                endings = self._expected_ending
            for ending in endings:
                if path.endswith(ending):
                    break
            else:
                self._logger.warning(f"file {path} has its extension different"
                                     f" from expected ({self._expected_ending}"
                                     ")")
        self._path = path

    def read(self, *args, extract_lines_only=False, **kwargs):
        """Read and parse file set in the 'path' attribute.

        Parameters
        ----------
        extract_lines_only: bool, optional
            If `True`, all args and kwargs are passed to the
            :meth:`extract the lines <
            abisuite.handlers.file_parsers.base.BaseFileParser.get_lines_from_file>`
            from the file. The list of lines is returned.
            If `False`, the data is extracted as well (default) and
            lines not returned.

        Other Parameters
        ----------------
        All args and kwargs are passed the the ``_extract_data`` private
        method or ``get_lines_from_file`` depending on what is requested.

        Raises
        ------
        FileNotFoundError:
            If the path given to the handler does not point towards an
            actual file or the file does not exists.

        Returns
        -------
        list: The list of lines if ``extract_lines_only`` is set to `True`.
        """
        if not os.path.exists(self.path) and not os.path.islink(self.path):
            raise FileNotFoundError(f"Nothing found here: {self.path}.")
        if not os.path.isfile(self.path) and not os.path.islink(self.path):
            raise FileNotFoundError(f"Not a file: {self.path}.")
        if extract_lines_only:
            if "cleanfunc" not in kwargs:
                # add it artificially
                return self.get_lines_from_file(
                    self.path,
                    *args,
                    cleanfunc=self._clean_lines,
                    logger=self._logger,
                    **kwargs)
            return self.get_lines_from_file(
                    self.path, *args, logger=self._logger, **kwargs)
        else:
            self._extract_data(*args, **kwargs)

    @abc.abstractmethod
    def _extract_data(self):
        pass

    @classmethod
    def from_file(cls, path, *args, **kwargs):
        """Initialize parser directly from a file.
        """
        instance = cls(*args, **kwargs)
        instance.path = path
        return instance

    @classmethod
    def _instance_from_filepath(cls, filepath, *args, **kwargs):
        """Given a str, a list or a dict of filepaths, returns either, a single
        instance, a list of instances or a dict of instances.

        The dict return (if a dict is given for filepath) will have the same
        keys. Other args and kwargs are passed to the init method for each
        instances created.
        """
        if is_list_like(filepath):
            # many files, return a list of parsers if needed
            if len(filepath) > 1:
                return [cls.from_file(x, *args, **kwargs) for x in filepath]
            filepath = filepath[0]
        elif isinstance(filepath, dict):
            # many files organized in a dict, return a dict
            return {k: cls.from_file(v, *args, **kwargs)
                    for k, v in filepath.items}
        return cls.from_file(filepath, *args, **kwargs)

    @staticmethod
    def get_lines_from_file(
            path,
            backwards=False,
            cleanfunc=None,
            cleanfunc_kwargs=None,
            line_limit=None,
            logger=None,
            ):
        """Extract lines from an actual file.

        Parameters
        ----------
        path: str
            The path of the file to extract data from.
        backwards: bool, optional
            If True, the file is read starting from the end instead of starting
            from the beginning. Especially usefull when we just want to
            read data printed at the end of a file to make it faster to
            read big files. Useful when combined with the 'line_limit' kwarg.
        cleanfunc: function, optional
            If not None, the list of lines will be cleaned using this function.
            What this function returns will be returned.
        cleanfunc_kwargs: dict, optional
            If not None, this is a dict that contains the arguments to pass
            to the cleanfunc function while cleaning the lines.
        line_limit: bool, optional
            If not None, specifies the number of lines to return.
        logger: Logger object, optional
            If not None, this logger object will be used in case an error
            is thrown while parsing the file.

        Returns
        -------
        List: The list of lines in the file (possibly cleaned).
        """
        # just extract the lines from the actual file
        read_func = open if not backwards else FileReadBackwards
        read_func_args = (path, "r") if not backwards else (path, )
        lines = []
        try:
            with read_func(*read_func_args) as f:
                for iline, line in enumerate(f):
                    lines.append(line)
                    if line_limit is not None:
                        if line_limit <= iline + 1:
                            break
        except UnicodeDecodeError as err:
            if logger is not None:
                logger.critical(f"Cannot read file '{path}'.")
                logger.exception(err)
            raise err
        if backwards:
            # reverse the line ordering since they were appended backwards
            lines = lines[::-1]
        # clean the lines if needed
        if cleanfunc is None:
            return lines
        if cleanfunc_kwargs is None:
            cleanfunc_kwargs = {}
        return cleanfunc(lines, **cleanfunc_kwargs)

    @staticmethod
    def _clean_lines(
            lines, strip_newlines=False, strip_spaces=False,
            strip_comas=False,
            ignore_python_comments=False, ignore_fortran_comments=False,
            ignore_empty_lines=False,
            ignore_lines_starting_with=None,
            ):
        """Processes a list of lines extracted from a file.

        Parameters
        ----------
        strip_comas: bool, optional
            If True, the comas will be stripped from each lines.
        strip_newlines: bool, optional
            If True, the newlines will be stripped from each lines.
        strip_spaces: bool, optional
            If True, the surrounding spaces of all lines will be stripped.
        ignore_empty_lines: bool, optional
            If True, empty lines will be discarded.
        ignore_python_comments: bool, optional
            If True, the lines starting with '#' will be discarded.
        ignore_fortran_comments: bool, optional
            If True, the lines starting with '!' will be discarded.
        ignore_lines_starting_with: list, optional
            If given, gives a list of what lines to ignore.
        """
        newlines = []
        if ignore_lines_starting_with is None:
            ignore_lines_starting_with = []
        if not is_list_like(ignore_lines_starting_with):
            raise DevError(
                    "'ignore_lines_starting_with' should be a list but "
                    f"got '{ignore_lines_starting_with}'.")
        ignore_lines_starting_with = list(ignore_lines_starting_with)
        if ignore_python_comments:
            ignore_lines_starting_with.append("#")
        if ignore_fortran_comments:
            ignore_lines_starting_with.append("!")

        def line_startswith_one_forbidden_char(line):
            for char in ignore_lines_starting_with:
                if line.startswith(char):
                    return True
            return False

        for line in lines:
            if strip_newlines:
                line = line.strip("\n")
            if strip_spaces:
                line = line.strip()
            if strip_comas:
                line = line.strip(",")
            if line_startswith_one_forbidden_char(line):
                continue
            if ignore_empty_lines and len(line) == 0:
                continue
            newlines.append(line)
        del lines
        return newlines

    def _set_attributes_from_data(self, data):
        """Sets attributes to the File Parser object from a dict of data.
        """
        for attr, value in data.items():
            setattr(self, attr, value)


class BaseFormattedFileParser(BaseFileParser, abc.ABC):
    """Base class for formatted files that does not necessarily belongs in
    a calculation directory.
    """

    def _extract_data(self, backwards=False, line_limit=None, **kwargs):
        """For this class, the data is read from the formatted file directly.
        This method parses the lines in the file, cleans the lines and parse
        the data from the lines. Then, the attributes are set.

        Parameters
        ----------
        backwards: bool, optional
            If True, the file is read from the end (but lines will have same
            order). Usefull when we only want to parse the end of a big file.
        line_limit: int, optional
            The maximum number of lines to parse and put in memory
            Set to `None` for no maximum.

        Other Parameters
        ----------------
        kwargs to this function are passed as the 'cleanfunc_kwargs' argument
        of the 'get_lines_from_file' function.
        """
        lines = self.read(
                    extract_lines_only=True,
                    backwards=backwards,
                    cleanfunc=self._clean_lines,
                    cleanfunc_kwargs=kwargs,
                    line_limit=line_limit,
                    )
        self._set_attributes_from_data(
                self._extract_data_from_lines(lines))

    @abc.abstractmethod
    def _extract_data_from_lines(self, lines):
        pass


class BaseCalculationFileParser(BaseFileParser, abc.ABC):
    """Mainstream base parser class for files belonging in a calculation
    directory.
    """

    @classmethod
    def from_calculation(cls, path, *args, **kwargs):
        """Returns a Parser from a calculation directory by reading the
        '.meta' file.
        """
        # import here to prevent import loops
        from ..file_handlers import MetaDataFile
        lvl = kwargs.get("loglevel", logging.INFO)
        with MetaDataFile.from_calculation(path, loglevel=lvl) as meta:
            return cls.from_metadatafile(meta)

    @classmethod
    def from_metadatafile(cls, meta, *args, **kwargs):
        """Initialize parser from a meta data file object.
        """
        # import here to prevent import loops
        from ..file_handlers import MetaDataFile
        if not isinstance(meta, MetaDataFile):
            raise TypeError("Requires a Meta data file object.")
        # if cls._calctype is not None:
        #     if meta.calctype != cls._calctype:
        #         raise ValueError(f"Meta calctype '{meta.calctype}' differs"
        #                          f" from Parser calctype "
        #                          f"'{cls._calctype}'")
        filepath = cls._filepath_from_meta(meta, *args, **kwargs)
        return cls._instance_from_filepath(filepath, *args, **kwargs)

    @abc.abstractclassmethod
    def _filepath_from_meta(cls, meta, *args, **kwargs):
        # returns the expected filepath from a meta file
        pass


class BaseFormattedCalculationFileParser(
        BaseCalculationFileParser, BaseFormattedFileParser, abc.ABC):
    """Base class for parsers that have a custom syntax (most parsers).
    Only for files belonging in a calculation directory.
    """

    # avoid DDD
    def _extract_data(self, *args, **kwargs):
        return BaseFormattedFileParser._extract_data(self, *args, **kwargs)


class BaseSubParser(BaseUtility, abc.ABC):
    """SubParser Base class.
    """
    can_be_recalled = False
    """If can_be_recalled is True, then the subparser will be updated
    each time the trigger is found in the file.
    """
    recalled_behavior = None
    """Specify what happens to the data when this sub parser is recalled.
    Can be either 'append' or 'overwrite'.
    """
    subject = None
    trigger = None

    def __init__(self, lines, **kwargs):
        super().__init__(**kwargs)
        self._ending_relative_index = None
        # TODO: check that this is not necessary!
        self.lines = self.preprocess_lines(lines)
        # self._logger.debug(f"SubParser triggered for: '{self.subject}'.")
        self.data = self._extract_data_from_lines(self.lines)
        if self.can_be_recalled and self.recalled_behavior is None:
            raise DevError(
                    "Need to set the 'recalled_behavior' for the subparser.")

    @abc.abstractmethod
    def _extract_data_from_lines(self, lines):
        pass

    @property
    def ending_relative_index(self):
        if hasattr(self, "_ending_relative_index"):
            if self._ending_relative_index is not None:
                return self._ending_relative_index
        # if we are here, there is a dev error
        raise DevError(
                f"'ending_relative_index' must be set: {self.__class__} - "
                f"{self.subject}.")

    @ending_relative_index.setter
    def ending_relative_index(self, index):
        self._ending_relative_index = index

    @staticmethod
    def preprocess_lines(lines):
        # TODO: lines should already have been cleaned up. This is redundant!
        return [x.strip("\n").strip() for x in lines]


class BaseParserWithSubParsers(BaseFormattedCalculationFileParser, abc.ABC):
    """Base class for parsers that use subparsers to parse
    their data.
    """
    _subparsers = None

    def __init__(self, *args, **kwargs):
        if self._subparsers is None:
            raise DevError("Need to set subparsers list.")
        super().__init__(*args, **kwargs)
        self.subparsers = SubParsersList(self._subparsers)

    def _extract_data_from_lines(self, lines):
        skip = 0
        data = {}
        # copy subparsers to not erase from the class attribute
        subparsers = self.subparsers.copy()
        for index, line in enumerate(lines):
            if index < skip:
                # don't work on this line
                continue
            for triggers, subparser in subparsers.items():
                # there can be multiple triggers for one type of data
                # e.g.:fermi_level and highest_occupied_level for QEPWLog
                if not is_list_like(triggers):
                    triggers = (triggers, )
                for trigger in triggers:
                    if trigger in line:
                        # this line is a trigger for the subparser to work
                        # parse the rest of the lines from here
                        s = subparser(lines[index:],
                                      loglevel=self._logger.level)
                        self._set_data_from_subparser(data, s)
                        if not subparser.can_be_recalled:
                            # remove the subparser from the list
                            # to not parse again
                            subparsers.remove(subparser)
                        # modify the skip to not reparse the parsed lines
                        skip = index + s.ending_relative_index
                        # stop parsing the active line
                        break
            if self._stop_parsing_at(line, index):
                # TODO: this is weird, find a way to make this better
                break
        # if len(subparsers):
        #     # couldnt get some information
        #     missed = [s.subject
        #               for s in subparsers if not s.can_be_recalled]
        #     # if len(missed):
        #     #     self._logger.debug(
        #            f"Some info couldn't be parsed: {missed}")
        return data

    def _set_data_from_subparser(self, data, subparser):
        subjects = subparser.subject
        # support for many subjects in a single subparser
        # different subjects are ordered in the data dicts
        # as the keys
        if not is_list_like(subjects):
            subjects = [subjects]
            subparser.data = {subjects[0]: subparser.data}
        for subject, subdata in subparser.data.items():
            if not subparser.can_be_recalled:
                if subject not in data:
                    data[subject] = subdata
                    continue
            if subparser.recalled_behavior == "overwrite":
                data[subject] = subdata
            elif subparser.recalled_behavior == "append":
                if subject not in data:
                    data[subject] = [subdata]
                else:
                    data[subject].append(subdata)
            else:
                raise NotImplementedError(subparser.recalled_behavior)

    def _stop_parsing_at(self, line, index):
        # use this if there's a need to stop parsing at a certain line
        return False


class SubParsersList:
    """Class that takes a list of subparsers and manages it.
    """
    def __init__(self, slist):
        self.subparsers = list(slist)

    def copy(self):
        return SubParsersList(self.subparsers.copy())

    def items(self):
        for subparser in self.subparsers:
            yield subparser.trigger, subparser

    def remove(self, parser):
        self.subparsers.remove(parser)

    def __len__(self):
        return len(self.subparsers)

    def __iter__(self):
        for subparser in self.subparsers:
            yield subparser

    def __repr__(self):
        return str(self.subparsers)


# TODO: why is this class here? FG 2021/07/13
class BaseInputParserNoInputVariables(
        BaseFormattedCalculationFileParser, abc.ABC):
    """Base class for an input file parser that does not have input variables.

    This class is more general than the 'BaseInputParser' class.
    """
    _expected_ending = ".in"

    def _extract_data(self, **kwargs):
        default_kwargs = {
                "strip_newlines": True,
                "strip_spaces": True,
                "ignore_python_comments": True,
                "ignore_fortran_comments": True,
                }
        default_kwargs.update(kwargs)
        super()._extract_data(**default_kwargs)

    @classmethod
    def _filepath_from_meta(cls, meta):
        return meta.input_file_path


class BaseInputParser(BaseInputParserNoInputVariables, abc.ABC):
    """Base class for an input file parser.
    """
    pass


class BaseLogParser(BaseParserWithSubParsers):
    """Base class for log parsers.
    """
    _expected_ending = ".log"

    def _extract_data(self):
        super()._extract_data(
                strip_newlines=True, strip_spaces=True)

    def _extract_data_from_lines(self, *args, **kwargs):
        return BaseParserWithSubParsers._extract_data_from_lines(
                self, *args, **kwargs)

    @classmethod
    def _filepath_from_meta(cls, meta, *args, **kwargs):
        try:
            # log path is located in PBS files.
            # import here to prevent import loops.
            from .pbs_parser import PBSParser
            pbsparser = PBSParser.from_metadatafile(meta)
            pbsparser.read()
            return pbsparser.log_path
        except FileNotFoundError as err:
            # PBS don't exists => perhaps it was cleaned
            # fallback to guessing where the log is.
            # usually it is located next to the meta data file
            with meta:
                workdir = meta.calc_workdir
            for filename in os.listdir(workdir):
                if filename.endswith(cls._expected_ending):
                    return os.path.join(workdir, filename)
            raise err
