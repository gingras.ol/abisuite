from ...exceptions import NotACalculationDirectoryError
from ...handlers import CalculationDirectory, is_calculation_directory


def get_calctype_from_calculation(calc):
    """Return the calctype of a calculation.

    Parameters
    ----------
    calc: str or CalculationDirectory instance
        The path of the calculation to get the calctype from.
        Can also be an instance of
        :class:`~abisuite.handlers.directory_handlers.calculation_directory.
        CalculationDirectory`.

    Raises
    ------
    CalculationNotFound: If the given path does not point to a calculation.

    Returns
    -------
    str: The calctype of the calculation.
    """
    if isinstance(calc, CalculationDirectory):
        return calc.calctype
    if not is_calculation_directory(calc):
        raise NotACalculationDirectoryError(calc)
    with CalculationDirectory.from_calculation(calc) as calcdir:
        return calcdir.calctype
