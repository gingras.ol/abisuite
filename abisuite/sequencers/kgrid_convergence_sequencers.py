import abc

from .scf_sequencer import SCFConvergenceSequencer
from ..routines import is_list_like


class BaseKgridConvergenceSequencer(SCFConvergenceSequencer, abc.ABC):
    """Kpoint grid convergence sequencer. Can do the convergence study on the
    k-point grid density.

    For Quantum Espresso: this class only works with the k_points parameter set
    to 'automatic' (automatically set to this one).
    """
    kgrids_units = None
    _parameter_to_converge = "kgrids"

    def __init__(self, *args, **kwargs):
        SCFConvergenceSequencer.__init__(self, *args, **kwargs)
        self._kgrids_input_variable_name = None

    @property
    def kgrids_input_variable_name(self):
        # we use properties because for abinit there are many possible
        # variables and user chooses one by setting it
        if self._kgrids_input_variable_name is None:
            raise ValueError(
                "Need to set 'kgrids_input_variable_name' for "
                f"'{self.__class__}'.")
        return self._kgrids_input_variable_name

    @kgrids_input_variable_name.setter
    def kgrids_input_variable_name(self, name):
        self._kgrids_input_variable_name = name

    @property
    def scf_converged_kgrid(self):
        return self.scf_converged_parameter

    @property
    def scf_kgrids(self):
        if self._scf_kgrids is not None:
            return self._scf_kgrids
        raise ValueError("Need to set 'kgrids'.")

    @scf_kgrids.setter
    def scf_kgrids(self, kgrids):
        if not is_list_like(kgrids):
            raise TypeError("kgrids should be list-like.")
        self._scf_kgrids = kgrids

    @abc.abstractmethod
    def _add_xtick_labels_to_convergence_plot(self, plot):
        # add xtick labels and rotate labels for clarity
        plot.xtick_rotation = 80
        # need to implement getting the kgrids labels
        # for each subclasses
