from ..bases import BaseCleaner


class AbinitMrgddbCleaner(BaseCleaner):
    """Cleaner class for a mrgddb calculation.
    """
    _calctype = "abinit_mrgddb"
    _loggername = "AbinitMrgddbCleaner"

    def delete_this_file(self, handler):
        return super().delete_this_file(handler)
