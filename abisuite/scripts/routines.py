import logging
import os

from ..bases import BaseRemoteClient
from ..handlers import CalculationTree
from ..routines import is_list_like


def get_all_calculations_recursively(
        source, return_path=False, loglevel=logging.INFO, **kwargs):
    """Get the list of all calculations from a source tree recursively.

    Parameters
    ----------
    source: path, list
        The source of everything. If a list of sources is given,
        the concatenation of all calculations is returned.
    return_path: bool, optional
        If True, only paths are returned. Otherwise, CalculationDirectory
        objects are returned.
    loglevel: int, optional
        States the loglevel to pass out to CalculationDirectory objects.
    other_kwargs:
        are passed to the StatusTree object that build the tree.

    Returns
    -------
    list: The list of all calculations.
    """
    if source is None or not source:
        raise ValueError("Please provide 'calculations'.")
    if is_list_like(source):
        # many sources. concatenate everything
        calcs = []
        for src in source:
            calcs += get_all_calculations_recursively(
                    src, return_path=return_path, loglevel=loglevel, **kwargs)
        return calcs
    if os.path.isfile(source):
        return []
    calc_tree = CalculationTree(loglevel=loglevel)
    calc_tree.path = source
    calc_tree.build_tree(**kwargs)
    calc_tree.sort_tree()
    if return_path:
        return [x["path"] for x in calc_tree.calculations]
    return [x["calculation_directory"] for x in calc_tree.calculations]


def walltime_to_str(walltime, max_walltime=None):
    """Converts a walltime (in seconds) to a string.

    Parameters
    ----------
    walltime: float
        The walltime to convert.
    max_walltime: float, optional
        If not None gives the maximal walltime to compare the string to.
        For example, if the max_walltime is 60 and walltime is 5, it will
        return '5s'. If max_walltime is 86400 (1 day) for the same walltime,
        it will return '0d 0h 0m 5s' instead. If None, it will return
        the shortest string possible. This argument is there in order to make
        it easier to compare walltimes together in case many walltimes are
        used in a row.

    Returns
    -------
    str: The walltime turned to a string.
    """
    if max_walltime is None:
        max_walltime = walltime
    ndays = int(walltime // 86400)
    rest = walltime % 86400
    nhours = int(rest // 3600)
    rest %= 3600
    nmins = int(rest // 60)
    nsecs = round(rest % 60, 1)
    string = ""
    if ndays or max_walltime >= 86400:
        # displays up to the days
        string += f"{ndays}d "
    if nhours or max_walltime >= 3600:
        # displays up to the hours
        string += f"{nhours:02}h "
    if nmins or max_walltime >= 60:
        # displays up to mins
        string += f"{nmins:02}m "
        # always display seconds
        string += f"{nsecs:.1f}s".zfill(5)
    if not nmins and not ndays and not nhours and max_walltime < 60:
        # always display seconds (but don't need zfill)
        string += f"{nsecs:.1f}s"
    return string


class AbisuiteScriptRemoteClient(BaseRemoteClient):
    """Class that handles remote clients when invoked by the --remote
    argument of the abisuite script.
    """
    _check_script_exists_on_connection = "abisuite"

    def remote_script_calculations(self, command_line):
        """Use the script remotely on given calculations.

        Parameters
        ----------
        command_line: str
            The command line to send to the remote.

        Raises
        ------
        RuntimeError: When this function is called while not in a with stmt.
        """
        from abisuite.colors import Colors
        if self.client is None:
            raise RuntimeError(
                    "Need to enter with statement before relaunching remote "
                    "calculations.")
        print(Colors.bold_text("EXECUTING ON REMOTE: ") +
              "{self.hostname} '{command_line}'.")
        stdin, stdout, stderr = self.client.exec_command(command_line)
        stderr = stderr.read().decode("utf-8")
        stdout = stdout.read().decode("utf-8")
        if stdout:
            print(stdout)
        if stderr:
            print(stderr)
