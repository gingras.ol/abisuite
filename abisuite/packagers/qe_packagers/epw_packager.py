from .bases import BaseQEPackager


class QEEPWPackager(BaseQEPackager):
    """Packager class for an epw.x calculation by Quantum Espresso.
    """
    _calctype = "qe_epw"
    _loggername = "QEEPWPackager"

    def keep_file(self, handler, *args, **kwargs):
        # there's a ton of files to keep in order to restart from a packaged
        # epw interpolation or save main results of a run
        keep_file_ext = [
                "_band.dat", "_band.kpt", ".txt", ".epmatwp", ".nnkp", "kgmap",
                ".mmn", ".ukk", ".win", ".wout",
                ]
        for file_ext in keep_file_ext:
            if handler.basename.endswith(file_ext):
                return True
        keep_filename = [
                "band.eig", "phband.freq", "ifc.q2r", "crystal.fmt",
                "IBTEvel_sup.fmt", "inv_taucb.fmt", "inv_tau.fmt",
                "vmedata.fmt",
                ]
        for filename in keep_filename:
            if handler.basename == filename:
                return True
        keep_file_prefix = [
                "decay",
                ]
        for file_prefix in keep_file_prefix:
            if handler.basename.startswith(file_prefix):
                return True
        if ".res.01" in handler.basename:
            # keep ziman resistivity files
            return True
        return super().keep_file(handler, *args, **kwargs)
