#!/bin/bash


MPIRUN=""
EXECUTABLE=/home/fgoudreault/Workspace/abinit/build/master/vanilla/src/98_main/abinit
INPUT=/home/fgoudreault/Workspace/abilaunch/examples/masslauncher/ecut10/run/ecut10.files
LOG=/home/fgoudreault/Workspace/abilaunch/examples/masslauncher/ecut10/ecut10.log
STDERR=/home/fgoudreault/Workspace/abilaunch/examples/masslauncher/ecut10/ecut10.stderr

$MPIRUN $EXECUTABLE < $INPUT > $LOG 2> $STDERR
