#!/bin/bash

MPIRUN="mpirun -np 4"
EXECUTABLE="/home/fgoudreault/Workspace/abinit/abinit/build/develop/2021-04-20/src/98_main/abinit"
INPUT=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_sequencers/files/abinit_Si_phonons/ph_runs/ph_run3/run/ph_run3.files
LOG=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_sequencers/files/abinit_Si_phonons/ph_runs/ph_run3/ph_run3.log
STDERR=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_sequencers/files/abinit_Si_phonons/ph_runs/ph_run3/ph_run3.stderr


$MPIRUN $EXECUTABLE < $INPUT > $LOG 2> $STDERR
