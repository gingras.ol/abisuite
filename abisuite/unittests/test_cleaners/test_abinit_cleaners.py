import unittest

from .bases import BaseCleanerTest
from ..bases import TestCase
from ..variables_for_tests import (
        abinit_vars, abinit_AlAs_pseudos, AbinitCalc,
        AbinitOpticCalc, abinit_optic_vars,
        )
from ...cleaners import AbinitCleaner, AbinitOpticCleaner
from ...exceptions import DevError


class BaseAbinitCleanerTest(BaseCleanerTest):
    """Base class for abinit cleaners unittests.
    """
    _calculation_pseudos = None

    def setUp(self):
        super().setUp()
        if self._calculation_pseudos is None:
            raise DevError("Need to set '_calculation_pseudos'.")
        self.calc.new_pseudos = self._calculation_pseudos

    def _get_launcher(self):
        launcher = super()._get_launcher()
        launcher.pseudos = self._calculation_pseudos
        return launcher


class TestAbinitCleaner(BaseAbinitCleanerTest, TestCase):
    """Test case for the AbinitCleaner class.
    """
    _cleaner_cls = AbinitCleaner
    _calculation_example = AbinitCalc
    _calculation_example_input_variables = abinit_vars
    _calculation_pseudos = abinit_AlAs_pseudos
    _script_name = "abinit"


class TestAbinitOpticCleaner(BaseAbinitCleanerTest, TestCase):
    """Test case for the AbinitOptic cleaner class.
    """
    _cleaner_cls = AbinitOpticCleaner
    _calculation_example = AbinitOpticCalc
    _calculation_example_input_variables = abinit_optic_vars
    _calculation_example_parents = []  # TODO: IMPLEMENT ME
    _script_name = "optic"

    def setUp(self):
        BaseCleanerTest.setUp(self)

    @unittest.skip("Not implemented...")
    def test_clean_abort_if_calculation_not_started(self):
        BaseCleanerTest.test_clean_abort_if_calculation_not_started(self)
