from .bases import BaseQELauncher_for_outdir_prefix
from ..scf_launchers import BaseSCFLauncher
from .... import USER_CONFIG
from ....handlers import QEPWInputFile
from ....handlers.file_approvers import QEPWInputParalApprover
from ....routines import full_abspath


__ALL_QE_GEOMETRY_VARIABLES__ = (
        "atomic_species", "atomic_positions", "nat", "ntyp", "ibrav",
        "celldm(*)",
        "pseudo_dir",  # we want to keep same pseudo files
        )


class QEPWLauncher(BaseQELauncher_for_outdir_prefix, BaseSCFLauncher):
    """Launcher class for a Quantum Espresso calculation using the pw.x script.
    """
    _calctype = "qe_pw"
    _loggername = "QEPWLauncher"
    _input_file_handler_class = QEPWInputFile
    _paral_approver_class = QEPWInputParalApprover
    _geometry_variables = __ALL_QE_GEOMETRY_VARIABLES__
    _kpts_variables = ("k_points", )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._temp = None  # needed if linking another calculation

    def __del__(self):
        if self._temp is not None:
            self._temp.cleanup()
            del self._temp

    def validate(self, *args, **kwargs):
        super().validate(*args, **kwargs)
        # for a 'bands' calculation, make sure that the 'save' folder is there
        if self.input_variables.get("calculation", "scf") in ("bands", "nscf"):
            self._check_for_input_file(".save")

    def _preprocess_inputs(self, inputs):
        inputs = super()._preprocess_inputs(inputs)
        # for PW calculation need to setup the pseudo directory.
        self._logger.debug("Checking pseudo_dir")
        # special treatment for pseudo dir as it can be defined in config file
        if "pseudo_dir" not in inputs:
            # try to set default value
            if USER_CONFIG.DEFAULTS.default_pseudos_dir is None:
                self._logger.error("No default pseudo dir defined in config.")
                raise ValueError("No default pseudo dir defined in config"
                                 " and it is not given in inputs.")
            else:
                inputs["pseudo_dir"] = USER_CONFIG.DEFAULTS.default_pseudos_dir
                self._logger.debug(f"Setting default pseudo dir in input"
                                   f" variables: "
                                   f"{inputs['pseudo_dir']}")
        pseudo_dir = inputs["pseudo_dir"]
        if not isinstance(pseudo_dir, str):
            pseudo_dir = inputs["pseudo_dir"].value
        inputs["pseudo_dir"] = full_abspath(pseudo_dir)
        return inputs
