from ..bases import BaseLogFileHandler
from ...file_parsers import QEFSLogParser
from ...file_structures import QEFSLogStructure


class QEFSLogFile(BaseLogFileHandler):
    """Log file handler for a Quantum Espresso fs.x log file.
    """
    _loggername = "QEFSLogFile"
    _parser_class = QEFSLogParser
    _structure_class = QEFSLogStructure
