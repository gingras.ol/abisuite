#!/bin/bash

MPIRUN="mpirun -np 4"
EXECUTABLE="/home/fgoudreault/Workspace/abinit/abinit/build/develop/2021-04-16/src/98_main/abinit"
INPUT=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_workflows/test_files/abinit_Al/relaxation/relax_full_cell_geometry/run/relax_full_cell_geometry.files
LOG=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_workflows/test_files/abinit_Al/relaxation/relax_full_cell_geometry/relax_full_cell_geometry.log
STDERR=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_workflows/test_files/abinit_Al/relaxation/relax_full_cell_geometry/relax_full_cell_geometry.stderr


$MPIRUN $EXECUTABLE < $INPUT > $LOG 2> $STDERR
