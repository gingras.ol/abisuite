Workflows Module
================

.. contents::
    :local:

This module contains the workflows objects which are used to easily produce
a whole tree of coherant calculations from a single python script that is
rerun (almost) everytime some calculations finish.

The way to use these kind of objects is like the following::
    
  from abisuite import QEWorkflow as Workflow  # or AbinitWorkflow

  workflow = Workflow(
                gs=True,  # set to True all other calculations wanted
                )
  workflow.set_gs(...)    # use the correspondint 'set' method to setup the calculations
  workflow.run()          # or write() if you just want to write the calculations

The workflow objects use a combination of Sequencers objects for each part of the
workflow. If one part depends of another one, the workflow object will stop at
the bottleneck part and wait for the corresponding calculation to finish before
continuing. Once these calculations are completed, one just need to rerun
the script for the remaining calculations. Do this until all calculations are completed.

Thus, all calculations for a given material (for example) can be contained
in a single script (only for the implemented parts of course). See the
corresponding API for all softwares to see what is implemented.

:mod:`~abisuite.workflows`
--------------------------

.. automodule:: abisuite.workflows.bases
    :members:
    :show-inheritance:

.. automodule:: abisuite.workflows.qe_workflow
    :members:
    :show-inheritance:

.. automodule:: abisuite.workflows.abinit_workflow
    :members:
    :show-inheritance:
