from ..bases import SequenceCalculation
from ..individual_phonon_sequencers import (
        BaseIndividualPhononSequencer, BasePhononConvergenceSequencer,
        BasePhononComparatorSequencer,
        )


class QEIndividualPhononSequencer(BaseIndividualPhononSequencer):
    """Individual phonon sequencer for Quantum Espresso.
    """
    _all_sequencer_prefixes = ("scf_", "phonons_")
    _loggername = "QEIndividualPhononSequencer"

    def __init__(self, *args, **kwargs):
        BaseIndividualPhononSequencer.__init__(self, "qe", *args, **kwargs)

    def init_phonons_qpoint_split_by_irreps(self, iqpt, scf_calc):
        """Init phonons calculations split by irreducible perturbations for
        a given qpoint number and a given scf calculation.

        Parameters
        ----------
        iqpt: int
            The qpt index to do.
        scf_calc: SequenceCalculation object
            The SequenceCalculation object of the SCF calculation
        """
        raise NotImplementedError(
                "Sorry this feature is not "
                "implemented for Quantum Espresso yet..."
                )

    def _get_phonon_calculation(
            self, iph, scf_calculation):
        # create a phonon calculation to the sequence based on the phonon index
        if self.phonons_input_variables is not None:
            phvars = self.phonons_input_variables.copy()
            phvars.update(
                    self._get_phonons_specific_input_variables(iph))
        else:
            phvars = None
        phdir = self._get_phonon_workdir(iph, scf_calculation)
        phon_calc = SequenceCalculation(
                        "qe_ph", phdir, phvars,
                        self.phonons_calculation_parameters,
                        loglevel=self._loglevel)
        phon_calc.dependencies.append(scf_calculation)
        return phon_calc

    def _get_phonons_specific_input_variables(self, iph):
        # returns specific input vars for phonon number 'iph'
        spec = {"start_q": iph + 1, "last_q": iph + 1}
        try:
            spec.update(self.phonons_specific_input_variables[iph])
        except ValueError:
            # don't mind if phonons specific input vars are not set
            pass
        return spec


class QEPhononComparatorSequencer(
        BasePhononComparatorSequencer,
        QEIndividualPhononSequencer):
    """Base ckass for Quantum Espresso phonon comparator sequencers.
    """

    def __init__(self, *args, **kwargs):
        QEIndividualPhononSequencer.__init__(self, *args, **kwargs)
        BasePhononComparatorSequencer.__init__(self, "qe", *args, **kwargs)

    def init_phonons_single_scf(self, scf_calc):
        # override this method because if nphonons is not set, we need
        # to determine it first.
        if self._nphonons is not None:
            BasePhononComparatorSequencer.init_phonons_single_scf(
                    self, scf_calc)
            return
        # init first qpoint
        BasePhononComparatorSequencer.init_phonons_qpoint(
                self, 0, scf_calc)
        first_ph = self.sequence[-1]
        with first_ph.calculation_directory as calc:
            if not calc.exists:
                # calc not written yet. wait for it to be ran
                return
            if calc.status["calculation_finished"] is False:
                # wait for it to finish
                return
            # if finished, get number of phonons
            with calc.log_file as log:
                self._nphonons = log.nphonons
        # set the other phonons
        for iph in range(1, self._nphonons):
            BasePhononComparatorSequencer.init_phonons_qpoint(
                    self, iph, scf_calc)

    def _get_phonon_workdir(self, *args, **kwargs):
        return BasePhononComparatorSequencer._get_phonon_workdir(
                self, *args, **kwargs)


class QEPhononConvergenceSequencer(
        QEPhononComparatorSequencer,
        BasePhononConvergenceSequencer,
        ):
    """Base class for quantum espresso phonon convergence sequencers.

    Only doing one qpt.
    """

    def __init__(self, *args, **kwargs):
        QEPhononComparatorSequencer.__init__(self, *args, **kwargs)
        BasePhononConvergenceSequencer.__init__(self, "qe", *args, **kwargs)

    def _get_phonon_frequencies_from_log(self, log):
        return log.phonon_frequencies[0]["frequencies_cm-1"]

    def _get_phonons_specific_input_variables(self, iph):
        # returns specific input vars for phonon number 'iph'
        spec = {}
        try:
            spec.update(self.phonons_specific_input_variables[iph])
        except ValueError:
            # don't mind if phonons specific input vars are not set
            pass
        return spec

    def _get_phonon_workdir(self, *args, **kwargs):
        return BasePhononConvergenceSequencer._get_phonon_workdir(
                self, *args, **kwargs)
