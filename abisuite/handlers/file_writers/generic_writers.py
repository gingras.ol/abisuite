from .bases import BaseStreamLineWriter
from ..file_structures import GenericInputStructure
from ...linux_tools import touch


class GenericInputWriter(BaseStreamLineWriter):
    """Class for a generic writer. Doesn't actually write anything.
    Have all the properties an input writer should have.
    """
    _loggername = "GenericInputWriter"
    _structure_class = GenericInputStructure

    def _do_write(self):
        # create empty file
        touch(self.path)
