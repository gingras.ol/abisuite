#!/bin/bash

MPIRUN="mpirun -np 4"
EXECUTABLE="/home/fgoudreault/Workspace/q-e/bin/epw.x -npool 4"
INPUT=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_workflows/test_files/qe_Pb/ibte_fine_grid_convergence/nk10_10_10_nq10_10_10/ibte_run/ibte_run.in
LOG=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_workflows/test_files/qe_Pb/ibte_fine_grid_convergence/nk10_10_10_nq10_10_10/ibte_run/ibte_run.log
STDERR=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_workflows/test_files/qe_Pb/ibte_fine_grid_convergence/nk10_10_10_nq10_10_10/ibte_run/ibte_run.stderr


$MPIRUN $EXECUTABLE -input $INPUT > $LOG 2> $STDERR
