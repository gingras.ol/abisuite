.. _file_approvers_api:


Approvers Module
================

.. contents::
    :local:

This module contains the `Approvers` which are classes that cross checks
input variables between them to make sure everything is consistent
prior launching or submitting a calculation using a `Launcher object<launchers_api>`.


:mod:`Bases <~abisuite.handlers.file_approvers.bases>`
------------------------------------------------------

.. automodule:: abisuite.handlers.file_approvers.bases
    :members:
    :show-inheritance:

:mod:`Approvers Exceptions<~abisuite.handlers.file_approvers.exceptions>`
-------------------------------------------------------------------------

These are exceptions classes used by the `Approvers` module.

.. automodule:: abisuite.handlers.file_approvers.exceptions
    :members:
    :show-inheritance:

:mod:`Generic Approvers <~abisuite.handlers.file_approvers.generic_approvers>`
------------------------------------------------------------------------------

These are dummy approvers that don't do anything. Contrary to base
classes, these can be instanciated directly.

.. automodule:: abisuite.handlers.file_approvers.generic_approvers
    :members:
    :show-inheritance:

:mod:`Meta Data File Approver <~abisuite.handlers.file_approvers.meta_data_approver>`
-------------------------------------------------------------------------------------

Specific approver for a meta data file.

.. automodule:: abisuite.handlers.file_approvers.meta_data_approver
    :members:
    :show-inheritance:

:mod:`Mpi Approver<~abisuite.handlers.file_approvers.mpi_approver>`
-------------------------------------------------------------------

Approver classes for mpi commands.

.. automodule:: abisuite.handlers.file_approvers.mpi_approver
    :members:
    :show-inheritance:

:mod:`PBS File Approver<~abisuite.handlers.file_approvers.pbs_approver>`
------------------------------------------------------------------------

Specific approver class for a batch PBS file.

.. automodule:: abisuite.handlers.file_approvers.pbs_approver
    :members:
    :show-inheritance:

:mod:`SymLink Approver<~abisuite.handlers.file_approvers.symlink_approver>`
---------------------------------------------------------------------------

Specific approver class for a symlink file.

.. automodule:: abisuite.handlers.file_approvers.symlink_approver
    :members:
    :show-inheritance:

:mod:`Abinit Approvers<~abisuite.handlers.file_approvers.abinit_appovers>`
--------------------------------------------------------------------------

.. automodule:: abisuite.handlers.file_approvers.abinit_approvers.abinit_exceptions
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.file_approvers.abinit_approvers.abinit_input_approver
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.file_approvers.abinit_approvers.anaddb_input_approver
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.file_approvers.abinit_approvers.mrgddb_input_approver
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.file_approvers.abinit_approvers.optic_input_approver
    :members:
    :show-inheritance:


:mod:`Quantum Espresso Approvers<~abisuite.handlers.file_approvers.qe_approvers>`
---------------------------------------------------------------------------------

.. automodule:: abisuite.handlers.file_approvers.qe_approvers.bases
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.file_approvers.qe_approvers.dos_input_approver
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.file_approvers.qe_approvers.dynmat_input_approver
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.file_approvers.qe_approvers.epsilon_input_approver
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.file_approvers.qe_approvers.epw_input_approver
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.file_approvers.qe_approvers.fs_input_approver
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.file_approvers.qe_approvers.ld1_input_approver
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.file_approvers.qe_approvers.matdyn_input_approver
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.file_approvers.qe_approvers.ph_input_approver
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.file_approvers.qe_approvers.pp_input_approver
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.file_approvers.qe_approvers.projwfc_input_approver
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.file_approvers.qe_approvers.pw2wannier90_input_approver
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.file_approvers.qe_approvers.pw_input_approver
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.file_approvers.qe_approvers.q2r_input_approver
    :members:
    :show-inheritance:
