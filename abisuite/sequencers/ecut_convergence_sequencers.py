import abc

from .scf_sequencer import SCFConvergenceSequencer
from .individual_phonon_sequencers import BasePhononConvergenceSequencer
from ..routines import is_list_like, is_scalar


class BaseEcutConvergenceSequencer(SCFConvergenceSequencer, abc.ABC):
    """A sequencer that can do the convergence study on the energy cutoff
    parameter.
    """
    _parameter_to_converge = "ecuts"

    @property
    def converged_ecut_variables(self):
        return self.scf_converged_parameter

    @property
    @abc.abstractmethod
    def ecuts_input_variable_name(self):
        pass

    @property
    @abc.abstractmethod
    def ecuts_units(self):
        pass

    @property
    def scf_converged_ecut(self):
        return self.scf_converged_parameter

    @property
    def scf_ecuts(self):
        if self._scf_ecuts is not None:
            return self._scf_ecuts
        raise ValueError("Need to set 'ecuts'.")

    @scf_ecuts.setter
    def scf_ecuts(self, ecuts):
        if isinstance(ecuts, float) or isinstance(ecuts, int):
            # only one ecut change to list
            ecuts = [ecuts]
        if not is_list_like(ecuts):
            raise TypeError("'ecuts' should be a list-like object.")
        # check that all ecuts are positive numbers
        for ecut in ecuts:
            if not is_scalar(ecut):
                raise TypeError(
                        f"ecuts should be positive scalar but got: '{ecut}'")
            if ecut <= 0:
                raise TypeError(
                        f"ecuts should be positive numbers but got: '{ecut}'")
        self._scf_ecuts = sorted(ecuts)  # make sure they are sorted


class BaseEcutPhononConvergenceSequencer(
        BaseEcutConvergenceSequencer,
        BasePhononConvergenceSequencer):
    """Base class for Ecut phonon convergence sequencers.
    """
    _all_sequencer_prefixes = ("scf_", "phonons_", "plot_", )

    @property
    def converged_ecut_parameter(self):
        return BasePhononConvergenceSequencer.scf_converged_parameter(self)
