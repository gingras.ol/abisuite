import os
import shutil

from .colors import Colors
from .exceptions import DirectoryExistsError
from .routines import full_abspath


class cd:
    """Context manager for changing the current working directory.
    Taken from https://stackoverflow.com/a/13197763/6362595
    """
    def __init__(self, newPath):
        self.newPath = full_abspath(newPath)

    def __enter__(self):
        self.savedPath = os.getcwd()
        os.chdir(self.newPath)

    def __exit__(self, etype, value, traceback):
        os.chdir(self.savedPath)


def which(name):
    """Python equivalent to the which Linux command.
    """
    return shutil.which(name)
    # for path in os.getenv("PATH").split(os.path.pathsep):
    #     full_path = os.path.join(path, name)
    #     if os.path.exists(full_path):
    #         """
    #         if os.stat(full_path).st_mode & stat.S_IXUSR:
    #             found = 1
    #             print(full_path)
    #         """
    #         return full_path
    # return None


def touch(path):
    """Python equivalent to the 'touch' linux script that creates an empty
    file.
    """
    if os.path.isfile(path) or os.path.islink(path):
        raise FileExistsError(path)
    if os.path.isdir(path):
        raise DirectoryExistsError(path)
    with open(path, "w"):
        pass


def rmdir(dirpath):
    """Remove an empty directory.

    Raises an error if directory is not empty.
    """
    dirpath = full_abspath(dirpath)
    if not os.path.isdir(dirpath):
        raise NotADirectoryError(dirpath)
    if len(os.listdir(dirpath)):
        raise FileExistsError(f"There are files inside '{dirpath}'")
    os.rmdir(dirpath)


def mkdir(path):
    """Python equivalent to mkdir linux tool with a few checks.
    """
    if os.path.isfile(path):
        raise FileExistsError(f"{path} is a file!")
    if os.path.isdir(path):
        return
    os.makedirs(full_abspath(path), exist_ok=True)
    # write recursively basedir
    # while not os.path.isdir(path):
    #     dirname = path
    #     while not os.path.isdir(dirname):
    #         if os.path.isfile(dirname) or os.path.islink(dirname):
    #             raise FileExistsError(
    #                     "Trying to make a directory where a file exists at: "
    #                     f"'{dirname}'.")
    #         if os.path.exists(dirname):
    #             raise FileExistsError(dirname)
    #         try:
    #             os.mkdir(dirname)
    #         except FileNotFoundError:
    #             dirname = os.path.dirname(dirname)
    #         else:
    #             break


def cat(*paths, pipe=None, overwrite=False, line_limit=None):
    """Similar to the cat linux tool. It prints the content of a file on
    screen.

    Parameters
    ----------
    paths: the list of paths to use cat on.
    pipe: str, optional
        Where to pipe the files. If None, everything is printed on screen.
    overwrite: bool, optional
        If True and the piped file already exists, it will be overwritten.
    line_limit: bool, optional
        If not None anc 'pipe' is None, then only 'line_limit' lines
        maximum will be printed on sreen. If the file (or concatanation
        of files) is longer than this
        limit, it will be stated on screen.

    Raises
    ------
    FileNotFoundError: If one of the path to cat is not found.
    FileExistsError: If the piped file exists and is not overwritten.
    TypeError: If 'line_limit' is not an int when it is not None.
    """
    if not len(paths):
        # nothing to do
        return
    if line_limit is not None:
        if not isinstance(line_limit, int):
            raise TypeError(
                    f"Expected int for 'line_limit' but got '{line_limit}'.")
    lines = []
    for path in paths:
        if not os.path.isfile(path):
            raise FileNotFoundError(path)
        with open(path, "r") as f:
            lines += f.readlines()
    if pipe is None:
        lines = [x.rstrip("\n") for x in lines]
        if not lines:
            echo("< nothing to echo >")
        if line_limit is None:
            echo(*lines)
        else:
            if len(lines) < line_limit:
                echo(*lines)
            else:
                echo(*lines[:line_limit // 2])
                print()
                print(Colors.bold_text("[...]"))
                print()
                echo(*lines[-line_limit // 2:])
                print("The number of lines to print exceeded the given line "
                      f"limit of {line_limit} lines.")
        return
    if os.path.isfile(pipe):
        if overwrite:
            os.remove(path)
        else:
            raise FileExistsError(pipe)
    with open(pipe, "w") as f:
        for line in lines:
            f.write(line)


def echo(*args):
    """Prints all the args given.
    """
    for arg in args:
        print(arg)


def ln_s(source, target):
    """Creates a symlink at location 'target' that points to 'source'.
    """
    dirname = os.path.dirname(target)
    if not os.path.exists(dirname):
        mkdir(dirname)
    os.symlink(source, target, target_is_directory=os.path.isdir(source))
