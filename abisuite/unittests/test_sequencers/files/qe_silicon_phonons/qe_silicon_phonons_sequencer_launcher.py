from abisuite import IndividualPhononSequencer as Sequencer


sequencer = Sequencer("qe")

# set scf parameters
sequencer.scf_input_variables = {
    "ibrav": 2,
    "celldm(1)": 10.3472,
    "ntyp": 1,
    "nat": 2,
    "ecutwfc": 25.0,
    "conv_thr": 1e-8,
    "pseudo_dir": "../pseudos",
    "atomic_species": [{"atom": "Si",
                        "pseudo": "Si.pbe-mt_fhi.UPF",
                        "atomic_mass": 28.085, }],
    "atomic_positions": {
        "parameter": "crystal",
        "positions": {"Si": [[0.0, 0.0, 0.0], [0.25, 0.25, 0.25]]}},
    "k_points": {
        "parameter": "automatic",
        "k_points": [4, 4, 4, 0, 0, 0],
        },
    }
sequencer.scf_workdir = "scf_run"
sequencer.scf_command_arguments = "-npool 4"
sequencer.scf_mpi_command = "mpirun -np 4"

# set phonons parameters
sequencer.nphonons = 3
sequencer.phonons_input_variables = {
    "amass(1)": 28.086,
    "nq1": 2,
    "nq2": 2,
    "nq3": 2,
    "ldisp": True,
    "tr2_ph": 1e-8,
    }
sequencer.phonons_workdir = "ph_run_q"

sequencer.launch()
