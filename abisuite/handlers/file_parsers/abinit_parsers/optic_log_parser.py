from ..bases import BaseLogParser
from ...file_structures import AbinitOpticLogStructure


class AbinitOpticLogParser(BaseLogParser):
    """An optic log file parser that gets data from an output file.

    Parameters
    ----------
    filepath : str
               The path to the log file.
    """
    _loggername = "AbinitOpticLogParser"
    _structure_class = AbinitOpticLogStructure
    _subparsers = tuple([])
