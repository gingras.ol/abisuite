from .bases import BaseAbinitDMFTParser
from ...file_structures import AbinitDMFTProjectorsStructure
from ....routines import decompose_line
import numpy as np


class AbinitDMFTProjectorsParser(BaseAbinitDMFTParser):
    """Parser to read projectors files.
    """
    _expected_ending = ".ovlp"
    _loggername = "AbinitDMFTProjectorsParser"
    _structure_class = AbinitDMFTProjectorsStructure

    def _extract_data_from_lines(self, lines):
        # DATA IS ORGANIZED AS FOLLOWS:
        # HEADER
        # ikpt = 1
        #  iband = 25
        #      1 1 1  realvalue imvalue
        #      1 1 1  realvalue imvalue
        #      ...
        #  iband = 26...

        # first extract header
        body, header = self._extract_header(lines)
        nband = self._get_nband(header)
        data = self._reshape_data(self._extract_projectors(body, nband))
        return {"nband": nband, "projectors": data}

    def _reshape_data(self, data):
        self._logger.info("Reshaping data.")
        # reshape data into an array with size:
        # nkpt x nband x nspins x natom x norb
        data = np.array(data)
        nkpt = len(data)
        nband = len(data[0])
        # NOTE: The current version of ABINIT does not print the spinor index
        # in forlb.ovlp, the projector file. O. Gingras fixed it, but it is
        # not yet in the production version of ABINIT. Thus, we use this fix
        # for now.
        if data.shape[-1] == 5:
            # find nspins
            allspins_indices = data[:, :, :, 0]
            nspin = len(np.unique(allspins_indices))
            # find natom
            allatom_indices = data[:, :, :, 1]
            natom = len(np.unique(allatom_indices))
            # find norb
            norb = []
            for a in np.unique(allatom_indices):
                block0 = data[0, 0, :]
                block1 = np.where(block0[:, 1] == a)
                norb.append(len(np.unique(block0[block1][:, 2])))
        elif data.shape[-1] == 6:
            # find nspins
            allspins_indices = data[:, :, :, 1]
            nspin = len(np.unique(allspins_indices))
            # find natom
            allatom_indices = data[:, :, :, 2]
            natom = len(np.unique(allatom_indices))
            # find norb
            norb = []
            for a in np.unique(allatom_indices):
                block0 = data[0, 0, :]
                block1 = np.where(block0[:, 2] == a)
                norb.append(len(np.unique(block0[block1][:, 3])))

        # reshape data
        for a in range(natom):
            self._logger.info("Reshaping for atom %d. The projector has shape "
                              "(nkpt, nband, nspin, norb) = (%i, %i, %i, %i)" %
                              (a, nkpt, nband, nspin, norb[a]))
        final = [np.zeros((nkpt, nband, nspin, norb[a]), dtype=complex)
                 for a in range(natom)]
        # here we drop the spin, atom and orbital indices as they are not
        # important (just arbitrary labels)
        # TODO: consider if those indices are useful to be kept.
        pre_norb = []
        tot_norb = 0
        i = 0
        for a in range(natom):
            pre_norb.append(i)
            i += norb[a]
            tot_norb += norb[a]

        for ik, kpt_block in enumerate(data):
            for ib, band in enumerate(kpt_block):
                for s in range(nspin):
                    for a in range(natom):
                        for o in range(norb[a]):
                            re = band[s * tot_norb + pre_norb[a] + o][-2]
                            im = band[s * tot_norb + pre_norb[a] + o][-1]
                            final[a][ik, ib, s, o] = re + 1j * im
        self._logger.info("Reshaping done.")
        return final

    def _get_nband(self, header):
        self._logger.info("Extracting number of bands for checkups.")
        # get total number of bands
        dataline = header[-1]
        s, i, f = decompose_line(dataline)
        return i[1] - i[0] + 1

    def _extract_header(self, lines):
        self._logger.info("Extracting header from file.")
        # return data lines separated from header
        for i, line in enumerate(lines):
            if "ikpt" in line:
                return lines[i:], lines[:i]
        raise LookupError("Could not separate header...")

    def _extract_projectors(self, lines, nband):
        self._logger.info("Starting data extraction.")
        data = []
        end = 0
        for i, line in enumerate(lines):
            if i < end:
                continue
            if "ikpt" in line:
                # start of kpt block
                kptdata, adv = self._extract_data_from_kpt_block(lines[i:])
                # check that we received correct number of bands
                if len(kptdata) != nband:
                    raise ValueError("Was expecting %i bands but read %i"
                                     " instead" % (len(kptdata), nband))
                data.append(kptdata)
                end += adv
        return data

    def _extract_data_from_kpt_block(self, lines):
        self._logger.debug("Extracting data from a kpt block.")
        data = []
        end = 0
        block_end = False
        for i, line in enumerate(lines):
            if i < end:
                continue
            if "iband" in line:
                (banddata,
                 adv) = self._extract_data_from_band_block(lines[i:])
                data.append(banddata)
                end += adv
            if "ikpt" in line:
                if not block_end:
                    block_end = True
                else:
                    # arrived at the end of kpt block
                    return data, i
        # arrived at the end of file
        return data, len(lines) - 1

    def _extract_data_from_band_block(self, lines):
        self._logger.debug("Extracting data from a band block.")
        data = []
        block_end = False
        for i, line in enumerate(lines):
            if "iband" in line:
                if block_end:
                    return data, i
                else:
                    block_end = True
            elif "ikpt" in line:
                # end of block
                return data, i
            else:
                # line containing data
                # since we do not know how many orbitals/atoms/spins
                # there are, just
                # gather data until end of block as a huge table
                s, i, f = decompose_line(line)
                data.append(i + f)
                block_end = True
        # arrived at the end of file
        return data, len(lines) - 1
