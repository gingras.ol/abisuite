import os
import shutil
import subprocess

from .bases import BaseAbisuiteSubScript
from ..handlers import (
        is_calculation_directory, CalculationDirectory, GenericDirectory,
        )
from ..launchers import AbinitCut3DLauncher
from ..linux_tools import which
from ..routines import full_abspath


__CUT3D_RUN_BASENAME__ = "cut3d_run"


class AbisuiteVisualizeStructureScript(BaseAbisuiteSubScript):
    """Executes the 'visualize_structure' subcommand of the abisuite script."""
    _loggername = "AbisuiteVisualizeStructureScript"

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.calc_path = None
        self.den_file = None
        self.xsf_file = None

    def generate_xsf_file(self, force=False):
        """Generate the xsf file if it does not exists.

        Parameters
        ----------
        force: bool, optional
            If xsf file already exists,
        """
        if self.xsf_file is not None and not force:
            print("XSF file already exists. Don't need to generate.")
            return
        if __CUT3D_RUN_BASENAME__ in os.listdir(
                os.path.dirname(self.den_file)):
            if force:
                # clean it up
                shutil.rmtree(
                        os.path.join(
                            os.path.dirname(self.den_file),
                            __CUT3D_RUN_BASENAME__))
            if self.xsf_file is None and not force:
                print("Use: --force in order to redo cut3d run.")
                return
        # xsf does not exist => generate it using cut3d
        # create cut3d launcher
        print("Launching 'cut3d' to generate 'xsf' file.")
        launcher = AbinitCut3DLauncher(loglevel=self._loglevel)
        if self.calc_path is not None:
            # generate from a calculation
            launcher.workdir = self.calc_path
        else:
            # generate next to the DEN file
            launcher.workdir = os.path.join(
                    os.path.dirname(self.den_file), __CUT3D_RUN_BASENAME__)
            # link den file
            launcher.link_den_file(self.den_file)
        launcher.write()
        launcher.run()
        self.xsf_file = os.path.join(
                launcher.calculation_directory.output_data_directory.path,
                launcher.input_file.output_file_path)
        if os.path.isfile(self.xsf_file):
            self.success("Generated XSF file.")
        else:
            self.error("An error occured while generating XSF file.")

    def load_den_file(self, calc_or_file):
        """Loads up the DEN file to visualize.

        Parameters
        ----------
        calc_or_file: str
            Path towards the calculation to visualize or file path
            of the DEN file to visualize.
        """
        if is_calculation_directory(calc_or_file):
            self._load_den_file_from_calc(calc_or_file)
        else:
            self._load_den_file_from_file(calc_or_file)

    def visualize_structure(self):
        """Visualize the crystal structure by calling VESTA on the
        (possibly generated) xsf file representing the electronic density.
        """
        if self.xsf_file is None:
            self.error("No xsf file available.")
            return
        # launch vesta if it is available
        if which("vesta") is not None:
            print("Launching 'vesta' to visualize structure.")
            subprocess.run(["vesta", self.xsf_file])
        else:
            self.error("'vesta' not available in 'PATH'.")

    def _load_den_file_from_calc(self, calcpath):
        with CalculationDirectory.from_calculation(calcpath) as calc:
            if calc.calctype != "abinit":
                raise TypeError(
                        f"Not an 'abinit' calculation: '{calcpath}'.")
            # check if an xsf file already exists
            with calc.output_data_directory as output:
                for filehandler in output:
                    if filehandler.path.endswith(".xsf"):
                        # xsf file already exists
                        self.xsf_file = filehandler.path
                    if "DEN" in filehandler.path:
                        self.den_file = filehandler.path
                    if self.xsf_file is not None and self.den_file is not None:
                        return

    def _load_den_file_from_file(self, filepath):
        if not os.path.isfile(filepath):
            raise FileNotFoundError(filepath)
        self.den_file = full_abspath(filepath)
        if __CUT3D_RUN_BASENAME__ in os.listdir(
                os.path.dirname(self.den_file)):
            # this dir is not a regular calc dir. thus we manually walk through
            dir_ = GenericDirectory(loglevel=self._loglevel)
            dir_.path = os.path.join(
                    os.path.dirname(self.den_file), __CUT3D_RUN_BASENAME__)
            for handler in dir_.walk(paths_only=False):
                if handler.basename.endswith(".xsf"):
                    self.xsf_file = handler.path
                    print("Found '.xsf' file already existing.")
                    return
            else:
                self.error("cut3d run dir exists but no xsf file found.")
