import os

from .bases import BaseQEDOSParser
from ...file_structures import QEDOSDOSStructure
from ....routines import decompose_line


class QEDOSDOSParser(BaseQEDOSParser):
    """Class that parses a dos file created by the 'dos.x'
    Quantum Espresso script.
    """
    _expected_ending = ".dos"
    _loggername = "QEDOSDOSParser"
    _structure_class = QEDOSDOSStructure

    def _extract_data_from_array(self, data):
        fermi_energy = self._extract_fermi_level(self.path)
        return {"energies": data[:, 0],
                "dos": data[:, 1],
                "integrated_dos": data[:, 2],
                "fermi_energy": fermi_energy
                }

    def _extract_fermi_level(self, path):
        with open(path, "r") as f:
            lines = f.readlines()
        s, i, f = decompose_line(lines[0].strip("\n").strip("#"))
        try:
            return f[-1]
        except IndexError:
            self._logger.error("Could not get the Fermi Level from "
                               f"{self.path}")
            return None

    @classmethod
    def _filepath_from_meta(cls, meta, *args, **kwargs):
        """Creates a DOSDOSParser from a MetaFileData object.
        """
        expected = os.path.join(meta.output_data_dir,
                                meta.jobname + cls._expected_ending)
        if not os.path.isfile(expected):
            raise FileNotFoundError("Could not find dos file from meta file."
                                    " User should specify file manually for "
                                    "the parser.")
        return expected
