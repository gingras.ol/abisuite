#!/bin/bash

MPIRUN="mpirun -np 4"
EXECUTABLE="/home/fgoudreault/Workspace/abinit/abinit/build/develop/2021-04-20/src/98_main/abinit"
INPUT=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_workflows/test_files/abinit_si/nc/phonon_dispersion_qgrid_convergence/qgrid_2_2_2/phonons_runs/ph_q1/run/ph_q1.files
LOG=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_workflows/test_files/abinit_si/nc/phonon_dispersion_qgrid_convergence/qgrid_2_2_2/phonons_runs/ph_q1/ph_q1.log
STDERR=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_workflows/test_files/abinit_si/nc/phonon_dispersion_qgrid_convergence/qgrid_2_2_2/phonons_runs/ph_q1/ph_q1.stderr


$MPIRUN $EXECUTABLE < $INPUT > $LOG 2> $STDERR
