.. _common_api:

Abisuite Common Functions and Classes
=====================================

.. contents::
    :local:

There are some classes and functions which are used by some or all
of abisuite's submodules. This section makes a list of them.


:mod:`Base Classes<~abisuite.bases>`
------------------------------------

This module contains all of abisuite's base classes.

.. automodule:: abisuite.bases
    :members:
    :show-inheritance:

:mod:`Colors<~abisuite.colors>`
-------------------------------

This module contains classes and functions to make colorful strings
with some other text formats.

.. automodule:: abisuite.colors
    :members:
    :show-inheritance:

:mod:`Config File<~abisuite.config>`
------------------------------------

This module contains the classes to handle the user's config file.
See the `Config File Documentation <config_file>` for more details.

.. automodule:: abisuite.config
    :members:
    :show-inheritance:

:mod:`Constants<~abisuite.constants>`
-------------------------------------

This module contains physical, mathematical or numerical constants.

.. automodule:: abisuite.constants
    :members:
    :show-inheritance:

:mod:`Global Exceptions<~abisuite.exceptions>`
----------------------------------------------

This module contains global exceptions used throughout abisuite.

.. automodule:: abisuite.exceptions
    :members:
    :show-inheritance:

:mod:`On Init Objects <~abisuite.__init__>`
-------------------------------------------

This module contains objects created upon abisuite's initialization.
These can be accessed everywhere. They were defined in order to be single instances.

.. automodule:: abisuite.__init__
    :private-members:
    :special-members:
    :members:
    :show-inheritance:

.. py:data:: abisuite.__init__.__USER_CONFIG__

    Contains all of the user's configuration file values.

:mod:`Linux Tools<~abisuite.linux_tools>`
-----------------------------------------

Contrary to what this module's name could infer, this module does not contain tools
that are exclusive to Linux but rather Linux-like tools that should work on any
OS. Useful for development.

.. automodule:: abisuite.linux_tools
    :members:
    :show-inheritance:

:mod:`Global Routines <~abisuite.routines>`
-------------------------------------------

This module contains global functions used everywhere.

.. automodule:: abisuite.routines
    :members:
    :show-inheritance:
