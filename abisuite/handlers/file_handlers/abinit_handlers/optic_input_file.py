from ..bases import BaseInputFileHandler
from ...file_approvers import AbinitOpticInputApprover
from ...file_parsers import AbinitOpticInputParser
from ...file_structures import AbinitOpticInputStructure
from ...file_writers import AbinitOpticInputWriter


class AbinitOpticInputFile(BaseInputFileHandler):
    """A file handler for the input file of the optic script.
    """
    _approver_class = AbinitOpticInputApprover
    _loggername = "AbinitOpticInputFile"
    _parser_class = AbinitOpticInputParser
    _structure_class = AbinitOpticInputStructure
    _writer_class = AbinitOpticInputWriter
