from .individual_phonon_sequencers import (
        QEPhononConvergenceSequencer,
        )
from ..kgrid_convergence_sequencers import (
        BaseKgridConvergenceSequencer,
        )


class QEKgridConvergenceSequencer(BaseKgridConvergenceSequencer):
    """Convergence sequencer for the k-points parameter with Quantum Espresso.
    """
    kgrids_input_variable_name = "k_points"
    _loggername = "QEKgridConvergenceSequencer"

    def __init__(self, *args, **kwargs):
        BaseKgridConvergenceSequencer.__init__(self, "qe", *args, **kwargs)

    @BaseKgridConvergenceSequencer.scf_kgrids.setter
    def scf_kgrids(self, kgrids):
        BaseKgridConvergenceSequencer.scf_kgrids.fset(self, kgrids)
        # need to reset kgrids as a list of dicts if not already done
        if isinstance(kgrids[0], dict):
            # everything already set
            return
        # else we need to convert arrays to the correct dicts
        self._scf_kgrids = [
                {"parameter": "automatic",
                 "k_points": x} for x in kgrids]

    def _add_xtick_labels_to_convergence_plot(self, plot):
        BaseKgridConvergenceSequencer._add_xtick_labels_to_convergence_plot(
                self, plot)
        kgrids = [x["k_points"][:3] for x in self.scf_kgrids]
        plot.xtick_labels = [(i, "x".join([str(k) for k in x]))
                             for i, x in enumerate(kgrids)]

    # override this method as kpoints input var is a dict.
    def _convert_param_to_str(self, param, *args):
        kgrid = param["k_points"]
        return f"{kgrid[0]}_{kgrid[1]}_{kgrid[2]}"


class QEKgridPhononConvergenceSequencer(
        QEKgridConvergenceSequencer,
        QEPhononConvergenceSequencer,
        ):
    """Convergence sequencer for the 'k_points' SCF parameter vs the phonon
    frequencies.
    """
    _all_sequencer_prefixes = ("scf_", "phonons_", "plot_", )
    _loggername = "QEKgridPhononConvergenceSequencer"

    def __init__(self, *args, **kwargs):
        QEKgridConvergenceSequencer.__init__(self, *args, **kwargs)
        QEPhononConvergenceSequencer.__init__(self, *args, **kwargs)
