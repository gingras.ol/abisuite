from .bases import BaseQERestarter


class QEMatdynRestarter(BaseQERestarter):
    """Restarter class for a qe_matdyn calculation for the matdyn.x script
    of Quantum Espresso.
    """
    _calctype = "qe_matdyn"
    _loggername = "QEMatdynRestarter"
