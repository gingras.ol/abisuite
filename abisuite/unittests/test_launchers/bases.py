import os
import tempfile

from ..routines_for_tests import copy_calculation
from ...databases import AbiDB
from ...exceptions import DevError
from ...handlers import MetaDataFile, SymLinkFile
from ...routines import is_list_like


class BaseLauncherTestNoInputVariables:
    _calctype = None
    _command_flags = None
    _jobname = None
    _launcher_class = None
    _modules = ["one_module"]
    _mpi_command = "mpirun"
    _nodes = 1
    _ppn = 2

    def setUp(self):
        # create temporary abinit script
        self.command_file = tempfile.NamedTemporaryFile(suffix=self._calctype)
        self.command = self.command_file.name
        if self._command_flags is not None:
            if not self._command_flags.startswith(" "):
                self.command += " " + self._command_flags
            else:
                self.command += self._command_flags
        self.tempdir = tempfile.TemporaryDirectory()
        self.workdir = os.path.join(self.tempdir.name, self._jobname)
        self.launcher = self._launcher_class(jobname=self._jobname)
        self.launcher.workdir = self.workdir
        self.launcher.allow_connection_to_database = False
        self.launcher.command = self.command
        self.launcher.modules = self._modules
        self.launcher.ppn = self._ppn
        self.launcher.nodes = self._nodes
        self.launcher.mpi_command = self._mpi_command
        self.launcher.queuing_system = "local"

    def tearDown(self):
        self.command_file.close()
        self.tempdir.cleanup()
        del self.launcher.meta_data_file.copied_files
        self.launcher.clear_input_files()
        del self.tempdir

    def test_basic_properties(self):
        # test assigning properties
        # jobname
        self.assertEqual(self.launcher.jobname, self._jobname)
        # workdir
        self.assertEqual(self.launcher.workdir, self.workdir)
        # modules
        mods = self._modules
        if not isinstance(self._modules, list):
            mods = [self._modules]
        self.assertListEqual(self.launcher.modules, mods)
        # ppn
        self.assertEqual(self.launcher.ppn, self._ppn)
        # nodes
        self.assertEqual(self.launcher.nodes, self._nodes)
        # command
        self.assertEqual(self.launcher.command, self.command)
        # mpi_command
        self.assertEqual(self.launcher.mpi_command, self._mpi_command)
        # input data dir
        idd = os.path.join(self.workdir, "input")
        self.launcher.input_data_dir.path = idd
        self.assertEqual(self.launcher.input_data_dir.path, idd)
        # output data dir
        odd = os.path.join(self.workdir, "output")
        self.launcher.output_data_dir.path = odd
        self.assertEqual(self.launcher.output_data_dir.path, odd)
        # run dir
        rd = os.path.join(self.workdir, "run")
        self.launcher.rundir.path = rd
        self.assertEqual(self.launcher.rundir.path, rd)
        # quality of service and its alias
        qos = "qos"
        self.launcher.quality_of_service = qos
        self.assertEqual(self.launcher.quality_of_service, qos)
        self.assertEqual(self.launcher.qos, qos)
        qos += "new"
        self.launcher.qos = qos
        self.assertEqual(self.launcher.quality_of_service, qos)
        self.assertEqual(self.launcher.qos, qos)

    def test_add_input_file(self):
        # create input files
        files = ["file1", "file2", "file3"]
        suffixes = ["_EIG", "_WFK", "_DEN"]
        temps = []
        filepaths = []
        tmpdir = tempfile.TemporaryDirectory()
        for fil, suffix in zip(files, suffixes):
            temps.append(tempfile.NamedTemporaryFile(suffix=suffix,
                                                     prefix=fil,
                                                     dir=tmpdir.name))
        for temp in temps:
            filepaths.append(temp.name)

        # check files are added one shot
        self.launcher.add_input_file(filepaths)
        links = []
        for item in self.launcher.input_data_dir.walk(paths_only=False):
            if isinstance(item, SymLinkFile):
                links.append(item.source)
        sources = self.launcher.meta_data_file.linked_files
        for fil in filepaths:
            # check input file has been added to input data dir and meta data
            self.assertIn(fil, links)
            self.assertIn(fil, sources)
        self.launcher.clear_input_files()
        # check star notation
        star = os.path.join(tmpdir.name, "file*.dat")
        self.launcher.add_input_file(star)
        for fil in filepaths:
            self.assertIn(fil, links)
            self.assertIn(fil, sources)

    def test_link_raise(self):
        # test that an error is raised when we want to link a non existent file
        with self.assertRaises(FileNotFoundError):
            path = os.path.join(self.workdir, "odat_random_file_EIG")
            self.launcher.add_input_file(path)
            self.launcher.queuing_system = "local"
            self.launcher.write()

    def test_clean_workdir(self):
        self.launcher.write()
        self.launcher.clean_workdir()
        self.assertFalse(os.path.exists(self.launcher.workdir))

    def test_raise_when_wrong_script(self):
        with self.assertRaises(ValueError):
            self.launcher.command = "Wrong script name"

    def test_launcher_write(self):
        self.launcher.queuing_system = "local"
        self.launcher.write()
        # check that input file is created in good dir
        self.assertTrue(os.path.isdir(self.launcher.workdir))
        self.assertTrue(os.path.isfile(self.launcher.input_file.path))
        self.assertTrue(os.path.isfile(self.launcher.pbs_file.path))
        self.assertTrue(os.path.isdir(self.launcher.input_data_dir.path))
        self.assertTrue(os.path.isdir(self.launcher.rundir.path))
        self.assertTrue(os.path.isdir(self.launcher.output_data_dir.path))

    def test_automatic_database_entry(self):
        # create temporary database
        dbtempdir = tempfile.TemporaryDirectory()
        db_path = os.path.join(dbtempdir.name, "test.db")
        try:
            db = AbiDB()
            db.create_database(db_path)
            # artificially connect calc dir to unittest database
            self.launcher.calculation_directory.connect_to_database(
                    database=db_path)
            # we don't really need to validate here
            self.launcher.write(
                    bypass_validation=True, bypass_pbs_validation=True)
            # artificially call add
            self.launcher.calculation_directory.add_to_database()
            # check that calculation has been appended to database
            self.assertIn(self.launcher.workdir, db)
        # clean
        finally:
            dbtempdir.cleanup()


class BaseLauncherTest(BaseLauncherTestNoInputVariables):
    """Test case base class to test Launchers with input variables.
    """
    _input_vars = None

    def setUp(self):
        if self._input_vars is None:
            raise DevError(
                f"Need to set '_input_vars' in '{self.__class__}'.")
        super().setUp()
        self.input_variables = self._input_vars.copy()
        self.launcher.input_variables = self.input_variables

    def test_basic_properties(self):
        super().test_basic_properties()
        # also test input variables
        self.assertTrue(self.launcher.input_variables is not None)
        for k in self.input_variables:
            self.assertIn(k, self.launcher.input_variables)

    def test_raise_when_no_variables(self):
        self.launcher.clear_input_variables()
        with self.assertRaises(ValueError):
            self.launcher.command = self.command
            self.launcher.write(force_queuing_system="local")


class BaseLauncherWithParentsTest(BaseLauncherTest):
    _link_calculation = None

    def setUp(self):
        if self._link_calculation is None:
            raise DevError("Need to set '_link_calculation'.")
        if not is_list_like(self._link_calculation):
            tolink = [self._link_calculation]
        else:
            tolink = self._link_calculation
        super().setUp()
        self._linked_calc = self._copy_calc_to_link(tolink)
        for calc_to_link in self._linked_calc:
            self.launcher.link_calculation(calc_to_link)

    def test_clear_calculations(self):
        # test that clearing calculations doesn't affect workflow.
        # in the sense that everything has been removed and that
        # we can safely readd calculations afterward
        self.launcher.clear_linked_calculations()
        self.assertEqual(self.launcher.meta_data_file.parents, [])
        self.assertEqual(self.launcher.meta_data_file.copied_files, [])
        self.assertEqual(self.launcher.meta_data_file.linked_files, [])
        for calc_to_link in self._linked_calc:
            self.launcher.link_calculation(calc_to_link)
        self.launcher.write()

    def test_linked_calculation(self):
        # check children have been added in linked calculation
        self.launcher.write()
        for calc in self._linked_calc:
            with MetaDataFile.from_calculation(calc) as meta:
                self.assertIn(self.launcher.workdir, meta.children)
            self.assertIn(calc, self.launcher.meta_data_file.parents)

    def test_linking_against_database(self):
        # create a db
        db_tempdir = tempfile.TemporaryDirectory()
        db_path = os.path.join(db_tempdir.name, "test.db")
        # purge calculations
        self.launcher.clear_linked_calculations()
        try:
            db = AbiDB()
            db.create_database(db_path, overwrite=True)
            for calc in self._linked_calc:
                db.add_calculation(calc)
                # link from database
                self.launcher.link_calculation(db[calc], database=db.db)
            self.launcher.write()
            for calc in self._linked_calc:
                with MetaDataFile.from_calculation(calc) as meta:
                    self.assertIn(self.launcher.workdir, meta.children)
                self.assertIn(calc, self.launcher.meta_data_file.parents)
        finally:
            db_tempdir.cleanup()

    def _copy_calc_to_link(self, calcs):
        # need to modify meta files because meta file are LOCAL
        # do this in a temporary directory to not modify file inplace
        self._link_tempdir = tempfile.TemporaryDirectory()
        newcalcs = []
        for i, calc in enumerate(calcs):
            whereto = os.path.join(self._link_tempdir.name, "calc" + str(i))
            copy_calculation(calc, whereto)
            newcalcs.append(whereto)
        return newcalcs


class BaseMassLauncherTest:
    _mass_launcher_class = None
    _calctype = None
    _jobnames = None
    _common_vars = None
    _specific_vars = None
    _pseudos = None
    _walltime = "1:00:00"
    _ppn = 1
    _nodes = 1
    _modules = ["first module", "second module"]
    _link_calculation = None

    def setUp(self):
        self._link_tempdir = None
        self.command_file = tempfile.NamedTemporaryFile(suffix=self._calctype)
        self.command = self.command_file.name
        self.tempdir_obj = tempfile.TemporaryDirectory()
        self.tempdir = self.tempdir_obj.name
        self.masslauncher = self._mass_launcher_class(self._jobnames)
        self.masslauncher.workdir = self.tempdir
        self.masslauncher.allow_connection_to_database = False
        self.masslauncher.command = self.command
        self.masslauncher.walltime = self._walltime
        self.masslauncher.ppn = self._ppn
        self.masslauncher.nodes = self._nodes
        self.masslauncher.modules = self._modules
        self.masslauncher.queuing_system = "local"
        self.masslauncher.common_input_variables = self._common_vars
        self.masslauncher.specific_input_variables = self._specific_vars
        if self._pseudos is not None:
            self.masslauncher.common_pseudos = self._pseudos
        if self._link_calculation is not None:
            # need to modify meta files because meta file are LOCAL
            # do this in a temporary directory to not modify file inplace
            self._link_tempdir = tempfile.TemporaryDirectory()
            whereto = os.path.join(self._link_tempdir.name, "calc")
            copy_calculation(self._link_calculation, whereto)
            # print(os.listdir(whereto))
            # print(os.listdir(whereto + "/run"))
            # cat(whereto + "/.pw.meta")
            # cat(whereto + "/run/pw.sh")
            self.masslauncher.link_calculation(whereto)

    def tearDown(self):
        self.command_file.close()
        self.tempdir_obj.cleanup()
        del self.tempdir_obj
        del self.masslauncher
        if self._link_tempdir is not None:
            self._link_tempdir.cleanup()
            del self._link_tempdir

    def test_basic_properties(self):
        # test assigning properties and make sure they are well set
        # workdir
        self.assertEqual(self.masslauncher.workdir, self.tempdir)
        # common input variables
        self.assertDictEqual(self.masslauncher.common_input_variables,
                             self._common_vars)
        for launcher in self.masslauncher.launchers:
            self.assertFalse(launcher.input_variables is None)
            for k in self._common_vars:
                self.assertIn(k, launcher.input_variables)
        # specific input variables
        self.assertListEqual(self.masslauncher.specific_input_variables,
                             self._specific_vars)
        for launcher, spec in zip(self.masslauncher.launchers,
                                  self._specific_vars):
            for k, v in spec.items():
                self.assertIn(k, launcher.input_variables)
                self.assertEqual(launcher.input_variables[k], v)
        # command
        self.assertEqual(self.masslauncher.command, self.command)
        for launcher in self.masslauncher.launchers:
            self.assertEqual(launcher.command, self.command)
        # walltime
        self.assertEqual(self.masslauncher.walltime,
                         self._walltime)
        for launcher in self.masslauncher.launchers:
            self.assertEqual(launcher.walltime, self._walltime)
        # ppn
        self.assertEqual(self.masslauncher.ppn, self._ppn)
        for launcher in self.masslauncher.launchers:
            self.assertEqual(launcher.ppn, self._ppn)
        # nodes
        self.assertEqual(self.masslauncher.nodes, self._nodes)
        for launcher in self.masslauncher.launchers:
            self.assertEqual(launcher.nodes, self._nodes)
        # modules
        self.assertListEqual(self.masslauncher.modules, self._modules)
        for launcher in self.masslauncher.launchers:
            self.assertListEqual(launcher.modules, self._modules)

    def test_mass_launcher_works(self):
        self.masslauncher.write()
        subdirs = [os.path.join(self.tempdir, jobname)
                   for jobname in self._jobnames]
        for subdir in subdirs:
            self.assertTrue(os.path.exists(subdir))

    def test_raise_var_not_good_len(self):
        with self.assertRaises(ValueError):
            specvar = self._specific_vars.copy()
            del specvar[-1]
            self.masslauncher.specific_input_variables = specvar
